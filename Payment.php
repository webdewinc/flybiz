<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        
    ];

    public function cartlist() {        
        return $this->hasMany('App\AddToCart','mst_payment_id','id');
    }

    public function users() {
        return $this->hasOne('App\User','user_id','user_id');
    }
    public function getpromos() {
        return $this->hasOne('App\Promo_code','promo_id','promo_code');
    }
}
