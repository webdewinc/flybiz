<?php

function book_appointment_ajax_request() {
    if (isset($_POST['user_email'])) {
    
        $user_full_name = $_POST['user_full_name'];
        $user_name = $_POST['user_email'];
        $user_company_name = $_POST['user_company_name'];
        $user_mobile_number = $_POST['user_mobile_number'];
        $user_email = $_POST['user_email'];
        //$user_company_name = $_POST['user_company_name'];
        $user_industry = $_POST['user_industry'];
        //$user_country_code = $_POST['user_country_code'];
        $authKey = "122573AyFLqoumaVR5cd9177b";
        // curl request
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "http://control.msg91.com/api/add_client.php?authkey=$authKey&user_full_name=$user_full_name&user_name=$user_name&user_mobile_number=$user_mobile_number&user_email=$user_email&user_company_name=$user_company_name&user_industry=$user_industry&user_country_code=91",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_SSL_VERIFYHOST => 0,
          CURLOPT_SSL_VERIFYPEER => 0,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        $data = json_decode($response);

        if($data->msg_type == 'error'){
            if(@$data->msg_arr->user_name){
                $array = ['status' =>  false, 'message' => $data->msg_arr->user_name];
            }
            if(@$data->msg_arr->user_full_name){
                $array = ['status' =>  false, 'message' => $data->msg_arr->user_full_name];
            }
            if(@$data->msg_arr->user_email){
                $array = ['status' =>  false, 'message' => $data->msg_arr->user_email];
            }
            if(@$data->msg_arr->user_mobile_number){
                $array = ['status' =>  false, 'message' => $data->msg_arr->user_mobile_number];
            }
        } else {
            $array = ['status' =>  true, 'message' => 'Registration successful!'];
        }
        curl_close($curl);
        if ($err) {
          echo "cURL Error #:" . $err;
        }
        header('Content-Type: application/json');
        echo json_encode($array);
        exit;
    }
}
 
add_action( 'wp_ajax_book_appointment_ajax_request', 'book_appointment_ajax_request' );

?>