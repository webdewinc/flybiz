<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class MstSearchUrl extends Authenticatable
{
    protected $table = 'mst_search_url';
    protected $primaryKey = 'surl_id';

    protected $guarded = [
        
    ];


}
