<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class MstDaPa extends Authenticatable
{
    protected $table = 'mst_da_pa';
    protected $primaryKey = 'dp_id';
    protected $guarded = [
        
    ];
}
