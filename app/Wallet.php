<?php
namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        
    ];

    public function orderDetails() {
        return $this->hasOne('App\AddToCart','id','order_id');
    }
    public function username() {
        return $this->hasOne('App\User','user_id','user_id');
    }
    public function paymentDetails() {        
        return $this->hasOne('App\Payment','id','payment_id');
    }
}
