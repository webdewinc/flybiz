<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Chat extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        
    ];

    public function chatroom() {
        return $this->hasOne('App\ChatRoom','id','chatroom_id');
    }
    public function sender() {
        return $this->hasOne('App\User','user_id','sender_id');
    }
    public function receiver() {
        return $this->hasOne('App\User','user_id','receiver_id');
    }
}
