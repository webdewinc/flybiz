<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class LogSearchQuery extends Authenticatable
{
    protected $table = 'log_searchquery';
    protected $primaryKey = 'query_id';
    protected $guarded = [
        
    ];
}
