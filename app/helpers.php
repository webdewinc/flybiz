<?php
// Function to convert CSV into associative array
function csvToArray($file, $delimiter) { 
    if (($handle = fopen($file, 'r')) !== FALSE) { 
        $i = 0; 
        while (($lineArray = fgetcsv($handle, 4000, $delimiter, '"')) !== FALSE) { 
            for ($j = 0; $j < count($lineArray); $j++) {
                $arr[$i][$j] = $lineArray[$j]; 
            } 
            $i++; 
        } 
        fclose($handle); 
    } 
    return $arr; 
}
function fixForUri($string){
   $slug = trim($string); // trim the string
   $slug= preg_replace('/[^a-zA-Z0-9 -]/','',$slug ); // only take alphanumerical characters, but keep the spaces and dashes too...
   $slug= str_replace(' ','-', $slug); // replace spaces by dashes
   $slug= strtolower($slug);  // make it lowercase
   return $slug;
}
function secureEncrypt($str){
    $secureString = "eyJpd6IjBepdZZS9Fa";
    for ($i = -1; $i <= 2; $i++) { 
        $str = $secureString.$i.$str;
        $str = \Crypt::encrypt($str);
    }
    return $str;
}
function secureDecrypt($str){
  $secureString = "eyJpd6IjBepdZZS9Fa";
  for ($i = 2; $i >= -1; $i--) { 
      $str = \Crypt::decrypt($str);
      $s = $secureString.$i;
      $str = explode($s, $str);
      $str = $str[1];
  }
  return $str;
}

function domain_status($url){
  
    $d_id = \DB::table('mst_domain_url')->select('domain_id')->where('domain_url',$url)->where('domain_status',1)->first();
    $d_id = json_decode(json_encode($d_id), true);
    
    if( $d_id):
        $domain_id = $d_id['domain_id'];    
        echo '<i class="flaticon2-correct kt-font-primary"></i>';
    endif;
}

function totalOrderCount($url){
    echo $count = \App\AddToCart::whereIn('is_billed', [0])->where('fk_domain_url',$url)->count();
}

function _promo_codes($promo_id,$amount){
    $promo_code = DB::table('promo_codes')->where('promo_id',$promo_id)->first();
    $promo_code = json_decode(json_encode($promo_code), true);
    if(!empty($promo_code)){
        if($promo_code['promo_type'] == 'percentage'){
            $v = (( $amount * $promo_code['promo_number'] ) / 100 );
            $amount = $amount - $v;
        } else {
            if($promo_code['promo_type'] != 'owner'){            
                $amount = $amount - $promo_code['promo_number'];
            }
        } 
    }
    return $amount;
}

function flockNotification($string){

  if($string != strip_tags($string)) {
      $data['flockml'] =  "<flockml>".$string."</flockml>" ;  
  } else {
      $data['text'] =  $string;
  }
  $argg['data'] = json_encode($data);
  $argg['url']  = env('FLOCK_URL'); 
  $res = curlAccessFlock('POST',$argg);
}
function curlAccessFlock($method, $array, $content_type = 'array' ) {
        
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $array['url']);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    switch ($method){
      case "POST":
        curl_setopt($ch, CURLOPT_POST, 1);        
         if ($array['data'])
            curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
         break;
      case "PUT":
         curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
         if ($array['data'])
            curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
         break;
      default:
         curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    }    

    $headers = array();
    $headers[] = 'Content-Type: application/json';
    
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $result = curl_exec($ch);
    
    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    }
    curl_close($ch);

    if($content_type == 'array'){
        $result = json_decode($result); 
    }  
    return $result;
}
  
function buyerDomainPriceCalculate($amount, $seller_extra_price = 40, $webdew_price = 10, $webdew_percentage = 10) {

    

    $seller_price = (float)$amount + (float)$seller_extra_price;
    $webdew_price = ( $seller_price + $webdew_price );

    $webdew_percentage = ($webdew_price * $webdew_percentage / 100);
    $round_digit =  ( ( $webdew_price + $webdew_percentage  )%10 );
    if($round_digit > 0){
        $round_digit = (10 - $round_digit );
    } 
    return intval($webdew_price + $webdew_percentage + $round_digit);
}

function buyerDomainWebsitePriceCalculate($amount, $seller_extra_price = 0, $webdew_price = 10, $webdew_percentage = 10) {

    $seller_price = (float)$amount + (float)$seller_extra_price;
    $webdew_price = ( $seller_price + $webdew_price );

    $webdew_percentage = ($webdew_price * $webdew_percentage / 100);
    $round_digit =  ( ( $webdew_price + $webdew_percentage  )%10 );
    if($round_digit > 0){
        $round_digit = (10 - $round_digit );
    } 
    return intval($webdew_price + $webdew_percentage + $round_digit);

}
function adminDomainPriceCalculate($amount, $seller_extra_price = 0, $webdew_price = 10, $webdew_percentage = 10) {
    

    $seller_price = (float)$amount + (float)$seller_extra_price;
    
    $webdew_price = ( $seller_price + $webdew_price );
    $webdew_percentage = ($webdew_price * $webdew_percentage / 100);
    $round_digit =  ( ( $webdew_price + $webdew_percentage  )%10 );

    if($round_digit > 0){
        $round_digit = (10 - $round_digit );
    } 
    return intval( $webdew_price + $webdew_percentage + $round_digit ) - intval($seller_price);
}

function total_website_count() {
    // with another group
    // $selectQuery = \App\SearchRecord::where('domain_approval_status',1)->where('cost_price','>',0)->count();
    // return $selectQuery;
  $selectQuery = \App\MstDomainUrl::select('domain_id')->where('domain_approval_status',1)->where('cost_price','>',0)->paginate(1);
  $total = $selectQuery->total();
  return $total;
}
function switchedOther(){
    if(Auth::check()):
        $switched = 'advertiser';
        $switched_done = \App\User::where('user_id',Auth::user()->user_id)->update(['switched'=>$switched]);
    endif;
}

 function curlAccessEmail($method, $array, $content_type = 'array' ) {
        
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $array['url']);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    switch ($method){
      case "POST":
        curl_setopt($ch, CURLOPT_POST, 1);        
         if ($array['data'])
            curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
         break;
      case "PUT":
         curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
         if ($array['data'])
            curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
         break;
      default:
         curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    }    

    $headers = array();
    $headers[] = 'Content-Type: application/json';
    //$headers[] = 'Authorization: ApiKey '.$array['username'].':'.$array['api_key'];
    $headers[] = 'authorization: Bearer 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';
    //$headers[] = 'Api-Key: ' . $array['api_key'];
    // if(!empty($array['username'])){
    //     $headers[] = 'Authorization: ' . $array['username'];
    // }

    
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $result = curl_exec($ch);
    
    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    }
    curl_close($ch);

    if($content_type == 'array'){
        $result = json_decode($result); 
    }  
    return $result;
} 

function sendgridEmail($array){

    $args['url'] = 'https://api.sendgrid.com/v3/mail/send';
    $data = '{
           "from":{
              "email":"noreply@guestpostengine.com"
           },
           "personalizations":[
                {
                     "to":[
                        {
                           "email":"'.$array['email'].'",
                           
                        }
                     ],
                     "subject": "'.$array['subject'].'",
                     "dynamic_template_data": '.$array['data'].'
                  },
           ],
           
           "template_id":"'.$array['template_id'].'"
        }';
    $args['data'] = $data;
    $result = $this->curlAccessEmail('POST',$args);

    return $result;
}
function getAdmminList(){
    $list_admin = \App\User::where('user_type','=','admin')->get()->toArray();
    return $list_admin;
}

// Refunt Paypal sent to buyer
function transferToSeller($capture_id = 0,  $payment_id, $AddToCartId, $message_id =1 ,$amount=0){
    
    if( $capture_id > 0 ) {
        /** curl api to generate token **/
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, env('PAYPAL_DOMAIN').'/v1/oauth2/token');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=client_credentials");
        curl_setopt($ch, CURLOPT_USERPWD, env('PAYPAL_key') . ':' . env('PAYPAL_Secret'));

        $headers = array();
        $headers[] = 'Accept: application/json';
        $headers[] = 'Accept-Language: en_US';
        $headers[] = 'Content-Type: application/x-www-form-urlencoded';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        $response = json_decode($result, true);
       // echo 'Token :'.$response['access_token'];
        /**  end here code for token generate **/
        /**  curl api for refund the transaction **/
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, env('PAYPAL_DOMAIN').'/v1/payments/sale/'.$capture_id.'/refund');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);

        ///$post_val = '{"amount": {  "value": "'. $amount.'","currency_code": "USD"  }}';
        curl_setopt($ch, CURLOPT_POSTFIELDS, "{\n  \"amount\": {\n    \"total\": ".$amount.",\n    \"currency\": \"USD\"\n  }}");
   

        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: Bearer '.$response['access_token'];
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        $response = json_decode($result, true);
        curl_close($ch);
        if(isset($response['state'])){

          if($response['state'] == 'completed'){
              $refund_id = $response['id'];
              $created = $response['create_time'];
              $amount = $response['amount']['total'];
              $currency = 'usd';
              $status = $response['state'] ;
              $payment_type = 'paypal';
              $txn_fee_refund = $response['refund_from_transaction_fee']['value'];
              $sale_id = $response['sale_id'];
              $refunded_amount = $response['refund_from_received_amount']['value'];            

               $refund = \App\Refund::create([
                      'object' => 'refund',
                      'amount' => $refunded_amount,
                      'sale_id' => $sale_id,
                      'txn_fee_refund' => $txn_fee_refund,
                      'currency' => $currency,
                      'status' => $status,
                      'type' => 'paypal',
                      'total_amount' => $amount,
                      'create_paypal_date' => $created,
                      'payment_type' => $payment_type,
                      'refund_stripe_id' => $refund_id,
                      'message_id' => $message_id
                  ]);


                /* flock notification */

                  $notiMessage = '<strong>' . env('APP_NAME') . '</strong> : Payment has been refunded ( Cost : $'.@$refunded_amount.' Txn Amount : $'.@$txn_fee_refund.' - paypal payment)';
                  flockNotification($notiMessage);

               /* flock notification */

                  $refund_id = $refund->id;
                  \App\AddToCart::where('id',$AddToCartId)->update(['refund_id' => $refund_id]);
              
              $response = ['status' => true, 'data' => $response,'message' => 'Refund has been done.'];
              /** end here code for refund the transaction **/

          }
        } else {
           $response = ['status' => false, 'data' => $response, 'message' => 'something wrong in refund.']; 
        }
        
    } else {
        $response = ['status' => false, 'data' => $response, 'message' => 'something wrong in refund.'];
    }
    return $response;
}


function manualRefund($payment_id, $AddToCartId, $message_id = 1, $amount = 0) {

    $full_refund_amount = \App\Payment::where('id',$payment_id)->value('amount');
        $refund = \App\Refund::create([
            'object' => 'refund',
            'amount' => $amount,
            'currency' => 'na',
            'status' => 'succeeded',
            'type' => 'manually',
            'create_stripe_date' => date('Y-m-d H:i:s'),
            'payment_type' => 'full',
            'refund_stripe_id' => rand(0000000,1111111),
            'message_id' => $message_id
        ]);
        $refund_id = $refund->id;
        \App\AddToCart::where('id',$AddToCartId)->update(['refund_id' => $refund_id]);
        $response = ['status'=> true, 'message' => 'Paymemt has been refunded.'];          
    return $response;
}
function refundWallet($payment_id, $AddToCartId, $message_id = 1, $amount = 0) {
      $user_id = \App\Payment::where('id',$payment_id)->value('user_id');
      $wallet = \App\Wallet::create([
          'amount' => $amount,
          'user_id' => $user_id,
          'order_id' => $AddToCartId,
          'payment_id' => $payment_id,
      ]);
      $response = ['status'=> true, 'message' => 'Payment has beed added into wallet'];          
    return $response;
}

function stripeRefund($ch_id, $payment_id, $AddToCartId, $message_id = 1, $partial_refund_amount = 0) {

    $full_refund_amount = \App\Payment::where('id',$payment_id)->where('stripe_id',$ch_id)->value('amount');
    if($partial_refund_amount <= $full_refund_amount) {
        $token = \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        if($partial_refund_amount > 0) {
            $payment_type = 'partial';
            $stripe =  \Stripe\Refund::create([
                "charge" => $ch_id,
                "amount" => $partial_refund_amount * 100,
                "metadata" => ['message_id' => $message_id]
            ]);
        } else {
            $payment_type = 'full';
            $stripe =  \Stripe\Refund::create([
                "charge" => $ch_id,
                "metadata" => ['message_id' => $message_id]
            ]);
        }

        $refund_id = $stripe->id;
        $object = $stripe->object;
        $refund_amount = $stripe->amount;
        $balance_txn = $stripe->balance_transaction;
        $ch_id = $stripe->charge;
        $created = $stripe->created;
        $currency = $stripe->currency;
        $status = $stripe->status; // succeeded

        $refunded = ( $refund_amount / 100);

        $refund = \App\Refund::create([
            'object'    => $object,
            'amount'    => $refunded,
            'balance_transaction' => $balance_txn,
            'stripe_id' => $ch_id,
            'currency' => $currency,
            'status' => $status,
            'type' => 'stripe',
            'create_stripe_date' => $created,
            'payment_type' => $payment_type,
            'refund_stripe_id' => $refund_id,
            'message_id' => $message_id
        ]);

        /* flock notification */

          $notiMessage = 'Payment has been refunded ( Cost : $'.@$refunded.' - stripe payment)';
      
          if(env('APP_ENV') == 'local' || env('APP_ENV') == 'stage' ){
              $notiMessage = '<strong>' . env('APP_NAME') . '</strong> : Payment has been refunded ( Cost : $'.@$refunded.' - stripe payment)';
          }        
          flockNotification($notiMessage);

        /* flock notification */


        $refund_id = $refund->id;
        \App\AddToCart::where('id',$AddToCartId)->update(['refund_id' => $refund_id]);
        $response = ['status'=> true, 'message' => 'Paymemt has been refunded.'];

      } else {
          $response = ['status'=> false, 'message' => 'It Should be less from full Amount'];
      }

    return $response;
}
 
  

function dataForRapidMoz($url = 'webdew.com'){
    $url = str_replace('/', '', $url);
    $curl = curl_init();

    curl_setopt_array($curl, array(
      //CURLOPT_URL => "https://moz-bulk-da-pa-checker.p.rapidapi.com/moz-data",
      CURLOPT_URL => "https://moz-bulk-da-pa-checker.p.rapidapi.com/stats/",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => "domains=".$url,
      CURLOPT_HTTPHEADER => array(
        "content-type: application/x-www-form-urlencoded",
        "x-rapidapi-host: moz-bulk-da-pa-checker.p.rapidapi.com",
        "x-rapidapi-key: 3bd73fd63cmsh653c45495d91b70p15aef2jsn6c6e3b4a8337",
        "X-SEBITES-KEY: iaK8dl_0Q0xuzB0JSR50gkyzxb3am7UCVdRghdN4HISZ0XlF3K"
      ),
    ));


    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      return json_decode($err, true);
    } else {
      return json_decode($response, true);
    }
}


function saveCardRefund($payment_id, $AddToCartId, $message_id = 1, $amount = 0) {

    $full_refund_amount = \App\Payment::where('id',$payment_id)->value('amount');
        $refund = \App\Refund::create([
            'object' => 'refund',
            'amount' => $amount,
            'currency' => 'na',
            'status' => 'succeeded',
            'type' => 'stripe-save-card',
            'create_stripe_date' => date('Y-m-d H:i:s'),
            'payment_type' => 'full',
            'refund_stripe_id' => rand(0000000,1111111),
            'message_id' => $message_id
        ]);
        $refund_id = $refund->id;
        \App\AddToCart::where('id',$AddToCartId)->update(['refund_id' => $refund_id]);
        $response = ['status'=> true, 'message' => 'Paymemt has been refunded.'];
    return $response;
}

function curlAccessPaypalToken(){

      $ch = curl_init();

      curl_setopt($ch, CURLOPT_URL, env('PAYPAL_DOMAIN').'/v1/oauth2/token');
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=client_credentials");
      curl_setopt($ch, CURLOPT_USERPWD, env('PAYPAL_key') . ':' . env('PAYPAL_Secret'));

      $headers = array();
      $headers[] = 'Accept: application/json';
      $headers[] = 'Accept-Language: en_US';
      $headers[] = 'Content-Type: application/x-www-form-urlencoded';
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

      $result = curl_exec($ch);

      $result = json_decode($result);

      if (curl_errno($ch)) {          
          echo 'Error:' . curl_error($ch);
          return false;
      }
      curl_close($ch);
      return $result;

}
