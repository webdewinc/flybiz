<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class PayToSeller extends Authenticatable
{
    protected $table = 'paytoseller';
    protected $primaryKey = 'id';

    protected $guarded = [
        
    ];

}
