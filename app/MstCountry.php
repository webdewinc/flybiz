<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class MstCountry extends Authenticatable
{
    protected $table = 'mst_country';
    protected $primaryKey = 'country_id';

    protected $guarded = [
        
    ];

    
}
