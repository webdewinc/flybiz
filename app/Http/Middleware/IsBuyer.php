<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\User;

class IsBuyer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $switched_done = User::where('user_id',Auth::user()->user_id)->update(['switched'=>'advertiser']);        
        return $next($request);
    }
}
