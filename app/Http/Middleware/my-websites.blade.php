@extends('layout.app')
@section('title', 'See website added as a Publisher - GuestPostEngine')
@section('title-description')

<meta name="description" content="Under my websites section at GuestPostEngine, you can see the catalogue of all the websites added by you at anytime with their relevant details.">
    
@endsection
@section('content')
@section('mainhead')
@parent
    <!--begin::Page Vendors Styles(used by this page) -->
    <link href="{{ asset('') . config('app.public_url') . '/assets/css/pages/wizard/wizard-2.css'  }}" rel="stylesheet" type="text/css" />
        <!--end::Page Vendors Styles -->
@show
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                
            </div>
        </div>
        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">
            @if(session()->has('message'))
            <div class="alert alert-outline-{{ session()->get('alert') }} fade show" role="alert">
                <div class="alert-icon"><i class="flaticon-warning"></i></div>
                <div class="alert-text">{{ session()->get('message') }}</div>
                <div class="alert-close">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="la la-close"></i></span>
                    </button>
                </div>
            </div>
            @endif
            
            <div class="alert alert-light alert-elevate" role="alert">
                <div class="kt-portlet__body w-100">

                    <!--begin: Search Form -->
                    <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                        <div class="row align-items-center">
                            <div class="col-xl-8 order-2 order-xl-1">
                                <div class="row align-items-center">
                                    <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                        <div class="kt-input-icon kt-input-icon--left">
                                            <input type="text" class="form-control" placeholder="Search..." id="generalSearch">
                                            <span class="kt-input-icon__icon kt-input-icon__icon--left">
                                                <span><i class="la la-search"></i></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                        <div class="kt-form__group kt-form__group--inline">
                                            <div class="kt-form__label">
                                                <label>Status:</label>
                                            </div>
                                            <div class="kt-form__control">
                                                <select class="form-control" id="kt_form_status">
                                                    <option value="">All</option>
                                                    <option value="0">Pending</option>
                                                    <option value="1">Approved</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                           
                        </div>
                    </div>

                    <!--end: Search Form -->
                </div>
                
            </div>
            <div class="kt-portlet kt-portlet--mobile">
                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect x="0" y="0" width="24" height="24"></rect>
                                    <polygon fill="#000000" opacity="0.3" points="6 7 6 15 18 15 18 7"></polygon>
                                    <path d="M11,19 L11,16 C11,15.4477153 11.4477153,15 12,15 C12.5522847,15 13,15.4477153 13,16 L13,19 L14.5,19 C14.7761424,19 15,19.2238576 15,19.5 C15,19.7761424 14.7761424,20 14.5,20 L9.5,20 C9.22385763,20 9,19.7761424 9,19.5 C9,19.2238576 9.22385763,19 9.5,19 L11,19 Z" fill="#000000" opacity="0.3"></path>
                                    <path d="M6,7 L6,15 L18,15 L18,7 L6,7 Z M6,5 L18,5 C19.1045695,5 20,5.8954305 20,7 L20,15 C20,16.1045695 19.1045695,17 18,17 L6,17 C4.8954305,17 4,16.1045695 4,15 L4,7 C4,5.8954305 4.8954305,5 6,5 Z" fill="#000000" fill-rule="nonzero"></path>
                                </g>
                            </svg>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            My Websites
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                            
                            <div class="dropdown dropdown-inline">
                                <a href="{{url('publisher/add-website')}}" class="btn btn-label-brand btn-bold" >
                                <i class="la la-plus"></i> Add Websites
                                </a>
                                
                            </div>
                        </div>
                    </div>
                </div>                
                <div class="kt-portlet__body kt-portlet__body--fit">

                    <!--begin: Datatable -->
                    <div class="kt-datatable" id="local_data"></div>

                    <!--end: Datatable -->
                </div>
            </div>
        </div>

        <!-- end:: Content -->
        <!--begin::Modal-->
            <div class="modal fade" id="kt_modal_delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header  text-center">
                            <h5 class="modal-title" id="exampleModalLabel">Delete</h5>
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon" data-dismiss="modal" aria-label="Close" style="cursor:pointer;">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#000000">
                                        <rect x="0" y="7" width="16" height="2" rx="1"></rect>
                                        <rect opacity="0.3" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000) " x="0" y="7" width="16" height="2" rx="1"></rect>
                                    </g>
                                </g>
                            </svg>
                        </div>

                        <div class="modal-body">
                            <div class="form-group">
                                <span class="form-text text-muted">Are you sure you want to delete this item?</span>
                            </div>
                        </div>
                        <div class="modal-footer kt-login__actions">                          
                            <button type="submit" id="delete" class="btn btn-label-primary kt-mr-10 delete no">
                                {{ __('No') }}
                            </button>
                            <button type="submit" class="btn btn-primary delete yes">
                                {{ __('Yes') }}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Modal-->

        
    </div>
</div>
    @section('mainscripts')
    @parent
        <!--begin::Page Vendors Styles(used by this page) -->
            <script>
                'use strict';
                // Class definition
                var KTDatatableDataLocalDemo = function() {
                    // Private functions
                var data = '<?php echo json_encode( $domain_lists ); ?>';
                //console.log(data);
                var split_data = JSON.parse(data);
                var domain_id = split_data[0]['domain_id'];
                    // demo initializer
                    var demo = function() {

                        var dataJSONArray = JSON.parse(data);
                        var datatable = $('.kt-datatable').KTDatatable({    
                            // datasource definition
                            data: {
                                type: 'local',
                                source: dataJSONArray,
                                pageSize: 10,
                            },

                            // layout definition
                            layout: {
                                scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
                                // height: 450, // datatable's body's fixed height
                                footer: false, // display/hide footer
                            },

                            // column sorting
                            sortable: true,

                            pagination: true,

                            search: {
                                input: $('#generalSearch'),
                            },

                            // columns definition
                            columns: [
                            {
                                field: 'domain_id',
                                title: '#',
                                sortable: false,
                                width: 20,
                                type: 'number',
                                selector: {class: 'kt-checkbox--solid'},
                                textAlign: 'center',
                            },
                             {
                                field: 'domain_url',
                                title: 'Website',
                            }, {
                                field: 'cost_price',
                                title: 'Price',             
                            }, 
                            {
                                field: 'dp_da',
                                title: 'DA',
                            },
                            {
                                field: 'extra_cost',
                                title: 'Extra Cost',
                            },
                            {
                                field: 'selling_price',
                                title: 'Selling Price',
                            },  
                            {
                                field: 'orders',
                                title: 'Orders',                
                            },
                            {
                                field: 'domain_status',
                                title: 'Status',
                                //callback function support for column rendering
                                template: function(row) {
                                    var status = {
                                        0: {'title': 'Pending', 'class': 'kt-badge--warning'},
                                        1: {'title': 'Approved', 'class': ' kt-badge--brand'},
                                    };
                                    return '<span class="kt-badge ' + status[row.domain_status].class + ' kt-badge--inline kt-badge--pill">' + status[row.domain_status].title + '</span>';
                                },
                            },
                             {
                                field: 'backlink_type',
                                title: 'Backlink',
                                autoHide: false,
                                // callback function support for column rendering
                                template: function(row) {

                                    var status = {
                                        'follow': {'title': 'Do Follow', 'state': 'success'},
                                        'no_follow': {'title': 'No Follow', 'state': 'warning'},                          
                                    };
                                    return '<span class="kt-badge kt-badge--' + status[row.backlink_type].state + ' kt-badge--dot"></span>&nbsp;<span class="kt-font-bold kt-font-' + status[row.backlink_type].state +
                                        '">' +
                                        status[row.backlink_type].title + '</span>';
                                },
                            },  {
                                field: 'Actions',
                                title: 'Actions',
                                sortable: false,
                                width: 110,
                                overflow: 'visible',
                                autoHide: false,
                                template: function(row) {
                                   
                                    return '\<a href="'+row.edit_url+'/'+row.domain_id+'" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit details">\
                                        <i class="la la-edit"></i>\
                                    </a>\
                                    <a href="javascript:void(0);" class="btn btn-sm btn-clean btn-icon btn-icon-md delete" rel="'+row.domain_id+'"  title="Delete">\
                                        <i class="la la-trash"></i>\
                                    </a>\
                                ';
                                },
                            }],
                        });

                        $('#kt_form_status').on('change', function() {
                            datatable.search($(this).val().toLowerCase(), 'domain_status');
                        });

                        $('#kt_form_type').on('change', function() {
                            datatable.search($(this).val().toLowerCase(), 'Type');
                        });

                        $('#kt_form_status,#kt_form_type').selectpicker();

                    };
                    return {
                        // Public functions
                        init: function() {
                            // init dmeo
                            demo();
                        },
                    };
                }();

                jQuery(document).ready(function() {
                    KTDatatableDataLocalDemo.init();
                });

    </script>
    <script>
    $(function() {
        $(document).on('click','.delete',function(){
            $('.delete.yes').attr('rel',$(this).attr('rel'));
            $('#kt_modal_delete').modal('show');
        });
        $(document).on('click','.delete.yes',function(e){
            e.preventDefault();
            $('#kt_modal_delete').modal('hide');
            

            $.ajax({
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                url  : "{{url('/publisher/delete_website')}}",
                type: 'POST',
                data: {id : $(this).attr('rel')},                        
                async: true,
                dataType: 'json',
                enctype: 'multipart/form-data',
                cache: false,                      
                beforeSend: function() {
                    //something before send
                    $(".loader").fadeIn("slow");
                },
                success: function(data) {
                    //console.log('Data: '+data);
                    if(data.alert == 'success'){
                        $(".loader").fadeOut("slow");
                        swal.fire({
                            "title": "",
                            "text": data.message,
                            "type": "success",
                            "confirmButtonClass": "btn btn-secondary"
                        });
                        window.location.href = data.url;
                    } else {
                        $(".loader").fadeOut("slow");
                        swal.fire({
                            "title": "",
                            "text": data.message,
                            "type": "success",
                            "confirmButtonClass": "btn btn-secondary"
                        });
                    }                            
                },
                error: function(xhr,textStatus,thrownError) {
                    //alert(xhr + "\n" + textStatus + "\n" + thrownError);
                    $(".loader").fadeOut("slow");
                    $('#kt_modal_success .form-text.text-muted').html(xhr + "\n" + textStatus + "\n" + thrownError);
                    $('#kt_modal_success').modal('show');
                }
            });
            e.stopImmediatePropagation();
            return false;
        });
        $(document).on('click','.delete.no',function(){
            $('#kt_modal_delete').modal('hide');
            e.stopImmediatePropagation();
            return false;
        });
    });
    </script>
            
            <!--end::Page Vendors Styles -->
    @show
@endsection
