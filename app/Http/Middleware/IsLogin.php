<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class IsLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->switched == 'seller'){
            return redirect()->to('seller/dashboard');
        } else if(Auth::user()->switched == 'buyer'){
            return redirect()->to('buyer/dashboard');
        } 
        return $next($request);
    }
}
