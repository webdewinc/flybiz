<?php

namespace App\Http\Middleware;

use Closure;
use Request;
use Auth;
use Redirect;

class CheckIpMiddleware
{
    public $whiteIps = ['175.176.184.185','175.176.184.171','122.173.13.173'];
    //public $whiteIps = ['175.176.184.181','175.176.184.175'];
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!in_array($_SERVER['HTTP_CF_CONNECTING_IP'], $this->whiteIps)) {  
            Auth::logout();   
            return redirect()->back()->with(['message'=> 'Your are not authorized for this login.', 'alert' => 'danger','form' => 'signin']);
        }
        return $next($request);
    }
}
