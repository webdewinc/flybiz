<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->switched = Auth::user()->switched;
        if( Auth::user()->user_type != 'admin'){
            return redirect()->to(Auth::user()->switched.'/dashboard');            
        } 
        
        return $next($request);
    }
}
