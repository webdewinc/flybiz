<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Twilio\Jwt\AccessToken;
use Illuminate\Support\Facades\Validator;
use Twilio\Jwt\Grants\ChatGrant;
use Twilio\Jwt\ClientToken;
use Auth;
use App\Chatroom;
use App\Chat;
use App\User;

class TokenController extends Controller
{
    public function generate(Request $request, AccessToken $accessToken, ChatGrant $chatGrant)
    {
        
        $appName = "FlyBiz";
        $deviceId = $request->input("device");
        $identity = $request->input("identity");

        $TWILIO_CHAT_SERVICE_SID = config('services.twilio')['chatServiceSid'];

        $endpointId = $appName . ":" . $identity . ":" . $deviceId;

        $accessToken->setIdentity($identity);

        $chatGrant->setServiceSid($TWILIO_CHAT_SERVICE_SID);
        $chatGrant->setEndpointId($endpointId);

        $accessToken->addGrant($chatGrant);

        $response = array(
            'identity' => $identity,
            'token' => $accessToken->toJWT()
        );

        return response()->json($response);
    }

    /**
     * Create a new capability token
     *
     * @return \Illuminate\Http\Response
     */
    public function voiceToken(Request $request, ClientToken $clientToken)
    {
        $forPage = $request->input('forPage');
        $identity = Auth::user()->id;
        $TWILIO_TWIML_AUDIO_APP_SID = config('services.twilio')['audioApplicationSid'];
        $clientToken->allowClientOutgoing($TWILIO_TWIML_AUDIO_APP_SID);
        $clientToken->allowClientIncoming($identity);        
        $token = $clientToken->generateToken();
        return response()->json(['token' => $token]);
    }
    
}
