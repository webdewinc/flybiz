<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Twilio\Jwt\AccessToken;
use Illuminate\Support\Facades\Validator;
use Twilio\Jwt\Grants\ChatGrant;
use Auth;
use App\Chatroom;
use App\Chat;
use App\User;
use App\Role;
use Crypt;
use Redirect;
use Twilio\Rest\Client;
use View;

class ChatController extends Controller
{

    public function chat($id = '')
    {
        $user_id = Auth::user()->user_id;
        $chatChannel = Chatroom::with(['createdBy','sender','receiver','lastmessage'])->where("sender_id",Auth::user()->user_id)->orWhere("receiver_id",Auth::user()->user_id)->get();
        return view('chat.chat',compact('chatChannel'));
    }
    public function chatRenderSearchAjax(Request $request){

        $data = $request->all();        
        $search = $data['val_search'];
        $user_id = Auth::user()->user_id;
        $chatChannel = Chatroom::join('mst_user','user_id','sender_id')->where("sender_id",Auth::user()->user_id)->orWhere("receiver_id",Auth::user()->user_id)->where('user_fname','LIKE','%'.$search.'%')->get();
        
        return view('chat.chat-render-search-channel-ajax',compact('chatChannel'));
    }
    public function chatRenderSearch(Request $request){
        $data = $request->all();
        
        $search = $data['val_search'];
        $user_id = Auth::user()->user_id;
        $chatChannelList = Chatroom::with(['createdBy','sender' => function($query) use ($search){
            $query->where('user_fname', 'LIKE' , '%'.$search.'%' );
        } ,'receiver','lastmessage'])->where("sender_id",Auth::user()->user_id)->orWhere("receiver_id",Auth::user()->user_id)->get();

        return view('chat.chat-render-search-channel',compact('chatChannelList'));
    }
    public function chatRenderAll(Request $request)
    {
        $data = $request->all();
        $twilio = new Client(env('TWILIO_API_KEY'), env('TWILIO_API_SECRET'));
        $user_id = $data['authUser'];
        $id = $data['otherUser'];
        $domain_id = $data['domain_id'];
        $exist = 1;
        $userdata = User::where('user_id',$user_id)->first()->toArray();
        $otheruserdata = User::where('user_id',$id)->first()->toArray();


        // room name generate with ids of both the users
        $room_create_name =  $id.'_'.$user_id.'_'.$domain_id;
        $room_create_name_rev = $user_id.'_'.$id.'_'.$domain_id;

        $room_create_name_view =  "user".$id."user".$user_id.'domain'.$domain_id;
        $room_create_name_rev_view = "user".$user_id."user".$id.'domain'.$domain_id;
        $chatChannel = Chatroom::where("chatroom",$room_create_name)->get()->toArray();           


        if(count($chatChannel) == 0)
        {
            $chatChannel = Chatroom::where("chatroom",$room_create_name_rev)->get()->toArray();
            $exist = 0;
            if(count($chatChannel) == 0)
            {
                $room_name = $room_create_name;
                $exist = 0;
                $room_name_view = $room_create_name_view;
                $chatChannel = Chatroom::create([
                    'sender_id'=>$id,
                    'receiver_id'=>$user_id,
                    'chatroom'=>$room_name,
                    'type'=>'chat',
                    'created_by' => Auth::user()->user_id,
                    'domain_id' => $domain_id
                    ]);
                $channel_id = $chatChannel->id;
            } else {
                $room_name_view = $room_create_name_rev_view;
                $room_name = $room_create_name_rev;                
                $channel_id = $chatChannel[0]['id'];
                
            }
        } 
        else
        {
            $room_name_view = $room_create_name_view;
            $room_name = $room_create_name;    
            $channel_id = $chatChannel[0]['id'];
        }
        
        //get old messages
        $getmsg = Chat::where('chatroom_id',$channel_id)->with('chatroom')->get()->toArray();
        $update_chat = Chat::where('receiver_id',$user_id)->where('chatroom_id',$channel_id)->update([ 
                'is_viewed' => 1
            ]);

        $update_user = User::where('user_id',$user_id)->update([ 
                'chat_user' => $id
            ]);

        try {

            $twilio->chat->v2->services(env('TWILIO_CHAT_SERVICE_SID'))
                 ->channels($room_name)
                 ->members(Auth::user()->user_code)
                 ->delete();

        } catch (\Twilio\Exceptions\RestException $e) {
            //echo 'die';
        }
         $chat = view('chat.chat-render',compact('userdata','room_name','otheruserdata','room_name_view','getmsg','channel_id','domain_id'))->render();
        
        /* render sidebar also */
        $chatChannel = Chatroom::with(['createdBy','sender','receiver','lastmessage'])->where("sender_id",Auth::user()->user_id)->orWhere("receiver_id",Auth::user()->user_id)->get();
        $channel = view('chat.chat-render-search-channel-ajax',compact('chatChannel'))->render();

        $profileFull = view('layout.include.profileFull')->render();

        /* render sidebar also */
        $response = ['chat'=> $chat, 'channel' => $channel,'profileFull' => $profileFull];
        return response()->json($response);
        //return view('chat.chat-render',compact('userdata','room_name','otheruserdata','room_name_view','getmsg','channel_id'));
    }

    public function Chating($id)
    {
        $remote_id = $id;   
        $explode_ids = \Crypt::decrypt($id);
        $explode_ids = explode('_', $explode_ids);
        $domain_id = \Crypt::decrypt($explode_ids[0]);
        $other_user_id = \Crypt::decrypt($explode_ids[1]);
        $auth_user_id = \Crypt::decrypt($explode_ids[2]);
        $other_user_class_id = \Crypt::decrypt($explode_ids[1]);

        $twilio = new Client(env('TWILIO_API_KEY'), env('TWILIO_API_SECRET'));
        $user_id = $auth_user_id;
        $id = $other_user_id;
        
        
        $exist = 1;
        $userdata = User::where('user_id',$user_id)->first()->toArray();
        $otheruserdata = User::where('user_id',$id)->first()->toArray();
        // room name generate with ids of both the users
        $room_create_name =  $id.'_'.$user_id.'_'.$domain_id;
        $room_create_name_rev = $user_id.'_'.$id.'_'.$domain_id;

        $room_create_name_view =  "user".$id."user".$user_id.'domain'.$domain_id;
        $room_create_name_rev_view = "user".$user_id."user".$id.'domain'.$domain_id;
        $chatChannel = Chatroom::where("chatroom",$room_create_name)->get()->toArray();           

        if(count($chatChannel) == 0)
        {
            $chatChannel = Chatroom::where("chatroom",$room_create_name_rev)->get()->toArray();
            $exist = 0;
            if(count($chatChannel) == 0)
            {                
                $room_name = $room_create_name;

                $exist = 0;
                $room_name_view = $room_create_name_view;
                $chatChannel = Chatroom::create([
                    'sender_id'=>$id,
                    'receiver_id'=>$user_id,
                    'chatroom'=>$room_name,
                    'type'=>'chat',
                    'created_by' => Auth::user()->user_id,
                    'domain_id' => $domain_id
                    ]);
                $channel_id = $chatChannel->id;
                
            } else {
                $room_name_view = $room_create_name_rev_view;
                $room_name = $room_create_name_rev;                
                $channel_id = $chatChannel[0]['id'];
                
            }
        } 
        else
        {
            $room_name_view = $room_create_name_view;
            $room_name = $room_create_name;    
            $channel_id = $chatChannel[0]['id'];        
            
        }
        
        //get old messages
        $getmsg = Chat::where('chatroom_id',$channel_id)->with('chatroom')->get()->toArray();
        $update_chat = Chat::where('receiver_id',$user_id)->where('chatroom_id',$channel_id)->update([ 
                'is_viewed' => 1
            ]);

        $update_user = User::where('user_id',$user_id)->update([ 
                'chat_user' => $id
            ]);
        try {

             $twilio->chat->v2->services(env('TWILIO_CHAT_SERVICE_SID'))
                 ->channels($room_name)
                 ->members(Auth::user()->user_code)
                 ->delete();

        } catch (\Twilio\Exceptions\RestException $e) {
            //echo 'die';
        }

        $chatChannelList = Chatroom::with(['createdBy','sender','receiver','lastmessage'])->where("sender_id",$auth_user_id)->orWhere("receiver_id",$auth_user_id)->get();

        return view('chat.chating',compact('userdata','room_name','otheruserdata','room_name_view','getmsg','channel_id','chatChannelList','other_user_class_id','domain_id'));
    }
    public function storemsg(Request $request)
    {
        $data = $request->all();

        $time = $data['date'];
        $time = substr($time,8);
        $date = date("Y-m-d").$time;
        $date1 = date_create($date);
        $actualdate = date_format($date1,"Y-m-d H:i:s");
        
        $get = User::where('user_id',$data['receiver'])->first();
        $get = json_decode(json_encode($get),true);

        $view = 0;
        if(!empty($get['chat_user'])){
            if($get['chat_user'] == $data['sender']){
                $view = 1;
            }
        }

        $save = Chat::create([
            'sender_id' => $data['sender'], // auth 
            'receiver_id' => $data['receiver'], //user
            'reply' => $data['msg'],
            'chatroom_id' => $data['chatChannelId'],
            'twilio_channel_id' => $data['channelid'],
            'is_viewed' => $view
            //'timings'=>$actualdate,
            ]);

        
        

        // if($get['chat_user_id'] != $data['sender'])
        // {
           
        //     $new= Notification::create([
        //             'sender_id'=>$data['sender'],
        //             'receiver_id'=>$data['receiver'],
        //             'title'=>'Chat',
        //             'message'=>$data['msg'],
        //             'device_type'=>1,
        //             'appointment_id'=>$save->id,                    
        //             ]);
        // }
        
        return $save;

    }

    
}
