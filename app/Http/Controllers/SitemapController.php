<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\MstDomainUrl;
use DB;

class SitemapController extends Controller
{
    public function index()
    {
      $MstDomainUrl = MstDomainUrl::select('domain_url','created_at')->groupBy('domain_url')->where('domain_approval_status',1)->where('cost_price','>',0)->get();

      return response()->view('sitemap.website.index', [
          'MstDomainUrl' => $MstDomainUrl,
      ])->header('Content-Type', 'text/xml');
    }
    // Function to convert CSV into associative array
    function csvToArray($file, $delimiter) { 
        if (($handle = fopen($file, 'r')) !== FALSE) { 
            $i = 0; 
            while (($lineArray = fgetcsv($handle, 4000, $delimiter, '"')) !== FALSE) { 
                for ($j = 0; $j < count($lineArray); $j++) {
                    $arr[$i][$j] = $lineArray[$j]; 
                } 
                $i++; 
            } 
            fclose($handle); 
        } 
        return $arr; 
    }
    public function category()
    {

        // $searchResult = DB::table('log_searchquery')->select('query_id','query_string','query_index','query_created_date','query_results_count')->groupBy('query_string')->orderBy()->limit(100)->get()->toArray();
        //$searchResult = DB::select("SELECT COUNT(*) as t, query_string, query_created_date FROM `log_searchquery` group by query_string order By t Desc limit 100");
        // foreach( $searchResult as $key => $get_searchResult ){
           
           //    $query_string  = $get_searchResult->query_string;
           //    $updateString  = preg_replace('/\s/', '', $query_string);
           //    $updateString  = str_replace('"', '', $updateString); 
           //    $updateString  = stripslashes($updateString);
           //    $searchResult[$key]->query_string = $updateString;
        // }
        //$MstDomainUrl = MstDomainUrl::groupBy('domain_url')->where('domain_approval_status',1)->where('cost_price','>',0)->get();
        
        $feed = 'https://docs.google.com/spreadsheets/d/e/2PACX-1vQo4jW3YcFICzxVeEusxTChXWdm9DVpiVd7lx33g3x56CrXPRt3UuXo_es0oZF9OpzCmrzfebYEMF_D/pub?gid=355214615&single=true&output=csv';
        // Do it
        $data = $this->csvToArray($feed, ',');
    
        $category = [];
        $count = 0;
        foreach ($data as $key => $value) {
            if(empty(trim($value[0])) || $key == 0){
                continue;
            }
            if(!empty(trim($value[0]))){
                if(!empty($value[1])){
                    $category[$value[0]][$value[1]] = $value[1];
                }
            }
        }
        return response()->view('sitemap.category.index', [
              'MstDomainUrl' => $category,
        ])->header('Content-Type', 'text/xml');

    }
    public function top100() {
        $query = "SELECT query_string, SUM(query_index) as total_search, query_created_date ";
        $query .= "FROM log_searchquery ";
        $query .= "GROUP BY  query_string ";
        $query .= "ORDER BY total_search DESC ";
        $query .= "LIMIT 100 ";
        //GROUP BY domain_url limit $no_of_page
        $results = DB::select( DB::raw($query) );
        return response()->view('sitemap.category.top100', [
          'MstDomainUrl' => $results,
        ])->header('Content-Type', 'text/xml');
    }
}


