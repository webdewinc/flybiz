<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\APIController;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Crypt;
use DB;
use App\MstDomainUrl;
use App\AddToCart;
use App\Payment;
use App\Chatroom;
use App\Chat;
use App\User;
use App\Role;
use Redirect;
use Twilio\Rest\Client;
use View;
use App\MstDaPa;
use App\Wishlist;
use App\Wallet;
use App\Mail\DomainOwnerMail;

class AdminController extends APIController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function dashboard()
    {
      //calculate orders and earned
      $countOrdersinQueue = DB::table('add_to_carts')->where('is_billed',0)->count();
      // $getOrdersinQueue   = DB::table('add_to_carts')->join('mst_domain_url','mst_domain_url.domain_id','add_to_carts.domain_id')->select([ DB::raw("SUM(mst_domain_url.extra_cost + mst_domain_url.cost_price)  as totalAmount") ])->get()->toArray();
      $getOrdersinQueue   = 0;
      $countOrders = DB::table('add_to_carts')->where('is_billed','>',0)->count();
      // $getOrders = DB::table('add_to_carts')->join('mst_domain_url','mst_domain_url.domain_id','add_to_carts.domain_id')->where('add_to_carts.is_billed',1)->select([ DB::raw("SUM(mst_domain_url.extra_cost + mst_domain_url.cost_price)  as totalAmount") ])->get()->toArray();

        $seller = DB::table('add_to_carts')
                      ->join('mst_domain_url','mst_domain_url.domain_id','add_to_carts.domain_id')
                      ->where('add_to_carts.promo_code',0)
                      ->where('add_to_carts.content_type','seller')                      
                      ->where('add_to_carts.is_billed',1)                      
                      ->sum('mst_domain_url.cost_price');
        $seller_count = DB::table('add_to_carts')
                      ->join('mst_domain_url','mst_domain_url.domain_id','add_to_carts.domain_id')
                      ->where('add_to_carts.promo_code',0)
                      ->where('add_to_carts.content_type','seller')                      
                      ->where('add_to_carts.is_billed',1)                      
                      ->count();        
        $seller_extra = DB::table('add_to_carts')
                      ->join('mst_domain_url','mst_domain_url.domain_id','add_to_carts.domain_id')
                      ->where('add_to_carts.promo_code',0)
                      ->where('add_to_carts.content_type','seller')
                      ->where('add_to_carts.is_billed',1)                      
                      ->sum('mst_domain_url.extra_cost');
        $webdew_price = $seller_count * 10;
        
        if(!empty($seller)){
            $seller = adminDomainPriceCalculate($seller,$seller_extra,$webdew_price);
        }
        $buyer = DB::table('add_to_carts')
                      ->join('mst_domain_url','mst_domain_url.domain_id','add_to_carts.domain_id')
                      ->where('add_to_carts.is_billed',1)
                      ->where('add_to_carts.promo_code',0)
                      ->where('add_to_carts.content_type','buyer')
                      ->sum('mst_domain_url.cost_price');
        $buyer_count = DB::table('add_to_carts')
                      ->join('mst_domain_url','mst_domain_url.domain_id','add_to_carts.domain_id')
                      ->where('add_to_carts.is_billed',1)
                      ->where('add_to_carts.promo_code',0)
                      ->where('add_to_carts.content_type','buyer')
                      ->count();
        $webdew_price = $buyer_count * 10;
        
        if(!empty($buyer)){
            
            $buyer = adminDomainPriceCalculate($buyer,0,$webdew_price);
        }

        $result = (float)$seller + (float)$buyer;

        
      // return view('admin.dashboard',compact('countOrders','getOrders','countOrdersinQueue','getOrdersinQueue'));

        return view('admin.dashboard',compact('countOrders','result','countOrdersinQueue','getOrdersinQueue'));

      
    }


    public function da_pa_save(Request $request)
    {
        $data = $request->all();
        $dp_id = MstDaPa::where('dp_fk_domain',$data['domain_id'])->value('dp_id');
        if(!empty($dp_id)){
            $update = DB::table('mst_da_pa')->where('dp_id',$dp_id)->update(['dp_da' => $data['data_da'], 'dp_pa' => $data['data_pa'], 'dp_followers' => 1,'created_date' => date('Y-m-d')]);
            if($update){
                return ['status' => true,'alert'=> 'success','url' => url('admin/websites'),'message' => 'Update successfull.'];
            }
        } else {
            $insert = DB::table('mst_da_pa')->insert(['dp_da' => $data['data_da'], 'dp_pa' => $data['data_pa'], 'dp_followers' => 1, 'dp_fk_domain' => $data['domain_id'],'created_date' => date('Y-m-d') ]);
            if($insert){
                return ['status' => true,'alert'=> 'success','url' => url('admin/websites'),'message' => 'Insert successfull.'];
            }
        }
    }
    public function websites()
    {
        //get approved websites
        $adminId = Auth::user()->user_id;
        $domains = DB::table('mst_domain_url')->select('mst_domain_url.domain_id','mst_domain_url.created_at','mst_domain_url.domain_url','mst_domain_url.cost_price','mst_domain_url.domain_status','mst_domain_url.backlink_type','mst_domain_url.cost_price','mst_domain_url.extra_cost','mst_domain_url.domain_approval_status','mst_da_pa.dp_da','mst_da_pa.dp_pa','mst_domain_url.number_words','mst_domain_url.domain_fk_user_id')->where('trash',0)
        ->leftjoin('mst_da_pa','mst_domain_url.domain_id','mst_da_pa.dp_fk_domain')
        ->where('mst_domain_url.domain_fk_user_id','>',0)
        //->where('mst_da_pa.dp_pa','>', 20)
        //->where('mst_domain_url.cost_price','>', 0)
        ->orderBy('domain_id','DESC')->get()->toArray();
        

        $domain_lists = [];
        foreach( $domains as $key => $splitDomainLists ){
            
            $domain_lists[$key] = json_decode(json_encode($splitDomainLists), true);

            $domain_id = $splitDomainLists->domain_id;
            $user_id    = $splitDomainLists->domain_fk_user_id;


            $code      =   User::where('user_id',$user_id)->value('user_code');
            $domain_lists[$key]['user_code'] = '@'.$code;
             //get total orders by domain_id
            $count_orders = DB::table('add_to_carts')->where('domain_id',$domain_id)->where('is_billed','>',0)->where('mst_payment_id','>',0)->count();
            $domain_lists[$key]['orders'] = $count_orders; 

            //get selling price
            $domain_lists[$key]['cost_price']   = '$'.$splitDomainLists->cost_price;
            $domain_lists[$key]['extra_cost']   = '$'.$splitDomainLists->extra_cost;

            $domain_lists[$key]['selling_price'] = '$'.buyerDomainPriceCalculate($splitDomainLists->cost_price,$splitDomainLists->extra_cost);

            //check cost price is null or not
            if( $splitDomainLists->backlink_type == ''){
                $domain_lists[$key]['backlink_type'] = 'no_follow';
            }
            
            if(empty($domain_lists[$key]['domain_approval_status'])){
                $domain_lists[$key]['domain_approval_status'] = 3; // not accepted
            }
            if($domain_lists[$key]['domain_approval_status'] == 1) {
                $class = "kt-badge kt-badge--success kt-badge--dot";
                $url = \Crypt::encrypt($domain_id);
                $domain_lists[$key]['url']  = $url;
                //$domain_lists[$key]->domain_approval_status_url = '<a href='. $url .'>Reject</a>';
            } else {
                $class = "kt-badge kt-badge--warning kt-badge--dot";
                $url = \Crypt::encrypt($domain_id);
                $domain_lists[$key]['url'] = $url;
                //$domain_lists[$key]->domain_approval_status_url = '<a href='. $url .'>Accept</a>';
            }
        }
        return view('admin.websites',compact('domain_lists'));
    }
    public function edit_website($domain_id){
        $single_webData = DB::table('mst_domain_url')->join('mst_da_pa','domain_id','dp_fk_domain')->where('domain_id',$domain_id)->get();
        return view('/seller/edit_website',compact('single_webData'));
    }
    public function update_website(Request $request){

        $data = $request->all();
        $guest_content  =  explode(',', $data['guest_content']) ;
        $guest_content  = serialize($guest_content);
        $data['guest_content'] = $guest_content;
        
        $domain_id = $data['domain_id'];
        //update table by domain-id
        unset($data['domain_id']);
        unset($data['_token']);
        $update_exists = DB::table('mst_domain_url')->where('domain_id',$domain_id)->update($data); 
        if( $update_exists ){
             echo json_encode(array('status' => true));
        }
        else{
            echo json_encode(array('status' => false));

        }
        
        die;
    }
    public function delete_website($domain_id){

       DB::table('mst_da_pa')->where('dp_fk_domain', '=', $domain_id)->delete();
       DB::table('assigned_websites')->where('mst_domain_url_id', '=', $domain_id)->delete();
       DB::table('mst_domain_url')->where('domain_id', '=', $domain_id)->delete();

      return redirect()->back()->with(['message'=> 'Website Deleted Succesfully!!', 'alert' => 'success']);
    }
    public function users()
    {
         $user_lists = DB::table('mst_user')->select('mst_user.user_id','mst_user.user_code','mst_user.user_fname','mst_user.user_lname','mst_user.user_type','mst_user.email','mst_user.user_status','mst_user.user_created_time')->orderBy('user_id','DESC')->get();

        foreach ($user_lists as $key => $value) {
             $user_lists[$key] =  $value;
             $status = @$value->user_status;
             if(empty($status)){
                $user_lists[$key]->user_status = 2;
             }
        }

        return view('admin.users',compact('user_lists'));
    }
    public function updateUserStatus(Request $request){
        $userId         = $request['id'];
        $udpateStatus   = $request['status'];
        //update status in user lists
        $data = array(
            'user_status' => $udpateStatus,
        );
        $checkStatus = DB::table('mst_user')->where('user_id',$userId)->update($data);
        if( $checkStatus ){
            echo json_encode(array('alert' => 'success', 'message' => 'User Status Updated Successfully!!'));
        }
        else{
            echo json_encode(array('alert' => '','message' => 'Error while Update!!'));
        }

    }
    public function orders() {
      

        $orderArr  = AddToCart::with(['domainname','username','domainusername','paymentDetails.get_promocode'])->whereIn('is_billed',[1,5])->where('mst_payment_id','>',0)->get()->toArray();
       

        foreach( $orderArr as $key => $value){
            $paypal_email = '';
            if(!empty($value['domainusername']['paypal_email'])){
                $paypal_email = $value['domainusername']['paypal_email'];
            }

            $payment_status = 2;
            if($value['payment_details']['status'] == 'succeeded' || $value['payment_details']['status'] == 'COMPLETED' || $value['payment_details']['status'] == 1){
              
                if($value['is_billed'] == 1 && $value['is_accept'] < 2){
                    $payment_status = 1;
                } else {
                    $payment_status = 2;  
                }
              
            } else {
              if($value['payment_details']['status'] == 'pay_later'){
                  if($value['is_billed'] == 5){
                      $payment_status = 2;
                  } else {
                      $payment_status = 1;  
                  }
              } else {
                  $payment_status = 2;  
              }
            }
            if(empty($value['payment_details']['amount'])){
                $value['payment_details']['amount'] = '-';
            }
            //calculate how much amount paid 
            $amount = DB::table('paytoseller')->where('order_id',$value['id'])->value('amount');

            if($value['extra_cost'] > 0){
                $extra_cost = '$'.$value['extra_cost'];  
            } else {
                $extra_cost = $value['extra_cost'];  
            }
            

            if($value['content_type'] == 'seller'){
                $total_amount =  ( $value['cost_price'] + $value['extra_cost'] );
                $net_amount =  $value['domain_amount'];
                
            } else if($value['content_type'] == 'buyer'){
         
                $total_amount =   $value['cost_price'];
                $net_amount =  $value['domain_amount'];
                $extra_cost   =   0;

            } else {
                $total_amount = 0;
            }

            $promo_code = 0;
            /* webdew discount */
            if($value['payment_details']['promo_code'] > 0){
                $webdew_amount = _promo_codes($value['payment_details']['promo_code'],$net_amount);
                $promo_code = $value['payment_details']['get_promocode']['promo_code'];
            }
            /* webdew discount */



            $gross_amount = $value['after_discount_price'];
            $profit = 0;
            if($payment_status == 1){
                $sub = ( $gross_amount - $total_amount );
                $profit = $sub;
            }

            $id = $value['id'];
            $wallet = Wallet::where('order_id',$id)->value('amount');

            $encrypt_id = \Crypt::encrypt($value['id']);
            $orderLists[] = array(
              'id'                 => @$value['id'],
              'wallet'             => $wallet,
              'user_id'            => @$value['user_id'],
              'promo_code'         => @$value['promo_code'],
              'domain_id'          => @$value['domain_id'],
              'created_at'         => @$value['created_at'],
              'updated_at'         => @$value['updated_at'],
              'domain_url'         => @$value['domainname']['domain_url'],
              'a_user_fname'       => @'@'.$value['username']['user_code'],
              'a_user_lname'       => @$value['username']['user_lname'],
              'a_email'            => @$value['username']['email'],
              'p_user_fname'       => @'@'.$value['domainusername']['user_code'],
              'p_user_lname'       => @$value['domainusername']['user_lname'],
              'p_email'            => @$value['domainusername']['email'],
              'p_paypal_email'     => @$value['domainusername']['paypal_email'],
              // 'cost_price'         => @$cost_price,
              // 'extra_cost'         => @$extra_cost,
              // 'amount'             => @$value['payment_details']['amount'],
              'cost_price'         => $value['cost_price'],
              'extra_cost'         => $value['extra_cost'],
              'amount'             => $value['domain_amount'],
              'webdew_amount'      => @$webdew_amount,
              'gross' => $gross_amount,
              'promo_code'         => @$promo_code,
              'is_accept'          => @$value['is_accept'],
              'total_amount'       => @$total_amount,
              'content_type'       => @$value['content_type'],
              'status'             => @$payment_status,
              'stages'             => @$value['stages'],
              'url'             => @$value['url'],
              'title'             => @$value['title'],
              'keyword'             => @$value['keyword'],
              'paypal_email'       => @$paypal_email,// seller pay email
              'pay_to_seller_id'   => @$value['pay_to_seller_id'],
              'payer_email'        => @$value['domainusername']['email'],
              'payer_name'         => @$value['domainusername']['user_fname'],
              'paid_to_seller'     => @$amount,
              'invoiceUrl'         => url('/admin/getInvoice'),
              'encrypt_id'         => $encrypt_id, 
              'profit' => $profit
            );


        }

        if(!empty($orderArr)){  
            return view('admin.orders',compact('orderLists'));
        }
        else{
          $message = 'No Order Found in the Lists';
          return view('admin.orders',compact('message'));
        }
    }
    public function searches(){
        // if(!empty($_POST)){

        //     $start_date = $_POST['start_date'];
        //     $end_date   = $_POST['end_date'];
        //     $searchVal  = $_POST['searchVal'];

        //     //get data from database by using search keyword
        //     if(!empty($searchVal)){
        //         $searchResult = DB::table('log_searchquery')->where('query_string','LIKE','%'.$searchVal.'%')->get()->toArray();
        //     }
        //     else if(!empty( $start_date)){
        //         $searchResult = DB::table('log_searchquery')->get()->toArray();
        //     }
        //     else if(!empty( $end_date )){

        //         $searchResult = DB::table('log_searchquery')->where('query_created_date','<=','%'.$end_date.'%')->get()->toArray(); 
        //     }
        //     else if( !empty($searchVal) && !empty($start_date) ){

        //         $searchResult = DB::table('log_searchquery')->where('query_string','LIKE','%'.$searchVal.'%')->where('query_created_date','<=','%'.$start_date.'%')->get()->toArray();   
        //     }
        //     else if( !empty($searchVal) && !empty($end_date) ){

        //         $searchResult = DB::table('log_searchquery')->where('query_string','LIKE','%'.$searchVal .'%')->where('query_created_date','<=','%'.$end_date.'%')->get()->toArray();   
        //     }
        //     else if( !empty($searchVal) && !empty($start_date) && !empty($end_date) ){

        //         $searchResult = DB::table('log_searchquery')->where('query_string','LIKE','%'.$searchVal .'%')->where('query_created_date','>=','%'.$start_date.'%')->where('query_created_date','<=','%'.$end_date.'%')->get()->toArray();
        //     }
        //     else if(empty($searchVal) && !empty($start_date) && !empty($end_date)){

        //         $searchResult = DB::table('log_searchquery')->where('query_created_date','>=','%'.$start_date.'%')->where('query_created_date','<=','%'.$end_date.'%')->get()->toArray();
        //     }
        // }
        // else{

              $searchResult = DB::table('log_searchquery')->select('query_id','query_string','query_index')->get()->toArray();
            //}
              $html = '';
            foreach( $searchResult as $key => $get_searchResult ){
               
              $query_string  = $get_searchResult->query_string;
              $updateString  = preg_replace('/\s/', '', $query_string);
              $updateString  = str_replace('"', '', $updateString); 
              $updateString  = stripslashes($updateString);
              $searchResult[$key]->query_string = $updateString;
              //$html .= '<tr><td>'.$get_searchResult->query_id.'</td><td>'.$updateString.'</td><td>'.$get_searchResult->query_index.'</td></tr>';
            }

        return view('admin.searches',compact('searchResult','html'));
    }

    

    // public function searchesAjax(){
    //     // if(!empty($_POST)){

    //     //     $start_date = $_POST['start_date'];
    //     //     $end_date   = $_POST['end_date'];
    //     //     $searchVal  = $_POST['searchVal'];

    //     //     //get data from database by using search keyword
    //     //     if(!empty($searchVal)){
    //     //         $searchResult = DB::table('log_searchquery')->where('query_string','LIKE','%'.$searchVal.'%')->get()->toArray();
    //     //     }
    //     //     else if(!empty( $start_date)){
    //     //         $searchResult = DB::table('log_searchquery')->get()->toArray();
    //     //     }
    //     //     else if(!empty( $end_date )){

    //     //         $searchResult = DB::table('log_searchquery')->where('query_created_date','<=','%'.$end_date.'%')->get()->toArray(); 
    //     //     }
    //     //     else if( !empty($searchVal) && !empty($start_date) ){

    //     //         $searchResult = DB::table('log_searchquery')->where('query_string','LIKE','%'.$searchVal.'%')->where('query_created_date','<=','%'.$start_date.'%')->get()->toArray();   
    //     //     }
    //     //     else if( !empty($searchVal) && !empty($end_date) ){

    //     //         $searchResult = DB::table('log_searchquery')->where('query_string','LIKE','%'.$searchVal .'%')->where('query_created_date','<=','%'.$end_date.'%')->get()->toArray();   
    //     //     }
    //     //     else if( !empty($searchVal) && !empty($start_date) && !empty($end_date) ){

    //     //         $searchResult = DB::table('log_searchquery')->where('query_string','LIKE','%'.$searchVal .'%')->where('query_created_date','>=','%'.$start_date.'%')->where('query_created_date','<=','%'.$end_date.'%')->get()->toArray();
    //     //     }
    //     //     else if(empty($searchVal) && !empty($start_date) && !empty($end_date)){

    //     //         $searchResult = DB::table('log_searchquery')->where('query_created_date','>=','%'.$start_date.'%')->where('query_created_date','<=','%'.$end_date.'%')->get()->toArray();
    //     //     }
    //     // }
    //     // else{

    //           $searchResult = DB::table('log_searchquery')->select('query_id','query_string','query_index')->get()->toArray();
    //         //}
    //         foreach( $searchResult as $key => $get_searchResult ){
               
    //           $query_string  = $get_searchResult->query_string;
    //           $updateString  = preg_replace('/\s/', '', $query_string);
    //           $updateString  = str_replace('"', '', $updateString); 
    //           $updateString  = stripslashes($updateString);
    //           $searchResult[$key]->query_string = $updateString;
    //         }
    //     $searchResult = json_encode($searchResult);
        
    //     return $searchResult;
    //     //return view('admin.searches',compact('searchResult'));
    // }
    /**
     * Show the application Customer List.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function customer_list()
    {
        $users = User::where('type',1)->get();
        return view('admin.customer.list',compact('users'));
    }
    /**
     * Show the application Customer View.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function customer_view($id)
    {
        $id = Crypt::decrypt($id);
        $user = User::where('type',1)->where('id',$id)->first();
        $users = json_decode(json_encode($user), true);
        return view('admin.customer.view',compact('user'));
       // abort(403, 'Unauthorized action.');
    }
    public function website_accept($id){
        $id = Crypt::decrypt($id);
      
          $domain = MstDomainUrl::where('domain_id',$id)->first();
          $publisherFname = $domain->websiteuser->user_code;
          $publisheremail = $domain->websiteuser->email;
          $domain_url = $domain->domain_url;
          $domain_id = $domain->domain_id;
          $price = $domain->cost_price;
          $extra = $domain->extra_cost;

          $domain_fk_user_id = @$domain->domain_fk_user_id;

          $assigned_domain_list =  \App\AssignedWebsite::where('mst_domain_url_id',$domain_id)->where('user_id',$domain_fk_user_id)->value('id');    
          if(empty($assigned_domain_list)){
                $assigned_domain_list =  \App\AssignedWebsite::insert([
                    'mst_domain_url_id' => $domain_id,
                    'user_id' => $domain_fk_user_id,
                ]);
          }

          if( env('APP_ENV') == 'live'){
          /* flock notification */

            if($domain->domain_status == -1){
                $subject        = 'Fly Biz Website Status';
                $d_id      = \Crypt::encrypt($domain_id);
                $link           = url('/publisher/updateWebsiteStatus/'.$d_id);
                \Mail::to($domain->domain_email)->send(new DomainOwnerMail($publisherFname, $subject, $publisherFname,$link));

                $mst_domain_url = MstDomainUrl::where('domain_id',$id)->update(['domain_status'=>0]);
            }
            

            $notiMessage = @$domain_url.' has been accepted by @Admin';
            
            if(env('APP_ENV') == 'local' || env('APP_ENV') == 'stage' ){
                $notiMessage = '<strong>' . env('APP_NAME') . '</strong> : '.@$domain_url.' has been accepted by @Admin';
            }        
            flockNotification($notiMessage);

          /* flock notification */


          /* Email template */
            $args['email'] = $publisheremail;
            $args['subject'] = 'New Website Accepted by Admin';
            $args['template_id'] = 'd-6918d7a01559459c9e1afb8bf3455e12';
            $args['data'] = '{
                    "Website" : {
                          "name":"'.@$domain_url.'"
                    },
                    "Publisher":{
                          "F_name":"'.@$publisherFname.'"
                       }
                  }';

            $this->sendgridEmail($args);

          /* Email template */
         }
          /* Update domain url */

          $mst_domain_url = MstDomainUrl::where('domain_id',$id)->update(['domain_approval_status' => 1,'domain_approval_by' => Auth::user()->user_id]);

        
        return redirect()->back();
    }

    public function website_reject($id){
        $id = Crypt::decrypt($id);

        try {
          $domain = MstDomainUrl::where('domain_id',$id)->first();
          $publisherFname = $domain->websiteuser->user_fname;
          $publisheremail = $domain->websiteuser->email;
          $domain_url = $domain->domain_url;

          $domain_id = $domain->domain_id;
          $price = $domain->cost_price;
          $extra = $domain->extra_cost;

          $domain_fk_user_id = @$domain->domain_fk_user_id;

          $assigned_domain_list =  \App\AssignedWebsite::where('mst_domain_url_id',$domain_id)->where('user_id',$domain_fk_user_id)->value('id');    
          if(empty($assigned_domain_list)){
                $assigned_domain_list =  \App\AssignedWebsite::insert([
                    'mst_domain_url_id' => $domain_id,
                    'user_id' => $domain_fk_user_id,
                ]);
          }

          if( env('APP_ENV') == 'live'){
          /* flock notification */

            $notiMessage = @$domain_url.' has been rejected by @Admin';
            
            if(env('APP_ENV') == 'local' || env('APP_ENV') == 'stage' ){
                $notiMessage = '<strong>' . env('APP_NAME') . '</strong> : '.@$domain_url.' has been rejected by @Admin';
            }        
            flockNotification($notiMessage);

          /* flock notification */

          /* Email template */
            $args['email'] = $publisheremail;
            $args['subject'] = 'Website Declined by Admin';
            $args['template_id'] = 'd-477b02b986e74d30a261ed0fce114bd4';
            $args['data'] = '{
                    "Website" : {
                          "name":"'.@$domain_url.'"
                    },
                    "Publisher":{
                          "F_name":"'.@$publisherFname.'"
                       }
                  }';
            $this->sendgridEmail($args);

          /* Email template */
          }
          /* Update domain url */

            $mst_domain_url = MstDomainUrl::where('domain_id',$id)->update(['domain_approval_status' => 2,'domain_approval_by' => Auth::user()->user_id]);

          /* Update domain url */
        } catch (Exception $e) {
          
        }
        
        return redirect()->back();
    }
    // payto seller 
    public function payToSeller(){
          
        $amount         = $_POST['dataAmount'];
        $order_id       = $_POST['order_id'];

        $order = AddToCart::where('id',$order_id)->with(['paymentDetails','domainusername','username','domainname'])->first();  

          /* refunds end*/
        $payment_id = @$order->mst_payment_id;

        if(!empty($payment_id)) {

            // if( $order->promo_code == 0 ){
            //     if($order->content_type == 'buyer'){
            //         //$amount = $order->domainname->cost_price;
            //         $amount = $order->cost_price;
            //     } else {
            //         //$amount =  ( $order->domainname->cost_price + $order->domainname->extra_cost );
            //         $amount =  ( $order->cost_price + $order->extra_cost );
            //     }
            // } else {
            //     if($order->content_type == 'buyer'){
            //         $amount = 0; 
            //     } else {
            //         $amount = 0;
            //     }
            // }

            //working

            $paypalEmail    = $order->domainusername->paypal_email; // send amount to publisher.
            $payer_email    = $order->domainusername->email; // send for email notification
            $payer_name     = @$order->domainusername->user_fname;
            if(empty($paypalEmail)){
                 echo json_encode(array('success'=>'mail_empty', 'payer_email'=> $payer_email,'payer_name' => $payer_name));
            }
            else{


                $url = url('/admin/orders');
                //check condition for already paid
                $exists_inCart = DB::table('add_to_carts')
                                ->where('id',$order_id)
                                ->where('pay_to_seller_id','>',0)->get()->toArray();                        
                if(count($exists_inCart) > 0 ){

                    echo json_encode(array('success'=>'false'));

                }
                else{
                    /** pay to seller **/

                    $sender_email = env('PAYPAL_sender_email');
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, env('PAYPAL_URL'));
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, "actionType=PAY&senderEmail=".$sender_email."&cancelUrl=".$url."&currencyCode=USD&receiverList.receiver(0).email=".$paypalEmail."&receiverList.receiver(0).amount=".$amount."&requestEnvelope.errorLanguage=en_US&returnUrl=".$url);

                    $headers = array();
                    $headers[] = 'X-Paypal-Security-Userid: '.env('PAYPAL_api_email');
                    $headers[] = 'X-Paypal-Security-Password: '.env('PAYPAL_Password');
                    $headers[] = 'X-Paypal-Security-Signature: '.env('PAYPAL_Signature');
                    $headers[] = 'X-Paypal-Request-Data-Format: NV';
                    $headers[] = 'X-Paypal-Response-Data-Format: NV';
                    $headers[] = 'X-Paypal-Application-Id: '.curlAccessPaypalToken()->app_id;
                    $headers[] = 'Content-Type: application/x-www-form-urlencoded';

                    
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    $result     = curl_exec($ch);
                    $keyArray = explode("&", $result);
                    $kArray = [];
                    foreach ($keyArray as $rVal){
                    list($qKey, $qVal) = explode ("=", $rVal);
                        $kArray[$qKey] = $qVal;
                    }

                    if(!empty($kArray)){
                      
                        if($kArray['payKey']){
                        /**  details after paypal send **/
                        $timestamp                  = @$kArray['responseEnvelope.timestamp']; 
                        $ack                        = @$kArray['responseEnvelope.ack'];
                        $correlationId              = @$kArray['responseEnvelope.correlationId']; 
                        $payKey                     = @$kArray['payKey']; 
                        $paymentExecStatus          = @$kArray['paymentExecStatus']; 

                        // $transactionId              = @$kArray['paymentInfoList.paymentInfo(0).transactionId']; 
                        // $transactionStatus          = @$kArray['paymentInfoList.paymentInfo(0).transactionStatus']; 
                        // $amount                     = @$kArray['paymentInfoList.paymentInfo(0).receiver.amount']; 
                        // $to_email                   = @$kArray['paymentInfoList.paymentInfo(0).receiver.email']; 
                        // $accountId                  = @$kArray['paymentInfoList.paymentInfo(0).receiver.accountId']; 
                        // $pendingRefund              = @$kArray['paymentInfoList.paymentInfo(0).pendingRefund']; 
                        // $senderTransactionId        = @$kArray['paymentInfoList.paymentInfo(0).senderTransactionId']; 
                        // $senderTransactionStatus    = @$kArray['paymentInfoList.paymentInfo(0).senderTransactionStatus']; 
                        // $seller_accountId           = @$kArray['sender.accountId']; 


                        $transactionId              = @$kArray['paymentInfoList.paymentInfo(0).transactionId']; 
                        $transactionStatus          = @$kArray['paymentInfoList.paymentInfo(0).transactionStatus']; 
                        $to_email                   = $paypalEmail; 
                        $accountId                  = @$kArray['paymentInfoList.paymentInfo(0).receiver.accountId']; 
                        $pendingRefund              = @$kArray['paymentInfoList.paymentInfo(0).pendingRefund']; 
                        $senderTransactionId        = @$kArray['paymentInfoList.paymentInfo(0).senderTransactionId']; 
                        $senderTransactionStatus    = @$kArray['paymentInfoList.paymentInfo(0).senderTransactionStatus']; 
                        $seller_accountId           = @$kArray['sender.accountId'];

                        /** insert into paytoseller table db **/
                        $data = array(

                          'timestamp'               => $timestamp,
                          'ack'                     => $ack,
                          'correlationId'           => $correlationId,
                          'payKey'                  => $payKey,
                          'paymentExecStatus'       => $paymentExecStatus,
                          'transactionId'           => $transactionId,
                          'transactionStatus'       => $transactionStatus,
                          'amount'                  => $amount,
                          'to_email'                => $to_email,
                          'accountId'               => $accountId,
                          'pendingRefund'           => $pendingRefund,
                          'senderTransactionId'     => $senderTransactionId,
                          'senderTransactionStatus' => $senderTransactionStatus,
                          'seller_accountId'        => $seller_accountId,
                          'order_id'                => $order_id,
                        );
                        $insertPayment = DB::table('paytoseller')->insertGetId($data);

                    if( env('APP_ENV') == 'live'){    
                        /* flock notification */
                          $code_user     = @$order->domainusername->user_code;
                          $notiMessage = 'Payment has been transffered to @'.$code_user;
                          if(env('APP_ENV') == 'local' || env('APP_ENV') == 'stage' ){
                              $notiMessage = '<strong>' . env('APP_NAME') . '</strong> : '.$notiMessage;
                          }        
                          flockNotification($notiMessage);

                        /* flock notification */
                    }
                        if($insertPayment > 0 ){
                            //print the response to screen for testing purposes
                          $data = array('pay_to_seller_id' => $insertPayment);
                          DB::table('add_to_carts')->where('id',$order_id)->update($data);
                          $message = $kArray['responseEnvelope.ack']; 
                          echo json_encode(array('success'=>'true'));
                        }
                      } else {
                         echo json_encode(array('success'=>'false','message' => 'Key not exist'));
                      }
                    } else {
                        echo json_encode(array('success'=>'false'));
                    }
                }
            }
        }
      die;

    }

   
    public function payToSellerPayouts(){
          
        $amount         = $_POST['dataAmount'];
        $order_id       = $_POST['order_id'];

        $order = AddToCart::where('id',$order_id)->with(['paymentDetails','domainusername','username','domainname'])->first();  

          /* refunds end*/
        $payment_id = @$order->mst_payment_id;

        if(!empty($payment_id)) {

            // if( $order->promo_code == 0 ){
            //     if($order->content_type == 'buyer'){
            //         $amount = $order->domainname->cost_price;
            //     } else {
            //         $amount =  ( $order->domainname->cost_price + $order->domainname->extra_cost );
            //     }
            // } else {
            //     if($order->content_type == 'seller'){
            //         $amount = 0; 
            //     } else {
            //         $amount = 0;
            //     }
            // }
            //working

            $paypalEmail    = $order->domainusername->paypal_email; // send amount to publisher.
            $payer_email    = $order->domainusername->email; // send for email notification
            $payer_name     = @$order->domainusername->user_fname;
            if(empty($paypalEmail)){
                 echo json_encode(array('success'=>'mail_empty', 'payer_email'=> $payer_email,'payer_name' => $payer_name));
            } else {

                $url = url('/admin/orders');
                //check condition for already paid
                $exists_inCart = DB::table('add_to_carts')
                                ->where('id',$order_id)
                                ->where('pay_to_seller_id','>',0)
                                ->get()
                                ->toArray();                        
                if(count($exists_inCart) > 0 ) {
                    echo json_encode(array('success'=>'false'));
                }
                else{
                    /** pay to seller **/
                    $sender_email = env('PAYPAL_sender_email');
                    $token = curlAccessPaypalToken()->access_token;

                    $data = '{
                          "sender_batch_header":{
                              "sender_batch_id":'.time().',
                              "email_subject":"You have a Payout!",
                              "email_message": "You have received a payout! Thanks for using our service!"
                          },
                          "items":[
                              {
                                  "recipient_type":"EMAIL",
                                  "amount":{
                                      "value":"'.$amount.'",
                                      "currency":"USD"
                                  },
                                  "note":"Thanks for your payment!",
                                  "sender_item_id":"2014031400023",
                                  "receiver":"'.$paypalEmail.'"
                              }
                          ]
                      }';
                      
                      $ch = curl_init();
                      curl_setopt($ch, CURLOPT_URL, env('PAYPAL_DOMAIN').'/v1/payments/payouts');
                      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                      curl_setopt($ch, CURLOPT_POST, 1);
                      curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

                      $headers = array();
                      $headers[] = 'Content-Type: application/json';
                      $headers[] = 'Authorization: Bearer '.$token;

                      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                      $result = curl_exec($ch);
                      $result = json_decode($result);

                     
                      if (curl_errno($ch)) {
                          echo json_encode(array('success'=>'false','message' => 'Error:' . curl_error($ch)));
                          die;
                      }
                      curl_close($ch);
                      
                      $payout_batch_id = @$result->batch_header->payout_batch_id;
                      if(!empty($payout_batch_id)){
                          $status = @$result->batch_header->batch_status;
                          $sender_batch_id = @$result->batch_header->sender_batch_header->sender_batch_id;

                          $data = array(

                            'timestamp'               => date('Y-m-d'),
                            'ack'                     => 'Success',
                            'correlationId'           => 'na',
                            'payKey'                  => $payout_batch_id,
                            'paymentExecStatus'       => $status,
                            'transactionId'           => $payout_batch_id,
                            'transactionStatus'       => 'paypal_payouts',
                            'amount'                  => $amount,
                            'to_email'                => $payer_email,
                            'accountId'               => 'na',
                            'pendingRefund'           => false,
                            'senderTransactionId'     => 'na',
                            'senderTransactionStatus' => 'COMPLETED',
                            'seller_accountId'        => 'na',
                            'order_id'                => $order_id,
                          );
                          $insertPayment = DB::table('paytoseller')->insertGetId($data);

                          if( env('APP_ENV') == 'live'){    
                              /* flock notification */
                                $code_user     = @$order->domainusername->user_code;
                                $notiMessage = 'Payment has been transffered to @'.$code_user;
                                if(env('APP_ENV') == 'local' || env('APP_ENV') == 'stage' ){
                                    $notiMessage = '<strong>' . env('APP_NAME') . '</strong> : '.$notiMessage;
                                }        
                                flockNotification($notiMessage);
                              /* flock notification */
                          }
                          if($insertPayment > 0 ){
                              //print the response to screen for testing purposes
                              $data = array('pay_to_seller_id' => $insertPayment);
                              DB::table('add_to_carts')->where('id',$order_id)->update($data);
                              echo json_encode(array('success'=>'true'));
                          }
                      } else {
                          echo json_encode(array('success'=>'false'));
                      }
                }
            }
        }
      die;

    }
    public function payToSellerManual(){
          
        $amount         = $_POST['dataAmount'];
        $order_id       = $_POST['order_id'];

        $order = AddToCart::where('id',$order_id)->with(['paymentDetails','domainusername','username','domainname'])->first();  

          /* refunds end*/
        $payment_id = @$order->mst_payment_id;

        if(!empty($payment_id)) {

            // if( $order->promo_code == 0 ){
            //     if($order->content_type == 'buyer'){
            //         $amount = $order->domainname->cost_price;
            //     } else {
            //         $amount =  ( $order->domainname->cost_price + $order->domainname->extra_cost );
            //     }
            // } else {
            //     if($order->content_type == 'seller'){
            //         $amount = 0; 
            //     } else {
            //         $amount = 0;
            //     }
            // }
            //working

            $paypalEmail    = $order->domainusername->paypal_email; // send amount to publisher.
            $payer_email    = $order->domainusername->email; // send for email notification
            $payer_name     = @$order->domainusername->user_fname;
            if(empty($paypalEmail)){
                 echo json_encode(array('success'=>'mail_empty', 'payer_email'=> $payer_email,'payer_name' => $payer_name));
            }
            else{


                $url = url('/admin/orders');
                //check condition for already paid
                $exists_inCart = DB::table('add_to_carts')
                                ->where('id',$order_id)
                                ->where('pay_to_seller_id','>',0)->get()->toArray();                        
                if(count($exists_inCart) > 0 ) {
                    echo json_encode(array('success'=>'false'));
                }
                else{
                    /** pay to seller **/

                        $data = array(

                          'timestamp'               => date('Y-m-d'),
                          'ack'                     => 'Success',
                          'correlationId'           => 'na',
                          'payKey'                  => 'na',
                          'paymentExecStatus'       => 'COMPLETED',
                          'transactionId'           => 'na',
                          'transactionStatus'       => 'direct_paypal',
                          'amount'                  => $amount,
                          'to_email'                => $payer_email,
                          'accountId'               => 'na',
                          'pendingRefund'           => false,
                          'senderTransactionId'     => 'na',
                          'senderTransactionStatus' => 'COMPLETED',
                          'seller_accountId'        => 'na',
                          'order_id'                => $order_id,
                        );
                        $insertPayment = DB::table('paytoseller')->insertGetId($data);

                        if( env('APP_ENV') == 'live'){    
                            /* flock notification */
                              $code_user     = @$order->domainusername->user_code;
                              $notiMessage = 'Payment has been transffered to @'.$code_user;
                              if(env('APP_ENV') == 'local' || env('APP_ENV') == 'stage' ){
                                  $notiMessage = '<strong>' . env('APP_NAME') . '</strong> : '.$notiMessage;
                              }        
                              flockNotification($notiMessage);
                            /* flock notification */
                        }
                        if($insertPayment > 0 ){
                            //print the response to screen for testing purposes
                            $data = array('pay_to_seller_id' => $insertPayment);
                            DB::table('add_to_carts')->where('id',$order_id)->update($data);
                            echo json_encode(array('success'=>'true'));
                        }
                }
            }
        }
      die;

    }

    public function add_paypalEmail(){
        $publisheremail = $_POST['payer_email'];
        $publisherName  = $_POST['payer_name'];
        if( env('APP_ENV') == 'live'){
          //send email
          $args['email'] = $publisheremail;
          $args['subject'] = 'Add Paypal Email to FlyBiz Account';
          $args['template_id'] = 'd-18e65374fe384caab67fb19cad632c38';
          $args['data'] = '{
              "Seller" : {
                    "name":"'.$publisherName.'"
              }
            }';
        
          $this->sendgridEmail($args);
        }
        echo json_encode(array('success'=> 'true'));
        die();
    }
    public function getInvoice($order_id){
       $order_id = \Crypt::decrypt($order_id);
        //get results by order-id
        // $invoiceDetails = AddToCart::with(['domainname','username','domainusername','paymentDetails','payToSeller'])->where('is_billed',1)->where('mst_payment_id','>',0)->where('id',$order_id)->get()->toArray();
       $invoiceDetails = AddToCart::with(['domainname','username','domainusername','paymentDetails','payToSeller'])->where('id',$order_id)->get()->toArray();
        foreach( $invoiceDetails as $split_invoice_details ){
            //invoice for paytoseller
            $invoiceData = array(
              'domain_url'              => $split_invoice_details['fk_domain_url'],
              'payment_type'            => 'paypal',
              'merchant_id'             => $split_invoice_details['pay_to_seller']['senderTransactionId'],
              'publisher_paypal_email'  => $split_invoice_details['domainusername']['paypal_email'],
              'amount'                  => $split_invoice_details['pay_to_seller']['amount'],
              'created_at'              => $split_invoice_details['pay_to_seller']['timestamp'],
              'publisher_fname'         => $split_invoice_details['domainusername']['user_fname'],
              'publisher_lname'         => $split_invoice_details['domainusername']['user_lname'],
              'order_id'                => $order_id,
              'invoice_status'          => $split_invoice_details['pay_to_seller']['ack'],
              'correlationId'           => $split_invoice_details['pay_to_seller']['correlationId'],
              'accountId'               => $split_invoice_details['pay_to_seller']['accountId'],
              'senderTransactionStatus' => $split_invoice_details['pay_to_seller']['senderTransactionStatus'],
              'seller_accountId'        => $split_invoice_details['pay_to_seller']['seller_accountId'],
              'order_status'            => $split_invoice_details['is_accept'],
            );
            //invoice for order
            $orderData  = array(
                'domain_url'              => $split_invoice_details['fk_domain_url'],
                'payment_type'          => $split_invoice_details['payment_details']['payment_type'],
                'currency'              => $split_invoice_details['payment_details']['currency'],
                'paypal_email'          => $split_invoice_details['payment_details']['paypal_email'],
                'paypal_merchant_id'    => $split_invoice_details['payment_details']['paypal_merchant_id'],
                'paypal_status'         => $split_invoice_details['payment_details']['status'],
                'stripe_merchant_id'    => $split_invoice_details['payment_details']['stripe_id'],
                'stripe_status'         => $split_invoice_details['payment_details']['status'],
                'paypal_address'        => $split_invoice_details['payment_details']['paypal_address'],
                'merchant_id'           => $split_invoice_details['payment_details']['merchant_id'],
                'amount'                => $split_invoice_details['payment_details']['amount'],
                'advertiser_fname'      => '@'.$split_invoice_details['username']['user_code'],
                'advertiser_lname'      => $split_invoice_details['username']['user_lname'],
                'created_at'            => $split_invoice_details['payment_details']['created_at'],
                'order_status'          => $split_invoice_details['is_accept'],
            );

            $invoicearr[] = array(
                'domain_url'              => $split_invoice_details['fk_domain_url'],
                'payment_type'          => $split_invoice_details['payment_details']['payment_type'],
                'currency'              => $split_invoice_details['payment_details']['currency'],
                'paypal_email'          => $split_invoice_details['payment_details']['paypal_email'],
                'paypal_merchant_id'    => $split_invoice_details['payment_details']['paypal_merchant_id'],
                'paypal_status'         => $split_invoice_details['payment_details']['status'],
                'stripe_merchant_id'    => $split_invoice_details['payment_details']['stripe_id'],
                'stripe_status'         => $split_invoice_details['payment_details']['status'],
                'paypal_address'        => $split_invoice_details['payment_details']['paypal_address'],
                'merchant_id'           => $split_invoice_details['payment_details']['merchant_id'],
                'amount'                => $split_invoice_details['payment_details']['amount'],
                'advertiser_fname'      => '@'.$split_invoice_details['username']['user_code'],
                'advertiser_lname'      => $split_invoice_details['username']['user_lname'],
                'created_at'            => $split_invoice_details['payment_details']['created_at'],
                'order_status'          => $split_invoice_details['is_accept'],
            );

        }
        return view('/admin/invoice',compact('invoiceData','orderData','invoicearr'));
    }

    public function getchat(){
        
        $chatChannel = Chatroom::with(['createdBy','sender','receiver','lastmessage'])->get();
        return view('admin.chat.chat',compact('chatChannel'));
    }
    public function chatRenderAll(Request $request)
    {
        $data = $request->all();
        $twilio = new Client(env('TWILIO_API_KEY'), env('TWILIO_API_SECRET'));
        $user_id = $data['authUser'];
        $id = $data['otherUser'];
        $domain_id = $data['domain_id'];
        $exist = 1;
        $userdata = User::where('user_id',$user_id)->first()->toArray();
        $otheruserdata = User::where('user_id',$id)->first()->toArray();


        // room name generate with ids of both the users
        $room_create_name =  $id.'_'.$user_id.'_'.$domain_id;
        $room_create_name_rev = $user_id.'_'.$id.'_'.$domain_id;

        $room_create_name_view =  "user".$id."user".$user_id.'domain'.$domain_id;
        $room_create_name_rev_view = "user".$user_id."user".$id.'domain'.$domain_id;
        $chatChannel = Chatroom::where("chatroom",$room_create_name)->get()->toArray();           


        if(count($chatChannel) == 0)
        {
            $chatChannel = Chatroom::where("chatroom",$room_create_name_rev)->get()->toArray();
            $exist = 0;
            if(count($chatChannel) == 0)
            {
                $room_name = $room_create_name;
                $exist = 0;
                $room_name_view = $room_create_name_view;
                $chatChannel = Chatroom::create([
                    'sender_id'=>$id,
                    'receiver_id'=>$user_id,
                    'chatroom'=>$room_name,
                    'type'=>'chat',
                    'created_by' => Auth::user()->user_id,
                    'domain_id' => $domain_id
                    ]);
                $channel_id = $chatChannel->id;
            } else {
                $room_name_view = $room_create_name_rev_view;
                $room_name = $room_create_name_rev;                
                $channel_id = $chatChannel[0]['id'];
                
            }
        } 
        else
        {
            $room_name_view = $room_create_name_view;
            $room_name = $room_create_name;    
            $channel_id = $chatChannel[0]['id'];
        }
        
        //get old messages
        $getmsg = Chat::where('chatroom_id',$channel_id)->with('chatroom')->get()->toArray();
        

         $chat = view('admin.chat.chat-render',compact('userdata','room_name','otheruserdata','room_name_view','getmsg','channel_id','domain_id'))->render();

        /* render sidebar also */
        $response = ['chat'=> $chat];
        return response()->json($response);
        //return view('chat.chat-render',compact('userdata','room_name','otheruserdata','room_name_view','getmsg','channel_id'));
    }

    // public function order_reject(Request $request) {

    //       $id = $_POST['order_id'];

    //       $order = AddToCart::where('id',$id)->with(['paymentDetails','domainusername','username','domainname'])->first();
    //       /* refunds end*/
    //       $payment_id = @$order->mst_payment_id;

    //       if(!empty($payment_id)) {

    //         if( $order->promo_code == 0 ){
    //             if($order->content_type == 'buyer'){
    //                 $amount = buyerDomainWebsitePriceCalculate($order->cost_price);
    //                 //$amount = buyerDomainWebsitePriceCalculate($order->domainname->cost_price);
    //             } else {
    //                 $amount = buyerDomainPriceCalculate($order->cost_price, $order->extra_cost);
    //                 //$amount = buyerDomainPriceCalculate($order->domainname->cost_price, $order->domainname->extra_cost);
    //             }
    //         } else {
    //             if($order->content_type == 'buyer'){
    //                 $amount = 0; 
    //             } else {
    //                 $amount = 0;
    //             }
    //         }

    //         if( $payment_id > 0 ){
    //             $payments = Payment::where('id',$payment_id)->first();

    //             if($payments->payment_type == 'stripe') { /* stripe refunds */  
    //                 $ch_id = $payments->stripe_id;
    //                 if(!empty($ch_id) && !empty($payment_id)){
    //                     $stripe_val = stripeRefund($ch_id,$payment_id,$order->id,0,$amount);
    //                 }
    //             } else if($payments->payment_type == 'paypal'){ // paypal refund
    //                 if(!empty($order->paymentDetails->capture_id)){
    //                     $return_val = transferToSeller($order->paymentDetails->capture_id,$payment_id,$order->id,0,$amount);
    //                 }
    //             } else {
    //                 // manual payment
    //                 $return_val = manualRefund($payment_id,$order->id,0,$amount);
    //             }
    //         }
    //         /* refunds end*/  

    //         $advertiserFname = $order->username->user_fname;
    //         $advertiseremail = $order->username->email;

            
    //         $publisherFname = @Auth::user()->user_fname;
    //         //$publisherFname = $order->domainusername->user_fname;        
    //         $domain_url = $order->domainname->domain_url;
    //     if( env('APP_ENV') == 'live'){  
    //         /* Email template */
    //         $args['email'] =  $advertiseremail;
    //        // $args['email'] =  'randeep.s@webdew.com';
    //         $args['subject'] = 'Your Order Rejected';
    //         $args['template_id'] = 'd-687a4ea36e014d819bdc1e9a4a88b3cb';
    //         $args['data'] = '{
    //               "Website" : {
    //                     "name":"'.@$domain_url.'"
    //               },
    //               "Publisher":{
    //                     "F_name":"'.@$publisherFname.'"
    //                  },
    //               "Advertiser":{
    //                     "F_name":"'.@$advertiserFname.'"
    //               }, 
    //             }';

    //         $this->sendgridEmail($args);


    //         $adminUsers = getAdmminList();
    //         foreach($adminUsers as $admin_users){
    //             $args['email']    = $admin_users['email'];
    //            // $args['email'] =  'randeep.s@webdew.com';
    //             $args['subject']  = 'Placed an Order';
    //             $args['template_id'] = 'd-0b5986c2a9a7496e8364cc247b8c7387';
    //             $args['data'] = '{
    //                     "Website" : {
    //                           "name":"'.@$domain_url.'"
    //                     },
    //                     "Publisher":{
    //                           "F_name":"'.@$publisherFname.'"
    //                        },
    //                     "Advertiser":{
    //                           "F_name":"'.@$advertiserFname.'"
    //                        }
    //                   }';
    //             $this->sendgridEmail($args);
              
    //         }

    //       /* flock notification */

    //         $publishercode = @Auth::user()->user_code;        
    //         $notiMessage = 'Order has been Rejected by @'.@$publishercode;
        
    //         if(env('APP_ENV') == 'local' || env('APP_ENV') == 'stage' ){
    //             $notiMessage = '<strong>' . env('APP_NAME') . '</strong> : Order has been Rejected by @'.@$publishercode;
    //         }        
    //         flockNotification($notiMessage);

    //       /* flock notification */

    //   }
    //         /* Update domain url */
    //         $mst_domain_url = AddToCart::where('id',$id)->update(['is_accept' => 2,'accept_by' => Auth::user()->user_id]);
    //         echo json_encode(array('success'=> false));
               
    //         /* Update domain url */
    //         die();
    //       }
    // }
    // public function order_accept() {
    //       //$id = Crypt::decrypt($id);
    //       $id = $_POST['order_id'];

    //       $order = AddToCart::where('id',$id)->first();
            
    //       $advertiserFname = $order->username->user_fname;
    //       $advertiseremail = $order->username->email;

    //       $publisherFname = $order->domainusername->user_fname;        
    //       $domain_url     = $order->domainname->domain_url;

    //       if( env('APP_ENV') == 'live'){
    //       /* Email template */
    //       $args['email'] =   $advertiseremail;
    //       //$args['email'] =   'randeep.s@webdew.com';
    //       $args['subject'] = 'Your Order Approved';
    //       $args['template_id'] = 'd-db458e432efd47a4baede2b1fe3af5f9';
    //       $args['data'] = '{
    //               "Website" : {
    //                     "name":"'.@$domain_url.'"
    //               },
    //               "Publisher":{
    //                     "F_name":"'.@$publisherFname.'"
    //                  }
    //             }';
    //       $this->sendgridEmail($args);
    //       /* Email template */

    //       $adminUsers = getAdmminList();
    //       foreach($adminUsers as $admin_users){
    //             $args['email']    = $admin_users['email'];
    //             //$args['email'] =   'randeep.s@webdew.com';
    //             $args['subject']  = 'Placed an Order';
    //             $args['template_id'] = 'd-f2fbc21b513f448c9ad2db15127832d1';
    //             $args['data'] = '{
    //                     "Website" : {
    //                           "name":"'.@$domain_url.'"
    //                     },
    //                     "Publisher":{
    //                           "F_name":"'.@$publisherFname.'"
    //                        },
    //                     "Advertiser":{
    //                           "F_name":"'.@$advertiserFname.'"
    //                        }
    //                   }';
    //             $this->sendgridEmail($args);
    //          // }               
    //       }

    //       /* flock notification */

    //         //$publishercode = @$order->domainusername->user_code;        
    //         $publishercode = @Auth::user()->user_code;        
    //         $notiMessage = 'Order has been Accepted by @'.@$publishercode;
        
    //         if(env('APP_ENV') == 'local' || env('APP_ENV') == 'stage' ){
    //             $notiMessage = '<strong>' . env('APP_NAME') . '</strong> : Order has been Accepted by @'.@$publishercode;
    //         }        
    //         flockNotification($notiMessage);
    //       }
    //       /* flock notification */
    //       $mst_domain_url = AddToCart::where('id',$id)->update(['is_accept' => 1,'accept_by' => Auth::user()->user_id]);
    //       echo json_encode(array('success'=> true));
    //       die();
    // }
    public function order_accept() {
          //$id = Crypt::decrypt($id);
          $id = $_POST['order_id'];
          $payment = 0;//  payment not done
          $order = AddToCart::where('id',$id)->first();
          $message = 'Order has been accepted';

          if($order->is_billed == 5 && $order->paymentDetails->type == 'stripe-save-card'){

                if(!empty($order->paymentDetails->stripe_customer_id) && !empty($order->paymentDetails->stripe_payment_method_id) && $order->after_discount_price > 0){
                    try {

                        $stripeToken = Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
                        $paymen_intent = \Stripe\PaymentIntent::create([
                          'amount' => $order->after_discount_price * 100,
                          'currency' => 'usd',
                          'customer' => secureDecrypt($order->paymentDetails->stripe_customer_id),
                          'payment_method' => secureDecrypt($order->paymentDetails->stripe_payment_method_id),
                          'off_session' => true,
                          'confirm' => true,
                        ]);
                        
                        $updateCart = AddToCart::where('id',$id)->update(['is_accept' => 1,'is_billed' => 1,'accept_by' => Auth::user()->user_id]);

                        Payment::where('id',$order->mst_payment_id)->update(['status' => 1]);
                        $message = 'Order has been accepted and Payment also debited to Advertiser - COST $'.$order->after_discount_price;
                        $payment = 1;//done payment
                        
                    } catch (\Stripe\Exception\CardException $e) {
                        // Error code will be authentication_required if authentication is needed
                        //echo 'Error code is:' . $e->getError()->code;
                        //$payment_intent_id = $e->getError()->payment_intent->id;
                        //$payment_intent = \Stripe\PaymentIntent::retrieve($payment_intent_id);                        
                        echo json_encode(array('success'=> false,'message' => $e->getError()->code));
                        die();
                    }

                } else {
                    echo json_encode(array('success'=> false,'message' => 'Payment has not been done. Card not updated.'));
                    die();
                    
                }

          } else {
              $mst_domain_url = AddToCart::where('id',$id)->update(['is_accept' => 1,'accept_by' => Auth::user()->user_id]);
              $payment = 2; // payment already done.
          }
            
          $advertiserFname = $order->username->user_fname;
          $advertiseremail = $order->username->email;
          $advertisercode = @$order->username->user_code;

          $publisherFname = $order->domainusername->user_fname;
          $publishercode = @$order->domainusername->user_code;
          $domain_url     = $order->domainname->domain_url;

          if( env('APP_ENV') == 'live'){

              /* Email template */
              $args['email'] =   $advertiseremail;
              //$args['email'] =   'randeep.s@webdew.com';
              $args['subject'] = 'Your Order has been Approved.';
              $args['template_id'] = 'd-db458e432efd47a4baede2b1fe3af5f9';
              $args['data'] = '{
                      "Website" : {
                            "name":"'.@$domain_url.'"
                      },
                      "Publisher":{
                            "F_name":"@'.@$publishercode.'"
                         }
                    }';
              $this->sendgridEmail($args);
              /* Email template */

              $adminUsers = getAdmminList();
              foreach($adminUsers as $admin_users){
                    $args['email']    = $admin_users['email'];
                    //$args['email'] =   'randeep.s@webdew.com';
                    $args['subject']  = 'Placed an Order';
                    $args['template_id'] = 'd-f2fbc21b513f448c9ad2db15127832d1';
                    $args['data'] = '{
                            "Website" : {
                                  "name":"'.@$domain_url.'"
                            },
                            "Publisher":{
                                  "F_name":"@'.@$publishercode.'"
                               },
                            "Advertiser":{
                                  "F_name":"@'.@$advertisercode.'"
                               }
                          }';
                    $this->sendgridEmail($args);
                 // }               
              }

              /* flock notification */
                $publishercode = @$order->domainusername->user_code;        
                $notiMessage = $message . ' by @'.@$publishercode;
                if(env('APP_ENV') == 'local' || env('APP_ENV') == 'stage' ){
                    $notiMessage = '<strong>' . env('APP_NAME') . '</strong> : '.$message.' by @'.@$publishercode;
                }        
                flockNotification($notiMessage);
          }
          /* flock notification */          
          echo json_encode(array('success'=> true,'payment' => $payment,'message' => $message));
          die();
    }
    public function order_reject() {

          $id = $_POST['order_id'];

          $order = AddToCart::where('id',$id)->with(['paymentDetails','domainusername','username','domainname'])->first();          
          /* refunds end*/
          $payment_id = @$order->mst_payment_id;

          if(!empty($payment_id)) {

            if( $order->promo_code == 0 ){
                if($order->content_type == 'buyer'){
                    //$amount = buyerDomainWebsitePriceCalculate($order->cost_price);
                    //$amount = buyerDomainWebsitePriceCalculate($order->domainname->cost_price);
                    //$amount = buyerDomainPriceCalculate($order->domainname->cost_price, $order->domainname->extra_cost);
                    $amount = $order->after_discount_price;
                } else {
                    //$amount = buyerDomainPriceCalculate($order->cost_price, $order->extra_cost);
                    //$amount = buyerDomainPriceCalculate($order->domainname->cost_price, $order->domainname->extra_cost);
                    //$amount = buyerDomainPriceCalculate($order->domainname->cost_price, $order->domainname->extra_cost);
                    $amount = $order->after_discount_price;
                }
            } else {
                if($order->content_type == 'buyer'){
                    $amount = 0; 
                } else {
                    $amount = 0;
                }
            }

            
            if( $payment_id > 0 ){
                $payments = Payment::where('id',$payment_id)->first();

                if($payments->payment_type == 'stripe') { /* stripe refunds */  
                    $ch_id = $payments->stripe_id;
                    if(!empty($ch_id) && !empty($payment_id)){
                        //stripeRefund($ch_id,$payment_id,$order->id,0,$amount);
                        $return_val = refundWallet($payment_id,$order->id,0,$amount);
                    }
                } else if($payments->payment_type == 'paypal'){ // paypal refund
                    if(!empty($order->paymentDetails->capture_id)){
                        $return_val = refundWallet($payment_id,$order->id,0,$amount);
                        //$return_val = transferToSeller($order->paymentDetails->capture_id,$payment_id,$order->id,0,$amount);
                    }
                } else if ($payments->payment_type == 'stripe-save-card'){
                    // Stripe save cards
                    $return_val = saveCardRefund($payment_id,$order->id,0,$amount);
                    
                } else if ($payments->payment_type == 'wallet'){
                    // Wallet payment
                    $return_val = refundWallet($payment_id,$order->id,0,$amount);
                } else {
                    // manual payment
                    $return_val = manualRefund($payment_id,$order->id,0,$amount);
                }
            }
            /* refunds end*/  

            $advertiserFname = $order->username->user_fname;
            $advertiseremail = $order->username->email;
            $advertisercode = @$order->username->user_code;

            $publisherFname = $order->domainusername->user_fname;        
            $publishercode = @$order->domainusername->user_code;
            $domain_url = $order->domainname->domain_url;
            if( env('APP_ENV') == 'live'){
                /* Email template */
                $args['email'] =  $advertiseremail;
               // $args['email'] =  'randeep.s@webdew.com';
                $args['subject'] = 'Your Order has been Rejected';
                $args['template_id'] = 'd-687a4ea36e014d819bdc1e9a4a88b3cb';
                $args['data'] = '{
                      "Website" : {
                            "name":"'.@$domain_url.'"
                      },
                      "Publisher":{
                            "F_name":"@'.@$publishercode.'"
                         },
                        "Advertiser":{
                            "F_name":"@'.@$advertisercode.'"
                         }, 
                    }';
                /* Email template */
                $this->sendgridEmail($args);


                $adminUsers = getAdmminList();
                foreach($adminUsers as $admin_users){
                    $args['email']    = $admin_users['email'];
                   // $args['email'] =  'randeep.s@webdew.com';
                    $args['subject']  = 'Order has been rejected';
                    $args['template_id'] = 'd-0b5986c2a9a7496e8364cc247b8c7387';
                    $args['data'] = '{
                            "Website" : {
                                  "name":"'.@$domain_url.'"
                            },
                            "Publisher":{
                                  "F_name":"@'.@$publishercode.'"
                               },
                            "Advertiser":{
                                  "F_name":"@'.@$advertisercode.'"
                               }
                          }';
                    $this->sendgridEmail($args);
                  
                }


              /* flock notification */

                $publishercode = @$order->domainusername->user_code;        
                $notiMessage = 'Order has been Rejected by @'.@$publishercode;
            
                if(env('APP_ENV') == 'local' || env('APP_ENV') == 'stage' ){
                    $notiMessage = '<strong>' . env('APP_NAME') . '</strong> : Order has been Rejected by @'.@$publishercode;
                }        
                flockNotification($notiMessage);

              /* flock notification */

          }
            /* Update domain url */
            $mst_domain_url = AddToCart::where('id',$id)->update(['is_accept' => 2,'accept_by' => Auth::user()->user_id]);
            echo json_encode(array('success'=> false));
               
            /* Update domain url */
            die();
          }
    }

    public function order_cancel() {

          $id = $_POST['order_id'];

          $order = AddToCart::where('id',$id)->with(['paymentDetails','domainusername','username','domainname'])->first();
          /* refunds end*/
          $payment_id = @$order->mst_payment_id;

          if(!empty($payment_id)) {
            $amount = $order->after_discount_price;
            if( $payment_id > 0 ) {
                // $order->payment_detail->
                // if($order->is_billed == 1){
                //     $return_val = refundWallet($payment_id,$order->id,0,$amount);
                // }
                $payments = Payment::where('id',$payment_id)->first();

                if($payments->payment_type == 'stripe') { /* stripe refunds */  
                    $ch_id = $payments->stripe_id;
                    if(!empty($ch_id) && !empty($payment_id)){
                        //stripeRefund($ch_id,$payment_id,$order->id,0,$amount);
                        $return_val = refundWallet($payment_id,$order->id,0,$amount);
                    }
                } else if($payments->payment_type == 'paypal'){ // paypal refund
                    if(!empty($order->paymentDetails->capture_id)){
                        $return_val = refundWallet($payment_id,$order->id,0,$amount);
                        //$return_val = transferToSeller($order->paymentDetails->capture_id,$payment_id,$order->id,0,$amount);
                    }
                } else if ($payments->payment_type == 'stripe-save-card'){
                    // Stripe save cards
                    $return_val = saveCardRefund($payment_id,$order->id,0,$amount);
                    
                } else if ($payments->payment_type == 'wallet'){
                    // Wallet payment
                    $return_val = refundWallet($payment_id,$order->id,0,$amount);
                } else {
                    // manual payment
                    $return_val = manualRefund($payment_id,$order->id,0,$amount);
                }
                
            }
            /* refunds end*/  

            $advertiserFname = $order->username->user_fname;
            $advertiseremail = $order->username->email;
            $advertisercode = @$order->username->user_code;

            $publisherFname = $order->domainusername->user_fname;        
            $publishercode = @$order->domainusername->user_code;
            $domain_url = $order->domainname->domain_url;
            if( env('APP_ENV') == 'live'){
                /* Email template */
                $args['email'] =  $advertiseremail;
                $args['subject'] = 'Your Order has been Cancelled.';
                $args['template_id'] = 'd-687a4ea36e014d819bdc1e9a4a88b3cb';
                $args['data'] = '{
                      "Website" : {
                            "name":"'.@$domain_url.'"
                      },
                      "Publisher":{
                            "F_name":"@'.@$publishercode.'"
                         },
                        "Advertiser":{
                            "F_name":"@'.@$advertisercode.'"
                         }, 
                    }';
                /* Email template */
                $this->sendgridEmail($args);


                $adminUsers = getAdmminList();
                foreach($adminUsers as $admin_users){
                    $args['email']    = $admin_users['email'];
                    $args['subject']  = 'Your Order has been Cancelled.';
                    $args['template_id'] = 'd-0b5986c2a9a7496e8364cc247b8c7387';
                    $args['data'] = '{
                            "Website" : {
                                  "name":"'.@$domain_url.'"
                            },
                            "Publisher":{
                                  "F_name":"@'.@$publishercode.'"
                               },
                            "Advertiser":{
                                  "F_name":"@'.@$advertisercode.'"
                               }
                          }';
                    $this->sendgridEmail($args);
                  
                }


              /* flock notification */

                $publishercode = @$order->domainusername->user_code;        
                $notiMessage = 'Order has been Cancelled by @'.@$publishercode;
            
                if(env('APP_ENV') == 'local' || env('APP_ENV') == 'stage' ){
                    $notiMessage = '<strong>' . env('APP_NAME') . '</strong> : Order has been Rejected by @'.@$publishercode;
                }        
                flockNotification($notiMessage);

              /* flock notification */

          }
            /* Update domain url */
            $mst_domain_url = AddToCart::where('id',$id)->update(['is_accept' => 3,'accept_by' => Auth::user()->user_id]);
            echo json_encode(array('success'=> false));
               
            /* Update domain url */
            die();
          }
    }
    public function cart_listings(){
          $data = AddToCart::where('is_billed','=',0)
                    ->where('mst_payment_id','=',0)
                    ->with(['domainname','username','domainusername','paymentDetails'])
                    ->get()
                    ->toArray();
           if(!empty($data)){
                foreach( $data as $split_data ){
                    
                    /** link for chat **/
                    $domain_id = \Crypt::encrypt($split_data['domain_id']);
                    $other_user_id = \Crypt::encrypt($split_data['user_id']);
                    $auth_user_id = \Crypt::encrypt(Auth::user()->user_id);
                    $id = \Crypt::encrypt($domain_id . '_' . $other_user_id . '_' . $auth_user_id);
                    /** end chat **/
                    //condition for seller & buyer
                    if( $split_data['content_type'] == 'buyer'){
                        $price = $split_data['domainname']['cost_price'];
                        $extra = 0;
                        $total_amount = buyerDomainWebsitePriceCalculate($split_data['domainname']['cost_price']);
                    }
                    else {
                        $price = $split_data['domainname']['cost_price'];
                        $extra = '$'.$split_data['domainname']['extra_cost'];
                        $total_amount = buyerDomainPriceCalculate($split_data['domainname']['cost_price'], $split_data['domainname']['extra_cost']);
                    }
                    $cartData[] = array(
                          'id'            => @$split_data['id'],
                          'user_id'       => @$split_data['user_id'],
                          'domain_id'     => @$split_data['domain_id'],
                          'domain_url'    => @$split_data['domainname']['domain_url'],
                          'username'      => '@'.@$split_data['username']['user_code'],
                          'mobile_no'     => @$split_data['username']['user_mob_no'],
                          'user_email'    => @$split_data['username']['email'],
                          'content_type'  => @$split_data['content_type'],
                          'publisher'     => '@'.@$split_data['domainusername']['user_code'],
                          'cost_price'    => @'$'.$price,
                          'extra_cost'    => @$extra,
                          'total'         => @'$'.$total_amount,
                          'link'          => @$id,
                    );
                }
                return view('admin.cart',compact('cartData'));
            }
            else{
              $cartMessage = 'Cart is Empty';
              return view('admin.cart',compact('cartMessage'));
            }


    }
    public function wishlist_items(){
        
        $wishlistData = Wishlist::with(['domainname','username'])->get()->toArray();

        if(!empty($wishlistData)){

            foreach( $wishlistData as $split_data ){
                $publisher_id  = $split_data['domainname']['domain_fk_user_id'];
                $publisherData = User::where('user_id',$publisher_id)->get()->toArray();

                 //condition for seller & buyer
                  
                  $price = $split_data['domainname']['cost_price'];
                  $extra = 0;
                  $total_amount = buyerDomainWebsitePriceCalculate($split_data['domainname']['cost_price']);
                  
                foreach( $publisherData as $split_publisherData )
                {
                    $wishlistArr[] = array(
                        'wishlist_id'      =>   @$split_data['wishlist_id'],
                        'domain_url'       =>   @$split_data['domain_url'],
                        'advertiser'       =>   '@'.@$split_data['username']['user_code'],
                        'advertiser_email' =>   @$split_data['username']['email'],
                        'publisher'        =>   '@'.@$split_publisherData['user_code'],
                        'cost_price'       =>   '$'.@$price,
                        'extra_cost'       =>   @$extra,
                        'total_amount'     =>   '$'.@$total_amount,
                   );
                } 
            }    
            return view('admin/wishlist_items',compact('wishlistArr'));
        }
        else{

            $wishlistMessage = 'Wishlist Empty';
            return view('admin/wishlist_items',compact('wishlistMessage'));   
        }
    }
    //bulk upload websites
    public function bulk_upload_websites(){
       return view('admin/upload_websites');
    }

    function is_valid_domain_name($domain_name) {
         return (preg_match("/^([a-zd](-*[a-zd])*)(.([a-zd](-*[a-zd])*))*$/i", $domain_name) //valid characters check
         && preg_match("/^.{1,253}$/", $domain_name) //overall length check
         && preg_match("/^[^.]{1,63}(.[^.]{1,63})*$/", $domain_name) ); //length of every label
    }
    function validateEmailDomain($email, $domains) {
        foreach ($domains as $domain) {
            $pos = strpos($email, $domain, strlen($email) - strlen($domain));

            if ($pos === false)
                continue;

            if ($pos == 0 || $email[(int) $pos - 1] == "@" || $email[(int) $pos - 1] == ".")
                return true;
        }

        return false;
    }

    public function upload_file(request $request)
    {

        $user_id = $request->user_id;
        $category = [];
        $count = 0;
        //if($request->file_type == 'link'){

            
            $feed = $request->upload_link;
            // Do it
            $data = csvToArray($feed, ',');

            $total = count($data);
            $total = $total - 1;
            foreach ($data as $key => $value) {
                if($key == 0){
                    continue;
                }
                
                if( !empty(trim($value[0])) && !empty(trim($value[3])) && !empty(trim($value[7])) && !empty(trim($value[8])) && !empty(trim($value[9])) && !empty(trim($value[6])) && !empty(trim($value[13])) && !empty(trim($value[1]))) {
                    $query = str_replace(array("'", '"','“', '”', "’",'‘', "`","@", "&quot;","https://","http://",'http://www.','https://www.','www.','WWW.'), "", htmlspecialchars($value[0]), $quotes);

                    $url = str_replace('/', '', $query);

                    //Function Call
                    if($this->is_valid_domain_name($url)){
                        
                        $whitelist = array($url); //You can add basically whatever you want here because it checks for one of these strings to be at the end of the $email string.
                        $email = $value[2];
                        if ($this->validateEmailDomain($email, $whitelist)){
                        
                            if(strtolower($value[1]) == 'yes'){
                                if(!empty(trim($value[2]))){
                                    $own_website = -1;
                                    $own_website_email = $value[2];
                                    // validate with domain
                                } else {
                                    continue;
                                }
                            } else {
                                $own_website = 0;  
                                $own_website_email = $value[2];
                            }
                        } else {
                            $own_website = 0;  
                            $own_website_email = '';
                        }
                        if(strtolower($value[3]) == 'publisher'){
                            $seller  =  array(0 => 'seller',1 => 'buyer',2 => 'hire_content');
                            $provide_content = serialize($seller);
                            if(!empty($value[4]) && !empty($value[5])){
                                $no_of_word = $value[4];
                                $content_cost = str_replace(array('$'), "", htmlspecialchars($value[5]), $quotes);
                            } else {
                                continue;
                            }
                        } else if(strtolower($value[3]) == 'advertiser'){
                            $buyer  =  array(0 => 'buyer',1 => 'seller',2 => 'hire_content');
                            $provide_content = serialize($buyer);
                            $no_of_word = @$value[4];
                            $content_cost = @$value[5];  
                        }

                        $website_amount = str_replace(array('$'), "", htmlspecialchars($value[13]), $quotes);
                        if($website_amount < 1){
                            continue;
                        }
                        
                        $id = MstDomainUrl::where('domain_url', $url)->where('domain_fk_user_id',$user_id)->where('trash',0)->value('domain_id');

                        if(!empty($id))
                        {
                            continue;
                        }

                        $category[$count]['domain_url'] = $url;
                        $category[$count]['domain_website'] = $url;
                        $category[$count]['domain_status'] = $own_website;  
                        $category[$count]['domain_email'] = $own_website_email;
                        $category[$count]['guest_content'] = $provide_content;
                        $category[$count]['number_words'] = $no_of_word;
                        $category[$count]['extra_cost'] = $content_cost;
                        $category[$count]['backlink_type'] = $value[6];
                        $category[$count]['backlinks'] = $value[7];
                        $category[$count]['max_words'] = $value[8];
                        $category[$count]['turnaround_time'] = $value[9];
                        $category[$count]['domain_guideline_url'] = $value[10];
                        $category[$count]['exampleurlOne'] = $value[11];
                        $category[$count]['exampleurlTwo'] = $value[12];
                        $category[$count]['domain_price'] = $website_amount;
                        $category[$count]['cost_price'] = $website_amount;
                        $category[$count]['domain_fk_user_id'] = $user_id;
                        $count++;
                    }

                }

            }
            $pending = $total - count($category);
            $insertion = DB::table('mst_domain_url')->insert($category); // Query Builder approach
           
            if($pending > 0){
                return ['status' => false, 'message' => 'Total websites are : ' . $total . ', Inserted website :' . count($category) . ' Pending websites :' . $pending .'. It could be existing domains or invalid.'];  
            } else {
                return ['status' => true, 'message' => 'Total websites are ' . $total . ' and all inserted Successfully.'];
            }

        // } else {

        //     $data = $_FILES['upload_csv'];
        //     $filename = $data['name'];
        //     $tmp_name = $data['tmp_name'];
        //     $allowed_ext = array("csv","ms-excel");  
        //     $extension =   explode(".", $filename)[1];

        //     if(in_array($extension, $allowed_ext))  
        //     {    

        //        $file_data = fopen($tmp_name, 'r');  
        //        fgetcsv($file_data);

        //         $total = count(fgetcsv($file_data));
        //         $total = $total - 1;

        //        while($value = fgetcsv($file_data))  
        //        {  
        //             // $website_url            = $row[0];
        //             // $domain_email           = $row[2];
        //             // //if content provide by publisher
        //             // $number_words           = $row[4];
        //             // $extra_cost             = $row[5];
        //             // $backlink_type          = $row[6];
        //             // $backlinks              = $row[7];
        //             // $turnaround_time        = $row[8];
        //             // $domain_guideline_url   = $row[9];
        //             // $exampleurlOne          = $row[10];
        //             // $exampleurlTwo          = $row[11];
        //             // $cost_price             = $row[12];

        //             // //submit data to database
        //             // $data = array(
        //             //     'domain_url'            => $website_url,
        //             //     'domain_email'          => $domain_email,
        //             //     'number_words'          => $number_words,
        //             //     'extra_cost'            => $extra_cost,
        //             //     'backlink_type'         => $backlink_type,
        //             //     'backlinks'             => $backlinks,
        //             //     'turnaround_time'       => $turnaround_time,
        //             //     'domain_guideline_url'  => $domain_guideline_url,
        //             //     'exampleurlOne'         => $exampleurlOne,
        //             //     'exampleurlTwo'         => $exampleurlTwo,
        //             //     'cost_price'            => $cost_price + $extra_cost,
        //             // );
        //             //insert into db
        //             if( !empty(trim($value[0])) && !empty(trim($value[3])) && !empty(trim($value[7])) && !empty(trim($value[8])) && !empty(trim($value[9])) && !empty(trim($value[6])) && !empty(trim($value[13])) && !empty(trim($value[1]))) {
        //                   $query = str_replace(array("'", '"','“', '”', "’",'‘', "`","@", "&quot;","https://","http://",'http://www.','https://www.','www.','WWW.'), "", htmlspecialchars($value[0]), $quotes);

        //                   $url = str_replace('/', '', $query);

        //                   //Function Call
        //                   if($this->is_valid_domain_name($url)){
                              
        //                       $whitelist = array($url); //You can add basically whatever you want here because it checks for one of these strings to be at the end of the $email string.
        //                       $email = $value[2];
        //                       if ($this->validateEmailDomain($email, $whitelist)){
                              
        //                           if(strtolower($value[1]) == 'yes'){
        //                               if(!empty(trim($value[2]))){
        //                                   $own_website = -1;
        //                                   $own_website_email = $value[2];
        //                                   // validate with domain
        //                               } else {
        //                                   continue;
        //                               }
        //                           } else {
        //                               $own_website = 0;  
        //                               $own_website_email = $value[2];
        //                           }
        //                       } else {
        //                           $own_website = 0;  
        //                           $own_website_email = '';
        //                       }
        //                       if(strtolower($value[3]) == 'publisher'){
        //                           $seller  =  array(0 => 'seller',1 => 'buyer',2 => 'hire_content');
        //                           $provide_content = serialize($seller);
        //                           if(!empty($value[4]) && !empty($value[5])){
        //                               $no_of_word = $value[4];
        //                               $content_cost = str_replace(array('$'), "", htmlspecialchars($value[5]), $quotes);
        //                           } else {
        //                               continue;
        //                           }
        //                       } else if(strtolower($value[3]) == 'advertiser'){
        //                           $buyer  =  array(0 => 'buyer',1 => 'seller',2 => 'hire_content');
        //                           $provide_content = serialize($buyer);
        //                           $no_of_word = @$value[4];
        //                           $content_cost = @$value[5];  
        //                       }

        //                       $website_amount = str_replace(array('$'), "", htmlspecialchars($value[13]), $quotes);
        //                       if($website_amount < 1){
        //                           continue;
        //                       }
                              
        //                       $id = MstDomainUrl::where('domain_url', $url)->where('domain_fk_user_id',$user_id)->where('trash',0)->value('domain_id');

        //                       if(!empty($id))
        //                       {
        //                           continue;
        //                       }

        //                       $category[$count]['domain_url'] = $url;
        //                       $category[$count]['domain_website'] = $url;
        //                       $category[$count]['domain_status'] = $own_website;  
        //                       $category[$count]['domain_email'] = $own_website_email;
        //                       $category[$count]['guest_content'] = $provide_content;
        //                       $category[$count]['number_words'] = $no_of_word;
        //                       $category[$count]['extra_cost'] = $content_cost;
        //                       $category[$count]['backlink_type'] = $value[6];
        //                       $category[$count]['backlinks'] = $value[7];
        //                       $category[$count]['max_words'] = $value[8];
        //                       $category[$count]['turnaround_time'] = $value[9];
        //                       $category[$count]['domain_guideline_url'] = $value[10];
        //                       $category[$count]['exampleurlOne'] = $value[11];
        //                       $category[$count]['exampleurlTwo'] = $value[12];
        //                       $category[$count]['domain_price'] = $website_amount;
        //                       $category[$count]['cost_price'] = $website_amount;
        //                       $category[$count]['domain_fk_user_id'] = $user_id;
        //                       $count++;
        //                   }

        //             }

        //        }
        //         $pending = $total - count($category);
        //         $insertion = DB::table('mst_domain_url')->insert($category); // Query Builder approach
               
        //         if($pending > 0){
        //             return ['status' => false, 'message' => 'Total websites are : ' . $total . ', Inserted website :' . count($category) . ' Pending websites :' . $pending .'. It could be existing domains or invalid.'];  
        //         } else {
        //             return ['status' => true, 'message' => 'Total websites are ' . $total . ' and all inserted Successfully.'];
        //         }
        //     }   
        // }
    }

}
