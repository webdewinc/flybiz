<?php

namespace App\Http\Controllers\Search;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use App\SearchRecord;
use App\MstDomainUrl;
use App\User;
use App\LogSearchQuery;
use Auth;
use App\AddToCart;
use Crypt;
use Illuminate\Http\Request;

class SearchController extends Controller
{
  
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
        //$this->middleware('guest');
        
        // $users = User::select('user_id','user_fname','user_lname','email','user_code')->get()->toArray();

        // foreach ($users as $key => $value) {

        //     if(!empty(trim($value['user_fname'])) && trim($value['user_lname'])){
        //         $fname = trim($value['user_fname']);
        //         $lname = trim($value['user_lname']);
        //         $id = $value['user_id'];
        //         $user_code =$fname.'.'.$lname.'.'.$id;
        //     } else if( !empty(trim($value['user_fname'])) ){
        //         $fname = trim($value['user_fname']);
        //         $id = $value['user_id'];
        //         $user_code = $fname.'.'.$id;
        //     } else if( !empty(trim($value['user_lname'])) ){
        //         $lname = trim($value['user_fname']);
        //         $id = $value['user_id'];
        //         $user_code = $lname.'.'.$id;
        //     } else {
        //         $user_code = uniqid('U');
        //         $id = $value['user_id'];
        //         $user_code = $user_code.'.'.$id;
        //     }
        //     User::where('user_id',$id)->update(['user_code'=>$user_code]);
        // }
        // echo '<pre>';
        // print_r($users);
        // die;


        // $array = ['seller','buyer','hire_content'];

        // $array = serialize($array);
        // MstDomainUrl::where('domain_id','!=',0)->update(['guest_content'=>$array]);
        // die;
        
    }
    public function stages_changes(Request $request){
        
        try {
            $data = [];
            $data['stages'] = $request->stages;
            if(isset($request->comment)){
                $data['comment'] = $request->comment;
            }
            $updated = AddToCart::where('id',Crypt::decrypt($request->order_id))->update($data);
            if($updated){
                return ['status' => true];    
            }
            return ['status' => false];
        } catch (Exception $e) {
            return ['status' => false];        
        }

    }
    public function stages_changes_1(Request $request){

        try {
            $data = [];
            $data['stages'] = $request['stages'];
            if(isset($request['link'])){
                $data['live_link'] = $request['link'];
            }
            $updated = AddToCart::where('id',Crypt::decrypt($request['cart_id']))->update($data);
            if($updated){
                return ['status' => true];    
            }
            return ['status' => false];
        } catch (Exception $e) {
            return ['status' => false];        
        }

    }
     public function stages_changes_2(Request $request){

        try {
            $data = [];
            $data['stages'] = $request['stages'];
            if(isset($request['link'])){
                $data['live_link'] = $request['link'];
            }
            $updated = AddToCart::where('id',Crypt::decrypt($request['cart_id']))->update($data);
            if($updated){
                return ['status' => true];    
            }
            return ['status' => false];
        } catch (Exception $e) {
            return ['status' => false];        
        }

    }
     public function stages_changes_3(Request $request){

        try {
            $data = [];
            $data['stages']  = $request['stages'];
            $data['comment'] = $request['comment']; 
            if(isset($request['link'])){
                $data['live_link'] = $request['link'];
            }
            $updated = AddToCart::where('id',Crypt::decrypt($request['cart_id']))->update($data);
            if($updated){
                return ['status' => true];    
            }
            return ['status' => false];
        } catch (Exception $e) {
            return ['status' => false];        
        }

    }
    public function order_deliver(Request $request){

        try {
            $data = [];
            $data['stages'] = $request['stages'];
            $updated = AddToCart::where('id',Crypt::decrypt($request['cart_id']))->update($data);
            if($updated){
                return ['status' => true];    
            }
            return ['status' => false];
        } catch (Exception $e) {
            return ['status' => false];        
        }

    }
    public function release_payment(Request $request){  
        try {
            $data = [];
            $data['stages'] = $request['stages'];
            $updated = AddToCart::where('id',Crypt::decrypt($request['cart_id']))->update($data);
            if($updated){
                return ['status' => true];    
            }
            return ['status' => false];
        } catch (Exception $e) {
            return ['status' => false];        
        }

    }
    function getQueryStr($col, $val)
    {
        $d = explode("-", $val);

        $selectQuery = "";
                
        if ($d[0] == "" && $d == "") {
        } elseif ($d[0] == "" || $d[1] == "") {
            $selectQuery = " AND ".$col." >= ".$d[0].$d[1];
        } else {
            $selectQuery = " AND (".$col." BETWEEN $d[0] AND $d[1]) ";
        }
        return $selectQuery;
    }

    public function log_searchquery($q = '', $total_results = 0){

        date_default_timezone_set('Asia/Kolkata');
        $date = date('Y-m-d');
        $datetime = date('Y-m-d H:i:s');
        // Remove special characters
        if(!empty($q)){
            $q = preg_replace('/[^A-Za-z0-9]/', ' ', $q);

            if(Auth::check()){
                $last = LogSearchQuery::where('query_created_date','>',$date.' 00:00:00')->where('query_created_date','<',$date.' 23:59:59')->where('query_fk_user',Auth::user()->user_id)->where('query_string',$q)->first();
                $last = json_decode(json_encode($last), true);

                $query_index = 1;
                if(!empty($last)){
                    
                    if(!empty($last['query_index'])) {
                        $query_index = $last['query_index'] + 1;
                    }                

                    $last = DB::table('log_searchquery')->where('query_id',$last['query_id'])->update([
                            'query_index' => $query_index,
                            'query_results_count' => $total_results
                        ]);

                } else {
                    
                    $last = LogSearchQuery::where('query_id',$last['query_id'])->insert([
                            'query_index' => $query_index,
                            'query_results_count' => $total_results,
                            'query_string' => $q,
                            'query_created_date' => $datetime,
                            'query_fk_user' => Auth::user()->user_id,
                        ]);
                }
            } else {
                $last = LogSearchQuery::where('query_created_date','>',$date.' 00:00:00')->where('query_created_date','<',$date.' 23:59:59')->where('query_fk_user',NULL)->where('query_string',$q)->first();
                $last = json_decode(json_encode($last), true);
                $query_index = 1;
                if(!empty($last)){
                    
                    if(!empty($last['query_index'])) {
                        $query_index = $last['query_index'] + 1;
                    }                

                    $last = DB::table('log_searchquery')->where('query_id',$last['query_id'])->update([
                            'query_index' => $query_index,
                            'query_results_count' => $total_results
                        ]);

                } else {
                    
                    $last = LogSearchQuery::where('query_id',$last['query_id'])->insert([
                            'query_index' => $query_index,
                            'query_results_count' => $total_results,
                            'query_string' => $q,
                            'query_created_date' => $datetime,
                        ]);
                }

            }    
        }
        
    }
    
    public function ajax_search($REQUEST,$terms, $no_of_page){

            $recursion[0] = [];
            $ids = [];
            $search = SearchRecord::query(); 

            foreach ($terms as $key => $term) {
                $term = trim($term);
                $innerSearch = SearchRecord::query(); 

                if(!empty($ids[$key-1])) {

                    // Use implode() function to join
                    $List = implode(', ', $ids[$key-1]); 
                    
                    $term_list = " a LIKE '%$term%' OR h6 LIKE '%$term%' OR h5 LIKE '%$term%' OR h4 LIKE '%$term%' OR h3 LIKE '%$term%' OR h2 LIKE '%$term%' OR h1 LIKE '%$term%' OR p LIKE '%$term%' OR  domain_website LIKE '%$term%' OR  domain_url LIKE '%$term%' OR title LIKE '%$term%' OR description LIKE '%$term%' OR domain_url LIKE '%$term%' ";
    

                    // $selectQuery = "SELECT domain_id FROM search_records WHERE ( $term_list ) AND domain_id IN ( $List ) GROUP BY domain_url ";
                    $selectQuery = "SELECT domain_id FROM search_records WHERE ( $term_list ) AND domain_id IN ( $List ) GROUP BY domain_url ";


                    //GROUP BY domain_url limit $no_of_page
                    $results = DB::select( DB::raw($selectQuery) );
                    $results = json_decode(json_encode($results), true);
                    if(!empty($results)){                                

                        $i = [];
                        foreach ($results as $key => $value) {
                            $i[] = $value['domain_id'];
                        }
                        $recursion[0] = $i;
                        //$recursion[0] = $ids[$key];
                    } else {
                        break;
                    }
                }

                if($key == 0){
                    
                    $innerSearch->orWhere('title' , 'LIKE','%'.$term.'%');
                    $innerSearch->orWhere('description' , 'LIKE','%'.$term.'%');
                    $innerSearch->orWhere('h1' , 'LIKE','%'.$term.'%');
                    $innerSearch->orWhere('h2' , 'LIKE','%'.$term.'%');
                    $innerSearch->orWhere('h3' , 'LIKE','%'.$term.'%');
                    $innerSearch->orWhere('h4' , 'LIKE','%'.$term.'%');
                    $innerSearch->orWhere('h5' , 'LIKE','%'.$term.'%');
                    $innerSearch->orWhere('h6' , 'LIKE','%'.$term.'%');
                    $innerSearch->orWhere('a' , 'LIKE','%'.$term.'%');
                    $innerSearch->orWhere('p' , 'LIKE','%'.$term.'%');
                    $innerSearch->orWhere('domain_url' , 'LIKE','%'.$term.'%');
                    $innerSearch->orWhere('domain_website' , 'LIKE','%'.$term.'%');
                    $ids[$key] =  $innerSearch->pluck('domain_id')->toArray();
                    $recursion[0] = $ids[$key];
                }

            }

            $search->whereIn('domain_id', $recursion[0]);
            
            if (@$REQUEST['da_start']) {
                $search->where('dp_da', '>=' ,$REQUEST['da_start'])->where('dp_da', '<=' ,$_REQUEST['da_end']);
                
            }
            if (@$REQUEST['traffic_start']) {
                $search->where('sd_visits', '>=' ,$REQUEST['da_start'])->where('sd_visits', '<=' ,$_REQUEST['traffic_end']);
            }

            if (@$REQUEST['orders_start']) {
                $search->where('orders', '>=' ,$REQUEST['orders_start'])->where('orders', '<=' ,$_REQUEST['orders_end']);
            }

            if (@$REQUEST['verify']) {
                $search->where('domain_status', '=' ,1);
            } 

            if (@$REQUEST['generalSearch']) {

                $searchQueryWeb = str_replace(" ", "%", $REQUEST['generalSearch']);
                //$search->where('domain_website', 'LIKE' ,'%'.$searchQueryWeb.'%');
                //$search->where('domain_url', 'LIKE' ,'%'.$searchQueryWeb.'%');
                $search->where('domain_url', '=' ,$searchQueryWeb);
            }

            $selectQuery =  $search->groupBy('domain_url')->pluck('domain_id')->toArray();
            return $selectQuery;

                    
    }

    public function ajax_search_single($REQUEST,$terms, $no_of_page){
            
            $terms = str_replace(array("'", '"','“', '”', "’",'‘', "`", "&quot;"), "", htmlspecialchars($terms));
            // $terms = str_replace('"', "", $terms);
            // $terms = str_replace("'", "", $terms);
       
            $term_list = " a LIKE '%$terms%' OR h6 LIKE '%$terms%' OR h5 LIKE '%$terms%' OR h4 LIKE '%$terms%' OR h3 LIKE '%$terms%' OR h2 LIKE '%$terms%' OR h1 LIKE '%$terms%' OR p LIKE '%$terms%' OR  domain_website LIKE '%$terms%' OR  domain_url LIKE '%$terms%' OR title LIKE '%$terms%' OR description LIKE '%$terms%' OR domain_url LIKE '%$terms%' ";
            
            
            $matchTheseOut = '';
            if (@$REQUEST['da_start']) {
                // $matchTheseOut .= " AND ( dp_da BETWEEN ".$REQUEST['da_start']." AND ".$_REQUEST['da_end']." ) ";

                $matchTheseOut .= " AND ( dp_da >= ".$REQUEST['da_start']." AND dp_da <= ".$_REQUEST['da_end']." ) ";
            }
            if (@$REQUEST['traffic_start']) {
                //$matchTheseOut .= " AND ( sd_visits BETWEEN ".$REQUEST['traffic_start']." AND ".$REQUEST['traffic_end']." )";
                $matchTheseOut .= " AND  ( sd_visits >= ".$REQUEST['traffic_start']." AND <= ".$REQUEST['traffic_end']." )";
            }
            // if (@$REQUEST['bounce_start']) {
            //     $matchTheseOut .= "AND ( sd_bounce_rate BETWEEN ".$REQUEST['bounce_start']." AND " .$REQUEST['bounce_end']." )"; 
            // }

            if (@$REQUEST['orders_start']) {
                $matchTheseOut .= " AND  ( orders >= ".$REQUEST['orders_start']." AND <= ".$REQUEST['orders_end']." )";
            }

            if (@$REQUEST['verify']) {
                $matchTheseOut .= " AND domain_status = 1";
            } 

            if (@$REQUEST['generalSearch']) {
                $searchQueryWeb = str_replace(" ", "%", $REQUEST['generalSearch']);
                //$matchTheseOut .= " AND domain_website LIKE '%".$searchQueryWeb."%' ";
                //$matchTheseOut .= " AND domain_url LIKE '%".$searchQueryWeb."%' ";
                $matchTheseOut .= " AND domain_url = '".$searchQueryWeb."'";
            }

            $selectQuery = "SELECT domain_id FROM search_records WHERE ( $term_list ) AND (domain_approval_status = 1 $matchTheseOut ) GROUP BY domain_url ";
            

            //GROUP BY domain_url limit $no_of_page
            $results = DB::select( DB::raw($selectQuery) );            
            $ids = [];
            foreach ($results as $key => $value) {
                $ids[] = $value->domain_id;
            }
            return $ids;

    }


    public function ajax_offset($REQUEST){
        $offset = '';
        
        
        $offset .= "&da_start=".$REQUEST['da_start']."&da_end=".$_REQUEST['da_end'];
        
        if($REQUEST['traffic_start'] > 0 || $REQUEST['traffic_end'] < 1000000){
            $offset .= "&traffic_start=".$REQUEST['traffic_start']."&traffic_end=".$REQUEST['traffic_end'];    
        }
        if($REQUEST['orders_start'] > 0 || $REQUEST['orders_end'] < 100){
            $offset .= "&orders_start=".$REQUEST['orders_start']."&orders_end=".$REQUEST['orders_end'];
        }

        if (@$REQUEST['verify']) {
            $offset .= "&verify=1";
        } 

        if (@$REQUEST['generalSearch']) {
            $searchQueryWeb = str_replace(" ", "%", $REQUEST['generalSearch']);
            $offset .= "&generalSearch=".$searchQueryWeb;
        }
        if (@$REQUEST['order']) {
            $offset .= "&order=".$REQUEST['order']."&orderwise=".$REQUEST['orderwise'];
        }
        return $offset;
    }

    // public function search(Request $request) {

    //     if(isset($_REQUEST['q'])) {
            
    //         $query = $_GET['q'];
    //         $query = str_replace(array("'", '"','“', '”', "’",'‘', "`", "&quot;"), "", htmlspecialchars($query), $quotes);

    //         $no_of_page = 10;
    //         $offset = '';
    //         if(isset($_REQUEST['offset'])){
    //             $no_of_page = $_REQUEST['offset'];
    //             $offset = '&offset=' . $_REQUEST['offset'];
    //         }

    //         $orderBy = 'dp_da';
    //         //$orderBy = 'cost_price';
    //         $orderwise = 'DESC';
            
    //         if(@$_REQUEST['order']) {
    //             $orderBy = $_REQUEST['order'];
    //             $orderwise = $_REQUEST['orderwise'];
    //             $offset .= '&order=' . $orderBy . '&orderwise=' . $orderwise;
    //         }

    //         $terms = [];    
    //         $terms = explode(' ', $query);
    //         $search = SearchRecord::query(); 
    //         $recursion[0] = [];
    //         if($request->ajax()) {

    //             // ajax result    
    //             // $ids = $this->ajax_search($_REQUEST, $terms,$no_of_page);
    //             // $search = SearchRecord::query()->whereIn('domain_id',$ids);
    //             $quotes = $_REQUEST['quotes'];
    //             if($quotes == 0){
    //                 $ids = $this->ajax_search($_REQUEST, $terms,$no_of_page);
    //                 $ajax_offset = $this->ajax_offset($_REQUEST);
    //                 $offset .= $ajax_offset;
    //                 $recursion[0] = $ids;
    //             } else {
    //                 $ids = $this->ajax_search_single($_REQUEST, $query,$no_of_page);
    //                 $ajax_offset = $this->ajax_offset($_REQUEST);
    //                 $offset .= $ajax_offset;
    //                 $recursion[0] = $ids;
    //             }

    //             if (@$_REQUEST['da_start'] || @$_REQUEST['generalSearch'] || @$_REQUEST['traffic_start'] || @$_REQUEST['bounce_start'] || @$_REQUEST['orders_start'] || @$_REQUEST['verify']) {
    //                 $path = 'render.search';
    //             }


    //         }  else {

    //             if($quotes == 0){
    //                 $counting = count($terms);
    //                 $ids = [];

    //                 foreach ($terms as $key => $term) {
    //                     $term = trim($term);
    //                     $innerSearch = SearchRecord::query(); 

    //                     if(!empty($ids[$key-1])) {

    //                         // Use implode() function to join
    //                         $List = implode(', ', $ids[$key-1]); 
                            
    //                         $term_list = " a LIKE '%$term%' OR h6 LIKE '%$term%' OR h5 LIKE '%$term%' OR h4 LIKE '%$term%' OR h3 LIKE '%$term%' OR h2 LIKE '%$term%' OR h1 LIKE '%$term%' OR p LIKE '%$term%' OR  domain_website LIKE '%$term%' OR  domain_url LIKE '%$term%' OR title LIKE '%$term%' OR description LIKE '%$term%' OR domain_url LIKE '%$term%' ";
            

    //                         $selectQuery = "SELECT domain_id FROM search_records WHERE ( $term_list ) AND domain_id IN ( $List ) GROUP BY domain_url ";


    //                         //GROUP BY domain_url limit $no_of_page
    //                         $results = DB::select( DB::raw($selectQuery) );
    //                         $results = json_decode(json_encode($results), true);
    //                         if(!empty($results)){                                

    //                             $i = [];
    //                             foreach ($results as $key => $value) {
    //                                 $i[] = $value['domain_id'];
    //                             }
    //                             $recursion[0] = $i;
    //                             //$recursion[0] = $ids[$key];
    //                         } else {
    //                             break;
    //                         }
    //                     }

    //                     if($key == 0){

    //                         $innerSearch->orWhere('title' , 'LIKE','%'.$term.'%');
    //                         $innerSearch->orWhere('description' , 'LIKE','%'.$term.'%');
    //                         $innerSearch->orWhere('sd_descr_n' , 'LIKE','%'.$term.'%');
    //                         $innerSearch->orWhere('sd_descr' , 'LIKE','%'.$term.'%');    
    //                         $innerSearch->orWhere('h1' , 'LIKE','%'.$term.'%');
    //                         $innerSearch->orWhere('h2' , 'LIKE','%'.$term.'%');
    //                         $innerSearch->orWhere('h3' , 'LIKE','%'.$term.'%');
    //                         $innerSearch->orWhere('h4' , 'LIKE','%'.$term.'%');
    //                         $innerSearch->orWhere('h5' , 'LIKE','%'.$term.'%');
    //                         $innerSearch->orWhere('h6' , 'LIKE','%'.$term.'%');
    //                         $innerSearch->orWhere('a' , 'LIKE','%'.$term.'%');
    //                         $innerSearch->orWhere('p' , 'LIKE','%'.$term.'%');
    //                         $innerSearch->orWhere('domain_url' , 'LIKE','%'.$term.'%');
    //                         $innerSearch->orWhere('domain_website' , 'LIKE','%'.$term.'%');
    //                         $ids[$key] =  $innerSearch->pluck('domain_id')->toArray();
    //                         $recursion[0] = $ids[$key];
    //                     }

    //                 }

    //             } else {

    //                 $term = str_replace(array("'", '"','“', '”', "’",'‘', "`","&quot;"), "", htmlspecialchars($query) );

    //                 $innerSearch = SearchRecord::query();
    //                 $innerSearch->orWhere('title' , 'LIKE','%'.$term.'%');
    //                 $innerSearch->orWhere('description' , 'LIKE','%'.$term.'%');
    //                 $innerSearch->orWhere('h1' , 'LIKE','%'.$term.'%');
    //                 $innerSearch->orWhere('h2' , 'LIKE','%'.$term.'%');
    //                 $innerSearch->orWhere('h3' , 'LIKE','%'.$term.'%');
    //                 $innerSearch->orWhere('h4' , 'LIKE','%'.$term.'%');
    //                 $innerSearch->orWhere('h5' , 'LIKE','%'.$term.'%');
    //                 $innerSearch->orWhere('h6' , 'LIKE','%'.$term.'%');
    //                 $innerSearch->orWhere('a' , 'LIKE','%'.$term.'%');
    //                 $innerSearch->orWhere('p' , 'LIKE','%'.$term.'%');
    //                 $innerSearch->orWhere('domain_url' , 'LIKE','%'.$term.'%');
    //                 $innerSearch->orWhere('domain_website' , 'LIKE','%'.$term.'%');
    //                 $recursion[0] =  $innerSearch->pluck('domain_id')->toArray();

    //                 /* with quotes */
    //             }
                
    //             $path = 'search';


    //             /* after filtered */
    //             $REQUEST = $_REQUEST;
    //             if (isset($REQUEST['da_start'])) {
    //                 $search->where('dp_da', '>=' ,$REQUEST['da_start'])->where('dp_da', '<=' ,$_REQUEST['da_end']);
    //                 $offset .= '&da_start='.$REQUEST['da_start'].'&da_end='.$REQUEST['da_end'];
    //             }
    //             if (isset($REQUEST['traffic_start'])) {

    //                 //$search->whereBetween('sd_visits', array($REQUEST['traffic_start'], $_REQUEST['traffic_end']));

    //                 $search->where('sd_visits', '>=' ,$REQUEST['traffic_start'])->where('sd_visits', '<=' ,$_REQUEST['traffic_end']);

    //                 $offset .= '&traffic_start='.$REQUEST['traffic_start'].'&traffic_end='.$REQUEST['traffic_end'];
    //             }

    //             if (isset($REQUEST['orders_start'])) {
    //                 //$search->whereBetween('orders', array($REQUEST['orders_start'], $_REQUEST['orders_end']));
    //                 $search->where('orders', '>=' ,$REQUEST['orders_start'])->where('orders', '<=' ,$_REQUEST['orders_end']);
    //                 $offset .= '&orders_start='.$REQUEST['orders_start'].'&orders_end='.$REQUEST['orders_end'];
    //             }

    //             if (isset($REQUEST['verify'])) {
    //                 $search->where('domain_status', '=' ,1);
    //                 $offset .= '&verify=1';
    //             } 

    //             if (isset($REQUEST['generalSearch'])) {

    //                 $searchQueryWeb = str_replace(" ", "%", $REQUEST['generalSearch']);
    //                 //$search->where('domain_website', 'LIKE' ,'%'.$searchQueryWeb.'%');
    //                 //$search->where('domain_url', 'LIKE' ,'%'.$searchQueryWeb.'%');
    //                 $search->where('domain_url', '=' ,$searchQueryWeb);
    //                 $offset .= '&generalSearch='.$REQUEST['generalSearch'];
    //             }

    //             /* after filtered */

    //         }
            


    //         $search->select('domain_id','domain_url','backlink_type','sd_title','sd_title_n','title','sd_descr','sd_descr_n','description','cost_price','dp_da','sd_global_rank','sd_visits','orders','domain_status')->whereIn('domain_id', $recursion[0])->where('domain_approval_status',1);

    //         $selectQuery =  $search->groupBy('domain_url')->orderBy($orderBy,$orderwise)->paginate($no_of_page);
    //         //store search start
    //         $this->log_searchquery($_REQUEST['q'],$selectQuery->total());
    //         // store search end 
    //         $search_record = $selectQuery->total();
    //         $record_found = json_decode(json_encode($selectQuery),true);            
            
    //         $record_found = count($record_found['data']);
            
    //         $selectQuery->withPath('?q=' . $_REQUEST['q'] . $offset);
    //         return view($path,compact('selectQuery','record_found','search_record'));
    //     } 
    //     return redirect()->to('/');
    // }
    public function search(Request $request) {

        if(isset($_REQUEST['q'])) {
            $query = $_REQUEST['q'];
            $query = str_replace(array("'", '"','“', '”', "’",'‘', "`","@", "&quot;"), "", htmlspecialchars($query), $quotes);
            $user_id = User::whereUserCode($query)->value('user_id');

            if(!empty($user_id)) {

                

                $no_of_page = 10;
                $offset = '';
                if(isset($_REQUEST['offset'])){
                    $no_of_page = $_REQUEST['offset'];
                    $offset = '&offset=' . $_REQUEST['offset'];
                }

                $orderBy = 'dp_da';
                $orderwise = 'DESC';
                
                if(@$_REQUEST['order']) {
                    $orderBy = $_REQUEST['order'];
                    $orderwise = $_REQUEST['orderwise'];
                    $offset .= '&order=' . $orderBy . '&orderwise=' . $orderwise;
                }
                
                $search = SearchRecord::query();

                /* after filtered */
                $REQUEST = $_REQUEST;
                if (isset($REQUEST['da_start'])) {
                    $search->where('dp_da', '>=' ,$REQUEST['da_start'])->where('dp_da', '<=' ,$_REQUEST['da_end']);
                    $offset .= '&da_start='.$REQUEST['da_start'].'&da_end='.$REQUEST['da_end'];
                }
                if (isset($REQUEST['traffic_start'])) {

                    //$search->whereBetween('sd_visits', array($REQUEST['traffic_start'], $_REQUEST['traffic_end']));

                    $search->where('sd_visits', '>=' ,$REQUEST['traffic_start'])->where('sd_visits', '<=' ,$_REQUEST['traffic_end']);

                    $offset .= '&traffic_start='.$REQUEST['traffic_start'].'&traffic_end='.$REQUEST['traffic_end'];
                }

                if (isset($REQUEST['orders_start'])) {
                    //$search->whereBetween('orders', array($REQUEST['orders_start'], $_REQUEST['orders_end']));
                    $search->where('orders', '>=' ,$REQUEST['orders_start'])->where('orders', '<=' ,$_REQUEST['orders_end']);
                    $offset .= '&orders_start='.$REQUEST['orders_start'].'&orders_end='.$REQUEST['orders_end'];
                }

                if (isset($REQUEST['verify'])) {
                    $search->where('domain_status', '=' ,1);
                    $offset .= '&verify=1';
                } 

                if (isset($REQUEST['generalSearch'])) {

                    $searchQueryWeb = str_replace(" ", "%", $REQUEST['generalSearch']);
                    //$search->where('domain_website', 'LIKE' ,'%'.$searchQueryWeb.'%');
                    //$search->where('domain_url', 'LIKE' ,'%'.$searchQueryWeb.'%');
                    $search->where('domain_url', '=' ,$searchQueryWeb);
                    $offset .= '&generalSearch='.$REQUEST['generalSearch'];
                }

                /* after filtered */
                $search->select('domain_id','domain_url','domain_fk_user_id','backlink_type','sd_title','sd_title_n','title','sd_descr','sd_descr_n','description','cost_price','dp_da','sd_global_rank','sd_visits','orders','domain_status')->where('assigned_user_id', $user_id)->where('domain_approval_status',1);

                $selectQuery =  $search->groupBy('domain_url')->orderBy($orderBy,$orderwise)->paginate($no_of_page);

                //store search start
                $this->log_searchquery($_REQUEST['q'],$selectQuery->total());
                // store search end 
                $search_record = $selectQuery->total();
                $record_found = json_decode(json_encode($selectQuery),true);            
                
                $record_found = count($record_found['data']);
                
                $selectQuery->withPath('?q=' . $_REQUEST['q'] . $offset);
                $path = 'search';
                return view($path,compact('selectQuery','record_found','search_record'));



            } else {
                $query = $_GET['q'];
                $query = str_replace(array("'", '"','“', '”', "’",'‘', "`", "&quot;"), "", htmlspecialchars($query), $quotes);

                $no_of_page = 10;
                $offset = '';
                if(isset($_REQUEST['offset'])){
                    $no_of_page = $_REQUEST['offset'];
                    $offset = '&offset=' . $_REQUEST['offset'];
                }

                $orderBy = 'dp_da';
                //$orderBy = 'cost_price';
                $orderwise = 'DESC';
                
                if(@$_REQUEST['order']) {
                    $orderBy = $_REQUEST['order'];
                    $orderwise = $_REQUEST['orderwise'];
                    $offset .= '&order=' . $orderBy . '&orderwise=' . $orderwise;
                }

                $terms = [];    
                $terms = explode(' ', $query);
                $search = SearchRecord::query(); 
                $recursion[0] = [];
                

                if($quotes == 0){

                    $counting = count($terms);
                    $ids = [];

                    foreach ($terms as $key => $term) {
                        $term = trim($term);
                        $innerSearch = SearchRecord::query(); 

                        if(!empty($ids[$key-1])) {

                            // Use implode() function to join
                            $List = implode(', ', $ids[$key-1]); 
                            
                            $term_list = " a LIKE '%$term%' OR h6 LIKE '%$term%' OR h5 LIKE '%$term%' OR h4 LIKE '%$term%' OR h3 LIKE '%$term%' OR h2 LIKE '%$term%' OR h1 LIKE '%$term%' OR p LIKE '%$term%' OR  domain_website LIKE '%$term%' OR  domain_url LIKE '%$term%' OR title LIKE '%$term%' OR description LIKE '%$term%' OR domain_url LIKE '%$term%' ";

                            $selectQuery = "SELECT domain_id FROM search_records WHERE ( $term_list ) AND domain_id IN ( $List ) GROUP BY domain_url ";

                            //GROUP BY domain_url limit $no_of_page
                            $results = DB::select( DB::raw($selectQuery) );
                            $results = json_decode(json_encode($results), true);
                            if(!empty($results)){                                

                                $i = [];
                                foreach ($results as $key => $value) {
                                    $i[] = $value['domain_id'];
                                }
                                $recursion[0] = $i;
                                //$recursion[0] = $ids[$key];
                            } else {
                                break;
                            }
                        }

                        if($key == 0){

                            $innerSearch->orWhere('title' , 'LIKE','%'.$term.'%');
                            $innerSearch->orWhere('description' , 'LIKE','%'.$term.'%');
                            $innerSearch->orWhere('sd_descr_n' , 'LIKE','%'.$term.'%');
                            $innerSearch->orWhere('sd_descr' , 'LIKE','%'.$term.'%');    
                            $innerSearch->orWhere('h1' , 'LIKE','%'.$term.'%');
                            $innerSearch->orWhere('h2' , 'LIKE','%'.$term.'%');
                            $innerSearch->orWhere('h3' , 'LIKE','%'.$term.'%');
                            $innerSearch->orWhere('h4' , 'LIKE','%'.$term.'%');
                            $innerSearch->orWhere('h5' , 'LIKE','%'.$term.'%');
                            $innerSearch->orWhere('h6' , 'LIKE','%'.$term.'%');
                            $innerSearch->orWhere('a' , 'LIKE','%'.$term.'%');
                            $innerSearch->orWhere('p' , 'LIKE','%'.$term.'%');
                            $innerSearch->orWhere('domain_url' , 'LIKE','%'.$term.'%');
                            $innerSearch->orWhere('domain_website' , 'LIKE','%'.$term.'%');
                            $ids[$key] =  $innerSearch->pluck('domain_id')->toArray();
                            $recursion[0] = $ids[$key];
                        }

                    }

                } else {

                    $term = str_replace(array("'", '"','“', '”', "’",'‘', "`","&quot;"), "", htmlspecialchars($query) );

                    $innerSearch = SearchRecord::query();
                    $innerSearch->orWhere('title' , 'LIKE','%'.$term.'%');
                    $innerSearch->orWhere('description' , 'LIKE','%'.$term.'%');
                    $innerSearch->orWhere('h1' , 'LIKE','%'.$term.'%');
                    $innerSearch->orWhere('h2' , 'LIKE','%'.$term.'%');
                    $innerSearch->orWhere('h3' , 'LIKE','%'.$term.'%');
                    $innerSearch->orWhere('h4' , 'LIKE','%'.$term.'%');
                    $innerSearch->orWhere('h5' , 'LIKE','%'.$term.'%');
                    $innerSearch->orWhere('h6' , 'LIKE','%'.$term.'%');
                    $innerSearch->orWhere('a' , 'LIKE','%'.$term.'%');
                    $innerSearch->orWhere('p' , 'LIKE','%'.$term.'%');
                    $innerSearch->orWhere('domain_url' , 'LIKE','%'.$term.'%');
                    $innerSearch->orWhere('domain_website' , 'LIKE','%'.$term.'%');
                    $recursion[0] =  $innerSearch->pluck('domain_id')->toArray();

                    /* with quotes */
                }
                
                $path = 'search';


                /* after filtered */
                $REQUEST = $_REQUEST;
                if (isset($REQUEST['da_start'])) {
                    $search->where('dp_da', '>=' ,$REQUEST['da_start'])->where('dp_da', '<=' ,$_REQUEST['da_end']);
                    $offset .= '&da_start='.$REQUEST['da_start'].'&da_end='.$REQUEST['da_end'];
                }
                if (isset($REQUEST['traffic_start'])) {

                    //$search->whereBetween('sd_visits', array($REQUEST['traffic_start'], $_REQUEST['traffic_end']));

                    $search->where('sd_visits', '>=' ,$REQUEST['traffic_start'])->where('sd_visits', '<=' ,$_REQUEST['traffic_end']);

                    $offset .= '&traffic_start='.$REQUEST['traffic_start'].'&traffic_end='.$REQUEST['traffic_end'];
                }

                if (isset($REQUEST['orders_start'])) {
                    //$search->whereBetween('orders', array($REQUEST['orders_start'], $_REQUEST['orders_end']));
                    $search->where('orders', '>=' ,$REQUEST['orders_start'])->where('orders', '<=' ,$_REQUEST['orders_end']);
                    $offset .= '&orders_start='.$REQUEST['orders_start'].'&orders_end='.$REQUEST['orders_end'];
                }

                if (isset($REQUEST['verify'])) {
                    $search->where('domain_status', '=' ,1);
                    $offset .= '&verify=1';
                } 

                if (isset($REQUEST['generalSearch'])) {

                    $searchQueryWeb = str_replace(" ", "%", $REQUEST['generalSearch']);
                    //$search->where('domain_website', 'LIKE' ,'%'.$searchQueryWeb.'%');
                    //$search->where('domain_url', 'LIKE' ,'%'.$searchQueryWeb.'%');
                    $search->where('domain_url', '=' ,$searchQueryWeb);
                    $offset .= '&generalSearch='.$REQUEST['generalSearch'];
                }

                /* after filtered */
                $search->select('domain_id','domain_url','domain_fk_user_id','backlink_type','sd_title','sd_title_n','title','sd_descr','sd_descr_n','description','cost_price','dp_da','sd_global_rank','sd_visits','orders','domain_status')->whereIn('domain_id', $recursion[0])->where('domain_approval_status',1);

                $selectQuery =  $search->groupBy('domain_url')->orderBy($orderBy,$orderwise)->paginate($no_of_page);

                //store search start
                $this->log_searchquery($_REQUEST['q'],$selectQuery->total());
                // store search end 
                $search_record = $selectQuery->total();
                $record_found = json_decode(json_encode($selectQuery),true);            
                
                $record_found = count($record_found['data']);
                
                $selectQuery->withPath('?q=' . $_REQUEST['q'] . $offset);
                return view($path,compact('selectQuery','record_found','search_record'));
            }
        } 
        return redirect()->to('/');
    }

    public function search_websites($domain_url) {

        $no_of_page = 10;
        $offset = '';
        if(isset($_REQUEST['offset'])){
            $no_of_page = $_REQUEST['offset'];
            $offset = '&offset=' . $_REQUEST['offset'];
        }

        $matchThese = [ ['mst_domain_url.domain_url' , $domain_url]];

        $selectQueryWebsite = MstDomainUrl::where($matchThese)->with(['data_da_pa','data_seo','data_report_spam','data_search_url','data_similar_site','data_organic_search','data_cnf_domain_country.data_country','data_assigned_website.data_username','data_add_to_cart.username','data_add_to_cart.domainusername','data_extra_field'])->where('cost_price','>',0)->groupBy('domain_url')->get()->toArray();

        $follow = MstDomainUrl::where($matchThese)->where('backlink_type','follow')->count();

        $selectQueryUserListcount = MstDomainUrl::where($matchThese)->where('domain_approval_status','=',1)->where('cost_price','>',0)->where('trash','<',1)->where('domain_status',1)->count();
        if($selectQueryUserListcount > 0){
            $selectQueryUserList = MstDomainUrl::where($matchThese)->has('data_assigned_website')->with(['data_da_pa','data_seo','data_report_spam','data_search_url','data_similar_site','data_organic_search','data_cnf_domain_country.data_country','data_assigned_website.data_username','data_add_to_cart.username','data_add_to_cart.domainusername','data_extra_field'])->where('domain_status',1)->where('domain_approval_status','=',1)->orderBy('cost_price','ASC')->where('cost_price','>',0)->where('trash','<',1)->groupBy('domain_fk_user_id')->paginate($no_of_page);
        } else {
            $selectQueryUserList = MstDomainUrl::where($matchThese)->has('data_assigned_website')->with(['data_da_pa','data_seo','data_report_spam','data_search_url','data_similar_site','data_organic_search','data_cnf_domain_country.data_country','data_assigned_website.data_username','data_add_to_cart.username','data_add_to_cart.domainusername','data_extra_field'])->where('domain_status',0)->where('domain_approval_status','=',1)->orderBy('cost_price','ASC')->where('cost_price','>',0)->where('trash','<',1)->groupBy('domain_fk_user_id')->paginate($no_of_page);
        }
        

        $selectQueryUserListArray = json_decode(json_encode($selectQueryUserList), true);

        if($selectQueryUserListArray['total'] > 0){
            $array['cnf']  = [];
            $array['org']  = [];
            $array['org_referral']  = [];
            $array['org_search'] = [];
            $array['org_social'] = [];
            
            foreach($selectQueryWebsite as $key => $split_Query) :

                $count = 0;
                if(!empty($split_Query['data_cnf_domain_country'])):
                    foreach($split_Query['data_cnf_domain_country'] as $k => $v) :
                        if(!empty($v['cnf_value'])):
                           $array['cnf'][0]['label'] =  $v['data_country']['country_name'];
                           $array['cnf'][0]['value'] =  $v['cnf_value'];
                           $count++;
                        endif;
                    endforeach;
                endif;
                $count = 0;
                $org_search = 0;
                $org_referral = 0;
                $org_social = 0;
                if(!empty($split_Query['data_extra_field'])):
                    foreach($split_Query['data_extra_field'] as $k => $v) :
                        if( $k == 'org_direct' ):
                            $data = @unserialize($v);
                            if(!empty($data['percent'])){
                                $array['org'][$count]['label'] =  'Direct';
                                $array['org'][$count]['value'] =  @$data['percent'];
                                $count++;
                            }
                        endif;
                        if( $k == 'org_main_percent' ):
                            if(!empty($v)){
                                $array['org'][$count]['label'] =  'Search Organic';
                                $array['org'][$count]['value'] =  @$v;
                                $count++;
                            }
                        endif;
                        if( $k == 'org_search_ad' ):
                           
                            
                                $data = @unserialize($v);
                                if(!empty($data['percent'])){
                                    $array['org'][$count]['label'] =  'Search Adds';
                                    $array['org'][$count]['value'] =  @$data['percent'];
                                    $count++;
                                }                        
                        endif;
                        if( $k == 'org_referral' ):

                            $data = @unserialize($v);
                            if(!empty($data['percent'])){
                                $array['org'][$count]['label'] =  'Referral';
                                $array['org'][$count]['value'] =  @$data['percent'];
                                $count++;
                            }
                        endif;
                        if( $k == 'org_referral_ad' ):
                            $data = @unserialize($v);
                            if(!empty($data['percent'])){
                                $array['org'][$count]['label'] =  'Referral Ads';
                                $array['org'][$count]['value'] =  @$data['percent'];
                                $count++;
                            }
                            
                        endif;
                        if( $k == 'org_social' ):
                            $data = @unserialize($v);
                            if(!empty($data['percent'])){
                                $array['org'][$count]['label'] =  'Social';
                                $array['org'][$count]['value'] =  @$data['percent'];
                                $count++;
                            }
                            
                        endif;                

                        if( $k == 'org_referral' ):
                            $data = @unserialize($v);
                            if(!empty($data['percent'])){
                                $array['org_referral'][$org_referral]['label'] =  'Organic';
                                $array['org_referral'][$org_referral]['value'] =  $data['percent'];
                                $org_referral++;
                            }
                        endif;

                        if( $k == 'org_referral_ad' ):
                            $data = @unserialize($v);
                            if(!empty($data['percent'])){
                                $array['org_referral'][$org_referral]['label'] =  'Paid';
                                $array['org_referral'][$org_referral]['value'] =  $data['percent'];
                                $org_referral++;
                            }
                        endif;
                        
                        if( $k == 'org_main_value' ):
                            if(!empty($v)){
                                $array['org_search'][$org_search]['label'] =  'Organic';
                                $array['org_search'][$org_search]['value'] =  $v;
                                $org_search++;
                            }
                        endif;
                        if( $k == 'org_search_ad' ):
                            $data = @unserialize($v);
                            if(!empty($data['value'])){
                                $array['org_search'][$org_search]['label'] =  'Paid';
                                $array['org_search'][$org_search]['value'] =  $data['value'];
                                $org_search++;
                            }
                        endif;

                        if( $k == 'org_social' ):
                            $data = @unserialize($v);
                            foreach ($data['top_socials'] as $keys => $values) {
                                $array['org_social'][$org_social]['label'] =  @$values['site'];
                                $array['org_social'][$org_social]['value'] =  @$values['value'];
                                $org_social++;
                            }
                        endif;
                    endforeach;
                endif;
            endforeach;
           
            $selectQueryUserList->withPath('?' . $offset);
            
            return view('search.websites',compact('selectQueryWebsite','selectQueryUserListArray','selectQueryUserList','domain_url','array','follow'));    
        } else {
            $alert = 'warning';
            $message = 'Data not found';
            return view('viewmessage-cart',compact('alert','message'));
        }
        

    }

    public function pricing_select($domain_url,$id) {

        $id = secureDecrypt($id);
        $list = MstDomainUrl::where('domain_id',$id)->select('cost_price','extra_cost','domain_id','domain_url','guest_content','promo_code_content','promo_code_website','number_words')->where('domain_approval_status','=',1)->where('cost_price','>',0)->where('trash','<',1)->groupBy('domain_fk_user_id')->first();
        return view('search.websites_pricing',compact('list','domain_url'));
    }    
    public function search_result(){
        
        $Name = $_POST['search'];
        $results = DB::select( DB::raw("SELECT DISTINCT query_string as 'name' FROM log_searchquery WHERE query_string LIKE '$Name%' LIMIT 500") );

        $html = '<ul>';
        foreach( $results as $get_results ){
            $result = preg_replace('/[^a-zA-Z0-9_ -]/s','',$get_results->name);
            $html .= '<li class="ajax-display" rel="'.$result.'"><a>'.preg_replace('/[^a-zA-Z0-9_ -]/s','',$result).'</a></li>';
        }
        $html .= '</ul>';
        return $html;
    }
    public function wish_cart_call($data, $wish_cart){

        $store = [];
        $get = [];
        foreach( $data as $key => $value ) {
            if( Cache::has( $wish_cart ) ) {
              $get = Cache::get( $wish_cart );
              $get = @unserialize($get);
              $count = count($get);
              foreach ($get as $k => $v) {                  
                  $store[] = $v;
              }
            }
            if(!in_array($value, $get)){
                $store[] = $value;
            }

            // if(Auth::check()){
            //     if($wish_cart == 'wishlist'){                    
            //         $record_exists   = DB::table('wishlist_tbl')
            //                        ->where('domain_id',$value)
            //                        ->where('user_id', Auth::user()->user_id)->value('wishlist_id');
            //         if(empty($record_exists)) {
            //             $domain_url = DB::table('mst_domain_url')->where('domain_id',$value)->value('domain_url');
            //             $data = array('user_id'=> Auth::user()->user_id, 'domain_id'=> $value,'domain_url'=>$domain_url);
            //             DB::table('wishlist_tbl')->insert($data);
            //         }
            //     }
            //     if($wish_cart == 'cartlist') {

            //         $record_exists   = DB::table('add_to_carts')
            //                            ->where('domain_id',$value)
            //                            ->where('is_billed',0)
            //                            ->where('user_id', Auth::user()->user_id)->value('id');
            //         if(empty($record_exists)) {
            //             $domain_url = DB::table('mst_domain_url')->where('domain_id',$value)->value('domain_url');
            //             $domain_user_id = DB::table('mst_domain_url')->where('domain_id',$value)->value('domain_fk_user_id');
            //             $data = array('user_id'=> Auth::user()->user_id, 'domain_id'=> $value,'fk_domain_url'=>$domain_url,'domain_user_id' => $domain_user_id ,'content_type' => 'seller','is_billed' => 0);
            //             DB::table('add_to_carts')->insert($data);
            //         }
            //     }
            // }
        }
        $store = serialize($store);
        Cache::forever($wish_cart, $store);

        if( Cache::has( $wish_cart ) ) {
            return true;
        }
        Cache::forget($wish_cart);
        return true;
    }

    public function wish_cart_call_delete($data, $wish_cart){

        $store = [];
        $get = [];
        foreach( $data as $key => $value ) {
            if( Cache::has( $wish_cart ) ) {
              $get = Cache::get( $wish_cart );
              $get = @unserialize($get);
              $count = count($get);
              $key_unset = $count + 2;
              foreach ($get as $k => $v) {
                    if($v == $value){
                        $key_unset = $k;
                    }
                $store[] = $v;
              }
            }
            if(!in_array($value, $get)){
                $store[] = $value;
            } else {
                unset($store[$key_unset]);
            }

            if(Auth::check()){
                if($wish_cart == 'wishlist'){                    
                    $record_exists   = DB::table('wishlists')
                                       ->where('domain_id',$value)
                                       ->where('user_id', Auth::user()->user_id)->value('wishlist_id');
                    if(empty($record_exists)) {
                        $domain_url = DB::table('mst_domain_url')->where('domain_id',$value)->value('domain_url');
                        $data = array('user_id'=> Auth::user()->user_id, 'domain_id'=> $value,'domain_url'=>$domain_url);
                        DB::table('wishlists')->insert($data);
                    } else {
                        DB::table('wishlists')->where('wishlist_id',$record_exists)->delete();
                    }
                    
                }
                if($wish_cart == 'cartlist'){
                    $record_exists   = DB::table('add_to_carts')
                                   ->where('domain_id',$value)
                                   ->where('is_billed',0)
                                   ->where('user_id', Auth::user()->user_id)->value('id');
                    
                    if(empty($record_exists)) {
                        $domain_url = DB::table('mst_domain_url')->where('domain_id',$value)->value('domain_url');
                        $domain_user_id = DB::table('mst_domain_url')->where('domain_id',$value)->value('domain_fk_user_id');
                        $data = array('user_id'=> Auth::user()->user_id, 'domain_id'=> $value,'fk_domain_url'=>$domain_url,'domain_user_id' => $domain_user_id ,'content_type' => 'buyer','is_billed' => 0);
                        DB::table('add_to_carts')->insert($data);
                    } else {
                        DB::table('add_to_carts')->where('id',$record_exists)->delete();
                    }
                }
            }
        }
        $store = serialize($store);
        Cache::forever($wish_cart, $store);

        if( Cache::has( $wish_cart ) ) {
            return true;
        }
        
        Cache::forget($wish_cart);
        return true;
    }
    // from search
    public function addToWishlist(){        

        $data = $_POST;
        $this->wish_cart_call_delete($data, 'wishlist');
        //$this->wish_cart_call_delete($data, 'cartlist');

        $response_wish = view('layout.include.partials.wish')->render();
        $response_cart = view('layout.include.partials.cart')->render();
        return ['alert'=>'success','wish'=>$response_wish, 'cart' => $response_cart ];

    }

    public function addToCartList() {

        $data = $_POST;
        $this->wish_cart_call($data, 'cartlist');
        $this->wish_cart_call_delete($data, 'wishlist');

        $response_wish = view('layout.include.partials.wish')->render();
        $response_cart = view('layout.include.partials.cart')->render();
        return ['alert'=>'success','wish'=>$response_wish, 'cart' => $response_cart ];

    }
    public function addToCartListDelete() {
        
        $data = $_POST;
        $this->wish_cart_call_delete($data, 'cartlist');
        $response_wish = view('layout.include.partials.wish')->render();
        $response_cart = view('layout.include.partials.cart')->render();
        return ['alert'=>'success','wish'=>$response_wish, 'cart' => $response_cart ];

    }
    public function addToWishListDelete() {
        
        $data = $_POST;
        $this->wish_cart_call_delete($data, 'wishlist');
        $response_wish = view('layout.include.partials.wish')->render();
        $response_cart = view('layout.include.partials.cart')->render();
        return ['alert'=>'success','wish'=>$response_wish, 'cart' => $response_cart ];

    }
    public function addWishMoveIntoCart() {
        $get = [];
        if( Cache::has( 'wishlist' ) ) {
            $get = Cache::get( 'wishlist' );
            $get = @unserialize($get);            
        }

        $this->wish_cart_call_delete($get, 'cartlist');
        //$this->wish_cart_call_delete($get, 'wishlist');
        Cache::forget('wishlist');
        $response_wish = view('layout.include.partials.wish')->render();
        $response_cart = view('layout.include.partials.cart')->render();
        return ['alert'=>'success','wish'=>$response_wish, 'cart' => $response_cart ];
    }
    // Function to convert CSV into associative array
    function csvToArray($file, $delimiter) { 
        if (($handle = fopen($file, 'r')) !== FALSE) { 
            $i = 0; 
            while (($lineArray = fgetcsv($handle, 4000, $delimiter, '"')) !== FALSE) { 
                for ($j = 0; $j < count($lineArray); $j++) {
                    $arr[$i][$j] = $lineArray[$j]; 
                } 
                $i++; 
            } 
            fclose($handle); 
        } 
        return $arr; 
    }
    function feed(){
        $feed = 'https://docs.google.com/spreadsheets/d/e/2PACX-1vQo4jW3YcFICzxVeEusxTChXWdm9DVpiVd7lx33g3x56CrXPRt3UuXo_es0oZF9OpzCmrzfebYEMF_D/pub?gid=355214615&single=true&output=csv';
        // Do it
        $data = $this->csvToArray($feed, ',');
        return $data;
    }
    public function category(){
        $data = $this->feed();
        $category = [];
        foreach ($data as $key => $value) {
            if(empty(trim($value[0])) || $key == 0){
                continue;
            }
            $category[$value[0]]['category'] = $value[0];
            $category[$value[0]]['url'] = url('/search/category').'/'.fixForUri($value[0]); 
        }
        return view('search.category-list',compact('category'));
    }
    public function search_category($url_cat){
        $data = $this->feed();
        $category = [];
        $count = 0;
        $category_name = '';
        foreach ($data as $key => $value) {
            if(empty(trim($value[0])) || $key == 0){
                continue;
            }
            if(fixForUri($value[0]) == $url_cat) {
                $category_name = $value[0];
                if(!empty(trim($value[1]))){
                    $category[$count]['category'] = $value[1];
                    $category[$count]['url'] = url('/search?q=').$value[1];
                    $count++;
                }
            }

        }
        return view('search.category-list',compact('category','category_name'));
    }
}
