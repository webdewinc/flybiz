<?php
namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\DB;
use App\SearchRecord;
use App\RestClient;
use App\MstDomainUrl;
use App\MstSeoData;
use App\CnfDomainCountry;
use App\MstCountry;
use App\MstOrganicSearch;
use App\MstOrgExtraField;
use App\MstSimilarSite;
use App\MstDaPa;
use App\User;
use App\Chat;

class APIController extends Controller
{
    
    public $accessID;
    public $secretKey;
    public $email;
    public $password;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest');
        // randeep Moz Credentials        
        $credentials['sandbox'] = ['accessID' => "mozscape-d6fcd3688f", 'secretKey' => "bd85c5277c448c376d93a882efa293ad"];
        // Danish sir Moz credentials

        $credentials['live'] = ['accessID' => "mozscape-7487c27b2d", 'secretKey' => "cad5a844cd3729bb10faf0c94302c88"];

        // change account
        $data = 'live';

        $accessID = $credentials[$data]['accessID'];
        $secretKey = $credentials[$data]['secretKey'];

        $email = 'danish@webdew.com';
        $password = 'UNUtNQxuvC1bFfK5';

        $this->hapikey = '2a79d16f-2032-4dde-a1a0-b5d4f4f22610';

    }
    /*live function start*/
    public function dataForSeo($url = 'webdew.com')
    {
        $url = str_replace('/', '', $url);
        try {
            
            $client = new RestClient('https://api.dataforseo.com/', null, 'danish@webdew.com', 'UNUtNQxuvC1bFfK5');
            $cmp_get_result = $client->get('v2/cmp_get/'.$url);            
            return $cmp_get_result;

        } catch (RestClientException $e) {
            echo "\n";
            print "HTTP code: {$e->getHttpCode()}\n";
            print "Error code: {$e->getCode()}\n";
            print "Message: {$e->getMessage()}\n";
            print  $e->getTraceAsString();
            echo "\n";
            exit();
        }
        
        return $cmp_get_result;
    }

    public function dataForMoz($url = 'webdew.com')
    {

        $url = str_replace('/', '', $url);
        
        try {       
                $accessID = "mozscape-7487c27b2d";
                $secretKey = "cad5a844cd3729bb10faf0c94302c88";
                $expires = time() + 300;
                $stringToSign = $accessID."\n".$expires;
                $binarySignature = hash_hmac('sha1', $stringToSign, $secretKey, true);
                $urlSafeSignature = urlencode(base64_encode($binarySignature));

                $total = 0;
                $array['uu']  = 4;
                $array['feid'] = 64;
                $array['peid'] = 128;
                $array['uid'] = 2048;
                $array['pid'] = 8192;
                $array['us'] = 536870912;
                $array['puid'] = 8589934592;
                $array['upa'] = 34359738368;
                $array['pda'] = 68719476736;
                $array['ulc'] = 144115188075855872;

                $total = $array['uu'] + $array['feid'] + $array['peid'] + $array['uid'] + $array['pid']  +  $array['us'] + $array['puid'] + $array['upa'] + $array['pda'] + $array['ulc'];
                  
                $cols = $total;
                $requestUrl = "http://lsapi.seomoz.com/linkscape/url-metrics/".urlencode($url)."?Cols=".$cols."&AccessID=".$accessID."&Expires=".$expires."&Signature=".$urlSafeSignature;
                // Use Curl to send off your request.
                $options = array(
                CURLOPT_RETURNTRANSFER => true
                );
                $ch = curl_init($requestUrl);
                curl_setopt_array($ch, $options);
                $content = curl_exec($ch);
                $data = json_decode($content);
              
                $arra = [];
                $main = [];
                if(!empty($data)){
                    $arra['uu']  = @$data->uu;
                    $arra['uid'] = @$data->uid;
                    $arra['us']  = @$data->us;
                    $arra['upa'] = @$data->upa;
                    $arra['pda'] = @$data->pda;
                    $arra['ulc'] = @$data->ulc;

                    $main['upa'] = @$arra['upa'];
                    $main['pda'] = @$arra['pda'];
                    $main['all_moz'] = serialize($arra);
                }                
                return $main;
        } 
        
        catch (RestClientException $e) 
        {
            echo "\n";
            print "HTTP code: {$e->getHttpCode()}\n";
            print "Error code: {$e->getCode()}\n";
            print "Message: {$e->getMessage()}\n";
            print  $e->getTraceAsString();
            echo "\n";
            exit();
        }

        return $data;
    }

    function webdewNET($limit = 10, $offset_id = 1) {

        //\Session::flush();

        $offset = DB::table('webdew_api')->where('id',$offset_id)->value('offset');        
        
        if(!empty($offset)){

            $args['url'] = "https://api.hubapi.com/companies/v2/companies/paged?properties=website&properties=moz_update_date&properties=domain_authority&offset=".$offset."&limit=".$limit."&hapikey=026d9a52-cf29-49bb-a73a-9a28261d32ce";

        } else {
            $args['url'] = "https://api.hubapi.com/companies/v2/companies/paged?properties=website&properties=moz_update_date&properties=domain_authority&limit=".$limit."&hapikey=026d9a52-cf29-49bb-a73a-9a28261d32ce";
        }

        $result = $this->curlAccess( 'GET', $args );
        $has_more = json_decode(json_encode($result),true);

        if($has_more['has-more'] == true){
            $offset = DB::table('webdew_api')->where('id',$offset_id)->update(['offset'=>$has_more['offset']]);
        } else {
            $offset = DB::table('webdew_api')->where('id',$offset_id)->update(['offset' => NULL ]);
        }

        foreach ($result->companies as $key => $value) : 

                $domain_url = $value->properties->website->value;
                $moz_result = $this->dataForMoz($domain_url);

                if($moz_result){

                    $wd_id = DB::table('webdew_domain_api')->where('domain_url',$domain_url)->value('id');
                    if(empty($id)){
                        $wd_id = DB::table('webdew_domain_api')->insertGetId(['domain_url'=>$domain_url,'status' => 1,'moz_data' => $moz_result['all_moz']]);
                    } else {
                        DB::table('webdew_domain_api')->where('id',$wd_id)->update([
                                    'domain_url'=> $domain_url,
                                    'status' => 1,
                                    'moz_data' => $moz_result['all_moz']
                                ]);
                    }

                    $all_moz = unserialize($moz_result['all_moz']);
                    
                    $da = @$moz_result['pda'];
                    $pa = @$moz_result['upa'];
                    $uid = @$all_moz['uid'];//Title
                    $uu = @$all_moz['uu'];//Canonical URL
                    $us = @$all_moz['us'];//HTTP Status Code


                    $companyId = $value->companyId;
                    $args['data'] = '{
                                  "properties": [
                                    {
                                      "name": "domain_authority",
                                      "value": "'. $da .'"
                                    },
                                    {
                                      "name": "page_authority",
                                      "value": "'.$pa.'"
                                    },
                                    {
                                      "name": "moz_update_date",
                                      "value": "'.time().'000"
                                    },
                                    {
                                      "name": "canonical_url",
                                      "value": "'.$uu.'"
                                    },
                                    {
                                      "name": "http_status_code",
                                      "value": "'.$us.'"
                                    },
                                    {
                                      "name": "moz_title",
                                      "value": "'.$uid.'"
                                    }
                                    
                                  ]
                                }';

                    $args['url'] = "https://api.hubapi.com/companies/v2/companies/".$companyId."?hapikey=026d9a52-cf29-49bb-a73a-9a28261d32ce";
                    $result = $this->curlAccess( 'PUT', $args );
                    echo $domain_url;
                }
          //      $limit++;
        endforeach;

        //$limit = DB::table('webdew_api')->where('id',1)->update(['limit' => $limit]);

    }
   
   

    /*live function end*/
    /*dummy function start*/
    // public function dataMOZ($url){

    //     $arra['uu'] = 'guestpostengine.com';
    //     $arra['uid'] = 33;
    //     $arra['us'] = 301;
    //     $arra['upa'] = 18;
    //     $arra['pda'] = 11;
    //     $arra['ulc'] = 1575590400;

    //     $main['upa'] = 40;
    //     $main['pda'] = 40;
    //     $main['all_moz'] = serialize($arra);
    //     return $main;

    // }

    // public function dataseo($url) {

    //     $main['status'] = 'ok';
    //     $main['results_time'] = '23.0285 sec.';
    //     $main['results_count'] = 1;

                    
    //     $main['results'][0]['task_id'] = 5483223509;
    //     $main['results'][0]['date'] = '2019-12-01';
    //     $main['results'][0]['site_url'] = 'webdew.com';
    //     $main['results'][0]['site_description'] = 'hey! we are webdew, inbound marketing and hubspot partner agency based out in india, where 50+ agency members are breathing hubspot in and out.';
    //     $main['results'][0]['global_rank']['rank'] = 3185623;

    //     $main['results'][0]['country_rank']['rank'] = 244238;
    //     $main['results'][0]['country_rank']['country'] = 'India';
    //     $main['results'][0]['category_rank']['rank'] = 0;
    //     $main['results'][0]['category_rank']['category'] = 'Unknown';


    //     $main['results'][0]['audience']['visits'] = 2078;
    //     $main['results'][0]['audience']['time_on_site_avg'] = '00:05:41';
    //     $main['results'][0]['audience']['page_views_avg'] = 3.1246390760346;
    //     $main['results'][0]['audience']['bounce_rate'] = 64.29;


    //     $main['results'][0]['traffic']['value'] = 2078;
    //     $main['results'][0]['traffic']['percent'] = 100;
    //     $main['results'][0]['traffic']['countries'][0]['country'] = 'Other';
    //     $main['results'][0]['traffic']['countries'][0]['value'] = 2078;
    //     $main['results'][0]['traffic']['countries'][0]['percent'] = 100;

                
                        
    //     $main['results'][0]['traffic']['sources']['direct']['value'] = 828;
    //     $main['results'][0]['traffic']['sources']['direct']['percent'] = 39.89;


    //     $main['results'][0]['traffic']['sources']['search_organic']['value'] = 1102;
    //     $main['results'][0]['traffic']['sources']['search_organic']['percent'] = 53.03;


    //     $main['results'][0]['traffic']['sources']['search_organic']['top_keywords'][0]['keyword'] = 'how to market your resume as a fresher';
    //     $main['results'][0]['traffic']['sources']['search_organic']['top_keywords'][0]['value'] = 131;
    //     $main['results'][0]['traffic']['sources']['search_organic']['top_keywords'][0]['percent'] = 11.91;


    //     $main['results'][0]['traffic']['sources']['search_organic']['top_keywords'][1]['keyword'] = 'best whiteboard video software';
    //     $main['results'][0]['traffic']['sources']['search_organic']['top_keywords'][1]['value'] = 107;
    //     $main['results'][0]['traffic']['sources']['search_organic']['top_keywords'][1]['percent'] = 9.78;



    //     $main['results'][0]['traffic']['sources']['search_organic']['top_keywords'][2]['keyword'] = 'downward trend 3d animation';
    //     $main['results'][0]['traffic']['sources']['search_organic']['top_keywords'][2]['value'] = 103;
    //     $main['results'][0]['traffic']['sources']['search_organic']['top_keywords'][2]['percent'] = 9.4;



    //     $main['results'][0]['traffic']['sources']['search_organic']['top_keywords'][3]['keyword'] = 'digital marketing fresher resume sample';
    //     $main['results'][0]['traffic']['sources']['search_organic']['top_keywords'][3]['value'] = 81;
    //     $main['results'][0]['traffic']['sources']['search_organic']['top_keywords'][3]['percent'] = 7.36;


    //     $main['results'][0]['traffic']['sources']['search_organic']['top_keywords'][4]['keyword'] = 'webdew';
    //     $main['results'][0]['traffic']['sources']['search_organic']['top_keywords'][4]['value'] = 79;
    //     $main['results'][0]['traffic']['sources']['search_organic']['top_keywords'][4]['percent'] = 7.25;


    //     $main['results'][0]['traffic']['sources']['search_ad']['value'] = 56;
    //     $main['results'][0]['traffic']['sources']['search_ad']['percent'] = 2.69;

                                         
    //     $main['results'][0]['traffic']['sources']['search_ad']['top_keywords'][0]['keyword'] = 'sketch animation program';
    //     $main['results'][0]['traffic']['sources']['search_ad']['top_keywords'][0]['value'] = 3;
    //     $main['results'][0]['traffic']['sources']['search_ad']['top_keywords'][0]['percent'] = 6.03;
                                         

    //     $main['results'][0]['traffic']['sources']['search_ad']['top_keywords'][1] ['keyword'] = 'explainer video';
    //     $main['results'][0]['traffic']['sources']['search_ad']['top_keywords'][1] ['value'] = 0;
    //     $main['results'][0]['traffic']['sources']['search_ad']['top_keywords'][1] ['percent'] = 0;
                                            


                        
    //     $main['results'][0]['traffic']['sources']['referral']['value'] = 40;
    //     $main['results'][0]['traffic']['sources']['referral']['percent'] = 1.93;
    //     $main['results'][0]['traffic']['sources']['referral']['top_referrals'] = array();

                        
    //     $main['results'][0]['traffic']['sources']['referral_ad']['value'] = 0;
    //     $main['results'][0]['traffic']['sources']['referral_ad']['percent'] = 0;
    //     $main['results'][0]['traffic']['sources']['referral_ad']['top_referrals'] = array();


    //     $main['results'][0]['traffic']['sources']['social']['value'] = 49;
    //     $main['results'][0]['traffic']['sources']['social']['percent'] = 2.38;
                        
    //     $main['results'][0]['traffic']['sources']['social']['top_socials'][0]['site'] = 'Facebook';
    //     $main['results'][0]['traffic']['sources']['social']['top_socials'][0]['value'] = 49;
    //     $main['results'][0]['traffic']['sources']['social']['top_socials'][0]['percent'] = 100;



    //     $main['results'][0]['traffic']['sources']['mobile_apps']['google_play_store'] = array();

    //     $main['results'][0]['traffic']['sources']['mobile_apps']['app_store']=array();


    //     $main['results'][0]['traffic']['sources']['mail']['value'] = 0;
    //     $main['results'][0]['traffic']['sources']['mail']['percent'] = 0;
                       
                
                
    //     $main['results'][0]['traffic']['estimated']['2019-07-01'] = 6082;
    //     $main['results'][0]['traffic']['estimated']['2019-08-01'] = 7218;
    //     $main['results'][0]['traffic']['estimated']['2019-09-01'] = 6506;
    //     $main['results'][0]['traffic']['estimated']['2019-10-01'] = 5878;
    //     $main['results'][0]['traffic']['estimated']['2019-11-01'] = 2220;
    //     $main['results'][0]['traffic']['estimated']['2019-12-01'] = 2078;
                
    //     $main['results'][0]['sites']['similar_sites']=array();
    //     $main['results'][0]['sites']['similar_sites_by_rank']=array();
    //     return $main;
    // }
    /*dummy function end*/


    function insertDAPA($domain_id, $arry) {

        $dp_id = MstDaPa::where('dp_fk_domain',$domain_id)->value('dp_id');

        $data = [
                    'dp_fk_domain' => $domain_id,
                    'dp_da' => $arry['pda'] ,
                    'dp_pa' => $arry['upa'] ,
                    'all_moz' => $arry['all_moz'],
                    'dp_followers' => 1,
                    'created_date' => date('Y-m-d')
                ];

        if(empty($dp_id)){
            
            $dp_id = DB::table('mst_da_pa')->insertGetId($data);
        } else {

            $dp_id = DB::table('mst_da_pa')->where('dp_id',$dp_id)->update($data);
            
        }
        return true;
    }

    function insertSEOdata($array = []){

        $sd_id = MstSeoData::where('sd_fk_domain',$array['domain_id'])->value('sd_id');

        if (empty($sd_id)) {

            DB::table('mst_seo_data')->insert($array['seo']);

        } else {
            DB::table('mst_seo_data')->where('sd_fk_domain',$array['seo']['sd_fk_domain'])->update($array['seo']);
        }

        return true;
    }

    function insertDomainCountries($domain_id, $country) {
      
        $country_id = MstCountry::where('country_name',$country['country'])->value('country_id');

        if(empty($country_id)){
            $country_id = DB::table('mst_country')->insertGetId(['country_name' => $country['country']]);
        }

        $domaincountry_id = CnfDomainCountry::where('dc_fk_domain',$domain_id)->where('dc_fk_country',$country_id)->value('dc_id');

        if(empty($domaincountry_id)) {
            $insert_query = DB::table('cnf_domain_country')->insert(['dc_fk_domain' => $domain_id,'dc_fk_country' => $country_id,'cnf_value' => $country['value'], 'cnf_percent' => $country['percent']]);
        } else {

            $update_query = DB::table('cnf_domain_country')->where('dc_id',$domaincountry_id)->update([
                'dc_fk_domain' => $domain_id,
                'dc_fk_country' => $country_id,
                'cnf_value' => $country['value'], 
                'cnf_percent' => $country['percent']
                ]);
        }
        return true;
    }

    function insertOrganicSearch($domain_id, $keywordArray) {

        $direct = @$keywordArray['direct'];
        $search_ad = @$keywordArray['search_ad'];
        $referral = @$keywordArray['referral'];
        $referral_ad = @$keywordArray['referral_ad'];
        $social = @$keywordArray['social'];
        $mobile_apps = @$keywordArray['mobile_apps'];
        $mail = @$keywordArray['mail'];
        $value = @$keywordArray['search_organic_value'];
        $percent = @$keywordArray['search_organic_percent'];
        $estimated = @$keywordArray['estimated'];
        $search_organic_serial = @$keywordArray['search_organic_serial'];

        $update = [
                    'org_direct' => $direct,
                    'org_search_ad' => $search_ad,
                    'org_referral' => $referral,
                    'org_referral_ad' => $referral_ad,
                    'org_mobile_apps' => $mobile_apps,
                    'org_mail' => $mail,
                    'org_main_value' => $value,
                    'org_main_percent' => $percent,
                    'org_social' => $social,
                    'fk_org_extra_domain_id' => $domain_id,
                    'org_search_organic_serial' => $search_organic_serial,
                    'org_estimated' => $estimated
                ];

        $mst_org_extra_id  = MstOrgExtraField::where('fk_org_extra_domain_id',$domain_id)->value('mst_org_extra_id');

        if(empty($mst_org_extra_id)){
            $created_domain  = MstOrgExtraField::create($update);
        } else {
            $updated_domain  = MstOrgExtraField::where('mst_org_extra_id',$mst_org_extra_id)->update($update);            
        }

        if(!empty($keywordArray['search_organic'])){
            foreach ($keywordArray['search_organic'] as $key => $value) {

                $orgsearch_id = MstOrganicSearch::where('org_fk_domain',$domain_id)->where('org_keyword',$value['keyword'])->value('org_id');                

                if(empty($orgsearch_id)) {
                    
                    $orgsearch_id = DB::table('mst_organic_search')->insert(['org_fk_domain' => $domain_id,'org_keyword' => $value['keyword'] , 'org_value' => $value['value'], 'org_percent' => $value['percent']]);
                } else {

                    $orgsearch_id = DB::table('mst_organic_search')->where('org_id',$orgsearch_id)->update(['org_fk_domain' => $domain_id,'org_keyword' => $value['keyword'] , 'org_value' => $value['value'], 'org_percent' => $value['percent']]);
                }
            }
        }
        
        return true;
      
    }
    function insertSimilarSites($domain_id, $site_url, $rank) {

        $orgsearch_id = MstSimilarSite::where('smsite_fk_domain',$domain_id)->where('smsite_url',$site_url)->value('smsite_id');      

        if(empty($orgsearch_id)){
            
            $insert =   [
                            'smsite_fk_domain' => $domain_id,
                            'smsite_url' => $site_url,
                            'smsite_rank' => $rank
                        ];
            $insert_query = DB::table('mst_similar_site')->insert($insert);
        } else {
            $insert =   [
                            'smsite_fk_domain' => $domain_id,
                            'smsite_url' => $site_url,
                            'smsite_rank' => $rank
                        ];
            $orgsearch_id = DB::table('mst_similar_site')->where('smsite_id',$orgsearch_id)->update($insert);
        }
        return true;
    }
    function insertAssigned($domain_id, $user_id) {

        $id = \App\AssignedWebsite::where('mst_domain_url_id',$domain_id)->where('user_id',$user_id)->value('id');

        if(empty($id)){
            
            $insert =   [
                            'mst_domain_url_id' => $domain_id,
                            'user_id' => $user_id,
                        ];
            $insert_query = DB::table('assigned_websites')->insert($insert);
        } else {
            $insert =   [
                            'mst_domain_url_id' => $domain_id,
                            'user_id' => $user_id,
                        ];
            $insert_query = DB::table('assigned_websites')->where('id',$id)->update($insert);
            
        }
        return true;
    }
    function existSEOData($url){

        $list = MstDomainUrl::wherehas('data_seo')->with('data_seo')->where('domain_url',$url)->first();        
        $list = json_decode(json_encode($list), true);
        if(empty($list)) {

            $list = MstDomainUrl::doesntHave('data_seo')->with('data_seo')->where('domain_url',$url)->first();
            $list = json_decode(json_encode($list), true);
            if(empty($list)) {                
                $list = MstDomainUrl::where('domain_url',$url)->first();
                $list = json_decode(json_encode($list), true);
            }
        }
        return $list;
    }

    function domain_update(){

        $assigned_domain_list =  \App\AssignedWebsite::get();    
        foreach ($assigned_domain_list as $key => $value) {
            $list = MstDomainUrl::where('domain_id',$value->mst_domain_url_id)->update(['domain_fk_user_id'=>$value->user_id]);
        }
        return $list;
    }

    // public function call_API_MOZ() {

    //     //$this->domain_update();

    //     $no_of_page = 50;
    //     $offset = '';
    //     if(isset($_REQUEST['page'])){
    //         $offset = 'page=' . $_REQUEST['page'];
    //     }
    //     $list = MstDaPa::where('all_moz','=',NULL)->orderBy('dp_id','DESC')->paginate($no_of_page);
    //     $list->withPath('?' . $offset);

    //     if($list->total() < 1){
    //         $list = MstDaPa::where('dp_id','>=',14457)->where('all_moz','=','')->orderBy('dp_id','DESC')->paginate($no_of_page);
        
    //         $list->withPath('?' . $offset);            
    //     }

    //     $a = [];
    //     $domain_id = '';
    //     foreach ($list as $pkey => $pval) :
                
    //             $domain_id = $pval->dp_fk_domain;
    //             $domain_url = MstDomainUrl::where('domain_id',$domain_id)->value('domain_url');

    //             if(!empty($domain_url)){
    //                 $moz_result = $this->dataForMoz($domain_url);
    //                 if(!empty($moz_result)) :
    //                     $this->insertDAPA($domain_id, $moz_result);
    //                 endif;
    //                 /* update and get */
    //                 $get = MstDaPa::where('dp_id',$pval->dp_id)->first();
    //             }
    //     endforeach;

        
    // }

    /*main function start*/
    public function call_API() {

        //$this->domain_update();

        $no_of_page = 1;
        $offset = '';
        if(isset($_REQUEST['page'])){
            $offset = 'page=' . $_REQUEST['page'];
        }
        $list = MstDomainUrl::wherehas('data_assigned_website')->where('cost_price','>',0)->where('status_update', NULL)->paginate($no_of_page);
        
        $list->withPath('?' . $offset);

        $a = [];
        $domain_id = '';
        foreach ($list as $pkey => $pval) :
            $data_list = [];
            $domain_data = $this->existSEOData($pval->domain_url);
            if(!empty($domain_data)):
                $data_list['domain_id'] = $domain_data['domain_id']; 
                $domain_id = $domain_data['domain_id'];
                $domain_url = $domain_data['domain_url'];

                /* Sandbox start*/

                    //$moz_result = $this->dataMOZ($domain_url);

                /* Sandbox end*/

                /* Production start*/

                    // $assigned = $this->insertAssigned($pval['domain_id'],$pval['domain_fk_user_id']);
                    // continue;

                    //$moz_result = $this->dataForMoz($domain_url);


                    /* get data from moz */
                    $da_result = dataForRapidMoz($domain_url);
                    /* get data from moz*/
                    $array['pda'] = 0;
                    $array['upa'] = 0;
                    $array['all_moz'] = $moz;
                    if(!empty($da_result['data'][0])){
                        foreach ($da_result['data'][0] as $key => $value) {
                          $moz = @serialize($value);
                          $array['all_moz'] = $moz;
                          $array['pda'] = @$value['domain_authority'];
                          $array['upa'] = @$value['page_authority'];
                        }
                        /* insert data */
                            $this->insertDAPA($domain_id, $array);
                        /* insert data */
                    }
                    


                /* Production End*/

                // if(!empty($moz_result)) :
                //     $this->insertDAPA($data_list['domain_id'], $moz_result);
                // endif;         

                /* Sandbox start*/

                    //$seo_result = $this->dataseo($domain_url);

                /* Sandbox end*/

                /* Production start*/
                
                $seo_result = $this->dataForSeo($domain_url);
                $a['seo_result'] = $seo_result;
                /* Production End*/
                if($seo_result['status'] == 'ok'):
                    if(!empty($seo_result['results'])) : 
                        $a[$data_list['domain_id']] = $data_list['domain_id']; 
                        $a[$data_list['domain_id']] = $domain_url;
                        foreach ($seo_result['results'] as $ckey => $cval) :

                              $data_list['seo']['sd_visits'] = @$cval['audience']['visits'];
                              $data_list['seo']['sd_page_views_avg'] = @$cval['audience']['page_views_avg'];
                              $data_list['seo']['sd_time_on_site_avg'] = @$cval['audience']['time_on_site_avg'];

                              $data_list['seo']['sd_bounce_rate'] = @$cval['audience']['bounce_rate'];
                              $data_list['seo']['sd_title_n'] = @$cval['site_title'];
                              $data_list['seo']['sd_descr_n'] = @$cval['site_description'];
                              $data_list['seo']['sd_percent'] = @$cval['traffic']['percent'];
                              $data_list['seo']['sd_fk_domain'] = $data_list['domain_id'];
                              $data_list['seo']['sd_global_rank'] = serialize(@$cval['global_rank']);
                              $data_list['seo']['sd_country_rank'] = serialize(@$cval['country_rank']);
                              $data_list['seo']['sd_category_rank'] = serialize(@$cval['category_rank']);
                              $data_list['seo']['sd_traffic_main_value'] = @$cval['traffic']['value'];
                              $data_list['seo']['sd_traffic_main_percent'] = @$cval['traffic']['percent'];

                              $this->insertSEOdata($data_list);
                            
                                if(!empty($cval['traffic']['countries'])) :
                                    foreach ($cval['traffic']['countries'] as $ctykey => $ctyvalue) :
                                        $this->insertDomainCountries($data_list['domain_id'], $ctyvalue);
                                    endforeach;
                                endif;
                                

                                if(!empty($cval['traffic']["sources"])) :

                                    if(!empty($cval['traffic']["sources"]['direct'])) :
                                        $data_list['direct'] = serialize($cval['traffic']["sources"]['direct']);
                                    endif;
                                    if(!empty($cval['traffic']["sources"]['search_ad'])) :
                                        $data_list['search_ad'] = serialize($cval['traffic']["sources"]['search_ad']);
                                    endif;
                                    if(!empty($cval['traffic']["sources"]['referral'])) :
                                        $data_list['referral'] = serialize($cval['traffic']["sources"]['referral']);
                                    endif;
                                    if(!empty($cval['traffic']["sources"]['referral_ad'])) :
                                        $data_list['referral_ad'] = serialize($cval['traffic']["sources"]['referral_ad']);
                                    endif;
                                    if(!empty($cval['traffic']["sources"]['social'])) :
                                        $data_list['social'] = serialize($cval['traffic']["sources"]['social']);
                                    endif;
                                    
                                    if(!empty($cval['traffic']["sources"]['estimated'])) :
                                        $data_list['estimated'] = serialize($cval['traffic']["sources"]['estimated']);
                                    endif;

                                    if(!empty($cval['traffic']["sources"]['mobile_apps'])) :
                                        $data_list['mobile_apps'] = serialize($cval['traffic']["sources"]['mobile_apps']);
                                    endif;
                                    if(!empty($cval['traffic']["sources"]['mail'])) :
                                        $data_list['mail'] = serialize($cval['traffic']["sources"]['mail']);
                                    endif;

                                    if(!empty($cval['traffic']["sources"]['search_organic']['top_keywords'])) :
                                        $data_list['search_organic'] = $cval['traffic']["sources"]["search_organic"]['top_keywords'];
                                        $data_list['search_organic_serial'] = serialize($cval['traffic']["sources"]["search_organic"]['top_keywords']);
                                    endif;

                                    if(!empty($cval['traffic']["sources"]['search_organic']['value'])) :
                                        $data_list['search_organic_value'] = $cval['traffic']["sources"]["search_organic"]['value'];
                                    endif;

                                    if(!empty($cval['traffic']["sources"]['search_organic']['percent'])) :
                                        $data_list['search_organic_percent'] = $cval['traffic']["sources"]["search_organic"]['percent'];
                                    endif;
                                    $this->insertOrganicSearch($data_list['domain_id'], $data_list);
                                endif;
                              
                              
                                if(!empty($cval["sites"])) :
                                    foreach ($cval["sites"] as $skey => $sval) :
                                        foreach ($sval as $key => $val) :
                                            $rank = 0;
                                            if(!empty($val["rank"])){
                                                $rank = $val["rank"];
                                            }
                                            if(!empty($val["site"]) ) :
                                                $this->insertSimilarSites($data_list['domain_id'], $val["site"], $rank);
                                            endif;
                                        endforeach;
                                    endforeach;                                
                                endif;
                        endforeach;                    
                    endif;
                    // updated
                    MstDomainUrl::where('domain_id',$data_list['domain_id'])->update([
                                    'status_update' => 3, 'all_seo_data' => serialize($a['seo_result'])]);

                    MstDomainUrl::where('domain_url',$domain_url)->where('domain_id','!=',$data_list['domain_id'])->update([
                                    'status_update' => 4, 'all_seo_data' => serialize($a['seo_result'])]);

                    
                else :

                    // not updated
                    MstDomainUrl::where('domain_id',$data_list['domain_id'])->update([
                                    'status_update' => 5,'all_seo_data' => 'na']);
                    MstDomainUrl::where('domain_url',$domain_url)->where('domain_id','!=',$data_list['domain_id'])->update([
                                    'status_update' => 6,'all_seo_data' => 'na']);

                endif;
            else :
                $data_list['domain_id'] = $pval->domain_id; 
                $domain_id = $pval->domain_id;
                $domain_url = $pval->domain_url;

                /* Sandbox start*/

                    //$moz_result = $this->dataMOZ($domain_url);

                /* Sandbox end*/

                /* Production start*/

                    // $assigned = $this->insertAssigned($pval['domain_id'],$pval['domain_fk_user_id']);
                    // continue;

                    //$moz_result = $this->dataForMoz($domain_url);
                     
                    /* get data from moz */
                    $da_result = dataForRapidMoz($domain_url);
                    /* get data from moz*/
                    $array['pda'] = 0;
                    $array['upa'] = 0;
                    $array['all_moz'] = $moz;
                    if(!empty($da_result['data'][0])){
                        foreach ($da_result['data'][0] as $key => $value) {
                          $moz = @serialize($value);
                          $array['all_moz'] = $moz;
                          $array['pda'] = @$value['domain_authority'];
                          $array['upa'] = @$value['page_authority'];
                        }
                        /* insert data */
                            $this->insertDAPA($domain_id, $array);
                        /* insert data */
                    }


                /* Production End*/

                // if(!empty($moz_result)) :
                //     $this->insertDAPA($data_list['domain_id'], $moz_result);
                // endif;            
                /* Sandbox start*/

                    //$seo_result = $this->dataseo($domain_url);

                /* Sandbox end*/

                /* Production start*/
                
                $seo_result = $this->dataForSeo($domain_url);
                 $a['seo_result'] = $seo_result;
                /* Production End*/
                if($seo_result['status'] == 'ok'):
                    if(!empty($seo_result['results'])) : 
                        $a[$data_list['domain_id']] = $data_list['domain_id']; 
                        $a[$data_list['domain_id']] = $domain_url;
                        foreach ($seo_result['results'] as $ckey => $cval) :

                              $data_list['seo']['sd_visits'] = @$cval['audience']['visits'];
                              $data_list['seo']['sd_page_views_avg'] = @$cval['audience']['page_views_avg'];
                              $data_list['seo']['sd_time_on_site_avg'] = @$cval['audience']['time_on_site_avg'];

                              $data_list['seo']['sd_bounce_rate'] = @$cval['audience']['bounce_rate'];
                              $data_list['seo']['sd_title_n'] = @$cval['site_title'];
                              $data_list['seo']['sd_descr_n'] = @$cval['site_description'];
                              $data_list['seo']['sd_percent'] = @$cval['traffic']['percent'];
                              $data_list['seo']['sd_fk_domain'] = $data_list['domain_id'];
                              $data_list['seo']['sd_global_rank'] = serialize(@$cval['global_rank']);
                              $data_list['seo']['sd_country_rank'] = serialize(@$cval['country_rank']);
                              $data_list['seo']['sd_category_rank'] = serialize(@$cval['category_rank']);
                              $data_list['seo']['sd_traffic_main_value'] = @$cval['traffic']['value'];
                              $data_list['seo']['sd_traffic_main_percent'] = @$cval['traffic']['percent'];

                              $this->insertSEOdata($data_list);
                            
                                if(!empty($cval['traffic']['countries'])) :
                                    foreach ($cval['traffic']['countries'] as $ctykey => $ctyvalue) :
                                        $this->insertDomainCountries($data_list['domain_id'], $ctyvalue);
                                    endforeach;
                                endif;
                                

                                if(!empty($cval['traffic']["sources"])) :

                                    if(!empty($cval['traffic']["sources"]['direct'])) :
                                        $data_list['direct'] = serialize($cval['traffic']["sources"]['direct']);
                                    endif;
                                    if(!empty($cval['traffic']["sources"]['search_ad'])) :
                                        $data_list['search_ad'] = serialize($cval['traffic']["sources"]['search_ad']);
                                    endif;
                                    if(!empty($cval['traffic']["sources"]['referral'])) :
                                        $data_list['referral'] = serialize($cval['traffic']["sources"]['referral']);
                                    endif;
                                    if(!empty($cval['traffic']["sources"]['referral_ad'])) :
                                        $data_list['referral_ad'] = serialize($cval['traffic']["sources"]['referral_ad']);
                                    endif;
                                    if(!empty($cval['traffic']["sources"]['social'])) :
                                        $data_list['social'] = serialize($cval['traffic']["sources"]['social']);
                                    endif;
                                    
                                    if(!empty($cval['traffic']["sources"]['estimated'])) :
                                        $data_list['estimated'] = serialize($cval['traffic']["sources"]['estimated']);
                                    endif;

                                    if(!empty($cval['traffic']["sources"]['mobile_apps'])) :
                                        $data_list['mobile_apps'] = serialize($cval['traffic']["sources"]['mobile_apps']);
                                    endif;
                                    if(!empty($cval['traffic']["sources"]['mail'])) :
                                        $data_list['mail'] = serialize($cval['traffic']["sources"]['mail']);
                                    endif;

                                    if(!empty($cval['traffic']["sources"]['search_organic']['top_keywords'])) :
                                        $data_list['search_organic'] = $cval['traffic']["sources"]["search_organic"]['top_keywords'];
                                        $data_list['search_organic_serial'] = serialize($cval['traffic']["sources"]["search_organic"]['top_keywords']);
                                    endif;

                                    if(!empty($cval['traffic']["sources"]['search_organic']['value'])) :
                                        $data_list['search_organic_value'] = $cval['traffic']["sources"]["search_organic"]['value'];
                                    endif;

                                    if(!empty($cval['traffic']["sources"]['search_organic']['percent'])) :
                                        $data_list['search_organic_percent'] = $cval['traffic']["sources"]["search_organic"]['percent'];
                                    endif;
                                    $this->insertOrganicSearch($data_list['domain_id'], $data_list);
                                endif;
                              
                              
                                if(!empty($cval["sites"])) :
                                    foreach ($cval["sites"] as $skey => $sval) :
                                        foreach ($sval as $key => $val) :
                                            $rank = 0;
                                            if(!empty($val["rank"])){
                                                $rank = $val["rank"];
                                            }
                                            if(!empty($val["site"]) ) :
                                                $this->insertSimilarSites($data_list['domain_id'], $val["site"], $rank);
                                            endif;
                                        endforeach;
                                    endforeach;                                
                                endif;
                        endforeach;                    
                    endif;
                    // updated
                    MstDomainUrl::where('domain_id',$data_list['domain_id'])->update([
                                    'status_update' => 3, 'all_seo_data' => serialize($a['seo_result'])]);
                    
                else :

                    // not updated
                    MstDomainUrl::where('domain_id',$data_list['domain_id'])->update([
                                    'status_update' => 5,'all_seo_data' => 'na']);

                endif;
            endif;
            
        endforeach;

        if(!empty($domain_id)){
            $this->call_API_check($a,$domain_id);    
        } else {
            echo 'all done Thanks!';
            $list = MstDomainUrl::where('cost_price','>',0)->update(['status_update' => NULL]);
        }
        
    }

    /* no use for now  during add website Start */ 
    public function call_API_Website($domain_url, $domain_id) {
        
        $data_list = [];
        /* Production start*/
        $data_list['domain_id'] = $domain_id; 
        $domain_id = $domain_id;
        $domain_url = $domain_url;

        $seo_result = $this->dataForSeo($domain_url);
        
        /* Production End*/
        if($seo_result['status'] == 'ok'):
            if(!empty($seo_result['results'])) : 
                // updated
                MstDomainUrl::where('domain_id',$data_list['domain_id'])->update([
                            'status_update' => 3, 'all_seo_data' => serialize($seo_result)]);
                foreach ($seo_result['results'] as $ckey => $cval) :

                      $data_list['seo']['sd_visits'] = @$cval['audience']['visits'];
                      $data_list['seo']['sd_page_views_avg'] = @$cval['audience']['page_views_avg'];
                      $data_list['seo']['sd_time_on_site_avg'] = @$cval['audience']['time_on_site_avg'];

                      $data_list['seo']['sd_bounce_rate'] = @$cval['audience']['bounce_rate'];
                      $data_list['seo']['sd_title_n'] = @$cval['site_title'];
                      $data_list['seo']['sd_descr_n'] = @$cval['site_description'];
                      $data_list['seo']['sd_percent'] = @$cval['traffic']['percent'];
                      $data_list['seo']['sd_fk_domain'] = $data_list['domain_id'];
                      $data_list['seo']['sd_global_rank'] = serialize(@$cval['global_rank']);
                      $data_list['seo']['sd_country_rank'] = serialize(@$cval['country_rank']);
                      $data_list['seo']['sd_category_rank'] = serialize(@$cval['category_rank']);
                      $data_list['seo']['sd_traffic_main_value'] = @$cval['traffic']['value'];
                      $data_list['seo']['sd_traffic_main_percent'] = @$cval['traffic']['percent'];

                      $this->insertSEOdata($data_list);
                    
                        if(!empty($cval['traffic']['countries'])) :
                            foreach ($cval['traffic']['countries'] as $ctykey => $ctyvalue) :
                                $this->insertDomainCountries($data_list['domain_id'], $ctyvalue);
                            endforeach;
                        endif;
                        

                        if(!empty($cval['traffic']["sources"])) :

                            if(!empty($cval['traffic']["sources"]['direct'])) :
                                $data_list['direct'] = serialize($cval['traffic']["sources"]['direct']);
                            endif;
                            if(!empty($cval['traffic']["sources"]['search_ad'])) :
                                $data_list['search_ad'] = serialize($cval['traffic']["sources"]['search_ad']);
                            endif;
                            if(!empty($cval['traffic']["sources"]['referral'])) :
                                $data_list['referral'] = serialize($cval['traffic']["sources"]['referral']);
                            endif;
                            if(!empty($cval['traffic']["sources"]['referral_ad'])) :
                                $data_list['referral_ad'] = serialize($cval['traffic']["sources"]['referral_ad']);
                            endif;
                            if(!empty($cval['traffic']["sources"]['social'])) :
                                $data_list['social'] = serialize($cval['traffic']["sources"]['social']);
                            endif;
                            
                            if(!empty($cval['traffic']["sources"]['estimated'])) :
                                $data_list['estimated'] = serialize($cval['traffic']["sources"]['estimated']);
                            endif;

                            if(!empty($cval['traffic']["sources"]['mobile_apps'])) :
                                $data_list['mobile_apps'] = serialize($cval['traffic']["sources"]['mobile_apps']);
                            endif;
                            if(!empty($cval['traffic']["sources"]['mail'])) :
                                $data_list['mail'] = serialize($cval['traffic']["sources"]['mail']);
                            endif;

                            if(!empty($cval['traffic']["sources"]['search_organic']['top_keywords'])) :
                                $data_list['search_organic'] = $cval['traffic']["sources"]["search_organic"]['top_keywords'];
                                $data_list['search_organic_serial'] = serialize($cval['traffic']["sources"]["search_organic"]['top_keywords']);
                            endif;

                            if(!empty($cval['traffic']["sources"]['search_organic']['value'])) :
                                $data_list['search_organic_value'] = $cval['traffic']["sources"]["search_organic"]['value'];
                            endif;

                            if(!empty($cval['traffic']["sources"]['search_organic']['percent'])) :
                                $data_list['search_organic_percent'] = $cval['traffic']["sources"]["search_organic"]['percent'];
                            endif;
                            $this->insertOrganicSearch($data_list['domain_id'], $data_list);
                        endif;
                      
                        if(!empty($cval["sites"])) :
                            foreach ($cval["sites"] as $skey => $sval) :
                                foreach ($sval as $key => $val) :
                                    $rank = 0;
                                    if(!empty($val["rank"])){
                                        $rank = $val["rank"];
                                    }
                                    if(!empty($val["site"]) ) :
                                        $this->insertSimilarSites($data_list['domain_id'], $val["site"], $rank);
                                    endif;
                                endforeach;
                            endforeach;                                
                        endif;
                endforeach;
            endif;
            $response = true;
        else :
            // not updated
            MstDomainUrl::where('domain_id',$domain_id)->update([
                            'status_update' => 5,'all_seo_data' => 'na']);
            $response = true;
        endif;
        return $response;
    }
    /*  no use for now during add website End */

    /*main function end*/
    public function call_API_check($a,$domain_id) {

        $list = MstDomainUrl::where('domain_id', $domain_id)->with(['data_da_pa','data_seo','data_report_spam','data_search_url','data_similar_site','data_organic_search','data_cnf_domain_country.data_country','data_assigned_website.data_username','data_add_to_cart.username','data_add_to_cart.domainusername','data_extra_field'])->get()->toArray();
        $this->dd($a);
        $this->dd($list);
    }

    private function websiteAccept($id, $message){

          $domain = MstDomainUrl::where('domain_id',$id)->first();
          $publisherFname = @$domain->websiteuser->user_fname;
          $publishercode = @$domain->websiteuser->user_code;
          $publisheremail = @$domain->websiteuser->email;
          $domain_url = @$domain->domain_url;

          /* flock notification */

            $notiMessage = @$domain_url.' has been accepted by @Admin';
            
            if(env('APP_ENV') == 'local' || env('APP_ENV') == 'stage' ){
                $notiMessage = '<strong>' . env('APP_NAME') . '</strong> : '.@$domain_url.' has been accepted by @Admin';
            }        
            flockNotification($notiMessage);

          /* flock notification */


          /* Email template */
            $args['email'] = $publisheremail;
            $args['subject'] = 'New Website Accepted by Admin';
            $args['template_id'] = 'd-6918d7a01559459c9e1afb8bf3455e12';
            $args['data'] = '{
                    "Website" : {
                          "name":"'.@$domain_url.'"
                    },
                    "Publisher":{
                          "F_name":"@'.@$publishercode.'"
                       }
                  }';

            $this->sendgridEmail($args);

          /* Email template */

          /* Update domain url */
            $mst_domain_url = MstDomainUrl::where('domain_id',$id)->update(['domain_approval_status' => 1,'domain_approval_by' => 7956,'approval_message' => $message]);
    }

    private function websiteReject($id, $message) {
          $domain = MstDomainUrl::where('domain_id',$id)->first();
          $publisherFname = $domain->websiteuser->user_fname;
          $publishercode = @$domain->websiteuser->user_code;
          $publisheremail = $domain->websiteuser->email;
          $domain_url = $domain->domain_url;          
          

          /* flock notification */

            $notiMessage = @$domain_url.' has been rejected by @Admin because '. $message;
            
            if(env('APP_ENV') == 'local' || env('APP_ENV') == 'stage' ){
                $notiMessage = '<strong>' . env('APP_NAME') . '</strong> : '.@$domain_url.' has been rejected by @Admin because '. $message;
            }        
            flockNotification($notiMessage);

          /* flock notification */

          /* Email template */
            $args['email'] = $publisheremail;
            $args['subject'] = 'Website Declined by Admin because ' . $message;
            $args['template_id'] = 'd-477b02b986e74d30a261ed0fce114bd4';
            $args['data'] = '{
                    "Website" : {
                          "name":"'.@$domain_url.'"
                    },
                    "Publisher":{
                          "F_name":"@'.@$publishercode.'"
                       }
                  }';
            $this->sendgridEmail($args);

          /* Email template */

          /* Update domain url */

            $mst_domain_url = MstDomainUrl::where('domain_id',$id)->update(['domain_approval_status' => 3,'domain_approval_by' => 7956,'approval_message' => $message]);

    }

    private function da_price($w, $c, $extra, $price){
        $status = true;
        $message = 'Website has been Approved';
        if($extra > $c){
            $status = false;
            $message = 'Content cost should not be greater than $' . $c . ' - Content cost entered : $' .$extra;
        } 
        if ($price > $w){
            $status = false;
            $message = 'Website cost should not be greater than $' . $w . ' - Website cost entered : $' .$price;
        }
        if ($price > $w && $extra > $c){
            $status = false;
            $message = 'Website cost should not be greater than $' . $w .' and content cost should not be greater than $' . $c . ' - Content cost entered : $' . $extra . ' - Website cost entered : $'.$price;
        }
        return ['status'=>$status, 'message' => $message];
    }

    public function automaticApproveWebsite() {
        
        $no_of_page = 1;
        $offset = '';
        if(isset($_REQUEST['page'])){
            $offset = 'page=' . $_REQUEST['page'];
        }
        $selectQuery = "SELECT mst_domain_url.domain_id, mst_da_pa.dp_da FROM mst_domain_url join mst_da_pa on mst_da_pa.dp_fk_domain = mst_domain_url.domain_id WHERE mst_da_pa.dp_da <= 70 AND mst_domain_url.cost_price > 0 AND mst_domain_url.domain_fk_user_id > 1 AND ( mst_domain_url.domain_approval_status is null OR mst_domain_url.domain_approval_status = 0 )";
        //GROUP BY domain_url limit $no_of_page
        $results = DB::select( DB::raw($selectQuery) );
        $results = json_decode(json_encode($results), true);
        $domainID = [];
        foreach ($results as $key => $value) {
            $domainID[] = $value['domain_id'];
        }

        $list = MstDomainUrl::where('domain_approval_status','=',NULL)->select('domain_id','domain_url','cost_price','extra_cost','domain_fk_user_id','all_seo_data')->whereIn('domain_id',$domainID)->orderBy('domain_id','DESC')->paginate($no_of_page);
        $list->withPath('?' . $offset);

        // $list = MstDomainUrl::select('domain_id','domain_url','cost_price','extra_cost','all_seo_data','domain_fk_user_id')->where('domain_fk_user_id','>',0)->where('domain_url','chartattack.com')->where('cost_price','>',0)->with('data_da_pa')->whereHas('data_da_pa', function ($query) {
        //         $query->where('dp_da','<', 51);
        //     })->orderBy('domain_id','DESC')->paginate(1);
        //$list->withPath('?' . $offset);

        if($list->total() < 1) {
            $list = MstDomainUrl::where('domain_approval_status','=',0)->select('domain_id','domain_url','cost_price','extra_cost','domain_fk_user_id','all_seo_data')->whereIn('domain_id',$domainID)->orderBy('domain_id','DESC')->paginate($no_of_page);
            $list->withPath('?' . $offset);
        }

        foreach ($list as $pkey => $pval) :            
            
            $da = @$pval->data_da_pa->dp_da;

            $domain_url = $pval->domain_url;
            $domain_id = $pval->domain_id;
            $price = $pval->cost_price;
            $extra = $pval->extra_cost;

            $domain_fk_user_id = @$pval->domain_fk_user_id;
            if(!empty($domain_fk_user_id)){

                $assigned_domain_list =  \App\AssignedWebsite::where('mst_domain_url_id',$domain_id)->where('user_id',$domain_fk_user_id)->value('id');    
                if(empty($assigned_domain_list)){
                    $assigned_domain_list =  \App\AssignedWebsite::insert([
                                'mst_domain_url_id' => $domain_id,
                                'user_id' => $domain_fk_user_id,
                            ]);
                }

            }


            /* if da empty */
            if(empty($da)) {
                /* get data from moz*/
                $da_result = dataForRapidMoz($domain_url);
                /* get data from moz*/
                $array['dp_da'] = 0;
                if(!empty($da_result['data'][0])){
                    foreach ($da_result['data'][0] as $key => $value) {
                          $moz = @serialize($value);
                          $array['all_moz'] = $moz;
                          $array['pda'] = @$value['domain_authority'];
                          $array['upa'] = @$value['page_authority'];

                    }
                    /* insert data */
                    $this->insertDAPA($domain_id, $array);
                    /* insert data */
                    $da = DB::table('mst_da_pa')->where('dp_fk_domain',$domain_id)->value('dp_da');
                } else {                    
                    $array['all_moz'] = 0;
                    $array['pda'] = 0;
                    $array['upa'] = 0;
                    /* insert data */
                    $this->insertDAPA($domain_id, $array);
                    /* insert data */
                    $da = DB::table('mst_da_pa')->where('dp_fk_domain',$domain_id)->value('dp_da');
                }
            }

            if($da >= 20){

                $w = 60;
                $c = 60;

                if($da >= 20 && $da <= 30){
                    $w = 60;
                    $c = 60;
                } else if($da >= 31 && $da <= 40){

                    $w = 160;
                    $c = 120;

                } else if($da >= 41 && $da <= 50){

                    $w = 300;
                    $c = 200;
                    
                } else if($da >= 51 && $da <= 60){

                    $w = 480;
                    $c = 300;
                    
                } else if($da >= 61 && $da <= 70){

                    $w = 840;
                    $c = 420;
                    
                } else if($da >= 71 && $da <= 80){
                    
                    $w = 1120;
                    $c = 560;

                } else if($da >= 81 && $da <= 90){
                    
                    $w = 1440;
                    $c = 720;

                } else if($da >= 91 && $da <= 100){
                    
                    $w = 1800;
                    $c = 900;

                }

                $res = $this->da_price($w, $c, $extra, $price);

                if($res['status'] == true) {
                    $this->websiteAccept($domain_id, $res['message']);

                    // if(isset($pval->all_seo_data)) {
                    //     if($pval->all_seo_data == 'na') {
                            $this->call_API_Website($domain_url, $domain_id);
                    //     }
                    // }


                } else {
                    $this->websiteReject($domain_id, $res['message']);
                }
            } else {
                $this->websiteReject($domain_id, 'Domain Authority should be greater than 20.');
            }
        endforeach;
    }
    public function DbToHubspotAPI(){
        $curr_date = date('Y-m-d');
        $users = User::where('user_email_verified','>',0)->where('hubspot',0)->paginate(20);
        
        
        $args = []; 
        $array = [];
        $count = 0;
        foreach ($users as $key => $value) :

            $purchases = DB::table('add_to_carts')->where('user_id',$value->user_id)->where('is_billed',1)->count();
            // buyer = advertiser
            $websites = DB::table('mst_domain_url')->where('domain_fk_user_id',$value->user_id)->count();
            // seller = publisher

            //$orders = DB::table('add_to_carts')->where('domain_user_id',$value->user_id)->where('is_billed',0)->count();            

            if($websites > 0 && $purchases > 0){
                $contact_type = 'Advertiser;Publisher';

            } else if($websites > 0){
                $contact_type = 'Publisher';
                
            } else if($purchases > 0){
                $contact_type = 'Advertiser';
            } else {
                $contact_type = '';
            }
            
            $email = $value->email;            
            if(!empty($email)) :
                // email
                $array[$count]['email'] = $value->email;

                // Firstname
                $array[$count]['firstname'] = @$value->user_fname;
                if(empty($array[$count]['firstname'])) :
                    $array[$count]['firstname'] = 'Client';
                endif;

                // Lastname
                $array[$count]['lastname'] = @$value->user_lname;
                if(empty($array[$count]['lastname'])) :
                    $array[$count]['lastname'] = 'Client';
                endif;

                // Usercode
                if(!empty($value->user_code)):
                    $array[$count]['user_code'] = url('profile').'/'.$value->user_code;
                else :
                    $array[$count]['user_code'] = '';
                endif;                

                // Id
                $array[$count]['id'] = @$value->user_id;

                // Type
                $array[$count]['type'] = @$value->user_type;
                if($array[$count]['type'] == 'webmaster' || $array[$count]['type'] == 'admin') :
                    $array[$count]['type'] = 'Admin'; // admin
                elseif ($array[$count]['type'] == 'user') :
                    $array[$count]['type'] = 'User'; // user
                else :
                    $array[$count]['type'] = 'User'; // user
                endif;

                // Email Verified
                $array[$count]['user_email_verified'] = @$value->user_email_verified;
                if(!empty($array[$count]['user_email_verified'])):
                    $array[$count]['user_email_verified'] = 'Yes'; // done
                else :
                    $array[$count]['user_email_verified'] = 'No'; // pending
                endif;


                // User Status
                $array[$count]['user_status'] = @$value->user_status;
                if(!empty($array[$count]['user_status'])) :
                    // Active
                    if($array[$count]['user_status'] == 1) :
                        $array[$count]['user_status'] = 'Active'; // Active
                    else :
                        $array[$count]['user_status'] = 'Deactive'; // Deactive
                    endif;
                else :
                    $array[$count]['user_status'] = ''; // No Active
                endif;


                // Contact Type
                $array[$count]['contact_type'] = $contact_type;
                

                // Created At
                if(!empty($value->created_at)) :
                    $myDate = new \DateTime($value->created_at, new \DateTimeZone('UTC'));
                    $hubSpotDate = $myDate->format('Y-m-d g:i a');
                    $array[$count]['signup_date'] = $hubSpotDate;

                else :
                    $date = date('Y-m-d H:i:s');
                    $myDate = new \DateTime($date , new \DateTimeZone('UTC'));
                    $hubSpotDate = $myDate->format('Y-m-d g:i a');
                    $array[$count]['signup_date'] = $hubSpotDate;
                    
                endif;

                // Last Login
                if(!empty($value->user_created_time)) :
                    
                    
                    $myDate = new \DateTime($value->user_created_time, new \DateTimeZone('UTC'));
                    $hubSpotDate = $myDate->format('Y-m-d g:i a');
                    $array[$count]['last_login'] = $hubSpotDate;                    

                else :
                    $array[$count]['last_login'] = '';
                endif;

                //$email = 'danish@webdew.com';
                $args['url'] = "https://api.hubapi.com/contacts/v1/contact/email/".$email."/profile?hapikey=" . $this->hapikey;                

                $dataContact = '{
                                  "properties": [
                                    {
                                      "property": "email",
                                      "value": "'. $array[$count]['email'] .'"
                                    },
                                    {
                                      "property": "firstname",
                                      "value": "'. $array[$count]['firstname'] .'"
                                    },
                                    {
                                      "property": "lastname",
                                      "value": "'. $array[$count]['lastname'] .'"
                                    },
                                    {
                                      "property": "sign_up_date",
                                      "value": "'.$array[$count]['signup_date'].'"
                                    },
                                    {
                                      "property": "email_verification",
                                      "value": "'.$array[$count]['user_email_verified'].'"
                                    },
                                    {
                                      "property": "contact_type",
                                      "value": "'.$contact_type.'"
                                    },
                                       
                                    {
                                      "property": "user_type",
                                      "value": "'.$array[$count]['type'].'"
                                    },                                 
                                    {
                                      "property": "user_status",
                                      "value": "'.$array[$count]['user_status'].'"
                                    },
                                    {
                                      "property": "last_login_date",
                                      "value": "'.$array[$count]['last_login'].'"
                                    },
                                    {
                                      "property": "user_id",
                                      "value": "'.$array[$count]['id'].'"
                                    },
                                    {
                                      "property": "user_code",
                                      "value": "'.$array[$count]['user_code'].'"
                                    }
                                  ]
                                }';

                $args['data'] = $dataContact;
                $result = $this->curlAccess( 'GET', $args );

              
                if(isset($result->status)){
                    if($result->status == 'error'){

                        $args['url'] = "https://api.hubapi.com/contacts/v1/contact/?hapikey=" . $this->hapikey;
                        $result = $this->curlAccess( 'POST', $args );
                        User::where('user_id',$array[$count]['id'])->update(['hubspot' => 1,'contact_type' => $contact_type]);
                    }
                    // Incremented array
                    $count++;
                } else {

                    $args['url'] = "https://api.hubapi.com/contacts/v1/contact/email/".$email."/profile?hapikey=" . $this->hapikey;
                    $result = $this->curlAccess( 'POST', $args );
                    User::where('user_id',$array[$count]['id'])->update(['hubspot' => 1,'contact_type' => $contact_type]);
                }

                
            endif;
        endforeach;      

        echo 'Success';

         //$users_reset = User::where('user_email_verified','>',0)->where('hubspot',0)->where('user_type','user')->count();
         //if($users_reset == 0){
           // $users_reset = User::where('user_email_verified','>',0)->where('user_type','user')->update(['hubspot' => 0]);
         //}
    }

    public function chatNotRead() {
        
        $chatList = Chat::where("is_viewed",0)->with('sender','receiver')->where("is_email_sent",0)->groupBy('sender_id')->get();
        foreach ($chatList as $key => $value) {
          $sender_id = @$value->sender->user_id;
          $receiver_id = @$value->receiver->user_id;
          $receiverName = @$value->receiver->user_fname;
          $senderFName = @$value->sender->user_fname;
          $senderLName = @$value->sender->user_lname;
          
          $args['email']    = $value->receiver->email;
          //$args['email']    = 'randeep.s@webdew.com';
          $args['subject']  = 'Unread message';
          $args['template_id'] = 'd-7f76c7ea3ea94a2bbf85d6bdf0020366';
          $args['data'] = '{
              "Receiver":{
                    "F_name":"'.@$receiverName.'"
                 },
                 "Sender":{
                    "F_name":"'.@$senderFName.'",
                    "L_name":"'.@$senderLName.'"
                 }
            }';
            $result = $this->sendgridEmail( $args );
            Chat::where('sender_id',$sender_id)->where('receiver_id',$receiver_id)->where('is_viewed',0)->update(['is_email_sent' => 1 ]);
        }
    }

    // Print Function Begin
    public function e( $body ){
        echo '<h2>';
        echo $body;
        echo '</h2>';   
    }
    public function dd( $array, $heading = 'list' ){

        e($heading . ' begin');

        echo '<pre>';
        print_r($array);
        echo '</pre>';

        e($heading . ' end');

        die;
    }
    public function d( $array, $heading = 'list' ){

        e($heading . ' begin');

        echo '<pre>';
        print_r($array);
        echo '</pre>';   

        e($heading . ' end');
    }
    // Print function end
    // Curl Function Begin

    public function curlAccess($method, $array, $content_type = 'array' ) {
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $array['url']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        switch ($method){
          case "POST":
            curl_setopt($ch, CURLOPT_POST, 1);        
             if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
             break;
          case "PUT":
             curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
             if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
             break;
          default:
             curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
       }

        

        $headers = array();
        $headers[] = 'Content-Type: application/json';
        //$headers[] = 'Authorization: ApiKey '.$array['username'].':'.$array['api_key'];
        //$headers[] = 'Authorization: Api-Key ' . $array['credentials'];
        //$headers[] = 'Api-Key: ' . $array['api_key'];
        // if(!empty($array['username'])){
        //     $headers[] = 'Authorization: ' . $array['username'];
        // }

        
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
        
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        if($content_type == 'array'){
            $result = json_decode($result); 
        }  
        return $result;
    } 
    public function curlAccessEmail($method, $array, $content_type = 'array' ) {
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $array['url']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        switch ($method){
          case "POST":
            curl_setopt($ch, CURLOPT_POST, 1);        
             if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
             break;
          case "PUT":
             curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
             if ($array['data'])
                curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
             break;
          default:
             curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
       }

        

        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'authorization: Bearer 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';
        
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
        
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        if($content_type == 'array'){
            $result = json_decode($result); 
        }  
        return $result;
    }
    // Curl Function Begin
    public function sendgridEmail($array){

        $args['url'] = 'https://api.sendgrid.com/v3/mail/send';
        $data = '{
               "from":{
                  "email":"noreply@guestpostengine.com"
               },
               "personalizations":[
                    {
                        "to":[
                            {
                               "email":"'.$array['email'].'",
                            }
                        ],
                        "subject": "'.$array['subject'].'",
                        "dynamic_template_data": '.$array['data'].'
                    }
               ],
               "template_id":"'.$array['template_id'].'"

            }';
        $args['data'] = $data;
        $result = $this->curlAccessEmail('POST',$args);
        return $result;
    }
    public function todayActivity($date = '') {
        if(empty($date)){
            $date = date('Y-m-d');
        }
        $result = User::where('created_at', '>' , $date.' 00:00:00')->where('created_at', '<' , $date.' 23:59:59')->get();
        $count = User::where('created_at', '>' , $date.' 00:00:00')->where('created_at', '<' , $date.' 23:59:59')->count();
        echo '<h4> Total Number of users ' .$count. ' : ' .$date.'</h5>';
        $html = '<table border="1">';
        $html .= '<tr>';
        $html .= '<th>Firstname</th>';
        $html .= '<th>Verified</th>';
        $html .= '<th>Switched</th>';
        $html .= '<th>Date</th>';
        $html .= '<th>Contact Type</th>';
        $html .= '</tr>';
        foreach ($result as $key => $value) {
            if(empty($value->user_email_verified)){
                $user_email_verified = 'Not verified';
            } else {
                $user_email_verified = 'verified';
            }
            $html .= '<tr>';
            $html .= '<td>'.$value->user_fname.'</td>';
            $html .= '<td>'.$user_email_verified.'</td>';
            $html .= '<td>'.$value->switched.'</td>';
            $html .= '<td>'.$date.'</td>';
            $html .= '<td>'.$value->contact_type.'</td>';
            $html .= '</tr>';
        }
        $html .= '</table>';
        echo $html;


        $count = MstDomainUrl::where('created_at', '>' , $date.' 00:00:00')->where('created_at', '<' , $date.' 23:59:59')->count();
        echo '<h4> Total Number of Websites ' .$count. ' : ' .$date.'</h5>';
        die;

    }
    public function testEmail(){
        // Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://api.sendgrid.com/v3/mail/send');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "{\n   \"from\":{\n      \"email\":\"no-reply@notify.cloudflare.com\"\n   },\n   \"personalizations\":[\n      {\n         \"to\":[\n            {\n               \"email\":\"randeep.s@webdew.com\"\n            }\n         ],\n         \"dynamic_template_data\":{\n            \"total\":\"$ 239.85\",\n            \"items\":[\n               {\n                  \"text\":\"New Line Sneakers\",\n                  \"image\":\"https://marketing-image-production.s3.amazonaws.com/uploads/8dda1131320a6d978b515cc04ed479df259a458d5d45d58b6b381cae0bf9588113e80ef912f69e8c4cc1ef1a0297e8eefdb7b270064cc046b79a44e21b811802.png\",\n                  \"price\":\"$ 79.95\"\n               },\n               {\n                  \"text\":\"Old Line Sneakers\",\n                  \"image\":\"https://marketing-image-production.s3.amazonaws.com/uploads/3629f54390ead663d4eb7c53702e492de63299d7c5f7239efdc693b09b9b28c82c924225dcd8dcb65732d5ca7b7b753c5f17e056405bbd4596e4e63a96ae5018.png\",\n                  \"price\":\"$ 79.95\"\n               },\n               {\n                  \"text\":\"Blue Line Sneakers\",\n                  \"image\":\"https://marketing-image-production.s3.amazonaws.com/uploads/00731ed18eff0ad5da890d876c456c3124a4e44cb48196533e9b95fb2b959b7194c2dc7637b788341d1ff4f88d1dc88e23f7e3704726d313c57f350911dd2bd0.png\",\n                  \"price\":\"$ 79.95\"\n               }\n            ],\n            \"receipt\":true,\n            \"name\":\"Sample Name\",\n            \"address01\":\"1234 Fake St.\",\n            \"address02\":\"Apt. 123\",\n            \"city\":\"Place\",\n            \"state\":\"CO\",\n            \"zip\":\"80202\"\n          }\n      }\n   ],\n   \"template_id\":\"d-7f76c7ea3ea94a2bbf85d6bdf0020366\"\n}");

        $headers = array();
        $headers[] = 'Authorization: Bearer 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';
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);
    }

    function getAuth(){
        $data = 'live';

        $credentials = [
            'sandbox' => [
                            'authkey'  => '122573AyFLqoumaVR5cd9177b',
                            'hapikey' => 'a3f1f7e3-6387-4b5a-8790-eadd8d5a8a45',
                            
                        ],
            'live'    => [
                            'authkey'  => '122573AyFLqoumaVR5cd9177b',
                            'hapikey' => 'a3f1f7e3-6387-4b5a-8790-eadd8d5a8a45',
                      ],
        ];

        $args = [];
        /* sandbox/Live */
            
        $args['authkey'] = $credentials[$data]['authkey'];
        $args['hapikey'] = $credentials[$data]['hapikey'];
        return $args;
    }
    public function getmsg91(){
        /* Hubspot data Start */

        $args = $this->getAuth();
        $args['url'] = "https://api.msg91.com/api/getAllClients.php?authkey=".$args['authkey']."&limit=2";

        $results = $this->curlAccess( 'GET', $args );
        
        $count = 0;
        foreach ($results as $key => $value) :
            // if($count < 8700){
            //     $count++;
            //     continue;
            // }

            $email = @$value->user_email;
            if($email){
                $result = DB::table('msgwow')->where('email', $email)->value('email');
                $data = serialize($value);
                if(empty($result)){
                    DB::table('msgwow')->insert(['email'=>$email, 'data' => $data, 'hubspot' => 0]);
                } else {
                    DB::table('msgwow')->where('email',$email)->update([ 'data' => $data, 'hubspot' => 0]);
                }
                $count++;    
            }
        endforeach;
        echo 'Success';
    }
    public function msgwow(){

        $args = $this->getAuth();
        $results = DB::table('msgwow')->where('hubspot', 0)->orderBy('id','DESC')->paginate(10);

        $countTotal = DB::table('msgwow')->where('hubspot', 0)->count();

        $count = 0;
        $fullname[0] = '';
        $fullname[1] = '';
        foreach ($results as $key => $val) :

            $value = unserialize($val->data);
            $myArray = json_decode(json_encode($value), true);

            $fullname = explode(" ",$value->user_fname);

            // User date
            if(!empty($value->user_date)){
                $date = date_create($value->user_date);
                //$array[$count]['user_date'] = date_format($date, 'U') * 1000;
                $array[$count]['user_date'] = date_format($date, 'd-m-Y A');
            } else {
                $date =  date('Y-m-d');
                $date = date_create($date);
                //$array[$count]['user_date'] = date_format($date, 'U') * 1000;
                $array[$count]['user_date'] = date_format($date, 'd-m-Y A');
            }
            $email = $value->user_email;
            if(!empty($email)) :
                // email
                $array[$count]['user_email']     = $value->user_email;
                // Firstname
                $array[$count]['user_fname']     = @$fullname[0];
                // Lastname
                $array[$count]['user_lname']     = @$fullname[1].' '.@$fullname[2];
                // Usercode
                $array[$count]['user_uname']     = @$value->user_uname;

                $array[$count]['user_mob_no']    =  @$value->user_mobno;

                $array[$count]['user_date']      = @$value->user_date;
                // user type
                $array[$count]['contact_type']   = @$value->user_type;

                $array[$count]['last_login']     = @$value->last_login;

                if(!empty($array[$count]['contact_type'])){
                    if($array[$count]['contact_type'] == 2){
                        $array[$count]['contact_type'] = 'Reseller';
                    } else if ($array[$count]['contact_type'] == 3){
                        $array[$count]['contact_type'] = 'Client';
                    }
                }
                
                // User status
                $array[$count]['user_status'] = @$value->user_status;
                
                $transactional_sms_balance = ''; // 4
                $promotional_sms_balance = ''; // 1
                $otp_sms_balance = ''; // 106
                    
                if(!empty($array[$count]['user_status'])){
                    if($array[$count]['user_status'] == 1){
                        $user_status = $array[$count]['user_status'];
                    } else if($array[$count]['user_status'] == 2){
                        $user_status = $array[$count]['user_status'];
                    }
                }
                
                if(!empty($myArray['1_bal'])){
                    $promotional_sms_balance    = $myArray['1_bal'];
                }
                if(!empty($myArray['4_bal'])){
                    $transactional_sms_balance  = $myArray['4_bal'];
                }

                $args['url'] = "https://api.hubapi.com/contacts/v1/contact/email/".$email."/profile?hapikey=" . $args['hapikey'];

                $dataContact = '{
                                  "properties": [
                                    {
                                          "property": "email",
                                          "value": "'. $array[$count]['user_email'] .'"
                                    },
                                    {
                                          "property": "firstname",
                                          "value": "'. $array[$count]['user_fname'] .'"
                                    },
                                    {
                                          "property": "lastname",
                                          "value": "'. $array[$count]['user_lname'] .'"
                                    },
                                    {
                                          "property": "contact_type",
                                          "value": "'.$array[$count]['contact_type'].'"
                                    },
                                    
                                    {
                                        "property": "mobilephone",
                                         "value": "+'.$array[$count]['user_mob_no'].'"
                                    },
                                    
                                    {
                                          "property": "transactional_sms_balance",
                                          "value": "'.$transactional_sms_balance.'"
                                    },
                                    {
                                          "property": "promotional_sms_balance",
                                          "value": "'.$promotional_sms_balance.'"
                                    },
                                    {
                                          "property": "otp_sms_balance",
                                          "value": "'.$otp_sms_balance.'"
                                    },
                                    {
                                          "property": "user_name",
                                          "value": "'.$array[$count]['user_uname'].'"
                                    },
                                    {
                                        "property": "sign_up_date",
                                         "value": "'.$array[$count]['user_date'].'"
                                    },
                                    {
                                        "property": "last_login_date",
                                         "value": "'.$array[$count]['last_login'].'"
                                    }
                                  ]
                                }';

                $args['data'] = $dataContact;
                
                $result = $this->curlAccess( 'GET', $args );              
                             
                if(isset($result->status)) {
                    if($result->status == 'error'){

                        $args['url'] = "https://api.hubapi.com/contacts/v1/contact/?hapikey=" . $args['hapikey'];
                        $result = $this->curlAccess( 'POST', $args );
                    }
                    $count++;
                } else {

                    $args['url'] = "https://api.hubapi.com/contacts/v1/contact/email/".$email."/profile?hapikey=" . $args['hapikey'];
                    $result = $this->curlAccess( 'POST', $args );
                }
                DB::table('msgwow')->where('email',$email)->update([ 'hubspot' => 1]);
            endif;
        endforeach;

        // reset
        if($countTotal < 1){
            DB::table('msgwow')->update([ 'hubspot' => 0]);
        }
    }
}
