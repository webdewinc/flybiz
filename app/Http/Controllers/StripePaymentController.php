<?php   
namespace App\Http\Controllers;
   
use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\API\APIController;
use Session;
use Stripe;
use App\Payment;
use App\MstDomainUrl;
use App\AddToCart;
use Auth;
   
class StripePaymentController extends APIController
{ 

	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripePost(Request $request)
    {


        $no_exist = AddToCart::whereUserId( Auth::user()->user_id )->where( 'is_billed', 0 )->first();
        $no_exist = json_decode(json_encode($no_exist), true);

        if(!empty($no_exist)){

            try {

                $amount = $request->amount;
                $stripeToken = Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
                $stripe = Stripe\Charge::create ([
                        "amount" => $amount * 100,
                        "currency" => "usd",
                        "source" => $request->stripeToken,
                        "description" => "Stripe payment from webdew company." ,
                        //"metadata" => ['question_id' => $service_id],
                ]);
                if($stripe->status == 'succeeded'){
                    $wallet = 0;
                    if(isset($request->wallet)){
                        $wallet = $request->wallet;
                    }
                    $ID = $stripe->id;
                    $currency = $stripe->currency;
                    $receipt_url = $stripe->receipt_url;
                    $paymentCardId = $stripe->source->id;  // Id
                    $paymentCardBrand = $stripe->source->brand; // visa
                    $status = $stripe->status; // succeeded
                    $domain_arr = AddToCart::where('user_id',Auth::user()->user_id)->where('is_billed','=',0)->where('mst_payment_id','=',0)->get()->toArray();
                    $payment = Payment::create([
                                    'stripe_id' => $ID,
                                    'currency' => $currency,
                                    'receipt_url' => $receipt_url,
                                    'payment_card_id' => $paymentCardId,
                                    'status' => $status,
                                    'user_id' => Auth::user()->user_id, // question_id, plan_id or service_id
                                    'amount' => $amount,
                                    'token' => $request->stripeToken,
                                    'promo_code' => $request->stripe_promo_id,
                                    'type'  => 'stripe',
                                    'wallet_amount' =>  $wallet

                            ]);
                    // $update = AddToCart::where('user_id',Auth::user()->user_id)->where('is_billed',0)->update([ 'mst_payment_id' => $payment->id,
                    //     'is_billed' => 1]);

                    /** add Email **/
                    foreach( $domain_arr as $split_domain_arr ){
                      $domain_id     = $split_domain_arr['domain_id'];
                      $order_id     = $split_domain_arr['id'];
                      $domain = MstDomainUrl::where('domain_id',$domain_id)->with(['websiteuser'])->first();
                      $cart = AddToCart::where('id',$order_id)->where('user_id',Auth::user()->user_id)->first();
                      $publisherFname = $domain->websiteuser->user_fname;
                      $publisheremail = $domain->websiteuser->email;

                      $domain_url = $domain->domain_url;

                        $cost = $domain->websiteuser->cost_price;
                        $cost_save = $cost;
                        if( $cart->promo_code == 0 ){
                            if($cart->content_type == 'buyer'){
                                $cost = '$' .buyerDomainWebsitePriceCalculate($cart->domainname->cost_price);
                                $cost_save = buyerDomainWebsitePriceCalculate($cart->domainname->cost_price);
                                $flybiz = $cost_save;
                            } else {
                                $cost = '$' .buyerDomainPriceCalculate($cart->domainname->cost_price, $cart->domainname->extra_cost);
                                $cost_save = buyerDomainPriceCalculate($cart->domainname->cost_price, $cart->domainname->extra_cost);
                                $flybiz = $cost_save;
                            }
                        } else {
                            if($cart->content_type == 'buyer'){
                                $cost = 0;
                            } else {
                                $cost = 0;
                            }
                        }


                          /* webdew discount */
                            if($request->stripe_promo_id > 0){
                                $cost_save = _promo_codes($request->stripe_promo_id,$cost_save);
                            }
                          /* webdew discount */


                          $cost_price = MstDomainUrl::where('domain_id',$domain_id)->value('cost_price');

                          $data = array('is_billed' => 1, 'mst_payment_id' => $payment->id, 'after_discount_price' => $cost_save,'cost_price' =>$cost_price, 'domain_amount' => $flybiz);
                           if($cart->content_type == 'seller'){
                              $extra_cost = MstDomainUrl::where('domain_id',$domain_id)->value('extra_cost'); 
                              $data = array('is_billed' => 1, 'mst_payment_id' => $payment->id, 'after_discount_price' => $cost_save,'cost_price' =>$cost_price, 'extra_cost'=>$extra_cost);
                           }
                         
                         AddToCart::where('id',$cart->id)->where('is_billed', 0)->where('user_id',Auth::user()->user_id)->update($data);


                      $AdvertiserFname = @Auth::user()->user_fname;
                      if(empty($AdvertiserFname)){
                          $AdvertiserFname = 'Advertiser'; 
                      }
                if( env('APP_ENV') == 'live'){
                      /* Email template for publiser */
                      $args['email'] = $publisheremail;
                     // $args['email']    = 'randeep.s@webdew.com';
                        $args['subject'] = 'New Order Received';
                        $args['template_id'] = 'd-d7fa1ef8e595481c8e406e51f1d9e5e4';
                        $args['data'] = '{
                            "Website" : {
                                  "name":"'.@$domain_url.'",
                                  "cost":"'.$cost.'"
                            },
                            "Publisher":{
                                  "F_name":"'.@$publisherFname.'"
                               },
                            "Advertiser":{
                                  "F_name":"'.$AdvertiserFname.'"
                               },
                          }';
                          $this->sendgridEmail($args);
                      /* Email template for publisher end */

                      /* Email template for Advertiser */
                       $args['email'] = Auth::user()->email;
                      //$args['email']    = 'randeep.s@webdew.com';
                        $args['subject'] = 'Placed an Order';
                        $args['template_id'] = 'd-94c82df8c5c743b2aa1e097820865acd';
                        $args['data'] = '{
                                "Website" : {
                                      "name":"'.@$domain_url.'",
                                      "cost":"'.$cost.'"
                                },
                                "Publisher":{
                                      "F_name":"'.@$publisherFname.'"
                                   },
                                "Advertiser":{
                                      "F_name":"'.@$AdvertiserFname.'"
                                   }
                              }';
                        $this->sendgridEmail($args);
                        /* Email template for Advertiser end */

                          /* flock notification */
                          
                          $notiMessage = 'New Order has been Placed by @'.@Auth::user()->user_code.' for '.@$domain_url.' at price '.$cost;
                          
                          if(env('APP_ENV') == 'local' || env('APP_ENV') == 'stage' ) {
                              $notiMessage = '<strong>' . env('APP_ENV') . '</strong> New Order has been Placed by @'.@Auth::user()->user_code.' for '.@$domain_url.' at price '.$cost;
                          }
                          flockNotification($notiMessage);

                          /* flock notification */



                        /** Email template for admin **/
                        $adminUsers = getAdmminList();
                        foreach($adminUsers as $admin_users){
                         $args['email']    = $admin_users['email'];
                        //$args['email']    = 'randeep.s@webdew.com';
                          $args['subject']  = 'Placed an Order';
                          $args['template_id'] = 'd-5a534dd45fb148d8892651295e198ac7';
                          $args['data'] = '{
                                  "Website" : {
                                        "name":"'.@$domain_url.'",
                                        "cost":"'.$cost.'"
                                  },
                                  "Publisher":{
                                        "F_name":"'.@$publisherFname.'"
                                     },
                                  "Advertiser":{
                                        "F_name":"'.@$AdvertiserFname.'"
                                     }
                                }';
                          $this->sendgridEmail($args);
                        } 
                    }
                }
                    $alert = 'success';
                    \Cache::forget('cartlist');
                    $message = 'Payment has been successfully.';
                    return view('viewmessage',compact('alert','message'));
                    
                } else {            
                    $alert = 'danger';
                    $message = 'Payment has not been successfully.';
                    return view('viewmessage',compact('alert','message'));
                }
                
            } catch (Exception $e) {
                
            }

        }
    }


    public function stripeSave(Request $request) {

        $data = $request->all();

        

        // $customer_id = @Auth::user()->stripe_customer_id;
        // if(empty($customer_id)){
            // User::where('user_id',Auth::user()->user_id)->update(['stripe_customer_id'=>$stripe_customer_id,'stripe_payment_method_id'=>$PAYMENT_METHOD->id]);

            $no_exist = AddToCart::whereUserId( Auth::user()->user_id )->where( 'is_billed', 0 )->first();
            $no_exist = json_decode(json_encode($no_exist), true);

            if(!empty($no_exist)){

                try {

                    $stripeToken = Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

                    $PAYMENT_METHOD = \Stripe\PaymentMethod::create([
                            'type' => 'card',
                            'card' => [
                              'number' => $data['card-number'],
                              'exp_month' => $data['card-expiry-month'],
                              'exp_year' => $data['card-expiry-year'],
                              'cvc' => $data['card-cvc'],
                            ],
                    ]);
                    // This creates a new Customer and attaches the PaymentMethod in one API call.
                    $customer =  \Stripe\Customer::create([
                        'email' => Auth::user()->email,
                        'description' => $data['fullname'],
                        'payment_method' => $PAYMENT_METHOD->id,
                    ]);

                    $amount = $request->amount;
                    if(!empty($PAYMENT_METHOD->id)){

                        $PAYMENT_METHOD_ID = secureEncrypt($PAYMENT_METHOD->id);
                        $CUSTOMER_ID = secureEncrypt($customer->id);

                        /* update on user table begin*/
                          User::where('user_id',Auth::user()->user_id)->update([
                              'stripe_payment_method_id' => $PAYMENT_METHOD_ID,
                              'stripe_customer_id' => $CUSTOMER_ID,
                          ]);
                        /* update on user table end*/

                        $ID = 0;
                        $currency = 'usd';
                        $receipt_url = '';
                        $paymentCardId = '';  // Id
                        $paymentCardBrand = 'visa'; // visa
                        $status = 'pay_later'; // succeeded    

                        $wallet = 0;
                        if(isset($request->wallet)){
                            $wallet = $request->wallet;
                        }


                        $domain_arr = AddToCart::where('user_id',Auth::user()->user_id)->where('is_billed','=',0)->where('mst_payment_id','=',0)->get()->toArray();
                        $payment = Payment::create([
                                        'stripe_id' => $ID,
                                        'currency' => $currency,
                                        'receipt_url' => $receipt_url,
                                        'payment_card_id' => $paymentCardId,
                                        'status' => $status,
                                        'user_id' => Auth::user()->user_id, // 
                                        'amount' => $amount,
                                        'token' => $request->stripeToken,
                                        'promo_code' => $request->promo_id_card_stripe,
                                        'type'  => 'stripe-save-card',
                                        'payment_type'  => 'stripe-save-card',
                                        'stripe_payment_method_id' => $PAYMENT_METHOD_ID,
                                        'stripe_customer_id' => $CUSTOMER_ID,
                                        'wallet_amount' => $wallet
                                ]);

                        /** add Email **/
                        foreach( $domain_arr as $split_domain_arr ){
                          $domain_id     = $split_domain_arr['domain_id'];
                          $order_id     = $split_domain_arr['id'];
                          $domain = MstDomainUrl::where('domain_id',$domain_id)->with(['websiteuser'])->first();
                          $cart = AddToCart::where('id',$order_id)->where('user_id',Auth::user()->user_id)->first();
                          $publisherFname = $domain->websiteuser->user_code;
                          $publisheremail = $domain->websiteuser->email;

                          $domain_url = $domain->domain_url;

                            $cost = $domain->websiteuser->cost_price;
                            $cost_save = $cost;
                            if( $cart->promo_code == 0 ){
                                if($cart->content_type == 'buyer'){
                                    $cost_save = buyerDomainWebsitePriceCalculate($cart->domainname->cost_price);
                                    $flybiz = $cost_save;
                                    $cost = '$' .$cost_save;
                                    
                                } else {
                                    $cost_save = buyerDomainPriceCalculate($cart->domainname->cost_price, $cart->domainname->extra_cost);
                                    $flybiz = $cost_save;
                                    $cost = '$' .$cost_save;
                                    
                                }
                            } else {
                                if($cart->content_type == 'buyer'){
                                    $cost = 0;
                                } else {
                                    $cost = 0;
                                }
                            }

                            /* webdew discount */
                              if($request->promo_id_card_stripe > 0){
                                  $cost_save = _promo_codes($request->promo_id_card_stripe,$cost_save);
                              }
                            /* webdew discount */

                              $cost_price = MstDomainUrl::where('domain_id',$domain_id)->value('cost_price');
                              //is_billed 5 no payment save card only
                              $data = array('is_billed' => 5, 'mst_payment_id' => $payment->id, 'after_discount_price' => $cost_save,'cost_price' =>$cost_price,'domain_amount' => $flybiz);
                               if($cart->content_type == 'seller'){
                                  $extra_cost = MstDomainUrl::where('domain_id',$domain_id)->value('extra_cost'); 
                                  $data = array('is_billed' => 5, 'mst_payment_id' => $payment->id, 'after_discount_price' => $cost_save,'cost_price' =>$cost_price, 'extra_cost'=>$extra_cost);
                               }
                             
                             AddToCart::where('id',$cart->id)->where('is_billed', 0)->where('user_id',Auth::user()->user_id)->update($data);


                          $AdvertiserFname = @Auth::user()->user_code;
                          if(empty($AdvertiserFname)){
                              $AdvertiserFname = 'Advertiser'; 
                          }
                      if( env('APP_ENV') == 'live'){
                          /* Email template for publiser */
                          $args['email'] = $publisheremail;
                         // $args['email']    = 'randeep.s@webdew.com';
                            $args['subject'] = 'New Order Received';
                            $args['template_id'] = 'd-d7fa1ef8e595481c8e406e51f1d9e5e4';
                            $args['data'] = '{
                                "Website" : {
                                      "name":"'.@$domain_url.'",
                                      "cost":"'.$cost.'"
                                },
                                "Publisher":{
                                      "F_name":"@'.@$publisherFname.'"
                                   },
                                "Advertiser":{
                                      "F_name":"@'.$AdvertiserFname.'"
                                   },
                              }';
                              $this->sendgridEmail($args);
                          /* Email template for publisher end */

                          /* Email template for Advertiser */
                           $args['email'] = Auth::user()->email;
                          //$args['email']    = 'randeep.s@webdew.com';
                            $args['subject'] = 'Placed an Order';
                            $args['template_id'] = 'd-94c82df8c5c743b2aa1e097820865acd';
                            $args['data'] = '{
                                    "Website" : {
                                          "name":"'.@$domain_url.'",
                                          "cost":"'.$cost.'"
                                    },
                                    "Publisher":{
                                          "F_name":"@'.@$publisherFname.'"
                                       },
                                    "Advertiser":{
                                          "F_name":"@'.@$AdvertiserFname.'"
                                       }
                                  }';
                            $this->sendgridEmail($args);
                            /* Email template for Advertiser end */

                              /* flock notification */
                              
                              $notiMessage = 'New Order has been Placed by @'.@Auth::user()->user_code.' for '.@$domain_url.' at price '.$cost;
                              
                              if(env('APP_ENV') == 'local' || env('APP_ENV') == 'stage' ) {
                                  $notiMessage = '<strong>' . env('APP_ENV') . '</strong> New Order has been Placed by @'.@Auth::user()->user_code.' for '.@$domain_url.' at price '.$cost;
                              }
                              flockNotification($notiMessage);

                              /* flock notification */


                            /** Email template for admin **/
                            $adminUsers = getAdmminList();
                            foreach($adminUsers as $admin_users){
                             $args['email']    = $admin_users['email'];
                            //$args['email']    = 'randeep.s@webdew.com';
                              $args['subject']  = 'Placed an Order';
                              $args['template_id'] = 'd-5a534dd45fb148d8892651295e198ac7';
                              $args['data'] = '{
                                      "Website" : {
                                            "name":"'.@$domain_url.'",
                                            "cost":"'.$cost.'"
                                      },
                                      "Publisher":{
                                            "F_name":"@'.@$publisherFname.'"
                                         },
                                      "Advertiser":{
                                            "F_name":"@'.@$AdvertiserFname.'"
                                         }
                                    }';
                              $this->sendgridEmail($args);
                            } 
                        }
                      }
                        $alert = 'success';
                        \Cache::forget('cartlist');
                        $message = 'Order has been placed with later payment.';
                        return view('viewmessage',compact('alert','message'));
                        
                    } else {            
                        $alert = 'danger';
                        $message = 'Order has not been placed something wrong in payment method.';
                        return view('viewmessage',compact('alert','message'));
                    }
                    
                } catch (Exception $e) {

                    $alert = 'danger';
                    $message = 'Order has not been placed something wrong in catch.';
                    return view('viewmessage',compact('alert','message'));
                    
                }

            //}
        }
    }

    public function stripeCardSave(Request $request) {

        $data = $request->all();
        // $customer_id = @Auth::user()->stripe_customer_id;
        // if(empty($customer_id)){
            // User::where('user_id',Auth::user()->user_id)->update(['stripe_customer_id'=>$stripe_customer_id,'stripe_payment_method_id'=>$PAYMENT_METHOD->id]);

            $no_exist = AddToCart::whereUserId( Auth::user()->user_id )->where( 'is_billed', 0 )->first();
            $no_exist = json_decode(json_encode($no_exist), true);

            if(!empty($no_exist)){

                try {

                    $stripeToken = Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));

                    $amount = $request->amount;

                    if(!empty(Auth::user()->stripe_payment_method_id)){

                        $PAYMENT_METHOD_ID = Auth::user()->stripe_payment_method_id;
                        $CUSTOMER_ID = Auth::user()->stripe_customer_id;

                        $ID = 0;
                        $currency = 'usd';
                        $receipt_url = '';
                        $paymentCardId = '';  // Id
                        $paymentCardBrand = 'visa'; // visa
                        $status = 'pay_later'; // succeeded      

                        $wallet = 0;
                        if(isset($request->wallet)){
                            $wallet = $request->wallet;
                        }                  

                        $domain_arr = AddToCart::where('user_id',Auth::user()->user_id)->where('is_billed','=',0)->where('mst_payment_id','=',0)->get()->toArray();
                        $payment = Payment::create([
                                        'stripe_id' => $ID,
                                        'currency' => $currency,
                                        'receipt_url' => $receipt_url,
                                        'payment_card_id' => $paymentCardId,
                                        'status' => $status,
                                        'user_id' => Auth::user()->user_id, // 
                                        'amount' => $amount,
                                        'token' => $request->stripeToken,
                                        'promo_code' => $request->promo_id_card_stripe,
                                        'type'  => 'stripe-save-card',
                                        'payment_type'  => 'stripe-save-card',
                                        'stripe_payment_method_id' => $PAYMENT_METHOD_ID,
                                        'stripe_customer_id' => $CUSTOMER_ID,
                                        'wallet_amount' => $wallet
                                ]);

                        /** add Email **/
                        foreach( $domain_arr as $split_domain_arr ){
                          $domain_id     = $split_domain_arr['domain_id'];
                          $order_id     = $split_domain_arr['id'];
                          $domain = MstDomainUrl::where('domain_id',$domain_id)->with(['websiteuser'])->first();
                          $cart = AddToCart::where('id',$order_id)->where('user_id',Auth::user()->user_id)->first();
                          $publisherFname = $domain->websiteuser->user_code;
                          $publisheremail = $domain->websiteuser->email;

                          $domain_url = $domain->domain_url;

                            $cost = $domain->websiteuser->cost_price;
                            $cost_save = $cost;
                            if( $cart->promo_code == 0 ){
                                if($cart->content_type == 'buyer'){
                                    $cost_save = buyerDomainWebsitePriceCalculate($cart->domainname->cost_price);
                                    $flybiz = $cost_save;
                                    $cost = '$' .$cost_save;
                                    
                                } else {
                                    $cost_save = buyerDomainPriceCalculate($cart->domainname->cost_price, $cart->domainname->extra_cost);
                                    $flybiz = $cost_save;
                                    $cost = '$' .$cost_save;
                                    
                                }
                            } else {
                                if($cart->content_type == 'buyer'){
                                    $cost = 0;
                                } else {
                                    $cost = 0;
                                }
                            }

                            /* webdew discount */
                              if($request->promo_id_card_stripe > 0){
                                  $cost_save = _promo_codes($request->promo_id_card_stripe,$cost_save);
                              }
                            /* webdew discount */

                              $cost_price = MstDomainUrl::where('domain_id',$domain_id)->value('cost_price');
                              //is_billed 5 no payment save card only
                              $data = array('is_billed' => 5, 'mst_payment_id' => $payment->id, 'after_discount_price' => $cost_save,'cost_price' =>$cost_price, 'domain_amount' => $flybiz );
                               if($cart->content_type == 'seller'){
                                  $extra_cost = MstDomainUrl::where('domain_id',$domain_id)->value('extra_cost'); 
                                  $data = array('is_billed' => 5, 'mst_payment_id' => $payment->id, 'after_discount_price' => $cost_save,'cost_price' =>$cost_price, 'extra_cost'=>$extra_cost);
                               }
                             
                             AddToCart::where('id',$cart->id)->where('is_billed', 0)->where('user_id',Auth::user()->user_id)->update($data);


                          $AdvertiserFname = @Auth::user()->user_code;
                          if(empty($AdvertiserFname)){
                              $AdvertiserFname = 'Advertiser'; 
                          }
                          if( env('APP_ENV') == 'live'){
                            /* Email template for publiser */
                            $args['email'] = $publisheremail;
                           // $args['email']    = 'randeep.s@webdew.com';
                              $args['subject'] = 'New Order Received';
                              $args['template_id'] = 'd-d7fa1ef8e595481c8e406e51f1d9e5e4';
                              $args['data'] = '{
                                  "Website" : {
                                        "name":"'.@$domain_url.'",
                                        "cost":"'.$cost.'"
                                  },
                                  "Publisher":{
                                        "F_name":"@'.@$publisherFname.'"
                                     },
                                  "Advertiser":{
                                        "F_name":"@'.$AdvertiserFname.'"
                                     },
                                }';
                                $this->sendgridEmail($args);
                            /* Email template for publisher end */

                            /* Email template for Advertiser */
                             $args['email'] = Auth::user()->email;
                            //$args['email']    = 'randeep.s@webdew.com';
                              $args['subject'] = 'Placed an Order';
                              $args['template_id'] = 'd-94c82df8c5c743b2aa1e097820865acd';
                              $args['data'] = '{
                                      "Website" : {
                                            "name":"'.@$domain_url.'",
                                            "cost":"'.$cost.'"
                                      },
                                      "Publisher":{
                                            "F_name":"@'.@$publisherFname.'"
                                         },
                                      "Advertiser":{
                                            "F_name":"@'.@$AdvertiserFname.'"
                                         }
                                    }';
                              $this->sendgridEmail($args);
                              /* Email template for Advertiser end */

                                /* flock notification */
                                
                                $notiMessage = 'New Order has been Placed by @'.@Auth::user()->user_code.' for '.@$domain_url.' at price '.$cost;
                                
                                if(env('APP_ENV') == 'local' || env('APP_ENV') == 'stage' ) {
                                    $notiMessage = '<strong>' . env('APP_ENV') . '</strong> New Order has been Placed by @'.@Auth::user()->user_code.' for '.@$domain_url.' at price '.$cost;
                                }
                                flockNotification($notiMessage);

                                /* flock notification */


                              /** Email template for admin **/
                              $adminUsers = getAdmminList();
                              foreach($adminUsers as $admin_users){
                               $args['email']    = $admin_users['email'];
                              //$args['email']    = 'randeep.s@webdew.com';
                                $args['subject']  = 'Placed an Order';
                                $args['template_id'] = 'd-5a534dd45fb148d8892651295e198ac7';
                                $args['data'] = '{
                                        "Website" : {
                                              "name":"'.@$domain_url.'",
                                              "cost":"'.$cost.'"
                                        },
                                        "Publisher":{
                                              "F_name":"@'.@$publisherFname.'"
                                           },
                                        "Advertiser":{
                                              "F_name":"@'.@$AdvertiserFname.'"
                                           }
                                      }';
                                $this->sendgridEmail($args);
                              } 
                          }
                      }
                        $alert = 'success';
                        \Cache::forget('cartlist');
                        $message = 'Order has been placed with later payment.';
                        return view('viewmessage',compact('alert','message'));
                        
                    } else {            
                        $alert = 'danger';
                        $message = 'Order has not been placed something wrong in payment method.';
                        return view('viewmessage',compact('alert','message'));
                    }
                    
                } catch (Exception $e) {

                    $alert = 'danger';
                    $message = 'Order has not been placed something wrong in catch.';
                    return view('viewmessage',compact('alert','message'));
                    
                }

            //}
        }
    }

}
