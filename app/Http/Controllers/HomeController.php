<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Auth;
use Crypt;
use Redirect;
use Hash;
use DB;
use App\User;
use App\SearchRecord;
use App\MstDomainUrl;
use App\LogSearchQuery;
use App\Notifications\UserRegisteredSuccessfully;
use Stripe;
//use Netshell\Paypal;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $_apiContext;
    public function __construct()
    {
        //$this->middleware('auth');
    
        // $this->_apiContext = Netshell\PayPal::ApiContext(
        //     config('services.paypal.client_id'),
        //     config('services.paypal.secret'));
        
        // $this->_apiContext->setConfig(array(
        //     'mode' => 'sandbox',
        //     'service.EndPoint' => 'https://api.sandbox.paypal.com',
        //     'http.ConnectionTimeOut' => 30,
        //     'log.LogEnabled' => true,
        //     'log.FileName' => storage_path('logs/paypal.log'),
        //     'log.LogLevel' => 'FINE'
        // ));
    }
    public function testRefunds() {
        
    // $curl = curl_init();

    // curl_setopt_array($curl, array(
    //   //CURLOPT_URL => "https://moz-bulk-da-pa-checker.p.rapidapi.com/moz-data",
    //   CURLOPT_URL => "https://moz-bulk-da-pa-checker.p.rapidapi.com/stats/",
    //   CURLOPT_RETURNTRANSFER => true,
    //   CURLOPT_FOLLOWLOCATION => true,
    //   CURLOPT_ENCODING => "",
    //   CURLOPT_MAXREDIRS => 10,
    //   CURLOPT_TIMEOUT => 30,
    //   CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    //   CURLOPT_CUSTOMREQUEST => "POST",
    //   CURLOPT_POSTFIELDS => "domains=https://webdew.com",
    //   CURLOPT_HTTPHEADER => array(
    //     "content-type: application/x-www-form-urlencoded",
    //     "x-rapidapi-host: moz-bulk-da-pa-checker.p.rapidapi.com",
    //     "x-rapidapi-key: 3bd73fd63cmsh653c45495d91b70p15aef2jsn6c6e3b4a8337",
    //     "X-SEBITES-KEY: iaK8dl_0Q0xuzB0JSR50gkyzxb3am7UCVdRghdN4HISZ0XlF3K"
    //   ),
    // ));

    // $response = curl_exec($curl);
    // $err = curl_error($curl);

    // curl_close($curl);

    // print_r($response);
    // die;
    // if ($err) {
    //   return json_decode($err, true);
    // } else {
    //   return json_decode($response, true);
    // }

        /* refund */
        try {
            $token = \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
            $stripe =  \Stripe\Refund::create([
                'charge' => 'ch_1GIazmKDKg4O8xRKrFBSjxU2',
                'amount' => 1 * 100,
                //"description" => "Test stripe payment from webdew company." ,
                "metadata" => ['message_id' => 'Client not happy for this site.'],
            ]);

            echo '<pre>';
            print_r($stripe);
            $refund_id = $stripe->id;
            $object = $stripe->object;
            $refund_amount = $stripe->amount;
            $balance_txn = $stripe->balance_transaction;
            $ch_id = $stripe->charge;
            $created = $stripe->created;
            $currency = $stripe->currency;
            $status = $stripe->status; // succeeded
            
            
            die;
        } catch (Exception $e) {
            
        }

    }

    public function cors() {
    
        $arr = MstDomainUrl::select('domain_id', 'domain_url', 'cost_price','p','a','h1','h2','h4','h5','h6','h3','title','description')->where('domain_id','>', 15500)->where('cost_price','>',0)->orderBy('domain_id','DESC')->paginate(500);
        $arr = json_decode(json_encode($arr), true);

        $arr1 = [];
        foreach($arr['data'] as $key => $val){
            $arr1[$key] = $val;
            $arr1[$key]['domain_url'] = str_replace('/', '', $val['domain_url']);
        }
        $arr = $arr1;
                
        return view('cors',compact('arr'));
    }

    public function corsStore(){
       
        if(!empty($_POST)){

            $description = '';
            $title = '';    
            $p = '';
            $a = '';
            $h1 = '';   
            $h2 = '';
            $h3 = '';
            $h4 = '';
            $h5 = '';
            $h6 = '';
            $favicon = '';
            
            if(!empty($_POST['title'])){
                //$title = preg_replace('/[^A-Za-z0-9]/', " ", $_POST['title'] );
                $title = $_POST['title'];
            }
            if(!empty($_POST['description'])){
                $description = $_POST['description'];
                //$description = preg_replace('/[^A-Za-z0-9]/', " ", $_POST['description'] );
            }
            if(!empty($_POST['p'])){
                $p = preg_replace('/[^A-Za-z0-9]/', " ", $_POST['p'] );
            }
            if(!empty($_POST['a'])){
                $a = preg_replace('/[^A-Za-z0-9]/', " ", $_POST['a'] );
            }
            if(!empty($_POST['h1'])){
                $h1 = preg_replace('/[^A-Za-z0-9]/', " ", $_POST['h1'] );
            }
            if(!empty($_POST['h2'])){
                $h2 = preg_replace('/[^A-Za-z0-9]/', " ", $_POST['h2'] );
            }
            if(!empty($_POST['h3'])){
                $h3 = preg_replace('/[^A-Za-z0-9]/', " ", $_POST['h3'] );
            }
            if(!empty($_POST['h4'])){
                $h4 = preg_replace('/[^A-Za-z0-9]/', " ", $_POST['h4'] );
            }
            if(!empty($_POST['h5'])){
                $h5 = preg_replace('/[^A-Za-z0-9]/', " ", $_POST['h5'] );
            }
            if(!empty($_POST['h6'])){
                $h6 = preg_replace('/[^A-Za-z0-9]/', " ", $_POST['h6'] );
            }
            if(!empty($_POST['favicon'])) {
                $favicon = $_POST['favicon'];
            }

            if( !empty($p) || !empty($a)  || !empty($h1) || !empty($h2) || !empty($h3) || !empty($h4) || !empty($h5) || !empty($h6) || !empty($description) || !empty($title) || !empty($_POST['id']) || !empty($favicon)){

                $result =    MstDomainUrl::where('domain_id',$_POST['id'])->update([
                    'p' => trim($p), 
                    'a' => trim($a), 
                    'h1'=> trim($h1), 
                    'h2'=> trim($h2), 
                    'h3'=> trim($h3),
                    'h4'=> trim($h4), 
                    'h5'=> trim($h5), 
                    'h6'=> trim($h6),
                    'description'=> trim($description),
                    'title'=> trim($title),
                    'favicon'=> trim($favicon)

                ]);
            
            }
        }
        $arr = MstDomainUrl::select('domain_id', 'domain_url', 'cost_price','p','a','h1','h2','h4','h5','h6','h3','title','description')->where('domain_id',$_POST['id'])->get()->toArray();
        return ['response' => true];
    }

    /** search query **/
    public function ajax_search($REQUEST,$terms, $no_of_page){

            $recursion[0] = [];
            $ids = [];
            $search = SearchRecord::query(); 

            foreach ($terms as $key => $term) {
                $term = trim($term);
                $innerSearch = SearchRecord::query(); 

                if(!empty($ids[$key-1])) {

                    // Use implode() function to join
                    $List = implode(', ', $ids[$key-1]); 
                    
                    $term_list = " a LIKE '%$term%' OR h6 LIKE '%$term%' OR h5 LIKE '%$term%' OR h4 LIKE '%$term%' OR h3 LIKE '%$term%' OR h2 LIKE '%$term%' OR h1 LIKE '%$term%' OR p LIKE '%$term%' OR  domain_website LIKE '%$term%' OR  domain_url LIKE '%$term%' OR title LIKE '%$term%' OR description LIKE '%$term%' OR domain_url LIKE '%$term%' ";
    

                    // $selectQuery = "SELECT domain_id FROM search_records WHERE ( $term_list ) AND domain_id IN ( $List ) GROUP BY domain_url ";
                    $selectQuery = "SELECT domain_id FROM search_records WHERE ( $term_list ) AND domain_id IN ( $List ) GROUP BY domain_url ";


                    //GROUP BY domain_url limit $no_of_page
                    $results = DB::select( DB::raw($selectQuery) );
                    $results = json_decode(json_encode($results), true);
                    if(!empty($results)){                                

                        $i = [];
                        foreach ($results as $key => $value) {
                            $i[] = $value['domain_id'];
                        }
                        $recursion[0] = $i;
                        //$recursion[0] = $ids[$key];
                    } else {
                        break;
                    }
                }

                if($key == 0){
                    
                    $innerSearch->orWhere('title' , 'LIKE','%'.$term.'%');
                    $innerSearch->orWhere('description' , 'LIKE','%'.$term.'%');
                    $innerSearch->orWhere('h1' , 'LIKE','%'.$term.'%');
                    $innerSearch->orWhere('h2' , 'LIKE','%'.$term.'%');
                    $innerSearch->orWhere('h3' , 'LIKE','%'.$term.'%');
                    $innerSearch->orWhere('h4' , 'LIKE','%'.$term.'%');
                    $innerSearch->orWhere('h5' , 'LIKE','%'.$term.'%');
                    $innerSearch->orWhere('h6' , 'LIKE','%'.$term.'%');
                    $innerSearch->orWhere('a' , 'LIKE','%'.$term.'%');
                    $innerSearch->orWhere('p' , 'LIKE','%'.$term.'%');
                    $innerSearch->orWhere('domain_url' , 'LIKE','%'.$term.'%');
                    $innerSearch->orWhere('domain_website' , 'LIKE','%'.$term.'%');
                    $ids[$key] =  $innerSearch->pluck('domain_id')->toArray();
                    $recursion[0] = $ids[$key];
                }

            }

            $search->whereIn('domain_id', $recursion[0]);
            
            if (@$REQUEST['da_start']) {
                $search->where('dp_da', '>=' ,$REQUEST['da_start'])->where('dp_da', '<=' ,$_REQUEST['da_end']);
                
            }
            if (@$REQUEST['traffic_start']) {
                $search->where('sd_visits', '>=' ,$REQUEST['da_start'])->where('sd_visits', '<=' ,$_REQUEST['traffic_end']);
            }

            if (@$REQUEST['orders_start']) {
                $search->where('orders', '>=' ,$REQUEST['orders_start'])->where('orders', '<=' ,$_REQUEST['orders_end']);
            }

            if (@$REQUEST['verify']) {
                $search->where('domain_status', '=' ,1);
            } 

            if (@$REQUEST['generalSearch']) {

                $searchQueryWeb = str_replace(" ", "%", $REQUEST['generalSearch']);
                //$search->where('domain_website', 'LIKE' ,'%'.$searchQueryWeb.'%');
                //$search->where('domain_url', 'LIKE' ,'%'.$searchQueryWeb.'%');
                $search->where('domain_url', '=' ,$searchQueryWeb);
            }

            $selectQuery =  $search->groupBy('domain_url')->pluck('domain_id')->toArray();
            return $selectQuery;

                    
    }

    /** search query **/
    public function ajax_offset($REQUEST){
        $offset = '';
        
        
        $offset .= "&da_start=".$REQUEST['da_start']."&da_end=".$_REQUEST['da_end'];
        
        if($REQUEST['traffic_start'] > 0 || $REQUEST['traffic_end'] < 1000000){
            $offset .= "&traffic_start=".$REQUEST['traffic_start']."&traffic_end=".$REQUEST['traffic_end'];    
        }
        if($REQUEST['orders_start'] > 0 || $REQUEST['orders_end'] < 100){
            $offset .= "&orders_start=".$REQUEST['orders_start']."&orders_end=".$REQUEST['orders_end'];
        }

        if (@$REQUEST['verify']) {
            $offset .= "&verify=1";
        } 

        if (@$REQUEST['generalSearch']) {
            $searchQueryWeb = str_replace(" ", "%", $REQUEST['generalSearch']);
            $offset .= "&generalSearch=".$searchQueryWeb;
        }
        if (@$REQUEST['order']) {
            $offset .= "&order=".$REQUEST['order']."&orderwise=".$REQUEST['orderwise'];
        }
        return $offset;
    }

    /** search query **/
    public function ajax_search_single($REQUEST,$terms, $no_of_page){
            
            $terms = str_replace(array("'", '"','“', '”', "’",'‘', "`", "&quot;"), "", htmlspecialchars($terms));
            // $terms = str_replace('"', "", $terms);
            // $terms = str_replace("'", "", $terms);
       
            $term_list = " a LIKE '%$terms%' OR h6 LIKE '%$terms%' OR h5 LIKE '%$terms%' OR h4 LIKE '%$terms%' OR h3 LIKE '%$terms%' OR h2 LIKE '%$terms%' OR h1 LIKE '%$terms%' OR p LIKE '%$terms%' OR  domain_website LIKE '%$terms%' OR  domain_url LIKE '%$terms%' OR title LIKE '%$terms%' OR description LIKE '%$terms%' OR domain_url LIKE '%$terms%' ";
            
            
            $matchTheseOut = '';
            if (@$REQUEST['da_start']) {
                // $matchTheseOut .= " AND ( dp_da BETWEEN ".$REQUEST['da_start']." AND ".$_REQUEST['da_end']." ) ";

                $matchTheseOut .= " AND ( dp_da >= ".$REQUEST['da_start']." AND dp_da <= ".$_REQUEST['da_end']." ) ";
            }
            if (@$REQUEST['traffic_start']) {
                //$matchTheseOut .= " AND ( sd_visits BETWEEN ".$REQUEST['traffic_start']." AND ".$REQUEST['traffic_end']." )";
                $matchTheseOut .= " AND  ( sd_visits >= ".$REQUEST['traffic_start']." AND <= ".$REQUEST['traffic_end']." )";
            }
            // if (@$REQUEST['bounce_start']) {
            //     $matchTheseOut .= "AND ( sd_bounce_rate BETWEEN ".$REQUEST['bounce_start']." AND " .$REQUEST['bounce_end']." )"; 
            // }

            if (@$REQUEST['orders_start']) {
                $matchTheseOut .= " AND  ( orders >= ".$REQUEST['orders_start']." AND <= ".$REQUEST['orders_end']." )";
            }

            if (@$REQUEST['verify']) {
                $matchTheseOut .= " AND domain_status = 1";
            } 

            if (@$REQUEST['generalSearch']) {
                $searchQueryWeb = str_replace(" ", "%", $REQUEST['generalSearch']);
                //$matchTheseOut .= " AND domain_website LIKE '%".$searchQueryWeb."%' ";
                //$matchTheseOut .= " AND domain_url LIKE '%".$searchQueryWeb."%' ";
                $matchTheseOut .= " AND domain_url = '".$searchQueryWeb."'";
            }

            $selectQuery = "SELECT domain_id FROM search_records WHERE ( $term_list ) AND (domain_approval_status = 1 $matchTheseOut ) GROUP BY domain_url ";
            

            //GROUP BY domain_url limit $no_of_page
            $results = DB::select( DB::raw($selectQuery) );            
            $ids = [];
            foreach ($results as $key => $value) {
                $ids[] = $value->domain_id;
            }
            return $ids;

    }
    /** search query **/
    public function log_searchquery($q = '', $total_results = 0){

        date_default_timezone_set('Asia/Kolkata');
        $date = date('Y-m-d');
        $datetime = date('Y-m-d H:i:s');
        // Remove special characters
        if(!empty($q)){
            $q = preg_replace('/[^A-Za-z0-9]/', ' ', $q);

            if(Auth::check()){
                $last = LogSearchQuery::where('query_created_date','>',$date.' 00:00:00')->where('query_created_date','<',$date.' 23:59:59')->where('query_fk_user',Auth::user()->user_id)->where('query_string',$q)->first();
                $last = json_decode(json_encode($last), true);

                $query_index = 1;
                if(!empty($last)){
                    
                    if(!empty($last['query_index'])) {
                        $query_index = $last['query_index'] + 1;
                    }                

                    $last = DB::table('log_searchquery')->where('query_id',$last['query_id'])->update([
                            'query_index' => $query_index,
                            'query_results_count' => $total_results
                        ]);

                } else {
                    
                    $last = LogSearchQuery::where('query_id',$last['query_id'])->insert([
                            'query_index' => $query_index,
                            'query_results_count' => $total_results,
                            'query_string' => $q,
                            'query_created_date' => $datetime,
                            'query_fk_user' => Auth::user()->user_id,
                        ]);
                }
            } else {
                $last = LogSearchQuery::where('query_created_date','>',$date.' 00:00:00')->where('query_created_date','<',$date.' 23:59:59')->where('query_fk_user',NULL)->where('query_string',$q)->first();
                $last = json_decode(json_encode($last), true);
                $query_index = 1;
                if(!empty($last)){
                    
                    if(!empty($last['query_index'])) {
                        $query_index = $last['query_index'] + 1;
                    }                

                    $last = DB::table('log_searchquery')->where('query_id',$last['query_id'])->update([
                            'query_index' => $query_index,
                            'query_results_count' => $total_results
                        ]);

                } else {
                    
                    $last = LogSearchQuery::where('query_id',$last['query_id'])->insert([
                            'query_index' => $query_index,
                            'query_results_count' => $total_results,
                            'query_string' => $q,
                            'query_created_date' => $datetime,
                        ]);
                }

            }    
        }
        
    }

    public function editProfile($id, Request $request){
        $user_code = $id;
        $user_data = DB::table('mst_user')
                    ->join('mst_domain_url', 'mst_user.user_id', '=', 'mst_domain_url.domain_fk_user_id')
                    ->where('mst_user.user_code',$user_code)
                    ->where('mst_domain_url.cost_price','>',0)
                    ->get()->toArray();
        // echo '<pre>';
        // print_r($user_data);
        // die;
        $_REQUEST['q'] = 'blog';
        if(isset($_REQUEST['q'])) {
            
            $query = $_REQUEST['q'];
            $query = str_replace(array("'", '"','“', '”', "’",'‘', "`", "&quot;"), "", htmlspecialchars($query), $quotes);

            $no_of_page = 10;
            $offset = '';
            if(isset($_REQUEST['offset'])){
                $no_of_page = $_REQUEST['offset'];
                $offset = '&offset=' . $_REQUEST['offset'];
            }

            $orderBy = 'dp_da';
            //$orderBy = 'cost_price';
            $orderwise = 'DESC';
            
            if(@$_REQUEST['order']) {
                $orderBy = $_REQUEST['order'];
                $orderwise = $_REQUEST['orderwise'];
                $offset .= '&order=' . $orderBy . '&orderwise=' . $orderwise;
            }

            $terms = [];    
            $terms = explode(' ', $query);
            $search = SearchRecord::query(); 
            $recursion[0] = [];
            if($request->ajax()) {
                // ajax result    
                // $ids = $this->ajax_search($_REQUEST, $terms,$no_of_page);
                // $search = SearchRecord::query()->whereIn('domain_id',$ids);
                $quotes = $_REQUEST['quotes'];
                if($quotes == 0){
                    $ids = $this->ajax_search($_REQUEST, $terms,$no_of_page);
                    $ajax_offset = $this->ajax_offset($_REQUEST);
                    $offset .= $ajax_offset;
                    $recursion[0] = $ids;
                } else {
                    $ids = $this->ajax_search_single($_REQUEST, $query,$no_of_page);
                    $ajax_offset = $this->ajax_offset($_REQUEST);
                    $offset .= $ajax_offset;
                    $recursion[0] = $ids;
                }

                if (@$_REQUEST['da_start'] || @$_REQUEST['generalSearch'] || @$_REQUEST['traffic_start'] || @$_REQUEST['bounce_start'] || @$_REQUEST['orders_start'] || @$_REQUEST['verify']) {
                    $path = 'render.search';
                }


            }  else {
                if($quotes == 0){
                    $counting = count($terms);
                    $ids = [];

                    foreach ($terms as $key => $term) {
                        $term = trim($term);
                        $innerSearch = SearchRecord::query(); 
                        if(!empty($ids[$key-1])) {

                            // Use implode() function to join
                            $List = implode(', ', $ids[$key-1]); 
                            
                            $term_list = " a LIKE '%$term%' OR h6 LIKE '%$term%' OR h5 LIKE '%$term%' OR h4 LIKE '%$term%' OR h3 LIKE '%$term%' OR h2 LIKE '%$term%' OR h1 LIKE '%$term%' OR p LIKE '%$term%' OR  domain_website LIKE '%$term%' OR  domain_url LIKE '%$term%' OR title LIKE '%$term%' OR description LIKE '%$term%' OR domain_url LIKE '%$term%' ";
            

                            $selectQuery = "SELECT domain_id FROM search_records WHERE ( $term_list ) AND domain_id IN ( $List ) GROUP BY domain_url ";


                            //GROUP BY domain_url limit $no_of_page
                            $results = DB::select( DB::raw($selectQuery) );
                            $results = json_decode(json_encode($results), true);
                            if(!empty($results)){                                

                                $i = [];
                                foreach ($results as $key => $value) {
                                    $i[] = $value['domain_id'];
                                }
                                $recursion[0] = $i;
                                //$recursion[0] = $ids[$key];
                            } else {
                                break;
                            }
                        }

                        if($key == 0){

                            $innerSearch->orWhere('title' , 'LIKE','%'.$term.'%');
                            $innerSearch->orWhere('description' , 'LIKE','%'.$term.'%');
                            $innerSearch->orWhere('sd_descr_n' , 'LIKE','%'.$term.'%');
                            $innerSearch->orWhere('sd_descr' , 'LIKE','%'.$term.'%');    
                            $innerSearch->orWhere('h1' , 'LIKE','%'.$term.'%');
                            $innerSearch->orWhere('h2' , 'LIKE','%'.$term.'%');
                            $innerSearch->orWhere('h3' , 'LIKE','%'.$term.'%');
                            $innerSearch->orWhere('h4' , 'LIKE','%'.$term.'%');
                            $innerSearch->orWhere('h5' , 'LIKE','%'.$term.'%');
                            $innerSearch->orWhere('h6' , 'LIKE','%'.$term.'%');
                            $innerSearch->orWhere('a' , 'LIKE','%'.$term.'%');
                            $innerSearch->orWhere('p' , 'LIKE','%'.$term.'%');
                            $innerSearch->orWhere('domain_url' , 'LIKE','%'.$term.'%');
                            $innerSearch->orWhere('domain_website' , 'LIKE','%'.$term.'%');
                            $ids[$key] =  $innerSearch->pluck('domain_id')->toArray();
                            $recursion[0] = $ids[$key];
                        }

                    }

                } else {

                    $term = str_replace(array("'", '"','“', '”', "’",'‘', "`","&quot;"), "", htmlspecialchars($query) );

                    $innerSearch = SearchRecord::query();
                    $innerSearch->orWhere('title' , 'LIKE','%'.$term.'%');
                    $innerSearch->orWhere('description' , 'LIKE','%'.$term.'%');
                    $innerSearch->orWhere('h1' , 'LIKE','%'.$term.'%');
                    $innerSearch->orWhere('h2' , 'LIKE','%'.$term.'%');
                    $innerSearch->orWhere('h3' , 'LIKE','%'.$term.'%');
                    $innerSearch->orWhere('h4' , 'LIKE','%'.$term.'%');
                    $innerSearch->orWhere('h5' , 'LIKE','%'.$term.'%');
                    $innerSearch->orWhere('h6' , 'LIKE','%'.$term.'%');
                    $innerSearch->orWhere('a' , 'LIKE','%'.$term.'%');
                    $innerSearch->orWhere('p' , 'LIKE','%'.$term.'%');
                    $innerSearch->orWhere('domain_url' , 'LIKE','%'.$term.'%');
                    $innerSearch->orWhere('domain_website' , 'LIKE','%'.$term.'%');
                    $recursion[0] =  $innerSearch->pluck('domain_id')->toArray();

                    /* with quotes */
                }
                
                $path = 'render.search';


                /* after filtered */
                $REQUEST = $_REQUEST;
                if (isset($REQUEST['da_start'])) {
                    $search->where('dp_da', '>=' ,$REQUEST['da_start'])->where('dp_da', '<=' ,$_REQUEST['da_end']);
                    $offset .= '&da_start='.$REQUEST['da_start'].'&da_end='.$REQUEST['da_end'];
                }
                if (isset($REQUEST['traffic_start'])) {

                    //$search->whereBetween('sd_visits', array($REQUEST['traffic_start'], $_REQUEST['traffic_end']));

                    $search->where('sd_visits', '>=' ,$REQUEST['traffic_start'])->where('sd_visits', '<=' ,$_REQUEST['traffic_end']);

                    $offset .= '&traffic_start='.$REQUEST['traffic_start'].'&traffic_end='.$REQUEST['traffic_end'];
                }

                if (isset($REQUEST['orders_start'])) {
                    //$search->whereBetween('orders', array($REQUEST['orders_start'], $_REQUEST['orders_end']));
                    $search->where('orders', '>=' ,$REQUEST['orders_start'])->where('orders', '<=' ,$_REQUEST['orders_end']);
                    $offset .= '&orders_start='.$REQUEST['orders_start'].'&orders_end='.$REQUEST['orders_end'];
                }

                if (isset($REQUEST['verify'])) {
                    $search->where('domain_status', '=' ,1);
                    $offset .= '&verify=1';
                } 

                if (isset($REQUEST['generalSearch'])) {

                    $searchQueryWeb = str_replace(" ", "%", $REQUEST['generalSearch']);
                    //$search->where('domain_website', 'LIKE' ,'%'.$searchQueryWeb.'%');
                    //$search->where('domain_url', 'LIKE' ,'%'.$searchQueryWeb.'%');
                    $search->where('domain_url', '=' ,$searchQueryWeb);
                    $offset .= '&generalSearch='.$REQUEST['generalSearch'];
                }

                /* after filtered */

            }
            
            $search->select('domain_id','domain_url','backlink_type','sd_title','sd_title_n','title','sd_descr','sd_descr_n','description','cost_price','dp_da','sd_global_rank','sd_visits','orders','domain_status')->whereIn('domain_id', $recursion[0])->where('domain_approval_status',1);

            $selectQuery =  $search->groupBy('domain_url')->orderBy($orderBy,$orderwise)->paginate($no_of_page);
            //store search start
            $this->log_searchquery($_REQUEST['q'],$selectQuery->total());
            // store search end 
            $search_record = $selectQuery->total();
            $record_found = json_decode(json_encode($selectQuery),true);            
            $record_found = count($record_found['data']);
            
            $selectQuery->withPath('?q=' . $_REQUEST['q'] . $offset);
            //return view($path,compact('selectQuery','record_found','search_record'));
            $search = view($path,compact('selectQuery','record_found','search_record'));
            
        } 
        // if(empty( $user_data )){
            $user_data = DB::table('mst_user')
                    ->where('mst_user.user_code',$user_code)
                    ->first();
         //}
        return view('profile.profile',compact('selectQuery','record_found','search_record','search','user_data'));
    }
    public function payToSeller(){

        //api request
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://svcs.sandbox.paypal.com/AdaptivePayments/Pay');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "actionType=PAY&senderEmail=sourav.v@webdew.com&cancelUrl=http://localhost/projects/laravel/gpe_laravel&currencyCode=USD&receiverList.receiver(0).email=sb-mqi7e794075@personal.example.com&receiverList.receiver(0).amount=120.00&requestEnvelope.errorLanguage=en_US&returnUrl=http://localhost/projects/laravel/gpe_laravel");

        $headers = array();
        $headers[] = 'X-Paypal-Security-Userid: sourav.v_api1.webdew.com';
        $headers[] = 'X-Paypal-Security-Password: ZMP7ZFB55EWYTRCL';
        $headers[] = 'X-Paypal-Security-Signature: AzUgrvIJb-c6le-WEh-8XtXSBw7vA3chiRPq0RM7DnTGQ6yPNPT5ShRd';
        $headers[] = 'X-Paypal-Request-Data-Format: NV';
        $headers[] = 'X-Paypal-Response-Data-Format: NV';
        $headers[] = 'X-Paypal-Application-Id: APP-80W284485P519543T';
        $headers[] = 'Content-Type: application/x-www-form-urlencoded';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result     = curl_exec($ch);
        $keyArray = explode("&", $result);
        foreach ($keyArray as $rVal){
        list($qKey, $qVal) = explode ("=", $rVal);
            $kArray[$qKey] = $qVal;
        }
        
     //print the response to screen for testing purposes
       if( $kArray['responseEnvelope.ack'] ){
            echo $kArray['responseEnvelope.ack'];
       }
       echo "<pre>";
       print_r($kArray);
       die;
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
        return view('/home');
    }
    public function about()
    {        
        return view('/about');
    }
    public function legal()
    {        
        return view('/legal');
    }
    public function contact()
    {        
        return view('/contact');
    }
    public function website_audit()
    {        
        return view('/website-audit');
    }
    public function moz_metrix()
    {
        return view('/domain_authority');
    }
    public function dataForRapidMoz()
    {
        $webUrl     = $_POST['url'];
        $filterUrl  = preg_replace( "#^[^:/.]*[:/]+#i", "", $webUrl);
        $domainUrl = preg_replace('/^www\./', '', $filterUrl);

        $url    = str_replace('/', '', $filterUrl);
        $curl   = curl_init();

        curl_setopt_array($curl, array(
          //CURLOPT_URL => "https://moz-bulk-da-pa-checker.p.rapidapi.com/moz-data",
          CURLOPT_URL => "https://moz-bulk-da-pa-checker.p.rapidapi.com/stats/",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "domains=".$url,
          CURLOPT_HTTPHEADER => array(
            "content-type: application/x-www-form-urlencoded",
            "x-rapidapi-host: moz-bulk-da-pa-checker.p.rapidapi.com",
            "x-rapidapi-key: 3bd73fd63cmsh653c45495d91b70p15aef2jsn6c6e3b4a8337",
            "X-SEBITES-KEY: iaK8dl_0Q0xuzB0JSR50gkyzxb3am7UCVdRghdN4HISZ0XlF3K"

          ),
        ));

        $response = curl_exec($curl);

        $err      = curl_error($curl);
        curl_close($curl);

        if ($err) {
          
            return json_decode($err, true);
        
        } else {
            
            $record_found  =  json_decode($response, true);
            return view('/render/search_authority', compact('record_found','domainUrl'));
        }

    }
}
?>
