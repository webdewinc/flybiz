<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Auth;
use Crypt;
use Stripe;
use Redirect;
use Hash;
use DB;
use App\AddToCart;
use App\MstDomainUrl;

class PaymentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        //switchedOther();
    }

    /**
     * Show the application profile.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function ajax_cart(Request $request)
    {
        
        if($request->type_user == 'seller'){
            $validatedData  = [
            'url'     => 'required|string|max:200',
            'keyword' => 'required|string|max:70',
            //'cktext'  => 'string|min:10|max:1000',
            'title'   => 'required|string|max:70'
        ];
        } else {
            $validatedData  = [
                'url'     => 'required|string|max:200',
                'keyword' => 'required|string|max:70'
            ];
        }

        $data = $request->all();
        $data['cktext'] = strip_tags(@$request->cktext);

        $v = \Validator::make($data, $validatedData);

        if ($v->fails()) {
            $errors = $v->errors();
            $response = ['status'=> false,'alert' => 'danger', 'message'=> 'Please fill required field.', 'errors' =>  $errors];            
            return response()->json($response);
        } 

        $user_id = Auth::user()->user_id;
        $domain_id = $request->domain_id;
        $promo_code_type    = $request->promo_code_type;
        $promo_code         = $request->promo_code;

        $price = MstDomainUrl::where('domain_id',$domain_id)->value('cost_price');
        $extra_cost = MstDomainUrl::where('domain_id',$domain_id)->value('extra_cost');

        if($request->type_user == 'seller'){
            $price = buyerDomainPriceCalculate($price,$extra_cost);
        } else {
            $price = buyerDomainWebsitePriceCalculate($price);
        }

        $seller_id = DB::table('assigned_websites')->where('mst_domain_url_id',$domain_id)->value('user_id');
        $exist = DB::table('add_to_carts')->where('user_id',$user_id)->where('domain_id',$domain_id)->where('is_billed', 0)->first();

        $exist = json_decode(json_encode($exist), true);

        if(empty($exist)){
            $data = $request->all();
            $domain_url = MstDomainUrl::where('domain_id',$domain_id)->value('domain_url');
            // DB::table('mst_domain_url')->where('domain_id',$data['domain_id'])->update(array('cost_price'=>$data['cost_price']));

            $insert = ['user_id' => $user_id, 'domain_id' => $domain_id,'fk_domain_url' => $domain_url,'domain_user_id' => $seller_id, 'content_type' => $request->type_user, 'is_billed' => 0, 'domain_amount' => $price ,'promo_code_type' => $data['promo_code_type'], 'promo_code'=>$data['promo_code'],'after_discount_price' =>$price,'title' => @$request->title, 'content' => @$request->cktext ,'url' => @$request->url, 'keyword' => @$request->keyword];
            $insertid = AddToCart::create($insert);
            $html = view('layout.include.profileFull')->render();
            $response = ['message'=> 'Added into cart', 'alert' => 'success', 'status'=> true,'html' => $html];
            //$response = ['message'=> 'Added into cart', 'alert' => 'success', 'status'=> true];
            
        } else {
            
            $response = ['message'=> 'This item already in cart.', 'alert' => 'warning', 'status'=> false];
        }
        return response()->json($response);
        

    }
    public function cart(Request $request)
    {

        $cartList = DB::table('add_to_carts')
        ->leftjoin('mst_user','mst_user.user_id','add_to_carts.user_id')
        ->leftjoin('mst_domain_url','mst_domain_url.domain_id','add_to_carts.domain_id')
        ->leftjoin('mst_da_pa','mst_da_pa.dp_fk_domain','mst_domain_url.domain_id')
        ->leftjoin('mst_seo_data','mst_seo_data.sd_fk_domain','mst_domain_url.domain_id')
        ->where('add_to_carts.user_id', Auth::user()->user_id)
        ->where('add_to_carts.is_billed', 0)
        ->get()->toArray();

        $count = count($cartList);
        $stripeToken = Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $cardlist = '';
        $customer = '';
        if(Auth::user()->stripe_payment_method_id){
            $cardlist = \Stripe\PaymentMethod::retrieve(
              secureDecrypt(Auth::user()->stripe_payment_method_id)
            );
            $customer = \Stripe\Customer::retrieve(
                secureDecrypt(Auth::user()->stripe_customer_id)
            );
        }

        if($count > 0):
            return view('buyer.cart',compact('cartList','count','cardlist','customer'));
        else :
            $alert = 'warning';
            $message = 'Cart empty.';
            return view('viewmessage-cart',compact('alert','message'));
        endif;
    }

    public function delete(Request $request) {
        $domain_id = AddToCart::where('id',$request->id)->value('domain_id');
        if( \Cache::has( 'cartlist' ) ) {
          $get = Cache::get( 'cartlist' );
          $get = @unserialize($get);
          $store = [];
          foreach ($get as $k => $v) {
              if($domain_id != $v){
                    $store[] = $v;  
              }
              
          }
           $store = serialize($store);
           \Cache::forever('cartlist', $store);
        }
        $delete = AddToCart::where('id',$request->id)->delete();
        return $response = ['message'=> 'Item has been deleted.','alert'=> 'success', 'url' => url('advertiser/cart')];
    }

    // public function testPay(){
        

    //      $total =  preg_replace('/[^0-9]+/', '',100);
         
    //      // $owner_request_response_id = $request->owner_request_response_id;
    //      // $booking_request_id        = $request->booking_request_id;
    //      // $resort_id                 = $request->resort_id;
    //       $total_pay_amount          = 110;
          
    //      $paypal_email = 'sb-su8j21153818@personal.example.com';

    //        $str_req="actionType=PAY&clientDetails_applicationId=APP-80W284485P519543T&clientDetails_ipAddress=127.0.0.1&currencyCode=USD&feesPayer=EACHRECEIVER&memo=Example&receiverList.receiver(0).amount=100&receiverList.receiver(0).email=$paypal_email&receiverList.receiver(0).primary=true&requestEnvelope.errorLanguage=en_US&returnUrl=http://localhost/QuoteStay/guest/payment/success/&cancelUrl=http://localhost/QuoteStay/"; 


    //        $ch = curl_init();
    //    curl_setopt($ch, CURLOPT_URL, "https://svcs.sandbox.paypal.com/AdaptivePayments/Pay");
    //    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    //    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    //    curl_setopt($ch, CURLOPT_HTTPHEADER, $this->getInvoiceAPIHeader());   

    //    curl_setopt($ch, CURLOPT_HEADER, 1); // tells curl to include headers in response, use for testing 
    //    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    //    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    //    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    //    curl_setopt($ch, CURLOPT_POST, TRUE);


       






    //    // setting the NVP $my_api_str as POST FIELD to curl
    //    if(!empty($str_req)){


    //       $log_req = $aryRresponse = explode("&", $str_req);
        
    //       curl_setopt($ch, CURLOPT_POSTFIELDS, $str_req);

    //       // getting response from server
    //       $httpResponse = curl_exec($ch);
    //       //var_dump($httpResponse);
    //       // $aryRresponse = explode("&", $httpResponse);
    //       $response=explode("&",$httpResponse);
          
    //       if(!empty($response)){
    //          $re = explode("responseEnvelope.ack=", $response[1]);
    //          $re4 = explode('payKey=', $response[4]);
                      
    //          if($re[1]=="Success"){
               
               
    //              $resp_array=array('status'=>$re[1], 'link'=>'https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_ap-payment&paykey='.$re4[1],'payKey'=>$re4[1]);
    //              $data['payKey']=$re4[1];

    //              // if(isset($data)){
    //              //  $payment_success                    = new PaymentDetail;
    //              //  $payment_success->paykey               = $data;
    //              //  $payment_success->paykey               = implode(',',$payment_success->paykey);
    //              //  $payment_success->owner_request_response_id = $owner_request_response_id;
    //              //  $payment_success->booking_request_id      = $booking_request_id;
    //              //  $payment_success->resort_id            = $resort_id;
    //              //  $payment_success->guest_id                = Auth::user()->id;
    //              //  $payment_success->paid_amount             = $total;
    //              //  $payment_success->total_pay_amount          = $total_pay_amount;
                  
    //              //  if ($payment_success->save()) {
                     
    //              //     $payment_sucs                     = new Payment;
    //              //     $payment_sucs->booking_id               = rand(111111,999999);
    //              //     $payment_sucs->name                     = $request->card_holder_name;
    //              //     $payment_sucs->card_number              = $request->card_no;
    //              //     $payment_sucs->month                 = $request->month;
    //              //     $payment_sucs->year               = $request->year;
    //              //     $payment_sucs->cvv_number            = $request->cvv_no;
    //              //     $payment_sucs->total                  = $request->total;
    //              //     $payment_sucs->owner_request_response_id = $owner_request_response_id;
    //              //     $payment_sucs->booking_request_id       = $booking_request_id;
    //              //     $payment_sucs->resort_id             = $resort_id;
    //              //     $payment_sucs->guest_id              = Auth::user()->id;


    //              //     $random_no = Payment::select('booking_id')->get();

    //              //     foreach ($random_no as $key => $value) {
                  
    //              //        $booking_id = $value->booking_id;
    //              //        if ($booking_id == $payment_sucs->booking_id) {
    //              //           echo 'Try again after sometime later.'; die;
    //              //        }
    //              //     }
                     
    //              //     if ($payment_sucs->save()) {

    //              //        return redirect('https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_ap-payment&paykey='.$re4[1]);
                        
    //              //     }else{
    //              //        return redirect()->back()->with('error',COMMON_ERROR);
    //              //     }
    //              //  }else{
    //              //     return redirect()->back()->with('error',COMMON_ERROR);
    //              //  }
    //              // }
    //           }
    //           //else{
    //            //return redirect()->back()->with('error',COMMON_ERROR);
    //           //}
    //      }
    //    }
       
    // }
}
