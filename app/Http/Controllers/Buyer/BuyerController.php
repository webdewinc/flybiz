<?php

namespace App\Http\Controllers\Buyer;

use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\API\APIController;
use Auth;
use Crypt;
use Redirect;
use Hash;
use App\Payment;
use App\AddToCart;
use App\MstDomainUrl;
use DB;
use App\Wallet;

class BuyerController extends APIController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

      public function content($order_id){
        $id = \Crypt::decrypt($order_id);
        $order_details = AddToCart::where('id',$id)->first();
        $number_words = MstDomainUrl::where('domain_id',$order_details['domainname']['domain_id'])->value('number_words');
        return view('buyer.content',compact('order_details','order_id','number_words'));
    }
    public function content_POST(Request $request)
    {   
        $user_id = Auth::user()->user_id;
        $order_id = \Crypt::decrypt($request->order_id);
        $domain_id = AddToCart::where('id',$order_id)->value('domain_id');
        $content_type = AddToCart::where('id',$order_id)->value('content_type');
        $domain = MstDomainUrl::where('domain_id',$domain_id)->first();
        $val = '|max:60000';
        // if($domain->number_words > 0){
        //     $val = '|max:'.$domain->number_words;
        // }
        $validatedData  = [
            'url'     => 'required|string|max:200',
            'keyword' => 'required|string|max:70',
            'title'   => 'required|string|max:70'
        ];
        if($content_type == 'buyer'){
            $validatedData  = [
              'url'     => 'required|string|max:200',
              'keyword' => 'required|string|max:70',
              'cktext'  => 'required|string'.$val,
              'title'   => 'required|string|max:70'
          ];  
        }

        $data = $request->all();
        $data['cktext'] = strip_tags(@$request->cktext);

        $v = \Validator::make($data, $validatedData);

        if ($v->fails()) {
            $errors = $v->errors();
            $response = ['status'=> false,'alert' => 'danger', 'message'=> 'Please fill required field.', 'errors' =>  $errors];
        } else {
            if($content_type == 'buyer'){
              $insert = ['title' => @$request->title, 'content' => @$request->cktext ,'url' => @$request->url, 'keyword' => @$request->keyword, 'stages' => 4];
              $insertid = AddToCart::where('id',$order_id)->update($insert);
              $response = ['message'=> 'Content has been updated.', 'alert' => 'success', 'status'=> true];
            } else {
                $insert = ['title' => @$request->title,'url' => @$request->url, 'keyword' => @$request->keyword, 'stages' => 4];
                $insertid = AddToCart::where('id',$order_id)->update($insert);
                $response = ['message'=> 'Content has been updated.', 'alert' => 'success', 'status'=> true];
            }
        }
        
        return response()->json($response);
    }
    public function dashboard(){
        $user_id = Auth::user()->user_id;
        $countOrders = AddToCart::where('user_id',$user_id)->where('refund_id','<',1)->where('is_billed','>',0)->count();
        // $getOrders = DB::table('add_to_carts')->join('mst_domain_url','mst_domain_url.domain_id','add_to_carts.domain_id')->where('add_to_carts.user_id',$user_id)->where('add_to_carts.is_billed',1)->where('refund_id','<',1)->select([ DB::raw("SUM(mst_domain_url.extra_cost + mst_domain_url.cost_price)  as totalAmount") ])->get()->toArray();

        $seller = DB::table('add_to_carts')
                      ->join('mst_domain_url','mst_domain_url.domain_id','add_to_carts.domain_id')
                      ->where('add_to_carts.promo_code',0)
                      ->where('add_to_carts.content_type','seller')
                      ->where('add_to_carts.user_id',$user_id)
                      ->where('add_to_carts.is_billed',1)                      
                      ->sum('mst_domain_url.cost_price');
        $seller_extra = DB::table('add_to_carts')
                      ->join('mst_domain_url','mst_domain_url.domain_id','add_to_carts.domain_id')
                      ->where('add_to_carts.promo_code',0)
                      ->where('add_to_carts.content_type','seller')
                      ->where('add_to_carts.user_id',$user_id)
                      ->where('add_to_carts.is_billed',1)                      
                      ->sum('mst_domain_url.extra_cost');
       
        if(!empty($seller)){
            $seller = buyerDomainPriceCalculate($seller,$seller_extra);
        }
        $buyer = DB::table('add_to_carts')
                      ->join('mst_domain_url','mst_domain_url.domain_id','add_to_carts.domain_id')
                      ->where('add_to_carts.user_id',$user_id)
                      ->where('add_to_carts.is_billed',1)
                      ->where('add_to_carts.promo_code',0)
                      ->where('add_to_carts.content_type','buyer')
                      ->sum('mst_domain_url.cost_price');
        
        if(!empty($buyer)){
            $buyer = buyerDomainWebsitePriceCalculate($buyer);
        }

        $result = (float)$seller + (float)$buyer;

        return view('buyer.dashboard',compact('result','countOrders'));
    }
    /** function to get order lists **/   
    public function purchases(){
        
        $purchases = AddToCart::where('mst_payment_id','>',0)->where('user_id',Auth::user()->user_id)->orderBy('id','DESC')->with(['domainname','username','domainusername','paymentDetails.get_promocode','refundDetails'])->get()->toArray();
        $data = [];
        foreach ($purchases as $key => $value) {

            $id = $value['id'];
            $encrypt_id = \Crypt::encrypt($id); 
            $data[$key]['RecordID']     = $value['id'];
            $data[$key]['OrderDate']    = $value['payment_details']['created_at'];
            $data[$key]['Seller']       = '@'.$value['domainusername']['user_code'];
            $data[$key]['content_type'] = $value['content_type'];
            $data[$key]['payment_type'] = $value['payment_details']['type'];
            $data[$key]['is_accept']    =  $value['is_accept'];
            $data[$key]['stages']       =  $value['stages'];
            $data[$key]['live_link']    = $value['live_link'];
            $data[$key]['domain_id']    = $value['domain_id'];
            $data[$key]['comment']      = $value['comment'];
            $data[$key]['domain_url']   = $value['fk_domain_url']; 

            /* refund */
            $data[$key]['refund'] = 1;
            if($value['refund_id'] > 0){
                $data[$key]['refund'] = 2;
            }

            $data[$key]['refund_amount'] = 'NA';
           
            if(!empty($value['refund_details'])) {
                if($value['refund_details']['type'] == 'paypal'){
                    $amount = 'Amount $'.$value['refund_details']['total_amount'] . ' + Paypal Transaction Fees $' . $value['refund_details']['txn_fee_refund'] . ' = Refunded Amount $' . $value['refund_details']['amount'];
                    $data[$key]['refund_amount'] = $amount;
                }
                if($value['refund_details']['type'] == 'stripe'){
                    $amount = 'Refunded Amount $' . $value['refund_details']['amount'];
                    $data[$key]['refund_amount'] = $amount;  
                }
                if($value['refund_details']['type'] == 'manually'){
                    $amount = 'Refunded Amount $' . $value['refund_details']['amount'];
                    $data[$key]['refund_amount'] = $amount;  
                }
                if($value['refund_details']['type'] == 'stripe-save-card'){
                    $amount = 'Virtual Refunded Amount $' . $value['refund_details']['amount'];
                    $data[$key]['refund_amount'] = $amount;  
                    $data[$key]['refund'] = 3;
                }
            }
            /* refund */

            
            $data[$key]['SellerURL'] = $value['domainname']['domain_url'];
            
            if($value['payment_details']['status'] == 'succeeded' || $value['payment_details']['status'] == 'COMPLETED' || $value['payment_details']['status'] == 1 ) {
                if($value['is_billed'] == 1){
                    $data[$key]['PaymentStatus'] = 1;  
                      
                } else {
                    $data[$key]['PaymentStatus'] = 2;
                }
            } else {
                if($value['is_billed'] == 1){
                    $data[$key]['PaymentStatus'] = 1;
                } else {
                    $data[$key]['PaymentStatus'] = 2;
                }
            }

            $data[$key]['cost_price'] = '$' .$value['cost_price'];
            if($value['content_type'] == 'buyer'){
                  $amount = '$'.$value['domain_amount'];
                  $data[$key]['extra_cost'] = 0;
            } else {
                  $amount = '$'.$value['domain_amount'];
                  $data[$key]['extra_cost'] = '$'.$value['extra_cost'];
            }

            $data[$key]['promo_code'] = 0;
            if($value['payment_details']['promo_code'] > 0){
                $data[$key]['promo_code'] = $value['payment_details']['get_promocode']['promo_code'];
            }
            $data[$key]['net_amount'] = $amount;
            $data[$key]['Total'] = '$'.$value['after_discount_price'];
            $data[$key]['title'] = trim(@$value['title']);
            $data[$key]['url'] = trim(@$value['url']);
            $data[$key]['keyword'] = trim(@$value['keyword']);
            $data[$key]['invoiceUrl']  = url('/advertiser/getInvoice');
            $data[$key]['encrypt_id']  = $encrypt_id;
            $wallet = Wallet::where('order_id',$value['id'])->value('amount');
            $data[$key]['wallet']  = $wallet;

        }
        return view('buyer.purchases',compact('data'));   
    }      
    public function add_orders(){
       $orderDetails = $_POST;
       //foreach( $orderDetails['details'] as $key => $value){
       $created_at         = $orderDetails['details']['create_time'];
       $timestamp          = strtotime($created_at);
       $payer_created_at   = date('Y-m-d H:i:s',$timestamp);
       $updated_at         = $orderDetails['details']['update_time'];
       $timestamp          = strtotime($updated_at);
       $payer_updated_at   = date('Y-m-d H:i:s',$timestamp);
        $payer_email        = $orderDetails['details']['payer']['email_address'];
        $payer_id           = $orderDetails['details']['payer']['payer_id'];
        $payer_address      = $orderDetails['details']['purchase_units'][0]['shipping']['address']['address_line_1'].''.$orderDetails['details']['purchase_units'][0]['shipping']['address']['admin_area_2'].''.$orderDetails['details']['purchase_units'][0]['shipping']['address']['admin_area_1'].''.$orderDetails['details']['purchase_units'][0]['shipping']['address']['postal_code'].' '.$orderDetails['details']['purchase_units'][0]['shipping']['address']['admin_area_1'].' '.$orderDetails['details']['purchase_units'][0]['shipping']['address']['country_code'];
        $payer_name         = $orderDetails['details']['payer']['name']['given_name'].''.$orderDetails['details']['payer']['name']['surname'];
        $payer_number       = '';
        //$payer_number       = $value['payer']['phone']['phone_number']['national_number'];
        $payer_amount       = $orderDetails['details']['purchase_units'][0]['amount']['value'];
        $currency_code      = $orderDetails['details']['purchase_units'][0]['amount']['currency_code'];
        $merchant_id        = $orderDetails['details']['purchase_units'][0]['payee']['merchant_id'];
        $shipping_full_name = $orderDetails['details']['purchase_units'][0]['shipping']['name']['full_name'];
        $shipping_address   = $orderDetails['details']['purchase_units'][0]['shipping']['address']['address_line_1'].''.$orderDetails['details']['purchase_units'][0]['shipping']['address']['admin_area_2'].''.$orderDetails['details']['purchase_units'][0]['shipping']['address']['admin_area_1'].''.$orderDetails['details']['purchase_units'][0]['shipping']['address']['postal_code'].''.$orderDetails['details']['purchase_units'][0]['shipping']['address']['country_code'];
        $payment_status     = $orderDetails['details']['purchase_units'][0]['payments']['captures'][0]['status'];
        $capture_id        = $orderDetails['details']['purchase_units'][0]['payments']['captures'][0]['id'];
        $payment_link       = $orderDetails['details']['purchase_units'][0]['payments']['captures'][0]['links'][0]['href'];
         
         $wallet = 0;
          if(isset($_POST['wallet'])){
              $wallet = $_POST['wallet'];
          }   

         //payment array
         $payment_data = array(
            'payment_type'          => 'paypal',
            'currency'              =>  $currency_code,
            'receipt_url'           =>  $payment_link,
            'paypal_email'          =>  $payer_email,
            'paypal_merchant_id'    =>  $merchant_id,
            'paypal_address'        =>  $payer_address,
            'merchant_id'           =>  $merchant_id,
            'shipping_address'      =>  $shipping_address,
            'status'                =>  $payment_status,
            'user_id'               =>  Auth::user()->user_id,
            'amount'                =>  $payer_amount,
            'promo_code'            =>  $_POST['promo_id'],
            'type'                  =>  'paypal',
            'created_at'            =>  $payer_created_at,
            'updated_at'            =>  $payer_updated_at,
            'capture_id'            =>  $capture_id,
            'wallet_amount'         =>  $wallet
         );
         //insert into db
        $insertData = DB::table('payments')->insertGetId($payment_data);
        if( $insertData ){
           //get domain details
            $domain_arr = json_decode($orderDetails['domain_arr']);
            foreach( $domain_arr as $split_domain_arr ) {
                 
                 $domain_id     = $split_domain_arr->domain_id;
                 $domain_url    = $split_domain_arr->domain;
                 $domain_status = $split_domain_arr->status;

                 $cart = AddToCart::where('domain_id',$domain_id)->where('is_billed', 0)->where('user_id',Auth::user()->user_id)->first();

                /*Email template start*/
                $domain = MstDomainUrl::where('domain_id',$domain_id)->with(['websiteuser'])->first();

                $cost = $domain->websiteuser->cost_price;
                $cost_save = $cost;
                //if( $cart->promo_code == 0 ){
                    if($cart->content_type == 'buyer'){
                        $cost = '$' .buyerDomainWebsitePriceCalculate($cart->domainname->cost_price);
                        $cost_save = buyerDomainWebsitePriceCalculate($cart->domainname->cost_price);
                        $flybiz = $cost_save;
                    } else {
                        $cost = '$' .buyerDomainPriceCalculate($cart->domainname->cost_price, $cart->domainname->extra_cost);
                        $cost_save = buyerDomainPriceCalculate($cart->domainname->cost_price, $cart->domainname->extra_cost);
                        $flybiz = $cost_save;
                    }
                // } else {
                //     if($cart->content_type == 'buyer'){
                //         $cost = 0;
                //     } else {
                //         $cost = 0;
                //     }
                // }
                $cost_price = MstDomainUrl::where('domain_id',$domain_id)->value('cost_price');

                $data = array('is_billed' => 1, 'mst_payment_id' => $insertData, 'after_discount_price' => $cost_save,'cost_price' =>$cost_price, 'domain_amount' => $flybiz);
                 if($cart->content_type == 'seller'){
                    $extra_cost = MstDomainUrl::where('domain_id',$domain_id)->value('extra_cost'); 
                    $data = array('is_billed' => 1, 'mst_payment_id' => $insertData, 'after_discount_price' => $cost_save,'cost_price' =>$cost_price, 'extra_cost'=>$extra_cost);
                 }
               
               DB::table('add_to_carts')->where('id',$cart->id)->where('is_billed', 0)->where('user_id',Auth::user()->user_id)->update($data);

              $publisherFname = $domain->websiteuser->user_fname;
              $publisheremail = $domain->websiteuser->email;
              $domain_url = $domain->domain_url;
              if( env('APP_ENV') == 'live'){
                  /* Email template for publiser */
                    $args['email'] = $publisheremail;
                  //$args['email']    = 'randeep.s@webdew.com';
                    $args['subject'] = 'New Order Received';
                    $args['template_id'] = 'd-d7fa1ef8e595481c8e406e51f1d9e5e4';
                    $args['data'] = '{
                        "Website" : {
                              "name":"'.@$domain_url.'",
                              "cost":"'.$cost.'"
                        },
                        "Publisher":{
                              "F_name":"'.@$publisherFname.'"
                           },
                        "Advertiser":{
                              "F_name":"'.Auth::user()->user_fname.'"
                           }
                      }';
                      $this->sendgridEmail($args);
                  /* Email template for publisher end */

                  /* Email template for Advertiser */
                    $args['email'] = Auth::user()->email;
                 // $args['email']    = 'randeep.s@webdew.com';
                    $args['subject'] = 'Placed an Order';
                    $args['template_id'] = 'd-94c82df8c5c743b2aa1e097820865acd';
                    $args['data'] = '{
                            "Website" : {
                                  "name":"'.@$domain_url.'",
                                  "cost":"'.$cost.'"
                            },
                            "Publisher":{
                                  "F_name":"'.@$publisherFname.'"
                               },
                            "Advertiser":{
                                  "F_name":"'.@Auth::user()->user_fname.'"
                               }
                          }';
                    $this->sendgridEmail($args);
                  /* Email template for Advertiser end */

                    /* flock notification */
                      
                      $notiMessage = 'New Order has been Placed by @'.@Auth::user()->user_code.' for '.@$domain_url.' at price '.$cost;
                      
                      if(env('APP_ENV') == 'local' || env('APP_ENV') == 'stage' ) {
                          $notiMessage = '<strong>' . env('APP_ENV') . '</strong> New Order has been Placed by @'.@Auth::user()->user_code.' for '.@$domain_url.' at price '.$cost;
                      }
                      flockNotification($notiMessage);

                      /* flock notification */


                   /** Email template for admin **/
                    $adminUsers = getAdmminList();
                    foreach($adminUsers as $admin_users){
                      $args['email']    = $admin_users['email'];
                      //$args['email']    = 'randeep.s@webdew.com';
                      $args['subject']  = 'Placed an Order';
                      $args['template_id'] = 'd-5a534dd45fb148d8892651295e198ac7';
                      $args['data'] = '{
                              "Website" : {
                                    "name":"'.@$domain_url.'",
                                    "cost":"'.$cost.'"
                              },
                              "Publisher":{
                                    "F_name":"'.@$publisherFname.'"
                                 },
                              "Advertiser":{
                                    "F_name":"'.@Auth::user()->user_fname.'"
                                 }
                            }';
                      $this->sendgridEmail($args);
                    } 
                    /*email templates End*/
              }
          }
            
            $message = 'Payment has been done Succesfully!!';
            $alert = 'success';

            \Cache::forget('cartlist');
            $html = view('viewmessage-render',compact('message','alert'))->render();
            $cart = view('layout.include.partials.cart')->render();
            echo json_encode(array('status' => true,'html' => $html,'cart' => $cart));
        }
        else{
            $message = 'Error in Payment!!';
            $alert = 'warning';
            $html = view('viewmessage-render',compact('message','alert'))->render();
            $cart = view('layout.include.partials.cart')->render();
            echo json_encode(array('status'=>false,'html' => $html,'cart' =>$cart));
            
        }    
    }


    public function add_manualOrder() {
        $cartData     = $_POST;
        $domain_arr   = json_decode($cartData['domain_arr']);
        $orderAmount  = $cartData['amount'];
        $promo_id     = $cartData['promo_id'];
        $created_at   = date('Y-m-d H:i:s');
        $updated_at   = date('Y-m-d H:i:s');
        //insert data in database
         $payment_data = array(
            'payment_type'          =>  'manually',
            'currency'              =>  '',
            'receipt_url'           =>  '',
            'paypal_email'          =>  '',
            'paypal_merchant_id'    =>  '',
            'paypal_address'        =>  'manually',
            'merchant_id'           =>  '',
            'shipping_address'      =>  '',
            'promo_code'            => $promo_id,
            'status'                =>  1,
            'user_id'               =>  Auth::user()->user_id,
            'amount'                =>  $orderAmount,
            'type'                  =>  'manually',
            'created_at'            =>  $created_at,
            'updated_at'            =>  $updated_at,
         );
         //insert into db
        $insertData = DB::table('payments')->insertGetId($payment_data);
        if( $insertData ){
          //get domain details
          foreach( $domain_arr as $split_domain_arr ) {
               
               $domain_id     = $split_domain_arr->domain_id;
               $domain_status = $split_domain_arr->status;
               // $cart = DB::table('add_to_carts')->select('domain_amount','id','content_type')->where('domain_id',$domain_id)->where('is_billed', 0)->where('user_id',Auth::user()->user_id)->first();

               // $cost_price = MstDomainUrl::where('domain_id',$domain_id)->value('cost_price');

               // $data = array('is_billed' => 1, 'mst_payment_id' => $insertData, 'after_discount_price' => $orderAmount,'cost_price' =>$cost_price);
               // if($cart->content_type == 'buyer'){
               //    $extra_cost = MstDomainUrl::where('domain_id',$domain_id)->value('extra_cost'); 
               //    $data = array('is_billed' => 1, 'mst_payment_id' => $insertData, 'after_discount_price' => $orderAmount,'cost_price' =>$cost_price, 'extra_cost'=>$extra_cost);
               // }
               

               /** update status in cart table **/
               //$data = array('is_billed' => 1, 'mst_payment_id' => $insertData, 'promo_code_type' => 'percentage', 'promo_code' => $promo_id, 'after_discount_price' => $orderAmount);
               
               // DB::table('add_to_carts')->where('id',$cart->id)->where('is_billed', 0)->where('user_id',Auth::user()->user_id)->update($data);

              /*Email template start*/
              $domain = MstDomainUrl::where('domain_id',$domain_id)->with(['websiteuser'])->first();

              $publisherFname = $domain->websiteuser->user_fname;
              $publisheremail = $domain->websiteuser->email;
              $domain_url = $domain->domain_url;

               $cart = AddToCart::where('domain_id',$domain_id)->where('is_billed', 0)->where('user_id',Auth::user()->user_id)->first();

                $cost = $domain->websiteuser->cost_price;
                $cost_save = $cost;
                if( $cart->promo_code == 0 ){
                    if($cart->content_type == 'buyer'){
                        $cost = '$' .buyerDomainWebsitePriceCalculate($cart->domainname->cost_price);
                        $cost_save = buyerDomainWebsitePriceCalculate($cart->domainname->cost_price);
                        $flybiz = $cost_save;
                    } else {
                        $cost = '$' .buyerDomainPriceCalculate($cart->domainname->cost_price, $cart->domainname->extra_cost);
                        $cost_save = buyerDomainPriceCalculate($cart->domainname->cost_price, $cart->domainname->extra_cost);
                        $flybiz = $cost_save;
                    }
                } else {
                    if($cart->content_type == 'buyer'){
                        $cost = 0;
                    } else {
                        $cost = 0;
                    }
                }

                $cost_price = MstDomainUrl::where('domain_id',$domain_id)->value('cost_price');

                $data = array('is_billed' => 1, 'mst_payment_id' => $insertData, 'after_discount_price' => $cost_save,'cost_price' =>$cost_price,'domain_amount'=>$flybiz );
                 if($cart->content_type == 'seller'){
                    $extra_cost = MstDomainUrl::where('domain_id',$domain_id)->value('extra_cost'); 
                    $data = array('is_billed' => 1, 'mst_payment_id' => $insertData, 'after_discount_price' => $cost_save,'cost_price' =>$cost_price, 'extra_cost'=>$extra_cost);
                 }
               
               DB::table('add_to_carts')->where('id',$cart->id)->where('is_billed', 0)->where('user_id',Auth::user()->user_id)->update($data);

            if( env('APP_ENV') == 'live'){

              /* Email template for publiser */
                $args['email'] = $publisheremail;
                $args['subject'] = 'New Order Received';
                $args['template_id'] = 'd-d7fa1ef8e595481c8e406e51f1d9e5e4';
                $args['data'] = '{
                    "Website" : {
                          "name":"'.@$domain_url.'",
                          "cost":"'.$cost.'"
                    },
                    "Publisher":{
                          "F_name":"'.@$publisherFname.'"
                       },
                    "Advertiser":{
                          "F_name":"'.@Auth::user()->user_fname.'"
                       }
                  }';
                  $this->sendgridEmail($args);
              /* Email template for publisher end */

              /* Email template for Advertiser */
                $args['email'] = Auth::user()->email;
                $args['subject'] = 'Placed an Order';
                $args['template_id'] = 'd-94c82df8c5c743b2aa1e097820865acd';
                $args['data'] = '{
                        "Website" : {
                              "name":"'.@$domain_url.'",
                              "cost":"'.$cost.'"
                        },
                        "Publisher":{
                              "F_name":"'.@$publisherFname.'"
                           },
                        "Advertiser":{
                              "F_name":"'.@Auth::user()->user_fname.'"
                           }
                      }';
                $this->sendgridEmail($args);
              /* Email template for Advertiser end */

                  /* flock notification */
                  
                  $notiMessage = 'New Order has been Placed by @'.@Auth::user()->user_code.' for '.@$domain_url.' at price '.$cost;
                  
                  if(env('APP_ENV') == 'local' || env('APP_ENV') == 'stage' ) {
                      $notiMessage = '<strong>' . env('APP_ENV') . '</strong> New Order has been Placed by @'.@Auth::user()->user_code.' for '.@$domain_url.' at price '.$cost;
                  }
                  flockNotification($notiMessage);

                  /* flock notification */


               /** Email template for admin **/
                 $adminUsers = getAdmminList();
                foreach($adminUsers as $admin_users){
                  $args['email']    = $admin_users['email'];
                  $args['subject']  = 'Placed an Order';
                  $args['template_id'] = 'd-5a534dd45fb148d8892651295e198ac7';
                  $args['data'] = '{
                          "Website" : {
                                "name":"'.@$domain_url.'",
                                "cost":"'.$cost.'"
                          },
                          "Publisher":{
                                "F_name":"'.@$publisherFname.'"
                             },
                          "Advertiser":{
                                "F_name":"'.@Auth::user()->user_fname.'"
                             }
                        }';
                  $this->sendgridEmail($args);
                }
            } 
          }
          /** Email template for admin end**/
          \Cache::forget('cartlist');
          $message  = 'Payment has been done Succesfully!!';
          $alert    = 'success';
          $html     = view('viewmessage-render',compact('message','alert'))->render();
          $cart = view('layout.include.partials.cart')->render();
          echo json_encode(array('status'=>true,'html' => $html,'cart' =>$cart));
        }
        else{

            $message  = 'Error in Payment!!';
            $alert    = 'warning';
            $html     = view('viewmessage-render',compact('message','alert'))->render();
            $cart = view('layout.include.partials.cart')->render();
            echo json_encode(array('status'=>false,'html' => $html,'cart' =>$cart));
        }    

        die;
    }

    public function add_WalletOrder() {
        $cartData     = $_POST;
        $domain_arr   = json_decode($cartData['domain_arr']);
        $orderAmount  = $cartData['amount'];
        $created_at   = date('Y-m-d H:i:s');
        $updated_at   = date('Y-m-d H:i:s');
        //insert data in database
         $payment_data = array(
            'payment_type'          =>  'wallet',
            'currency'              =>  '',
            'receipt_url'           =>  '',
            'paypal_email'          =>  '',
            'paypal_merchant_id'    =>  '',
            'paypal_address'        =>  'wallet',
            'merchant_id'           =>  '',
            'shipping_address'      =>  '',
            'promo_code'            =>  0,
            'status'                =>  1,
            'user_id'               =>  Auth::user()->user_id,
            'amount'                =>  0,
            'type'                  =>  'wallet',
            'created_at'            =>  $created_at,
            'updated_at'            =>  $updated_at,
            'wallet_amount'         =>  $orderAmount,
         );
         //insert into db
        $insertData = DB::table('payments')->insertGetId($payment_data);
        if( $insertData ){
          //get domain details
          foreach( $domain_arr as $split_domain_arr ) {
               
               $domain_id     = $split_domain_arr->domain_id;
               $domain_status = $split_domain_arr->status;
              /*Email template start*/
              $domain = MstDomainUrl::where('domain_id',$domain_id)->with(['websiteuser'])->first();

              $publisherFname = $domain->websiteuser->user_fname;
              $publisheremail = $domain->websiteuser->email;
              $domain_url = $domain->domain_url;

               $cart = AddToCart::where('domain_id',$domain_id)->where('is_billed', 0)->where('user_id',Auth::user()->user_id)->first();

                $cost = $domain->websiteuser->cost_price;
                $cost_save = $cost;
                if( $cart->promo_code == 0 ){
                    if($cart->content_type == 'buyer'){
                        $cost = '$' .buyerDomainWebsitePriceCalculate($cart->domainname->cost_price);
                        $cost_save = buyerDomainWebsitePriceCalculate($cart->domainname->cost_price);
                        $flybiz = $cost_save;
                    } else {
                        $cost = '$' .buyerDomainPriceCalculate($cart->domainname->cost_price, $cart->domainname->extra_cost);
                        $cost_save = buyerDomainPriceCalculate($cart->domainname->cost_price, $cart->domainname->extra_cost);
                        $flybiz = $cost_save;
                    }
                } else {
                    if($cart->content_type == 'buyer'){
                        $cost = 0;
                    } else {
                        $cost = 0;
                    }
                }

                $cost_price = MstDomainUrl::where('domain_id',$domain_id)->value('cost_price');

                $data = array('is_billed' => 1, 'mst_payment_id' => $insertData, 'after_discount_price' => $cost_save,'cost_price' =>$cost_price,'domain_amount'=>$flybiz );
                 if($cart->content_type == 'seller'){
                    $extra_cost = MstDomainUrl::where('domain_id',$domain_id)->value('extra_cost'); 
                    $data = array('is_billed' => 1, 'mst_payment_id' => $insertData, 'after_discount_price' => $cost_save,'cost_price' =>$cost_price, 'extra_cost'=>$extra_cost);
                 }
               
               DB::table('add_to_carts')->where('id',$cart->id)->where('is_billed', 0)->where('user_id',Auth::user()->user_id)->update($data);

            if( env('APP_ENV') == 'live'){

              /* Email template for publiser */
                $args['email'] = $publisheremail;
                $args['subject'] = 'New Order Received';
                $args['template_id'] = 'd-d7fa1ef8e595481c8e406e51f1d9e5e4';
                $args['data'] = '{
                    "Website" : {
                          "name":"'.@$domain_url.'",
                          "cost":"'.$cost.'"
                    },
                    "Publisher":{
                          "F_name":"'.@$publisherFname.'"
                       },
                    "Advertiser":{
                          "F_name":"'.@Auth::user()->user_fname.'"
                       }
                  }';
                  $this->sendgridEmail($args);
              /* Email template for publisher end */

              /* Email template for Advertiser */
                $args['email'] = Auth::user()->email;
                $args['subject'] = 'Placed an Order';
                $args['template_id'] = 'd-94c82df8c5c743b2aa1e097820865acd';
                $args['data'] = '{
                        "Website" : {
                              "name":"'.@$domain_url.'",
                              "cost":"'.$cost.'"
                        },
                        "Publisher":{
                              "F_name":"'.@$publisherFname.'"
                           },
                        "Advertiser":{
                              "F_name":"'.@Auth::user()->user_fname.'"
                           }
                      }';
                $this->sendgridEmail($args);
              /* Email template for Advertiser end */

                  /* flock notification */
                  
                  $notiMessage = 'New Order has been Placed by @'.@Auth::user()->user_code.' for '.@$domain_url.' at price '.$cost;
                  
                  if(env('APP_ENV') == 'local' || env('APP_ENV') == 'stage' ) {
                      $notiMessage = '<strong>' . env('APP_ENV') . '</strong> New Order has been Placed by @'.@Auth::user()->user_code.' for '.@$domain_url.' at price '.$cost;
                  }
                  flockNotification($notiMessage);

                  /* flock notification */


               /** Email template for admin **/
                 $adminUsers = getAdmminList();
                foreach($adminUsers as $admin_users){
                  $args['email']    = $admin_users['email'];
                  $args['subject']  = 'Placed an Order';
                  $args['template_id'] = 'd-5a534dd45fb148d8892651295e198ac7';
                  $args['data'] = '{
                          "Website" : {
                                "name":"'.@$domain_url.'",
                                "cost":"'.$cost.'"
                          },
                          "Publisher":{
                                "F_name":"'.@$publisherFname.'"
                             },
                          "Advertiser":{
                                "F_name":"'.@Auth::user()->user_fname.'"
                             }
                        }';
                  $this->sendgridEmail($args);
                }
            } 
          }
          /** Email template for admin end**/
          \Cache::forget('cartlist');
          $message  = 'Payment has been done Succesfully!!';
          $alert    = 'success';
          $html     = view('viewmessage-render',compact('message','alert'))->render();
          $cart = view('layout.include.partials.cart')->render();
          echo json_encode(array('status'=>true,'html' => $html,'cart' =>$cart));
        }
        else{

            $message  = 'Error in Payment!!';
            $alert    = 'warning';
            $html     = view('viewmessage-render',compact('message','alert'))->render();
            $cart = view('layout.include.partials.cart')->render();
            echo json_encode(array('status'=>false,'html' => $html,'cart' =>$cart));
        }    

        die;
    }

    public function apply_coupon(Request $request) {
        $coupon_code = $request->coupon_code;
        $price = $request->price;
        //check in db coupon code exists or not
        $promo_id = DB::table('promo_codes')->where('promo_code',$coupon_code)->value('promo_id');
        
        if(!empty(@$promo_id)) {
            $promo_id = @$promo_id;
            $details = DB::table('promo_codes')->where('promo_id',$promo_id)->first();
            $for = '';
            if($details->promo_type == 'percentage'){
                $after_discount_price = (float)$price * (float)$details->promo_number / 100;
                $after_discount_price = (float)$price - $after_discount_price;
            } else if($details->promo_type == 'owner'){
                $after_discount_price = $price;
                $for = 'owner';
            } else {
                $after_discount_price = (float)$price - (float)$details->promo_number;
            }
            $response = ['success'=>'true','message'=>'Coupon Code Applied Succesfully!!','promo_id' => $promo_id, 'afterdiscountAmount' => $after_discount_price,'for' => $for ];
        }
        else{
            $response = ['success'=>'false','message'=>'InValid Coupon Code'];
        }
      return $response;
    }
    public function getInvoice($order_id){
        $order_id = \Crypt::decrypt($order_id);
        $payment_id = AddToCart::where('mst_payment_id','>',0)->where('id',$order_id)->where('user_id',Auth::user()->user_id)->value('mst_payment_id');

        $orderDetails = AddToCart::with(['domainname','username','domainusername','paymentDetails'])->where('is_billed','>',0)->where('mst_payment_id','>',0)->where('mst_payment_id',$payment_id)->where('user_id',Auth::user()->user_id)->get()->toArray();
        $data = [];
        foreach( $orderDetails as $key => $split_orderDetails ){
          if($key == 0){
            $invoice_arr = array(
                'order_id'              => $split_orderDetails['id'], 
                'domain_url'            => $split_orderDetails['fk_domain_url'],
                'payment_type'          => $split_orderDetails['payment_details']['payment_type'],
                'currency'              => $split_orderDetails['payment_details']['currency'],
                'paypal_email'          => $split_orderDetails['payment_details']['paypal_email'],
                'paypal_merchant_id'    => $split_orderDetails['payment_details']['paypal_merchant_id'],
                'paypal_status'         => $split_orderDetails['payment_details']['status'],
                'promo_code'            => $split_orderDetails['payment_details']['promo_code'],
                'stripe_merchant_id'    => $split_orderDetails['payment_details']['stripe_id'],
                'stripe_status'         => $split_orderDetails['payment_details']['status'],
                'paypal_address'        => $split_orderDetails['payment_details']['paypal_address'],
                'merchant_id'           => $split_orderDetails['payment_details']['merchant_id'],
                'amount'                => $split_orderDetails['payment_details']['amount'],
                'publisher_fname'       => $split_orderDetails['username']['user_fname'],
                'publisher_lname'       => $split_orderDetails['username']['user_lname'],
                'created_at'            => $split_orderDetails['payment_details']['created_at'],
                'is_accept'             => $split_orderDetails['is_accept'],
            );
          }
          $data[$key] = array(
              'order_id'              => $split_orderDetails['id'], 
              'domain_url'            => $split_orderDetails['fk_domain_url'],
              'payment_type'          => $split_orderDetails['payment_details']['payment_type'],
              'currency'              => $split_orderDetails['payment_details']['currency'],
              'paypal_email'          => $split_orderDetails['payment_details']['paypal_email'],
              'paypal_merchant_id'    => $split_orderDetails['payment_details']['paypal_merchant_id'],
              'paypal_status'         => $split_orderDetails['payment_details']['status'],
              'stripe_merchant_id'    => $split_orderDetails['payment_details']['stripe_id'],
              'stripe_status'         => $split_orderDetails['payment_details']['status'],
              'paypal_address'        => $split_orderDetails['payment_details']['paypal_address'],
              'merchant_id'           => $split_orderDetails['payment_details']['merchant_id'],
              'domain_amount'         => $split_orderDetails['domain_amount'],
              'amount'                => $split_orderDetails['payment_details']['amount'],
              'publisher_fname'       => $split_orderDetails['username']['user_fname'],
              'publisher_lname'       => $split_orderDetails['username']['user_lname'],
              'advertiser_fname'      => $split_orderDetails['domainusername']['user_fname'],
              'advertiser_lname'      => $split_orderDetails['domainusername']['user_lname'],
              'created_at'            => $split_orderDetails['payment_details']['created_at'],
              'is_accept'             => $split_orderDetails['is_accept'],
          );
          
        }
        return view('buyer.invoice',compact('invoice_arr','data'));
    }
    public function add_reviews_db(){
        
       $user_reviews    = $_POST['user_reviews'];
       $website_reviews = $_POST['website_reviews'];
       $domain_id       = $_POST['domain_id'];
       $domain_url      = $_POST['domain_url'];

       //check already exists
       $exists_domain = DB::table('reviews')->where('domain_id',$domain_id)->first();
       if(!$exists_domain){
 
           if(!empty($user_reviews && !empty($website_reviews))){
              $data   = array(
                  'user_id'    => Auth::user()->user_id,
                  'domain_url' => $domain_url,
                  'domain_id'  =>$domain_id,
                  'type'       =>'user', 
                  'rating'     => $user_reviews,
                );
              $data_1 = array(
                'user_id'    =>  Auth::user()->user_id,
                'domain_url' => $domain_url,
                'domain_id'  =>  $domain_id,
                'type'       =>  'website',
                'rating'     =>  $website_reviews,
              );
              $insert_reviews = DB::table('reviews')->insert($data);
              $insert_reviews = DB::table('reviews')->insert($data_1);

           }
          echo  json_encode(array('success'=>'true','message'=>'Rating Submitted'));
        }
        else{
          echo  json_encode(array('success'=>'false','message'=>'Domain Already Exists'));
        }
    }
}