<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Auth;
use Crypt;
use Redirect;
use Hash;
use DB;
use App\User;
use App\Review;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Show the application profile.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit_profile()
    {
        $user_id = Auth::user()->user_id;
        $reviews = Review::where('user_id',$user_id)->where('type','user')->get()->toArray();
        $count_reviews = Review::where('user_id',$user_id)->where('type','user')->count();

        $user_data = DB::table('mst_user')
                    ->join('mst_domain_url', 'mst_user.user_id', '=', 'mst_domain_url.domain_fk_user_id')
                    ->where('mst_user.user_id',$user_id)
                    ->first();

        if(empty( $user_data )){
            $user_data = DB::table('mst_user')
                    ->where('mst_user.user_id',$user_id)
                    ->first();
         }
        return view('profile.edit-profile',compact('user_data','reviews','count_reviews'));
    }


    public function store_profile(Request $request)
    {
        $args   = [
            'user_fname'         =>  $request->first_name,
            'user_lname'         =>  $request->last_name,
            'user_mob_no'        =>  $request->contact_number,
            'select_flag'        => $request->selected_flag, 
            'paypal_email'       => $request->paypal_email,
        ];
        if(request()->file('image')){
            $args['image'] = request()->file('image')->store('public/images');
        }        
        //update profile db
        User::where('user_id', Auth::user()->user_id)->update($args);
        return redirect()->back()->with(['message'=> 'Profile has been Updated Successfully!!', 'alert' => 'success','current_index'=> 1]);
    }

    /**
     * Show the application change password.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function change_password()
    {
        return view('customer.change-password');
    }


    public function store_change_password(Request $request)
    {
        $rules = array(
            'current_password'      => 'required|string|max:20',
            'new_password'      => 'required|string|max:20',
            'verify_password' => 'required|same:new_password'
        );

        $validatedData = $request->all();
        $validator = Validator::make($validatedData, $rules);
        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->with(['message'=> 'All fields are required', 'alert' => 'danger', 'current_index'=> 3])
                        ->withInput();
        } else {
            $input = $request->all();
            $user = User::find(Auth::user()->user_id);
            if(Hash::check($input['current_password'], $user->password)){
                try {
                    unset($validatedData['_token']);
                    unset($validatedData['current_password']);
                    unset($validatedData['verify_password']);
                    $validatedData['password']        = bcrypt(array_get($validatedData, 'new_password'));
                    unset($validatedData['new_password']);
                    $password                        =  User::where('user_id',Auth::user()->user_id)->update($validatedData);
                } catch (\Exception $exception) {
                    logger()->error($exception);
                    return redirect()->back()->with(['message'=> 'Unable to change password.', 'alert' => 'danger', 'current_index'=> 3]);
                    
                }

            } else if(md5($input['current_password']) == $user->password){
                // die('true2');
                // return redirect()->back()->with(['message'=> 'Current password not match. please try again', 'alert' => 'danger'])
                try {
                    unset($validatedData['_token']);
                    unset($validatedData['current_password']);
                    unset($validatedData['verify_password']);
                    $validatedData['password']        = bcrypt(array_get($validatedData, 'new_password'));
                    unset($validatedData['new_password']);
                    $password                        =  User::where('user_id',Auth::user()->user_id)->update($validatedData);
                } catch (\Exception $exception) {
                    logger()->error($exception);
                    return redirect()->back()->with(['message'=> 'Unable to change password.', 'alert' => 'danger', 'current_index'=> 3]);
                    
                }

            } else {
            return redirect()->back()->with(['message'=> 'Current password not match. please try again', 'alert' => 'danger', 'current_index'=> 3]);
            
            }            
            return redirect()->back()->with(['message'=> 'Password has been changed.', 'alert' => 'success', 'current_index'=> 3]);
        }    
    }

    public function switched(Request $request)
    {
        $switched = Auth::user()->switched;
        if($switched == 'publisher'){
            $switched = 'advertiser';
        } else if ($switched == 'advertiser') {
            $switched = 'publisher';
        } else {
            $switched = 'publisher';
        }

        $switched_done = User::where('user_id',Auth::user()->user_id)->update(['switched'=>$switched]);
        return redirect()->to($switched.'/dashboard');
    }
    public function switchedOther(Request $request)
    {
        $switched = Auth::user()->switched;
        if($switched == 'publisher'){
            $switched = 'advertiser';
        } else if ($switched == 'advertiser') {
            $switched = 'publisher';
        } else {
            $switched = 'publisher';
        }

        $switched_done = User::where('user_id',Auth::user()->user_id)->update(['switched'=>$switched]);
        return redirect()->back();
    }
    public function editProfile(){

        $user_id = Auth::user()->user_id;
        $user_data = User::where('user_id',$user_id)->first();
        return view('profile.profile',compact('user_data'));
    }

}
