<?php

namespace App\Http\Controllers\Seller;

use Illuminate\Support\Facades\Mail;
use App\Mail\DomainOwnerMail;
use Illuminate\Http\Request;
use App\User;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\APIController;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use Auth;
use Crypt;
use Redirect;
use Hash;
use DB;
use Stripe;
use App\MstDomainUrl;
use App\AddToCart;
use App\Payment;
use App\Refund;
use App\WishlistTbl;

class SellerController extends APIController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    { 
      $this->middleware('auth');           
    }

    /**
     * Show the application profile.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function dashboard()
    {
        $user_id = Auth::user()->user_id;
        $countOrders = DB::table('add_to_carts')->where('domain_user_id',$user_id)->where('is_billed','>',0)->count();
        $seller = DB::table('add_to_carts')
                      ->join('mst_domain_url','mst_domain_url.domain_id','add_to_carts.domain_id')
                      ->where('add_to_carts.promo_code',0)
                      ->where('add_to_carts.content_type','seller')
                      ->where('add_to_carts.domain_user_id',$user_id)
                      ->where('add_to_carts.is_billed',1)                      
                      ->sum('mst_domain_url.cost_price');
        $seller_extra = DB::table('add_to_carts')
                      ->join('mst_domain_url','mst_domain_url.domain_id','add_to_carts.domain_id')
                      ->where('add_to_carts.promo_code',0)
                      ->where('add_to_carts.content_type','seller')
                      ->where('add_to_carts.domain_user_id',$user_id)
                      ->where('add_to_carts.is_billed',1)                      
                      ->sum('mst_domain_url.extra_cost');
       
        if(!empty($seller)){
            $seller = buyerDomainPriceCalculate($seller,$seller_extra);
        }
        $buyer = DB::table('add_to_carts')
                      ->join('mst_domain_url','mst_domain_url.domain_id','add_to_carts.domain_id')
                      ->where('add_to_carts.domain_user_id',$user_id)
                      ->where('add_to_carts.is_billed',1)
                      ->where('add_to_carts.promo_code',0)
                      ->where('add_to_carts.content_type','buyer')
                      ->sum('mst_domain_url.cost_price');
        
        if(!empty($buyer)){
            $buyer = buyerDomainWebsitePriceCalculate($buyer);
        }

        $result = (float)$seller + (float)$buyer;
        return view('seller.dashboard',compact('result','countOrders'));
    }
    /**
     * Show the application profile.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function analytics()
    {
        return view('seller.analytics');
    }

    public function add_website(Request $request)
    {
        //$user_data = DB::table('mst_domain_url')->get();
        return view('seller.add-website');
    }

    public function verifyExistUrl(Request $request)
    {
        $web_url = $request->web_url;
        $filterUrl = preg_replace( "#^[^:/.]*[:/]+#i", "", $web_url);
        $domainUrl = preg_replace('/^www\./', '', $filterUrl);
        $code = MstDomainUrl::select('domain_id')->where('domain_url', $domainUrl)->where('domain_fk_user_id',Auth::user()->user_id)->where('trash',0)->get()->toArray();

        if(count($code) > 0)
        {
            echo json_encode( array('success' => false) );

        } else {
          /** calculate da pa **/
          try {
            
            //$data = dataForRapidMoz($domainUrl);
            // Convert JSON string to Array
              
            $dp_pa  = 0;
            $dp_da  = 0;
            // if(!empty($data['data'][0])){
            //     foreach ($data['data'][0] as $key => $value) {
            //       $moz = @serialize($value);
            //       $dp_da = @$value['domain_authority'];
            //       $dp_pa = @$value['page_authority'];
            //     }
            // }
                        
            echo json_encode(array('success' => true, 'dp_da' => $dp_da, 'dp_pa' => $dp_pa) );

          } catch (Exception $e) {
            echo json_encode(array('success' => false, 'dp_da' => 0, 'dp_pa' => 0) );            
          }
            
        }
        die;
    }

    /**
     * Show the application change password.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function my_websites()
    {
      $user_id = Auth::user()->user_id;

        // $domain_lists = DB::table('mst_domain_url')
        // ->leftJoin('assigned_websites','assigned_websites.mst_domain_url_id','mst_domain_url.domain_id')
        // ->select('mst_domain_url.domain_id','mst_domain_url.domain_url','mst_domain_url.cost_price','mst_domain_url.domain_status','mst_domain_url.backlink_type','assigned_websites.user_id','mst_domain_url.cost_price','mst_domain_url.extra_cost','mst_domain_url.domain_approval_status')
        // ->where('mst_domain_url.domain_fk_user_id',$user_id )
        // //->where('assigned_websites.user_id',$user_id )
        // ->where('mst_domain_url.cost_price','>',0)
        // ->groupBy('mst_domain_url.domain_url')
        // ->where('mst_domain_url.trash','<',1)
        // ->get();

        $domain_lists = DB::table('mst_domain_url')
        ->select('mst_domain_url.domain_id','mst_domain_url.domain_url','mst_domain_url.cost_price','mst_domain_url.domain_status','mst_domain_url.backlink_type','mst_domain_url.cost_price','mst_domain_url.extra_cost','mst_domain_url.domain_approval_status')
        ->where('mst_domain_url.domain_fk_user_id',$user_id )
        ->where('mst_domain_url.cost_price','>',0)
        ->groupBy('mst_domain_url.domain_url')
        ->where('mst_domain_url.trash','<',1)
        ->get();

      foreach( $domain_lists as $key => $splitDomainLists ){

          $domain_approval_status = $splitDomainLists->domain_approval_status; 
          if(empty($domain_approval_status)){
              $domain_lists[$key]->domain_approval_status = '0';
          }

          $domain_id = $splitDomainLists->domain_id;
          //get total orders by domain_id
          $count_orders = DB::table('add_to_carts')->where('domain_id',$domain_id)->where('domain_user_id',$user_id)->where('is_billed','>',0)->where('mst_payment_id','>',0)->count();
          //$count_orders = count($orderLists);
          if(!empty( $count_orders )){
              $domain_lists[$key]->orders = $count_orders;
          } else{
              $domain_lists[$key]->orders = 0;
          }
          $domain_lists[$key]->domain_id = \Crypt::encrypt($domain_lists[$key]->domain_id);
          $domain_lists[$key]->dp_da = DB::table('mst_da_pa')->where('dp_fk_domain', $domain_id)->max('dp_da');

          //get selling price
          $cost_price   = $splitDomainLists->cost_price;
          $extra_cost   = $splitDomainLists->extra_cost;
          $domain_lists[$key]->selling_price = buyerDomainPriceCalculate($cost_price,$extra_cost);
      }
      
      return view('seller.my-websites',compact('domain_lists'));
    }
    /**
     * Show the application change password.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function orders()
    {
        $auth_id    = Auth::user()->user_id;          
        $orderArr   = AddToCart::where('mst_payment_id','>',0)->where('is_billed','>',0)->where('domain_user_id',Auth::user()->user_id)->orderBy('id','DESC')->with(['domainname','username','domainusername','paymentDetails.get_promocode','refundDetails'])->get()->toArray();
        foreach( $orderArr as $key => $value) {

            if($value['payment_details']['status'] == 'succeeded' || $value['payment_details']['status'] == 'COMPLETED' || $value['payment_details']['status'] == 1){
              if($value['is_billed'] == 1){
                  $value['PaymentStatus'] = 1;
              } else {
                  $value['PaymentStatus'] = 2;
              }
            } else {
              
              if($value['is_billed'] == 1){
                  $value['PaymentStatus'] = 1;
              } else {
                  $value['PaymentStatus'] = 2;
              }
            }
            
            $value['content_type'] = $value['content_type'];
            $value['stages'] = $value['stages'];
            if($value['stages'] == '9'){
                $value['comment'] = $value['comment'];
            }
            $value['payment_type'] = $value['payment_details']['type'];
            $value['refund_type'] = $value['refund_details']['type'];

            $value['refund'] = 1;
            if($value['refund_id'] > 0){
                $value['refund'] = 2;
            }

            $value['refund_amount'] = 'NA';
           
            if(!empty($value['refund_details'])) {
                if($value['refund_details']['type'] == 'paypal'){
                    $ramount = 'Amount $'.$value['refund_details']['total_amount'] . ' + Paypal Transaction Fees $' . $value['refund_details']['txn_fee_refund'] . ' = Refunded Amount $' . $value['refund_details']['amount'];
                    $value['refund_amount'] = $ramount;  
                }
                if($value['refund_details']['type'] == 'stripe'){
                    $ramount = 'Refunded Amount $' . $value['refund_details']['amount'];
                    $value['refund_amount'] = $ramount;  
                }
                if($value['refund_details']['type'] == 'manually'){
                    $ramount = 'Refunded Amount $' . $value['refund_details']['amount'];
                    $value['refund_amount'] = $ramount;  
                }
                if($value['refund_details']['type'] == 'stripe-save-card'){
                    $ramount = 'Virtual Refunded Amount $' . $value['refund_details']['amount'];
                    $value['refund_amount'] = $ramount;  
                    $value['refund'] = 3;
                }
            }

            $value['promo_code'] = 0;
            if($value['payment_details']['promo_code'] > 0){
                $value['promo_code'] = $value['payment_details']['get_promocode']['promo_code'];
            }

            $id = $value['id'];
            $encrypt_id = \Crypt::encrypt($id);
            $domain_id_enc = \Crypt::encrypt($value['domainname']['domain_id']);
            $orderLists[] = array(
              'id'      => $value['id'],
              'user_id'       => $value['user_id'],
              'domain_id'     => $value['domain_id'],
              'domain_id_enc' => $domain_id_enc,
              'created_at'    => $value['created_at'],
              'updated_at'    => $value['updated_at'],
              'domain_url'    => $value['domainname']['domain_url'],
              'user_code'     => '@'.$value['username']['user_code'],
              'cost_price'    => $value['cost_price'],
              'stages'        => $value['stages'],
              'extra_cost'    => $value['extra_cost'],
              'amount'        => $value['after_discount_price'],
              'is_accept'     => $value['is_accept'],
              'refund_amount' => $value['refund_amount'],
              'refund'        => $value['refund'],//status
              'payment_type'  => $value['payment_type'],
              'PaymentStatus' => $value['PaymentStatus'],
              'content_type'  => $value['content_type'],
              'promo_code'    => $value['promo_code'],
              'pay_to_seller_id' => $value['pay_to_seller_id'],
              'url'           => trim(@$value['url']),
              'OrderDate'     => $value['created_at'],
              'is_billed'     => $value['is_billed'],
              //'content'     => @trim(strip_tags($value['content'])),
              'title'         => @$value['title'],
              'keyword'       => @$value['keyword'],
              'invoiceUrl'    => url('/publisher/getInvoice'),
              'encrypt_id'    => $encrypt_id,
              'comment'       => $value['comment'],
            );

        } 
        if(!empty($orderArr)){
          return view('seller.orders',compact('orderLists'));
        }
        else{
          $message = 'No Order Found in the Lists';
          return view('seller.orders',compact('message'));
        }

    }
    public function existing_domain($url){
        $web_url = $url;
        $filterUrl = preg_replace( "#^[^:/.]*[:/]+#i", "", $web_url);
        $domainUrl = preg_replace('/^www\./', '', $filterUrl);
        $domain_id = MstDomainUrl::select('domain_id')->where('domain_url', $domainUrl)->where('domain_fk_user_id', Auth::user()->user_id)->where('trash',0)->value('domain_id');
        if(empty($domain_id)){
            $domain_id = 0;
        }
        return $domain_id;
    }
    public function content($order_id){
        $id = \Crypt::decrypt($order_id);
        $order_details = AddToCart::where('id',$id)->first();
        $number_words = MstDomainUrl::where('domain_id',$order_details['domainname']['domain_id'])->value('number_words');
        return view('seller.content',compact('order_details','order_id','number_words'));
    }
    public function content_POST(Request $request)
    {   
        $user_id = Auth::user()->user_id;
        $order_id = \Crypt::decrypt($request->order_id);
        $domain_id = AddToCart::where('id',$order_id)->value('domain_id');
        $content_type = AddToCart::where('id',$order_id)->value('content_type');
        $domain = MstDomainUrl::where('domain_id',$domain_id)->first();
        $val = '|max:0';
        if($domain->number_words > 0){
            $val = '|max:'.$domain->number_words;
        } 
        $validatedData  = [
            'cktext'  => 'required|string'.$val
        ];

        $data = $request->all();
        $data['cktext'] = strip_tags(@$request->cktext);

        $v = \Validator::make($data, $validatedData);

        if ($v->fails()) {
            $errors = $v->errors();
            $response = ['status'=> false,'alert' => 'danger', 'message'=> 'Please fill required field.', 'errors' =>  $errors];
        } else {
            $insert = ['content' => @$request->cktext, 'stages' => 4];
            $insertid = AddToCart::where('id',$order_id)->update($insert);
            $response = ['message'=> 'Content has been updated.', 'alert' => 'success', 'status'=> true];
        }
        
        return response()->json($response);
        

    }
    public function insert_website(){

          $data = $_POST;
          $guest_content  =  explode(',', $data['guest_content']) ;
          $guest_content  = serialize($guest_content);
          $data['guest_content'] = $guest_content;

          if(empty($data['number_words'])) {
              $data['number_words'] = 0;
          }
          if(empty($data['extra_cost'])) {
              $data['extra_cost'] = 0;
          }
          $moz = '';
          if( empty($data['dp_da']) && empty($data['dp_pa']) ){

              $url = $data['domain_suffix'];

              //$data = dataForRapidMoz($domainUrl);
              // Convert JSON string to Array
              $dp_pa  = 0;
              $dp_da  = 0;
              // if(!empty($data['data'][0])){
              //     foreach ($data['data'][0] as $key => $value) {
              //       $moz = @serialize($value);
              //       $dp_da = @$value['domain_authority'];
              //       $dp_pa = @$value['page_authority'];
              //     }
              // }
          }
          else{

             $dp_da = @$data['dp_da'];
             $dp_pa = @$data['dp_pa'];
          }
          
          /** end here code **/
            //$data = $_POST;
            $domain_url      = @$data['domain_url'];
            $domain_email    = @$data['domain_email']; 

            $data['domain_fk_user_id'] = Auth::user()->user_id;
          
            /* Check Exist in trash start*/            
                $domain_id_exist = $this->existing_domain($domain_url);
            /* Check Exist in trash end */

            if(empty($domain_id_exist)){
                unset($data['dp_da']);
                unset($data['dp_pa']);
                //if($dp_da >= 20){
                
                    $insert_record = DB::table('mst_domain_url')->insertGetId($data);
                    if($insert_record){
                        //send mail
                          $user_fname  = @Auth::user()->user_fname;
                          $user_lname  = @Auth::user()->user_lname;
                          $user_code   = @Auth::user()->user_code;                      

                          /** insert DA PA into mst_da_pa **/ 
                          $new_data = ['dp_fk_domain' => $insert_record,'dp_pa'=> $dp_pa,'dp_da'=>$dp_da,'dp_followers'=>1, 'all_moz' => $moz ,'created_date' => date('Y-m-d')];

                          $dp_id = DB::table('mst_da_pa')->where('dp_fk_domain', $insert_record)->value('dp_id');
                          if(!empty($dp_id)){
                              DB::table('mst_da_pa')->where('dp_id',$dp_id)->update($new_data);
                          } else {
                              DB::table('mst_da_pa')->insertGetId($new_data);
                          }
                          
                          $data_1 = ['user_id' => Auth::user()->user_id, 'mst_domain_url_id' => $insert_record];
                          $assign_id = DB::table('assigned_websites')->where('mst_domain_url_id', $insert_record)->where('user_id', Auth::user()->user_id)->value('id');
                          if(!empty($assign_id)){
                              DB::table('assigned_websites')->where('id',$assign_id)->update($data_1);
                          } else {
                              
                              DB::table('assigned_websites')->insertGetId($data_1);
                          }
                          
                          /** insert DA PA into mst_da_pa **/

                          /* API insertion start*/
                           // $this->call_API_Website( $domain_url,$insert_record);
                          /* API insertion end */
                        if( env('APP_ENV') == 'live'){

                            if($domain_email){
                                
                                $subject        = 'GuestPostEngine Website Status';
                                $domain_id      = \Crypt::encrypt($insert_record);
                                $link           = url('/publisher/updateWebsiteStatus/'.$domain_id);
                                \Mail::to($domain_email)->send(new DomainOwnerMail($user_fname, $subject, $user_lname,$link));
                                 /** Mail function for advertiser **/
                                $args['email']    = Auth::user()->email;                
                                $args['subject'] = 'Website Submitted';                
                                $args['template_id'] = 'd-a907b74857774af6a7e38a575129ecf5';                
                                $args['data'] = '{                          
                                   
                                    "Advertiser":{   
                                          "F_name":"@'.@$user_code.'"                           
                                    }, 
                                    "Website":{   
                                        "name":"'.@$domain_url.'"                           
                                     },
                                }';
                                $this->sendgridEmail($args);


                                $adminUsers = getAdmminList();
                                foreach($adminUsers as $admin_users){

                                    $args['email']    = $admin_users['email'];               
                                    $args['subject'] = 'New Website submitted by User';                
                                    $args['template_id'] = 'd-3251b3a42e2246029c535536a84c23c5';                
                                    $args['data'] = '{                        
                                        "F_name" : "@'.@$user_code.'",
                                        "website_name": "'.@$domain_url.'",
                                    }';
                                    

                                    $this->sendgridEmail($args);
                                }
                                //getTemplate($domain_email, $user_fname, $user_lname,  $subject, $link);
                            }

                            /* flock notification */
                            $notiMessage = 'New website '.@$domain_url.' has been submitted by @'.@$user_code;
                            flockNotification($notiMessage);

                        }

                        /* flock notification */

                        $html = "<p>Thanks for Submitting your website!<p><br><ul><li>We approve each website manually, so we take almost 24 to 72Hrs for approving the website.</li><li>Once the website is approved you will receive an email confirmation, make sure to check the spam folder.</li><li>If you get our email in Spam folder don't forget to mark it as not spam, by Clicking on &quot;Report not Spam&quot;</li></ul>";
                        
                        // $response = ['status' => true, 'message' => 'Website has been successfully submitted! Please check your spam or junk mail folder, also dont forgot to  click on Report not spam', 'url' => url('publisher/my-websites')];
                        $response = ['status' => true, 'message' => $html, 'url' => url('publisher/my-websites')];
                    }
                    else {
                        
                        $response = ['status' => false, 'message' => 'Error while Insertion'];
                    }  
                // } else {
                //     $response = ['status' => false, 'message' => 'Domain Authority should be greater than equal to 20'];
                // }
                
            } else {
                $response = ['status' => false, 'message' => 'Domain Already Exists.'];
            }

        return $response;
    }

    public function edit_website($domain_id){
       $domain_id = Crypt::decrypt($domain_id);
        $single_webData = DB::table('mst_domain_url')->join('mst_da_pa','domain_id','dp_fk_domain')->where('domain_id',$domain_id)->get();
        return view('/seller/edit_website',compact('single_webData'));
    }
    public function update_website(Request $request){

        $data = $request->all();
        $guest_content  =  explode(',', $data['guest_content']) ;
        $guest_content  = serialize($guest_content);
        $data['guest_content'] = $guest_content;
        $domain_id = Crypt::decrypt($data['domain_id']);
        //update table by domain-id
        unset($data['domain_id']);
        unset($data['_token']);
        $update_exists = DB::table('mst_domain_url')->where('domain_id',$domain_id)->update($data); 
        if( $update_exists ){
            $response = ['status' => true, 'message' => 'Website has been successfully updated!', 'url' => url('publisher/my-websites')];
        }
        else{
            $response = ['status' => false, 'message' => 'Error while Updation'];
        }    
        return $response;
    }
    public function delete_website(Request $request){
       
       $domain_id = Crypt::decrypt($request->id);
       try {
          
          // DB::table('mst_da_pa')->where('dp_fk_domain', '=', $domain_id)->delete();
          // DB::table('mst_seo_data')->where('sd_fk_domain', '=', $domain_id)->delete();
          // DB::table('cnf_domain_country')->where('dc_fk_domain', '=', $domain_id)->delete();
          // DB::table('assigned_websites')->where('mst_domain_url_id', '=', $domain_id)->delete();
          // DB::table('add_to_carts')->where('domain_id', '=', $domain_id)->where('is_billed',0)->delete();
          //MstDomainUrl::where('domain_id', '=', $domain_id)->delete();

           MstDomainUrl::where('domain_id', '=', $domain_id)->update(['trash'=>1]);
           
           $url = url('publisher/my-websites');
           return ['alert' => 'success', 'message' => 'Website Deleted Succesfully!!','url' => $url];
       } catch (Exception $e) {
          return ['alert' => 'success', 'message' => 'Something error in deletion.'];         
       }
    }
    public function updateWebsiteStatus($domain_id){
      $domain_id  = \Crypt::decrypt($domain_id);
      //update domain status by id
      $updateData = array(
        'domain_status' => 1,
      );
      DB::table('mst_domain_url')->where('domain_fk_user_id',Auth::user()->user_id)->where('domain_id',$domain_id)->update($updateData);

      $domain_url = MstDomainUrl::where('domain_fk_user_id',Auth::user()->user_id)->where('domain_id',$domain_id)->value('domain_url');

      /* flock notification */
      if( env('APP_ENV') == 'live'){
        $notiMessage = @$domain_url.' has been verified by @' . @Auth::user()->user_code;
        
        if(env('APP_ENV') == 'local' || env('APP_ENV') == 'stage' ){
            $notiMessage = '<strong>' . env('APP_NAME') . '</strong> : '.@$domain_url.' has been verified by @' . @Auth::user()->user_code;
        }        
        flockNotification($notiMessage);
      }
      /* flock notification */

      return redirect()->to('/publisher/my-websites')->with(['message'=> 'Website Status Updated Succesfully!!', 'alert' => 'success']);
    }
    public function order_accept() {
          //$id = Crypt::decrypt($id);
          $id = $_POST['order_id'];
          $payment = 0;//  payment not done
          $order = AddToCart::where('id',$id)->first();
          $message = 'Order has been accepted';

          if($order->is_billed == 5 && $order->paymentDetails->type == 'stripe-save-card'){

                if(!empty($order->paymentDetails->stripe_customer_id) && !empty($order->paymentDetails->stripe_payment_method_id) && $order->after_discount_price > 0){
                    try {

                        $stripeToken = Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
                        $paymen_intent = \Stripe\PaymentIntent::create([
                          'amount' => $order->after_discount_price * 100,
                          'currency' => 'usd',
                          'customer' => secureDecrypt($order->paymentDetails->stripe_customer_id),
                          'payment_method' => secureDecrypt($order->paymentDetails->stripe_payment_method_id),
                          'off_session' => true,
                          'confirm' => true,
                        ]);
                        
                        $updateCart = AddToCart::where('id',$id)->update(['is_accept' => 1,'is_billed' => 1,'accept_by' => Auth::user()->user_id]);

                        Payment::where('id',$order->mst_payment_id)->update(['status' => 1]);
                        $message = 'Order has been accepted and Payment also debited to Advertiser - COST $'.$order->after_discount_price;
                        $payment = 1;//done payment
                        
                    } catch (\Stripe\Exception\CardException $e) {
                        // Error code will be authentication_required if authentication is needed
                        //echo 'Error code is:' . $e->getError()->code;
                        //$payment_intent_id = $e->getError()->payment_intent->id;
                        //$payment_intent = \Stripe\PaymentIntent::retrieve($payment_intent_id);                        
                        echo json_encode(array('success'=> false,'message' => $e->getError()->code));
                        die();
                    }

                } else {
                    echo json_encode(array('success'=> false,'message' => 'Payment has not been done. Card not updated.'));
                    die();
                    
                }

          } else {
              $mst_domain_url = AddToCart::where('id',$id)->update(['is_accept' => 1,'accept_by' => Auth::user()->user_id]);
              $payment = 2; // payment already done.
          }
            
          $advertiserFname = $order->username->user_fname;
          $advertiseremail = $order->username->email;
          $advertisercode = @$order->username->user_code;

          $publisherFname = $order->domainusername->user_fname;
          $publishercode = @$order->domainusername->user_code;
          $domain_url     = $order->domainname->domain_url;

          if( env('APP_ENV') == 'live'){

              /* Email template */
              $args['email'] =   $advertiseremail;
              //$args['email'] =   'randeep.s@webdew.com';
              $args['subject'] = 'Your Order Approved';
              $args['template_id'] = 'd-db458e432efd47a4baede2b1fe3af5f9';
              $args['data'] = '{
                      "Website" : {
                            "name":"'.@$domain_url.'"
                      },
                      "Publisher":{
                            "F_name":"@'.@$publishercode.'"
                         }
                    }';
              $this->sendgridEmail($args);
              /* Email template */

              $adminUsers = getAdmminList();
              foreach($adminUsers as $admin_users){
                    $args['email']    = $admin_users['email'];
                    //$args['email'] =   'randeep.s@webdew.com';
                    $args['subject']  = 'Placed an Order';
                    $args['template_id'] = 'd-f2fbc21b513f448c9ad2db15127832d1';
                    $args['data'] = '{
                            "Website" : {
                                  "name":"'.@$domain_url.'"
                            },
                            "Publisher":{
                                  "F_name":"@'.@$publishercode.'"
                               },
                            "Advertiser":{
                                  "F_name":"@'.@$advertisercode.'"
                               }
                          }';
                    $this->sendgridEmail($args);
                 // }               
              }

              /* flock notification */
                $publishercode = @$order->domainusername->user_code;        
                $notiMessage = $message . ' by @'.@$publishercode;
                if(env('APP_ENV') == 'local' || env('APP_ENV') == 'stage' ){
                    $notiMessage = '<strong>' . env('APP_NAME') . '</strong> : '.$message.' by @'.@$publishercode;
                }        
                flockNotification($notiMessage);
          }
          /* flock notification */          
          echo json_encode(array('success'=> true,'payment' => $payment,'message' => $message));
          die();
    }
    public function order_reject() {

          $id = $_POST['order_id'];

          $order = AddToCart::where('id',$id)->with(['paymentDetails','domainusername','username','domainname'])->first();          
          /* refunds end*/
          $payment_id = @$order->mst_payment_id;

          if(!empty($payment_id)) {
            
            $amount = $order->after_discount_price;
            if( $payment_id > 0 ){
                $payments = Payment::where('id',$payment_id)->first();

                if($payments->payment_type == 'stripe') { /* stripe refunds */  
                    $ch_id = $payments->stripe_id;
                    if(!empty($ch_id) && !empty($payment_id)){
                        $return_val = refundWallet($payment_id,$order->id,0,$amount);
                        //$this->stripeRefund($ch_id,$payment_id,$order->id,0,$amount);
                    }
                } else if($payments->payment_type == 'paypal'){ // paypal refund
                    if(!empty($order->paymentDetails->capture_id)){
                        $return_val = refundWallet($payment_id,$order->id,0,$amount);
                        //$return_val = transferToSeller($order->paymentDetails->capture_id,$payment_id,$order->id,0,$amount);
                    }
                } else if ($payments->payment_type == 'stripe-save-card'){
                    // Stripe save cards
                    $return_val = saveCardRefund($payment_id,$order->id,0,$amount);
                } else if ($payments->payment_type == 'wallet'){
                    // Wallet payment
                    $return_val = refundWallet($payment_id,$order->id,0,$amount);
                } else {

                    // manual payment
                    $return_val = manualRefund($payment_id,$order->id,0,$amount);
                }
            }
            /* refunds end*/  

            $advertiserFname = $order->username->user_fname;
            $advertiseremail = $order->username->email;
            $advertisercode = @$order->username->user_code;

            $publisherFname = $order->domainusername->user_fname;        
            $publishercode = @$order->domainusername->user_code;
            $domain_url = $order->domainname->domain_url;
            if( env('APP_ENV') == 'live'){
                /* Email template */
                $args['email'] =  $advertiseremail;
               // $args['email'] =  'randeep.s@webdew.com';
                $args['subject'] = 'Your Order Rejected';
                $args['template_id'] = 'd-687a4ea36e014d819bdc1e9a4a88b3cb';
                $args['data'] = '{
                      "Website" : {
                            "name":"'.@$domain_url.'"
                      },
                      "Publisher":{
                            "F_name":"@'.@$publishercode.'"
                         },
                        "Advertiser":{
                            "F_name":"@'.@$advertisercode.'"
                         }, 
                    }';
                /* Email template */
                $this->sendgridEmail($args);


                $adminUsers = getAdmminList();
                foreach($adminUsers as $admin_users){
                    $args['email']    = $admin_users['email'];
                   // $args['email'] =  'randeep.s@webdew.com';
                    $args['subject']  = 'Placed an Order';
                    $args['template_id'] = 'd-0b5986c2a9a7496e8364cc247b8c7387';
                    $args['data'] = '{
                            "Website" : {
                                  "name":"'.@$domain_url.'"
                            },
                            "Publisher":{
                                  "F_name":"@'.@$publishercode.'"
                               },
                            "Advertiser":{
                                  "F_name":"@'.@$advertisercode.'"
                               }
                          }';
                    $this->sendgridEmail($args);
                  
                }


              /* flock notification */

                $publishercode = @$order->domainusername->user_code;        
                $notiMessage = 'Order has been Rejected by @'.@$publishercode;
            
                if(env('APP_ENV') == 'local' || env('APP_ENV') == 'stage' ){
                    $notiMessage = '<strong>' . env('APP_NAME') . '</strong> : Order has been Rejected by @'.@$publishercode;
                }        
                flockNotification($notiMessage);

              /* flock notification */

          }
            /* Update domain url */
            $mst_domain_url = AddToCart::where('id',$id)->update(['is_accept' => 2,'accept_by' => Auth::user()->user_id]);
            echo json_encode(array('success'=> false));
               
            /* Update domain url */
            die();
          }
    }

    // public function manualRefund($payment_id, $AddToCartId, $message_id = 1, $amount = 0) {

    //     $full_refund_amount = Payment::where('id',$payment_id)->value('amount');
    //         $refund = Refund::create([
    //             'object' => 'refund',
    //             'amount' => $amount,
    //             'currency' => 'na',
    //             'status' => 'succeeded',
    //             'type' => 'manually',
    //             'create_stripe_date' => date('Y-m-d H:i:s'),
    //             'payment_type' => 'full',
    //             'refund_stripe_id' => rand(0000000,1111111),
    //             'message_id' => $message_id
    //         ]);
    //         $refund_id = $refund->id;
    //         AddToCart::where('id',$AddToCartId)->update(['refund_id' => $refund_id]);
    //         $response = ['status'=> true, 'message' => 'Paymemt has been refunded.'];          
    //     return $response;
    // }

    // public function saveCardRefund($payment_id, $AddToCartId, $message_id = 1, $amount = 0) {

    //     $full_refund_amount = Payment::where('id',$payment_id)->value('amount');
    //         $refund = Refund::create([
    //             'object' => 'refund',
    //             'amount' => $amount,
    //             'currency' => 'na',
    //             'status' => 'succeeded',
    //             'type' => 'stripe-save-card',
    //             'create_stripe_date' => date('Y-m-d H:i:s'),
    //             'payment_type' => 'full',
    //             'refund_stripe_id' => rand(0000000,1111111),
    //             'message_id' => $message_id
    //         ]);
    //         $refund_id = $refund->id;
    //         AddToCart::where('id',$AddToCartId)->update(['refund_id' => $refund_id]);
    //         $response = ['status'=> true, 'message' => 'Paymemt has been refunded.'];          
    //     return $response;
    // }

    // public function stripeRefund($ch_id, $payment_id, $AddToCartId, $message_id = 1, $partial_refund_amount = 0) {

    //     $full_refund_amount = Payment::where('id',$payment_id)->where('stripe_id',$ch_id)->value('amount');
    //     if($partial_refund_amount <= $full_refund_amount) {
    //         $token = \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
    //         if($partial_refund_amount > 0) {
    //             $payment_type = 'partial';
    //             $stripe =  \Stripe\Refund::create([
    //                 "charge" => $ch_id,
    //                 "amount" => $partial_refund_amount * 100,
    //                 "metadata" => ['message_id' => $message_id]
    //             ]);
    //         } else {
    //             $payment_type = 'full';
    //             $stripe =  \Stripe\Refund::create([
    //                 "charge" => $ch_id,
    //                 "metadata" => ['message_id' => $message_id]
    //             ]);
    //         }

    //         $refund_id = $stripe->id;
    //         $object = $stripe->object;
    //         $refund_amount = $stripe->amount;
    //         $balance_txn = $stripe->balance_transaction;
    //         $ch_id = $stripe->charge;
    //         $created = $stripe->created;
    //         $currency = $stripe->currency;
    //         $status = $stripe->status; // succeeded

    //         $refunded = ( $refund_amount / 100);

    //         $refund = Refund::create([
    //             'object'    => $object,
    //             'amount'    => $refunded,
    //             'balance_transaction' => $balance_txn,
    //             'stripe_id' => $ch_id,
    //             'currency' => $currency,
    //             'status' => $status,
    //             'type' => 'stripe',
    //             'create_stripe_date' => $created,
    //             'payment_type' => $payment_type,
    //             'refund_stripe_id' => $refund_id,
    //             'message_id' => $message_id
    //         ]);

    //         /* flock notification */
    //       if( env('APP_ENV') == 'live'){
    //           $notiMessage = 'Payment has been refunded ( Cost : $'.@$refunded.' - stripe payment)';
          
    //           if(env('APP_ENV') == 'local' || env('APP_ENV') == 'stage' ){
    //               $notiMessage = '<strong>' . env('APP_NAME') . '</strong> : Payment has been refunded ( Cost : $'.@$refunded.' - stripe payment)';
    //           }        
    //           flockNotification($notiMessage);

    //         /* flock notification */
    //       }

    //         $refund_id = $refund->id;
    //         AddToCart::where('id',$AddToCartId)->update(['refund_id' => $refund_id]);
    //         $response = ['status'=> true, 'message' => 'Paymemt has been refunded.'];

    //       } else {
    //           $response = ['status'=> false, 'message' => 'It Should be less from full Amount'];
    //       }

    //     return $response;
    // }
    public function getInvoice($order_id) {
      
        $order_id = \Crypt::decrypt($order_id);
        $payment_id = AddToCart::where('mst_payment_id','>',0)->where('id',$order_id)->where('domain_user_id',Auth::user()->user_id)->value('mst_payment_id');
        $orderDetails = AddToCart::with(['domainname','username','domainusername','paymentDetails'])->where('is_billed','>',0)->where('mst_payment_id',$payment_id)->where('domain_user_id',Auth::user()->user_id)->get()->toArray();
        $data = [];
        foreach( $orderDetails as $key => $split_orderDetails ){
          if($key == 0){
            $invoice_arr = array(
                'order_id'              =>  $order_id, 
                'domain_url'            => $split_orderDetails['fk_domain_url'],
                'payment_type'          => $split_orderDetails['payment_details']['payment_type'],
                'currency'              => $split_orderDetails['payment_details']['currency'],
                'paypal_email'          => $split_orderDetails['payment_details']['paypal_email'],
                'paypal_merchant_id'    => $split_orderDetails['payment_details']['paypal_merchant_id'],
                'paypal_status'         => $split_orderDetails['payment_details']['status'],
                'promo_code'            => $split_orderDetails['payment_details']['promo_code'],
                'stripe_merchant_id'    => $split_orderDetails['payment_details']['stripe_id'],
                'stripe_status'         => $split_orderDetails['payment_details']['status'],
                'paypal_address'        => $split_orderDetails['payment_details']['paypal_address'],
                'merchant_id'           => $split_orderDetails['payment_details']['merchant_id'],
                'amount'                => $split_orderDetails['payment_details']['amount'],
                'publisher_fname'       => $split_orderDetails['username']['user_fname'],
                'publisher_lname'       => $split_orderDetails['username']['user_lname'],
                'created_at'            => $split_orderDetails['payment_details']['created_at'],
                'order_status'          => $split_orderDetails['is_accept'],
            );
          }
          $data[$key] = array(
              'order_id'              =>  $order_id, 
              'domain_url'            => $split_orderDetails['fk_domain_url'],
              'payment_type'          => $split_orderDetails['payment_details']['payment_type'],
              'currency'              => $split_orderDetails['payment_details']['currency'],
              'paypal_email'          => $split_orderDetails['payment_details']['paypal_email'],
              'paypal_merchant_id'    => $split_orderDetails['payment_details']['paypal_merchant_id'],
              'paypal_status'         => $split_orderDetails['payment_details']['status'],
              'stripe_merchant_id'    => $split_orderDetails['payment_details']['stripe_id'],
              'stripe_status'         => $split_orderDetails['payment_details']['status'],
              'paypal_address'        => $split_orderDetails['payment_details']['paypal_address'],
              'merchant_id'           => $split_orderDetails['payment_details']['merchant_id'],
              'domain_amount'         => $split_orderDetails['domain_amount'],
              'amount'                => $split_orderDetails['payment_details']['amount'],
              'publisher_fname'       => $split_orderDetails['username']['user_fname'],
              'publisher_lname'       => $split_orderDetails['username']['user_lname'],
              'advertiser_fname'      => $split_orderDetails['domainusername']['user_fname'],
              'advertiser_lname'      => $split_orderDetails['domainusername']['user_lname'],
              'created_at'            => $split_orderDetails['payment_details']['created_at'],
              'order_status'          => $split_orderDetails['is_accept'],
          );
          $comb_order[]  = array(
              'order_id'              =>  $order_id, 
              'domain_url'            => $split_orderDetails['fk_domain_url'],
              'publisher_fname'       => $split_orderDetails['username']['user_fname'],
              'publisher_lname'       => $split_orderDetails['username']['user_lname'],
              'advertiser_fname'      => $split_orderDetails['domainusername']['user_fname'],
              'advertiser_lname'      => $split_orderDetails['domainusername']['user_lname'],
              'order_status'          => $split_orderDetails['is_accept'], 
          ); 
        }
        return view('seller.invoice',compact('invoice_arr','data','comb_order'));

    }

    public function cart_listings(){

            $data = AddToCart::where('is_billed','=',0)
                    ->where('mst_payment_id','=',0)
                    ->where('domain_user_id',Auth::user()->user_id)
                    ->with(['domainname','username','domainusername','paymentDetails'])
                    ->get()
                    ->toArray();

           if(!empty($data)){
                foreach( $data as $split_data ){
                    
                    /** link for chat **/
                    $domain_id = \Crypt::encrypt($split_data['domain_id']);
                    $other_user_id = \Crypt::encrypt($split_data['user_id']);
                    $auth_user_id = \Crypt::encrypt(Auth::user()->user_id);
                    $id = \Crypt::encrypt($domain_id . '_' . $other_user_id . '_' . $auth_user_id);
                    /** end chat **/
                    //condition for seller & buyer
                    if( $split_data['content_type'] == 'buyer'){
                        $price = $split_data['domainname']['cost_price'];
                        $extra = 0;
                        $total_amount = buyerDomainWebsitePriceCalculate($split_data['domainname']['cost_price']);
                    }
                    else {
                        $price = $split_data['domainname']['cost_price'];
                        $extra = '$'.$split_data['domainname']['extra_cost'];
                        $total_amount = buyerDomainPriceCalculate($split_data['domainname']['cost_price'], $split_data['domainname']['extra_cost']);
                    }
                    $cartData[] = array(
                          'id'            => @$split_data['id'],
                          'user_id'       => @$split_data['user_id'],
                          'domain_id'     => @$split_data['domain_id'],
                          'domain_url'    => @$split_data['domainname']['domain_url'],
                          'username'      => '@'.@$split_data['username']['user_code'],
                          'mobile_no'     => @$split_data['username']['user_mob_no'],
                          'user_email'    => @$split_data['username']['email'],
                          'content_type'  => @$split_data['content_type'],
                          'publisher'     => '@'.@$split_data['domainusername']['user_code'],
                          'cost_price'    => @'$'.$price,
                          'extra_cost'         => @$extra,
                          'total'         => @'$'.$total_amount,
                          'link'          => @$id,
                    );
                }
                return view('seller.cart',compact('cartData'));
            }
            else{
              $cartMessage = 'Cart is Empty';
              return view('seller.cart',compact('cartMessage'));
            }
    }
}