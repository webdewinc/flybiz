<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Controllers\API\APIController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\ActivationAccountFlybiz;
use App\Mail\DomainOwnerMail;
use Illuminate\Support\Facades\Route;
use DB;
use Illuminate\Notifications\Notifiable;
use App\Notifications\UserRegisteredSuccessfully;



class RegisterController extends APIController
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/signin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest');
        $this->middleware('guest')->except('logout');
    }

    public function sentEmail(){

        // $userlist = User::where('user_email_verified',NULL)->where('user_id','>',3000)->where('user_id','<=',3200)->count();
        // echo '<pre>';
        // print_r($userlist);
        // die;

        $userlist = User::where('user_email_verified',NULL)->where('sentEmail','<',1)->paginate(1);


        //$userlist = User::where('email','randeep.s@webdew.com')->get();
        $validatedData = [];
        $array = [];
        $count = 0;
        if($userlist){
            foreach ($userlist as $key => $value) {

                try {
                    $user_id = $value->user_id;
                    $validatedData['activation_code'] = str_random(30).time();
                    $user                             = User::where('user_id',$user_id)->update($validatedData);
                    $users                             = User::where('user_id',$user_id)->first();
                    $link = url('/verify-user') . '/' . $users->activation_code;
                    // if($users->user_id == 2231 || $users->user_id == 2431 || $users->user_id == 2820 || $users->user_id == 5389){
                    //     continue;
                    // }
                    if($users->email){
                        $array[$count] = $users->email;
                        $count++;
                    }
                    
                    \Mail::to($users->email)->send(new ActivationAccountFlybiz(@$users->user_fname, @$users->user_lname, $link));
                    //getTemplate($users->email,@$users->user_fname, @$users->user_lname, $link);
                    // $emails = $users->email;
                    // $user_fname = @$users->user_fname;
                    // $user_lname = @$users->user_lname;
                    // $array['user_fname'] = @$user_fname;
                    // $array['user_lname'] = @$user_lname;
                    // $array['link'] = @$link;
                    // \Mail::send(new ActivationAccountFlybiz(@$users->user_fname, @$users->user_lname, $link), [], function($message) use ($emails) {
                    //     $message->to($emails)->subject('Activation account in FlyBiz.');
                    // });

                    // if(!empty($emails)){
                    //     \Mail::send('emails.auth.send-email-to-all', [$array], function($message) use ($emails) {
                    //         $message->to($emails)->subject('Activation account in FlyBiz.');
                    //     }); 
                    //     var_dump( \Mail:: failures()); 
                    //     //exit;     
                    // }
                    
                    User::where('user_id',$user_id)->update(['sentEmail'=>1]);
                } catch (Exception $e) {
                    User::where('user_id',$user_id)->update(['sentEmail'=>2]);
                }
                
            }
        }
        
      //  print_r($array);
        //die;
        //\Mail::to($array)->send(new ActivationAccountFlybiz('User', '', $link));

        // echo 'ss';
        // die;
        
    }

    protected function register(Request $request)
    {
        $data = $_POST;
        /** @var User $user */
            $validatedData = $request->validate([
                'user_code'     => 'required|string|max:255|unique:mst_user',
                'email'         => 'required|string|email|max:255|unique:mst_user',
                'password'      => 'required|string',
                'user_fname'    => 'required|string',
                'user_lname'    => 'required|string'
            ]);
            
            try {
                $validatedData['password']        = bcrypt(array_get($validatedData, 'password'));
                $validatedData['user_type']       = 'user';
                $validatedData['activation_code'] = str_random(30).time();
                $user                             = app(User::class)->create($validatedData);
                
            } catch (\Exception $exception) {
                logger()->error($exception);
                return redirect()->to('/signup')->with(['message'=> 'Unable to create new user.', 'alert' => 'danger','form' => 'signup']);                
            }

            try {
               // $user->notify(new UserRegisteredSuccessfully($user));
                $link = url('/verify-user') . '/' . $user->activation_code;

                $args['email'] = $user->email;                
                $args['subject'] = 'GuestPostEngine';                
                $args['template_id'] = 'd-85e8c6fc0df84faf85319b2ca578e46c';                
                $args['data'] = '{                        
                    "F_name" : "'.@$user->user_fname.'",                       
                    "L_name" : "'.@$user->user_lname.'",
                    "link"   : "'.$link.'"
                }';

                //email send to register user
                    $this->sendgridEmail($args);
                //email send to Admin users  

                if( env('APP_ENV') == 'live'){
                  /* flock notification */
                  
                  $notiMessage = 'New user @' . @$user->user_code .' has registered as a Advertiser.';
                  if(env('APP_ENV') == 'local' || env('APP_ENV') == 'stage' ) {
                      $notiMessage = '<strong>' . env('APP_NAME').'</strong> : New user @' . @$user->user_code .' has registered as a Advertiser.';
                  }
                  flockNotification($notiMessage);

                  /* flock notification */
                    
                    
                    $adminUsers = getAdmminList();
                    foreach($adminUsers as $admin_users){
                        $args['email'] = $admin_users['email'];                
                        $args['subject'] = 'New User Registered';                
                        $args['template_id'] = 'd-e10a97dc92e84e9fa5581120cde616f0';                
                        $args['data'] = '{                        
                            "F_name" : "'.@$user->user_fname.'",
                        }';
                        $this->sendgridEmail($args);
                    }
                }
                //User::where('user_id',$user->user_id)->update(['sentEmail'=>1]);
            } catch (\Exception $exception) {
                User::where('user_id',$user->user_id)->update(['sentEmail'=>0]);
            }
            
            return redirect()->to('/signup')->with(['message'=> 'Thank You for Registration. Please check your email.', 'alert' => 'success','form' => 'signup']);
    }
    protected function websiteRegister(Request $request)
    {

        // $validatedData = $request->validate([
        //     'user_code'     => 'required|string|max:255|unique:mst_user',
        //     'email'         => 'required|string|email|max:255|unique:mst_user',
        //     'password'      => 'required|string',
        //     'user_fname'    => 'required|string',
        //     'user_lname'    => 'required|string'
        // ]); 
        $validatedData  = [
            'user_code'     => 'required|string|max:255|unique:mst_user',
            'email'         => 'required|string|email|max:255|unique:mst_user',
            'password'      => 'required|string',
            'user_fname'    => 'required|string',
            'user_lname'    => 'required|string' ];

        $v = \Validator::make($request->all(), $validatedData);

        if ($v->fails()) {
            $errors = $v->errors();
            $response = ['success'=> false, 'message'=> 'Please fill required field.', 'errors' =>  $errors];
            return $response;
        } 
        try {
            $data = $request->all();
            unset($data['_token']);
            $validatedData = $data;
            $validatedData['password']        = bcrypt(array_get($validatedData, 'password'));
            $validatedData['user_type']       = 'user';
            $validatedData['activation_code'] = str_random(30).time();
            $user                             = app(User::class)->create($validatedData);
            

        } catch (\Exception $exception) {
            //logger()->error($exception);
            $response = ['success'=> false, 'message'=> 'Unable to create new user.'];
            return $response;
        }

            try {
               // $user->notify(new UserRegisteredSuccessfully($user));
                $link = url('/verify-user') . '/' . $user->activation_code;

                $args['email'] = $user->email;                
                $args['subject'] = 'GuestPostEngine';
                $args['template_id'] = 'd-85e8c6fc0df84faf85319b2ca578e46c';                
                $args['data'] = '{                        
                    "F_name" : "'.@$user->user_fname.'",                       
                    "L_name" : "'.@$user->user_lname.'",
                    "link"   : "'.$link.'"
                }';

                //email send to register user
                    $this->sendgridEmail($args);
                //email send to Admin users  

                if( env('APP_ENV') == 'live'){
                  /* flock notification */
                  
                      $notiMessage = 'New user @' . @$user->user_code .' has registered as a Advertiser.';
                      if(env('APP_ENV') == 'local' || env('APP_ENV') == 'stage' ) {
                          $notiMessage = '<strong>' . env('APP_NAME').'</strong> : New user @' . @$user->user_code . ' has registered as a Advertiser.';
                      }
                      flockNotification($notiMessage);

                      /* flock notification */
                    
                    
                    $adminUsers = getAdmminList();
                    foreach($adminUsers as $admin_users){
                        $args['email'] = $admin_users['email'];                
                        $args['subject'] = 'New User Registered';                
                        $args['template_id'] = 'd-e10a97dc92e84e9fa5581120cde616f0';                
                        $args['data'] = '{                        
                            "F_name" : "'.@$user->user_fname.'",
                        }';
                        $this->sendgridEmail($args);
                    }
                }
                $response = ['success'=> true, 'message'=> 'Thank You for Registration. Please check your email.'];
                return $response;
                
            } catch (\Exception $exception) {
                User::where('user_id',$user->user_id)->update(['sentEmail'=>0]);
                $response = ['success'=> false, 'message'=> 'Error in email sent.'];
            }

        //$user->notify(new UserRegisteredSuccessfully($user));
        return $response;        
    }
    /**
     * Activate the user with given activation code.
     * @param string $activationCode

    * @return string
    */
    public function activateUser(string $activationCode)
    {
        try {
            $user = app(User::class)->where('activation_code', $activationCode)->first();
            if (!$user) {
                return redirect()->to('/signin')->with(['message'=> 'The code does not exist for any user in our system.', 'alert' => 'danger', 'form' => 'signup']);
                
            }
            $user->user_status         = 1;
            $user->user_email_verified = 1;
            $user->activation_code = null;
            $user->save();
            if( env('APP_ENV') == 'live'){
            /* flock notification */
                  
              $notiMessage = 'New user @' . @$user->user_code .' has verified the email.';
              if(env('APP_ENV') == 'local' || env('APP_ENV') == 'stage' ) {
                  $notiMessage = '<strong>' . env('APP_NAME').'</strong> : New user @' . @$user->user_code .' has verified the email.';
              }
              flockNotification($notiMessage);

              /* flock notification */

            }
            return redirect()->to('/signin')->with(['message'=> 'Successfully activate your account. you can login now.', 'alert' => 'success', 'form' => 'signin']);
        } catch (\Exception $exception) {
            logger()->error($exception);
            return "Whoops! something went wrong.";
        }
        return redirect()->to('/');
    }

    public function varifyemail(Request $request)
    {
        $email = User::where('email', $request->email)->get()->toArray();

        if(count($email) > 0)
        {
            echo json_encode(false);
        } else {
            echo json_encode(true);
        }
    }
    public function varifyUsercode(Request $request)
    {
        $code = User::where('user_code', $request->user_code)->get()->toArray();

        if(count($code) > 0)
        {
            echo json_encode(false);
        } else {
            echo json_encode(true);
        }
    }
    public function varifyemailForgot(Request $request)
    {
        $email = User::where('email', $request->email)->get()->toArray();
        if(count($email) > 0)
        {
            echo json_encode(true);
        } 
        else {
            echo json_encode(false);
        }
    }
    public function showformRedirection(Request $request)
    {
        return \Redirect::to('signup');
    }
    public function sendEmail($email){


        //sent mail to User for user activation
        $activation_code = str_random(30).time();
        /* update activation code */
        User::where('email',$email)->update(['activation_code' => $activation_code]);

        /* get user */
        $user = User::where('email',$email)->first();

        $link = url('/verify-user') . '/' . $activation_code;

        $args['email'] = $email;
        $args['subject'] = 'GuestPostEngine';                
        $args['template_id'] = 'd-85e8c6fc0df84faf85319b2ca578e46c';                
        $args['data'] = '{                        
            "F_name" : "'.@$user->user_fname.'",                       
            "L_name" : "'.@$user->user_lname.'",
            "link"   : "'.$link.'"
        }';

        //email send to register user
        $this->sendgridEmail($args);
        //email send to Admin users

    }
   
}
