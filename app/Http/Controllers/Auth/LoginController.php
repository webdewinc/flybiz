<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Auth;
use App\User;
use Illuminate\Support\Facades\Mail;
use Illuminate\Notifications\Notifiable;
use App\Notifications\UserRegisteredSuccessfully;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/signin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function login(Request $request)
    {   

        if(Auth::attempt(['email' => $request->user_email, 'password' => $request->password_login, 'user_type' => 'admin'],($request->remember == 'on') ? true : false)) {
            
            if(Auth::user()->user_email_verified == 1)
            {
                if( env('APP_ENV') == 'live'){
                /* flock notification */
                  
                  $notiMessage = '@'. @Auth::user()->user_code .' has logged as '.ucfirst(Auth::user()->user_type);
                  if(env('APP_ENV') == 'local' || env('APP_ENV') == 'stage' ) {
                      $notiMessage = '<strong>' . env('APP_NAME').'</strong> : @' . @Auth::user()->user_code .' has logged as <strong>'.ucfirst(Auth::user()->user_type).'</strong>.';
                  }
                  flockNotification($notiMessage);

                /* flock notification */
                }
                return redirect('admin/dashboard');
                //return redirect('publisher/dashboard');
            } else {

                if(Auth::user()->sentEmail == 0){
                    $validatedData['activation_code'] = str_random(30).time();
                    $user                             = User::where('user_id',Auth::user()->user_id)->update($validatedData);
                    $user = User::where('user_id',Auth::user()->user_id)->first();
                    Auth::logout();
                    try {
                        $user->notify(new UserRegisteredSuccessfully($user));
                        User::where('user_id',$user->user_id)->update(['sentEmail'=>1]);
                    } catch (\Exception $exception) {
                        User::where('user_id',$user->user_id)->update(['sentEmail'=>0]);
                    }
                }
                Auth::logout();
                return redirect()->to('/signin')->with(['message'=> 'Please activate your account first.', 'alert' => 'danger','form' => 'signin']);    
            }
            

        } else if(Auth::attempt(['email' => $request->user_email, 'password' => $request->password_login, 'switched' => 'publisher'],($request->remember == 'on') ? true : false)) { // Seller

            if(Auth::user()->user_email_verified == 1)
            {   

                if( env('APP_ENV') == 'live'){
                /* flock notification */
                  
                  $notiMessage = '@'. @Auth::user()->user_code .' has logged as '.ucfirst(Auth::user()->switched);
                  if(env('APP_ENV') == 'local' || env('APP_ENV') == 'stage' ) {
                      $notiMessage = '<strong>' . env('APP_NAME').'</strong> : @' . @Auth::user()->user_code .' has logged as <strong>'.ucfirst(Auth::user()->switched).'</strong>.';
                  }
                  flockNotification($notiMessage);

                /* flock notification */
                }
                return redirect('publisher/dashboard');
            } else {
                if(Auth::user()->sentEmail == 0){
                    $validatedData['activation_code'] = str_random(30).time();
                    $user                             = User::where('user_id',Auth::user()->user_id)->update($validatedData);
                    $user = User::where('user_id',Auth::user()->user_id)->first();
                    Auth::logout();
                    try {
                        $user->notify(new UserRegisteredSuccessfully($user));
                        User::where('user_id',$user->user_id)->update(['sentEmail'=>1]);
                    } catch (\Exception $exception) {
                        User::where('user_id',$user->user_id)->update(['sentEmail'=>0]);
                    }
                }
                Auth::logout();
                return redirect()->to('/signin')->with(['message'=> 'Please activate your account first.', 'alert' => 'danger','form' => 'signin']);    
            }

            
            
        } else if(Auth::attempt(['email' => $request->user_email, 'password' => $request->password_login, 'switched' => 'advertiser'],($request->remember == 'on') ? true : false)) { // Buyer
            
            if(Auth::user()->user_email_verified == 1)
            {
                if( env('APP_ENV') == 'live'){
                /* flock notification */
                  
                  $notiMessage = '@'. @Auth::user()->user_code .' has logged as '.ucfirst(Auth::user()->switched);
                  if(env('APP_ENV') == 'local' || env('APP_ENV') == 'stage' ) {
                      $notiMessage = '<strong>' . env('APP_NAME').'</strong> : @' . @Auth::user()->user_code .' has logged as <strong>'.ucfirst(Auth::user()->switched).'</strong>.';
                  }
                  flockNotification($notiMessage);

                /* flock notification */
                }
                return redirect('advertiser/dashboard');
            } else {

               if(Auth::user()->sentEmail == 0){
                    $validatedData['activation_code'] = str_random(30).time();
                    $user                             = User::where('user_id',Auth::user()->user_id)->update($validatedData);
                    $user = User::where('user_id',Auth::user()->user_id)->first();
                    Auth::logout();
                    try {
                        $user->notify(new UserRegisteredSuccessfully($user));
                        User::where('user_id',$user->user_id)->update(['sentEmail'=>1]);
                    } catch (\Exception $exception) {
                        User::where('user_id',$user->user_id)->update(['sentEmail'=>0]);
                    }
                }
                Auth::logout();
                return redirect()->to('/signin')->with(['message'=> 'Please activate your account first.', 'alert' => 'danger','form' => 'signin']);    
            }            
         
        } else {
            $user = User::where('email', $request->user_email)
                  ->where('password',md5($request->password_login))
                  ->first();
            $user_exist = json_decode(json_encode($user), true);
            if($user_exist){
                Auth::login($user);
                if($user_exist['user_type'] == 'admin'){
                   
                    if($user_exist['user_email_verified'] == 1) {

                        if( env('APP_ENV') == 'live'){
                        /* flock notification */
                  
                          $notiMessage = '@'. @Auth::user()->user_code .' has logged as '.ucfirst(Auth::user()->user_type);
                          if(env('APP_ENV') == 'local' || env('APP_ENV') == 'stage' ) {
                              $notiMessage = '<strong>' . env('APP_NAME').'</strong> : @' . @Auth::user()->user_code .' has logged as <strong>'.ucfirst(Auth::user()->user_type).'</strong>.';
                          }
                          flockNotification($notiMessage);

                        /* flock notification */
                        }

                        return redirect('admin/dashboard');
                    } else {
                        if(Auth::user()->sentEmail == 0){
                            $validatedData['activation_code'] = str_random(30).time();
                            $user                             = User::where('user_id',Auth::user()->user_id)->update($validatedData);
                            $user = User::where('user_id',Auth::user()->user_id)->first();
                            Auth::logout();
                            try {
                                $user->notify(new UserRegisteredSuccessfully($user));
                                User::where('user_id',$user->user_id)->update(['sentEmail'=>1]);
                            } catch (\Exception $exception) {
                                User::where('user_id',$user->user_id)->update(['sentEmail'=>0]);
                            }
                        }
                        Auth::logout();
                        return redirect()->to('/signin')->with(['message'=> 'Please activate your account first.', 'alert' => 'danger','form' => 'signin']);    
                    }
                } else if ($user_exist['switched'] == 'publisher'){
                    
                    if($user_exist['user_email_verified'] == 1) {
                        /* flock notification */
                        if( env('APP_ENV') == 'live'){
                          $notiMessage = '@'. @Auth::user()->user_code .' has logged as '.ucfirst(Auth::user()->switched);
                          if(env('APP_ENV') == 'local' || env('APP_ENV') == 'stage' ) {
                              $notiMessage = '<strong>' . env('APP_NAME').'</strong> : @' . @Auth::user()->user_code .' has logged as <strong>'.ucfirst(Auth::user()->switched).'</strong>.';
                          }
                          flockNotification($notiMessage);

                        /* flock notification */
                        }
                        return redirect('publisher/dashboard');
                    } else {
                        if(Auth::user()->sentEmail == 0){
                            $validatedData['activation_code'] = str_random(30).time();
                            $user                             = User::where('user_id',Auth::user()->user_id)->update($validatedData);
                            $user = User::where('user_id',Auth::user()->user_id)->first();
                            Auth::logout();
                            try {
                                $user->notify(new UserRegisteredSuccessfully($user));
                                User::where('user_id',$user->user_id)->update(['sentEmail'=>1]);
                            } catch (\Exception $exception) {
                                User::where('user_id',$user->user_id)->update(['sentEmail'=>0]);
                            }
                        }
                        Auth::logout();
                        return redirect()->to('/signin')->with(['message'=> 'Please activate your account first.', 'alert' => 'danger','form' => 'signin']);
                    }
                } else if($user_exist['switched'] == 'advertiser') {
                    
                    if($user_exist['user_email_verified'] == 1) {
                        if( env('APP_ENV') == 'live'){
                        /* flock notification */
                  
                          $notiMessage = '@'. @Auth::user()->user_code .' has logged as '.ucfirst(Auth::user()->switched);
                          if(env('APP_ENV') == 'local' || env('APP_ENV') == 'stage' ) {
                              $notiMessage = '<strong>' . env('APP_NAME').'</strong> : @' . @Auth::user()->user_code .' has logged as <strong>'.ucfirst(Auth::user()->switched).'</strong>.';
                          }
                          flockNotification($notiMessage);

                        /* flock notification */
                        }
                        return redirect('advertiser/dashboard');
                    } else {
                        if(Auth::user()->sentEmail == 0){
                            $validatedData['activation_code'] = str_random(30).time();
                            $user                             = User::where('user_id',Auth::user()->user_id)->update($validatedData);
                            $user = User::where('user_id',Auth::user()->user_id)->first();
                            Auth::logout();
                            try {
                                $user->notify(new UserRegisteredSuccessfully($user));
                                User::where('user_id',$user->user_id)->update(['sentEmail'=>1]);
                            } catch (\Exception $exception) {
                                User::where('user_id',$user->user_id)->update(['sentEmail'=>0]);
                            }
                        }
                        Auth::logout();
                        return redirect()->to('/signin')->with(['message'=> 'Please activate your account first.', 'alert' => 'danger','form' => 'signin']);    
                    }

                } else {
                    return redirect()->to('/signin')->with(['message'=> 'Something went wrong in user_type and switched user.', 'alert' => 'danger','form' => 'signin']);
                }
            } else {
                return redirect()->to('/signin')->with(['message'=> 'Please enter a valid email address and password.', 'alert' => 'danger','form' => 'signin']);

            }            
        }
    }
    // public function websiteLogin(Request $request)
    // {   
    //     // Include the reCaptcha library
    //     // require_once __DIR__ . "/libs/recaptcha-php/recaptchalib.php";

    //     // $privatekey = "your_private_key";

    //     // // reCaptcha looks for the POST to confirm
    //     // $resp = recaptcha_check_answer ($privatekey,
    //     //                                 $_SERVER["REMOTE_ADDR"],
    //     //                                 $_POST["recaptcha_challenge_field"],
    //     //                                 $_POST["recaptcha_response_field"]);

    //     // // If the entered code is correct it returns true (or false)
    //     // if ($resp->is_valid) {
    //     //   echo "true";
    //     // } else {
    //     //   echo "false";
    //     // }
    //     //Assuming, the remember-input is a checkbox and its value is 'on'
    //     // Auth::attempt($userData, ($request->remember == 'on') ? true : false)
    //     if(Auth::attempt(['email' => $request->user_email, 'password' => $request->password_login, 'user_type' => 'admin'],($request->remember == 'on') ? true : false)) {
    //         //return redirect('admin/dashboard');
            
    //         if(Auth::user()->user_email_verified == 1) {
    //             return redirect('publisher/dashboard');    
    //         } else {
    //             Auth::logout();
    //             return redirect()->to('/login')->with(['message'=> 'Please activate your account first.', 'alert' => 'danger','form' => 'signin']);    
    //         }

    //     } else if(Auth::attempt(['email' => $request->user_email, 'password' => $request->password_login, 'switched' => 'publisher'],($request->remember == 'on') ? true : false)) { // Seller
    //         if(Auth::user()->user_email_verified == 1) {
    //             return redirect('publisher/dashboard');    
    //         } else {
    //             Auth::logout();
    //             return redirect()->to('/login')->with(['message'=> 'Please activate your account first.', 'alert' => 'danger','form' => 'signin']);    
    //         }
            
            
    //     } else if(Auth::attempt(['email' => $request->user_email, 'password' => $request->password_login, 'switched' => 'advertiser'],($request->remember == 'on') ? true : false)) { // Buyer
    //         if(Auth::user()->user_email_verified == 1) {
    //             return redirect('advertiser/dashboard');
    //         } else {
    //             Auth::logout();
    //             return redirect()->to('/login')->with(['message'=> 'Please activate your account first.', 'alert' => 'danger','form' => 'signin']);    
    //         }
            
         
    //     } else {
    //         $user = User::where('email', $request->user_email)
    //               ->where('password',md5($request->password_login))
    //               ->first();
    //         $user_exist = json_decode(json_encode($user), true);
    //         if($user_exist){
    //             Auth::login($user);
    //             if($user_exist['user_type'] == 'admin'){
    //                // return redirect('admin/dashboard');
                    
    //                 if($user_exist['user_email_verified'] == 1) {
    //                     return redirect('publisher/dashboard');
    //                 } else {
    //                     Auth::logout();
    //                     return redirect()->to('/login')->with(['message'=> 'Please activate your account first.', 'alert' => 'danger','form' => 'signin']);    
    //                 }
    //             } else if ($user_exist['switched'] == 'publisher'){
    //                 if($user_exist['user_email_verified'] == 1) {
    //                     return redirect('publisher/dashboard');
    //                 } else {
    //                     Auth::logout();
    //                     return redirect()->to('/login')->with(['message'=> 'Please activate your account first.', 'alert' => 'danger','form' => 'signin']);    
    //                 }
    //             } else if($user_exist['switched'] == 'advertiser') {
                    
    //                 if($user_exist['user_email_verified'] == 1) {
    //                     return redirect('advertiser/dashboard');
    //                 } else {
    //                     Auth::logout();
    //                     return redirect()->to('/login')->with(['message'=> 'Please activate your account first.', 'alert' => 'danger','form' => 'signin']);    
    //                 }
    //             } else {
    //                 return redirect()->to('/login')->with(['message'=> 'Something went wrong in user_type and switched user.', 'alert' => 'danger','form' => 'signin']);
    //             }
    //         } else {
    //             return redirect()->to('/login')->with(['message'=> 'Incorrect username or password. Please try again.', 'alert' => 'danger','form' => 'signin']);

    //         }            
    //     }
    // }
    public function login_ajax(Request $request)
    {
        
        //Assuming, the remember-input is a checkbox and its value is 'on'
        // Auth::attempt($userData, ($request->remember == 'on') ? true : false)
        if(Auth::attempt(['email' => $request->user_email, 'password' => $request->password_login],($request->remember == 'on') ? true : false)) {

            if(Auth::user()->user_email_verified == 1) {
                
                if(Auth::user()->user_type != 'admin'){                    

                    if( env('APP_ENV') == 'live'){
                    /* flock notification */
                  
                      $notiMessage = '@' . @Auth::user()->user_code .' has logged as Advertiser for purchases.';
                      if(env('APP_ENV') == 'local' || env('APP_ENV') == 'stage' ) {
                          $notiMessage = '<strong>' . env('APP_NAME').'</strong> : @' . @Auth::user()->user_code .' has logged as Advertiser for purchases.';
                      }
                      flockNotification($notiMessage);

                    /* flock notification */
                    }
                    switchedOther();
                    $user = User::where('user_id', Auth::user()->user_id)
                  ->first();
                    $html = view('layout.include.profileFull')->render();
                    $response = ['message'=> 'Login success', 'alert' => 'success', 'status'=> true,'user_id' => Auth::user()->user_id, 'html' => $html];
                } else {
                    Auth::logout();
                    $response = ['message'=> 'You are not authorized user.', 'alert' => 'danger', 'status'=> false, 'user_id' => ''];
                }
                
            } else {
            
                if(Auth::user()->sentEmail == 0){
                    $validatedData['activation_code'] = str_random(30).time();
                    $user                             = User::where('user_id',Auth::user()->user_id)->update($validatedData);
                    $user = User::where('user_id',Auth::user()->user_id)->first();
                    Auth::logout();
                    try {
                        $user->notify(new UserRegisteredSuccessfully($user));
                        User::where('user_id',$user->user_id)->update(['sentEmail'=>1]);
                    } catch (\Exception $exception) {
                        User::where('user_id',$user->user_id)->update(['sentEmail'=>0]);
                    }
                }            

                Auth::logout();
                $response = ['message'=> 'Please activate your account first.', 'alert' => 'danger', 'status'=> false, 'user_id' => ''];
            } 

        } else {
            $user = User::where('email', $request->user_email)
                  ->where('password',md5($request->password_login))
                  ->first();
            $user_exist = json_decode(json_encode($user), true);
            if($user_exist){

                if($user_exist['user_email_verified'] == 1) {

                    if($user_exist['user_type'] != 'admin'){
                        
                        Auth::login($user);
                        switchedOther();
                        /* */
                        if( env('APP_ENV') == 'live'){
                        /* flock notification */
                  
                          $notiMessage = '@' . @Auth::user()->user_code .' has logged as Advertiser for purchases.';
                          if(env('APP_ENV') == 'local' || env('APP_ENV') == 'stage' ) {
                              $notiMessage = '<strong>' . env('APP_NAME').'</strong> : @' . @Auth::user()->user_code .' has logged as Advertiser for purchases.';
                          }
                          flockNotification($notiMessage);
                        }
                        /* flock notification */
                        $html = view('layout.include.profileFull')->render();
                        $response = ['message'=> 'Login success', 'alert' => 'success', 'status'=> true, 'user_id' => Auth::user()->user_id, 'html' => $html];
                    } else {
                        $response = ['message'=> 'You are not authorized user.', 'alert' => 'danger', 'status'=> false, 'user_id' => ''];
                    }
                } else {
                    if($user_exist['sentEmail'] == 0){
                        $validatedData['activation_code'] = str_random(30).time();
                        $user                             = User::where('user_id',$user_exist['user_id'])->update($validatedData);
                        $user = User::where('user_id',$user_exist['user_id'])->first();
                        try {
                            $user->notify(new UserRegisteredSuccessfully($user));
                            User::where('user_id',$user->user_id)->update(['sentEmail'=>1]);
                        } catch (\Exception $exception) {
                            User::where('user_id',$user->user_id)->update(['sentEmail'=>0]);
                        }
                    }
                    $response = ['message'=> 'Please activate your account first.', 'alert' => 'danger', 'status'=> false, 'user_id' => ''];
                }

            } else {
                $response = ['message'=> 'Incorrect username or password. Please try again.', 'alert' => 'danger', 'status'=> false, 'user_id' => ''];
            }            
        }
        
        return $response;
      
    }
    
    /**
     * @param Request $request
     * @throws ValidationException
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        $request->session()->put('login_error', trans('auth.failed'));
        throw ValidationException::withMessages(
            [
                'error' => [trans('auth.failed')],
            ]
        );
    }

    /**
     * @param Request $request
     * @throws ValidationException
     */
    protected function logout(Request $request)
    {
        Auth::logout();
        return redirect()->to('/signin')->with(['message'=> 'Thanks for visiting us. you are logged out now.', 'alert' => 'success','form' => 'signin']);
    }
}
