<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        
    ];

    public function cartlist() {        
        return $this->hasMany('App\AddToCart','mst_payment_id','id');
    }

    public function users() {
        return $this->hasOne('App\User','user_id','user_id');
    }

    public function refund_detail() {
        return $this->hasOne('App\Refund','id','refund_id');
    }
    public function get_promocode() {
        return $this->hasOne('App\PromoCode','promo_id','promo_code');
    }

}
