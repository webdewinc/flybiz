<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ActivationAccountFlybiz extends Mailable
{
    use Queueable, SerializesModels;

    private $user_fname;
    private $user_lname;
    private $link;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user_fname, $user_lname, $link)
    {
        $this->user_fname = $user_fname;
        $this->user_lname = $user_lname;
        $this->link = $link;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.auth.send-email-to-all', [
            'user_fname' => $this->user_fname,
            'user_lname' => $this->user_lname,
            'subject'  => 'Activate your account in GuestPostEngine',
            'link' => $this->link
        ]);
    }
}
