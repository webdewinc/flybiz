<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DomainOwnerMail extends Mailable
{
    use Queueable, SerializesModels;

    public $subject;
    protected $user_lname;
    protected $user_fname;
    protected $link;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user_fname, $subject, $user_lname, $link)
    {
        $this->subject      = $subject;
        $this->user_lname   = $user_lname;
        $this->user_fname   = $user_fname;
        $this->link         = $link;
    }

    /**
     * Build the template.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.domain-owner')
            ->with([
                'user_fname'     => $this->user_fname,
                'subject'  => $this->subject,
                'user_lname'  => $this->user_lname,
                'link'     => $this->link,
            ]);
    }
}
