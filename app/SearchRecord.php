<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class SearchRecord extends Authenticatable
{
   
    protected $guarded = [ ];

    public function data_da_pa() {
        return $this->hasOne('App\MstDaPa','dp_fk_domain','domain_id');
    }
    public function data_seo() {
        return $this->hasOne('App\MstSeoData','sd_fk_domain','domain_id');
    }
    public function data_report_spam() {
        return $this->hasMany('App\MstReportSpam','spm_fk_domain','domain_id');
    }
    public function data_search_url() {
        return $this->hasMany('App\MstSearchUrl','surl_fk_domain','domain_id');
    }
    public function data_similar_site() {
        return $this->hasMany('App\MstSimilarSite','smsite_fk_domain','domain_id');
    }
    public function data_organic_search() {
        return $this->hasMany('App\MstOrganicSearch','org_fk_domain','domain_id');
    }
    public function data_cnf_domain_country() {
        return $this->hasMany('App\CnfDomainCountry','dc_fk_domain','domain_id');
    }   
    public function data_assigned_website() {
        return $this->hasMany('App\AssignedWebsite','mst_domain_url_id','domain_id');
    }
    public function data_add_to_cart() {
        return $this->hasMany('App\AddToCart','domain_id','domain_id');
    }
    public function data_extra_field() {
        return $this->hasOne('App\MstOrgExtraField','fk_org_extra_domain_id','domain_id');
    }
   
}
