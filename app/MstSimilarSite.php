<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class MstSimilarSite extends Authenticatable
{
    protected $table = 'mst_similar_site';
    protected $primaryKey = 'smsite_id';
    protected $guarded = [
        
    ];
}
