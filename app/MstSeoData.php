<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class MstSeoData extends Authenticatable
{
    protected $table = 'mst_seo_data';
    protected $primaryKey = 'sd_id';

    protected $guarded = [
        
    ];


}
