<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use View;
use App\Notification;
use Twilio\Rest\Client;
use Twilio\Twiml;
use Illuminate\Http\Request;
use Twilio\Jwt\AccessToken;
use Twilio\Domain;
use Twilio\Exceptions\TwilioException;
use Twilio\Jwt\ClientToken;
use Crypt;
use Hash;
use App\User;
use DB;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
