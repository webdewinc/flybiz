<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class MstReportSpam extends Authenticatable
{
    protected $table = 'mst_report_spam';
    protected $primaryKey = 'spm_id';

    protected $guarded = [
        
    ];


}
