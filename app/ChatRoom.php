<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Chatroom extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        
    ];


    public function createdBy() {
        return $this->hasOne('App\User','user_id','created_by');
    }
    public function sender() {
        return $this->hasOne('App\User','user_id','sender_id');
    }
    public function receiver() {
        return $this->hasOne('App\User','user_id','receiver_id');
    }
    public function lastmessage() {
        return $this->hasOne('App\Chat','chatroom_id','id');
    }
}
