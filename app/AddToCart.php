<?php
namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class AddToCart extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        
    ];

    public function domainname() {
        return $this->hasOne('App\MstDomainUrl','domain_id','domain_id');
    }
    public function username() {
        return $this->hasOne('App\User','user_id','user_id');
    }
    public function domainusername() {
        return $this->hasOne('App\User','user_id','domain_user_id');
    }
    public function paymentDetails() {        
        return $this->hasOne('App\Payment','id','mst_payment_id');
    }
    public function payToSeller() {
        return $this->hasOne('App\PayToSeller','order_id','id');
    }
    public function refundDetails() {
        return $this->hasOne('App\Refund','id','refund_id');
    }
    public function promoDetails() {
        return $this->hasOne('App\PromoCode','id','promo_code');
    }
}
