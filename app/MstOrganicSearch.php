<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class MstOrganicSearch extends Authenticatable
{
    protected $table = 'mst_organic_search';
    protected $primaryKey = 'org_id';

    protected $guarded = [
        
    ];


}
