<?php
namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        
    ];

    public function domainname() {
        return $this->hasOne('App\MstDomainUrl','domain_id','domain_id');
    }
    public function username() {
        return $this->hasOne('App\User','user_id','user_id');
    }
    public function domainusername() {
        return $this->hasOne('App\User','user_id','user_id');
    }
}
