<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class CnfDomainCountry extends Authenticatable
{
    protected $table = 'cnf_domain_country';
    protected $primaryKey = 'dc_id';

    protected $guarded = [
        
    ];

    public function data_country() {
        return $this->hasOne('App\MstCountry','country_id','dc_fk_country');
    }

}
