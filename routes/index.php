<?php

if(!empty( $_GET['q']) ){

    include_once 'search.php';
    die;
}
//home
include_once 'include/class.user.php';
$user = new User();
session_start();
$_SESSION['url'] = $_SERVER['REQUEST_URI'];
error_reporting(0);
if ($_GET['action'] == "logout") {
    $user->user_logout();
    header("location:/");
}

?>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<style type="text/css">
    .hidden{
        display: none;
    }
    #display a:hover {
       cursor: pointer;
      /* background-color: #009bff;*/
    }
    #display {
        background-color: #fff;
        /* color: black; */
        /* width: 80.5%; */
        /* margin: auto; */
        z-index: 9999999;
        color: #000;
        text-align: left;
        padding-left: 10px;
    }
    #display li {
        list-style: none;
    }
    .display-dropdown {
        overflow-x: hidden;
        height: 230px;
        top: 50px;
        padding: 0px 18px;
        border: 4px solid #009bffb5;
        width: 100%;
        position: absolute;
    }
   
</style>

 <?php include('route.php'); ?>
 
 <?php include('header.php'); ?>

        <div class="content_section">
            <div class="search_wrapper">
                <!-- search-start -->
                <div class="container">
                    <!-- elastic search script start-->
                <script type="text/javascript">
                        //Getting value from "ajax.php".
                        function fill(Value) {
                           //Assigning value to "search" div in "search.php" file.
                           $('#search').val(Value);
                           //window.location.href = 'search?q='+Value;
                           window.location.href = '?q='+Value;
                           //Hiding "display" div in "search.php" file.
                           $('#display').hide();
                        }
                        /*function redirect(Value){
                            alert(value);
                        }*/
                        window.onload = function(){
                            var hideMe = document.getElementById('display');
                            document.onclick = function(e){
                               if(e.target.id !== 'hideMe'){
                                  $('#display').removeClass('display-dropdown');
                                  $('#display').html('');
                               }    
                            };
                         };
                        $(document).ready(function() {
                           //On pressing a key on "Search box" in "search.php" file. This function will be called.
                           $("#search").keyup(function() {
                                $('#display').addClass('display-dropdown');
                               //Assigning search box value to javascript variable named as "name".
                               var name = $('#search').val();
                               //Validating, if "name" is empty.
                               if (name == "") {
                                    //alert('true');
                                   //Assigning empty value to "display" div in "search.php" file.
                                   $("#display").html("");
                               }
                               //If name is not empty.
                               else {
                                   // alert('false');
                                   //AJAX is called.
                                   $.ajax({
                                       //AJAX type is "Post".
                                       type: "POST",
                                       //Data will be sent to "ajax.php".
                                       url: "ajax.php",
                                       //Data, that will be sent to "ajax.php".
                                       data: {
                                           //Assigning value of "name" into "search" variable.
                                           search: name
                                       },
                                       //If result found, this funtion will be called.
                                       success: function(response) {
                                           //Assigning result to "display" div in "search.php" file.
                                           $("#display").html(response);
                                       }
                                   });
                               }
                           });
                        });
                </script>

               

                <!-- elastic search script end-->
                    <div class="search_bar">
                        <div class="search_db center-align">
                            <h2 class="">World's 1st Guest Posting Search Engine</h2>
                            <form action="" method="get" onsubmit="return validateForm()" name="searchForm">
                                <div class="input-field col">
                                    <input name="q" type="text" class="validate desktop-placeholder home" placeholder="Search from the List of <?= $count_site ?> Blogs that accept Guest Posts" id="search" autocomplete="off">
                                    <div id="display"></div>
                                    <span id="error22" class="hidden error-msg pull-left ml-3 mt-1">Please Enter the search value</span>
                                    <input type="submit" name="" value="submit">
                                </div>
                            </form>
                            <div>
                            <?php
                                echo "<a href='#how_it_works' class='header-btn btn'>How it Works</a>";
                                if ($_SESSION['login'] == 1) {
                                echo "<a href='https://www.guestpostengine.com/webmaster/add-website' class='header-btn btn'>Add Website</a>";
                                }
                                else{
                                echo "<a href='signin' class='header-btn btn'>Add Website</a>";
                                }

                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="section_guest_postwork pd-top pd-bottom" id="how_it_works">
                <!-- Client-Section-start -->
              <div class="container">
                    <div class="row">
                        <div class="col s12 Head_titale">
                            <h2 class="">How <span>Guest Post Engine</span> Works</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="video-wrapper pd-top">
                            <div class="video_frame">
                                <iframe width="560" height="315" src="https://www.youtube.com/embed/p8OqDB2tLjQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                  
                        </div>
                    </div>
                </div>
            </div>
            <div class="section_why_guest_post pd-bottom" id="Features">
                <!-- why guet post-Section-start -->
                <div class="container">
                    <div class="row">
                        <div class="col s12 Head_titale">
                            <h2 class="">Why<span> Guest Post Engine</span></h2>
                        </div>
                    </div>
                  
                        <div class="guest_potwhy pd-top row">
                            <div class="col-12 col-sm-6 col-lg-4 featured_cat">
                                <div class="icon_guest">
                                    <img src="images/Robust-Engine.png" alt="">
                                </div>
                                <h4>Robust Engine</h4>
                                <p>Guest Post Engine will fulfill your quest of finding high-quality websites, as it brings hand-picked quality websites for your SEO campaigns.</p>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-4 featured_cat">
                                <div class="icon_guest">
                                    <img src="images/Largest-Database.png" alt="">
                                </div>
                                <h4>Largest Database</h4>
                                <p>One of the largest database that holds around 10,000+ quality websites and it gets updated on a daily basis.</p>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-4 featured_cat">
                                <div class="icon_guest">
                                    <img src="images/Updated-Key-Metrics.png" alt="">
                                </div>
                                <h4>Updated Key Metrics</h4>
                                <p>Over Guest Post Engine, websites get regularly updated and so does their metrics. Usually, websites key metrics gets updated once a month.</p>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-4 featured_cat">
                                <div class="icon_guest">
                                    <img src="images/Quality-Prospecting.png" alt="">
                                </div>
                                <h4>Quality Prospecting</h4>
                                <p>Websites provided by Guest Post Engine are hand-picked quality websites that provide quality backlinks.</p>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-4 featured_cat">
                                <div class="icon_guest">
                                    <img src="images/Save-Time-&-Money.png" alt="">
                                </div>
                                <h4>Save Time & Money</h4>
                                <p>Over Guest Post Engine you will get relevant quality websites within a second. So, it will save your time and eventually saves you money.</p>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-4 featured_cat">
                                <div class="icon_guest">
                                    <img src="images/Powerful-Backlinks.png" alt="">
                                </div>
                                <h4>Powerful Backlinks</h4>
                                <p>Using Guest Post Engine, you can get relevant, quality websites and these websites provide you Powerful backlinks</p>
                            </div>
                        </div>
                   
                </div>
            </div>

            <div class="section_review" id="Reviews">
                 <div class="container">
                 <div class="row">
                         <div class="col-12 ">
                             <!-- <h2 class="Head_tital">Reviews</h2> -->
                            <div class='embedsocial-reviews' data-ref="d94fdeee2a313220ccdc77eeccaa72ed6bf54919"></div><script>(function(d, s, id){var js; if (d.getElementById(id)) {return;} js = d.createElement(s); js.id = id; js.src = "https://embedsocial.com/embedscript/ri.js"; d.getElementsByTagName("head")[0].appendChild(js);}(document, "script", "EmbedSocialReviewsScript"));</script>

                        </div>
                    </div>
                </div>
            </div>
            <div class="section_frequently">
                <!-- Frequently Asked Questions-Section-start -->
                <div class="container">
                    <!-- <div class="row">
                        <div class="col s12 Head_titale">
                            <h2 class="">Frequently Asked <span>Questions</span></h2>
                        </div>
                    </div> -->
                    <div class="row">                        
                        <div class="col-12">
                            <h2 class="Head_titale">Frequently Asked <span>Questions</span></h2>
                            <div class="frequently_questions">
                                <div id="accordion">
                                    <div class="card">
                                        <div class="collapsible-header collapsed" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">
                                          What is Guest Blogging?
                                        </div>
                                        <div id="collapse1" class="collapse" data-parent="#accordion">
                                            <div class="collapsible-body">
                                                <span>Guest Blogging or guest posting is the act of writing content for various companies websites. Generally in guest blogging, usually bloggers write for a similar type of blogs as a guest blogger. The guest bloggers motive behind writing content is to:</span><br><br>
                                                <span>1. Spread information using their Blogs.</span><br>
                                                <span>2. Attract visitors on their website.</span><br>
                                                <span>3. Boost DA by using external links to high-authority domains.</span><br>
                                                <span>4. Create relationships with peers in their industry.</span><br><br>
                                                <span>Guest blogging holds mutual benefits for both website and guest blogger. </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="collapsible-header collapsed" data-toggle="collapse" data-target="#collapse2" aria-expanded="false" aria-controls="collapse2">
                                          Why you need Guest Blogging?
                                        </div>
                                        <div id="collapse2" class="collapse" data-parent="#accordion">
                                            <div class="collapsible-body">
                                                <span>Guest blogging offers a number of benefits for bloggers and businesses. When you share your expertise on the website of another company, it allows you to build yourself as an authority figure in the market. </span><br><br>
                                                <span>Also, featuring guest blogs on your own blog can help you to share fresh content with a unique perspective for the audience. It is a great way of keeping the readers busy- not to mention the promotional boost occurs when a blogger shares their posts via a personal network.</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="collapsible-header collapsed" data-toggle="collapse" data-target="#collapse3" aria-expanded="false" aria-controls="collapse3">
                                         How does Guest Post Engine work?
                                        </div>
                                        <div id="collapse3" class="collapse" data-parent="#accordion">
                                            <div class="collapsible-body">
                                                <span>GuestPostEngine database holds various quality websites that accept guest blog.
                                                </span><br><br>
                                                <span>So all you need to do is, search your keyword in GuestPostEngine. As a result, you'll get websites (with DA, PA, Traffic, Bounce rate) that suits best to your keyword.</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="collapsible-header collapsed" data-toggle="collapse" data-target="#collapse4" aria-expanded="false" aria-controls="collapse4">
                                         How GuestPostEngine is helpful for SEO Campaigns? 
                                        </div>
                                        <div id="collapse4" class="collapse" data-parent="#accordion">
                                            <div class="collapsible-body">
                                                <span>With Guest Post Engine, you can get high-quality websites of your niche. Further, the website will help you to get high-quality backlinks for your website.</span><br><br>
                                                <span>And you'll agree on that finding High-quality website for guest blogging is really time-consuming.</span><br><br>
                                                <span>So, GuestPostEngine will save you time and provide you quality websites within a seconds.</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                            <div class="collapsible-header collapsed" data-toggle="collapse" data-target="#collapse5" aria-expanded="false" aria-controls="collapse5">
                                             Is GuestPostEngine Free to use? 
                                            </div>
                                        <div id="collapse5" class="collapse" data-parent="#accordion">
                                             <div class="collapsible-body"><span>Yes, as for now Guest Post Engine is free to use and you can browse websites over it. Soon, we will come with the paid version.</span></div>
                                        </div>
                                    </div>
                                    <div class="card">
                                            <div class="collapsible-header collapsed" data-toggle="collapse" data-target="#collapse6" aria-expanded="false" aria-controls="collapse6">
                                             How often the Data Metrics of the website is updated? 
                                            </div>
                                        <div id="collapse6" class="collapse" data-parent="#accordion">
                                               <div class="collapsible-body"><span>In the guest post engine, we regularly update the websites and we update websites metrics once a month.</span></div>
                                        </div>
                                    </div>

                                    <div class="card">
                                            <div class="collapsible-header collapsed" data-toggle="collapse" data-target="#collapse7" aria-expanded="false" aria-controls="collapse6">
                                            Are the websites in Guest Post Engine, free or paid? 
                                            </div>
                                        <div id="collapse7" class="collapse" data-parent="#accordion">
                                               <div class="collapsible-body"><span>You will get both free and paid websites in Guest Post Engine. And, you can know whether the website is free or paid after contacting webmasters.</span></div>
                                        </div>
                                    </div>


                                <div class="card">
                                            <div class="collapsible-header collapsed" data-toggle="collapse" data-target="#collapse8" aria-expanded="false" aria-controls="collapse6">
                                            What green tick with websites means?
                                            </div>
                                        <div id="collapse8" class="collapse" data-parent="#accordion">
                                               <div class="collapsible-body"><span>It means that these websites are verified by us and we can post your article on those websites.</span></div>
                                        </div>
                                    </div>


                                    <div class="card">
                                            <div class="collapsible-header collapsed" data-toggle="collapse" data-target="#collapse9" aria-expanded="false" aria-controls="collapse6">
                                            How to place an order on Guest Post Engine?
                                            </div>
                                        <div id="collapse9" class="collapse" data-parent="#accordion">
                                               <div class="collapsible-body"><span>You can place an order in two ways,</span><br>
                                        <span> 1. You can get the contact details after availing our premium plan of $10. In this plan, you’ll get contact details of 100 websites of your choice,</span><br>
                                         <span> Or</span><br>
                                       <span> 2. You can place an order to us, and we will publish your blog on verified websites. And we will let you know whether the website is free or paid.
                                       </span></div>
                                        </div>
                                    </div>



                                    <div class="card">
                                            <div class="collapsible-header collapsed" data-toggle="collapse" data-target="#collapse10" aria-expanded="false" aria-controls="collapse6">
                                            How much time do websites take to publish the article?
                                            </div>
                                        <div id="collapse10" class="collapse" data-parent="#accordion">
                                               <div class="collapsible-body"><span>Every website has a different time span to publish the article. However, average websites take 2-4 weeks to publish your blog.</span></div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section_pricing" id="section_pricing">
                <div class="container">
                <h2 class="Head_titale">Our Plans</h2> 

                <style type="text/css">
                    .clearfix.elegant .plan-item {
                        display: none;
                    }
                    .clearfix.elegant .plan-item.active {
                        display: block;
                    }
                    .nav-item a.block {
                        background-color: #16a9f7;
                        border-color: #16a9f7;
                        color:#fff !important;
                    }
                    a.rounded.cursor-pointer:hover {    
                        color: #fff;    
                        text-decoration: none;
                    }
                </style>
                
                <div id="app">
                    <div class="pricing-table-main">
                        <div class="frequency-nav">
                            <div class="pricing-table-frequency clearfix">
                                <ul class="navs">
                                    <li class="nav-item"><a class="nav-link frequency block contact" name="1_months" >Monthly</a></li>
                                    <li class="nav-item"><a class="nav-link frequency contact" name="1_years">Yearly</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="frequency-dropown">
                            <div class="dd-header">
                                <div class="dd-header-title">Monthly</div>
                                <div class="line-arrow "></div>
                            </div>
                        </div>
                        <div class="pricing-table-body">
                            <div class="pricing-table popular">
                                <form action="https://www.guestpostengine.com/thank-you" method="POST">
                                <ul class="clearfix elegant">
                                    <input type="hidden" name="term" value="monthly">
                                    <input type="hidden" name="amount" value="1900">
                                    <li class="plan-item active" id="Monthly">
                                        <div class="plan-block ">
                                            <h3 id="plan-name">Unlimited Plan</h3>
                                            <div class="pricing-img"><img class="icon-basic" src="https://js.zohostatic.com/books/zfwidgets/assets/images/plan.png"></div>
                                            <div class="main-price"><span class="price-figure"><small>$</small><span class="basic-plan price-value"><span class="otherCurrency" id="plan-amount"> 19 </span></span>
                                                </span><span class="price-term"><span>Billed Monthly </span></span><span class="goal">
                                                    <!-- <a class="rounded cursor-pointer" href="http://guestpostengine.com/contact-us">Contact Us</a> -->
                                                    <script
                                                    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                                    data-key="pk_live_xuSB40lE6EYpWmEwLzew6rkU00nnPtPa1t"
                                                    data-amount="1900"
                                                    data-name="Stripe Payment"
                                                    data-description="GUEST POST"
                                                    data-image=""
                                                    data-locale="auto"
                                                    data-zip-code="true">
                                                  </script>
                                                </span>
                                                
                                            </div>
                                            <ul id="price-features" class="price-features" style="border-top: 1px solid rgb(237, 237, 237); padding-bottom: 7px;">
                                                <li>
                                                    <p style="position: relative;">Monthly Plan</p>
                                                </li>
                                                <li>
                                                    <p style="position: relative;">Browse Unlimited Websites</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                                </form>
                                <form action="https://www.guestpostengine.com/thank-you" method="POST">
                                    <ul class="clearfix elegant">
                                        <input type="hidden" name="term" value="yearly">
                                        <input type="hidden" name="amount" value="9900">
                                        <li class="plan-item" id="Yearly">
                                            <div class="plan-block ">
                                                <h3 id="plan-name">Unlimited Plan</h3>
                                                <div class="pricing-img"><img class="icon-basic" src="https://js.zohostatic.com/books/zfwidgets/assets/images/plan.png"></div>
                                                <div class="main-price"><span class="price-figure"><small>$</small><span class="basic-plan price-value"><span class="otherCurrency" id="plan-amount"> 99 </span></span>
                                                    </span><span class="price-term"><span>Billed Yearly </span></span><span class="goal"><!-- <a class="rounded cursor-pointer" href="http://guestpostengine.com/contact-us">Contact Us</a> -->
                                                    <script
                                                    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                                    data-key="pk_live_xuSB40lE6EYpWmEwLzew6rkU00nnPtPa1t"
                                                    data-amount="9900"
                                                    data-name="Stripe Payment"
                                                    data-description="GUEST POST Engine"
                                                    data-image=""
                                                    data-locale="auto"
                                                    data-zip-code="true">
                                                  </script></span>                                              
                                                </div>
                                                <ul id="price-features" class="price-features" style="border-top: 1px solid rgb(237, 237, 237); padding-bottom: 7px;">
                                                    <li>
                                                        <p style="position: relative;">Yearly Plan</p>
                                                    </li>
                                                    <li>
                                                        <p style="position: relative;">Browse Unlimited Websites</p>
                                                    </li>
                                                    <li>
                                                        <p style="position: relative;">Live Chat Support</p>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                    </ul>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- Test Begin -->
<!-- 
                <div id="zf-widget-root-id"></div> <script type="text/javascript" src='https://js.zohostatic.com/books/zfwidgets/assets/js/zf-widget.js'></script> <script> var pricingTableComponentOptions = { id: 'zf-widget-root-id', product_id: '2-b27f8f0e7a17078e2bc791190032fe1a89a1aa8fa606648e997bd095a28da6acbefc935811ebbfc858bcad6fd428a6fda080fc980711524fcdc51d8ddc5526b9', template: 'combo', most_popular_plan: '', is_group_by_frequency: false, group_options: [ ], plans: [ { plan_code: 'UNY' }, { plan_code: 'UN' }, ], theme: { color: '#009bff', theme_color_light: '#F9FCFF'}, button_text: 'Buy Now', product_url: 'https://subscriptions.zoho.com', price_caption: 'Browse Unlimited Websites that accept Guest Posts', language_code: 'en' }; ZFWidget.init('zf-pricing-table', pricingTableComponentOptions); </script>  -->


                <!--<div id="zf-widget-root-id"></div> <script type="text/javascript" src='https://js.zohostatic.com/books/zfwidgets/assets/js/zf-widget.js'></script> <script> var pricingTableComponentOptions = { id: 'zf-widget-root-id', product_id: '2-b27f8f0e7a17078e2bc791190032fe1a89a1aa8fa606648edb6a28cb4c1bc8ad88f865bbc94fd24158bcad6fd428a6fda080fc980711524fcdc51d8ddc5526b9', template: 'elegant', most_popular_plan: '', is_group_by_frequency: true, group_options: [ { frequency: 'Monthly', frequency_recurrence_value: '1_months', most_popular_plan: '', plans: [ { plan_code: 'UN' }, ] }, { frequency: 'Yearly', frequency_recurrence_value: '1_years', most_popular_plan: '', plans: [ { plan_code: 'UNY' }, ] }, ], plans: [ ], theme: { color: '#009bff', theme_color_light: '#F9FCFF'}, button_text: 'Subscribe', product_url: 'https://subscriptions.zoho.com', price_caption: '', language_code: 'en' }; ZFWidget.init('zf-pricing-table', pricingTableComponentOptions); </script>-->

                <!-- Test End -->

                <!-- Live Begin -->

                                    <!--  <div id="zf-widget-root-id"></div> <script type="text/javascript" src='https://js.zohostatic.com/books/zfwidgets/assets/js/zf-widget.js'></script> <script> var pricingTableComponentOptions = { id: 'zf-widget-root-id', product_id: '2-cdca512cdbd91de58d4f7b6474c594d95b662bf5cad23c832b7db0c25cc82d66e6ea00679ca7f99b58bcad6fd428a6fdd692a9c7e2c9686ecdc51d8ddc5526b9', template: 'elegant', most_popular_plan: '', is_group_by_frequency: true, group_options: [ { frequency: 'Monthly', frequency_recurrence_value: '1_months', most_popular_plan: '', plans: [ { plan_code: 'Month' }, ] }, { frequency: 'Yearly', frequency_recurrence_value: '1_years', most_popular_plan: '', plans: [ { plan_code: 'Year' }, ] }, ], plans: [ ], theme: { color: '#16a9f7', theme_color_light: '#F9FCFF'}, button_text: 'Contact Us', product_url: 'https://subscriptions.zoho.com', price_caption: '', language_code: 'en' }; ZFWidget.init('zf-pricing-table', pricingTableComponentOptions); </script>   -->
                 
                <!-- Live End -->
                
                    <!--<h2 class="Head_titale">FREE Subscription for Limited Time</h2> 
                    <div class="pricing">
                    
                    
                    
                        <div class="col-12 col-lg-3 pr_spc pricestart">
                            <h2>Startup</h2>
                        </div>
                        <div class="col-12 col-lg-3 pr_spc pricebx">
                            <h2>$0.00</h2>
                        </div>
                        <div class="col-12 col-lg-6 pr_spc pricelist-type">
                            <div>
                                <ul>
                                    <li>Search Unlimited Database of Websites</li>
                                    <li>Access 100 Contact Details every Month</li>
                                    <li>Deep Level of Website Filtering</li>
                                    <li>FREE Live Chat & Email Support</li>
                                </ul>
                                <div class="price_button">
                                    <a href="https://www.guestpostengine.com/signup" class="Signup_btn btn primary-btn">Sign up Now</a>
                               </div>
                            </div>
                        </div>--->
                       
                    </div>
                </div>
            </div>
            <div class="section_blogging_paybook pd-bottom">
                <!-- blogging_paybook-Section-start -->
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-md-7 blogplaybook mb-3">
                            <h2 class="">Guest Blogging Playbook</h2>
                            <span>EVERYTHING THAT YOU NEED TO KNOW</span>
                            <p>Guest blogging is one of the well-known marketing techniques in which a blogger writes
                                the content for a website that belongs to some other company or individual. In return, a
                                blogger gain a backlink.</p>
                            <p>If you are new to guest blogging than this playbook will help you to understand:</p>
                            <ul>
                                <li>How you can do Guest blogging.</li>
                                <li>Importance of Goals in Guest blogging</li>
                                <li>Learn steps to perform effective guest post prospecting</li>
                                <li>How you can do great outreach.</li>
                                <li>How to Write a Killer Guest Post.</li>
                                <li>Why and How to Promote your Guest Blog.</li>
                            </ul>
                            <a href="http://guestpostengine.com/playbook/" class="playbook_btn btn primary-btn">
                                Go to playbook</a>
                        </div>
                        <div class="col-12 col-md-5 playbook d-flex align-items-center">
                            <img src="images/Blogging-Playbook.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>



        
        <!-- footer-start -->
    <?php include('footer.php'); ?>
    <?php include('front_js.php'); ?>

    </div>
</body>
<script type="text/javascript">



$(document).ready(function(){

    $(document).on('click','.contact',function() {
        $('.clearfix.elegant .plan-item').removeClass('active');
        $('.nav-link.frequency').removeClass('block');
        
        $('.dd-header-title').text($(this).text());
        $('#'+$(this).text()).addClass('active');
        $(this).addClass('block');
    });
});

    
</script>

</html>