	<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes(['verify' => true,'login' => true, 'register'=>true]);
//Route::get('register','Auth\RegisterController@showformRedirection');
Route::get('test-email','API\APIController@testEmail');
Route::get('todayActivity/{date?}','API\APIController@todayActivity');

Route::get('tools/website-audit','HomeController@website_audit');
Route::get('/search/moz-metrix','HomeController@moz_metrix');
Route::post('/search/dataForRapidMoz','HomeController@dataForRapidMoz');

// Route::get('/sitemap/website/sitemap.xml','SitemapController@index');
// Route::get('/sitemap/category/sitemap.xml','SitemapController@category');

Route::get('$2y$$2y$10$V2CbQUeyqvZjKCJXwQvyeBLs19gCt75JKPUznf6cyQudqOqKbKu10$2y$10$V2CbQUeyqvZjKCJXwQvyeBLs19gCt75JKPUCbQUeyqvZjKCJX$2y$10$V2CbQUeyqvZjKCJXwQvyeBLs19gCt7','SitemapController@index');
Route::get('vZjKCJX$2y$10$y$$2y$10$V2CbQjKCJX$2y$10$V2CbQUeyqvBLs19gCt75JKPUznf6cyQudqOqKbKu10$2y$10$V2CbQUeyqvZjKCJXwQvyeBLs19gCt75JKPUCbQUeyvZjKCJX$2y$10$V2CbQUeyqvZjK','SitemapController@category');

Route::get('CbQjKX$2$12CqvwyeBLsg5JKPe','SitemapController@top100');


//Route::match(['get','post'],'signin','Auth\LoginController@showLoginForm');

Route::match(['get','post'],'signin',[ 'as' => 'signin', 'uses' => 'Auth\LoginController@showLoginFormSignIn']);

Route::match(['get','post'],'signup','Auth\RegisterController@showRegistrationFormSignup');
Route::match(['get','post'],'signout','Auth\LoginController@logout');
//Route::match(['get','post'],'signout','Auth\RegisterController@logout');

Route::match(['get','post'],'testRefund','HomeController@testRefunds');

Route::get('/verify-user/{code}', 'Auth\RegisterController@activateUser')->name('activate.user');
Route::match(['get','post'],'varifyemail','Auth\RegisterController@varifyemail');
Route::match(['get','post'],'varifyemailForgot','Auth\RegisterController@varifyemailForgot');
Route::match(['get','post'],'varifyusercode','Auth\RegisterController@varifyUsercode');
Route::match(['get','post'],'login-ajax','Auth\LoginController@login_ajax')->name('login-ajax');
//Route::post('/websiteLogin','Auth\LoginController@websiteLogin');
Route::post('/websiteRegister','Auth\RegisterController@websiteRegister');

Route::match(['get','post'],'thanks',function(){
	return view('thank-you');
});

// common profile
Route::get('/profile/{id}','HomeController@editProfile');
Route::get('/transferToSeller','HomeController@transferToSeller');
// API begin
//Route::get('callMOZ','API\APIController@call_API_MOZ');
Route::get('webdew-net/{limit?}/{offset_id?}','API\APIController@webdewNET');// webdew NET portal
Route::get('dataForSeo','API\APIController@dataForSeo');
Route::get('dataForMoz','API\APIController@dataForMoz');
Route::get('call-api','API\APIController@call_API');
Route::get('hubspot-api','API\APIController@DbToHubspotAPI');
Route::get('chat-not-read','API\APIController@chatNotRead');
Route::get('getTemplate','API\APIController@getTemplate');

Route::get('automatic-approve-website','API\APIController@automaticApproveWebsite');

/*msgwow*/
Route::get('msgwow/getmsg91','API\APIController@getmsg91');
Route::get('msgwow/get','API\APIController@msgwow');
/*msgwow*/

Route::get('/cors','HomeController@cors');
Route::post('/cors-store','HomeController@corsStore');
Route::get('send-email-to-all','Auth\RegisterController@sentEmail');
Route::get('send_email/{email}','Auth\RegisterController@sendEmail');
// api end



// website pages

Route::get('/', 'HomeController@index');
Route::get('/about', 'HomeController@about');
Route::get('/legal', 'HomeController@legal');
Route::get('contact','HomeController@contact');
Route::get('/search', 'Search\SearchController@search');
Route::get('/search/website/{domain_url}', 'Search\SearchController@search_websites');

//
Route::get('/search/category/', 'Search\SearchController@category');
Route::get('/search/category/{category_name}', 'Search\SearchController@search_category');

Route::post('/search_result','Search\SearchController@search_result');
Route::post('/add_to_wishlist','Search\SearchController@addToWishlist' );
Route::post('/add_to_cartlist','Search\SearchController@addToCartList' );
Route::post('/delete_wishlist','Search\SearchController@addToWishListDelete' );
Route::post('/delete_cartlist','Search\SearchController@addToCartListDelete' );
Route::post('/addWishMoveIntoCart','Search\SearchController@addWishMoveIntoCart' );

// website pages

Route::group(['middleware' => 'auth'], function() {

	Route::match(['get','post'],'/stages-changes', 'Search\SearchController@stages_changes');
	Route::match(['get','post'],'/stages-changes_1', 'Search\SearchController@stages_changes_1');
	Route::match(['get','post'],'/stages-changes_2', 'Search\SearchController@stages_changes_2');
	Route::match(['get','post'],'/stages-changes_3', 'Search\SearchController@stages_changes_3');
	Route::match(['get','post'],'/order_deliver', 'Search\SearchController@order_deliver');
	Route::match(['get','post'],'/release_payment', 'Search\SearchController@release_payment');
	/** Token **/
		Route::match(['get','post'],'/token', 'TokenController@generate');
	/* Token */

	/* Chat begin */
		Route::get('/chat/{id?}',  'ChatController@chat');
		Route::post('/chat-render',  'ChatController@chatRenderAll');
		Route::post('/chat-render-pop',  'ChatController@chatRenderPop');
		Route::post('/chat-render-search',  'ChatController@chatRenderSearch');
		Route::post('/chat-render-search-ajax',  'ChatController@chatRenderSearchAjax');
		Route::get('/chatroom/{id}',  'ChatController@chating');
	    Route::post('/storemsg', 'ChatController@storemsg');
    /* Chat end */    

    /* Payment gateways */
		Route::get('thankyou', 'StripePaymentController@thankyou');
		Route::post('stripe', 'StripePaymentController@stripePost')->name('stripe.post');
		Route::post('/stripeSave','StripePaymentController@stripeSave')->name('stripeSave.post');
		Route::post('/stripeCardSave','StripePaymentController@stripeCardSave')->name('stripeCardSave.post');
		

	/* Payment gateways */

	/* cart */
		Route::match(['get','post'],'ajax-cart','PaymentController@ajax_cart')->name('ajax-cart');
	/* cart */

	Route::get('/search/website/{domain_url}/{id}', 'Search\SearchController@pricing_select');
	
	/* switch user */
		Route::get('/switched', 'ProfileController@switched');
		Route::get('/switched-other', 'ProfileController@switchedOther');
	/* switch user */

	Route::get('edit-profile','ProfileController@edit_profile');
	Route::post('update_profile','ProfileController@store_profile');
	Route::post('/change_password','ProfileController@store_change_password');
			
	Route::group(['middleware' => 'is_seller'], function(){
		Route::get('/publisher/dashboard', 'Seller\SellerController@dashboard');
		Route::get('/publisher/add-website', 'Seller\SellerController@add_website');
		Route::post('/publisher/add-website', 'Seller\SellerController@add_websitePOST');
		Route::get('/publisher/my-websites', 'Seller\SellerController@my_websites');
		Route::get('/publisher/edit_website/{id}','Seller\SellerController@edit_website');
		Route::get('/publisher/orders', 'Seller\SellerController@orders');
		Route::get('/publisher/analytics', 'Seller\SellerController@analytics');
		Route::post('/publisher/add_website','Seller\SellerController@insert_website');
		Route::post('/publisher/update_website','Seller\SellerController@update_website');
		Route::post('/publisher/delete_website','Seller\SellerController@delete_website');
		Route::post('/publisher/order/accept','Seller\SellerController@order_accept');
		Route::post('/publisher/order/reject','Seller\SellerController@order_reject');

		Route::get('/publisher/content/{order_id}','Seller\SellerController@content');
		Route::post('/publisher/content','Seller\SellerController@content_POST');
		

		/** update status by token **/
		Route::get('/publisher/updateWebsiteStatus/{domain_id}','Seller\SellerController@updateWebsiteStatus');
		Route::get('/verifyExistUrl','Seller\SellerController@verifyExistUrl');
		Route::get('/matchExistUrl','Seller\SellerController@matchExistUrl');
		Route::get('/publisher/getInvoice/{id}','Seller\SellerController@getInvoice');
		Route::get('/publisher/cart/','Seller\SellerController@cart_listings');
	});
	
	Route::group(['middleware' => 'is_buyer'], function(){	
		// Buyer
		Route::get('/advertiser/dashboard', 'Buyer\BuyerController@dashboard');
		Route::get('advertiser/add-website', function(){
			return Redirect::to('publisher/add-website');
		});

		Route::match(['get','post'],'cart/delete','PaymentController@delete');
		Route::match(['get','post'],'advertiser/cart','PaymentController@cart')->name('cart');	
		Route::get('/advertiser/purchases', 'Buyer\BuyerController@purchases');
		Route::post('/advertiser/add_orders','Buyer\BuyerController@add_orders');
		Route::post('/advertiser/apply_coupon','Buyer\BuyerController@apply_coupon');
		Route::post('/advertiser/add_manualOrder','Buyer\BuyerController@add_manualOrder');
		Route::post('/advertiser/add_WalletOrder','Buyer\BuyerController@add_WalletOrder');
		Route::match(['get','post'],'/advertiser/wishlist',function(){
			return view('wishlist');
		});

		Route::get('/advertiser/content/{order_id}','Buyer\BuyerController@content');
		Route::post('/advertiser/content','Buyer\BuyerController@content_POST');

		Route::get('/advertiser/getInvoice/{id}','Buyer\BuyerController@getInvoice');
		
		Route::post('/advertiser/add_reviews_db','Buyer\BuyerController@add_reviews_db');
	});

	Route::group(['middleware' => 'is_admin'], function(){
		/** routes for admin panel **/
		Route::get('/admin/dashboard','Admin\AdminController@dashboard');	
		Route::get('/admin/websites','Admin\AdminController@websites');
		Route::get('/admin/edit_website/{id}','Admin\AdminController@edit_website');
		Route::post('/admin/update_website','Admin\AdminController@update_website');
		Route::post('/admin/delete_website','Admin\AdminController@delete_website');
		Route::get('/admin/users','Admin\AdminController@users');
		Route::get('/admin/searches','Admin\AdminController@searches');		
		//Route::get('/admin/searches-ajax','Admin\AdminController@searchesAjax');

		Route::post('/admin/searches','Admin\AdminController@searches');
		Route::post('/admin/updateUserStatus','Admin\AdminController@updateUserStatus');
		Route::get('/admin/orders','Admin\AdminController@orders');

		Route::post('/admin/order/accept','Admin\AdminController@order_accept');
		Route::post('/admin/order/reject','Admin\AdminController@order_reject');
		Route::post('/admin/order/cancel','Admin\AdminController@order_cancel');

		Route::get('/admin/website/accept/{id}','Admin\AdminController@website_accept');
		Route::get('/admin/website/reject/{id}','Admin\AdminController@website_reject');
		Route::post('/admin/payToSeller','Admin\AdminController@payToSeller');
		Route::post('/admin/payToSellerPayouts','Admin\AdminController@payToSellerPayouts');
		Route::post('/admin/payToSellerManual','Admin\AdminController@payToSellerManual');

		Route::post('/admin/add_paypalEmail','Admin\AdminController@add_paypalEmail');
		Route::get('/admin/getInvoice/{id}','Admin\AdminController@getInvoice');

		Route::post('/admin/daPaSave','Admin\AdminController@da_pa_save');
		Route::get('/admin/cart/','Admin\AdminController@cart_listings');


		Route::post('admin/chat-render',  'Admin\AdminController@chatRenderAll');

		Route::get('/admin/chats/','Admin\AdminController@getchat');

		Route::get('/admin/cart/','Admin\AdminController@cart_listings');

		Route::get('/admin/wishlist','Admin\AdminController@wishlist_items');

		Route::get('/admin/bulk_upload_websites','Admin\AdminController@bulk_upload_websites');
		Route::post('/admin/upload_file','Admin\AdminController@upload_file');
	});

});
