@extends('layout.website')

@section('content')
@section('mainhead')
@parent
@show
@section('customCSS')

@php
$totalTitle = 'List of ' . $search_record . ' websites that accept Guest Blogs by GuestPostEngine.';
@endphp
@section('title', $totalTitle)
@section('title-description')

<meta name="description" content="GuestPostEngine is a database of 3000+ websites of all categories that accept guest posting. In this pool, you can search websites according to your category.">
    
@endsection

 <!--begin::Page Vendors Styles(used by this page) -->
    <link href="{{ asset('') . config('app.public_url') . '/assets/plugins/custom/fullcalendar/fullcalendar.bundle.css' }}" rel="stylesheet" type="text/css" />    
@endsection

<div class="search-wrapper kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">

    <div class="banner-wrapper text-center">
        @include('layout.include.partials.banner')
    </div>
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">
            <div class="kt-subheader   kt-grid__item" id="kt_subheader">
                <div class="w-100">
                    <button type="button" class="btn btn-label-brand btn-bold kt-ml-0" id="alert-btn">Get Alert</button>
                    <div class="pull-right">
                        <button type="button" class="btn btn-light btn-elevate" data-toggle="dropdown" aria-expanded="true"><i class="flaticon2-console"></i><span style="padding-right: 0.5rem;">Sort By :</span> 
                            @if(@$_GET['order'])
                                <?php
                                    $active = $_GET['order'];
                                    $by = $_GET['orderwise'];
                                ?>
                            @else
                                <?php
                                    $active = 'dp_da';
                                    $by = '';
                                ?>
                            @endif
                            @if($active == 'dp_da')
                                <span id="sort-append"> DA : High to Low</span>
                            @endif
                            @if($active == 'sd_visits')
                                <span id="sort-append"> Traffic: High to Low</span>
                            @endif
                            @if($active == 'cost_price' && $by == 'DESC')
                                <span id="sort-append"> Price: High to Low</span>
                            @endif
                            @if($active == 'cost_price' && $by == 'ASC')
                                <span id="sort-append"> Price: Low to High </span>
                            @endif

                            <i class="la la-angle-down kt-ml-5 kt-padding-0"></i> </button>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-fit dropdown-menu-xs">
                            <ul class="kt-nav">
                                <li class="kt-nav__item @if($active == 'dp_da') {{ 'kt-nav__item--active' }} @endif " id="da_high">
                                    <a href="javascript:void(0);" data-order="dp_da" data-order-wise="DESC" data-rel=" DA: High to Low " rel="da_high" class="kt-nav__link">
                                    <span class="kt-nav__link-text"> DA: High to Low </span>
                                    </a>
                                </li>
                                <li class="kt-nav__item @if($active == 'sd_visits') {{ 'kt-nav__item--active' }} @endif "  id="traffic_high">
                                    <a href="javascript:void(0);" data-order="sd_visits" data-order-wise="DESC" data-rel=" Traffic: High to Low " rel="traffic_high" class="kt-nav__link">
                                    <span class="kt-nav__link-text"> Traffic: High to Low </span>
                                    </a>
                                </li>
                                <li class="kt-nav__item @if($active == 'cost_price' && $by == 'DESC') {{ 'kt-nav__item--active' }} @endif"  id="price_high">
                                    <a href="javascript:void(0);" data-order="cost_price" data-order-wise="DESC" data-rel=" Price: High to Low " rel="price_high" class="kt-nav__link">
                                    <span class="kt-nav__link-text"> Price: High to Low </span>
                                    </a>
                                </li>
                                <li class="kt-nav__item @if($active == 'cost_price' && $by == 'ASC') {{ 'kt-nav__item--active' }} @endif"  id="price_low">
                                    <a href="javascript:void(0);" data-order="cost_price"  data-order-wise="ASC" data-rel=" Price: Low to High " rel="price_low" class="kt-nav__link">
                                        <span class="kt-nav__link-text"> Price: Low to High </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        
                    </div>
                </div>
            </div>          
            <div class="alert alert-solid-brand alert-bold hidden" role="alert" id="show-alert">
               <!-- <div class="alert-text justify-content-between d-flex align-items-center"><p class="kt-mb-0">Currently we have 100 websites for your keyword " .." once we have more websites for this keyword we will send you an email.</p>
                    <a type="submit" class="btn btn-brand btn-upper btn-hover-content" data-dismiss="alert" href="#">Submit</a>
                </div>-->
                <div class="alert-text justify-content-between d-flex align-items-center">
                    <p class="kt-mb-0">Coming Soon....<p>
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon" data-dismiss="alert" aria-label="Close" style="cursor:pointer;">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#000000">
                                <rect x="0" y="7" width="16" height="2" rx="1"></rect>
                                <rect opacity="0.3" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000) " x="0" y="7" width="16" height="2" rx="1"></rect>
                            </g>
                        </g>
                    </svg>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3 col-12  kt-mb-20">
                    <div class="fixed-sidebar">
                        <section>
                            <div class="kt-portlet kt-portlet--tabs kt-portlet--height-fluid">
                                <div class="kt-portlet__head">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title">
                                            <i class="la la-filter kt-padding-r-5"></i>Filter Websites
                                        </h3>
                                    </div>
                                </div>
                                <div class="kt-portlet__body">
                                    <!-- <form class="" id="kt_subheader_search_form"> -->
                                        <div class="kt-input-icon kt-input-icon--right kt-subheader__search">
                                            <input type="text" class="form-control search-ajax" placeholder="Search Domain" id="generalSearch" rel="website" name="website" value="{{@$_GET['generalSearch']}}">
                                            <span class="kt-input-icon__icon kt-input-icon__icon--right">
                                                <span>
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"></rect>
                                                            <path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                                            <path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" fill="#000000" fill-rule="nonzero"></path>
                                                        </g>
                                                    </svg>
                                                    <!--<i class="flaticon2-search-1"></i>-->
                                                </span>
                                            </span>
                                        </div>
                                    <!-- </form> -->
                                    <div class="kt-checkbox-list kt-mt-25">                             
                                        <label class="kt-checkbox">
                                        <?php if(@$_GET['verify']) :?>

                                        
                                                <input type="checkbox" name="verify" id="owner_verify" checked="checkbox">   
                                        Show Owners Websites

                                        <?php   else: ?>
                                                <input type="checkbox" name="verify" id="owner_verify" >  
                                        Show Owners Websites
                                        <?php endif; ?>

                                        <span></span>
                                        </label>
                                    </div>
                                    <div class="kt-mt-20 da-irs">
                                        <label class="col-form-label">Domain Authority</label>
                                        <div class="kt-ion-range-slider">
                                            
                                            <input type="hidden" id="kt_slider_DA" name="da" />
                                        </div>
                                    </div>
                                    <div class="kt-mt-20 traffic-irs">
                                        <label class="col-form-label">Traffic</label>
                                        <div class="kt-ion-range-slider">
                                            <input type="hidden" id="kt_slider_traffic" name="traffc" />
                                        </div>
                                    </div>
                                    <div class="kt-mt-20 order-irs">
                                        <label class="col-form-label">Orders in Queue</label>
                                        <div class="kt-ion-range-slider">
                                            <input type="hidden" id="kt_slider_orders" name="order" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <?php 
                            $query = $_GET['q'];
                            $findme = '"';
                            $quotes = 0;
                            if (strpos($query, $findme) === false) {
                                $quotes = 0;
                                $findme   = "'";
                                if (strpos($query, $findme) === false) {
                                    $quotes = 0;
                                } else {
                                    $quotes = 1;
                                }
                            } else {
                                $quotes = 1;
                            }
                            $terms = str_replace('"', "", $query);
                            $terms = str_replace("'", "", $terms);
                        ?>
                        <input type="hidden" id="get_value" name="get_value" value="{{$terms}}">
                    </div>
                </div>
                <div class="col-md-9 col-12" id="append-search-data">
                    <!-- render search -->
                    @include('render.search')
                </div>
                <!-- end:: Content -->                  
            </div>
        </div>
    </div>
</div>

        <!--ENd:: Chat-->
        @section('mainscripts')
        @parent

        @section('customScript')
        <script>
        $("#alert-btn").click(function() {
            $("#show-alert").show("fade");
        });
        </script>
   
        <!--begin::Page Vendors Styles(used by this page) -->
        <script href="{{ asset('') . config('app.public_url') . '/assets/js/owl.carousel.js'  }}" type="text/javascript"></script>
    <!--     <script src="{{ asset('') . config('app.public_url') . '/assets/js/pages/crud/forms/widgets/ion-range-slider.js' }}" type="text/javascript"></script> -->

        <script>
            // Class definition

            var KTIONRangeSlider = function () {
                
                var ajaxResultSlider = function () {
                    ajaxResult();
                };
                
                function ajaxResult(data_order='dp_da', data_order_wise='DESC' ){
                    var quotes = '<?php echo $quotes;?>';
                    var url = "{{url('/search')}}";
                    // var q = $('#get_value').val();
                    var q = $('#search_keyword').val();

                    if(q != ''){
                        url += "?q="+q;    
                    }
                    var offset = "{{@$_GET['offset']}}";
                    if(offset != ''){
                        url += "&offset="+offset;    
                    }

                    url += "&quotes="+quotes;    
                    
                    var page = "{{@$_GET['page']}}";
                    if(page != ''){
                        url += "&page="+page;
                    }
                    var generalSearch = $('#generalSearch').val();
                    generalSearch = generalSearch.replace(/ /g, '');
                    if(generalSearch != ''){
                        url += "&generalSearch="+generalSearch;
                    }
                    var da_start = $('.da-irs .irs-from').text();
                    da_start = da_start.replace(/ /g, '');
                    var da_end = $('.da-irs .irs-to').text();
                    da_end = da_end.replace(/ /g, '');
                    if(da_start != '' && da_end != ''){
                        url += "&da_start="+da_start;
                        url += "&da_end="+da_end;
                    }
                    var traffic_start = $('.traffic-irs .irs-from').text();
                    traffic_start = traffic_start.replace(/ /g, '');
                    var traffic_end = $('.traffic-irs .irs-to').text();
                    traffic_end = traffic_end.replace(/ /g, '');
                    if(traffic_start != '' && traffic_end != ''){
                        if(traffic_start > 0 || traffic_end < 1000000){
                            url += "&traffic_start="+traffic_start;
                            url += "&traffic_end="+traffic_end;
                        }
                    }

                    var orders_start = $('.order-irs .irs-from').text();
                    orders_start = orders_start.replace(/ /g, '');
                    var orders_end = $('.order-irs .irs-to').text();
                    orders_end = orders_end.replace(/ /g, '');
                    if(orders_start != '' && orders_end != ''){
                        if(orders_start > 0 || orders_end < 100){
                            url += "&orders_start="+orders_start;
                            url += "&orders_end="+orders_end;
                        }
                    }

                    var $boxes = $('input[name=verify]:checked');
                    var checked = 2;
                    if($boxes[0]){
                        if($boxes[0].type == 'checkbox'){
                            checked = 1;
                            url += "&verify="+checked;
                        } else {
                            checked = 2;
                        }
                    }
                                

                    if($( "#da_high" ).hasClass( "kt-nav__item--active" )){
                        url += "&order=dp_da&orderwise=DESC";
                    } else if($( "#traffic_high" ).hasClass( "kt-nav__item--active" )){
                        url += "&order=sd_visits&orderwise=DESC";
                    } else if($( "#price_high" ).hasClass( "kt-nav__item--active" )){
                        url += "&order=cost_price&orderwise=DESC";
                    } else if($( "#price_low" ).hasClass( "kt-nav__item--active" )){
                        url += "&order=cost_price&orderwise=ASC";
                    } else {
                        url += "&order="+data_order+"&orderwise="+data_order_wise;
                    }
                    
                    $(".loader").fadeIn("slow");
                    window.location.href = url;

                    // $.ajax({
                    //     headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    //     url  : url,
                    //     type: 'GET',
                    //     cache: false,                        
                    //     beforeSend: function() {
                    //         //something before send
                    //         $(".loader").fadeIn("slow");
                    //     },
                    //     success: function(data) {
                    //         $('#append-search-data').html(data);
                    //         $(".loader").fadeOut("slow");
                    //     },
                    //     error: function(xhr,textStatus,thrownError) {
                    //         alert(xhr + "\n" + textStatus + "\n" + thrownError);
                    //     }
                    // });                    
                }

                // Private functions
                var demos = function () {

                    var get = <?php echo json_encode($_GET); ?>;
                    
                    var da_start = 20;
                    var da_end = 100;
                    if(get.da_start){
                        da_start = get.da_start;
                        da_end = get.da_end;
                    }

                    // min & max values
                    $('#kt_slider_DA').ionRangeSlider({
                        type: "double",
                        grid: true,
                        min: 20,
                        max: 100,
                        from: da_start,
                        to: da_end,
                        onFinish: ajaxResultSlider
                    });                    

                    var traffic_start = 0;
                    var traffic_end = 1000000;
                    if(get.traffic_start){
                        traffic_start = get.traffic_start;
                        traffic_end = get.traffic_end;
                    }


                    $('#kt_slider_traffic').ionRangeSlider({
                        type: "double",
                        grid: true,
                        min: 0,
                        max: 1000000,
                        //step:5000,
                        from: traffic_start,
                        to: traffic_end,
                       // scale:[100,200,300,400,500,600,1000000],
                        //isRange:true,
                        onFinish: ajaxResultSlider
                    });                    


                    var orders_start = 0;
                    var orders_end = 100;
                    if(get.orders_start){
                        orders_start = get.orders_start;
                        orders_end = get.orders_end;
                    }

                    $('#kt_slider_orders').ionRangeSlider({
                        type: "double",
                        grid: true,
                        min: 0,
                        max: 100,
                        from: orders_start,
                        to: orders_end,
                        onFinish: ajaxResultSlider
                    });


                    $(document).on('keypress','#generalSearch',function(event){
                        var keycode = (event.keyCode ? event.keyCode : event.which);
                        if(keycode == '13'){
                            ajaxResult();
                        }
                        
                    });
                    $(document).on('click','#owner_verify',function(){
                        ajaxResult();
                    });

                    $(document).on('click','.kt-nav .kt-nav__item .kt-nav__link',function() {
                        var rel = $(this).attr('rel');
                        var data_rel = $(this).attr('data-rel');
                        var data_order = $(this).attr('data-order');
                        var data_order_wise = $(this).attr('data-order-wise');
                        $('.kt-nav .kt-nav__item').removeClass('kt-nav__item--active');
                        $('#'+rel).addClass('kt-nav__item--active');
                        $('#sort-append').text(data_rel);                        
                        ajaxResult(data_order,data_order_wise);
                    });                    
                }

                return {
                    // public functions
                    init: function() {
                        demos(); 
                    }
                };
            }();

            jQuery(document).ready(function() {
                KTIONRangeSlider.init();
            });

            $(document).ready(function() {
              $('.owl-carousel').owlCarousel({
                loop: true,
                responsiveClass: true,
                responsive: {
                  0: {
                    items: 1,
                    nav: false,
                    dots: false,
                    autoplay: true
                  },
                  600: {
                    items: 2,
                    nav: false,
                    dots: false,
                    autoplay: true
                  },
                  768: {
                    items: 2,
                    nav: false,
                    dots: false
                  },
                  900: {
                    items: 3,
                    nav: false,
                    dots: false
                  },
                  1200: {
                    items: 4,
                    nav: false,
                    dots: false
                  },
                  1399: {
                    items: 5,
                    nav: false,
                    dots: false
                  }
                }
              })
            })          
        </script>
        <script type="text/javascript">
            //Getting value from "ajax.php".
            function fill(Value) {
               //Assigning value to "search" div in "search.php" file.
               $('#search_keyword').val(Value);
               window.location.href = 'search?q='+Value;
               //Hiding "display" div in "search.php" file.
               $('#display').hide();
            }

            $(document).on('click','.ajax-display',function(){
                fill($(this).attr('rel'));
            });

            /** script for onsroll down and up **/
            // $(window).keydown(function(e){ 
            //     if(e.which ===   40 ){
            //        //alert('true');
            //     }else if(e.which === 38){
            //         //alert('false');
            //     }
            // });

             /*$(document).hover('.list-items >  li > a',function() {
                    alert('hover working!!');
                });*/
            
            // window.onload = function(){
            //     var hideMe = document.getElementById('display');
            //     document.onclick = function(e){
            //        if(e.target.id !== 'hideMe'){

            //         alert('s');

            //          $('#display').hide();
                      /*$('#display').removeClass('search-dropdown');
                      $('#display').html('');*/
            //        }
            //     };
            // };
            $(document).ready(function() {
               //On pressing a key on "Search box" in "search.php" file. This function will be called.
               $("#search_keyword").keyup(function() {
                    var rel = $(this).attr('rel');
                    $('#display').addClass('search-dropdown');
                   //Assigning search box value to javascript variable named as "name".
                   var name = $(this).val();

                   //Validating, if "name" is empty.
                   if (name == "") {
                       //Assigning empty value to "display" div in "search.php" file.
                       $('#display').html("");
                   }
                   //If name is not empty.
                   else {
                       //AJAX is called.
                       $.ajax({
                           //AJAX type is "Post".
                           headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                           },
                            type: "POST",
                           //Data will be sent to "ajax.php".
                           url: "{{ url('/search_result') }}",
                           //Data, that will be sent to "ajax.php".
                           data: {
                               //Assigning value of "name" into "search" variable.
                               search: name,
                           },
                           //If result found, this funtion will be called.
                           success: function(html) {
                               //Assigning result to "display" div in "search.php" file.
                               $('#display').html(html).show();
                           }
                       });
                   }
               });
            });

            $(document).on('change','.kt-font-brand', function(){
                var value = $(this).val();
                var search = "{{@$_GET['q']}}";

                if(search){
                    search = '?q=' + search;
                }

                var page = "{{@$_GET['page']}}";
                if(page){
                    page = '&page=' + page;
                }

                if(value){
                    value = '&offset=' + value;   
                }
                var APP_URL = "{{url('')}}" + '/search/' + search + page + value;
                window.location.href = APP_URL;
            });


        </script>

        @endsection
<!--end::Page Scripts -->

        @show

@endsection