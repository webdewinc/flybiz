<!-- ref= https://github.com/Rob--W/cors-anywhere -->
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>CORS</title>
<meta name="viewport" content="width=device-width">

<style>
html, body {
  margin: 0;
  height: 100%;
  padding: 3px;
  font-family: Arial, sans-serif;
  font-size: 16px;
}
* {
  -moz-box-sizing: border-box;
       box-sizing: border-box;
}
label { display: block; }
input {
  display: block;
  width: 100%;
  padding: 8px 5px;
  border: 1px solid #CCC;
}
button {
  display: inline-block;
  width: 49%;
  padding: 8px;
}
textarea {
  width: 100%;
  height: 100%;
}
#top {
  height: 180px;
  position: relative;
}
#bottom {
  height: 100%;
  margin-top: -180px;
  padding-top: 180px;
}
</style>
</head>
<body>

<div id="top">
    CORS Demo
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
  <label>
      Url to be fetched
      <input type="url" id="url" value="">
  </label>
  <label>
      <!-- If using POST, enter the data: -->
      <input type="hidden" id="data">
  </label>
  <label>
      <button id="get">GET</button>
      <button id="post">POST</button> 
  </label>
</div>


<div id="bottom">
  <div id="h1"></div>
  <div id="h2"></div>
  <div id="h3"></div>
  <div id="h4"></div>
  <div id="h5"></div>
  <div id="h6"></div>
  <div id="a"></div>
  <div id="p"></div>
  <div id="title"></div>
  <div id="description"></div>

  <!-- <textarea id="output">
      
  </textarea> -->
</div>

<script>
  
  
  //var cors_api_url = 'https://cors-anywhere.herokuapp.com/';
  var cors_api_url = 'http://localhost:5688/';
  function doCORSRequest(options, printResult) {
    var x = new XMLHttpRequest();
    x.open(options.method, cors_api_url + options.url);
    x.onload = x.onerror = function() {
      printResult(
        options.method + ' ' + options.url + '\n' +
        x.status + ' ' + x.statusText + '\n\n' +
        (x.responseText || '')
      );
    };
    if (/^POST/i.test(options.method)) {
      x.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    }
    x.send(options.data);
  }

  function ajaxCall(url, p, a, h1, h2, h3, h4, h5, h6, content, id, title, fav ){
    
      $.post("{{url('/cors-store')}}",
        { p: p, a: a, h1: h1, h2: h2, h3: h3, h4: h4, h5: h5, h6: h6, description: content, id: id, title: title, favicon : fav },
        function(data, status) {
          //alert("Data: " + data + "\nStatus: " + status);
      });

  }
  function storeData(url, id){
    
      doCORSRequest({
        method: 'GET',
        url: url        
      }, function printResult(result) {
       
       // var  title = jQuery(result).filter('a').attr('href');
        //console.log(result);

        var div = $(result).clone();
        var h1 = div.find('h1').add().end();
        if(h1 != ''){
            $('#h1').html('<h1>H1</h1>' + h1.text() );
        }
        var h2 = div.find('h2').add().end();        
        if(h2 != ''){
            $('#h2').html('<h2>H2</h2>' + h2.text() );   
        }
        var h3 = div.find('h3').add().end();
        if(h3 != ''){
            $('#h3').html('<h3>H3</h3>' + h3.text() );
        }
        var h4 = div.find('h4').add().end();
        if(h4 != ''){
            $('#h4').html('<h4>H4</h4>' + h4.text() );
        }
        var h5 = div.find('h5').add().end();
        if(h5 != ''){
            $('#h5').html('<h5>H5</h5>' + h5.text() );
        }
        var h6 = div.find('h6').add().end();
        if(h6 != ''){
            $('#h6').html('<h6>H6</h6>' + h6.text() );
        }
        var a = div.find('a').add().end();
        if(a != ''){
            $('#a').html('<h1>A</h1>' + a.text() );
        }
        var p = div.find('p').add().end();
        if(p != ''){
            $('#p').html('<h1>P</h1>' + p.text() );
        }
        var  title = jQuery(result).filter('title').text();
        
        if(title != ''){
            $('#title').html('<h1>Title</h1>' + title );
        }
        
        //outputField.value = div.text();
        // var content = jQuery(result).filter('meta[name=description]').attr("content");
        //var title = jQuery(result).filter('title').attr("content");
        var content = jQuery(result).filter('meta[name=description]').attr("content");
        //var title = jQuery(result).filter('meta[property=og:title]').attr("content");
        var fav = jQuery(result).filter('link[rel=shortcut icon]').attr("href");

        //$('#title').html('<h1>Title</h1>' + title );
        $('#description').html('<h1>Description</h1>' + content );

        var url_get = "{{url('cors-store')}}";
        // store data
        ajaxCall(url_get, p.text(), a.text(), h1.text(), h2.text(), h3.text(), h4.text(), h5.text(), h6.text(), content, id, title, fav);
       
          //return;
      });

  }

  var data = <?php echo json_encode($arr); ?>;
  for (var i = data.length - 1; i >= 0; i--) {    
      var str = data[i].domain_url;
      str.replace("http://", "https://");
      str.replace('/', '');
      console.log(str);
      storeData(data[i].domain_url, data[i].domain_id);
            
  }

  // Bind event
  // (function() {
  //   var urlField = document.getElementById('url');
  //   var dataField = document.getElementById('data');
  //   var outputField = document.getElementById('output');
  //   document.getElementById('get').onclick =
  //   document.getElementById('post').onclick = function(e) {
  //     e.preventDefault();
  //     doCORSRequest({
  //       method: this.id === 'post' ? 'POST' : 'GET',
  //       url: urlField.value,
  //       data: dataField.value
  //     }, function printResult(result) {
        
  //       // outputField.value = result;

  //       var div = $(result).clone();
  //       var h1 = div.find('h1').add().end();
  //       if(h1 != ''){
  //           $('#h1').html('<h1>H1</h1>' + h1.text() );
  //       }
  //       var h2 = div.find('h2').add().end();        
  //       if(h2 != ''){
  //           $('#h2').html('<h2>H2</h2>' + h2.text() );   
  //       }
  //       var h3 = div.find('h3').add().end();
  //       if(h3 != ''){
  //           $('#h3').html('<h3>H3</h3>' + h3.text() );
  //       }
  //       var h4 = div.find('h4').add().end();
  //       if(h4 != ''){
  //           $('#h4').html('<h4>H4</h4>' + h4.text() );
  //       }
  //       var h5 = div.find('h5').add().end();
  //       if(h5 != ''){
  //           $('#h5').html('<h5>H5</h5>' + h5.text() );
  //       }
  //       var h6 = div.find('h6').add().end();
  //       if(h6 != ''){
  //           $('#h6').html('<h6>H6</h6>' + h6.text() );
  //       }
  //       var a = div.find('a').add().end();
  //       if(a != ''){
  //           $('#a').html('<h1>A</h1>' + a.text() );
  //       }
  //       var p = div.find('p').add().end();
  //       if(p != ''){
  //           $('#p').html('<h1>P</h1>' + p.text() );
  //       }

  //       var  title = jQuery(result).filter('title').text();
        
  //       if(title != ''){
  //           $('#title').html('<h1>Title</h1>' + title );
  //       }
        
  //       //outputField.value = div.text();
  //     	// var content = jQuery(result).filter('meta[name=description]').attr("content");
  //      //  var title = jQuery(result).filter('meta[name=title]').attr("content");
  //       // var content = jQuery(result).filter('meta[property=og:description]').attr("content");
  //       // var title = jQuery(result).filter('meta[property=og:title]').attr("content");

  //       // $('#title').html('<h1>Title</h1>' + title );
  //       // $('#description').html('<h1>Description</h1>' + content );

  //     	// console.log(content);
  //      //  outputField.value = title + ' ' + content;
  //     });
  //   };
  // })();
  // if (typeof console === 'object') {
  //   console.log('// To test a local CORS Anywhere server, set cors_api_url. For example:');
  //   console.log('cors_api_url = "http://localhost:8080/"');
  // }
</script>
</body>
</html>