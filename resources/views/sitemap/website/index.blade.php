<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
	@foreach ($MstDomainUrl as $key => $domain)
		<url>
			<loc>{{url('/search/website/'.trim(@$domain->domain_url))}}</loc>
            <lastmod>{{ @$domain->created_at }}</lastmod>
			<changefreq>daily</changefreq>
			<priority>0.80</priority>
		</url>
	@endforeach
	<url>
		<loc>{{url('/')}}</loc>
        <lastmod>{{ date('Y-m-d').'T'.date('H:m:s').'+00:00' }}</lastmod>
		<changefreq>daily</changefreq>
		<priority>0.80</priority>
	</url>
	<url>
		<loc>{{url('/search')}}</loc>
        <lastmod>{{ date('Y-m-d').'T'.date('H:m:s').'+00:00' }}</lastmod>
		<changefreq>daily</changefreq>
		<priority>0.80</priority>
	</url>
	<url>
		<loc>{{url('/signin')}}</loc>
        <lastmod>{{ date('Y-m-d').'T'.date('H:m:s').'+00:00' }}</lastmod>
		<changefreq>daily</changefreq>
		<priority>1</priority>
	</url>
	<url>
		<loc>{{url('/signup')}}</loc>
        <lastmod>{{ date('Y-m-d').'T'.date('H:m:s').'+00:00' }}</lastmod>
		<changefreq>daily</changefreq>
		<priority>1</priority>
	</url>
</urlset>