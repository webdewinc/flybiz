<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
	@foreach ($MstDomainUrl as $key => $domain)
		<url>
			<loc>{{url('/search?q='.trim(@$key))}}</loc>
            <lastmod>{{ @$domain->query_created_date }}</lastmod>
			<changefreq>daily</changefreq>
			<priority>0.80</priority>
		</url>
		@foreach ($domain as $k => $dom)
			<url>
				<loc>{{url('/search?q='.trim(@$k))}}</loc>
	            <lastmod>{{ @$domain->query_created_date }}</lastmod>
				<changefreq>daily</changefreq>
				<priority>0.80</priority>
			</url>
		@endforeach	
	@endforeach
</urlset>