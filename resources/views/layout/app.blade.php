<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

    <title>@yield('title')</title>
     
    <!-- begin::Head -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="rankz-verification" content="dmSMHt7jjgIOGHhK">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--RoboAuditor popup js code start-->
    <script  id="growthrobot" src="https://d3ikwiixxizqwk.cloudfront.net/assets/site/js/popup.js" data-content="5097"></script>
    <!--RoboAuditor popup js code end-->
    <!--begin::Page Vendors Styles(used by this page) -->
    
    @yield('title-description')

    @section('mainhead')
        @include('layout.include.mainhead')
    @show
    <!-- end::Head -->
    <!-- <script src="{{URL::asset('public/manifest.json')}}"></script> -->

    @yield('customCSS')

    @if(env('APP_ENV') == 'live')
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-MG2W4QB');</script>


    <!-- End Google Tag Manager -->

    @endif
    @if(Auth::check())

    <?php

        // Advertiser
        /* spend */
        $user_id = Auth::user()->user_id;

        $seller = DB::table('add_to_carts')
                      ->join('mst_domain_url','mst_domain_url.domain_id','add_to_carts.domain_id')
                      ->where('add_to_carts.promo_code',0)
                      ->where('add_to_carts.content_type','seller')
                      ->where('add_to_carts.user_id',$user_id)
                      ->where('add_to_carts.is_billed',1)                      
                      ->sum('mst_domain_url.cost_price');
        $seller_extra = DB::table('add_to_carts')
                      ->join('mst_domain_url','mst_domain_url.domain_id','add_to_carts.domain_id')
                      ->where('add_to_carts.promo_code',0)
                      ->where('add_to_carts.content_type','seller')
                      ->where('add_to_carts.user_id',$user_id)
                      ->where('add_to_carts.is_billed',1)
                      ->sum('mst_domain_url.extra_cost');
       
        if(!empty($seller)){
            $seller = buyerDomainPriceCalculate($seller,$seller_extra);
        }
        $buyer = DB::table('add_to_carts')
                      ->join('mst_domain_url','mst_domain_url.domain_id','add_to_carts.domain_id')
                      ->where('add_to_carts.user_id',$user_id)
                      ->where('add_to_carts.is_billed',1)
                      ->where('add_to_carts.promo_code',0)
                      ->where('add_to_carts.content_type','buyer')
                      ->sum('mst_domain_url.cost_price');
        
        if(!empty($buyer)){
            $buyer = buyerDomainWebsitePriceCalculate($buyer);
        }

        $spent = (float)$seller + (float)$buyer;

        /* spend */

        /* earned */
    
        $seller = DB::table('add_to_carts')
                      ->join('mst_domain_url','mst_domain_url.domain_id','add_to_carts.domain_id')
                      ->where('add_to_carts.promo_code',0)
                      ->where('add_to_carts.content_type','seller')
                      ->where('add_to_carts.domain_user_id',$user_id)
                      ->where('add_to_carts.is_billed',1)                      
                      ->sum('mst_domain_url.cost_price');
        $seller_extra = DB::table('add_to_carts')
                      ->join('mst_domain_url','mst_domain_url.domain_id','add_to_carts.domain_id')
                      ->where('add_to_carts.promo_code',0)
                      ->where('add_to_carts.content_type','seller')
                      ->where('add_to_carts.domain_user_id',$user_id)
                      ->where('add_to_carts.is_billed',1)                      
                      ->sum('mst_domain_url.extra_cost');
       
        if(!empty($seller)){
            $seller = buyerDomainPriceCalculate($seller,$seller_extra);
        }
        $buyer = DB::table('add_to_carts')
                      ->join('mst_domain_url','mst_domain_url.domain_id','add_to_carts.domain_id')
                      ->where('add_to_carts.domain_user_id',$user_id)
                      ->where('add_to_carts.is_billed',1)
                      ->where('add_to_carts.promo_code',0)
                      ->where('add_to_carts.content_type','buyer')
                      ->sum('mst_domain_url.cost_price');
        
        if(!empty($buyer)){
            $buyer = buyerDomainWebsitePriceCalculate($buyer);
        }

        $earned = (float)$seller + (float)$buyer;

        /* earned */

        $domain_count = DB::table('mst_domain_url')
        ->join('assigned_websites','assigned_websites.mst_domain_url_id','mst_domain_url.domain_id')
        ->where('assigned_websites.user_id',$user_id )
        ->where('mst_domain_url.cost_price','>',0)->where('mst_domain_url.domain_approval_status',1)->where('mst_domain_url.trash','<',1)->count();

        $adv = false;
        $pub = false;
        if(Auth::user()->content_type == 'Advertiser;Publisher'){
            $adv = true;
            $pub = true;
        } else if(Auth::user()->content_type == 'Publisher'){
            $pub = true;
        } else if(Auth::user()->content_type == 'Advertiser'){
            $adv = true;
        } else {
            if(Auth::user()->switched == 'publisher'){
                $pub = true;
            } else if(Auth::user()->switched == 'advertiser'){ 
                $adv = true;
            }
        }

    ?>

    <script>

      window.intercomSettings = {
        app_id: "qn6bzp46",
        name: "{{ @Auth::user()->user_fname }}", // Full name
        email: "{{ @Auth::user()->email }}", // Email address
        created_at: "{{ Auth::user()->created_at }}", // Signup date as a Unix timestamp
        UserName: "{{ Auth::user()->user_code }}",
        PublisherWebsitePublished : "{{$domain_count}}",
        PublisherTotalMoneyEarned : "{{@$earned}}",
        AdvertiserTotalMoneySpent : "{{@$spent}}",
        Publisher : "{{$pub}}",
        Advertiser : "{{$adv}}"
      };
    </script>

    @else
        
        <script>

          window.intercomSettings = {
            app_id: "qn6bzp46"
          };
        </script>

    @endif


    <script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',w.intercomSettings);}else{var d=document;var i=function(){i.c(arguments);};i.q=[];i.c=function(args){i.q.push(args);};w.Intercom=i;var l=function(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/qn6bzp46';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);};if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>

</head>
    <?php 
        if(Auth::check()){
            $class = Auth::user()->switched . '-wrapper ';
        } else {
            $class = '';
        }
    ?>
    <body class="{{$class}}kt-page--loading-enabled kt-page--loading kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--left kt-aside--fixed kt-page--loading">

    @if(env('APP_ENV') == 'live')
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MG2W4QB"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
    @endif
    <!-- begin::Page loader -->

        @include('layout.include.partials.loader')

        <!-- end::Page Loader -->

        <!-- begin:: Page -->

        <!-- begin:: Header Mobile -->
        <div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
            <div class="kt-header-mobile__logo">
                @include('layout.include.partials.brand')
            </div>
            <div class="kt-header-mobile__toolbar">
              
                
                <button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more-1"></i></button>
            </div>
        </div>

        <!-- end:: Header Mobile -->
        <div class="kt-grid kt-grid--hor kt-grid--root">
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
                <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

                    @include('layout.include.header')
                    @if(Auth::check())
                     @include('layout.include.aside')
                    @else
                        <div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop" id="kt_aside">

                            <!-- begin:: Aside Menu -->
                            <div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
                                <div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1">
                                    <ul class="kt-menu__nav ">
                                        <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon fa fa-cogs"></i><span class="kt-menu__link-text">Services</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>
                                            <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                                                <ul class="kt-menu__subnav">
                                                
                                                    <li class="kt-menu__item " aria-haspopup="true"><a href="https://www.webdew.com/services/guest-blogging" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Guest Blogging</span></a></li>
                                                    <li class="kt-menu__item " aria-haspopup="true"><a href="https://www.webdew.com/services/content-writing" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Category</span></a></li>
                                                    <li class="kt-menu__item " aria-haspopup="true"><a href="#" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">Toastr</span></a></li>
                                                    
                                                </ul>
                                            </div>
                                        </li>
                                        <li class="kt-menu__item " aria-haspopup="true"><a target="_blank" href="https://www.guestpostengine.com/category" class="kt-menu__link "><i class="kt-menu__link-icon fa fa-layer-group"></i><span class="kt-menu__link-text">Category</span></a></li>     
                                        <li class="kt-menu__item " aria-haspopup="true"><a href="https://www.guestpostengine.com/playbook/" class="kt-menu__link "><i class="kt-menu__link-icon fa fa-book-open"></i><span class="kt-menu__link-text">Playbook</span></a></li>
                                        
                            
                                    </ul>
                                </div>
                            </div>

                            <!-- end:: Aside Menu -->
                        </div>
                    @endif

                         @yield('content')

            <!-- end::Page Loader -->
                    @include('layout.include.footer')
            <!-- end:: Footer -->
                </div>
            </div>
        </div>

    <!-- end:: Page -->
   

        <!-- begin::Scrolltop -->
        <div id="kt_scrolltop" class="kt-scrolltop">
            <i class="fa fa-arrow-up"></i>
        </div>

        <!-- end::Scrolltop -->

        <!--Begin:: Chat-->
        <div class="modal fade- modal-sticky-bottom-right" id="kt_chat_modal" role="dialog" data-backdrop="false">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="kt-chat" id="chat-append-pop">
                        <!-- render function -->
                    </div>
                </div>
            </div>
        </div>

        @section('mainscripts')
            @include('layout.include.mainscripts')
        @show

        @yield('customScript')

        @if(Auth::check())
            @if(Auth::user()->user_type == 'admin')
                <!-- <script type="text/javascript">
                $(document).ready(function () {
                    //Disable cut copy paste
                    $('body').bind('cut copy paste', function (e) {
                        e.preventDefault();
                    });
                   
                    //Disable mouse right click
                    $("body").on("contextmenu",function(e){
                        return false;
                    });
                });
                </script> -->
            @endif
        @endif

    </body>
    <!-- end::Body -->
</html>