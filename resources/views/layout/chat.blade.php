<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

    <title>@yield('title')</title>
     
    <!-- begin::Head -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="rankz-verification" content="dmSMHt7jjgIOGHhK">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--RoboAuditor popup js code start-->
    <script  id="growthrobot" src="https://d3ikwiixxizqwk.cloudfront.net/assets/site/js/popup.js" data-content="5097"></script>
    <!--RoboAuditor popup js code end-->
    <!--begin::Page Vendors Styles(used by this page) -->
    @yield('title-description')
    @include('layout.include.chathead')

    @yield('customCSS')
     @if(env('APP_ENV') == 'live')
      <!-- Google Tag Manager -->
      <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
      new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
      'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
      })(window,document,'script','dataLayer','GTM-MG2W4QB');</script>
      <!-- End Google Tag Manager -->
    @endif
    <!-- end::Head -->
    <!-- <script src="{{URL::asset('public/manifest.json')}}"></script> -->

     @if(Auth::check())

    <?php

        // Advertiser
        /* spend */
        $user_id = Auth::user()->user_id;

        $seller = DB::table('add_to_carts')
                      ->join('mst_domain_url','mst_domain_url.domain_id','add_to_carts.domain_id')
                      ->where('add_to_carts.promo_code',0)
                      ->where('add_to_carts.content_type','seller')
                      ->where('add_to_carts.user_id',$user_id)
                      ->where('add_to_carts.is_billed',1)                      
                      ->sum('mst_domain_url.cost_price');
        $seller_extra = DB::table('add_to_carts')
                      ->join('mst_domain_url','mst_domain_url.domain_id','add_to_carts.domain_id')
                      ->where('add_to_carts.promo_code',0)
                      ->where('add_to_carts.content_type','seller')
                      ->where('add_to_carts.user_id',$user_id)
                      ->where('add_to_carts.is_billed',1)
                      ->sum('mst_domain_url.extra_cost');
       
        if(!empty($seller)){
            $seller = buyerDomainPriceCalculate($seller,$seller_extra);
        }
        $buyer = DB::table('add_to_carts')
                      ->join('mst_domain_url','mst_domain_url.domain_id','add_to_carts.domain_id')
                      ->where('add_to_carts.user_id',$user_id)
                      ->where('add_to_carts.is_billed',1)
                      ->where('add_to_carts.promo_code',0)
                      ->where('add_to_carts.content_type','buyer')
                      ->sum('mst_domain_url.cost_price');
        
        if(!empty($buyer)){
            $buyer = buyerDomainWebsitePriceCalculate($buyer);
        }

        $spent = (float)$seller + (float)$buyer;

        /* spend */

        /* earned */
    
        $seller = DB::table('add_to_carts')
                      ->join('mst_domain_url','mst_domain_url.domain_id','add_to_carts.domain_id')
                      ->where('add_to_carts.promo_code',0)
                      ->where('add_to_carts.content_type','seller')
                      ->where('add_to_carts.domain_user_id',$user_id)
                      ->where('add_to_carts.is_billed',1)                      
                      ->sum('mst_domain_url.cost_price');
        $seller_extra = DB::table('add_to_carts')
                      ->join('mst_domain_url','mst_domain_url.domain_id','add_to_carts.domain_id')
                      ->where('add_to_carts.promo_code',0)
                      ->where('add_to_carts.content_type','seller')
                      ->where('add_to_carts.domain_user_id',$user_id)
                      ->where('add_to_carts.is_billed',1)                      
                      ->sum('mst_domain_url.extra_cost');
       
        if(!empty($seller)){
            $seller = buyerDomainPriceCalculate($seller,$seller_extra);
        }
        $buyer = DB::table('add_to_carts')
                      ->join('mst_domain_url','mst_domain_url.domain_id','add_to_carts.domain_id')
                      ->where('add_to_carts.domain_user_id',$user_id)
                      ->where('add_to_carts.is_billed',1)
                      ->where('add_to_carts.promo_code',0)
                      ->where('add_to_carts.content_type','buyer')
                      ->sum('mst_domain_url.cost_price');
        
        if(!empty($buyer)){
            $buyer = buyerDomainWebsitePriceCalculate($buyer);
        }

        $earned = (float)$seller + (float)$buyer;

        /* earned */

        $domain_count = DB::table('mst_domain_url')
        ->join('assigned_websites','assigned_websites.mst_domain_url_id','mst_domain_url.domain_id')
        ->where('assigned_websites.user_id',$user_id )
        ->where('mst_domain_url.cost_price','>',0)->where('mst_domain_url.domain_approval_status',1)->where('mst_domain_url.trash','<',1)->count();

        $adv = false;
        $pub = false;
        if(Auth::user()->content_type == 'Advertiser;Publisher'){
            $adv = true;
            $pub = true;
        } else if(Auth::user()->content_type == 'Publisher'){
            $pub = true;
        } else if(Auth::user()->content_type == 'Advertiser'){
            $adv = true;
        } else {
            if(Auth::user()->switched == 'publisher'){
                $pub = true;
            } else if(Auth::user()->switched == 'advertiser'){ 
                $adv = true;
            }
        }

    ?>

    <script>

      window.intercomSettings = {
        app_id: "qn6bzp46",
        name: "{{ @Auth::user()->user_fname }}", // Full name
        email: "{{ @Auth::user()->email }}", // Email address
        created_at: "{{ Auth::user()->created_at }}", // Signup date as a Unix timestamp
        UserName: "{{ Auth::user()->user_code }}",
        PublisherWebsitePublished : "{{$domain_count}}",
        PublisherTotalMoneyEarned : "{{@$earned}}",
        AdvertiserTotalMoneySpent : "{{@$spent}}",
        Publisher : "{{$pub}}",
        Advertiser : "{{$adv}}"
      };
    </script>

    

    @else
        
        <script>

          window.intercomSettings = {
            app_id: "qn6bzp46"
          };
        </script>

    @endif


    <script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',w.intercomSettings);}else{var d=document;var i=function(){i.c(arguments);};i.q=[];i.c=function(args){i.q.push(args);};w.Intercom=i;var l=function(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/qn6bzp46';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);};if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>

</head>
    <?php 
        if(Auth::check()){
            $class = Auth::user()->switched . '-wrapper ';
        } else {
            $class = '';
        }
    ?>
    <body class="{{$class}}kt-page--loading-enabled kt-page--loading kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--left kt-aside--fixed kt-page--loading">
    @if(env('APP_ENV') == 'live')
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MG2W4QB"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
    @endif
        @include('layout.include.partials.loader-chat')

        <!-- end::Page Loader -->

        <!-- begin:: Page -->

        <!-- begin:: Header Mobile -->
        <div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
            <div class="kt-header-mobile__logo">
                @include('layout.include.partials.brand')
            </div>
            <div class="kt-header-mobile__toolbar">              
                
                <button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more-1"></i></button>
            </div>
        </div>

        <!-- end:: Header Mobile -->
        <div class="kt-grid kt-grid--hor kt-grid--root">
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
                <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

                    @include('layout.include.header')
                    @if(Auth::check())
                        @include('layout.include.aside')                    
                    @endif

                    @yield('content')

            <!-- end::Page Loader -->
                    @include('layout.include.footer')
            <!-- end:: Footer -->
                </div>
            </div>
        </div>

    <!-- end:: Page -->
 

        <!-- begin::Scrolltop -->
        <div id="kt_scrolltop" class="kt-scrolltop">
            <i class="fa fa-arrow-up"></i>
        </div>

        <!-- end::Scrolltop -->

        

        
        @include('layout.include.chatscripts')    


        @yield('customScript')

        <script type="text/javascript">
        // $(window).load(function() {
        //     $(".loader").fadeOut("slow");
        // });
    </script>
    </body>
    <!-- end::Body -->
</html>