<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

    <title>@yield('title')</title>
    
     
    <!-- begin::Head -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @yield('title-description')
    <!-- CSRF Token -->
    <meta name="rankz-verification" content="dmSMHt7jjgIOGHhK">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @section('head')
        @include('layout.include.head')
    @show

   

<meta name="description" content="Login to GuestPostEngine with your email ID and password and enjoy the services of GPE as an advertiser and publisher.">
<!--RoboAuditor popup js code start-->
<script  id="growthrobot" src="https://d3ikwiixxizqwk.cloudfront.net/assets/site/js/popup.js" data-content="5097"></script>
<!--RoboAuditor popup js code end--> 

    @if(env('APP_ENV') == 'live')
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-MG2W4QB');</script>
        <!-- End Google Tag Manager -->
    @endif
    <!-- end::Head -->
    <!-- <script src="{{URL::asset('public/manifest.json')}}"></script> -->
 @if(Auth::check())

    <?php

        // Advertiser
        /* spend */
        $user_id = Auth::user()->user_id;

        $seller = DB::table('add_to_carts')
                      ->join('mst_domain_url','mst_domain_url.domain_id','add_to_carts.domain_id')
                      ->where('add_to_carts.promo_code',0)
                      ->where('add_to_carts.content_type','seller')
                      ->where('add_to_carts.user_id',$user_id)
                      ->where('add_to_carts.is_billed',1)                      
                      ->sum('mst_domain_url.cost_price');
        $seller_extra = DB::table('add_to_carts')
                      ->join('mst_domain_url','mst_domain_url.domain_id','add_to_carts.domain_id')
                      ->where('add_to_carts.promo_code',0)
                      ->where('add_to_carts.content_type','seller')
                      ->where('add_to_carts.user_id',$user_id)
                      ->where('add_to_carts.is_billed',1)
                      ->sum('mst_domain_url.extra_cost');
       
        if(!empty($seller)){
            $seller = buyerDomainPriceCalculate($seller,$seller_extra);
        }
        $buyer = DB::table('add_to_carts')
                      ->join('mst_domain_url','mst_domain_url.domain_id','add_to_carts.domain_id')
                      ->where('add_to_carts.user_id',$user_id)
                      ->where('add_to_carts.is_billed',1)
                      ->where('add_to_carts.promo_code',0)
                      ->where('add_to_carts.content_type','buyer')
                      ->sum('mst_domain_url.cost_price');
        
        if(!empty($buyer)){
            $buyer = buyerDomainWebsitePriceCalculate($buyer);
        }

        $spent = (float)$seller + (float)$buyer;

        /* spend */

        /* earned */
    
        $seller = DB::table('add_to_carts')
                      ->join('mst_domain_url','mst_domain_url.domain_id','add_to_carts.domain_id')
                      ->where('add_to_carts.promo_code',0)
                      ->where('add_to_carts.content_type','seller')
                      ->where('add_to_carts.domain_user_id',$user_id)
                      ->where('add_to_carts.is_billed',1)                      
                      ->sum('mst_domain_url.cost_price');
        $seller_extra = DB::table('add_to_carts')
                      ->join('mst_domain_url','mst_domain_url.domain_id','add_to_carts.domain_id')
                      ->where('add_to_carts.promo_code',0)
                      ->where('add_to_carts.content_type','seller')
                      ->where('add_to_carts.domain_user_id',$user_id)
                      ->where('add_to_carts.is_billed',1)                      
                      ->sum('mst_domain_url.extra_cost');
       
        if(!empty($seller)){
            $seller = buyerDomainPriceCalculate($seller,$seller_extra);
        }
        $buyer = DB::table('add_to_carts')
                      ->join('mst_domain_url','mst_domain_url.domain_id','add_to_carts.domain_id')
                      ->where('add_to_carts.domain_user_id',$user_id)
                      ->where('add_to_carts.is_billed',1)
                      ->where('add_to_carts.promo_code',0)
                      ->where('add_to_carts.content_type','buyer')
                      ->sum('mst_domain_url.cost_price');
        
        if(!empty($buyer)){
            $buyer = buyerDomainWebsitePriceCalculate($buyer);
        }

        $earned = (float)$seller + (float)$buyer;

        /* earned */

        $domain_count = DB::table('mst_domain_url')
        ->join('assigned_websites','assigned_websites.mst_domain_url_id','mst_domain_url.domain_id')
        ->where('assigned_websites.user_id',$user_id )
        ->where('mst_domain_url.cost_price','>',0)->where('mst_domain_url.domain_approval_status',1)->where('mst_domain_url.trash','<',1)->count();

        $adv = false;
        $pub = false;
        if(Auth::user()->content_type == 'Advertiser;Publisher'){
            $adv = true;
            $pub = true;
        } else if(Auth::user()->content_type == 'Publisher'){
            $pub = true;
        } else if(Auth::user()->content_type == 'Advertiser'){
            $adv = true;
        } else {
            if(Auth::user()->switched == 'publisher'){
                $pub = true;
            } else if(Auth::user()->switched == 'advertiser'){ 
                $adv = true;
            }
        }

    ?>

    <script>

      window.intercomSettings = {
        app_id: "qn6bzp46",
        name: "{{ @Auth::user()->user_fname }}", // Full name
        email: "{{ @Auth::user()->email }}", // Email address
        created_at: "{{ Auth::user()->created_at }}", // Signup date as a Unix timestamp
        UserName: "{{ Auth::user()->user_code }}",
        PublisherWebsitePublished : "{{$domain_count}}",
        PublisherTotalMoneyEarned : "{{@$earned}}",
        AdvertiserTotalMoneySpent : "{{@$spent}}",
        Publisher : "{{$pub}}",
        Advertiser : "{{$adv}}"
      };
    </script>

    

    @else
        
        <script>

          window.intercomSettings = {
            app_id: "qn6bzp46"
          };
        </script>

    @endif


    <script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',w.intercomSettings);}else{var d=document;var i=function(){i.c(arguments);};i.q=[];i.c=function(args){i.q.push(args);};w.Intercom=i;var l=function(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/qn6bzp46';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);};if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
</head>
    <body class="kt-page--loading-enabled kt-page--loading kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--left kt-aside--fixed kt-page--loading">
    @if(env('APP_ENV') == 'live')
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MG2W4QB"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
    @endif
    <!-- begin::Page loader -->
        @include('layout.include.partials.loader')
    <!-- end::Page Loader -->


        @yield('content')
        

    <!-- end:: Page -->
 

    @section('scripts')
        @include('layout.include.scripts')
    @show   

    <!--begin::Page Scripts(used by this page) -->
        <script src="{{ asset('') . config('app.public_url') . '/assets/js/pages/custom/login/login-general.js'  }}" type="text/javascript"></script> 
    <!--end::Page Scripts -->
    <!-- <script type="text/javascript" src="http://www.google.com/recaptcha/api/js/recaptcha_ajax.js"></script> -->
        <script type="text/javascript">
        
        $('#kt_logins').click(function(e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');           

            form.validate({
                rules: {
                    user_email: {
                        required: true,
                        email: true
                    },
                    password_login: {
                        required: true
                    }
                },
                messages :{
                    user_email: {
                        required: 'Please enter a valid email address.',
                        email: 'Please enter a valid email address.',
                    },
                    password_login:{ 
                        required:  'Please enter a valid password.'
                    }
                },
                onkeyup: true,
                onfocusout: true,
                onclick: true
            });

            if (!form.valid()) {
                return;
            }

            //btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

            form.submit();
            
        });
        $('#kt_register').click(function(e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');


            // Displays the recpatcha form in the element with id "captcha"
                // Recaptcha.create("6LdQRbgUAAAAACb4r31Veez9xCKdTHi86sA2K6Ni", "captcha", {
                // theme: "clean",
                // callback: Recaptcha.focus_response_field
                // });

                // Add reCaptcha validator to form validation
                // jQuery.validator.addMethod("checkCaptcha", (function() {
                // var isCaptchaValid;
                // isCaptchaValid = false;
                // $.ajax({
                //     url: "libs/checkCaptcha.php",
                //     type: "POST",
                //     async: false,
                //     data: {
                //     recaptcha_challenge_field: Recaptcha.get_challenge(),
                //     recaptcha_response_field: Recaptcha.get_response()
                //     },
                //     success: function(resp) {
                //     if (resp === "true") {
                //         isCaptchaValid = true;
                //     } else {
                //         Recaptcha.reload();
                //     }
                //     }
                // });
                // return isCaptchaValid;
                // }), "");

            form.validate({
                rules: {
                    user_code: {
                        required: true,
                        pattern:'^[a-zA-Z0-9._-]{3,15}$',
                        remote: {
                            url: "{{url('varifyusercode')}}",
                            type: "GET",
                            data: {
                                action: function () {
                                    return "1";
                                },
                            }
                        },
                       // letterDigitsOnly: '#user_code' 
                    },
                    user_fname: {
                        required: true,
                        
                    },
                    user_lname: {
                        required: true,
                       
                    },
                    email: {
                        required: true,
                        email: true,                        
                        remote: {
                            url: "{{url('varifyemail')}}",
                            type: "GET",
                            data: {
                                action: function () {
                                    return "1";
                                },
                            }
                        }  
                        
                    },
                    '#password': {
                        required: true,
                    },
                    password_confirm: {
                        required: true,
                        equalTo: "#password"
                    },
                    agree: {
                        required: true
                    },
                    // recaptcha_response_field: {
                    // required: true,
                    // checkCaptcha: true
                    // }
                },
                messages: {
                    email: {
                        required: "Please enter a valid email address.",
                        remote: "Email has been already registered."
                    },
                    user_code: {
                        required: "Please enter a valid username.",
                        remote: "Please enter a valid username.",
                        pattern: "Please choose characters (a to z) / special characters ( userscore(_) / hyphin(-) / dot(.) ) only."
                    },
                    user_fname: {
                        required: "Please enter a valid first name.",
                       
                    },
                    user_lname: {
                        required: 'Please enter a valid last name.',
                       
                    },
                    password: {
                        required: 'Please enter a valid password.',
                    },
                    password_confirm: {
                        required: 'Please enter a valid confirm password.',
                        equalTo: 'Confirm password should be same as password.'
                    },
                    agree: {
                        required: 'Please accept the terms & conditions.',
                    },
                    // recaptcha_response_field: {
                    // checkCaptcha: "Your Captcha response was incorrect. Please try again."
                    // }
                },
                onkeyup: true,
                onfocusout: true,
                onclick: true
            });

            if (!form.valid()) {
                return;
            }

            form.submit();
        });

        // jQuery.validator.addMethod("letterDigitsOnly", function(value, element) {
        //   return this.optional(element) || /^[a-zA-Z0-9]*$/i.test(value);
        // }, "Letters and digits only please.");


        $('#kt_login_forgot_pass').click(function(e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                rules: {
                    email: {
                        required: true,
                        email: true,
                        remote: {
                            url: "{{url('varifyemailForgot')}}",
                            type: "GET",
                            data: {
                                action: function () {
                                    return "1";
                                },
                            }
                        }
                    }
                },
                messages: {
                    email:{
                        required: 'Please enter a valid email address.',
                        remote: "Please enter a valid/existing email address."
                    }
                },
                onkeyup: true,
                onfocusout: true,
                onclick: true
            });

            if (!form.valid()) {
                return;
            }

            //btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            form.submit();
        });
        $('#kt_logins_reset_custom').click(function(e) {
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                rules: {
                    email: {
                        required: true,
                    },
                    '#password': {
                        required: true,
                    },
                    password_confirmation: {
                        required: true,
                        equalTo: "#password"
                    },
                },
                messages: {
                    email:{
                        required: 'Please enter a valid email address.',
                        remote: "Please enter a valid/existing email address."
                    },
                    password: {
                        required: 'Please enter a valid password.',
                    },
                    password_confirmation: {
                        required: 'Please enter a valid confirm password.',
                        equalTo: 'Please enter a valid password.'
                    }
                }
            });

            if (!form.valid()) {
                return;
            }

            //btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
            form.submit();
        });

    </script>
    </body>
    <!-- end::Body -->
</html>