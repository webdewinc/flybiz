@if(Auth::check())
<!-- begin:: Aside -->
<button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>
<div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop" id="kt_aside">
	<!-- begin:: Aside Menu -->
	<div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
		<div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1">
			<ul class="kt-menu__nav ">
				@if(Auth::user()->switched == 'publisher')
					<li class="kt-menu__item " aria-haspopup="true"><a target="_blank" href="{{url('publisher/dashboard')}}" class="kt-menu__link "><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-mr-10">
						<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
							<rect x="0" y="0" width="24" height="24"></rect>
							<path d="M3,4 L20,4 C20.5522847,4 21,4.44771525 21,5 L21,7 C21,7.55228475 20.5522847,8 20,8 L3,8 C2.44771525,8 2,7.55228475 2,7 L2,5 C2,4.44771525 2.44771525,4 3,4 Z M10,10 L20,10 C20.5522847,10 21,10.4477153 21,11 L21,13 C21,13.5522847 20.5522847,14 20,14 L10,14 C9.44771525,14 9,13.5522847 9,13 L9,11 C9,10.4477153 9.44771525,10 10,10 Z M10,16 L20,16 C20.5522847,16 21,16.4477153 21,17 L21,19 C21,19.5522847 20.5522847,20 20,20 L10,20 C9.44771525,20 9,19.5522847 9,19 L9,17 C9,16.4477153 9.44771525,16 10,16 Z" fill="#000000"></path>
							<rect fill="#000000" opacity="0.3" x="2" y="10" width="5" height="10" rx="1"></rect>
						</g>
					</svg><span class="kt-menu__link-text">Dashboard</span></a></li>		
					<li class="kt-menu__item " aria-haspopup="true"><a href="{{url('publisher/orders')}}" class="kt-menu__link "><i class="kt-menu__link-icon la la-plus-square"></i><span class="kt-menu__link-text">Order</span></a></li>
					<li class="kt-menu__item " aria-haspopup="true"><a href="{{url('publisher/billings')}}" class="kt-menu__link "><i class="kt-menu__link-icon la la-file-text-o"></i><span class="kt-menu__link-text">Billing</span></a></li>
					<li class="kt-menu__item " aria-haspopup="true"><a href="{{url('publisher/reports')}}" class="kt-menu__link "><i class="kt-menu__link-icon la la-th-list"></i><span class="kt-menu__link-text">Report</span></a></li>
				@elseif(Auth::user()->switched == 'advertiser')
					<li class="kt-menu__item " aria-haspopup="true"><a target="_blank" href="{{url('advertiser/dashboard')}}" class="kt-menu__link "><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-mr-10">
					<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
						<rect x="0" y="0" width="24" height="24"></rect>
						<path d="M3,4 L20,4 C20.5522847,4 21,4.44771525 21,5 L21,7 C21,7.55228475 20.5522847,8 20,8 L3,8 C2.44771525,8 2,7.55228475 2,7 L2,5 C2,4.44771525 2.44771525,4 3,4 Z M10,10 L20,10 C20.5522847,10 21,10.4477153 21,11 L21,13 C21,13.5522847 20.5522847,14 20,14 L10,14 C9.44771525,14 9,13.5522847 9,13 L9,11 C9,10.4477153 9.44771525,10 10,10 Z M10,16 L20,16 C20.5522847,16 21,16.4477153 21,17 L21,19 C21,19.5522847 20.5522847,20 20,20 L10,20 C9.44771525,20 9,19.5522847 9,19 L9,17 C9,16.4477153 9.44771525,16 10,16 Z" fill="#000000"></path>
						<rect fill="#000000" opacity="0.3" x="2" y="10" width="5" height="10" rx="1"></rect>
					</g>
					</svg><span class="kt-menu__link-text">Dashboard</span></a></li>		
					<li class="kt-menu__item " aria-haspopup="true"><a href="{{url('advertiser/purchases')}}" class="kt-menu__link "><i class="kt-menu__link-icon la la-plus-square"></i><span class="kt-menu__link-text">Purchases</span></a></li>
					<li class="kt-menu__item " aria-haspopup="true"><a href="{{url('advertiser/billings')}}" class="kt-menu__link "><i class="kt-menu__link-icon la la-file-text-o"></i><span class="kt-menu__link-text">Billing</span></a></li>
					<li class="kt-menu__item " aria-haspopup="true"><a href="{{url('advertiser/reports')}}" class="kt-menu__link "><i class="kt-menu__link-icon la la-th-list"></i><span class="kt-menu__link-text">Report</span></a></li>
				@endif
			</ul>
		</div>
	</div>
	<!-- end:: Aside Menu -->
</div>

<!-- end:: Aside -->
@endif