    <!-- begin::Global Config(global config for global JS sciprts) -->
    <script>
        var KTAppOptions = {
            "colors": {
                "state": {
                    "brand": "#009bff",
                    "light": "#d5efff",
                    "dark": "#282a3c",
                    "primary": "#009bff",
                    "success": "#34bfa3",
                    "info": "#36a3f7",
                    "warning": "#ffb822",
                    "danger": "#F2545B",
                    "facebook": "#3b5998",
                    "youtube": "#c4302b",
                    "whatsapp": "#075e54",
                    "twitter": "#00acee",
                    "linkedin": "#0e76a8",
                    "pinterest": "#c8232c"
                },
                "base": {
                    "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
                    "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
                }
            }
        };
    </script>

    <!-- end::Global Config -->

    <!--begin::Global Theme Bundle(used by all pages) -->   

		<script src="{{ asset('') . config('app.public_url') . '/assets/plugins/global/plugins.bundle.js' }}" type="text/javascript"></script>
		<script src="{{ asset('') . config('app.public_url') . '/assets/js/scripts.bundle.js' }}" type="text/javascript"></script>

	<!--end::Global Theme Bundle(used by all pages) -->   



    <script>
        $(document).on('click','.kt-ribbon__target', function() {
            var exist_class = $(this).attr('class');
            if(exist_class == 'kt-ribbon__target active-wishlist'){
                $(this).removeClass('active-wishlist');
            } else {
                $(this).addClass('active-wishlist'); 
            }
            var domain_id  = $(this).find('#domain_id').val();
            var domain_url = $(this).find('#domain_url').val(); 
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                 url :"{{ url('/add_to_wishlist') }}",
                type : 'POST',
                data : {
                    'domain_id' :domain_id,
                },
                success:function(response){
                    $('#cartlist_notify_full').html(response.cart);
                    $('#wishlist_notify_full').html(response.wish);
                },
                error:function(errorThrown){
                    console.log(errorThrown);
                }
            });
        });

        $(document).on('click','.delete-wish', function(){     
            var domain_id  = $(this).attr('rel');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                 url :"{{ url('/delete_wishlist') }}",
                type : 'POST',
                data : {
                    'domain_id' :domain_id,
                },
                success:function(response){
                    $('#cartlist_notify_full').html(response.cart);
                    $('#wishlist_notify_full').html(response.wish);
                },
                error:function(errorThrown){
                    console.log(errorThrown);
                }
            });

        });

        $(document).on('click','.add_to_cart_top', function(){     
            var domain_id  = $(this).attr('rel');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                 url :"{{ url('/add_to_cartlist') }}",
                type : 'POST',
                data : {
                    'domain_id' :domain_id,
                },
                success:function(response){
                    $('#cartlist_notify_full').html(response.cart);
                    $('#wishlist_notify_full').html(response.wish);
                },
                error:function(errorThrown){
                    console.log(errorThrown);
                }
            });
        });


        $(document).on('click','.add_to_cart_delete', function(){     
            var domain_id  = $(this).attr('rel');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                 url :"{{ url('/delete_cartlist') }}",
                type : 'POST',
                data : {
                    'domain_id' :domain_id,
                },
                success:function(response){
                    $('#cartlist_notify_full').html(response.cart);
                    $('#wishlist_notify_full').html(response.wish);
                },
                error:function(errorThrown){
                    console.log(errorThrown);
                }
            });
        });

        $(document).on('click','.addWishMoveIntoCart', function(){     
            
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url :"{{ url('/addWishMoveIntoCart') }}",
                type : 'POST',
                data : {
                    'domain_id' :'s',
                },
                success:function(response){
                    $('#cartlist_notify_full').html(response.cart);
                    $('#wishlist_notify_full').html(response.wish);
                },
                error:function(errorThrown){
                    console.log(errorThrown);
                }
            });
        });
    </script>