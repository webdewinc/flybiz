<form method="get" class="kt-quick-search__form kt-margin-0" onsubmit="return validateForm()" name="searchForm" action="{{url('search')}}">
    <div class="input-group">
        <div class="input-group-prepend"><span class="input-group-text"><i class="flaticon2-search-1"></i></span></div>
        <input type="text" class="form-control search_keyword kt-quick-search__input" name="q" id="search_keyword_search" placeholder="Search from the List of {{total_website_count()}} Blogs that accept Guest Posts" autocomplete="off" value="{{@$_GET['q']}}" rel="search"  required/>
        <div class="input-group-append"><span class="input-group-text"><i class="la la-close kt-quick-search__close"></i></span></div>
    </div>
</form>