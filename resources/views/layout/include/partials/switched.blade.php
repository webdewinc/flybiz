<a href="{{URL::asset('/switched')}}" class="btn btn-primary btn-sm btn-bold">
	<div class="kt-notification__item-details">
		<div class="kt-notification__item-title kt-font-bold">
			<?php
				$switched = Auth::user()->switched;
				if($switched == 'advertiser'){
		            $switched = 'Publisher';
		        } else if ($switched == 'publisher') {
		            $switched = 'Advertiser';
		        } else {
		            $switched = 'Advertiser';
		        }
			?>
			Swiched to {{$switched}}
		</div>
	</div>
</a>