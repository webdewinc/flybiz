<h1 class="">World's 1st Guest Posting Search Engine</h1>
<div class="kt-quick-search kt-quick-search--dropdown kt-quick-search--result-compact" >
    <form method="get" onsubmit="return validateForm()" name="searchForm"  id="searchForm" action="{{URL::asset('search')}}">
        <div>
            <div class="input-group kt-quick-search__form">
                <div class="input-group-prepend"><span class="input-group-text"><i class="flaticon2-search-1"></i></span></div>
                <input type="text" class="form-control kt-quick-search__input" name="q" id="search_keyword" placeholder="Search from the List of {{total_website_count()}} Blogs that accept Guest Posts" autocomplete="off"  value="{{@$_GET['q']}}" />
                <!-- <div class="input-group-append"><span class="input-group-text"><i class="la la-close kt-quick-search__close"></i></span></div> -->              
            </div>
            <span id="q_validate" class="search-error"></span>
            <div class="input-group">
                <div id="display"></div>
            </div>
        </div>
    </form>
    <div class="kt-quick-search__wrapper kt-scroll ps" data-scroll="true" data-height="325" data-mobile-height="200" style="height: 325px; overflow: hidden;">
        <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
            <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
        </div>
        <div class="ps__rail-y" style="top: 0px; right: 0px;">
            <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div>
        </div>
    </div>
</div>

<a href="{{url('/#how-it-works')}}" class="btn btn-outline-hover scroll-btn d-flex"><i class="la la-youtube-play"></i> How it Works</a>
<script>
    $(document).ready(function(){
        $('#searchForm').validate({
            rules : {
                    q : {
                        required : true,
                    },
            },
            messages : {
                q : {
                    required : "Please enter a valid Search Value.",   
                },
            },
             errorPlacement: function(error, element){
                var name = $(element).attr('name');
                error.appendTo($('#' + name + "_validate"));
             }, 

        });
    })
</script>

