<div class="kt-header__brand   kt-grid__item" id="kt_header_brand">		
	<a class="kt-header__brand-logo" href="{{URL::asset('/')}}">
		<img alt="Logo" src="{{ asset('') . config('app.public_url') . '/assets/media/logos/fly.biz_logo.svg'}}" title="GuestPostEngine">
	</a>
</div>