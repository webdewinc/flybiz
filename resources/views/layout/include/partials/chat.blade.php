@if(Auth::user()->user_type != 'admin')

<?php

	$chatChannels = \App\Chatroom::where('sender_id',Auth::user()->user_id)->orWhere('receiver_id',Auth::user()->user_id)->get();
	$ids = 0;
	$chat_count = 0;
	foreach($chatChannels as $key => $val) :

        $id = 0;
        $date = '';
        $ids_explode = explode("_",$val->chatroom);
        $otherUser = '';
        if($ids_explode[0] == Auth::user()->user_id):
            $id = $ids_explode[0];
            $otherUser = $ids_explode[1];
            $image = \App\User::where('user_id',$otherUser)->value('image');
            $name = \App\User::where('user_id',$otherUser)->value('user_fname');
			$lname = \App\User::where('user_id',$otherUser)->value('user_lname');
			$count = \App\Chat::where('chatroom_id',$val->id)->where('is_viewed',0)->where('sender_id','=',$otherUser)->count();
        else :
            $id = $ids_explode[1];
            $otherUser = $ids_explode[0];
            $image = \App\User::where('user_id',$otherUser)->value('image');
            $name = \App\User::where('user_id',$otherUser)->value('user_fname');
			$lname = \App\User::where('user_id',$otherUser)->value('user_lname');
			$count = \App\Chat::where('chatroom_id',$val->id)->where('is_viewed',0)->where('sender_id','=',$otherUser)->count();
        endif;
        
		$chat_count += $count;
		$ids = 1;		
	endforeach;

?>
@if(!empty($ids))
<div class="kt-header__topbar-wrapper position-relative">
	<?php
	if($chat_count > 0 ):
	?>
	<span class="kt-badge kt-badge--brand" id="">{{$chat_count}}</span>
	<?php endif; ?>
	<a href="{{url('chat')}}" target="_blank" class="kt-header__topbar-icon" title="Chat">

		<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
			<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
				<rect x="0" y="0" width="24" height="24"></rect>
				<path d="M16,15.6315789 L16,12 C16,10.3431458 14.6568542,9 13,9 L6.16183229,9 L6.16183229,5.52631579 C6.16183229,4.13107011 7.29290239,3 8.68814808,3 L20.4776218,3 C21.8728674,3 23.0039375,4.13107011 23.0039375,5.52631579 L23.0039375,13.1052632 L23.0206157,17.786793 C23.0215995,18.0629336 22.7985408,18.2875874 22.5224001,18.2885711 C22.3891754,18.2890457 22.2612702,18.2363324 22.1670655,18.1421277 L19.6565168,15.6315789 L16,15.6315789 Z" fill="#000000"></path>
				<path d="M1.98505595,18 L1.98505595,13 C1.98505595,11.8954305 2.88048645,11 3.98505595,11 L11.9850559,11 C13.0896254,11 13.9850559,11.8954305 13.9850559,13 L13.9850559,18 C13.9850559,19.1045695 13.0896254,20 11.9850559,20 L4.10078614,20 L2.85693427,21.1905292 C2.65744295,21.3814685 2.34093638,21.3745358 2.14999706,21.1750444 C2.06092565,21.0819836 2.01120804,20.958136 2.01120804,20.8293182 L2.01120804,18.32426 C1.99400175,18.2187196 1.98505595,18.1104045 1.98505595,18 Z M6.5,14 C6.22385763,14 6,14.2238576 6,14.5 C6,14.7761424 6.22385763,15 6.5,15 L11.5,15 C11.7761424,15 12,14.7761424 12,14.5 C12,14.2238576 11.7761424,14 11.5,14 L6.5,14 Z M9.5,16 C9.22385763,16 9,16.2238576 9,16.5 C9,16.7761424 9.22385763,17 9.5,17 L11.5,17 C11.7761424,17 12,16.7761424 12,16.5 C12,16.2238576 11.7761424,16 11.5,16 L9.5,16 Z" fill="#000000" opacity="0.3"></path>
			</g>
		</svg>
	</a>
</div>
@endif

@else
<div class="kt-header__topbar-wrapper position-relative">
	
	
	<a href="{{url('admin/chats')}}" target="_blank" class="kt-header__topbar-icon" title="Chat">

		<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
			<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
				<rect x="0" y="0" width="24" height="24"></rect>
				<path d="M16,15.6315789 L16,12 C16,10.3431458 14.6568542,9 13,9 L6.16183229,9 L6.16183229,5.52631579 C6.16183229,4.13107011 7.29290239,3 8.68814808,3 L20.4776218,3 C21.8728674,3 23.0039375,4.13107011 23.0039375,5.52631579 L23.0039375,13.1052632 L23.0206157,17.786793 C23.0215995,18.0629336 22.7985408,18.2875874 22.5224001,18.2885711 C22.3891754,18.2890457 22.2612702,18.2363324 22.1670655,18.1421277 L19.6565168,15.6315789 L16,15.6315789 Z" fill="#000000"></path>
				<path d="M1.98505595,18 L1.98505595,13 C1.98505595,11.8954305 2.88048645,11 3.98505595,11 L11.9850559,11 C13.0896254,11 13.9850559,11.8954305 13.9850559,13 L13.9850559,18 C13.9850559,19.1045695 13.0896254,20 11.9850559,20 L4.10078614,20 L2.85693427,21.1905292 C2.65744295,21.3814685 2.34093638,21.3745358 2.14999706,21.1750444 C2.06092565,21.0819836 2.01120804,20.958136 2.01120804,20.8293182 L2.01120804,18.32426 C1.99400175,18.2187196 1.98505595,18.1104045 1.98505595,18 Z M6.5,14 C6.22385763,14 6,14.2238576 6,14.5 C6,14.7761424 6.22385763,15 6.5,15 L11.5,15 C11.7761424,15 12,14.7761424 12,14.5 C12,14.2238576 11.7761424,14 11.5,14 L6.5,14 Z M9.5,16 C9.22385763,16 9,16.2238576 9,16.5 C9,16.7761424 9.22385763,17 9.5,17 L11.5,17 C11.7761424,17 12,16.7761424 12,16.5 C12,16.2238576 11.7761424,16 11.5,16 L9.5,16 Z" fill="#000000" opacity="0.3"></path>
			</g>
		</svg>
	</a>
</div>

@endif
