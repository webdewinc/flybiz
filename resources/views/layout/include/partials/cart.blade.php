<span id="cartlist_notify_full">
	{{-- DB::table('add_to_carts')->whereIn('domain_id',$get)->where('is_billed',0)->where('user_id',Auth::user()->user_id)->delete(); --}}
{{--
@php
Cache::forget( 'cartlist' );
Cache::forget( 'wishlist' );
die;
@endphp--}}
@if(Auth::check())

	@if(Auth::user()->switched == 'advertiser')


	@if( Cache::has( 'cartlist' ) ) 
		@php

	            $get = Cache::get( 'cartlist' );
	            $get = unserialize($get);


	            foreach( $get as $key => $value){
	          		$record_exists   = DB::table('add_to_carts')
                                   ->where('domain_id',$value)
                                   ->where('is_billed',0)
                                   ->where('user_id', Auth::user()->user_id)->value('id');
                    if(empty($record_exists)) {
                        $domain_url = DB::table('mst_domain_url')->where('domain_id',$value)->value('domain_url');
                        $domain_user_id = DB::table('mst_domain_url')->where('domain_id',$value)->value('domain_fk_user_id');
                        $data = array('user_id'=> Auth::user()->user_id, 'domain_id'=> $value,'fk_domain_url'=>$domain_url,'domain_user_id' => $domain_user_id ,'content_type' => 'buyer','is_billed' => 0);
                        DB::table('add_to_carts')->insert($data);
                    }
	            }
	            
	    @endphp
	
	@endif
{{--
	            Cache::forget('cartlist');
	            $cart   = DB::table('add_to_carts')
                                   ->where('is_billed',0)
                                   ->where('user_id',Auth::user()->user_id)->pluck('id')->toArray();
                $cart = serialize($cart);
	            Cache::forever('cartlist',$cart);
	            --}}
	<?php
	    $cartList = \DB::table('add_to_carts')
	    //->leftjoin('mst_user','mst_user.user_id','add_to_carts.user_id')
	    ->join('mst_domain_url','mst_domain_url.domain_id','add_to_carts.domain_id')
	    //->leftjoin('mst_da_pa','mst_da_pa.dp_fk_domain','mst_domain_url.domain_id')
	    //->leftjoin('mst_seo_data','mst_seo_data.sd_fk_domain','mst_domain_url.domain_id')
	    ->where('add_to_carts.user_id', Auth::user()->user_id)
	    ->where('add_to_carts.is_billed', 0)
	    ->get()->toArray();

	    $count = count($cartList);
	    $class = '';
	    if($count > 0){
	    	$class = 'dropdown';
	    }
	?>

	<div class="kt-header__topbar-wrapper" data-toggle="{{$class}}" data-offset="10px,0px" aria-expanded="true">
		@if($count > 0)
		<span class="kt-badge kt-badge--brand" id="">{{$count}}</span>
		@endif
			<span class="kt-header__topbar-icon" title="Cart"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
					<rect x="0" y="0" width="24" height="24"></rect>
					<path d="M18.1446364,11.84388 L17.4471627,16.0287218 C17.4463569,16.0335568 17.4455155,16.0383857 17.4446387,16.0432083 C17.345843,16.5865846 16.8252597,16.9469884 16.2818833,16.8481927 L4.91303792,14.7811299 C4.53842737,14.7130189 4.23500006,14.4380834 4.13039941,14.0719812 L2.30560137,7.68518803 C2.28007524,7.59584656 2.26712532,7.50338343 2.26712532,7.4104669 C2.26712532,6.85818215 2.71484057,6.4104669 3.26712532,6.4104669 L16.9929851,6.4104669 L17.606173,3.78251876 C17.7307772,3.24850086 18.2068633,2.87071314 18.7552257,2.87071314 L20.8200821,2.87071314 C21.4717328,2.87071314 22,3.39898039 22,4.05063106 C22,4.70228173 21.4717328,5.23054898 20.8200821,5.23054898 L19.6915238,5.23054898 L18.1446364,11.84388 Z" fill="#000000" opacity="0.3"></path>
					<path d="M6.5,21 C5.67157288,21 5,20.3284271 5,19.5 C5,18.6715729 5.67157288,18 6.5,18 C7.32842712,18 8,18.6715729 8,19.5 C8,20.3284271 7.32842712,21 6.5,21 Z M15.5,21 C14.6715729,21 14,20.3284271 14,19.5 C14,18.6715729 14.6715729,18 15.5,18 C16.3284271,18 17,18.6715729 17,19.5 C17,20.3284271 16.3284271,21 15.5,21 Z" fill="#000000"></path>

				</g>
			</svg></span>
			
		</div>
		<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl">
		<!-- begin:: Mycart -->
			<div class="kt-mycart">
				<div class="kt-mycart__head kt-head">
					<div class="kt-mycart__info">
						<span class="kt-mycart__icon"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
						<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
							<rect x="0" y="0" width="24" height="24"></rect>
							<path d="M18.1446364,11.84388 L17.4471627,16.0287218 C17.4463569,16.0335568 17.4455155,16.0383857 17.4446387,16.0432083 C17.345843,16.5865846 16.8252597,16.9469884 16.2818833,16.8481927 L4.91303792,14.7811299 C4.53842737,14.7130189 4.23500006,14.4380834 4.13039941,14.0719812 L2.30560137,7.68518803 C2.28007524,7.59584656 2.26712532,7.50338343 2.26712532,7.4104669 C2.26712532,6.85818215 2.71484057,6.4104669 3.26712532,6.4104669 L16.9929851,6.4104669 L17.606173,3.78251876 C17.7307772,3.24850086 18.2068633,2.87071314 18.7552257,2.87071314 L20.8200821,2.87071314 C21.4717328,2.87071314 22,3.39898039 22,4.05063106 C22,4.70228173 21.4717328,5.23054898 20.8200821,5.23054898 L19.6915238,5.23054898 L18.1446364,11.84388 Z" fill="#000000" opacity="0.3"></path>
							<path d="M6.5,21 C5.67157288,21 5,20.3284271 5,19.5 C5,18.6715729 5.67157288,18 6.5,18 C7.32842712,18 8,18.6715729 8,19.5 C8,20.3284271 7.32842712,21 6.5,21 Z M15.5,21 C14.6715729,21 14,20.3284271 14,19.5 C14,18.6715729 14.6715729,18 15.5,18 C16.3284271,18 17,18.6715729 17,19.5 C17,20.3284271 16.3284271,21 15.5,21 Z" fill="#000000"></path>

						</g>
					</svg></span>
						<h3 class="kt-mycart__title">My Cart</h3>
					</div>
					<div class="kt-mycart__button">
						<?php
							$text = 'Item';
							if($count > 1){
								$text = 'Items';
							}
						?>
						<a href="{{url('advertiser/cart')}}" class="btn btn-primary btn-sm" style=" ">{{$count}} {{$text}}</a>
					</div>
				</div>
				<div class="kt-mycart__body kt-scroll" data-scroll="true" data-mobile-height="100">
					@php
	                    $total_price = 0;
	                @endphp
					@foreach($cartList as $key => $val)
					
					@php
						$cost = 0;
					@endphp

					@if($val->content_type == 'buyer')
						@php
						$cost = buyerDomainWebsitePriceCalculate($val->cost_price);
						@endphp
					@elseif($val->content_type == 'seller')
						@php
							$extra_cost = 0;
						@endphp
						@if(!empty($val->extra_cost))
							@php
								$extra_cost = $val->extra_cost;
							@endphp
						@endif
						@php
							$cost = buyerDomainPriceCalculate($val->cost_price, $val->extra_cost);
						@endphp
					@endif

					@php
						$total_price += $cost;
					@endphp
					<div class="kt-mycart__item">
						<div class="kt-mycart__container">
							<div class="kt-mycart__info w-100">
								<div class="kt-space-between flex-wrap">
									<div>
										<!-- <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon favicon-svg">
											<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
												<rect x="0" y="0" width="24" height="24"></rect>
												<circle fill="#000000" opacity="0.3" cx="12" cy="12" r="9"></circle>
												<path d="M11.7357634,20.9961946 C6.88740052,20.8563914 3,16.8821712 3,12 C3,11.9168367 3.00112797,11.8339369 3.00336944,11.751315 C3.66233009,11.8143341 4.85636818,11.9573854 4.91262842,12.4204038 C4.9904938,13.0609191 4.91262842,13.8615942 5.45804656,14.101772 C6.00346469,14.3419498 6.15931561,13.1409372 6.6267482,13.4612567 C7.09418079,13.7815761 8.34086797,14.0899175 8.34086797,14.6562185 C8.34086797,15.222396 8.10715168,16.1034596 8.34086797,16.2636193 C8.57458427,16.423779 9.5089688,17.54465 9.50920913,17.7048097 C9.50956962,17.8649694 9.83857487,18.6793513 9.74040201,18.9906563 C9.65905192,19.2487394 9.24857641,20.0501554 8.85059781,20.4145589 C9.75315358,20.7620621 10.7235846,20.9657742 11.7357634,20.9960544 L11.7357634,20.9961946 Z M8.28272988,3.80112099 C9.4158415,3.28656421 10.6744554,3 12,3 C15.5114513,3 18.5532143,5.01097452 20.0364482,7.94408274 C20.069657,8.72412177 20.0638332,9.39135321 20.2361262,9.6327358 C21.1131932,10.8600506 18.0995147,11.7043158 18.5573343,13.5605384 C18.7589671,14.3794892 16.5527814,14.1196773 16.0139722,14.886394 C15.4748026,15.6527403 14.1574598,15.137809 13.8520064,14.9904917 C13.546553,14.8431744 12.3766497,15.3341497 12.4789081,14.4995164 C12.5805657,13.664636 13.2922889,13.6156126 14.0555619,13.2719546 C14.8184743,12.928667 15.9189236,11.7871741 15.3781918,11.6380045 C12.8323064,10.9362407 11.963771,8.47852395 11.963771,8.47852395 C11.8110443,8.44901109 11.8493762,6.74109366 11.1883616,6.69207022 C10.5267462,6.64279981 10.170464,6.88841096 9.20435656,6.69207022 C8.23764828,6.49572949 8.44144409,5.85743687 8.2887174,4.48255778 C8.25453994,4.17415686 8.25619136,3.95717082 8.28272988,3.80112099 Z M20.9991771,11.8770357 C20.9997251,11.9179585 21,11.9589471 21,12 C21,16.9406923 17.0188468,20.9515364 12.0895088,20.9995641 C16.970233,20.9503326 20.9337111,16.888438 20.9991771,11.8770357 Z" fill="#000000" opacity="0.3"></path>
											</g>
										</svg> -->
										<img width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon favicon-svg kt-mr-5" src="<?php echo 'https://www.google.com/s2/favicons?domain='. $val->domain_url; ?>" />
										<a href="{{url('/search/website/').'/'.$val->domain_url}}" class="kt-mycart__title">
											{{$val->domain_url}}
										</a>
									</div>
									<div class="d-flex">
										<div class="kt-mycart__action">
											<span class="kt-mycart__price">$ {{$cost}}</span>
										</div>
										<a href="javascript:void(0);"  class="add_to_cart_delete delete-icon kt-ml-10" rel="{{$val->domain_id}}">
											<i class="la la-trash-o" style="font-size: 1.6rem;margin-top: 0.3rem;"></i>
										</a>
									</div>
								</div>
								<span class="kt-mycart__desc">
									{{$val->domain_website}}
								</span>								
							</div>
							<!-- <a href="#" class="kt-mycart__pic">
								<img src="assets/media/products/product9.jpg" title="">
							</a> -->
						</div>
					</div>
					@endforeach
				</div>
				<div class="kt-mycart__footer">
					<div class="kt-mycart__section">
						<div class="kt-mycart__subtitel">
							<!-- <span>Sub Total</span>
							<span>Taxes</span> -->
							<span>Total</span>
						</div>
						<div class="kt-mycart__prices">
							<!-- <span>$ 840.00</span>
							<span>$ 72.00</span> -->
							<span class="kt-font-brand">$ {{$total_price}}</span>
						</div>
					</div>
					<div class="kt-mycart__button kt-align-right kt-mb-0">
						<a href="{{url('/advertiser/cart')}}" class="btn btn-primary btn-sm">Place Order</a>
					</div>
				</div>
			</div>
		<!-- end:: Mycart -->
		</div>
	@endif
@else
	@if( Cache::has( 'cartlist' ) ) 
		@php
            $get = Cache::get( 'cartlist' );
            $get = @unserialize($get);
            $count = count($get);
	    @endphp
	@else
		@php
			$get = [];
			$count = 0;
		@endphp
	@endif
	
	@if($count > 0)
        <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px" aria-expanded="true">
    @else
        <div class="kt-header__topbar-wrapper" data-offset="10px,0px" aria-expanded="true">
    @endif
	
        @if($count > 0)
            <span class="kt-badge kt-badge--brand">{{$count}}</span>
        @endif		
			<span class="kt-header__topbar-icon" title="Cart"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
					<rect x="0" y="0" width="24" height="24"></rect>
					<path d="M18.1446364,11.84388 L17.4471627,16.0287218 C17.4463569,16.0335568 17.4455155,16.0383857 17.4446387,16.0432083 C17.345843,16.5865846 16.8252597,16.9469884 16.2818833,16.8481927 L4.91303792,14.7811299 C4.53842737,14.7130189 4.23500006,14.4380834 4.13039941,14.0719812 L2.30560137,7.68518803 C2.28007524,7.59584656 2.26712532,7.50338343 2.26712532,7.4104669 C2.26712532,6.85818215 2.71484057,6.4104669 3.26712532,6.4104669 L16.9929851,6.4104669 L17.606173,3.78251876 C17.7307772,3.24850086 18.2068633,2.87071314 18.7552257,2.87071314 L20.8200821,2.87071314 C21.4717328,2.87071314 22,3.39898039 22,4.05063106 C22,4.70228173 21.4717328,5.23054898 20.8200821,5.23054898 L19.6915238,5.23054898 L18.1446364,11.84388 Z" fill="#000000" opacity="0.3"></path>
					<path d="M6.5,21 C5.67157288,21 5,20.3284271 5,19.5 C5,18.6715729 5.67157288,18 6.5,18 C7.32842712,18 8,18.6715729 8,19.5 C8,20.3284271 7.32842712,21 6.5,21 Z M15.5,21 C14.6715729,21 14,20.3284271 14,19.5 C14,18.6715729 14.6715729,18 15.5,18 C16.3284271,18 17,18.6715729 17,19.5 C17,20.3284271 16.3284271,21 15.5,21 Z" fill="#000000"></path>

				</g>
			</svg></span>
			
		</div>
		<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl">
		<!-- begin:: Mycart -->
			<div class="kt-mycart">
				<div class="kt-mycart__head kt-head">
					<div class="kt-mycart__info">
						<span class="kt-mycart__icon"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
							<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
								<rect x="0" y="0" width="24" height="24"></rect>
								<path d="M18.1446364,11.84388 L17.4471627,16.0287218 C17.4463569,16.0335568 17.4455155,16.0383857 17.4446387,16.0432083 C17.345843,16.5865846 16.8252597,16.9469884 16.2818833,16.8481927 L4.91303792,14.7811299 C4.53842737,14.7130189 4.23500006,14.4380834 4.13039941,14.0719812 L2.30560137,7.68518803 C2.28007524,7.59584656 2.26712532,7.50338343 2.26712532,7.4104669 C2.26712532,6.85818215 2.71484057,6.4104669 3.26712532,6.4104669 L16.9929851,6.4104669 L17.606173,3.78251876 C17.7307772,3.24850086 18.2068633,2.87071314 18.7552257,2.87071314 L20.8200821,2.87071314 C21.4717328,2.87071314 22,3.39898039 22,4.05063106 C22,4.70228173 21.4717328,5.23054898 20.8200821,5.23054898 L19.6915238,5.23054898 L18.1446364,11.84388 Z" fill="#000000" opacity="0.3"></path>
								<path d="M6.5,21 C5.67157288,21 5,20.3284271 5,19.5 C5,18.6715729 5.67157288,18 6.5,18 C7.32842712,18 8,18.6715729 8,19.5 C8,20.3284271 7.32842712,21 6.5,21 Z M15.5,21 C14.6715729,21 14,20.3284271 14,19.5 C14,18.6715729 14.6715729,18 15.5,18 C16.3284271,18 17,18.6715729 17,19.5 C17,20.3284271 16.3284271,21 15.5,21 Z" fill="#000000"></path>

							</g>
						</svg></span>
						<h3 class="kt-mycart__title">My Cart</h3>
					</div>
					<div class="kt-mycart__button">
						@if($count < 2)
		                    <a href="{{url('advertiser/cart')}}" class="btn btn-primary btn-sm" style=" ">{{$count}} Item</a>
		                @else 
		                    <a href="{{url('advertiser/cart')}}" class="btn btn-primary btn-sm" style=" ">{{$count}} Items</a>
		                @endif
					</div>
				</div>
				<div class="kt-mycart__body kt-scroll" data-scroll="true" data-mobile-height="100">
					@php
	                    $total_price = 0;
	                @endphp
	                @php            
		            	$cartList = DB::table('mst_domain_url')->whereIn('domain_id', $get)->get()->toArray();
		            @endphp
					@foreach($cartList as $key => $val)
					
					@php
						$cost = buyerDomainWebsitePriceCalculate($val->cost_price);
					@endphp					
					@php
						$total_price += $cost;
					@endphp


					<div class="kt-mycart__item">
						<div class="kt-mycart__container">
							<div class="kt-mycart__info w-100">
								<div class="kt-space-between flex-wrap">
									<div>
										<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon favicon-svg">
											<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
												<rect x="0" y="0" width="24" height="24"></rect>
												<circle fill="#000000" opacity="0.3" cx="12" cy="12" r="9"></circle>
												<path d="M11.7357634,20.9961946 C6.88740052,20.8563914 3,16.8821712 3,12 C3,11.9168367 3.00112797,11.8339369 3.00336944,11.751315 C3.66233009,11.8143341 4.85636818,11.9573854 4.91262842,12.4204038 C4.9904938,13.0609191 4.91262842,13.8615942 5.45804656,14.101772 C6.00346469,14.3419498 6.15931561,13.1409372 6.6267482,13.4612567 C7.09418079,13.7815761 8.34086797,14.0899175 8.34086797,14.6562185 C8.34086797,15.222396 8.10715168,16.1034596 8.34086797,16.2636193 C8.57458427,16.423779 9.5089688,17.54465 9.50920913,17.7048097 C9.50956962,17.8649694 9.83857487,18.6793513 9.74040201,18.9906563 C9.65905192,19.2487394 9.24857641,20.0501554 8.85059781,20.4145589 C9.75315358,20.7620621 10.7235846,20.9657742 11.7357634,20.9960544 L11.7357634,20.9961946 Z M8.28272988,3.80112099 C9.4158415,3.28656421 10.6744554,3 12,3 C15.5114513,3 18.5532143,5.01097452 20.0364482,7.94408274 C20.069657,8.72412177 20.0638332,9.39135321 20.2361262,9.6327358 C21.1131932,10.8600506 18.0995147,11.7043158 18.5573343,13.5605384 C18.7589671,14.3794892 16.5527814,14.1196773 16.0139722,14.886394 C15.4748026,15.6527403 14.1574598,15.137809 13.8520064,14.9904917 C13.546553,14.8431744 12.3766497,15.3341497 12.4789081,14.4995164 C12.5805657,13.664636 13.2922889,13.6156126 14.0555619,13.2719546 C14.8184743,12.928667 15.9189236,11.7871741 15.3781918,11.6380045 C12.8323064,10.9362407 11.963771,8.47852395 11.963771,8.47852395 C11.8110443,8.44901109 11.8493762,6.74109366 11.1883616,6.69207022 C10.5267462,6.64279981 10.170464,6.88841096 9.20435656,6.69207022 C8.23764828,6.49572949 8.44144409,5.85743687 8.2887174,4.48255778 C8.25453994,4.17415686 8.25619136,3.95717082 8.28272988,3.80112099 Z M20.9991771,11.8770357 C20.9997251,11.9179585 21,11.9589471 21,12 C21,16.9406923 17.0188468,20.9515364 12.0895088,20.9995641 C16.970233,20.9503326 20.9337111,16.888438 20.9991771,11.8770357 Z" fill="#000000" opacity="0.3"></path>
											</g>
										</svg>
										<a href="{{url('/search/website/').'/'.$val->domain_url}}" class="kt-mycart__title">
											{{$val->domain_url}}
										</a>
									</div>
									<div class="d-flex">
										<div class="kt-mycart__action">
											<span class="kt-mycart__price">$ {{$cost}}</span>
										
										</div>
										<a href="javascript:void(0);"  class="add_to_cart_delete delete-icon kt-ml-10" rel="{{$val->domain_id}}">
											<i class="la la-trash-o" style="font-size: 1.6rem;margin-top: 0.3rem;"></i>
										</a>
									</div>
								</div>
								<span class="kt-mycart__desc">
									{{$val->domain_website}}
								</span>
								
								
							</div>
							<!-- <a href="#" class="kt-mycart__pic">
								<img src="assets/media/products/product9.jpg" title="">
							</a> -->
						</div>
					</div>
					@endforeach
				</div>
				<div class="kt-mycart__footer">
					<div class="kt-mycart__section">
						<div class="kt-mycart__subtitel">
							<!-- <span>Sub Total</span>
							<span>Taxes</span> -->
							<span>Total</span>
						</div>
						<div class="kt-mycart__prices">
							<!-- <span>$ 840.00</span>
							<span>$ 72.00</span> -->
							<span class="kt-font-brand">$ {{$total_price}}</span>
						</div>
					</div>
					<div class="kt-mycart__button kt-align-right kt-mb-0">
						<a href="{{url('/advertiser/cart')}}" class="btn btn-primary btn-sm">Place Order</a>
					</div>
				</div>
			</div>
		<!-- end:: Mycart -->
		</div>
@endif
</span>


