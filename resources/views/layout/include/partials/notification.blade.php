<div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
		<span class="kt-badge kt-badge--brand">3</span>
		<span class="kt-header__topbar-icon"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
			<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
				<rect x="0" y="0" width="24" height="24"></rect>
				<path d="M13.2070325,4 C13.0721672,4.47683179 13,4.97998812 13,5.5 C13,8.53756612 15.4624339,11 18.5,11 C19.0200119,11 19.5231682,10.9278328 20,10.7929675 L20,17 C20,18.6568542 18.6568542,20 17,20 L7,20 C5.34314575,20 4,18.6568542 4,17 L4,7 C4,5.34314575 5.34314575,4 7,4 L13.2070325,4 Z" fill="#000000"></path>
				<circle fill="#000000" opacity="0.3" cx="18.5" cy="5.5" r="2.5"></circle>
			</g>
		</svg></span>
		
	</div>
	<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">

		<!--begin: Head -->
		<div class="kt-head kt-head--skin-light kt-head--fit-x kt-head--fit-b">
			<h3 class="kt-head__title">
				User Notifications
				&nbsp;
				<span class="btn btn-label-primary btn-sm btn-bold btn-font-md">23 new</span>
			</h3>
			<ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-3x nav-tabs-line-brand  kt-notification-item-padding-x" role="tablist">
				<li class="nav-item">
					<a class="nav-link active show" data-toggle="tab" href="#topbar_notifications_notifications" role="tab" aria-selected="true">Alerts</a>
				</li>
			</ul>
		</div>
		<!--end: Head -->

		<div class="tab-content">
		<div class="tab-pane active show" id="topbar_notifications_notifications" role="tabpanel">
			<div class="kt-notification kt-margin-t-10 kt-margin-b-10 kt-scroll" data-scroll="true" data-height="300" data-mobile-height="200">
				<a href="#" class="kt-notification__item">
					<div class="kt-notification__item-icon">
						<i class="flaticon2-line-chart kt-font-success"></i>
					</div>
					<div class="kt-notification__item-details">
						<div class="kt-notification__item-title">
							New order has been received
						</div>
						<div class="kt-notification__item-time">
							2 hrs ago
						</div>
					</div>
				</a>
				<a href="#" class="kt-notification__item">
					<div class="kt-notification__item-icon">
						<i class="flaticon2-box-1 kt-font-brand"></i>
					</div>
					<div class="kt-notification__item-details">
						<div class="kt-notification__item-title">
							New customer is registered
						</div>
						<div class="kt-notification__item-time">
							3 hrs ago
						</div>
					</div>
				</a>
				<a href="#" class="kt-notification__item">
					<div class="kt-notification__item-icon">
						<i class="flaticon2-chart2 kt-font-danger"></i>
					</div>
					<div class="kt-notification__item-details">
						<div class="kt-notification__item-title">
							Application has been approved
						</div>
						<div class="kt-notification__item-time">
							3 hrs ago
						</div>
					</div>
				</a>
				<a href="#" class="kt-notification__item">
					<div class="kt-notification__item-icon">
						<i class="flaticon2-image-file kt-font-warning"></i>
					</div>
					<div class="kt-notification__item-details">
						<div class="kt-notification__item-title">
							New file has been uploaded
						</div>
						<div class="kt-notification__item-time">
							5 hrs ago
						</div>
					</div>
				</a>
				<a href="#" class="kt-notification__item">
					<div class="kt-notification__item-icon">
						<i class="flaticon2-drop kt-font-info"></i>
					</div>
					<div class="kt-notification__item-details">
						<div class="kt-notification__item-title">
							New user feedback received
						</div>
						<div class="kt-notification__item-time">
							8 hrs ago
						</div>
					</div>
				</a>
				<a href="#" class="kt-notification__item">
					<div class="kt-notification__item-icon">
						<i class="flaticon2-pie-chart-2 kt-font-success"></i>
					</div>
					<div class="kt-notification__item-details">
						<div class="kt-notification__item-title">
							System reboot has been successfully completed
						</div>
						<div class="kt-notification__item-time">
							12 hrs ago
						</div>
					</div>
				</a>
				<a href="#" class="kt-notification__item">
					<div class="kt-notification__item-icon">
						<i class="flaticon2-favourite kt-font-danger"></i>
					</div>
					<div class="kt-notification__item-details">
						<div class="kt-notification__item-title">
							New order has been placed
						</div>
						<div class="kt-notification__item-time">
							15 hrs ago
						</div>
					</div>
				</a>
				<a href="#" class="kt-notification__item kt-notification__item--read">
					<div class="kt-notification__item-icon">
						<i class="flaticon2-safe kt-font-primary"></i>
					</div>
					<div class="kt-notification__item-details">
						<div class="kt-notification__item-title">
							Company meeting canceled
						</div>
						<div class="kt-notification__item-time">
							19 hrs ago
						</div>
					</div>
				</a>
				<a href="#" class="kt-notification__item">
					<div class="kt-notification__item-icon">
						<i class="flaticon2-psd kt-font-success"></i>
					</div>
					<div class="kt-notification__item-details">
						<div class="kt-notification__item-title">
							New report has been received
						</div>
						<div class="kt-notification__item-time">
							23 hrs ago
						</div>
					</div>
				</a>
				<a href="#" class="kt-notification__item">
					<div class="kt-notification__item-icon">
						<i class="flaticon-download-1 kt-font-danger"></i>
					</div>
					<div class="kt-notification__item-details">
						<div class="kt-notification__item-title">
							Finance report has been generated
						</div>
						<div class="kt-notification__item-time">
							25 hrs ago
						</div>
					</div>
				</a>
				<a href="#" class="kt-notification__item">
					<div class="kt-notification__item-icon">
						<i class="flaticon-security kt-font-warning"></i>
					</div>
					<div class="kt-notification__item-details">
						<div class="kt-notification__item-title">
							New customer comment recieved
						</div>
						<div class="kt-notification__item-time">
							2 days ago
						</div>
					</div>
				</a>
				<a href="#" class="kt-notification__item">
					<div class="kt-notification__item-icon">
						<i class="flaticon2-pie-chart kt-font-success"></i>
					</div>
					<div class="kt-notification__item-details">
						<div class="kt-notification__item-title">
							New customer is registered
						</div>
						<div class="kt-notification__item-time">
							3 days ago
						</div>
					</div>
				</a>
			</div>
		</div>
		<div class="tab-pane" id="topbar_notifications_events" role="tabpanel">
			<div class="kt-notification kt-margin-t-10 kt-margin-b-10 kt-scroll" data-scroll="true" data-height="300" data-mobile-height="200">
				<a href="#" class="kt-notification__item">
					<div class="kt-notification__item-icon">
						<i class="flaticon2-psd kt-font-success"></i>
					</div>
					<div class="kt-notification__item-details">
						<div class="kt-notification__item-title">
							New report has been received
						</div>
						<div class="kt-notification__item-time">
							23 hrs ago
						</div>
					</div>
				</a>
				<a href="#" class="kt-notification__item">
					<div class="kt-notification__item-icon">
						<i class="flaticon-download-1 kt-font-danger"></i>
					</div>
					<div class="kt-notification__item-details">
						<div class="kt-notification__item-title">
							Finance report has been generated
						</div>
						<div class="kt-notification__item-time">
							25 hrs ago
						</div>
					</div>
				</a>
				<a href="#" class="kt-notification__item">
					<div class="kt-notification__item-icon">
						<i class="flaticon2-line-chart kt-font-success"></i>
					</div>
					<div class="kt-notification__item-details">
						<div class="kt-notification__item-title">
							New order has been received
						</div>
						<div class="kt-notification__item-time">
							2 hrs ago
						</div>
					</div>
				</a>
				<a href="#" class="kt-notification__item">
					<div class="kt-notification__item-icon">
						<i class="flaticon2-box-1 kt-font-brand"></i>
					</div>
					<div class="kt-notification__item-details">
						<div class="kt-notification__item-title">
							New customer is registered
						</div>
						<div class="kt-notification__item-time">
							3 hrs ago
						</div>
					</div>
				</a>
				<a href="#" class="kt-notification__item">
					<div class="kt-notification__item-icon">
						<i class="flaticon2-chart2 kt-font-danger"></i>
					</div>
					<div class="kt-notification__item-details">
						<div class="kt-notification__item-title">
							Application has been approved
						</div>
						<div class="kt-notification__item-time">
							3 hrs ago
						</div>
					</div>
				</a>
				<a href="#" class="kt-notification__item">
					<div class="kt-notification__item-icon">
						<i class="flaticon2-image-file kt-font-warning"></i>
					</div>
					<div class="kt-notification__item-details">
						<div class="kt-notification__item-title">
							New file has been uploaded
						</div>
						<div class="kt-notification__item-time">
							5 hrs ago
						</div>
					</div>
				</a>
				<a href="#" class="kt-notification__item">
					<div class="kt-notification__item-icon">
						<i class="flaticon2-drop kt-font-info"></i>
					</div>
					<div class="kt-notification__item-details">
						<div class="kt-notification__item-title">
							New user feedback received
						</div>
						<div class="kt-notification__item-time">
							8 hrs ago
						</div>
					</div>
				</a>
				<a href="#" class="kt-notification__item">
					<div class="kt-notification__item-icon">
						<i class="flaticon2-pie-chart-2 kt-font-success"></i>
					</div>
					<div class="kt-notification__item-details">
						<div class="kt-notification__item-title">
							System reboot has been successfully completed
						</div>
						<div class="kt-notification__item-time">
							12 hrs ago
						</div>
					</div>
				</a>
				<a href="#" class="kt-notification__item">
					<div class="kt-notification__item-icon">
						<i class="flaticon2-favourite kt-font-brand"></i>
					</div>
					<div class="kt-notification__item-details">
						<div class="kt-notification__item-title">
							New order has been placed
						</div>
						<div class="kt-notification__item-time">
							15 hrs ago
						</div>
					</div>
				</a>
				<a href="#" class="kt-notification__item kt-notification__item--read">
					<div class="kt-notification__item-icon">
						<i class="flaticon2-safe kt-font-primary"></i>
					</div>
					<div class="kt-notification__item-details">
						<div class="kt-notification__item-title">
							Company meeting canceled
						</div>
						<div class="kt-notification__item-time">
							19 hrs ago
						</div>
					</div>
				</a>
				<a href="#" class="kt-notification__item">
					<div class="kt-notification__item-icon">
						<i class="flaticon2-psd kt-font-success"></i>
					</div>
					<div class="kt-notification__item-details">
						<div class="kt-notification__item-title">
							New report has been received
						</div>
						<div class="kt-notification__item-time">
							23 hrs ago
						</div>
					</div>
				</a>
				<a href="#" class="kt-notification__item">
					<div class="kt-notification__item-icon">
						<i class="flaticon-download-1 kt-font-danger"></i>
					</div>
					<div class="kt-notification__item-details">
						<div class="kt-notification__item-title">
							Finance report has been generated
						</div>
						<div class="kt-notification__item-time">
							25 hrs ago
						</div>
					</div>
				</a>
				<a href="#" class="kt-notification__item">
					<div class="kt-notification__item-icon">
						<i class="flaticon-security kt-font-warning"></i>
					</div>
					<div class="kt-notification__item-details">
						<div class="kt-notification__item-title">
							New customer comment recieved
						</div>
						<div class="kt-notification__item-time">
							2 days ago
						</div>
					</div>
				</a>
				<a href="#" class="kt-notification__item">
					<div class="kt-notification__item-icon">
						<i class="flaticon2-pie-chart kt-font-success"></i>
					</div>
					<div class="kt-notification__item-details">
						<div class="kt-notification__item-title">
							New customer is registered
						</div>
						<div class="kt-notification__item-time">
							3 days ago
						</div>
					</div>
				</a>
			</div>
		</div>
		<div class="tab-pane" id="topbar_notifications_logs" role="tabpanel">
			<div class="kt-grid kt-grid--ver" style="min-height: 200px;">
				<div class="kt-grid kt-grid--hor kt-grid__item kt-grid__item--fluid kt-grid__item--middle">
					<div class="kt-grid__item kt-grid__item--middle kt-align-center">
						All caught up!
						<br>No new notifications.
					</div>
				</div>
			</div>
		</div>
		</div>
</div>