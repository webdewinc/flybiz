 
<div id="kt_header" class="kt-header  kt-header--fixed" data-ktheader-minimize="on">
	
	<div class="kt-container  kt-container--fluid">
		@if(Auth::check())
			<button class="kt-aside-toggler kt-aside-toggler--left" id="kt_aside_toggler"><span></span></button>
		@endif
		<!-- begin: Header Menu -->
		<button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
		
		<!-- begin:: Brand -->
		<div class="kt-header__brand   kt-grid__item" id="kt_header_brand">		
			@include('layout.include.partials.brand')
		</div>

		<!-- end: Header Menu -->	

		<!-- begin:: Header Topbar -->
		<div class="kt-header__topbar kt-grid__item">

			<!-- end:: Brand -->
			<div class="kt-header-menu-wrapper kt-grid__item kt-grid__item--fluid justify-content-center flex-grow-1" id="kt_header_menu_wrapper">
				
				<div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile ">
					
					
					<div class="kt-quick-search kt-quick-search--dropdown kt-quick-search--result-compact" id="">
						
			            @include('layout.include.partials.search-top')

						<div class="kt-quick-search__wrapper kt-scroll" data-scroll="true" data-height="325" data-mobile-height="200">
						</div>
					</div>
					

				</div>
			</div>

			@if(Auth::check())

				<div class="d-flex align-items-center" id="partials-profile">

					@include('layout.include.profileFull')

				</div>
			@else
				<div class="d-flex align-items-center" id="partials-profile">

					<div class="kt-header__topbar-item dropdown">
						@include('layout.include.partials.wish')
					</div>		

					<!--begin: Cart -->
					<div class="kt-header__topbar-item dropdown">
						@include('layout.include.partials.cart')
					</div>
					<!--end: Cart -->

					<div class="kt-header__topbar-item" >
						@include('layout.include.partials.login')
					</div>
				</div>
			@endif

			
		</div>

		<!-- end:: Header Topbar -->
	</div>
</div>
