
<div class="kt-header__topbar-item">
	@include('layout.include.partials.chat')									
</div>

@if(Auth::user()->user_type != 'admin')
	@if( Auth::user()->switched == 'advertiser' )
		<div class="kt-header__topbar-item dropdown">
			@include('layout.include.partials.wish')								
		</div>
	@endif
	@if( Auth::user()->switched == 'advertiser' )
	<!--begin: Cart -->
	<div class="kt-header__topbar-item dropdown">						
		@include('layout.include.partials.cart')
	</div>
	<!--end: Cart -->
	@endif
@endif

@include('layout.include.partials.profile')


<!--end: User bar -->