<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- begin::Head -->
    <meta charset="utf-8" />
    <meta name="description" content="Latest updates and statistic charts">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{{ config('app.name') }} | @yield('title')</title>
    <!-- CSS -->
    <link rel="stylesheet"
          href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- <link rel="stylesheet" href="{{ asset('public/css/twiliochat.css') }}"> -->

    <!--begin::Global Theme Styles(used by all pages) -->
    <link href="{{URL::asset('public/assets/vendors/global/vendors.bundle.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{URL::asset('public/assets/css/demo4/style.bundle.css')}}" rel="stylesheet" type="text/css" />
    <!--end::Global Theme Styles -->
    <link rel="shortcut icon" href="{{URL::asset('public/assets/media/logos/favicon.ico')}}" />
    <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css"> -->
    <!-- <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script> -->


  </head>
  <body style="background-image: url({{URL::asset('public/assets/media/demos/demo4/header.jpg')}}); background-position: center top; background-size: 100% 350px;" class="kt-page--loading-enabled kt-page--loading kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-page--loading">

    @include('layouts.partials._header-base-mobile')

        <div class="kt-grid kt-grid--hor kt-grid--root">
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
                <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
                    <!-- begin:: Header -->
                    <div id="kt_header" class="kt-header kt-header--fixed " data-ktheader-minimize="on">
                        <div class="kt-container ">
                            @include('layouts.partials._header-brand')
                            @include('layouts.admin.partials._header-menu')
                            @include('layouts.admin.partials._header-topbar')
                        </div>
                    </div>
                    <!-- end:: Header -->
                    <div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
                        <div class="kt-content--fit-top kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

                            <!--begin:: Content -->
                            <?php 
                                // Available alpha caracters
                                $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' . Auth::user()->name;

                                // generate a pin based on 2 * 7 digits + a random character
                                $pin = mt_rand(1000000, 9999999)
                                    . mt_rand(1000000, 9999999)
                                    . $characters[rand(0, strlen($characters) - 1)];

                                // shuffle the result
                                $string = str_shuffle($pin);
                            ?>
                            <input id="username-input" value="{{Auth::user()->name}}_{{$string}}" type="hidden" placeholder="username"/>
                            <input id="username-input-get" value="{{Auth::user()->name}}" type="hidden" placeholder="username"/>
                            <input id="chatChannelId" name="chatChannelId" value="{{$channel_id}}" type="hidden">

                            <div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
                                <div class="kt-content kt-content--fit-top kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
                                    <!-- begin:: Content -->
                                    <div class="kt-container kt-grid__item kt-grid__item--fluid">
                                        <div class="row">
                                            <div class="col-xl-4">
                                                <!--Begin::Portlet-->
                                                <div class="kt-portlet kt-portlet--height-fluid">
                                                    <div class="kt-portlet__head kt-portlet__head--noborder">
                                                    </div>
                                                    <div class="kt-portlet__body">
                                                        <!--begin::Widget -->
                                                        <div class="kt-widget kt-widget--user-profile-2">
                                                            <div class="kt-widget__head">
                                                                <div class="kt-widget__media">
                                                                    <img class="kt-widget__img kt-hidden-" src="../assets/media/users/default.jpg" alt="image">
                                                                    <div class="kt-widget__pic kt-widget__pic--success kt-font-success kt-font-boldest kt-hidden">
                                                                        ChS
                                                                    </div>
                                                                </div>
                                                                <div class="kt-widget__info">
                                                                    <a href="#" class="kt-widget__username">
                                                                        Tiger Nixon
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="kt-widget__body">
                                                                <div class="kt-widget__item">
                                                                    <div class="kt-widget__contact">
                                                                        <span class="kt-widget__label">Question:</span>
                                                                        <span class="kt-widget__data">How do I install Skype?</span>
                                                                    </div>
                                                                    <div class="kt-widget__contact">
                                                                        <span class="kt-widget__label">Amount:</span>
                                                                        <span class="kt-widget__data">$320</span>
                                                                    </div>
                                                                    <div class="kt-widget__contact">
                                                                        <span class="kt-widget__label">Premium service:</span>
                                                                        <span class="kt-widget__data">No</span>
                                                                    </div>
                                                                    <div class="kt-widget__contact">
                                                                        <span class="kt-widget__label">Category:</span>
                                                                        <span class="kt-widget__data">Law</span>
                                                                    </div>
                                                                    <div class="kt-widget__contact">
                                                                        <span class="kt-widget__label">Time:</span>
                                                                        <span class="kt-widget__data">2 hours</span>
                                                                    </div>
                                                                    <div class="kt-widget__contact">
                                                                        <span class="kt-widget__label">Previously Asked Questions:</span>
                                                                        <span class="kt-widget__data"><a href="previously-asked-questions.php">2</a></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="mt-4 row">
                                                                <div class="col-6">
                                                                    <button type="button" class="btn primary-btn btn-block" data-toggle="modal" data-target="#refund-customer">Refund</button>
                                                                </div>
                                                                <div class="col-6">
                                                                    <button type="button" class="btn primary-btn btn-block" data-toggle="modal" data-target="#offer-premium-service">Offer Premium</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--end::Widget -->
                                                    </div>
                                                </div>
                                                <!--End::Portlet-->
                                            </div>
                                            <div class="modal fade" id="refund-customer" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLongTitle">Refund Customer</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="form-group form-group-xs row">
                                                                <label class="col-4 col-form-label">Question:</label>
                                                                <div class="col-8">
                                                                    <span class="form-control-plaintext">Lorem ipsum dolor sit amet, consectetur.</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group form-group-xs row">
                                                                <label class="col-4 col-form-label">Customer Name:</label>
                                                                <div class="col-8">
                                                                    <span class="form-control-plaintext">Tiger Nixon</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group form-group-xs row">
                                                                <label class="col-4 col-form-label">Category:</label>
                                                                <div class="col-8">
                                                                    <span class="form-control-plaintext">Law</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group form-group-xs row">
                                                                <label class="col-4 col-form-label">Refund Amount:</label>
                                                                <div class="col-8">
                                                                    <span class="form-control-plaintext">$320</span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group form-group-xs row">
                                                                <label class="col-4 col-form-label">Why this customer is requesting a refund?</label>
                                                                <div class="col-8">
                                                                    <label for="input" class="label-effect">
                                                                        <textarea class="form-control" rows="5" id="comment" placeholder="&nbsp;"></textarea>
                                                                        <span class="label"></span>
                                                                        <span class="border"></span>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="reset" class="btn secondary-btn" data-dismiss="modal">Cancel</button>
                                                            <button type="reset" class="btn primary-btn">Refund</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal fade" id="offer-premium-service" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLongTitle">Offer Premium Service</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <form>
                                                                <div class="form-group">
                                                                    <label for="service-name" class="label-effect">
                                                                        <select class="form-control" id="service-name">
                                                                            <option>Remote tech support with phone call</option>
                                                                            <option>Remote tech support with Email</option>
                                                                            <option>Others</option>
                                                                        </select>
                                                                        <span class="label">Name of service</span>
                                                                        <span class="border"></span>
                                                                    </label>
                                                                </div>
                                                                <div class="form-group kt-hidden">
                                                                    <label for="Createown">Create your own</label>
                                                                    <input type="text" class="form-control" id="Createown" placeholder="">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="description" class="label-effect">
                                                                        <textarea class="form-control" id="description" placeholder="&nbsp;"></textarea>
                                                                        <span class="label">Description</span>
                                                                        <span class="border"></span>
                                                                    </label>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="input" class="label-effect">
                                                                        <input type="text" id="input" placeholder="&nbsp;">
                                                                        <span class="label">Amount</span>
                                                                        <span class="border"></span>
                                                                    </label>
                                                                </div>
                                                            </form>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn secondary-btn" data-dismiss="modal">Cancel</button>
                                                            <button type="button" class="btn primary-btn">Submit Offer</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--Begin:: App Content-->
                                            <div class="kt-grid__item kt-grid__item--fluid kt-app__content col-xl-8" id="kt_chat_content">
                                                <div class="kt-chat">
                                                    <div class="kt-portlet kt-portlet--head-lg kt-portlet--last">
                                                        <div class="kt-portlet__head">
                                                            <div class="kt-chat__head ">
                                                                <div class="kt-chat__left">
                                                                    <div class="kt-chat__label">
                                                                        <a href="#" class="kt-chat__title">Tiger Nixon</a>
                                                                    </div>
                                                                </div>
                                                                <div class="kt-chat__right d-flex justify-content-end">
                                                                    <button type="button" class="btn primary-btn">Accept</button>
                                                                    <div class="dropdown dropdown-inline">
                                                                        <button type="button" class="btn secondary-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                            Reject
                                                                        </button>
                                                                        <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-md" x-placement="top-end" style="position: absolute; transform: translate3d(-227px, -238px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                                            <!--begin::Nav-->
                                                                            <ul class="kt-nav">
                                                                                <!--  <li class="kt-nav__head">
                                                                        Request Alerts
                                                                    </li>
                                                                    <li class="kt-nav__separator"></li> -->
                                                                                <li class="kt-nav__item">
                                                                                    <a href="#" class="kt-nav__link">
                                                                                        <i class="kt-nav__link-icon flaticon2-open-text-book"></i>
                                                                                        <span class="kt-nav__link-text">Report this question as span</span>
                                                                                    </a>
                                                                                </li>
                                                                                <li class="kt-nav__item">
                                                                                    <a href="#" class="kt-nav__link">
                                                                                        <i class="kt-nav__link-icon flaticon2-bell-2"></i>
                                                                                        <span class="kt-nav__link-text">Duplicate question</span>
                                                                                        <span class="kt-nav__link-badge">
                                                                                            <span class="kt-badge kt-badge--brand  kt-badge--rounded-">2</span>
                                                                                        </span>
                                                                                    </a>
                                                                                </li>
                                                                                <li class="kt-nav__item">
                                                                                    <a href="#" class="kt-nav__link">
                                                                                        <i class="kt-nav__link-icon flaticon2-dashboard"></i>
                                                                                        <span class="kt-nav__link-text">Miscategorized</span>
                                                                                    </a>
                                                                                </li>
                                                                                <li class="kt-nav__item">
                                                                                    <a href="#" class="kt-nav__link">
                                                                                        <i class="kt-nav__link-icon flaticon2-protected"></i>
                                                                                        <span class="kt-nav__link-text">Refund Customer</span>
                                                                                    </a>
                                                                                </li>
                                                                                <li class="kt-nav__separator"></li>
                                                                                <li class="kt-nav__item">
                                                                                    <a href="#" class="kt-nav__link">
                                                                                        <i class="kt-nav__link-icon flaticon2-bell-2"></i>
                                                                                        <span class="kt-nav__link-text">Underpriced</span>
                                                                                    </a>
                                                                                </li>
                                                                            </ul>
                                                                            <!--end::Nav-->
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="kt-portlet__body">
                                                            <div class="kt-scroll kt-scroll--pull" data-mobile-height="300">
                                                                <div class="kt-chat__messages">

                                                                    <!-- message her -->

                                                                    <!-- channels -->
                                                                    <div class="wrapper-chat">
                                                                        <div id="channel-list" class="row not-showing" style="display:none;"></div>
                                                                        <div class="row">
                                                                            <div class="col-lg-12">                              
                                                                                <div class="all-messages" id="message-list">
                                                                                    
                                                                                <?php $numItems = count($getmsg); $i=0;?>    
                                                                                                <!-- message list here -->
                                                                                    @foreach($getmsg as $msg)
                                                                                        <?php $date = date_create($msg['created_at']);
                                                                                                $date=date_format($date,"d/m/Y h:iA");
                                                                                                $date1=explode(' ', $date);
                                                                                                $todaydate=date("d/m/Y");
                                                                                                if($todaydate == $date1[0])
                                                                                                {
                                                                                                    $actualdate='Today';
                                                                                                }
                                                                                                else
                                                                                                {
                                                                                                    $actualdate=$date1[0];
                                                                                                }
                                                                                                
                                                                                        ?>
                                                                                            @if($msg['sender_id'] == Auth::user()->id)
                                                                                                <div class="kt-chat__message kt-chat__message--right">
                                                                                                    <div class="kt-chat__user">
                                                                                                        <span data-content="date" class="kt-chat__datetime">{{$actualdate}} - {{$date1[1]}}</span>

                                                                                                        <?php 
                                                                                                            $name = \App\User::where('id',$msg['receiver_id'])->first();
                                                                                                            $name = json_decode(json_encode($name),true);

                                                                                                        ?>

                                                                                                        <a href="javascript:void(0);" class="kt-chat__username"  data-content="username">You</span></a>
                                                                                                        <span class="kt-userpic kt-userpic--circle kt-userpic--sm">
                                                                                                            <img src="{{url('public/assets/media/users/default.jpg')}}" alt="image">
                                                                                                        </span>
                                                                                                    </div>
                                                                                                    
                                                                                                    <div class="kt-chat__text kt-bg-light-success" data-content="body">
                                                                                                        {{$msg['reply']}}
                                                                                                    </div>
                                                                                                </div>
                                                                                            @else  
                                                                                                
                                                                                                <div class="kt-chat__message">
                                                                                                    <div class="kt-chat__user">
                                                                                                        <span class="kt-userpic kt-userpic--circle kt-userpic--sm">
                                                                                                            <!-- <img src="{{asset('frontend/userImages/'.Auth::user()->user_image)}}" alt="image"> -->
                                                                                                            <img src="{{url('public/assets/media/users/default.jpg')}}" alt="image">
                                                                                                        </span>
                                                                                                        <?php 
                                                                                                            $name = \App\User::where('id',$msg['sender_id'])->first();
                                                                                                            $name = json_decode(json_encode($name),true);

                                                                                                        ?>

                                                                                                        <a href="javascript:void(0);" data-content="username" class="kt-chat__username">{{$name['name']}}</span></a>
                                                                                                        <span data-content="date" class="kt-chat__datetime">{{$actualdate}} - {{$date1[1]}}</span>
                                                                                                    </div>               
                                                                                                    <div class="kt-chat__text kt-bg-light-success" data-content="body">
                                                                                                        {{$msg['reply']}}
                                                                                                    </div>                                    
                                                                                                </div>

                                                                                            @endif    
                                                                                        @endforeach      

                                                                                </div>
                                                                            </div>
                                                                        </div>  
                                                                    </div>

                                                                    <!-- channels -->
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="kt-portlet__foot">
                                                            <div class="kt-chat__input">
                                                                <div class="kt-chat__editor">
                                                                    <textarea id="input-text" style="height: 50px" disabled="true" name="msg" placeholder="Type here..."></textarea>
                                                                </div>
                                                                <div class="kt-chat__toolbar">
                                                                    <div class="kt_chat__tools">
                                                                        <a href="#"><i class="flaticon2-link"></i></a>
                                                                        <a href="#"><i class="flaticon2-photograph"></i></a>
                                                                        <a href="#"><i class="flaticon2-photo-camera"></i></a>
                                                                    </div>
                                                                    <div class="kt_chat__actions">
                                                                        <button type="button" class="btn btn-md btn-upper btn-bold kt-chat__reply primary-btn">reply</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--End:: App Content-->
                                        </div>
                                    </div>
                                    <!-- end:: Content -->
                                </div>
                            </div>

                            <!-- HTML Templates -->
                            <script type="text/html" id="message-template">
                                
                                    <div class="kt-chat__user">
                                        <span class="kt-userpic kt-userpic--circle kt-userpic--sm">
                                            <!-- <img src="{{asset('frontend/userImages/'.Auth::user()->user_image)}}" alt="image"> -->
                                            <img src="{{url('public/assets/media/users/default.jpg')}}" alt="image">
                                        </span>
                                        <a href="javascript:void(0);" data-content="username" class="kt-chat__username"></span></a>
                                        <span data-content="date" class="kt-chat__datetime"></span>
                                    </div>                                                                                                
                                    <div class="kt-chat__text kt-bg-light-success" data-content="body">
                                        
                                    </div>

                                <!-- <div class="kt-chat__message kt-chat__message--right">
                                    <div class="kt-chat__user">
                                        <span class="kt-chat__datetime">10 Seconds</span>
                                        <a href="#" class="kt-chat__username">You</span></a>
                                        <span class="kt-userpic kt-userpic--circle kt-userpic--sm">
                                            <img src="../assets/media/users/default.jpg" alt="image">
                                        </span>
                                    </div>
                                    <div class="kt-chat__text kt-bg-light-brand">
                                        Offer Proposed: Full installation support. $21
                                    </div><br>
                                    <div class="kt-chat__text kt-bg-light-brand">
                                        Offer Accepted: Full installation support.
                                    </div><br>
                                    <div class="kt-chat__text kt-bg-light-brand">
                                        The Customer has accepted your Answer.
                                    </div>
                                </div> -->
                            </script>
                            <!-- HTML Templates -->
                            <script type="text/html" id="message-template-right">
                                
                                <div class="kt-chat__user">
                                    <span class="kt-userpic kt-userpic--circle kt-userpic--sm">
                                        <!-- <img src="{{asset('frontend/userImages/'.Auth::user()->user_image)}}" alt="image"> -->
                                        <img src="{{url('public/assets/media/users/default.jpg')}}" alt="image">
                                    </span>
                                    
                                    <span data-content="date" class="kt-chat__datetime"></span>
                                    <a href="javascript:void(0);" class="kt-chat__username"  data-content="username"></span></a>
                                    <span class="kt-userpic kt-userpic--circle kt-userpic--sm">
                                        <img src="{{url('public/assets/media/users/default.jpg')}}" alt="image">
                                    </span>
                                </div>
                                
                                <div class="kt-chat__text kt-bg-light-success" data-content="body">
                                    
                                </div>
                            </script>

                            <script type="text/html" id="channel-template">
                                <!-- <div class="col-md-12">
                                    <p class="channel-element" data-content="channelName"></p>
                                </div> -->
                            </script>
                            <script type="text/html" id="member-notification-template">
                                <!-- <p class="member-status" data-content="status"></p> -->
                            </script>
                            <!-- JavaScript -->
                            <script src="//code.jquery.com/jquery-2.2.0.min.js"></script>
                            <script src="{{ asset('public/js/vendor/jquery-throttle.min.js') }}"></script>
                            <script src="{{ asset('public/js/vendor/jquery.loadTemplate-1.4.4.min.js') }}"></script>
                            <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
                            <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.1/moment.min.js"></script>
                            <!-- Twilio Common helpers and Twilio Chat JavaScript libs from CDN. -->
                            <script src="//media.twiliocdn.com/sdk/js/common/v0.1/twilio-common.min.js"></script>
                            <script src="//media.twiliocdn.com/sdk/js/chat/v3.0/twilio-chat.min.js"></script>
                            <script>
                                    var twiliochat = (function() {
                                var tc = {};

                                var GENERAL_CHANNEL_UNIQUE_NAME = "{{$room_name}}";

                                var GENERAL_CHANNEL_NAME = "{{$room_name_view}}";
                                var MESSAGES_HISTORY_LIMIT = 50;

                                var $channelList;
                                var $inputText;
                                var $usernameInput;
                                var $statusRow;
                                var $connectPanel;
                                var $newChannelInputRow;
                                var $newChannelInput;
                                var $typingRow;
                                var $typingPlaceholder;

                                $(document).ready(function() {
                                    tc.$messageList = $('#message-list');
                                    $channelList = $('#channel-list');
                                    $inputText = $('#input-text');
                                    $usernameInput = $('#username-input');
                                    $statusRow = $('#status-row');
                                    $connectPanel = $('#connect-panel');
                                    $newChannelInputRow = $('#new-channel-input-row');
                                    $newChannelInput = $('#new-channel-input');
                                    $typingRow = $('#typing-row');
                                    $typingPlaceholder = $('#typing-placeholder');
                                    $usernameInput.focus();
                                    $usernameInput.on('keypress', handleUsernameInputKeypress);
                                    
                                    $inputText.on('keypress', handleInputTextKeypress);
                                    $newChannelInput.on('keypress', tc.handleNewChannelInputKeypress);
                                    $('#connect-image').on('click', connectClientWithUsername);
                                    $('#add-channel-image').on('click', showAddChannelInput);
                                    $('#leave-span').on('click', disconnectClient);
                                    $('#delete-channel-span').on('click', deleteCurrentChannel);
                                });

                                $( window ).on( "load", function() {
                                   
                                    connectClientWithUsername();
                                    //addChannel();

                                });

                                function getUsername(author) {
                                    var str = author;
                                    var res = str.split("_");
                                    return res[0];
                                }

                                function handleUsernameInputKeypress(event) {
                                    if (event.keyCode === 13){
                                    connectClientWithUsername();
                                    }
                                }

                                function handleInputTextKeypress(event) {
                                    var sender = '{{Auth::user()->id}}';    
                                    var receiver = '{{$userdata["id"]}}';
                                    var channelid = tc.currentChannel.sid;
                                    var msg = $('#input-text').val();
                                    if (event.keyCode === 13) {
                                    tc.currentChannel.sendMessage($(this).val());
                                    event.preventDefault();
                                    $(this).val('');
                                    var chatChannelId = $('#chatChannelId').val();
                                    var roomname='{{$room_name}}';
                                    var date= dateFormatter.getTodayDate();
                                    $.post("{{url('/storemsg')}}", {chatChannelId:chatChannelId,sender:sender,receiver:receiver,channelid:channelid,msg:msg,roomname:roomname,date:date}, null, 'json')
                                        .done(function(response) {            

                                        })
                                        .fail(function(error) {
                                            alert('error saving data');
                                            console.log('Failed to fetch the Access Token with error: ' + error);
                                        });
                                    }
                                    else {
                                    notifyTyping();
                                    }
                                }

                                var notifyTyping = $.throttle(function() {
                                    tc.currentChannel.typing();
                                }, 1000);

                                tc.handleNewChannelInputKeypress = function(event) {
                                    if (event.keyCode === 13) {
                                    tc.messagingClient.createChannel({
                                        friendlyName: $newChannelInput.val()
                                    }).then(hideAddChannelInput);
                                    $(this).val('');
                                    event.preventDefault();
                                    }
                                };

                                function connectClientWithUsername() {
                                    var usernameText = $usernameInput.val();
                                    $usernameInput.val('');
                                    if (usernameText == '') {
                                        alert('Username cannot be empty');
                                        return;
                                    }
                                    tc.username = usernameText;
                                    fetchAccessToken(tc.username, connectMessagingClient);
                                }

                                function fetchAccessToken(username, handler) {
                                    
                                    $.post("{{url('/token')}}", {identity: username, device: 'browser'}, null, 'json')
                                    .done(function(response) {
                                        handler(response.token);
                                    })
                                    .fail(function(error) {
                                        console.log('Failed to fetch the Access Token with error: ' + error);
                                    });
                                }

                                function connectMessagingClient(token) {
                                    // Initialize the Chat messaging client
                                    
                                    tc.accessManager = new Twilio.AccessManager(token);
                                    Twilio.Chat.Client.create(token).then(function(client) {
                                    tc.messagingClient = client;
                                    updateConnectedUI();
                                    
                                    tc.loadChannelList(tc.joinGeneralChannel);
                                    tc.messagingClient.on('channelAdded', $.throttle(tc.loadChannelList));
                                    tc.messagingClient.on('channelRemoved', $.throttle(tc.loadChannelList));
                                    tc.messagingClient.on('tokenExpired', refreshToken);
                                    });
                                }

                                function refreshToken() {
                                    fetchAccessToken(tc.username, setNewToken);
                                }

                                function setNewToken(tokenResponse) {
                                    tc.accessManager.updateToken(tokenResponse.token);
                                }

                                function updateConnectedUI() {
                                    $('#username-span').text(tc.username);
                                    $statusRow.addClass('connected').removeClass('disconnected');
                                    tc.$messageList.addClass('connected').removeClass('disconnected');
                                    $connectPanel.addClass('connected').removeClass('disconnected');
                                    $inputText.addClass('with-shadow');
                                    $typingRow.addClass('connected').removeClass('disconnected');
                                }

                                tc.loadChannelList = function(handler) {
                                    if (tc.messagingClient === undefined) {
                                    console.log('Client is not initialized');
                                    return;
                                    }

                                    tc.messagingClient.getPublicChannelDescriptors().then(function(channels) {
                                    tc.channelArray = tc.sortChannelsByName(channels.items);
                                    $channelList.text('');
                                    tc.channelArray.forEach(addChannel);
                                    if (typeof handler === 'function') {
                                        handler();
                                    }
                                    });
                                };

                                tc.joinGeneralChannel = function() {
                                    console.log('Attempting to join "general" chat channel...');
                                    if (!tc.generalChannel) {
                                        //If it doesn't exist, let's create it
                                        tc.messagingClient.createChannel({
                                            uniqueName: GENERAL_CHANNEL_UNIQUE_NAME,
                                            friendlyName: GENERAL_CHANNEL_NAME
                                        }).then(function(channel) {

                                            console.log('Created general channel');
                                            tc.generalChannel = channel;
                                            tc.loadChannelList(tc.joinGeneralChannel);
                                        });

                                        
                                    }
                                    else {
                                    console.log('Found general channel:');
                                    setupChannel(tc.generalChannel);
                                    }
                                };

                                function initChannel(channel) {
                                    console.log('Initialized channel ' + channel.friendlyName);
                                    return tc.messagingClient.getChannelBySid(channel.sid);
                                }

                                function joinChannel(_channel) {
                                    
                                        return _channel.join()
                                        .then(function(joinedChannel) {
                                            console.log('Joined channel ' + joinedChannel.friendlyName);
                                            updateChannelUI(_channel);
                                            tc.currentChannel = _channel;
                                            //tc.loadMessages();
                                            return joinedChannel;
                                        });
                                    
                                }

                                function initChannelEvents() {
                                    console.log(tc.currentChannel.friendlyName + ' ready.');
                                    tc.currentChannel.on('messageAdded', tc.addMessageToList);
                                    tc.currentChannel.on('typingStarted', showTypingStarted);
                                    tc.currentChannel.on('typingEnded', hideTypingStarted);
                                    tc.currentChannel.on('memberJoined', notifyMemberJoined);
                                    tc.currentChannel.on('memberLeft', notifyMemberLeft);
                                    $inputText.prop('disabled', false).focus();
                                }

                                function setupChannel(channel) {
                                    return leaveCurrentChannel()
                                    .then(function() {
                                        
                                        return initChannel(channel);
                                    })
                                    .then(function(_channel) {

                                        return joinChannel(_channel);
                                    })
                                    .then(initChannelEvents);
                                }

                                tc.loadMessages = function() {
                                    tc.currentChannel.getMessages(MESSAGES_HISTORY_LIMIT).then(function (messages) {
                                    messages.items.forEach(tc.addMessageToList);
                                    });
                                };

                                function leaveCurrentChannel() {

                                    if (tc.currentChannel) {
                                        
                                    return tc.currentChannel.leave().then(function(leftChannel) {
                                        console.log('left ' + leftChannel.friendlyName);
                                        leftChannel.removeListener('messageAdded', tc.addMessageToList);
                                        leftChannel.removeListener('typingStarted', showTypingStarted);
                                        leftChannel.removeListener('typingEnded', hideTypingStarted);
                                        leftChannel.removeListener('memberJoined', notifyMemberJoined);
                                        leftChannel.removeListener('memberLeft', notifyMemberLeft);
                                    });
                                    } else {
                                        
                                        return Promise.resolve();
                                    }
                                }

                                tc.addMessageToList = function(message) {
                                    
                                    var username = getUsername(message.author);
                                    
                                    if (message.author === tc.username) {

                                        var rowDiv = $('<div>').addClass('kt-chat__message kt-chat__message--right');
                                        rowDiv.loadTemplate($('#message-template-right'), {
                                        username: 'You',
                                        date: dateFormatter.getTodayDate(message.timestamp),
                                        body: message.body
                                        });
                                        rowDiv.addClass('own-message');

                                        
                                    } else {

                                        var rowDiv = $('<div>').addClass('kt-chat__message');
                                        rowDiv.loadTemplate($('#message-template'), {
                                        username: username,
                                        date: dateFormatter.getTodayDate(message.timestamp),
                                        body: message.body
                                        });
                                        rowDiv.addClass('own-message');
                                        
                                    }

                                    tc.$messageList.append(rowDiv);
                                    scrollToMessageListBottom();
                                };

                                function notifyMemberJoined(member) {
                                    notify(member.identity + ' joined the channel')
                                }

                                function notifyMemberLeft(member) {
                                    notify(member.identity + ' left the channel');
                                }

                                function notify(message) {
                                    var row = $('<div>').addClass('col-md-12');
                                    row.loadTemplate('#member-notification-template', {
                                    status: message
                                    });
                                    tc.$messageList.append(row);
                                    scrollToMessageListBottom();
                                }

                                function showTypingStarted(member) {
                                    $typingPlaceholder.text(member.identity + ' is typing...');
                                }

                                function hideTypingStarted(member) {
                                    $typingPlaceholder.text('');
                                }

                                function scrollToMessageListBottom() {
                                    tc.$messageList.scrollTop(tc.$messageList[0].scrollHeight);
                                }

                                function updateChannelUI(selectedChannel) {
                                    var channelElements = $('.channel-element').toArray();
                                    var channelElement = channelElements.filter(function(element) {
                                    return $(element).data().sid === selectedChannel.sid;
                                    });
                                    channelElement = $(channelElement);
                                    if (tc.currentChannelContainer === undefined && selectedChannel.uniqueName === GENERAL_CHANNEL_UNIQUE_NAME) {
                                    tc.currentChannelContainer = channelElement;
                                    }
                                    tc.currentChannelContainer.removeClass('selected-channel').addClass('unselected-channel');
                                    channelElement.removeClass('unselected-channel').addClass('selected-channel');
                                    tc.currentChannelContainer = channelElement;
                                }

                                function showAddChannelInput() {
                                    if (tc.messagingClient) {
                                    $newChannelInputRow.addClass('showing').removeClass('not-showing');
                                    $channelList.addClass('showing').removeClass('not-showing');
                                    $newChannelInput.focus();
                                    }
                                }

                                function hideAddChannelInput() {
                                    $newChannelInputRow.addClass('not-showing').removeClass('showing');
                                    $channelList.addClass('not-showing').removeClass('showing');
                                    $newChannelInput.val('');
                                }

                                function addChannel(channel) {
                                   
                                    if (channel.uniqueName === GENERAL_CHANNEL_UNIQUE_NAME) {
                                        tc.generalChannel = channel;
                                    }
                                   
                                    var rowDiv = $('<div>').addClass('row channel-row');
                                    rowDiv.loadTemplate('#channel-template', {
                                    channelName: channel.friendlyName
                                    });

                                    var channelP = rowDiv.children().children().first();
                                    
                                    rowDiv.on('click', selectChannel);
                                    channelP.data('sid', channel.sid);
                                    if (tc.currentChannel && channel.sid === tc.currentChannel.sid) {
                                    tc.currentChannelContainer = channelP;
                                    channelP.addClass('selected-channel');

                                    }
                                    else {
                                    channelP.addClass('unselected-channel')
                                    }

                                    $channelList.append(rowDiv);
                                }

                                function deleteCurrentChannel() {
                                    if (!tc.currentChannel) {
                                    return;
                                    }
                                    if (tc.currentChannel.sid === tc.generalChannel.sid) {
                                    alert('You cannot delete the general channel');
                                    return;
                                    }
                                    tc.currentChannel.delete().then(function(channel) {
                                    console.log('channel: '+ channel.friendlyName + ' deleted');
                                    setupChannel(tc.generalChannel);
                                    });
                                }

                                function selectChannel(event) {
                                    var target = $(event.target);
                                    var channelSid = target.data().sid;
                                    var selectedChannel = tc.channelArray.filter(function(channel) {
                                    return channel.sid === channelSid;
                                    })[0];
                                    if (selectedChannel === tc.currentChannel) {
                                    return;
                                    }
                                    setupChannel(selectedChannel);
                                };

                                function disconnectClient() {
                                    leaveCurrentChannel();
                                    $channelList.text('');
                                    tc.$messageList.text('');
                                    channels = undefined;
                                    $statusRow.addClass('disconnected').removeClass('connected');
                                    tc.$messageList.addClass('disconnected').removeClass('connected');
                                    $connectPanel.addClass('disconnected').removeClass('connected');
                                    $inputText.removeClass('with-shadow');
                                    $typingRow.addClass('disconnected').removeClass('connected');
                                }

                                tc.sortChannelsByName = function(channels) {
                                    return channels.sort(function(a, b) {
                                    if (a.friendlyName === GENERAL_CHANNEL_NAME) {
                                        return -1;
                                    }
                                    if (b.friendlyName === GENERAL_CHANNEL_NAME) {
                                        return 1;
                                    }
                                    return a.friendlyName.localeCompare(b.friendlyName);
                                    });
                                };

                                return tc;
                                })();

                            </script>
                            <script src="{{ asset('public/js/dateformatter.js') }}"></script>
                            <!-- end:: Content-->


                        </div>
                    </div>

                    @include('layouts.admin.partials._footer')

                </div>
            </div>
        </div>

    
    </body>
</html>
