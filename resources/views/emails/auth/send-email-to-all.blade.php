@extends('emails.layouts.email')

@section('title','Send Emails to all unverified user')
@section('content')

@section('customCSS')

@endsection

<table align="center" class="" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto;padding:10px 20px;    color: #595d6e;">
    @include('emails.includes.brand')    
    <tr>
        <td>
            <div>
                <div>
                    <p style="margin-bottom:10px;padding: 10px 20px;font-size: 13px;font-weight: 300;font-family: Poppins, Helvetica, sans-serif;"> Hello, 
                      <br>
                      <br>
                      We have reprocessed our FlyBiz website, so you are required to activate the same.
                    </p>

                    <div class="text" style="padding: 0 2.5em; text-align: center;">
                        <p style="padding: 10px 20px;">
                            <a href="{{@$link}}" class="btn btn-label-brand btn-bold" style="background-color: rgba(0, 155, 255, 0.1);color: #009bff;border: 1px solid transparent;padding: 0.65rem 1rem;font-size: 1rem;line-height: 1.5;border-radius: 0.25rem;font-weight: 600;text-decoration: none;font-family: Poppins, Helvetica, sans-serif;font-size: 13px;">Click here</a>
                        </p>
                    </div>                                    
                </div>
            </div>
        </td>
    </tr>
</table>
@endsection