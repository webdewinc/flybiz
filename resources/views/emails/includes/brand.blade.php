<tr>
    <td valign="top">
        <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%" style="margin: 0 auto 2rem !important;background: #e5f5ff;">
            <tr>
                <td class="logo" style="text-align: center;padding: 1rem 0 2rem;">
                    <a class="kt-header__brand-logo" href="{{url('/')}}">
                        <img alt="Logo" src="{{ asset('') . config('app.public_url') . '/images/fly-biz-logo.png' }}" width="110px">
                    </a>
                </td>
            </tr>
        </table>
    </td>
</tr>