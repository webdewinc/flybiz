<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700">
<!--   <link href="http://fly.biz/public/assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
<link href="http://fly.biz/public/assets/css/style.bundle.css" rel="stylesheet" type="text/css" /> -->
<style>
/* What it does: Remove spaces around the email design added by some email clients. */
/* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
body {
    background-color: #fff;
}

table,
td {
    mso-table-lspace: 0pt !important;
    mso-table-rspace: 0pt !important;
}

/* What it does: Fixes webkit padding issue. */
table {
    border-spacing: 0 !important;
    border-collapse: collapse !important;
    table-layout: fixed !important;
    margin: 0 auto !important;
}

ul.social {
    padding: 0;
}

ul.social li {
    display: inline-block;
    margin-right: 10px;
}

/*FOOTER*/
.footer ul {
    margin: 0;
    padding: 0;
}

.footer ul li {
    list-style: none;
    margin-bottom: 10px;
}

.footer ul li a {
    color: rgba(0, 0, 0, 1);
}
</style>
