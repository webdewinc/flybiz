<table align="center" class="" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto;padding:10px 20px;    color: #595d6e;">
    <tr>
        <td>
            <div>
                <div>
                    <p style="margin-bottom:10px;padding: 10px 20px;font-size: 13px;font-weight: 300;font-family: Poppins, Helvetica, sans-serif;">
                    Thank you for using our application!</p>
                    <p style="margin-bottom:10px;padding: 10px 20px;font-size: 13px;font-weight: 300;font-family: Poppins, Helvetica, sans-serif;">Regards,<br>{{ config('app.name') }}</p>
                </div>
            </div>
        </td>
    </tr>
</table>