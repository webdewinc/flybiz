<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <title>{{ config('app.name') }} | @yield('title')</title>
    <!-- begin::Head -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="x-apple-disable-message-reformatting">

    <!--begin::Page Vendors Styles(used by this page) -->
    @include('emails.includes.header')
    @yield('customCSS')
</head>
    <body>
        <center style=" background-color: #fff;">
            <div style="max-width: 600px; margin: 0 auto; border: 2px solid #009bff;" class="email-container">
                @yield('content')
                @include('emails.includes.company')
                @include('emails.includes.footer')
            </div>
        </center>
    </body>
    <!-- end::Body -->
</html>