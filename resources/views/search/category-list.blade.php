@extends('layout.app')
@php
$totalTitle = ' Write and Publish a Guest Post Article on category';
@endphp
@section('title', $totalTitle)
@section('content')
@section('mainhead')
@parent
<!-- <link rel="shortcut icon" href="https://www.guestpostengine.com/images/favicon.ico" /> -->
@show

<style>
.checked {
  color: orange;
}
</style>
<div class="kt-body website-details-wrapper kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
            </div>
        </div>
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">
            <!--begin::Portlet-->
            <div class="" data-sticky-container>
                <div class="row position-relative">
                    <div class="col-12 col-lg-12">
                        <div class="kt-portlet ">
                            <div class="kt-portlet__body">
                                @if(!empty($category_name))
                                    <a href="{{url('/search?q=').$category_name}}">{{$category_name}}</a>
                                @endif
                                @foreach($category as $key => $value)
                                    <a href="{{$value['url']}}">{{$value['category']}}</a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
           
        </div>
    </div>
</div>
<!--ENd:: Chat-->
@section('mainscripts')
@parent
@section('customScript')
<!--begin::Page Vendors Styles(used by this page) -->
@endsection

@show
@endsection