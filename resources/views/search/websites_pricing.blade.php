@extends('layout.app')
@php
$totalTitle = ' Looking for reliable website details on GuestPostEngine?';
@endphp
@section('title', $totalTitle)
@section('content')
@section('mainhead')
@parent

@section('title-description')

<meta name="description" content="Get the appropriate website details on GuestPostEngine by exploring a wide range of quality websites with high domain authority, category rank, etc.">
    
@endsection


<!-- <link rel="shortcut icon" href="https://www.guestpostengine.com/images/favicon.ico" /> -->
@show
<!-- Authentication Links --> 
@if(session()->has('message'))
    <?php
        $form = session()->get('form');
        if($form == ''){
            $form = 'signin';
            $result = 1;
            $message = 'Password has been changed.';
            $alert = 'success';
        } else {
            $result = 0;
        }
        
    ?>
@else 
    <?php
        $form = 'signin';
        $result = 0;
    ?>
@endif
<style>
.checked {
  color: orange;
}
</style>
<div class="kt-body website-details-wrapper kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
        <div class="kt-subheader kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
            </div>
        </div>
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">
            <!--begin::Portlet-->
            <div class="" data-sticky-container>
                <div class="row position-relative">
                    <div class="col-12 col-lg-1"></div>
                    <div class="col-12 col-lg-10">
                        <div class="kt-portlet ">
                            <div class="kt-portlet__body">
                                <form class="kt-form kt-login__signin" method="POST" action="{{ url('ajax-cart')  }}" id="setpolicyform">
                                @csrf  
                                    <span id="alert-show-add"></span>
                                    <div class="kt-grid-nav kt-grid-nav--skin-light">
                                        <div class="kt-grid-nav__row">
                                            <a href="javascript:void(0);" class="kt-grid-nav__item active-bg" id="kt-grid-nav__item_buyer" rel="buyer">
                                                <span class="kt-grid-nav__icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <polygon points="0 0 24 0 24 24 0 24" />
                                                            <path d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                                            <path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
                                                        </g>
                                                    </svg>
                                                </span>
                                                <span class="kt-grid-nav__title">Advertiser</span>
                                                <span class="kt-grid-nav__desc">I will Provide Content myself or Hiring Content Writer</span>
                                            </a>
                                            <a href="javascript:void(0);" id="kt-grid-nav__item_seller" class="kt-grid-nav__item" rel="seller">
                                                <span class="kt-grid-nav__icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <polygon points="0 0 24 0 24 24 0 24" />
                                                            <path d="M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z M21,8 L17,8 C16.4477153,8 16,7.55228475 16,7 C16,6.44771525 16.4477153,6 17,6 L21,6 C21.5522847,6 22,6.44771525 22,7 C22,7.55228475 21.5522847,8 21,8 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                                            <path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
                                                        </g>
                                                    </svg>
                                                </span>
                                                <span class="kt-grid-nav__title">Publisher</span>
                                                <span class="kt-grid-nav__desc">I want the Publisher to write Content at Extra Cost of <span style="color: #009bff;"><span class="extra_price_span">${{$list->extra_cost}}</span></span></span>
                                                
                                            </a>
                                             <input type="hidden" id="domain_id" name="domain_id" value="{{$list->domain_id}}">
                                            <input type="hidden" id="seller_id" name="seller_id" value="{{$list->data_assigned_website[0]->user_id}}">
                                            <!-- <input type="hidden" id="cost_price" name="cost_price" value="{{$list->cost_price}}"> -->
                                            <!-- <input type="hidden" id="seller_price" name="seller_price" value="0">
                                            <input type="hidden" id="total" name="total" value="0"> -->
                                            <input type="hidden" name="type_user" id="type_user" value="buyer">
                                            <input type="hidden" name="user_id" value="{{@Auth::user()->user_id}}" id="user_id">
                                            <input type="hidden" id="promo_code_type" name="promo_code_type" value="null" />
                                            <input type="hidden" id="promo_code" name="promo_code" value="0" />
                                            <input type="hidden" id="discount_price" name="discount_price" value="0" />
                                        </div>
                                    </div>
                                    <div class="col-12 kt-mt-40">
                                        <div class="modal_buyer" >
                                            <div class="container kt-padding-20">
                                                <div class="row">
                                                    <div class="form-group col-12">
                                                        <div class="input-group">
                                                            <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ old('title') }}" autocomplete="off" placeholder="Article Title" maxlength="70">
                                                        </div>
                                                        <span class="title-error"></span>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-6">
                                                        <div class="input-group">
                                                            <input id="url" type="text" class="form-control @error('url') is-invalid @enderror" name="url" value="{{ old('url') }}" autocomplete="off" placeholder="Target URL" maxlength="200">
                                                        </div>
                                                        <span class="url-error"></span>
                                                    </div>
                                                    <div class="form-group col-6">
                                                        <div class="input-group">
                                                            <input id="keyword" type="text" class="form-control @error('keyword') is-invalid @enderror" name="keyword" value="{{ old('keyword') }}" autocomplete="off" placeholder="Keyword" maxlength="70">
                                                        </div>
                                                        <span class="keyword-error"></span>
                                                    </div>
                                                </div>                                        
                                            </div>
                                        </div>
                                        <!-- <div class="modal_seller">
                                            <div class="container kt-padding-20">
                                                <div class="row">    
                                                    <div class="col-12 text-center kt-mt-20 d-flex">
                                                        <div class="form-group col-12">
                                                            <div class="input-group">
                                                                <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ old('title') }}" autocomplete="off" placeholder="Title">
                                                            </div>
                                                            <span class="title-error"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 text-center kt-mt-20 d-flex">
                                                        <div class="form-group col-12">
                                                            <div class="input-group lnbrd">
                                                                <textarea name="cktext" id="cktext" ></textarea>
                                                            </div>
                                                            <span class="cktext-error">
                                                                
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>                                        
                                            </div>
                                        </div> -->
                                        
                                        <div class="col-12 text-center kt-mt-20 kt-mb-20 d-flex justify-content-center">
                                            <span id="buyer_wise_price">
                                                <button type="submit" rel="{{@Auth::user()->user_id}}" class="btn btn-label-brand kt-mt-10 kt-mr-10 cart_buyer" data-id="cart">Add to Cart (<span class="total_amount_span_buyer">${{buyerDomainWebsitePriceCalculate(@$list->cost_price)}}</span>)</button>
                                                <button type="submit" rel="{{@Auth::user()->user_id}}" class="btn btn-brand kt-mt-10 cart_buyer" data-id="checkout">Checkout (<span class="total_amount_span_buyer">${{buyerDomainWebsitePriceCalculate(@$list->cost_price)}}</span>)</button>
                                            </span>
                                            <span id="seller_wise_price">
                                                <button type="submit" class="btn btn-label-brand kt-mt-10 kt-mr-10 cart_seller" data-id="cart">Add to Cart (<span class="total_amount_span_seller">{{buyerDomainPriceCalculate($list->cost_price,$list->extra_cost )}}</span>)</button>
                                                <button type="submit" rel="{{@Auth::user()->user_id}}" class="btn btn-brand kt-mt-10 cart_seller" data-id="checkout">Checkout (<span class="total_amount_span_seller">{{buyerDomainPriceCalculate($list->cost_price,$list->extra_cost )}}</span>)</button>
                                            </span>
                                        </div>
                                        <div class="kt-portlet kt-iconbox kt-iconbox--brand kt-iconbox--animate-slow">
                                            <div class="kt-portlet__body">
                                                <div class="kt-iconbox__body">
                                                    <div class="d-flex w-100">
                                                        <div class="kt-iconbox__icon">
                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                                    <path d="M8,3 L8,3.5 C8,4.32842712 8.67157288,5 9.5,5 L14.5,5 C15.3284271,5 16,4.32842712 16,3.5 L16,3 L18,3 C19.1045695,3 20,3.8954305 20,5 L20,21 C20,22.1045695 19.1045695,23 18,23 L6,23 C4.8954305,23 4,22.1045695 4,21 L4,5 C4,3.8954305 4.8954305,3 6,3 L8,3 Z" fill="#000000" opacity="0.3"></path>
                                                                    <path d="M10.875,15.75 C10.6354167,15.75 10.3958333,15.6541667 10.2041667,15.4625 L8.2875,13.5458333 C7.90416667,13.1625 7.90416667,12.5875 8.2875,12.2041667 C8.67083333,11.8208333 9.29375,11.8208333 9.62916667,12.2041667 L10.875,13.45 L14.0375,10.2875 C14.4208333,9.90416667 14.9958333,9.90416667 15.3791667,10.2875 C15.7625,10.6708333 15.7625,11.2458333 15.3791667,11.6291667 L11.5458333,15.4625 C11.3541667,15.6541667 11.1145833,15.75 10.875,15.75 Z" fill="#000000"></path>
                                                                    <path d="M11,2 C11,1.44771525 11.4477153,1 12,1 C12.5522847,1 13,1.44771525 13,2 L14.5,2 C14.7761424,2 15,2.22385763 15,2.5 L15,3.5 C15,3.77614237 14.7761424,4 14.5,4 L9.5,4 C9.22385763,4 9,3.77614237 9,3.5 L9,2.5 C9,2.22385763 9.22385763,2 9.5,2 L11,2 Z" fill="#000000"></path>
                                                                </g>
                                                            </svg>
                                                        </div>
                                                        <div class="kt-iconbox__desc w-100">
                                                            <a href="">
                                                            </a>
                                                            <h3 class="kt-iconbox__title">
                                                                <a href="">
                                                                </a> Hire
                                                            </h3>
                                                            <div class="kt-iconbox__content">Content Writer</div>
                                                        </div>
                                                    </div>
                                                    <div style="width: 100%;">
                                                        <button type="button" class="btn btn-label-brand btn-bold kt-mt-10" style="float: right;">Hire Writer Now</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <!-- end -->
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-1"></div>
                </div>
            </div>
            
            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--ENd:: Chat-->
@section('mainscripts')
@parent
@section('customScript')
<script src="https://cdn.ckeditor.com/4.9.2/standard/ckeditor.js"></script> 
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>      
<!-- Include CKEditor and jQuery adapter -->
<!-- <script src="//cdn.ckeditor.com/4.4.3/basic/ckeditor.js"></script> -->
<!-- <script src="//cdn.ckeditor.com/4.4.3/basic/adapters/jquery.js"></script> -->

<script>  
    CKEDITOR.replace('cktext');
</script>  

<!--begin::Page Scripts(used by this page) -->
<script src="{{ asset('') . config('app.public_url') . '/assets/js/pages/custom/login/login-general.js'  }}" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<!--end::Page Scripts -->
<script type="text/javascript">
$('#seller_wise_price').hide();
$('#buyer_wise_price').show();
$('#code_content').hide();
$('#code_website').hide();
$('#code_buyer_website').on('click',function(e){

    var contentPrice = $(this).attr('data-price');
    
    var promo_code   = $(this).attr('data-promo-number');
    /** calculate percentage **/
    var calcPercent  = $(this).attr('data-percentage');
    var CalcPrice    = contentPrice * calcPercent / 100;

    var pendingPrice = parseInt(contentPrice, 10) - parseInt(CalcPrice, 10);
    $('#code_buyer_website').addClass('active-bg');
    $('#code_content').find('label').removeClass('active-bg');
    $('#code_website').find('label').removeClass('active-bg'); 
    $('.total_amount_span_buyer').html('$'+pendingPrice);
    $('#cost_price').val(pendingPrice);
    $('#promo_code_type').val('advertiser');
    $('#promo_code').val(promo_code);
    $('#discount_price').val(pendingPrice);
    $('.show_mssg').html('<div class="alert alert-primary kt-padding-10 kt-margin-10 " role="alert"><div class="alert-text">Promo Code Applied Successfully!!!</div></div>').delay(2000).fadeOut();
    e.stopImmediatePropagation();
    return false;
});

$(document).on('click','.advertiser', function(){
    $('#kt_modal_switched').modal('show');
});

function add_cart(data_id){
    var user_id = $('#user_id').val();
    if (user_id == '') {
        $('#kt_modal_6').modal('hide');
        $('#kt_modal_login').modal('show');
        return;
    } else {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{ url('ajax-cart') }}",
            type: 'POST',
            async: true,
            dataType: 'json',
            enctype: 'multipart/form-data',
            data: {
                domain_id: $('#domain_id').val(),
                //user_id: $('#user_id').val(),
                type_user: $('#type_user').val(),
                seller_id: $('#seller_id').val(),
                url: $('#url').val(),
                keyword: $('#keyword').val(),
                title: $('#title').val(),
                cktext: $('#cktext').val(),
                //cost_price: $('#cost_price').val(),
                // seller_price: $('#seller_price').val(),
                // total: $('#total').val(),
                promo_code : $('#promo_code').val(),
                promo_code_type : $('#promo_code_type').val(),
                //discount_price : $('#discount_price').val(),
            },
            cache: false,
            success: function(response) {

                if (data_id == 'cart') {
                    
                    if(response.status == false){
                        if(response.errors){
                            if(response.errors.keyword){
                               $('.keyword-error').html('<span class="error" role="alert"><strong>'+response.errors.keyword[0]+'</strong></span>');
                            }
                            if(response.errors.url){
                                
                                $('.url-error').html('<span class="error" role="alert"><strong>'+response.errors.url[0]+'</strong></span>');
                            }
                            if(response.errors.title){                                
                                $('.title-error').html('<span class="error" role="alert"><strong>'+response.errors.title[0]+'</strong></span>');
                            }
                            if(response.errors.cktext){
                                swal.fire({
                                    "title": "",
                                    "text": response.errors.cktext[0],
                                    "type": "danger",
                                    "confirmButtonClass": "btn btn-secondary"
                                });
                                $('.cktext-error').html('<span class="error" role="alert"><strong>'+response.errors.cktext[0]+'</strong></span>');
                            }
                        }
                    }
                    var alert = 'alert-outline-' + response.alert;
                    var html_message = response.html;
                    var message = response.message;
                    var alert = '<div class="alert ' + alert + ' fade show" role="alert"><div class="alert-icon"><i class="flaticon-warning"></i></div><div class="alert-text message">' + response.message + '</div><div class="alert-close"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="la la-close"></i></span></button></div></div>';
                    $('#alert-show-add').html(alert);
                    
                    $('#partials-profile').html(html_message);
                    KTUtil.scrollTop();

                    //window.location.href = "{{url('/search/website/')}}"+'/'+"{{$domain_url}}";

                } else {
                    window.location.href = "{{url('/advertiser/cart')}}";
                }
            },
            error: function() {}
        });


    }
}
    $(document).on('click','.cart_seller',function(e) {

        var user_id = '{{@Auth::user()->user_id}}';
        if (user_id == '') {
            $('#kt_modal_6').modal('hide');
            $('#kt_modal_login').modal('show');
            return;
        }

        var data_id = $(this).attr('data-id');
        
        e.preventDefault();
        var btn = $(this);
        var form = $(this).closest('form');
        form.validate({
            // Validate only visible fields
            ignore: [],
            debug: false,
            rules: {
                url: {
                    required: true,
                    maxlength:200,
                    url: '/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/'
                },
                keyword: {
                    required: true,
                    maxlength:70
                },
                title: {
                    required: true,
                    maxlength:70,
                },
                // cktext:{
                //     required: function() {
                //         CKEDITOR.instances.cktext.updateElement();
                //     },
                //     minlength:10
                // }
                // cktext: {  
                //     //ckrequired: true //CKEDITORustom required field   
                //     ckrequired: true, //CKEDITORustom required field   
                //     maxlength:"{{$list->number_words}}"
                // }
            },

            messages: {
                // cktext:{
                //     //required:"Please enter Text",
                //     minlength:"Please enter 10 characters"
                // }
            },
            onkeyup: true,
            onfocusout: true,
            onclick: true
        });


        if (!form.valid()) {
            return;
        }
        //form.submit();
        
        add_cart(data_id);
        e.stopImmediatePropagation();
        return false;
    });
    $(document).on('click','.cart_buyer',function(e) {

        
        var user_id = '{{@Auth::user()->user_id}}';
        if (user_id == '') {
            $('#kt_modal_6').modal('hide');
            $('#kt_modal_login').modal('show');
            return;
        }
        var data_id = $(this).attr('data-id');
        e.preventDefault();
        var btn = $(this);
        var form = $(this).closest('form');
        form.validate({
            // Validate only visible fields
            ignore: ":hidden",
            rules: {
                url: {
                    required: true,
                    maxlength:200,
                    url: '/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/'
                },
                keyword: {
                    required: true,
                    maxlength:200
                }
            },
            onkeyup: true,
            onfocusout: true,
            onclick: true
        });

        if (!form.valid()) {
            return;
        }

        
        add_cart(data_id);
        e.stopImmediatePropagation();
        return false;
    });


        //Extention method for check CKEditor Control   
        //jQuery.validator.addMethod("customfunctionanme",validationfunction,validationmessage);  
  
        jQuery.validator.addMethod("ckrequired", function (value, element) {  
            var number_words = "{{$list->number_words}}";
            var idname = $(element).attr('id');  
            var editor = CKEDITOR.instances[idname];  
            var ckValue = GetTextFromHtml(editor.getData()).replace(/<[^>]*>/gi, '').trim();
            if (ckValue.length === 0) {  
                //if empty or trimmed value then remove extra spacing to current control  
                $(element).val(ckValue);  
            } else {  
                //If not empty then leave the value as it is

                if(ckValue.length <= number_words){
                    $(element).val(editor.getData());    
                } else {
                    alert('ss');
                }
                
            }  
            return $(element).val().length > 0;  
        }, "This field is required");  
  
        function GetTextFromHtml(html) {  
            var dv = document.createElement("DIV");  
            dv.innerHTML = html;  
            return dv.textContent || dv.innerText || "";  
        }  


$(document).on('click', '.kt-grid-nav__item', function(e) {
    var user_val = $(this).attr('rel');
    
    $('.modal_seller').hide();

    if(user_val == 'buyer'){
        //$('.modal_seller').slideToggle();    
        $('#title').attr('required');
    }
    

    if (user_val == 'buyer') {
        var no_show = 'seller';
        $('#code_buyer_website').show();
        $('#code_content').hide();
        $('#code_website').hide();
        /** apply code for buyer **/
         $('#code_buyer_website').on('click',function(e){

            var contentPrice = $(this).attr('data-price');

            var promo_code   = $(this).attr('data-promo-number');
            // /** calculate percentage **/
            var calcPercent  = $(this).attr('data-percentage');
            var CalcPrice    = contentPrice * calcPercent / 100;
            var pendingPrice = parseInt(contentPrice, 10) - parseInt(CalcPrice, 10); 
            $('#code_buyer_website').addClass('active-bg');
            $('#code_content').find('label').removeClass('active-bg');
            $('#code_website').find('label').removeClass('active-bg');  
            $('.total_amount_span_buyer').html('$'+pendingPrice);
            $('#cost_price').val(pendingPrice);
            $('#promo_code_type').val('advertiser');
            $('#promo_code').val(promo_code);
            $('#discount_price').val(pendingPrice);
            $('.show_mssg').html('<div class="alert alert-primary kt-padding-10 kt-margin-10 " role="alert"><div class="alert-text">Promo Code Applied Successfully!!!</div></div>').delay(2000).fadeOut();

            e.stopImmediatePropagation();
            return false;
         });      
    }
    if (user_val == 'seller') {
        var no_show = 'buyer';
        $('#code_buyer_website').hide();
        $('#code_content').show();
        $('#code_website').show();
        /** apply code for seller **/
        $(document).on('click','#code_content',function(e){
            var actual_price = $('#code_website').attr('data-price');
           var contentPrice = $(this).attr('data-price');
           if(contentPrice != ''){
                actual_price = parseInt(Number(actual_price), 10) + parseInt(Number(contentPrice), 10) ;
           }
           
            var promo_code   = $(this).attr('data-promo-number');
            /** calculate percentage **/
            var calcPercent  = $(this).attr('data-percentage');
            var CalcPrice    = contentPrice * calcPercent / 100;
            
            var pendingPrice = parseInt(actual_price, 10) - parseInt(CalcPrice, 10);
           $('#code_buyer_website').removeClass('active-bg');
           $('#code_content').find('label').addClass('active-bg');
           $('#code_website').find('label').removeClass('active-bg');  
           $('.total_amount_span_seller').html('$'+pendingPrice);
           $('#cost_price').val(pendingPrice);
           $('#promo_code_type').val('publisher');
           $('#promo_code').val(promo_code);
           $('#discount_price').val(pendingPrice);
           $('.show_mssg_1').html('<div class="alert alert-primary kt-padding-10 kt-margin-10 " role="alert"><div class="alert-text">Promo Code Applied Successfully!!!</div></div>').delay(2000).fadeOut();

            e.stopImmediatePropagation();
            return false;
       });
       $(document).on('click','#code_website',function(e){ 
            $('#code_buyer_website').hide();
            $('#code_content').show();
            $('#code_website').show();    
            var contentPrice = $(this).attr('data-price');
             var promo_code  = $(this).attr('data-promo-number');
            /** calculate percentage **/
            var calcPercent  = $(this).attr('data-percentage');
            var CalcPrice    = contentPrice * calcPercent / 100;
            var pendingPrice = parseInt(contentPrice, 10) - parseInt(CalcPrice, 10);

            $('#code_buyer_website').removeClass('active-bg');
            $('#code_content').find('label').removeClass('active-bg');
            $('#code_website').find('label').addClass('active-bg');
            $('.total_amount_span_seller').html('$'+pendingPrice);
            $('#cost_price').val(pendingPrice);
            $('#promo_code_type').val('publisher');
            $('#promo_code').val(promo_code);
            $('#discount_price').val(pendingPrice);
            $('.show_mssg_2').html('<div class="alert alert-primary kt-padding-10 kt-margin-10 " role="alert"><div class="alert-text">Promo Code Applied Successfully!!!</div></div>').delay(2000).fadeOut();
             e.stopImmediatePropagation();
            return false;
       });   
    }
    $('#type_user').val(user_val);
    $('#kt-grid-nav__item_' + no_show).removeClass('active-bg');
    $('#kt-grid-nav__item_' + user_val).addClass('active-bg');

    $('#' + no_show + '_wise_price').hide();
    $('#' + user_val + '_wise_price').show();

});


</script>

@endsection

@show
@endsection