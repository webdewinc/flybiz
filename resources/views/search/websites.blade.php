@extends('layout.app')
@php
$totalTitle = ' Write and Publish a Guest Post Article on ' . $domain_url;
@endphp
@section('title', $totalTitle)
@section('content')
@section('mainhead')
@parent


<!-- <link rel="shortcut icon" href="https://www.guestpostengine.com/images/favicon.ico" /> -->
@show
<!-- Authentication Links --> 
@if(session()->has('message'))
    <?php
        $form = session()->get('form');
        if($form == ''){
            $form = 'signin';
            $result = 1;
            $message = 'Password has been changed.';
            $alert = 'success';
        } else {
            $result = 0;
        }
        
    ?>
@else 
    <?php
        $form = 'signin';
        $result = 0;
    ?>
@endif
<style>
.checked {
  color: orange;
}
</style>
<div class="kt-body website-details-wrapper kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
            </div>
        </div>
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">
            <!--begin::Portlet-->
            <div class="" data-sticky-container>
                <div class="row position-relative">
                    <div class="col-12 col-lg-7">
                        <div class="kt-portlet ">
                            <div class="kt-portlet__body">
                                @include('search.partials.websiteDetails')
                            </div>
                        </div>
                        <div class="kt-portlet ">
                            <div class="kt-portlet__body">
                                <div class="kt-widget kt-widget--user-profile-3">
                                    <div class="kt-widget__top">
                                        <div class="kt-widget__content">
                                            <div class="kt-widget__head">
                                                <div class="d-flex align-items-center">
                                                    <div class="kt-widget__media kt-hidden-">
                                                        <img width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon favicon-svg" src="<?php echo 'https://www.google.com/s2/favicons?domain='. $domain_url; ?>" />
                                                    </div>

                                                    <!-- <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon favicon-svg kt-mr-5">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"></rect>
                                                            <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="9"></circle>
                                                            <path d="M11.7357634,20.9961946 C6.88740052,20.8563914 3,16.8821712 3,12 C3,11.9168367 3.00112797,11.8339369 3.00336944,11.751315 C3.66233009,11.8143341 4.85636818,11.9573854 4.91262842,12.4204038 C4.9904938,13.0609191 4.91262842,13.8615942 5.45804656,14.101772 C6.00346469,14.3419498 6.15931561,13.1409372 6.6267482,13.4612567 C7.09418079,13.7815761 8.34086797,14.0899175 8.34086797,14.6562185 C8.34086797,15.222396 8.10715168,16.1034596 8.34086797,16.2636193 C8.57458427,16.423779 9.5089688,17.54465 9.50920913,17.7048097 C9.50956962,17.8649694 9.83857487,18.6793513 9.74040201,18.9906563 C9.65905192,19.2487394 9.24857641,20.0501554 8.85059781,20.4145589 C9.75315358,20.7620621 10.7235846,20.9657742 11.7357634,20.9960544 L11.7357634,20.9961946 Z M8.28272988,3.80112099 C9.4158415,3.28656421 10.6744554,3 12,3 C15.5114513,3 18.5532143,5.01097452 20.0364482,7.94408274 C20.069657,8.72412177 20.0638332,9.39135321 20.2361262,9.6327358 C21.1131932,10.8600506 18.0995147,11.7043158 18.5573343,13.5605384 C18.7589671,14.3794892 16.5527814,14.1196773 16.0139722,14.886394 C15.4748026,15.6527403 14.1574598,15.137809 13.8520064,14.9904917 C13.546553,14.8431744 12.3766497,15.3341497 12.4789081,14.4995164 C12.5805657,13.664636 13.2922889,13.6156126 14.0555619,13.2719546 C14.8184743,12.928667 15.9189236,11.7871741 15.3781918,11.6380045 C12.8323064,10.9362407 11.963771,8.47852395 11.963771,8.47852395 C11.8110443,8.44901109 11.8493762,6.74109366 11.1883616,6.69207022 C10.5267462,6.64279981 10.170464,6.88841096 9.20435656,6.69207022 C8.23764828,6.49572949 8.44144409,5.85743687 8.2887174,4.48255778 C8.25453994,4.17415686 8.25619136,3.95717082 8.28272988,3.80112099 Z M20.9991771,11.8770357 C20.9997251,11.9179585 21,11.9589471 21,12 C21,16.9406923 17.0188468,20.9515364 12.0895088,20.9995641 C16.970233,20.9503326 20.9337111,16.888438 20.9991771,11.8770357 Z" fill="#000000" opacity="0.3"></path>
                                                        </g>
                                                    </svg> -->
                                                    <a href="{{url('search/website/')}}{{'/'.$domain_url}}" class="kt-widget__username">
                                                        {{$domain_url}} 
                                                    </a>
                                                </div>
                                            </div>
                                            <!-- no review  -->
                                            <?php
                                            foreach( $selectQueryWebsite as $split_website ){
                                                    $domain_id  = $split_website['domain_id'];
                                                    $domain_url = $split_website['domain_url'];

                                                    $review_lists = DB::table('reviews')->where('domain_url',$domain_url)->where('type','website')->get()->toArray();
                                                    $count_lists  = DB::table('reviews')->where('domain_url',$domain_url)->where('type','website')->count();
                                                        $calc_rating = 0;
                                                        foreach( $review_lists as $split_review_lists ){
                                                            if( $split_review_lists->type == 'website'){
                                                                $total_rec = count($review_lists);
                                                                $calc_rating += $split_review_lists->rating;
                                                            }
                                                        }
                                                        if($count_lists == 1){

                                                            $averageStar = $calc_rating;

                                                        }else{
                                                            $averageStar = round($calc_rating / 5);
                                                        }
                                            ?>
                                            <div class="kt-mb-5 d-flex">
                                                <div class=" kt-ml-10 kt-mt-5">
                                                    <div class="d-flex website_reviews">
                                                        @if(empty($averageStar))
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        @elseif( $averageStar == 1)
                                                        <div class="kt-demo-icon__preview checked">
                                                            <i class="fas fa-star" data-val ="1"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star" data-val ="2"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star" data-val ="3"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star" data-val ="4"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star" data-val ="5"></i>
                                                        </div>
                                                        @elseif( $averageStar == 2)
                                                        <div class="kt-demo-icon__preview checked">
                                                            <i class="fas fa-star" data-val ="1"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview checked">
                                                            <i class="fas fa-star" data-val ="2"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star" data-val ="3"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star" data-val ="4"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star" data-val ="5"></i>
                                                        </div>
                                                        @elseif( $averageStar == 3 )
                                                        <div class="kt-demo-icon__preview checked">
                                                            <i class="fas fa-star" data-val ="1"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview checked">
                                                            <i class="fas fa-star" data-val ="2"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview checked">
                                                            <i class="fas fa-star" data-val = "3"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star" data-val ="4"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star" data-val ="5"></i>
                                                        </div>
                                                        @elseif( $averageStar == 4)
                                                        <div class="kt-demo-icon__preview checked">
                                                            <i class="fas fa-star" data-val ="1"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview checked">
                                                            <i class="fas fa-star" data-val ="2"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview checked">
                                                            <i class="fas fa-star" data-val = "3"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview checked">
                                                            <i class="fas fa-star" data-val = "4"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star" data-val ="5"></i>
                                                        </div>
                                                        @elseif( $averageStar == 5)
                                                        <div class="kt-demo-icon__preview checked">
                                                            <i class="fas fa-star" data-val ="1"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview checked">
                                                            <i class="fas fa-star" data-val ="2"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview checked">
                                                            <i class="fas fa-star" data-val = "3"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview checked">
                                                            <i class="fas fa-star" data-val = "4"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview checked">
                                                            <i class="fas fa-star" data-val = "5"></i>
                                                        </div>
                                                        @endif
                                                    </div>
                                                    @if(empty($averageStar))
                                                    <span style="
                                                        display: flex;
                                                        font-size: 0.9rem;
                                                        color: #74788d;
                                                        font-weight: normal;
                                                        " class="kt-widget4__text">
                                                        No Reviews
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <?php } ?>
                                            <!-- no review  -->
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="kt-widget kt-widget--user-profile-3">
                                    <div class="kt-widget__top align-items-center review-list">
                                        <div class="kt-widget__media kt-hidden-">
                                            <img src="https://keenthemes.com/metronic/themes/metronic/theme/default/demo9/dist/assets/media/users/100_5.jpg" alt="image" style="
                                                width: 80px;
                                                border-radius: 4px;
                                                ">
                                        </div>
                                        <div class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-font-light kt-hidden">
                                            MP
                                        </div>
                                        <div class="kt-widget__content kt-ml-20 d-flex" style="
                                            justify-content: space-between;
                                            ">
                                            <div>
                                                <div class="kt-widget__head">
                                                    <a href="#" class="kt-widget__username">
                                                    Matt Pears
                                                    </a>
                                                </div>
                                                <div class="kt-widget__info">
                                                    <div class="kt-widget__desc" style="
                                                        font-weight: 500;
                                                        ">"The best cost-efficient SEO"</div>
                                                </div>
                                            </div>
                                            <div class="kt-widget__action d-flex" style="
                                                font-size: 1.rem;
                                                font-weight: 500;
                                                ">
                                                <i class="flaticon-confetti kt-font-brand kt-mr-15" style="
                                                    font-size: 2.8rem;
                                                    "></i>
                                                <div>
                                                    <div class="kt-mb-5 d-flex">
                                                        <span style="
                                                            width: 120px;
                                                            color: #009bff;
                                                            ">Backlink Impact:</span>
                                                        <div class=" kt-ml-10">
                                                            <div class="d-flex">
                                                                <div class="kt-demo-icon__preview">
                                                                    <i class="la la-star"></i>
                                                                </div>
                                                                <div class="kt-demo-icon__preview">
                                                                    <i class="la la-star"></i>
                                                                </div>
                                                                <div class="kt-demo-icon__preview">
                                                                    <i class="la la-star"></i>
                                                                </div>
                                                                <div class="kt-demo-icon__preview">
                                                                    <i class="la la-star"></i>
                                                                </div>
                                                                <div class="kt-demo-icon__preview">
                                                                    <i class="la la-star"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div style="
                                                        display: flex;
                                                        ">
                                                        <span style="
                                                            width: 120px;
                                                            color: #009bff;
                                                            ">Traffic Impact:</span>
                                                        <div class=" kt-ml-10">
                                                            <div class="d-flex">
                                                                <div class="kt-demo-icon__preview">
                                                                    <i class="la la-star"></i>
                                                                </div>
                                                                <div class="kt-demo-icon__preview">
                                                                    <i class="la la-star"></i>
                                                                </div>
                                                                <div class="kt-demo-icon__preview">
                                                                    <i class="la la-star"></i>
                                                                </div>
                                                                <div class="kt-demo-icon__preview">
                                                                    <i class="la la-star"></i>
                                                                </div>
                                                                <div class="kt-demo-icon__preview">
                                                                    <i class="la la-star"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-widget__top align-items-center review-list">
                                        <div class="kt-widget__media kt-hidden">
                                            <img src="/metronic/themes/metronic/theme/default/demo9/dist/assets/media/users/100_1.jpg" alt="image">
                                        </div>
                                        <div class="kt-widget__pic kt-widget__pic--primary kt-font-primary kt-font-boldest kt-font-light kt-hidden-" style="
                                            max-width: 80px;
                                            height: 80px;
                                            ">
                                            MP
                                        </div>
                                        <div class="kt-widget__content kt-ml-20 d-flex" style="
                                            justify-content: space-between;
                                            ">
                                            <div>
                                                <div class="kt-widget__head">
                                                    <a href="#" class="kt-widget__username">
                                                    Matt Pears
                                                    </a>
                                                </div>
                                                <div class="kt-widget__info">
                                                    <div class="kt-widget__desc" style="
                                                        font-weight: 500;
                                                        ">"The best cost-efficient SEO"</div>
                                                </div>
                                            </div>
                                            <div class="kt-widget__action d-flex" style="
                                                font-size: 1.rem;
                                                font-weight: 500;
                                                ">
                                                <i class="flaticon-confetti kt-font-brand kt-mr-15" style="
                                                    font-size: 2.8rem;
                                                    "></i>
                                                <div>
                                                    <div class="kt-mb-5 d-flex">
                                                        <span style="
                                                            width: 120px;
                                                            color: #009bff;
                                                            ">Backlink Impact:</span>
                                                        <div class=" kt-ml-10">
                                                            <div class="d-flex">
                                                                <div class="kt-demo-icon__preview">
                                                                    <i class="la la-star"></i>
                                                                </div>
                                                                <div class="kt-demo-icon__preview">
                                                                    <i class="la la-star"></i>
                                                                </div>
                                                                <div class="kt-demo-icon__preview">
                                                                    <i class="la la-star"></i>
                                                                </div>
                                                                <div class="kt-demo-icon__preview">
                                                                    <i class="la la-star"></i>
                                                                </div>
                                                                <div class="kt-demo-icon__preview">
                                                                    <i class="la la-star"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div style="
                                                        display: flex;
                                                        ">
                                                        <span style="
                                                            width: 120px;
                                                            color: #009bff;
                                                            ">Traffic Impact:</span>
                                                        <div class=" kt-ml-10">
                                                            <div class="d-flex">
                                                                <div class="kt-demo-icon__preview">
                                                                    <i class="la la-star"></i>
                                                                </div>
                                                                <div class="kt-demo-icon__preview">
                                                                    <i class="la la-star"></i>
                                                                </div>
                                                                <div class="kt-demo-icon__preview">
                                                                    <i class="la la-star"></i>
                                                                </div>
                                                                <div class="kt-demo-icon__preview">
                                                                    <i class="la la-star"></i>
                                                                </div>
                                                                <div class="kt-demo-icon__preview">
                                                                    <i class="la la-star"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>   -->
                            </div>
                        </div>
                    </div>
                    <!-- loop user_list -->
                    <div class="col-12 col-lg-5">
                        <div class="sticky" data-sticky="true" data-margin-top="100px" data-sticky-for="1023" data-sticky-class="kt-sticky" style="">
                            <div class="accordion accordion-solid accordion-panel accordion-toggle-svg" id="accordionExample8">
                                @include('search.partials.websiteUserList')
                            </div>
                        </div>
                    </div>
                    <!-- loop - user_list end -->
                    <!-- single review start-->
                    <div class="col-12 col-lg-7">
                        
                    <!-- single review end-->
                </div>
            </div>
            <!--begin::Modal-->
            <div class="modal" id="kt_modal_6" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header  text-center">
                            <h5 class="modal-title" id="exampleModalLabel">Who can Provide the Content?</h5>
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon" data-dismiss="modal" aria-label="Close" style="cursor:pointer;">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#000000">
                                        <rect x="0" y="7" width="16" height="2" rx="1"></rect>
                                        <rect opacity="0.3" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000) " x="0" y="7" width="16" height="2" rx="1"></rect>
                                    </g>
                                </g>
                            </svg>
                            </button>
                        </div>
                        <div class="modal-body">
                            <span id="alert-show-add"></span>
                            <div class="kt-grid-nav kt-grid-nav--skin-light">
                                <div class="kt-grid-nav__row">
                                    <a href="javascript:void(0);" class="kt-grid-nav__item active-bg" id="kt-grid-nav__item_buyer" rel="buyer">
                                        <span class="kt-grid-nav__icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <polygon points="0 0 24 0 24 24 0 24" />
                                                    <path d="M18,8 L16,8 C15.4477153,8 15,7.55228475 15,7 C15,6.44771525 15.4477153,6 16,6 L18,6 L18,4 C18,3.44771525 18.4477153,3 19,3 C19.5522847,3 20,3.44771525 20,4 L20,6 L22,6 C22.5522847,6 23,6.44771525 23,7 C23,7.55228475 22.5522847,8 22,8 L20,8 L20,10 C20,10.5522847 19.5522847,11 19,11 C18.4477153,11 18,10.5522847 18,10 L18,8 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                                    <path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
                                                </g>
                                            </svg>
                                        </span>
                                        <span class="kt-grid-nav__title">Advertiser</span>
                                        <span class="kt-grid-nav__desc">I will Provide Content myself or Hiring Content Writer</span>
                                    </a>
                                    <a href="javascript:void(0);" id="kt-grid-nav__item_seller" class="kt-grid-nav__item" rel="seller">
                                        <span class="kt-grid-nav__icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <polygon points="0 0 24 0 24 24 0 24" />
                                                    <path d="M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z M21,8 L17,8 C16.4477153,8 16,7.55228475 16,7 C16,6.44771525 16.4477153,6 17,6 L21,6 C21.5522847,6 22,6.44771525 22,7 C22,7.55228475 21.5522847,8 21,8 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                                    <path d="M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
                                                </g>
                                            </svg>
                                        </span>
                                        <span class="kt-grid-nav__title">Publisher</span>
                                        <span class="kt-grid-nav__desc">I want the Publisher to write Content at Extra Cost of <span style="color: #009bff;"><span class="extra_price_span"></span></span></span>
                                    </a>
                                     <input type="hidden" id="domain_id" name="domain_id" value="">
                                    <input type="hidden" id="seller_id" name="seller_id" value="">
                                    <input type="hidden" id="cost_price" name="cost_price" value="">
                                    <input type="hidden" id="seller_price" name="seller_price" value="0">
                                    <input type="hidden" id="total" name="total" value="0">
                                    <input type="hidden" name="type_user" id="type_user" value="buyer">
                                    <input type="hidden" name="user_id" value="{{@Auth::user()->user_id}}" id="user_id">
                                    <input type="hidden" id="promo_code_type" name="promo_code_type" value="null" />
                                    <input type="hidden" id="promo_code" name="promo_code" value="0" />
                                    <input type="hidden" id="discount_price" name="discount_price" value="0" />
                                </div>
                            </div>
                            @foreach( $selectQueryWebsite as $getWebsiteLists )
                            <?php 

                                // print_r($getWebsiteLists);
                                // die;
                            ?>
                            @php
                            $getDataContent = DB::table('promo_codes')->where('promo_id',$getWebsiteLists['promo_code_content'])->get()->toArray();
                            $getDataWebsite = DB::table('promo_codes')->where('promo_id',$getWebsiteLists['promo_code_website'])->get()->toArray();
                            @endphp
                                <div class="kt-mt-20">
                                    <div class="row w-100 row-no-margin">
                                        @if(count($getDataContent) > 0)
                                        <div class="col-md-6 col-12" id="code_content" data-price="{{$getWebsiteLists['cost_price']}}" data-promo-number = "{{ $getDataContent[0]->promo_id }}" data-percentage="{{ $getDataContent[0]->promo_number }}">                                         
                                            <label class="kt-option">
                                                <!--<span class="kt-option__control">
                                                <span class="kt-radio kt-radio--bold kt-radio--brand">
                                                <input type="radio" name="m_option_1">
                                                <span></span>
                                                </span>
                                                </span>-->
                                                <span class="kt-option__label">
                                                <span class="kt-option__body kt-padding-0">Apply Promo Code</span>
                                                    <span class="kt-option__head">
                                                <span class="kt-option__focus">{{$getDataContent[0]->promo_number }}% Off with Content</span>
                                                                                                 
                                                </span>
                                                
                                                </span> 
                                            </label>
                                        </div>
                                        @endif
                                        @if(count($getDataWebsite) > 0)
                                            <div class="col-md-6 col-12" id="code_website" data-price="{{$getWebsiteLists['cost_price']}}" data-promo-number = "{{ $getDataWebsite[0]->promo_id }}"  data-percentage="{{ $getDataWebsite[0]->promo_number }}">
                                                <label class="kt-option">
                                                    <!--<span class="kt-option__control">
                                                    <span class="kt-radio kt-radio--bold kt-radio--brand">
                                                    <input type="radio" name="m_option_1">
                                                    <span></span>
                                                    </span>
                                                    </span>-->
                                                    <span class="kt-option__label">
                                                    <span class="kt-option__body kt-padding-0">Apply Promo Code</span><span class="kt-option__head">
                                                    
                                                    <span class="kt-option__focus">{{$getDataWebsite[0]->promo_number }}% Off with Website</span>                                                
                                                    </span>
                                                    
                                                    </span>     
                                                </label>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="row w-100 row-no-margin">
                                        @if(count($getDataWebsite) > 0)                             
                                            <div class="col-md-6 col-12">                                               
                                                <label id="code_buyer_website" class="kt-option"  data-price="{{$getWebsiteLists['cost_price']}}" data-promo-number = "{{ $getDataWebsite[0]->promo_id }}"  data-percentage="{{ $getDataWebsite[0]->promo_number }}">
                                                    <!-- <span class="kt-option__control">
                                                    <span class="kt-radio kt-radio--bold kt-radio--brand">
                                                    <input type="radio" name="m_option_1">
                                                    <span></span>
                                                    </span>
                                                    </span> -->
                                                    <span class="kt-option__label">
                                                        <span class="kt-option__body kt-padding-0">Apply Promo Code</span><span class="kt-option__head">
                                                        
                                                        <span class="kt-option__focus">{{$getDataWebsite[0]->promo_number }}% Off with Website</span>                                                
                                                        </span>
                                                        
                                                        </span> 
                                                </label>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            
                            @endforeach
                            <span class="show_mssg"></span>
                            <span class="show_mssg_1"></span>
                            <span class="show_mssg_2"></span>

                            <div class="col-12 text-center kt-mt-20 d-flex justify-content-center">
                                <span id="buyer_wise_price">
                                    <a href="javascript:void(0);" rel="{{@Auth::user()->user_id}}" class="btn btn-label-brand kt-mt-10 kt-mr-10 add_to_cart" data-id="cart">Add to Cart (<span class="total_amount_span_buyer"></span>)</a>
                                    <a href="javascript:void(0);" rel="{{@Auth::user()->user_id}}" class="btn btn-brand kt-mt-10 add_to_cart" data-id="checkout">Checkout (<span class="total_amount_span_buyer"></span>)</a>
                                </span>
                                <span id="seller_wise_price">
                                    <a href="javascript:void(0);" rel="{{@Auth::user()->user_id}}" class="btn btn-label-brand kt-mt-10 kt-mr-10 add_to_cart" data-id="cart">Add to Cart (<span class="total_amount_span_seller"></span>)</a>
                                    <a href="javascript:void(0);" rel="{{@Auth::user()->user_id}}" class="btn btn-brand kt-mt-10 add_to_cart" data-id="checkout">Checkout (<span class="total_amount_span_seller"></span>)</a>
                                </span>
                            </div>
                            <div class="col-12 kt-mt-40">
                                <div class="kt-portlet kt-iconbox kt-iconbox--brand kt-iconbox--animate-slow">
                                    <div class="kt-portlet__body">
                                        <div class="kt-iconbox__body">
                                            <div class="d-flex w-100">
                                                <div class="kt-iconbox__icon">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"></rect>
                                                            <path d="M8,3 L8,3.5 C8,4.32842712 8.67157288,5 9.5,5 L14.5,5 C15.3284271,5 16,4.32842712 16,3.5 L16,3 L18,3 C19.1045695,3 20,3.8954305 20,5 L20,21 C20,22.1045695 19.1045695,23 18,23 L6,23 C4.8954305,23 4,22.1045695 4,21 L4,5 C4,3.8954305 4.8954305,3 6,3 L8,3 Z" fill="#000000" opacity="0.3"></path>
                                                            <path d="M10.875,15.75 C10.6354167,15.75 10.3958333,15.6541667 10.2041667,15.4625 L8.2875,13.5458333 C7.90416667,13.1625 7.90416667,12.5875 8.2875,12.2041667 C8.67083333,11.8208333 9.29375,11.8208333 9.62916667,12.2041667 L10.875,13.45 L14.0375,10.2875 C14.4208333,9.90416667 14.9958333,9.90416667 15.3791667,10.2875 C15.7625,10.6708333 15.7625,11.2458333 15.3791667,11.6291667 L11.5458333,15.4625 C11.3541667,15.6541667 11.1145833,15.75 10.875,15.75 Z" fill="#000000"></path>
                                                            <path d="M11,2 C11,1.44771525 11.4477153,1 12,1 C12.5522847,1 13,1.44771525 13,2 L14.5,2 C14.7761424,2 15,2.22385763 15,2.5 L15,3.5 C15,3.77614237 14.7761424,4 14.5,4 L9.5,4 C9.22385763,4 9,3.77614237 9,3.5 L9,2.5 C9,2.22385763 9.22385763,2 9.5,2 L11,2 Z" fill="#000000"></path>
                                                        </g>
                                                    </svg>
                                                </div>
                                                <div class="kt-iconbox__desc w-100">
                                                    <a href="">
                                                    </a>
                                                    <h3 class="kt-iconbox__title">
                                                        <a href="">
                                                        </a> Hire
                                                    </h3>
                                                    <div class="kt-iconbox__content">Content Writer</div>
                                                </div>
                                            </div>
                                            <div style="width: 100%;">
                                                <button type="button" class="btn btn-label-brand btn-bold kt-mt-10" style="float: right;">Hire Writer Now</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Modal-->
            <!--begin::Modal-->
            <div class="modal fade" id="kt_modal_login" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header text-center kt-pb-0">
                            <ul class="nav nav-tabs nav-tabs-line kt-mb-0 w-100" role="tablist">
                                <li class="nav-item flex-grow-1">
                                    <a class="nav-link active" data-toggle="tab" href="#kt_tabs_1_1" role="tab" aria-selected="true">Sign In</a>
                                </li>
                                <li class="nav-item flex-grow-1">
                                    <a class="nav-link" data-toggle="tab" href="#kt_tabs_1_3" role="tab" aria-selected="false">Sign Up</a>
                                </li>
                            </ul>
                            
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon" data-dismiss="modal" aria-label="Close" style="cursor:pointer;">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#000000">
                                        <rect x="0" y="7" width="16" height="2" rx="1"></rect>
                                        <rect opacity="0.3" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000) " x="0" y="7" width="16" height="2" rx="1"></rect>
                                    </g>
                                </g>
                            </svg>
                            </button>
                        </div>
                        <div class="modal-body">
                            
                            <div class="kt-portlet__body">
                                <div class="tab-content kt-login kt-login--v6 kt-login--{{$form}}" id="kt_login">
                                    <div class="tab-pane active" id="kt_tabs_1_1" role="tabpanel">
                                        <span id="alert-show"></span>
                                        <!-- <form class="kt-form" method="POST" action="{{ route('login-ajax') }}"> -->
                                        <form class="kt-form kt-login__signin" method="POST" action="{{ url('/websiteLogin ') }}">
                                            @csrf
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-envelope"></i></span></div>
                                                    <input id="user_email" type="email" class="form-control @error('user_email') is-invalid @enderror" name="user_email" value="{{ old('user_email') }}" required autocomplete="email" autofocus placeholder="Email">
                                                </div>
                                                <span class="form-text text-muted">We'll never share your email with anyone else.</span>
                                            </div>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-lock"></i></span></div>
                                                   
                                                    <input id="password_login" type="password" class="form-control @error('password_login') is-invalid @enderror" name="password_login" required autocomplete="password" placeholder="Password">
                                                </div>
                                            </div>
                                            <div class="kt-login__extra  d-flex justify-content-between kt-mb-10">
                                                <label class="kt-checkbox">
                                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}> Remember me
                                                    <span></span>
                                                </label>
                                                
                                                @if (Route::has('password.request'))
                                                    <a class="kt-link kt-mb-5" href="{{ route('password.request') }}"  id="kt_login_forgot">
                                                        {{ __('Forget Password ?') }}
                                                    </a>
                                                @endif

                                            </div>
                                            <div class="modal-footer kt-padding-0 kt-padding-t-15">
                                                <a type="javascript:void(0);" id="kt_logins" class="btn btn-label-brand btn-bold">
                                                    {{ __('Sign In') }}
                                                </a>
                                            </div>
                                        </form>
                                        <div class="kt-login__forgot">
                                            <div class="kt-login__head">                                             
                                                <div class="kt-login__desc">Enter your email to reset your password.</div>
                                            </div>
                                            <div class="kt-login__form">
                                                @if(session()->has('message'))
                                                    @if(session()->get('form') == 'forgot')
                                                    <div class="alert alert-outline-{{ session()->get('alert') }} fade show" role="alert">
                                                        <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                                        <div class="alert-text">{{ session()->get('message') }}</div>
                                                        <div class="alert-close">
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                <span aria-hidden="true"><i class="la la-close"></i></span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    @endif
                                                @endif
                                                <form class="kt-form" method="POST" action="{{ route('password.email') }}">
                                                    @csrf
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-prepend"><span class="input-group-text"><i class="la la-envelope"></i></span></div>                                           
                                                            <input maxlength="100"  autocomplete="off" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required placeholder="Email">
                                                        </div> 
                                                        
                                                        @if ($errors->has('email'))
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $errors->first('email') }}</strong>
                                                                </span>
                                                            @endif 
                                                    </div>
                                                    <div class="modal-footer kt-padding-0 kt-padding-t-15">
                                                        <button type="submit" id="kt_login_forgot_pass" class="btn btn-label-brand btn-bold">
                                                            {{ __('Reset Password') }}
                                                        </button>
                                                        <button id="kt_login_forgot_cancel" class="btn btn-label-brand btn-bold">Cancel</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="kt_tabs_1_3" role="tabpanel">
                                        <span id="alert-show-register"></span>
                                        <form method="POST" class="kt-form" action="{{ url('/websiteRegister') }}">
                                            @csrf
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-user"></i></span></div>
                                                    <input id="user_fname" name="user_fname" type="text" class="form-control @error('user_fname') is-invalid @enderror" value="{{ old('user_fname') }}" required autocomplete="user_fname" autofocus placeholder="First Name">
                                                </div>
                                                @error('user_fname')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-user"></i></span></div>
                                                    <input id="user_lname" type="text" name="user_lname" class="form-control @error('user_lname') is-invalid @enderror" name="user_lname" value="{{ old('user_lname') }}" required autocomplete="user_lname" autofocus placeholder="Last Name">
                                                </div>
                                                @error('user_lname')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend"><span class="input-group-text">@</span></div>
                                                    <input id="user_code" type="text" class="form-control @error('user_code') is-invalid @enderror" name="user_code" value="{{ old('user_code') }}" required autocomplete="user_code" autofocus placeholder="Username">
                                                </div>
                                                @error('user_code')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-envelope"></i></span></div>
                                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Email">
                                                </div>
                                                @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend"><span class="input-group-text"><i class="la la-lock"></i></span></div>
                                                
                                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="password" placeholder="Password">
                                            </div>
                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend"><span class="input-group-text"><i class="la la-lock"></i></span></div>
                                                
                                                <input id="password_confirm" type="password" class="form-control" name="password_confirm" required autocomplete="password_confirm" placeholder="Confirm Password">

                                            </div>
                                        </div>
                                            <div class="kt-login__extra">
                                                <label class="kt-checkbox">
                                                    <input type="checkbox" name="agree"><span></span> I Agree the <a href="https://help.fly.biz/terms-of-service" target="_blank" class="kt-link">terms and conditions</a>.
                                                    <span></span>
                                                </label>
                                            </div>
                                            <div class="modal-footer kt-padding-0 kt-padding-t-15">
                                                <button class="btn btn-label-brand btn-bold" id="kt_register">Sign Up</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Modal--> 


            <!--begin::Modal-->
            <div class="modal fade" id="kt_modal_switched" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header text-center kt-pb-0">
                            <h4>Switched</h4>
                        </div>
                        <div class="modal-body">
                            <span id="show_message"></span>
                            <div class="kt-portlet__body">
                                <div class="tab-content">
                                    <div class="kt-widget kt-widget--user-profile-3">
                                        <div class="kt-widget__top">
                                            <div class="kt-widget__content">
                                                <div class="kt-widget__head">
                                                    <div class="d-flex align-items-center">
                                                        Please change type Publisher to Advertiser.</br>                       
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer kt-padding-0 kt-padding-t-15">
                            <a class="btn btn-label-brand btn-bold" href="{{url('switched-other')}}">Advertiser</a>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Modal-->       
            
            
            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--ENd:: Chat-->
@section('mainscripts')
@parent
@section('customScript')
<!--begin::Page Vendors Styles(used by this page) -->
<script href="{{ asset('') . config('app.public_url') . '/assets/js/pages/components/extended/sticky-panels.js'  }}" type="text/javascript"></script>
<!-- <script src="{{ asset('') . config('app.public_url') . '/assets/js/pages/dashboard.js' }}" type="text/javascript"></script>  -->


<script type="text/javascript">
"use strict";

// Class definition
var KTDashboard = function() {

    // Trends Stats.
    // Based on Chartjs plugin - http://www.chartjs.org/
    var latestTrendsMap = function() {
        if ($('#kt_chart_latest_trends_map').length == 0) {
            return;
        }

        try {
            var map = new GMaps({
                div: '#kt_chart_latest_trends_map',
                lat: -12.043333,
                lng: -77.028333
            });
        } catch (e) {
            console.log(e);
        }
    }

    // Revenue Change.
    // Based on Morris plugin - http://morrisjs.github.io/morris.js/
    var revenueChange = function() {

        var data = <?php echo json_encode($array['cnf']); ?>;
        
        if ($('#kt_chart_revenue_change').length == 0) {
            return;
        }

        Morris.Donut({
            element: 'kt_chart_revenue_change',
            data: data,
            colors: [
                KTApp.getStateColor('success'),
                KTApp.getStateColor('danger'),
                KTApp.getStateColor('light'),
                KTApp.getStateColor('warning'),
                KTApp.getStateColor('dark'),
                KTApp.getStateColor('primary')

            ],
        });
    }

    var revenueChange1 = function() {
        var data = <?php echo json_encode($array['org']); ?>;

        if ($('#kt_chart_revenue_change1').length == 0) {
            return;
        }

        Morris.Donut({
            element: 'kt_chart_revenue_change1',
            data: data,
            colors: [
                KTApp.getStateColor('primary'),
                KTApp.getStateColor('danger'),
                KTApp.getStateColor('light'),
                KTApp.getStateColor('warning'),
                KTApp.getStateColor('dark'),
                KTApp.getStateColor('success')

            ],
        });
    }

    var revenueChange2 = function() {
        var data = <?php echo json_encode($array['org_search']); ?>;
        if ($('#kt_chart_revenue_change2').length == 0) {
            return;
        }

        Morris.Donut({
            element: 'kt_chart_revenue_change2',
            data: data,
            colors: [
                KTApp.getStateColor('success'),
                KTApp.getStateColor('primary')

            ],
        });
    }

    var socialchart = function() {

        var data = <?php echo json_encode($array['org_social']); ?>;        
        
        if ($('#socialchart').length == 0) {
            return;
        }

        Morris.Donut({
            element: 'socialchart',
           data: data,
            colors: [
                KTApp.getStateColor('facebook'),
                KTApp.getStateColor('youtube'),
                KTApp.getStateColor('whatsapp'),
                KTApp.getStateColor('twitter'),
                KTApp.getStateColor('linkedin'),
                KTApp.getStateColor('pinterest')

            ],
        });
    }

    var referralchart = function() {
        var data = <?php echo json_encode($array['org_referral']); ?>;
        if ($('#referralchart').length == 0) {
            return;
        }

        Morris.Donut({
            element: 'referralchart',
            data: data,
            colors: [
                KTApp.getStateColor('primary'),
                KTApp.getStateColor('success'),


            ],
        });
    }

    // Support Tickets Chart.              

    return {
        // Init demos
        init: function() {
            // init charts

            revenueChange();
            revenueChange1();
            revenueChange2();
            socialchart();
            referralchart();


            // demo loading
            var loading = new KTDialog({ 'type': 'loader', 'placement': 'top center', 'message': 'Loading ...' });
            loading.show();

            setTimeout(function() {
                loading.hide();
            }, 3000);
        }
    };
}();

// Class initialization on page load
$(document).ready(function() {
    KTDashboard.init();
});
</script>
<!--begin::Page Scripts(used by this page) -->
<script src="{{ asset('') . config('app.public_url') . '/assets/js/pages/custom/login/login-general.js'  }}" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<!--end::Page Scripts -->
<script type="text/javascript">
$('#seller_wise_price').hide();
$('#buyer_wise_price').show();

// $(document).on('click', '.continue', function() {

//     var domain_id = $(this).attr('data-domain-id');
    
//     var domain_user_id = $(this).attr('data-domain-seller-id');
//     var domain_price = $(this).attr('data-domain-price');
//     var domain_extra_cost = $(this).attr('data-domain-extra-cost');
//     var domain_content = $(this).attr('data-domain-content');
//     var total_domain_price = $(this).attr('data-grand-total-with-extra-cost');
//     var total_domain_price_without = $(this).attr('data-grand-total-without-extra-cost');
//     var total = Number(total_domain_price_without);
//     var total_buyer = Number(total_domain_price);

//     $('#domain_id').val(domain_id);
//     $('#seller_id').val(domain_user_id);
//     $('#cost_price').val(domain_price);
//     $('#seller_price').val(domain_extra_cost);
//     $('#seller_content').val(domain_content);
//     $('#cost_price_span').text('$' + domain_price);
//     $('.extra_price_span').text('$' + domain_extra_cost);
//     $('#number_words_span').text(domain_content);
//     $('#total').val(total);
//     $('.total_amount_span_seller').text('$' + total_buyer);
//     $('.total_amount_span_buyer').text('$' + total);

//     $('#code_content').attr('data-price',domain_extra_cost);
//     $('#code_buyer_website').attr('data-price',total);
//     $('#code_website').attr('data-price',total);

//     $('#kt_modal_6').modal('show');
// });

$(document).on('click', '.redirect-continue', function() {
    var user_id = "{{@Auth::user()->user_id}}";
    if (user_id == '') {
        $('#kt_modal_6').modal('hide');
        $('#kt_modal_login').modal('show');
        return;
    } else {
        var domain_id = $(this).attr('data-domain-id');
        var domain_url = $(this).attr('data-domain-url');
        window.location.href = "{{url('/search/website')}}"+'/'+domain_url+'/'+domain_id;
    }
    
});


$(document).on('click', '.chat', function() {
    $('#kt_modal_login').modal('show');
});


$('#code_content').hide();
$('#code_website').hide();
$('#code_buyer_website').on('click',function(e){

    var contentPrice = $(this).attr('data-price');
    
    var promo_code   = $(this).attr('data-promo-number');
    /** calculate percentage **/
    var calcPercent  = $(this).attr('data-percentage');
    var CalcPrice    = contentPrice * calcPercent / 100;

    var pendingPrice = parseInt(contentPrice, 10) - parseInt(CalcPrice, 10);
    $('#code_buyer_website').addClass('active-bg');
    $('#code_content').find('label').removeClass('active-bg');
    $('#code_website').find('label').removeClass('active-bg'); 
    $('.total_amount_span_buyer').html('$'+pendingPrice);
    $('#cost_price').val(pendingPrice);
    $('#promo_code_type').val('advertiser');
    $('#promo_code').val(promo_code);
    $('#discount_price').val(pendingPrice);
    $('.show_mssg').html('<div class="alert alert-primary kt-padding-10 kt-margin-10 " role="alert"><div class="alert-text">Promo Code Applied Successfully!!!</div></div>').delay(2000).fadeOut();
    e.stopImmediatePropagation();
    return false;
});   
$(document).on('click', '.kt-grid-nav__item', function(e) {
    var user_val = $(this).attr('rel');
    if (user_val == 'buyer') {
        var no_show = 'seller';
        $('#code_buyer_website').show();
        $('#code_content').hide();
        $('#code_website').hide();
        /** apply code for buyer **/
         $('#code_buyer_website').on('click',function(e){

            var contentPrice = $(this).attr('data-price');

            var promo_code   = $(this).attr('data-promo-number');
            // /** calculate percentage **/
            var calcPercent  = $(this).attr('data-percentage');
            var CalcPrice    = contentPrice * calcPercent / 100;
            var pendingPrice = parseInt(contentPrice, 10) - parseInt(CalcPrice, 10); 
            $('#code_buyer_website').addClass('active-bg');
            $('#code_content').find('label').removeClass('active-bg');
            $('#code_website').find('label').removeClass('active-bg');  
            $('.total_amount_span_buyer').html('$'+pendingPrice);
            $('#cost_price').val(pendingPrice);
            $('#promo_code_type').val('advertiser');
            $('#promo_code').val(promo_code);
            $('#discount_price').val(pendingPrice);
            $('.show_mssg').html('<div class="alert alert-primary kt-padding-10 kt-margin-10 " role="alert"><div class="alert-text">Promo Code Applied Successfully!!!</div></div>').delay(2000).fadeOut();

            e.stopImmediatePropagation();
            return false;
         });      
    }
    if (user_val == 'seller') {
        var no_show = 'buyer';
        $('#code_buyer_website').hide();
        $('#code_content').show();
        $('#code_website').show();
        /** apply code for seller **/
        $(document).on('click','#code_content',function(e){
            var actual_price = $('#code_website').attr('data-price');
           var contentPrice = $(this).attr('data-price');
           if(contentPrice != ''){
                actual_price = parseInt(Number(actual_price), 10) + parseInt(Number(contentPrice), 10) ;
           }
           
            var promo_code   = $(this).attr('data-promo-number');
            /** calculate percentage **/
            var calcPercent  = $(this).attr('data-percentage');
            var CalcPrice    = contentPrice * calcPercent / 100;
            
            var pendingPrice = parseInt(actual_price, 10) - parseInt(CalcPrice, 10);
           $('#code_buyer_website').removeClass('active-bg');
           $('#code_content').find('label').addClass('active-bg');
           $('#code_website').find('label').removeClass('active-bg');  
           $('.total_amount_span_seller').html('$'+pendingPrice);
           $('#cost_price').val(pendingPrice);
           $('#promo_code_type').val('publisher');
           $('#promo_code').val(promo_code);
           $('#discount_price').val(pendingPrice);
           $('.show_mssg_1').html('<div class="alert alert-primary kt-padding-10 kt-margin-10 " role="alert"><div class="alert-text">Promo Code Applied Successfully!!!</div></div>').delay(2000).fadeOut();

            e.stopImmediatePropagation();
            return false;
       });
       $(document).on('click','#code_website',function(e){ 
            $('#code_buyer_website').hide();
            $('#code_content').show();
            $('#code_website').show();    
            var contentPrice = $(this).attr('data-price');
             var promo_code  = $(this).attr('data-promo-number');
            /** calculate percentage **/
            var calcPercent  = $(this).attr('data-percentage');
            var CalcPrice    = contentPrice * calcPercent / 100;
            var pendingPrice = parseInt(contentPrice, 10) - parseInt(CalcPrice, 10);

            $('#code_buyer_website').removeClass('active-bg');
            $('#code_content').find('label').removeClass('active-bg');
            $('#code_website').find('label').addClass('active-bg');
            $('.total_amount_span_seller').html('$'+pendingPrice);
            $('#cost_price').val(pendingPrice);
            $('#promo_code_type').val('publisher');
            $('#promo_code').val(promo_code);
            $('#discount_price').val(pendingPrice);
            $('.show_mssg_2').html('<div class="alert alert-primary kt-padding-10 kt-margin-10 " role="alert"><div class="alert-text">Promo Code Applied Successfully!!!</div></div>').delay(2000).fadeOut();
             e.stopImmediatePropagation();
            return false;
       });   
    }
    $('#type_user').val(user_val);
    $('#kt-grid-nav__item_' + no_show).removeClass('active-bg');
    $('#kt-grid-nav__item_' + user_val).addClass('active-bg');

    $('#' + no_show + '_wise_price').hide();
    $('#' + user_val + '_wise_price').show();

});
// $(document).on('click', '.kt-grid-nav__item', function() {
//     var user_val = $(this).attr('rel');

//     if (user_val == 'buyer') {
//         var no_show = 'seller';
//     }
//     if (user_val == 'seller') {
//         var no_show = 'buyer';
//     }
//     $('#type_user').val(user_val);
//     $('#kt-grid-nav__item_' + no_show).removeClass('active-bg');
//     $('#kt-grid-nav__item_' + user_val).addClass('active-bg');

//     $('#' + no_show + '_wise_price').hide();
//     $('#' + user_val + '_wise_price').show();

// });

$(document).on('click','.advertiser', function(){
    $('#kt_modal_switched').modal('show');
    // var url = "{{url('switched-other')}}";
    // var html = '<a href="'+url+'">Advertiser</a>';
    // //advertiser
    // swal.fire({
    //     "title": "Change Type",
    //     "text": "Please change type Publisher to Advertiser " + html,
    //     "type": "error",
    //     "confirmButtonClass": "btn btn-secondary"
    // });
});
$('.add_to_cart').click(function(e) {
    e.preventDefault();
    // $('#kt_modal_6').modal('hide');
    // $(".loader").fadeIn("slow");
    var user_id = $('#user_id').val();
    var data_id = $(this).attr('data-id');

    if (user_id == '') {
        $('#kt_modal_6').modal('hide');
        $('#kt_modal_login').modal('show');
        //$(".loader").fadeOut("slow");
        return;
    } else {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{ url('ajax-cart') }}",
            type: 'POST',
            async: true,
            dataType: 'json',
            enctype: 'multipart/form-data',
            data: {
                domain_id: $('#domain_id').val(),
                user_id: $('#user_id').val(),
                type_user: $('#type_user').val(),
                seller_id: $('#seller_id').val(),
                cost_price: $('#cost_price').val(),
                seller_price: $('#seller_price').val(),
                total: $('#total').val(),
                promo_code : $('#promo_code').val(),
                promo_code_type : $('#promo_code_type').val(),
                discount_price : $('#discount_price').val(),
            },
            cache: false,
            success: function(response) {

                if (data_id == 'cart') {
                    var alert = 'alert-outline-' + response.alert;
                    var html_message = response.html;
                    var message = response.message;
                    var alert = '<div class="alert ' + alert + ' fade show" role="alert"><div class="alert-icon"><i class="flaticon-warning"></i></div><div class="alert-text message">' + response.message + '</div><div class="alert-close"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="la la-close"></i></span></button></div></div>';
                    $('#alert-show-add').html(alert);
                    
                    $('#partials-profile').html(html_message);
                } else {
                    window.location.href = "{{url('/advertiser/cart')}}";
                }
                //$(".loader").fadeOut("slow");


            },
            error: function() {}
        });
        e.stopImmediatePropagation();
        return false;

    }

});

$('#kt_logins').click(function(e) {
    //$(".loader").fadeIn("slow");
    e.preventDefault();
    var btn = $(this);
    var form = $(this).closest('form');

    form.validate({
        rules: {
            user_email: {
                required: true,
                email: true
            },
            password_login: {
                required: true
            }
        },
        messages :{
                    user_email: {
                        required: 'Please enter a valid email address.',
                        email: true
                    },
                    password_login: 'Please enter a valid password.'
                }
    });

    if (!form.valid()) {
        return;
    }

    btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "{{ url('/login-ajax') }}",
        type: 'POST',
        async: true,
        dataType: 'json',
        enctype: 'multipart/form-data',
        cache: false,
        data: {
            user_email: $('#user_email').val(),
            password_login: $('#password_login').val()
        },
        success: function(response) {

            if (response.status === false) {
                var alert = 'alert-outline-' + response.alert;
                var message = response.message;
                var alert = '<div class="alert ' + alert + ' fade show" role="alert"><div class="alert-icon"><i class="flaticon-warning"></i></div><div class="alert-text message">' + response.message + '</div><div class="alert-close"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="la la-close"></i></span></button></div></div>';
                $('#alert-show').html(alert);
                btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);

            } else {
                $('#user_id').val(response.user_id);
                $('#partials-profile').html(response.html);
                $('#kt_modal_login').modal('hide');
                window.location.href = "{{url('search/website/')}}" + '/' + "{{$domain_url}}";
                //$('#kt_modal_6').modal('show');
            }
        },
        error: function() {}
    });
    e.stopImmediatePropagation();
    return false;

});

$('#kt_register').on('click',function(e) {
  e.preventDefault();

  var btn = $(this);
  var form = $(this).closest('form');

    form.validate({
      rules: {
          user_code: {
              required: true,
              remote: {
                  url: "{{url('varifyusercode')}}",
                  type: "GET",
                  data: {
                      action: function () {
                          return "1";
                      },
                  }
              }
          },
          email: {
              required: true,
              email: true,                        
              remote: {
                  url: "{{url('varifyemail')}}",
                  type: "GET",
                  data: {
                      action: function () {
                          return "1";
                      },
                  }
              }  
              
          },
          '#password': {
              required: true,
          },
          password_confirm: {
              required: true,
              equalTo: "#password"
          },
          agree: {
              required: true
          },
          user_fname: {
              required: true
          },
          user_lname: {
              required: true
          }
      },
      messages: {
          email: {
              required: 'Please enter a valid email address.',
              remote: "Email id already registred"
          },
          user_code: {
              required: 'Please enter a valid username.',
              remote: 'Already Exist!! Please choose another username.'
          },
          user_fname: {
            required: 'Please enter a valid first name.',
            remote: 'Please enter a valid first name.'
        },
        user_lname: {
            required: 'Please enter a valid last name.',
            remote: 'Please enter a valid last name.'
        },
          password_confirm: {
              required: 'Please enter a valid confirm password.',
              remote: 'Password does not match.'
          },
          password: {
              required: 'Please enter a valid password.',
              remote: 'Please enter a valid password.'
          },
          agree: {
              required: 'Please accept the terms & conditions.',
          },
      },
    });

    if (!form.valid()) {
        return;
    }
    else {

        btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
        
       $.ajax({
             headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              url  : "{{ url('/websiteRegister') }}",
              type : 'POST',
              data:  {
                'user_fname': $('#user_fname').val(),
                'user_lname': $('#user_lname').val(),
                'user_code' : $('#user_code').val(),
                'email'     : $('#email').val(),
                'password'  : $('#password').val(),
              },
              async: true,
              dataType: 'json',
              enctype: 'multipart/form-data',
              cache: false, 
              success:function(response){
                if(response.success == true){
                    var alert = 'alert-outline-' + response.alert;
                    var message = response.message;
                    var alert = '<div class="alert ' + alert + ' fade show" role="alert"><div class="alert-icon"><i class="flaticon-warning"></i></div><div class="alert-text message">' + response.message + '</div><div class="alert-close"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="la la-close"></i></span></button></div></div>';
                    $('#alert-show-register').html(alert);
                    btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                }
                else{
                    if(response.errors.user_code){
                        var user_code = response.errors.user_code[0];
                        $('#user_code').addClass('is-invalid');
                        //$('#user_code').attr('aria-invalid', 'true');
                        $('#user_code').after('<div id="user_code-error" class="error invalid-feedback">Already Exist!! Please choose another username.</div>');

                    }
                    if(response.errors.email){
                        var email = response.errors.email[0];
                        $('#email').addClass('is-invalid');
                        //$('#email').attr('aria-invalid', 'true');
                        $('#email').after('<div id="user_code-error" class="error invalid-feedback">Email id already registred.</div>');
                    }
                    var alert = 'alert-outline-danger';
                    var message = response.message;
                    var alert = '<div class="alert ' + alert + ' fade show" role="alert"><div class="alert-icon"><i class="flaticon-warning"></i></div><div class="alert-text message">' + response.message + '</div><div class="alert-close"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="la la-close"></i></span></button></div></div>';
                    $('#alert-show-register').html(alert);
                    btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                }
              },    
              error:function(errorThrown){
                console.log(errorThrown);
              }
            
       });
        //form.submit();
        e.stopImmediatePropagation();
        return false; 
    }
});
$('#kt_login_forgot_pass').click(function(e) {
  e.preventDefault();

  var btn = $(this);
  var form = $(this).closest('form');

  form.validate({
      rules: {
          email: {
              required: true,
              email: true,
              remote: {
                  url: "{{url('varifyemailForgot')}}",
                  type: "GET",
                  data: {
                      action: function () {
                          return "1";
                      },
                  }
              }
          }
      },
      messages: {
          email:{
              required: 'Please enter a valid email address.',
              remote: "Please enter a valid/existing email address."
          }
      }
  });

  if (!form.valid()) {
      return;
  }

  //btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);
  form.submit();
});
</script>
<!--end::Page Scripts -->
<script>
   $(document).on('click','.website_reviews .fa-star',function(e){
        var starVal = $(this).attr('data-val');
        if(starVal == 1){
            $('.website_reviews .fa-star:nth-child(2)').addClass('checked');
        }
    e.stopImmediatePropagation();
    return false;         
   });
</script>
@endsection

@show
@endsection