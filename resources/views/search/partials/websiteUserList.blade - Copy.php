@foreach($selectQueryUserList as $ke => $split_Query) 
                                  
    @foreach ($split_Query->data_assigned_website as $key => $value)       

        @if(!empty($value->user_id))

            @if($ke==0)
                @php
                    $show = 'show';
                @endphp
            @else
                @php
                    $show = '';
                @endphp
            @endif
            <div class="card">
                <div class="card-header" id="headingOne{{$ke}}">
                    <div class="card-title">
                        <div class="kt-widget4 w-100">
                            <div class="kt-widget4__item w-100">
                                <div class="d-flex collapsed flex-grow-1 align-items-center" data-toggle="collapse" data-target="#collapseOne{{$ke}}" aria-expanded="false" aria-controls="collapseOne8">
                                    @php
                                        $image = @$value->image;
                                        $name  = '';
                                        $lname = '';
                                    @endphp
                                    @if(!empty($image))
                                        @php
                                            $image = url('storage/app/') . '/' . $image;
                                        @endphp
                                    @else
                                        @php
                                            $name  = strtoupper($rest = substr($value->data_username->user_fname, 0, 1));
                                            $lname = strtoupper($rest = substr($value->data_username->user_lname, 0, 1));
                                        @endphp
                                    @endif
                                    @if(!empty($image))
                                        <div class="kt-widget4__pic kt-widget4__pic--pic">
                                            <img src="{{$image}}" alt="">           
                                        </div>
                                    @else
                                        @php
                                            $image = asset('') . config('app.public_url') .'/assets/media/users/default.jpg';
                                        @endphp
                                        @if(!empty($name))
                                            <div class="kt-widget4__pic kt-widget__pic--primary kt-font-primary kt-font-boldest kt-font-light">
                                            <span class="kt-badge kt-badge--username kt-badge--unified-primary kt-badge--lg kt-badge--rounded kt-badge--bold">
                                            {{$name}}{{$lname}}</span>
                                                
                                            </div>
                                        @else
                                            <div class="kt-widget4__pic kt-widget4__pic--pic">
                                                <img src="{{$image}}" alt="">           
                                            </div>

                                        @endif
                                    @endif
                                    <div class="kt-widget4__info flex-grow-0">
                                        
                                        <a href="{{url('profile').'/'.Crypt::encrypt($value->data_username->user_id)}}" class="kt-widget4__username">
                                            {{@$value->data_username->user_fname}} {{@$value->data_username->user_lname}}
                                        </a>
                                        <p class="kt-widget4__text">
                                            </p>
                                    </div>
                                    
                                        <div class="d-flex flex-column flex-grow-1 align-items-center">
                                            {{--
                                            <div class="d-flex">
                                                <div class="kt-demo-icon__preview">
                                                    <i class="la la-star"></i>
                                                </div>
                                                <div class="kt-demo-icon__preview">
                                                    <i class="la la-star"></i>
                                                </div>
                                                <div class="kt-demo-icon__preview">
                                                    <i class="la la-star"></i>
                                                </div>
                                                <div class="kt-demo-icon__preview">
                                                    <i class="la la-star"></i>
                                                </div>
                                                <div class="kt-demo-icon__preview">
                                                    <i class="la la-star"></i>
                                                </div>
                                            </div>
                                            <!-- <span style="
                                                display: flex;
                                                font-size: 0.9rem;
                                                color: #74788d;
                                                font-weight: normal;
                                                " class="kt-widget4__text">
                                                4.9 
                                                <p style="
                                                    margin: 0;
                                                    margin-left: 0.2rem;
                                                    "> (100 reviews)</p>
                                            </span> -->
                                            <span style="
                                                display: flex;
                                                font-size: 0.9rem;
                                                color: #74788d;
                                                font-weight: normal;
                                                " class="kt-widget4__text">
                                                No Reviews 
                                            </span>
                                            --}}
                                            <!-- no review  -->
                                           <!--  <div class="kt-mb-5 d-flex">                            
                                                <div class=" kt-ml-10">
                                                    <div class="d-flex">
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> -->
                                            <!-- no review  -->

                                        </div>
                                        
                                </div>
                                 
                                    <a href="javascript:void(0)" class="btn btn-brand btn-upper continue" data-domain-id="{{$split_Query->domain_id}}" data-domain-seller-id="{{$value->user_id}}" data-domain-price="{{$split_Query->cost_price}}"  data-domain-extra-cost="{{$split_Query->extra_cost}}" data-domain-content="{{$split_Query->number_words}}" data-grand-total-with-extra-cost="{{buyerDomainPriceCalculate($split_Query->cost_price)}}">Continue
                                    (${{buyerDomainPriceCalculate(@$split_Query->cost_price, $split_Query->extra_cost)}})</a>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div id="collapseOne{{$ke}}" class="collapse {{$show}}" aria-labelledby="headingOne{{$ke}}" data-parent="#accordionExample8" style="">
                    <div class="card-body">
                        <div class="kt-widget kt-widget--user-profile-2">
                            <div class="kt-widget__body">
                                <div class="kt-widget__item" style="
                                    display: none;
                                    ">
                                    <div class="kt-widget__contact">
                                        <span class="kt-widget__label kt-pr10">SUCCESS RATE:</span>
                                        <a href="javascript:void(0);" class="kt-widget__data">90%</a>
                                    </div>
                                    <div class="kt-widget__contact">
                                        <span class="kt-widget__label kt-pr10"> MAXIMUM NO BACKLINKS:</span>
                                        <a href="javascript:void(0);" class="kt-widget__data"> @if($split_Query->backlinks)
                                                    @if($split_Query->backlinks == 1)
                                                        {{$split_Query->backlinks.' Backlink'}}
                                                    @else 
                                                        {{$split_Query->backlinks.' Backlinks'}}
                                                    @endif
                                                @else
                                                    {{'0 Backlink'}}
                                                @endif
                                            </a>
                                    </div>
                                    <div class="kt-widget__contact">
                                        <span class="kt-widget__label kt-pr10">ORDERS IN QUEUE:</span>
                                        <a href="javascript:void(0);" class="kt-widget__data">
                                            {{$split_Query->data_add_to_cart->count()}}
                                        </a>
                                    </div>
                                    <div class="kt-widget__contact">
                                        <span class="kt-widget__label kt-pr10">TURNAROUND TIME:</span>
                                        <a href="javascript:void(0);" class="kt-widget__data">
                                            {{str_replace("_"," ",$split_Query->turnaround_time)}}
                                        </a>
                                    </div>
                                    <div class="kt-widget__contact">
                                        <span class="kt-widget__label kt-pr10">CONTENT PROVIDER OPTIONS:</span>
                                        <a href="javascript:void(0);" class="kt-widget__data"></a>
                                    </div>
                                    <div class="kt-widget__contact">
                                        <span class="kt-widget__label kt-pr10">GUEST POSTS EXAMPLES:</span>
                                        <a href="javascript:void(0);" class="kt-widget__data">500 words ($1)</a>
                                    </div>
                                    <div class="kt-widget__contact">
                                        <span class="kt-widget__label kt-pr10">MORE OPTIONS:</span>
                                        <a href="javascript:void(0);" class="kt-widget__data">500 words ($1)</a>
                                    </div>
                                </div>
                                <div class="kt-notification">
                                    <a href="#" class="kt-notification__item">
                                        <div class="kt-notification__item-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <path d="M9,10 L9,19 L10.1525987,19.3841996 C11.3761964,19.7920655 12.6575468,20 13.9473319,20 L17.5405883,20 C18.9706314,20 20.2018758,18.990621 20.4823303,17.5883484 L21.231529,13.8423552 C21.5564648,12.217676 20.5028146,10.6372006 18.8781353,10.3122648 C18.6189212,10.260422 18.353992,10.2430672 18.0902299,10.2606513 L14.5,10.5 L14.8641964,6.49383981 C14.9326895,5.74041495 14.3774427,5.07411874 13.6240179,5.00562558 C13.5827848,5.00187712 13.5414031,5 13.5,5 L13.5,5 C12.5694044,5 11.7070439,5.48826024 11.2282564,6.28623939 L9,10 Z" fill="#000000"></path>
                                                    <rect fill="#000000" opacity="0.3" x="2" y="9" width="5" height="11" rx="1"></rect>
                                                </g>
                                            </svg>
                                        </div>
                                        @foreach( $selectQueryWebsite as $split_data )
                                            @php
                                                @$domain_id = $split_data['domain_id'];
                                                @$cartData = DB::table('add_to_carts')->where('domain_id', $domain_id)->get();
                                                @$percent  = DB::table('add_to_carts')->where('domain_id',$domain_id)->where('domain_user_id',Auth::user()->user_id)->where('is_billed',1)->get();
                                                @$successRate = $percent->count() * 100 / $cartData->count();
                                            @endphp
                                        @endforeach
                                        <div class="kt-notification__item-details">
                                            <div class="kt-notification__item-title">Success Rate</div>
                                            <div class="kt-notification__item-time">{{(int)$successRate}}%</div>
                                        </div>
                                    </a>
                                    <a href="#" class="kt-notification__item">
                                        <div class="kt-notification__item-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <path d="M12.4644661,14.5355339 L9.46446609,14.5355339 C8.91218134,14.5355339 8.46446609,14.9832492 8.46446609,15.5355339 C8.46446609,16.0878187 8.91218134,16.5355339 9.46446609,16.5355339 L12.4644661,16.5355339 L12.4644661,17.5355339 C12.4644661,18.6401034 11.5690356,19.5355339 10.4644661,19.5355339 L6.46446609,19.5355339 C5.35989659,19.5355339 4.46446609,18.6401034 4.46446609,17.5355339 L4.46446609,13.5355339 C4.46446609,12.4309644 5.35989659,11.5355339 6.46446609,11.5355339 L10.4644661,11.5355339 C11.5690356,11.5355339 12.4644661,12.4309644 12.4644661,13.5355339 L12.4644661,14.5355339 Z" fill="#000000" opacity="0.3" transform="translate(8.464466, 15.535534) rotate(-45.000000) translate(-8.464466, -15.535534) "></path>
                                                    <path d="M11.5355339,9.46446609 L14.5355339,9.46446609 C15.0878187,9.46446609 15.5355339,9.01675084 15.5355339,8.46446609 C15.5355339,7.91218134 15.0878187,7.46446609 14.5355339,7.46446609 L11.5355339,7.46446609 L11.5355339,6.46446609 C11.5355339,5.35989659 12.4309644,4.46446609 13.5355339,4.46446609 L17.5355339,4.46446609 C18.6401034,4.46446609 19.5355339,5.35989659 19.5355339,6.46446609 L19.5355339,10.4644661 C19.5355339,11.5690356 18.6401034,12.4644661 17.5355339,12.4644661 L13.5355339,12.4644661 C12.4309644,12.4644661 11.5355339,11.5690356 11.5355339,10.4644661 L11.5355339,9.46446609 Z" fill="#000000" transform="translate(15.535534, 8.464466) rotate(-45.000000) translate(-15.535534, -8.464466) "></path>
                                                </g>
                                            </svg>
                                        </div>
                                        <div class="kt-notification__item-details">
                                            <div class="kt-notification__item-title">Number of Backlinks</div>
                                            <div class="kt-notification__item-time">
                                                @if($split_Query->backlinks)
                                                    @if($split_Query->backlinks == 1)
                                                        {{$split_Query->backlinks.' Backlink'}}
                                                    @else 
                                                        {{$split_Query->backlinks.' Backlinks'}}
                                                    @endif
                                                @else
                                                    {{'0 Backlink'}}
                                                @endif
                                                
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#" class="kt-notification__item">
                                        <div class="kt-notification__item-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--brand">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <path d="M12,10.9996338 C12.8356605,10.3719448 13.8743941,10 15,10 C17.7614237,10 20,12.2385763 20,15 C20,17.7614237 17.7614237,20 15,20 C13.8743941,20 12.8356605,19.6280552 12,19.0003662 C11.1643395,19.6280552 10.1256059,20 9,20 C6.23857625,20 4,17.7614237 4,15 C4,12.2385763 6.23857625,10 9,10 C10.1256059,10 11.1643395,10.3719448 12,10.9996338 Z M13.3336047,12.504354 C13.757474,13.2388026 14,14.0910788 14,15 C14,15.9088933 13.7574889,16.761145 13.3336438,17.4955783 C13.8188886,17.8206693 14.3938466,18 15,18 C16.6568542,18 18,16.6568542 18,15 C18,13.3431458 16.6568542,12 15,12 C14.3930587,12 13.8175971,12.18044 13.3336047,12.504354 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                                    <circle fill="#000000" cx="12" cy="9" r="5"></circle>
                                                </g>
                                            </svg>
                                        </div>
                                        <div class="kt-notification__item-details">
                                            <div class="kt-notification__item-title">Order in Queue</div>
                                            <div class="kt-notification__item-time">
                                            @if( $split_Query->data_add_to_cart)
                                                @if( $split_Query->data_add_to_cart->count() < 1)        
                                                    {{$split_Query->data_add_to_cart->count().' Order'}}
                                                @else 
                                                    {{$split_Query->data_add_to_cart->count().' Orders'}}
                                                @endif
                                            @endif
                                            </div>
                                        </div>
                                    </a>
                                    <a href="javascript:void(0);" class="kt-notification__item">
                                        <div class="kt-notification__item-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <path d="M12,21 C7.581722,21 4,17.418278 4,13 C4,8.581722 7.581722,5 12,5 C16.418278,5 20,8.581722 20,13 C20,17.418278 16.418278,21 12,21 Z" fill="#000000" opacity="0.3"></path>
                                                    <path d="M13,5.06189375 C12.6724058,5.02104333 12.3386603,5 12,5 C11.6613397,5 11.3275942,5.02104333 11,5.06189375 L11,4 L10,4 C9.44771525,4 9,3.55228475 9,3 C9,2.44771525 9.44771525,2 10,2 L14,2 C14.5522847,2 15,2.44771525 15,3 C15,3.55228475 14.5522847,4 14,4 L13,4 L13,5.06189375 Z" fill="#000000"></path>
                                                    <path d="M16.7099142,6.53272645 L17.5355339,5.70710678 C17.9260582,5.31658249 18.5592232,5.31658249 18.9497475,5.70710678 C19.3402718,6.09763107 19.3402718,6.73079605 18.9497475,7.12132034 L18.1671361,7.90393167 C17.7407802,7.38854954 17.251061,6.92750259 16.7099142,6.53272645 Z" fill="#000000"></path>
                                                    <path d="M11.9630156,7.5 L12.0369844,7.5 C12.2982526,7.5 12.5154733,7.70115317 12.5355117,7.96165175 L12.9585886,13.4616518 C12.9797677,13.7369807 12.7737386,13.9773481 12.4984096,13.9985272 C12.4856504,13.9995087 12.4728582,14 12.4600614,14 L11.5399386,14 C11.2637963,14 11.0399386,13.7761424 11.0399386,13.5 C11.0399386,13.4872031 11.0404299,13.4744109 11.0414114,13.4616518 L11.4644883,7.96165175 C11.4845267,7.70115317 11.7017474,7.5 11.9630156,7.5 Z" fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </div>
                                        <div class="kt-notification__item-details">
                                            <div class="kt-notification__item-title">Turnaround Time</div>
                                            <div class="kt-notification__item-time">
                                                @if($split_Query->turnaround_time)
                                                @php
                                                $turnaround_time = str_replace("_"," ",$split_Query->turnaround_time);
                                                @endphp
                                                {{ ucwords($turnaround_time) }}
                                                @endif
                                            </div>
                                        </div>
                                    </a>
                                    <a href="#" class="kt-notification__item">
                                        <div class="kt-notification__item-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect opacity="0.200000003" x="0" y="0" width="24" height="24"></rect>
                                                    <path d="M4.5,7 L9.5,7 C10.3284271,7 11,7.67157288 11,8.5 C11,9.32842712 10.3284271,10 9.5,10 L4.5,10 C3.67157288,10 3,9.32842712 3,8.5 C3,7.67157288 3.67157288,7 4.5,7 Z M13.5,15 L18.5,15 C19.3284271,15 20,15.6715729 20,16.5 C20,17.3284271 19.3284271,18 18.5,18 L13.5,18 C12.6715729,18 12,17.3284271 12,16.5 C12,15.6715729 12.6715729,15 13.5,15 Z" fill="#000000" opacity="0.3"></path>
                                                    <path d="M17,11 C15.3431458,11 14,9.65685425 14,8 C14,6.34314575 15.3431458,5 17,5 C18.6568542,5 20,6.34314575 20,8 C20,9.65685425 18.6568542,11 17,11 Z M6,19 C4.34314575,19 3,17.6568542 3,16 C3,14.3431458 4.34314575,13 6,13 C7.65685425,13 9,14.3431458 9,16 C9,17.6568542 7.65685425,19 6,19 Z" fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </div>
                                        <div class="kt-notification__item-details">
                                            <div class="kt-notification__item-title">Content Options</div>
                                            <div class="kt-notification__item-time">
                                                
                                                @php
                                                    $guest_content = [];

                                                    if($split_Query->guest_content == 'seller'){
                                                        $guest_content[] = $split_Query->guest_content;
                                                    } 
                                                    else {
                                                        if(unserialize($split_Query->guest_content)){
                                                            $guest_content = unserialize($split_Query->guest_content);
                                                        }
                                                    }
                                                    if($split_Query->guest_content == 'buyer'){
                                                        $guest_content[] = $split_Query->guest_content;
                                                    } 
                                                    if($split_Query->guest_content == 'hire_content'){
                                                        $guest_content[] = $split_Query->guest_content;
                                                    }              
                                                @endphp

                                                @if($guest_content)
                                                    @if(in_array('seller', $guest_content))
                                                        <span style="width: 171px;"><span class="kt-badge kt-badge--primary kt-badge--dot"></span>&nbsp;<span class="kt-font-bold kt-font-primary">Publisher</span></span>
                                                    @elseif(in_array('buyer', $guest_content))
                                                        <span style="width: 171px;margin-left: 1rem;"><span class="kt-badge kt-badge--primary kt-badge--dot"></span>&nbsp;<span class="kt-font-bold kt-font-primary">Advertiser</span></span>
                                                    @elseif(in_array('hire_content', $guest_content))
                                                        <span class="kt-badge  kt-badge--brand kt-badge--inline kt-badge--pill kt-badge--unified-primary kt-ml-10">Hire Content Writer</span>
                                                    @endif
                                                @endif
                                                
                                            </div>
                                        </div>
                                    </a>
                                    <a href="javascript:void(0);" class="kt-notification__item">
                                        <div class="kt-notification__item-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" x="5" y="4" width="6" height="16" rx="1.5"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="13" y="4" width="6" height="16" rx="1.5"></rect>
                                                </g>
                                            </svg>
                                        </div>
                                        <div class="kt-notification__item-details">
                                            <div class="kt-notification__item-title">Guest Post Examples</div>
                                            <div class="kt-notification__item-time">
                                                @if(!empty($split_Query->exampleurlOne))
                                                    <span class="kt-badge  kt-badge--brand kt-badge--inline kt-badge--pill kt-badge--unified-primary">{{$split_Query->exampleurlOne}}</span>
                                                @endif
                                                @if(!empty($split_Query->exampleurlTwo))
                                                    <span class="kt-badge  kt-badge--brand kt-badge--inline kt-badge--pill kt-badge--unified-primary kt-ml-10">{{$split_Query->exampleurlTwo}}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="kt-pt20
                                    d-flex" style="
                                    justify-content: space-between;
                                    ">
                                    <!-- <a href="javascript:void(0);" class="btn btn-label-success btn-bold" data-toggle="modal" data-target="#kt_chat_modal">Chat (Online) soon..</a> -->
                                    @if(Auth::check())
                                        <a href="javascript:void(0);" data-domain-id="{{$split_Query->domain_id}}" data-other-user="{{$value->user_id}}" data-auth-user="{{Auth::user()->user_id}}"  class="btn btn-label-success btn-bold chatroom" >Chat (Online)</a>
                                    @else
                                        <a href="{{url('login')}}" class="btn btn-label-success btn-bold">Chat (Online)</a>
                                    @endif
                                    
                                    <a href="javascript:void(0)" class="btn btn-brand btn-upper continue" data-domain-id="{{$split_Query->domain_id}}" data-domain-seller-id="{{$value->user_id}}" data-domain-price="{{$split_Query->cost_price}}"  data-domain-extra-cost="{{$split_Query->extra_cost}}" data-domain-content="{{$split_Query->number_words}}" data-grand-total-with-extra-cost="{{buyerDomainPriceCalculate($split_Query->cost_price)}}">Continue
                                    (${{buyerDomainPriceCalculate(@$split_Query->cost_price, $split_Query->extra_cost)}})</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @else
            No Publisher found for this website...
        @endif
    @endforeach
    

@endforeach


 @section('customScript')

  <script type="text/javascript">
            
            $(document).ready(function(){
                $(document).on('click','.chatroom',function(e){
                    $(".loader").fadeIn("slow");
                    e.preventDefault();
                    
                    var authUser    = $(this).attr('data-auth-user');
                    var otherUser    = $(this).attr('data-other-user');
                    var domain_id = $(this).attr('data-domain-id');
                    
                    $.ajax({
                         headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                          },
                           // beforeSend: function() {
                           //      // setting a timeout
                           //      $(".loader").fadeIn("slow");
                           //  },
                          url  : "{{ url('/chat-render-pop') }}",
                          type : 'POST',
                          data: {                   
                                'domain_id' : domain_id, 
                                'authUser' : authUser,
                                'otherUser' : otherUser
                             },
                          async: true,
                          //dataType: 'json',
                          //enctype: 'multipart/form-data',
                          //cache: false,
                          success: function(response){

                            $('#chat-append-pop').html(response);
                            run();
                            
                          },
                          error: function(){}
                        });
                        e.stopImmediatePropagation();
                        return false; 

                });
            });
        </script>

<!--    <script href="{{ asset('') . config('app.public_url') . '/assets/js/pages/custom/chat/chat.js'  }}" type="text/javascript"></script> -->

         <!-- HTML Templates -->
<!--     <script type="text/html" id="message-template">
      <div class="row no-margin">
        <div class="row no-margin message-info-row" style="">
          <div class="col-md-8 left-align"><p data-content="username" class="message-username"></p></div>
          <div class="col-md-4 right-align"><span data-content="date" class="message-date"></span></div>
        </div>
        <div class="row no-margin message-content-row">
          <div style="" class="col-md-12"><p data-content="body" class="message-body"></p></div>
        </div>
      </div>
    </script> -->
    <script type="text/html" id="channel-template">
      <div class="col-md-12">
        <p class="channel-element" data-content="channelName"></p>
      </div>
    </script>
    <script type="text/html" id="member-notification-template">
      <p class="member-status" data-content="status"></p>
    </script>


     <!-- HTML Templates -->
    <script type="text/html" id="message-template">        

        <div class="kt-chat__message kt-chat__message--success">
            <div class="kt-chat__user">
                <span class="kt-media kt-media--circle kt-media--sm">
                    <img src="{{ asset('') . config('app.public_url') . '/assets/media/users/default.jpg'}}" alt="image">
                    <!-- <img src="{{url('public/assets/media/users/default.jpg')}}" alt="image"> -->
                </span>

                <a href="javascript:void(0);" data-content="username" class="kt-chat__username"></span></a>
                <span data-content="date" class="kt-chat__datetime"></span>
            </div>
            <div class="kt-chat__text" data-content="body">
                
            </div>
        </div>

    </script>
    <!-- HTML Templates -->
    <script type="text/html" id="message-template-right">

        <div class="kt-chat__message kt-chat__message--right kt-chat__message--brand">
            <div class="kt-chat__user">
                <span data-content="date" class="kt-chat__datetime"></span>
                <a href="javascript:void(0);" class="kt-chat__username"  data-content="username"></span></a>
                
                <span class="kt-media kt-media--circle kt-media--sm">
                    <!-- <img src="{{asset('frontend/userImages/'.Auth::user()->image)}}" alt="image"> -->
                    <img src="{{ asset('') . config('app.public_url') . '/assets/media/users/default.jpg'}}" alt="image">
                </span>
            </div>
            <div class="kt-chat__text" data-content="body">
            </div>
        </div>

    </script>
    <!-- JavaScript -->
    <script src="//code.jquery.com/jquery-2.2.0.min.js"></script>
    
    <script src="{{ asset('') . config('app.public_url') . '/js/vendor/jquery-throttle.min.js' }}"></script>
    <script src="{{ asset('') . config('app.public_url') . '/js/vendor/jquery.loadTemplate-1.4.4.min.js' }}"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.1/moment.min.js"></script>
    <!-- Twilio Common helpers and Twilio Chat JavaScript libs from CDN. -->
    <script src="//media.twiliocdn.com/sdk/js/common/v0.1/twilio-common.min.js"></script>
    <script src="//media.twiliocdn.com/sdk/js/chat/v3.0/twilio-chat.min.js"></script>
    <script type="text/javascript">
    function run(){


        var twiliochat = (function() {
        var tc = {};

        var GENERAL_CHANNEL_UNIQUE_NAME = $('#roomname').val();

        var GENERAL_CHANNEL_NAME = $('#roomnameview').val();
        var MESSAGES_HISTORY_LIMIT = 50;

        var $channelList;
        var $inputText;
        var $usernameInput;
        var $statusRow;
        var $connectPanel;
        var $newChannelInputRow;
        var $newChannelInput;
        var $typingRow;
        var $typingPlaceholder;

        $(document).ready(function() {
            tc.$messageList = $('#message-list');
            $channelList = $('#channel-list');
            $inputText = $('#input-text');
            $usernameInput = $('#username-input');
            $statusRow = $('#status-row');
            
            $connectPanel = $('#connect-panel');
            $newChannelInputRow = $('#new-channel-input-row');
            $newChannelInput = $('#new-channel-input');
            $typingRow = $('#typing-row');
            $typingPlaceholder = $('#typing-placeholder');
            $usernameInput.focus();
            $usernameInput.on('keypress', handleUsernameInputKeypress);
            
            $inputText.on('keypress', handleInputTextKeypress);
            $('#input-button').on('click', handleInputTextKeypress);

            $newChannelInput.on('keypress', tc.handleNewChannelInputKeypress);
            $('#connect-image').on('click', connectClientWithUsername);
            $('#add-channel-image').on('click', showAddChannelInput);
            $('#leave-span').on('click', disconnectClient);
            $('#delete-channel-span').on('click', deleteCurrentChannel);
        });
        connectClientWithUsername();

        function getUsername(author) {
            var str = author;
            //var res = str.split("_");
            //return res[0];
            return str;
        }

        function handleUsernameInputKeypress(event) {
            if (event.keyCode === 13){
                connectClientWithUsername();
            }
        }

        function handleInputTextKeypress(event) {
            var sender = $('#auth').val();
            var receiver = $('#user_id').val();
            var channelid = tc.currentChannel.sid;            
            var msg = $.trim($('#input-text').val());
            if(msg == '') {
                $('label.error').text('Please enter value here.');
                return;
            } 
            $('label.error').text('');
            if ((event.keyCode === 13 || event.type == 'click') && msg != '') {
                tc.currentChannel.sendMessage($('#input-text').val());
                event.preventDefault();
                $('#input-text').val('');
                var chatChannelId = $('#chatChannelId').val();
                var roomname=$('#roomname').val();
                var date= dateFormatter.getTodayDate();
                $.post("{{url('/storemsg')}}", {chatChannelId:chatChannelId,sender:sender,receiver:receiver,channelid:channelid,msg:msg,roomname:roomname,date:date}, null, 'json')
                    .done(function(response) {
                    })
                    .fail(function(error) {
                        alert('error saving data');
                        console.log('Failed to fetch the Access Token with error: ' + error);
                    });
            }
            else {
                notifyTyping();
            }
        }

        var notifyTyping = $.throttle(function() {
            tc.currentChannel.typing();
        }, 1000);

        tc.handleNewChannelInputKeypress = function(event) {
            if (event.keyCode === 13) {
            tc.messagingClient.createChannel({
                friendlyName: $newChannelInput.val()
            }).then(hideAddChannelInput);
            $(this).val('');
            event.preventDefault();
            }
        };

        function connectClientWithUsername() {
            var usernameText = $usernameInput.val();
            $usernameInput.val('');
            if (usernameText == '') {
                alert('Username cannot be empty');
                return;
            }
            tc.username = usernameText;
            fetchAccessToken(tc.username, connectMessagingClient);
        }

        function fetchAccessToken(username, handler) {
            
            $.post("{{url('/token')}}", {identity: username, device: 'browser'}, null, 'json')
            .done(function(response) {
                handler(response.token);
            })
            .fail(function(error) {
                console.log('Failed to fetch the Access Token with error: ' + error);
            });
        }

        function connectMessagingClient(token) {
            // Initialize the Chat messaging client
            
            tc.accessManager = new Twilio.AccessManager(token);
            Twilio.Chat.Client.create(token).then(function(client) {
            tc.messagingClient = client;
            updateConnectedUI();
            
            tc.loadChannelList(tc.joinGeneralChannel);
            tc.messagingClient.on('channelAdded', $.throttle(tc.loadChannelList));
            tc.messagingClient.on('channelRemoved', $.throttle(tc.loadChannelList));
            tc.messagingClient.on('tokenExpired', refreshToken);
            });
        }

        function refreshToken() {
            fetchAccessToken(tc.username, setNewToken);
        }

        function setNewToken(tokenResponse) {
            tc.accessManager.updateToken(tokenResponse.token);
        }

        function updateConnectedUI() {
            $('#username-span').text(tc.username);
            $statusRow.addClass('connected').removeClass('disconnected');
            tc.$messageList.addClass('connected').removeClass('disconnected');
            $connectPanel.addClass('connected').removeClass('disconnected');
            $inputText.addClass('with-shadow');
            $typingRow.addClass('connected').removeClass('disconnected');
        }

        tc.loadChannelList = function(handler) {
            if (tc.messagingClient === undefined) {
            console.log('Client is not initialized');
            return;
            }

            tc.messagingClient.getPublicChannelDescriptors().then(function(channels) {
            tc.channelArray = tc.sortChannelsByName(channels.items);
            $channelList.text('');
            tc.channelArray.forEach(addChannel);
            if (typeof handler === 'function') {
                handler();
            }
            });
        };

        tc.joinGeneralChannel = function() {
            console.log('Attempting to join "general" chat channel...');
            if (!tc.generalChannel) {
                //If it doesn't exist, let's create it
                tc.messagingClient.createChannel({
                    uniqueName: GENERAL_CHANNEL_UNIQUE_NAME,
                    friendlyName: GENERAL_CHANNEL_NAME
                }).then(function(channel) {

                    console.log('Created general channel');
                    tc.generalChannel = channel;
                    tc.loadChannelList(tc.joinGeneralChannel);
                });

                
            }
            else {
            console.log('Found general channel:');
            setupChannel(tc.generalChannel);
            }
        };

        function initChannel(channel) {
            console.log('Initialized channel ' + channel.friendlyName);
            return tc.messagingClient.getChannelBySid(channel.sid);
        }

        function joinChannel(_channel) {
            
                return _channel.join()
                .then(function(joinedChannel) {
                    console.log('Joined channel ' + joinedChannel.friendlyName);
                    updateChannelUI(_channel);
                    tc.currentChannel = _channel;
                    //tc.loadMessages();
                    return joinedChannel;
                });
            
        }

        function initChannelEvents() {
            console.log(tc.currentChannel.friendlyName + ' ready.');
            tc.currentChannel.on('messageAdded', tc.addMessageToList);
            tc.currentChannel.on('typingStarted', showTypingStarted);
            tc.currentChannel.on('typingEnded', hideTypingStarted);
            tc.currentChannel.on('memberJoined', notifyMemberJoined);
            tc.currentChannel.on('memberLeft', notifyMemberLeft);
            $inputText.prop('disabled', false).focus();

            $(".loader").fadeOut("slow");
            $('#kt_chat_modal').modal('show');
        }

        function setupChannel(channel) {
            return leaveCurrentChannel()
            .then(function() {
                
                return initChannel(channel);
            })
            .then(function(_channel) {

                return joinChannel(_channel);
            })
            .then(initChannelEvents);
        }

        tc.loadMessages = function() {
            tc.currentChannel.getMessages(MESSAGES_HISTORY_LIMIT).then(function (messages) {
            messages.items.forEach(tc.addMessageToList);
            });
        };

        function leaveCurrentChannel() {

            if (tc.currentChannel) {
                
            return tc.currentChannel.leave().then(function(leftChannel) {
                console.log('left ' + leftChannel.friendlyName);
                leftChannel.removeListener('messageAdded', tc.addMessageToList);
                leftChannel.removeListener('typingStarted', showTypingStarted);
                leftChannel.removeListener('typingEnded', hideTypingStarted);
                leftChannel.removeListener('memberJoined', notifyMemberJoined);
                leftChannel.removeListener('memberLeft', notifyMemberLeft);
            });
            } else {
                
                return Promise.resolve();
            }
        }

        tc.addMessageToList = function(message) {
            
            var username = getUsername(message.author);
            
            if (message.author === tc.username) {

                var rowDiv = $('<div>').addClass('kt-chat__message kt-chat__message--right');
                rowDiv.loadTemplate($('#message-template-right'), {
                username: message.author,
                date: dateFormatter.getTodayDate(message.timestamp),
                body: message.body
                });
                rowDiv.addClass('own-message');
                
            } else {

                var rowDiv = $('<div>').addClass('kt-chat__message');
                rowDiv.loadTemplate($('#message-template'), {
                username: username,
                date: dateFormatter.getTodayDate(message.timestamp),
                body: message.body
                });
                rowDiv.addClass('own-message');
                
            }

            tc.$messageList.append(rowDiv);
            scrollToMessageListBottom();
        };

        function notifyMemberJoined(member) {
            notify(member.identity + ' joined the channel')
        }

        function notifyMemberLeft(member) {
            notify(member.identity + ' left the channel');
        }

        function notify(message) {
            var row = $('<div>').addClass('col-md-12');
            row.loadTemplate('#member-notification-template', {
            status: message
            });
            tc.$messageList.append(row);
            scrollToMessageListBottom();
        }

        function showTypingStarted(member) {
            $typingPlaceholder.text(member.identity + ' is typing...');
        }

        function hideTypingStarted(member) {
            $typingPlaceholder.text('');
        }

        function scrollToMessageListBottom() {
            tc.$messageList.scrollTop(tc.$messageList[0].scrollHeight);
        }

        function updateChannelUI(selectedChannel) {
            var channelElements = $('.channel-element').toArray();
            var channelElement = channelElements.filter(function(element) {
            return $(element).data().sid === selectedChannel.sid;
            });
            channelElement = $(channelElement);
            if (tc.currentChannelContainer === undefined && selectedChannel.uniqueName === GENERAL_CHANNEL_UNIQUE_NAME) {
            tc.currentChannelContainer = channelElement;
            }
            tc.currentChannelContainer.removeClass('selected-channel').addClass('unselected-channel');
            channelElement.removeClass('unselected-channel').addClass('selected-channel');
            tc.currentChannelContainer = channelElement;
        }

        function showAddChannelInput() {
            if (tc.messagingClient) {
            $newChannelInputRow.addClass('showing').removeClass('not-showing');
            $channelList.addClass('showing').removeClass('not-showing');
            $newChannelInput.focus();
            }
        }

        function hideAddChannelInput() {
            $newChannelInputRow.addClass('not-showing').removeClass('showing');
            $channelList.addClass('not-showing').removeClass('showing');
            $newChannelInput.val('');
        }

        function addChannel(channel) {
           
            if (channel.uniqueName === GENERAL_CHANNEL_UNIQUE_NAME) {
                tc.generalChannel = channel;
            }
           
            var rowDiv = $('<div>').addClass('row channel-row');
            rowDiv.loadTemplate('#channel-template', {
            channelName: channel.friendlyName
            });

            var channelP = rowDiv.children().children().first();
            
            rowDiv.on('click', selectChannel);
            channelP.data('sid', channel.sid);
            if (tc.currentChannel && channel.sid === tc.currentChannel.sid) {
            tc.currentChannelContainer = channelP;
            channelP.addClass('selected-channel');

            }
            else {
            channelP.addClass('unselected-channel')
            }

            $channelList.append(rowDiv);
        }

        function deleteCurrentChannel() {
            if (!tc.currentChannel) {
            return;
            }
            if (tc.currentChannel.sid === tc.generalChannel.sid) {
            alert('You cannot delete the general channel');
            return;
            }
            tc.currentChannel.delete().then(function(channel) {
            console.log('channel: '+ channel.friendlyName + ' deleted');
            setupChannel(tc.generalChannel);
            });
        }

        function selectChannel(event) {
            var target = $(event.target);
            var channelSid = target.data().sid;
            var selectedChannel = tc.channelArray.filter(function(channel) {
            return channel.sid === channelSid;
            })[0];
            if (selectedChannel === tc.currentChannel) {
            return;
            }
            setupChannel(selectedChannel);
        };

        function disconnectClient() {
            leaveCurrentChannel();
            $channelList.text('');
            tc.$messageList.text('');
            channels = undefined;
            $statusRow.addClass('disconnected').removeClass('connected');
            tc.$messageList.addClass('disconnected').removeClass('connected');
            $connectPanel.addClass('disconnected').removeClass('connected');
            $inputText.removeClass('with-shadow');
            $typingRow.addClass('disconnected').removeClass('connected');
        }

        tc.sortChannelsByName = function(channels) {
            return channels.sort(function(a, b) {
            if (a.friendlyName === GENERAL_CHANNEL_NAME) {
                return -1;
            }
            if (b.friendlyName === GENERAL_CHANNEL_NAME) {
                return 1;
            }
            return a.friendlyName.localeCompare(b.friendlyName);
            });
        };

        return tc;
        })();



        }
    </script>
    <script src="{{ asset('') . config('app.public_url') . '/js/dateformatter.js' }}"></script>

@endsection  