
@foreach($selectQueryUserList as $key => $split_Query)
        
        <?php 
            $ke = $key.'-k';
            $domain_id = $split_Query->domain_id; 
            $value = \App\AssignedWebsite::where('mst_domain_url_id',$domain_id)->first();
            $user_id = $value['user_id'];
            $next_domain_id = secureEncrypt($split_Query->domain_id);
        ?>
        @if(!empty($value->user_id))

            @if($key==0)
                @php
                    $show = 'show';
                @endphp
            @else
                @php
                    $show = '';
                @endphp
            @endif
            <div class="card">
                <div class="card-header" id="headingOne{{$ke}}">
                    <div class="card-title">
                        <div class="kt-widget4 w-100">
                            <div class="kt-widget4__item w-100">
                                <div class="d-flex collapsed flex-grow-1 align-items-center" data-toggle="collapse" data-target="#collapseOne{{$ke}}" aria-expanded="false" aria-controls="collapseOne8">
                                    @php
                                        $image = @$value->data_username->image;
                                        $name  = '';
                                        $lname = '';
                                    @endphp
                                    @if(!empty($image))
                                        @php
                                            $image = url('storage/app/') . '/' . $image;
                                        @endphp
                                    @else
                                        @php
                                            $name  = strtoupper($rest = substr($value->data_username->user_fname, 0, 1));
                                            $lname = strtoupper($rest = substr($value->data_username->user_lname, 0, 1));
                                        @endphp
                                    @endif
                                    @if(!empty($image))
                                        <div class="kt-widget4__pic kt-widget4__pic--pic">
                                            <img src="{{$image}}" alt="">           
                                        </div>
                                    @else
                                        @php
                                            $image = asset('') . config('app.public_url') .'/assets/media/users/default.jpg';
                                        @endphp
                                        @if(!empty($name))
                                            <div class="kt-widget4__pic kt-widget__pic--primary kt-font-primary kt-font-boldest kt-font-light">
                                            <span class="kt-badge kt-badge--username kt-badge--unified-primary kt-badge--lg kt-badge--rounded kt-badge--bold">
                                            {{$name}}{{$lname}}</span>
                                                
                                            </div>
                                        @else
                                            <div class="kt-widget4__pic kt-widget4__pic--pic">
                                                <img src="{{$image}}" alt="">           
                                            </div>

                                        @endif
                                    @endif
                                    
                                    
                                    <div class="kt-widget4__info flex-grow-0">
                                        
                                        <!-- <a href="{{url('profile').'/'.$value->data_username->user_code}}" class="kt-widget4__username"> -->
                                        <a href="{{ url('/search?q=@'.$value->data_username->user_code) }}" class="kt-widget4__username">
                                            {{ '@'. @$value->data_username->user_code}} 
                                        </a>
                                        @if(!empty($split_Query->backlink_type))

                                            @if($split_Query->backlink_type == 'no_follow')
                                                @php
                                                    $follow = 'No Follow';
                                                    $class = 'warning';
                                                @endphp                          
                                            @elseif($split_Query->backlink_type == 'follow')
                                                @php
                                                    $follow = 'Do Follow';
                                                    $class = 'success';
                                                @endphp
                                            @endif
                                        @else
                                            @php
                                                $follow = 'No Follow';
                                                $class = 'warning';
                                            @endphp
                                        @endif
                                        <p class="kt-widget4__text">
                                            <span class="kt-badge kt-badge--{{$class}} kt-badge--dot"></span>&nbsp;
                                                <span class="kt-font-bold kt-font-{{$class}}">
                                                   {{$follow}} 
                                                </span>
                                            </p>
                                    </div>
                                        <style>
                                        .card-title .checked i{
                                            color:orange;
                                        }
                                    </style>
                                        <?php
                                        foreach( $selectQueryWebsite as $split_website ){
                                            $domain_id_id = $split_website['domain_id'];
                                            $review_lists = DB::table('reviews')->where('domain_url',$domain_url)->where('type','user')->where('user_id',$user_id)->get()->toArray();

                                            $calc_rating = 0;
                                            foreach( $review_lists as $split_review_lists ){
                                                if( $split_review_lists->type == 'user'){
                                                    $calc_rating = $split_review_lists->rating;
                                                }
                                            }
                                            $averageStar = $calc_rating;
                                        ?>
                                        <div class="d-flex flex-column flex-grow-1 align-items-center">                        
                                            <div class="d-flex">
                                                <!-- <div class="kt-demo-icon__preview">
                                                    <i class="fas fa-star active-star"></i>
                                                </div> -->
                                                     @if(empty($averageStar))
                                                       <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                      @elseif($averageStar == 1)
                                                       <div class="kt-demo-icon__preview  checked">
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        @elseif( $averageStar == 2)
                                                        <div class="kt-demo-icon__preview checked">
                                                            <i class="fas fa-star" data-val ="1"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview checked">
                                                            <i class="fas fa-star" data-val ="2"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star" data-val ="3"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star" data-val ="4"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star" data-val ="5"></i>
                                                        </div>
                                                        @elseif( $averageStar == 3)
                                                        <div class="kt-demo-icon__preview checked">
                                                            <i class="fas fa-star" data-val ="1"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview checked">
                                                            <i class="fas fa-star" data-val ="2"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview checked">
                                                            <i class="fas fa-star" data-val = "3"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star" data-val ="4"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star" data-val ="5"></i>
                                                        </div>
                                                        @elseif( $averageStar == 4)
                                                        <div class="kt-demo-icon__preview checked">
                                                            <i class="fas fa-star" data-val ="1"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview checked">
                                                            <i class="fas fa-star" data-val ="2"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview checked">
                                                            <i class="fas fa-star" data-val = "3"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview checked">
                                                            <i class="fas fa-star" data-val = "4"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star" data-val ="5"></i>
                                                        </div>
                                                        @elseif( $averageStar == 5)
                                                        <div class="kt-demo-icon__preview checked">
                                                            <i class="fas fa-star" data-val ="1"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview checked">
                                                            <i class="fas fa-star" data-val ="2"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview checked">
                                                            <i class="fas fa-star" data-val = "3"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview checked">
                                                            <i class="fas fa-star" data-val = "4"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview checked">
                                                            <i class="fas fa-star" data-val = "5"></i>
                                                        </div>
                                                        @endif
                                            </div>
                                            @if(empty($averageStar))
                                            <span style="
                                                display: flex;
                                                font-size: 0.9rem;
                                                color: #74788d;
                                                font-weight: normal;
                                                " class="kt-widget4__text">
                                                No Reviews 
                                            </span>
                                            @endif
                                            
                                            <!-- no review  -->
                                           <!--  <div class="kt-mb-5 d-flex">                            
                                                <div class=" kt-ml-10">
                                                    <div class="d-flex">
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> -->
                                            <!-- no review  -->

                                        </div>
                                        <?php } ?>
                                </div>
                                    
                                @if(Auth::check())
                                    @if(Auth::user()->user_id != $value->user_id)
                                        @if(Auth::user()->switched == 'advertiser' && Auth::user()->user_type != 'admin')
                                           <!-- <a href="javascript:void(0)" class="btn btn-brand btn-upper continue" data-domain-id="{{$split_Query->domain_id}}" data-domain-seller-id="{{$value->user_id}}" data-domain-price="{{$split_Query->cost_price}}"  data-domain-extra-cost="{{$split_Query->extra_cost}}" data-domain-content="{{$split_Query->number_words}}" data-grand-total-with-extra-cost="{{buyerDomainPriceCalculate($split_Query->cost_price,$split_Query->extra_cost )}}" data-grand-total-without-extra-cost="{{buyerDomainWebsitePriceCalculate($split_Query->cost_price )}}">Continue
                                             (${{buyerDomainWebsitePriceCalculate(@$split_Query->cost_price)}})</a> -->
                                            <!-- <a href="{{url('/search/website').'/'.$split_Query->domain_url.'/'.$next_domain_id}}" class="btn btn-brand btn-upper" >Continue
                                             (${{buyerDomainWebsitePriceCalculate(@$split_Query->cost_price)}})</a> -->
                                             <a href="javascript:void(0);" class="btn btn-brand btn-upper redirect-continue" data-domain-id="{{$next_domain_id}}" data-domain-url="{{$split_Query->domain_url}}">Continue
                                             (${{buyerDomainWebsitePriceCalculate(@$split_Query->cost_price)}})</a>

                                             
                                        @else
                                            <a href="javascript:void(0)" class="btn btn-brand btn-upper advertiser" >Continue
                                             (${{buyerDomainWebsitePriceCalculate(@$split_Query->cost_price)}})</a>
                                        @endif
                                    @else
                                        <a href="javascript:void(0)" class="btn btn-brand btn-upper disabled" >Continue
                                    (${{buyerDomainWebsitePriceCalculate(@$split_Query->cost_price)}})</a>
                                    @endif
                                @else
                                    <!-- <a href="javascript:void(0)" class="btn btn-brand btn-upper continue" data-domain-id="{{$split_Query->domain_id}}" data-domain-seller-id="{{$value->user_id}}" data-domain-price="{{$split_Query->cost_price}}"  data-domain-extra-cost="{{$split_Query->extra_cost}}" data-domain-content="{{$split_Query->number_words}}" data-grand-total-with-extra-cost="{{buyerDomainPriceCalculate($split_Query->cost_price,$split_Query->extra_cost )}}" data-grand-total-without-extra-cost="{{buyerDomainWebsitePriceCalculate($split_Query->cost_price )}}">Continue
                                             (${{buyerDomainWebsitePriceCalculate(@$split_Query->cost_price)}})</a> -->
                                    <a href="javascript:void(0);" class="btn btn-brand btn-upper redirect-continue" data-domain-id="{{$next_domain_id}}" data-domain-url="{{$split_Query->domain_url}}">Continue
                                             (${{buyerDomainWebsitePriceCalculate(@$split_Query->cost_price)}})</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                
                <div id="collapseOne{{$ke}}" class="collapse {{$show}}" aria-labelledby="headingOne{{$ke}}" data-parent="#accordionExample8" style="">
                    <div class="card-body">
                        <div class="kt-widget kt-widget--user-profile-2">
                            <div class="kt-widget__body">
                                <div class="kt-widget__item" style="
                                    display: none;
                                    ">
                                    <?php
                                            if( $split_Query->data_add_to_cart) :
                                                if( $split_Query->data_add_to_cart->count() < 1) :
                                                    $total = 0;
                                                else :
                                                    $total = $split_Query->data_add_to_cart->count();
                                                endif;
                                            endif;
                                            $success  = DB::table('add_to_carts')->where('domain_id',$domain_id)->where('is_billed',1)->count();
                                            @$successRate = $success * 100 / $total;
                                            if($successRate <= 0){
                                                $successRate = 'No Orders Yet';
                                            }
                                            else{

                                                $successRate = (int)$successRate.'%';
                                            }
                                        ?>

                                    <div class="kt-widget__contact">
                                        <span class="kt-widget__label kt-pr10">SUCCESS RATE:</span>
                                        <a href="javascript:void(0);" class="kt-widget__data">{{$successRate}}</a>
                                    </div>
                                    <div class="kt-widget__contact">
                                        <span class="kt-widget__label kt-pr10"> MAXIMUM NO BACKLINKS:</span>
                                        <a href="javascript:void(0);" class="kt-widget__data"> @if($split_Query->backlinks)
                                                    @if($split_Query->backlinks == 1)
                                                        {{$split_Query->backlinks.' Backlink'}}
                                                    @else 
                                                        {{$split_Query->backlinks.' Backlinks'}}
                                                    @endif
                                                @else
                                                    {{'0 Backlink'}}
                                                @endif
                                            </a>
                                    </div>
                                    <div class="kt-widget__contact">
                                        <span class="kt-widget__label kt-pr10">ORDERS IN QUEUE:</span>
                                        <a href="javascript:void(0);" class="kt-widget__data">
                                            {{$split_Query->data_add_to_cart->count()}}
                                        </a>
                                    </div>
                                    <div class="kt-widget__contact">
                                        <span class="kt-widget__label kt-pr10">TURNAROUND TIME:</span>
                                        <a href="javascript:void(0);" class="kt-widget__data">
                                            {{str_replace("_"," ",$split_Query->turnaround_time)}}
                                        </a>
                                    </div>
                                    <!-- <div class="kt-widget__contact">
                                        <span class="kt-widget__label kt-pr10">CONTENT PROVIDER OPTIONS:</span>
                                        <a href="javascript:void(0);" class="kt-widget__data"></a>
                                    </div>
                                    <div class="kt-widget__contact">
                                        <span class="kt-widget__label kt-pr10">GUEST POSTS EXAMPLES:</span>
                                        <a href="javascript:void(0);" class="kt-widget__data">500 words ($1)</a>
                                    </div>
                                    <div class="kt-widget__contact">
                                        <span class="kt-widget__label kt-pr10">MORE OPTIONS:</span>
                                        <a href="javascript:void(0);" class="kt-widget__data">500 words ($1)</a>
                                    </div> -->
                                </div>
                                <div class="kt-notification">
                                    <a href="javascript:void(0);" class="kt-notification__item">
                                        <div class="kt-notification__item-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <path d="M9,10 L9,19 L10.1525987,19.3841996 C11.3761964,19.7920655 12.6575468,20 13.9473319,20 L17.5405883,20 C18.9706314,20 20.2018758,18.990621 20.4823303,17.5883484 L21.231529,13.8423552 C21.5564648,12.217676 20.5028146,10.6372006 18.8781353,10.3122648 C18.6189212,10.260422 18.353992,10.2430672 18.0902299,10.2606513 L14.5,10.5 L14.8641964,6.49383981 C14.9326895,5.74041495 14.3774427,5.07411874 13.6240179,5.00562558 C13.5827848,5.00187712 13.5414031,5 13.5,5 L13.5,5 C12.5694044,5 11.7070439,5.48826024 11.2282564,6.28623939 L9,10 Z" fill="#000000"></path>
                                                    <rect fill="#000000" opacity="0.3" x="2" y="9" width="5" height="11" rx="1"></rect>
                                                </g>
                                            </svg>
                                        </div>
                                        
                                        <?php
                                            if( $split_Query->data_add_to_cart) :
                                                if( $split_Query->data_add_to_cart->count() < 1) :
                                                    $total = 0;
                                                else :
                                                    $total = $split_Query->data_add_to_cart->count();
                                                endif;
                                            endif;
                                            $success  = DB::table('add_to_carts')->where('domain_id',$domain_id)->where('is_billed',1)->count();
                                            
                                            @$successRate = $success * 100 / $total;
                                            if($successRate <= 0){
                                                $successRate = 'No Orders Yet';
                                            } else{

                                                $successRate = (int)$successRate.'%';
                                            }
                                        ?>
                                        
                                        <div class="kt-notification__item-details">
                                            <div class="kt-notification__item-title">Success Rate</div>
                                            <div class="kt-notification__item-time">{{$successRate}}</div>
                                        </div>
                                    </a>
                                    <a href="javascript:void(0);" class="kt-notification__item">
                                        <div class="kt-notification__item-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <path d="M12.4644661,14.5355339 L9.46446609,14.5355339 C8.91218134,14.5355339 8.46446609,14.9832492 8.46446609,15.5355339 C8.46446609,16.0878187 8.91218134,16.5355339 9.46446609,16.5355339 L12.4644661,16.5355339 L12.4644661,17.5355339 C12.4644661,18.6401034 11.5690356,19.5355339 10.4644661,19.5355339 L6.46446609,19.5355339 C5.35989659,19.5355339 4.46446609,18.6401034 4.46446609,17.5355339 L4.46446609,13.5355339 C4.46446609,12.4309644 5.35989659,11.5355339 6.46446609,11.5355339 L10.4644661,11.5355339 C11.5690356,11.5355339 12.4644661,12.4309644 12.4644661,13.5355339 L12.4644661,14.5355339 Z" fill="#000000" opacity="0.3" transform="translate(8.464466, 15.535534) rotate(-45.000000) translate(-8.464466, -15.535534) "></path>
                                                    <path d="M11.5355339,9.46446609 L14.5355339,9.46446609 C15.0878187,9.46446609 15.5355339,9.01675084 15.5355339,8.46446609 C15.5355339,7.91218134 15.0878187,7.46446609 14.5355339,7.46446609 L11.5355339,7.46446609 L11.5355339,6.46446609 C11.5355339,5.35989659 12.4309644,4.46446609 13.5355339,4.46446609 L17.5355339,4.46446609 C18.6401034,4.46446609 19.5355339,5.35989659 19.5355339,6.46446609 L19.5355339,10.4644661 C19.5355339,11.5690356 18.6401034,12.4644661 17.5355339,12.4644661 L13.5355339,12.4644661 C12.4309644,12.4644661 11.5355339,11.5690356 11.5355339,10.4644661 L11.5355339,9.46446609 Z" fill="#000000" transform="translate(15.535534, 8.464466) rotate(-45.000000) translate(-15.535534, -8.464466) "></path>
                                                </g>
                                            </svg>
                                        </div>
                                        <div class="kt-notification__item-details">
                                            <div class="kt-notification__item-title">Content Words</div>
                                            <div class="kt-notification__item-time">
                                                {{ @$split_Query->number_words.' Words' }}
                                            </div>
                                        </div>
                                    </a>
                                    <a href="javascript:void(0);" class="kt-notification__item">
                                        <div class="kt-notification__item-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <path d="M12.4644661,14.5355339 L9.46446609,14.5355339 C8.91218134,14.5355339 8.46446609,14.9832492 8.46446609,15.5355339 C8.46446609,16.0878187 8.91218134,16.5355339 9.46446609,16.5355339 L12.4644661,16.5355339 L12.4644661,17.5355339 C12.4644661,18.6401034 11.5690356,19.5355339 10.4644661,19.5355339 L6.46446609,19.5355339 C5.35989659,19.5355339 4.46446609,18.6401034 4.46446609,17.5355339 L4.46446609,13.5355339 C4.46446609,12.4309644 5.35989659,11.5355339 6.46446609,11.5355339 L10.4644661,11.5355339 C11.5690356,11.5355339 12.4644661,12.4309644 12.4644661,13.5355339 L12.4644661,14.5355339 Z" fill="#000000" opacity="0.3" transform="translate(8.464466, 15.535534) rotate(-45.000000) translate(-8.464466, -15.535534) "></path>
                                                    <path d="M11.5355339,9.46446609 L14.5355339,9.46446609 C15.0878187,9.46446609 15.5355339,9.01675084 15.5355339,8.46446609 C15.5355339,7.91218134 15.0878187,7.46446609 14.5355339,7.46446609 L11.5355339,7.46446609 L11.5355339,6.46446609 C11.5355339,5.35989659 12.4309644,4.46446609 13.5355339,4.46446609 L17.5355339,4.46446609 C18.6401034,4.46446609 19.5355339,5.35989659 19.5355339,6.46446609 L19.5355339,10.4644661 C19.5355339,11.5690356 18.6401034,12.4644661 17.5355339,12.4644661 L13.5355339,12.4644661 C12.4309644,12.4644661 11.5355339,11.5690356 11.5355339,10.4644661 L11.5355339,9.46446609 Z" fill="#000000" transform="translate(15.535534, 8.464466) rotate(-45.000000) translate(-15.535534, -8.464466) "></path>
                                                </g>
                                            </svg>
                                        </div>
                                        <div class="kt-notification__item-details">
                                            <div class="kt-notification__item-title">Number of Backlinks</div>
                                            <div class="kt-notification__item-time">
                                                @if($split_Query->backlinks)
                                                    @if($split_Query->backlinks == 1)
                                                        {{$split_Query->backlinks.' Backlink'}}
                                                    @else 
                                                        {{$split_Query->backlinks.' Backlinks'}}
                                                    @endif
                                                @else
                                                    {{'0 Backlink'}}
                                                @endif
                                                
                                            </div>
                                        </div>
                                    </a>
                                    <a href="javascript:void(0);" class="kt-notification__item">
                                        <div class="kt-notification__item-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--brand">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <path d="M12,10.9996338 C12.8356605,10.3719448 13.8743941,10 15,10 C17.7614237,10 20,12.2385763 20,15 C20,17.7614237 17.7614237,20 15,20 C13.8743941,20 12.8356605,19.6280552 12,19.0003662 C11.1643395,19.6280552 10.1256059,20 9,20 C6.23857625,20 4,17.7614237 4,15 C4,12.2385763 6.23857625,10 9,10 C10.1256059,10 11.1643395,10.3719448 12,10.9996338 Z M13.3336047,12.504354 C13.757474,13.2388026 14,14.0910788 14,15 C14,15.9088933 13.7574889,16.761145 13.3336438,17.4955783 C13.8188886,17.8206693 14.3938466,18 15,18 C16.6568542,18 18,16.6568542 18,15 C18,13.3431458 16.6568542,12 15,12 C14.3930587,12 13.8175971,12.18044 13.3336047,12.504354 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                                    <circle fill="#000000" cx="12" cy="9" r="5"></circle>
                                                </g>
                                            </svg>
                                        </div>
                                        <div class="kt-notification__item-details">
                                            <div class="kt-notification__item-title">Order in Queue</div>
                                            <div class="kt-notification__item-time">
                                            @if( $split_Query->data_add_to_cart)
                                                @if( $split_Query->data_add_to_cart->count() < 1)        
                                                    {{$split_Query->data_add_to_cart->count().' Order'}}
                                                @else 
                                                    {{$split_Query->data_add_to_cart->count().' Orders'}}
                                                @endif
                                            @endif
                                            </div>
                                        </div>
                                    </a>
                                    <a href="javascript:void(0);" class="kt-notification__item">
                                        <div class="kt-notification__item-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <path d="M12,21 C7.581722,21 4,17.418278 4,13 C4,8.581722 7.581722,5 12,5 C16.418278,5 20,8.581722 20,13 C20,17.418278 16.418278,21 12,21 Z" fill="#000000" opacity="0.3"></path>
                                                    <path d="M13,5.06189375 C12.6724058,5.02104333 12.3386603,5 12,5 C11.6613397,5 11.3275942,5.02104333 11,5.06189375 L11,4 L10,4 C9.44771525,4 9,3.55228475 9,3 C9,2.44771525 9.44771525,2 10,2 L14,2 C14.5522847,2 15,2.44771525 15,3 C15,3.55228475 14.5522847,4 14,4 L13,4 L13,5.06189375 Z" fill="#000000"></path>
                                                    <path d="M16.7099142,6.53272645 L17.5355339,5.70710678 C17.9260582,5.31658249 18.5592232,5.31658249 18.9497475,5.70710678 C19.3402718,6.09763107 19.3402718,6.73079605 18.9497475,7.12132034 L18.1671361,7.90393167 C17.7407802,7.38854954 17.251061,6.92750259 16.7099142,6.53272645 Z" fill="#000000"></path>
                                                    <path d="M11.9630156,7.5 L12.0369844,7.5 C12.2982526,7.5 12.5154733,7.70115317 12.5355117,7.96165175 L12.9585886,13.4616518 C12.9797677,13.7369807 12.7737386,13.9773481 12.4984096,13.9985272 C12.4856504,13.9995087 12.4728582,14 12.4600614,14 L11.5399386,14 C11.2637963,14 11.0399386,13.7761424 11.0399386,13.5 C11.0399386,13.4872031 11.0404299,13.4744109 11.0414114,13.4616518 L11.4644883,7.96165175 C11.4845267,7.70115317 11.7017474,7.5 11.9630156,7.5 Z" fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </div>
                                        <div class="kt-notification__item-details">
                                            <div class="kt-notification__item-title">Turnaround Time</div>
                                            <div class="kt-notification__item-time">
                                               
                                                @php
                                                $myString = str_replace("_"," ",$split_Query->turnaround_time);
                                                $word = 'week';
                                                $word1 = 'Week';
                                                @endphp
                                                @if(strpos($myString, $word) !== false || strpos($myString, $word1) !== false )
                                                  {{ $myString }}
                                                @else
                                                  {{ $myString.' week' }}
                                                @endif
                                            </div>
                                        </div>
                                    </a>
                                    <a href="javascript:void(0);" class="kt-notification__item">
                                        <div class="kt-notification__item-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect opacity="0.200000003" x="0" y="0" width="24" height="24"></rect>
                                                    <path d="M4.5,7 L9.5,7 C10.3284271,7 11,7.67157288 11,8.5 C11,9.32842712 10.3284271,10 9.5,10 L4.5,10 C3.67157288,10 3,9.32842712 3,8.5 C3,7.67157288 3.67157288,7 4.5,7 Z M13.5,15 L18.5,15 C19.3284271,15 20,15.6715729 20,16.5 C20,17.3284271 19.3284271,18 18.5,18 L13.5,18 C12.6715729,18 12,17.3284271 12,16.5 C12,15.6715729 12.6715729,15 13.5,15 Z" fill="#000000" opacity="0.3"></path>
                                                    <path d="M17,11 C15.3431458,11 14,9.65685425 14,8 C14,6.34314575 15.3431458,5 17,5 C18.6568542,5 20,6.34314575 20,8 C20,9.65685425 18.6568542,11 17,11 Z M6,19 C4.34314575,19 3,17.6568542 3,16 C3,14.3431458 4.34314575,13 6,13 C7.65685425,13 9,14.3431458 9,16 C9,17.6568542 7.65685425,19 6,19 Z" fill="#000000"></path>
                                                </g>
                                            </svg>
                                        </div>
                                        <div class="kt-notification__item-details">
                                            <div class="kt-notification__item-title">Content Options</div>
                                            <div class="kt-notification__item-time">
                                                
                                                @php
                                                    $guest_content = [];

                                                    if($split_Query->guest_content == 'seller'){
                                                        $guest_content[] = $split_Query->guest_content;
                                                    } 
                                                    else {
                                                        if(unserialize($split_Query->guest_content)){
                                                            $guest_content = unserialize($split_Query->guest_content);
                                                        }
                                                    }
                                                    if($split_Query->guest_content == 'buyer'){
                                                        $guest_content[] = $split_Query->guest_content;
                                                    } 
                                                    if($split_Query->guest_content == 'hire_content'){
                                                        $guest_content[] = $split_Query->guest_content;
                                                    }              
                                                @endphp

                                                @if($guest_content)
                                                    @if(in_array('seller', $guest_content))
                                                        <span style="width: 171px;"><span class="kt-badge kt-badge--primary kt-badge--dot"></span>&nbsp;<span class="kt-font-bold kt-font-primary">Publisher</span></span>
                                                    @endif
                                                    @if(in_array('buyer', $guest_content))
                                                        <span style="width: 171px;margin-left: 1rem;"><span class="kt-badge kt-badge--primary kt-badge--dot"></span>&nbsp;<span class="kt-font-bold kt-font-primary">Advertiser</span></span>
                                                    @endif
                                                    @if(in_array('hire_content', $guest_content))
                                                        <span class="kt-badge  kt-badge--brand kt-badge--inline kt-badge--pill kt-badge--unified-primary kt-ml-10">Hire Content Writer</span>
                                                    @endif
                                                @endif
                                                
                                            </div>
                                        </div>
                                    </a>
                                    <div class="kt-notification__item">
                                        <div class="kt-notification__item-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" x="5" y="4" width="6" height="16" rx="1.5"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="13" y="4" width="6" height="16" rx="1.5"></rect>
                                                </g>
                                            </svg>
                                        </div>
                                        <div class="kt-notification__item-details">
                                            <div class="kt-notification__item-title">Guest Post Examples</div>
                                            <div class="kt-notification__item-time kt-mt-5">
                                                 
                                                @if(!empty($split_Query->exampleurlOne))
                                                    <a href="{{$split_Query->exampleurlOne}}" target="_blank"><span class="kt-badge  kt-badge--brand kt-badge--inline kt-badge--pill kt-badge--unified-primary kt-ml-10">Guest Post Example 1</span></a>
                                                @elseif(empty($split_Query->exampleurlOne))
                                                    <span class="kt-badge  kt-badge--brand kt-badge--inline kt-badge--pill kt-badge--unified-primary">NA</span>
                                                @endif
                                                @if(!empty($split_Query->exampleurlTwo))
                                                    <a href="{{$split_Query->exampleurlTwo}}" target="_blank"><span class="kt-badge  kt-badge--brand kt-badge--inline kt-badge--pill kt-badge--unified-primary kt-ml-10">Guest Post Example 2</span></a>
                                                @elseif(empty($split_Query->exampleurlTwo))
                                                      <span class="kt-badge  kt-badge--brand kt-badge--inline kt-badge--pill kt-badge--unified-primary kt-ml-10">NA</span> 
                                                @endif
                                            </div>
                                        </div>                                       
                                    </div>
                                    <div class="kt-notification__item">
                                        <div class="kt-notification__item-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <rect x="0" y="0" width="24" height="24"></rect>
                                                    <rect fill="#000000" x="5" y="4" width="6" height="16" rx="1.5"></rect>
                                                    <rect fill="#000000" opacity="0.3" x="13" y="4" width="6" height="16" rx="1.5"></rect>
                                                </g>
                                            </svg>
                                        </div>
                                        <div class="kt-notification__item-details">
                                            <div class="kt-notification__item-title">Guidelines</div>
                                            <div class="kt-notification__item-time kt-mt-5">
                                                 

                                                @if(!empty($split_Query->domain_guideline_url))
                                                    <a href="{{$split_Query->domain_guideline_url}}" target="_blank"><span class="kt-badge  kt-badge--brand kt-badge--inline kt-badge--pill kt-badge--unified-primary kt-ml-10">Guideline Url</span></a>
                                                @elseif(empty($split_Query->domain_guideline_url))
                                                    <span class="kt-badge  kt-badge--brand kt-badge--inline kt-badge--pill kt-badge--unified-primary kt-ml-10">NA</span>
                                                @endif

                                            </div>
                                        </div>                                       
                                    </div>

                                </div>
                                <div class="kt-pt20
                                    d-flex" style="
                                    justify-content: space-between;
                                    ">
                                    <!-- <a href="javascript:void(0);" class="btn btn-label-success btn-bold" data-toggle="modal" data-target="#kt_chat_modal">Chat (Online) soon..</a> -->
                                    @if(Auth::check())
                                            <?php
                                                $domain_id = \Crypt::encrypt($split_Query->domain_id);
                                                $other_user_id = \Crypt::encrypt($value->user_id);
                                                $auth_user_id = \Crypt::encrypt(Auth::user()->user_id);
                                                $id = \Crypt::encrypt($domain_id . '_' . $other_user_id . '_' . $auth_user_id);
                                            ?>
                                        <!-- <a href="{{url('chatroom').'/'.$id}}" data-domain-id="{{$split_Query->domain_id}}" data-other-user="{{$value->user_id}}" data-auth-user="{{Auth::user()->user_id}}"  class="btn btn-label-success btn-bold chatroom" >Chat (Online)</a> -->
                                        @if(Auth::user()->user_id != $value->user_id)
                                            <a href="{{url('chatroom').'/'.$id}}" class="btn btn-label-success btn-bold chatroom" target="_blank">Chat (Online)</a>
                                        @else
                                            <a href="javascript:void(0);" class="btn btn-label-success btn-bold disabled">Chat (Online)</a>
                                        @endif

                                    @else
                                        <a href="javascript:void(0);" class="btn btn-label-success btn-bold chat">Chat (Online)</a>
                                        
                                    @endif
                                    
                                    @if(Auth::check())
                                        @if(Auth::user()->user_id != $value->user_id)
                                            
                                            @if(Auth::user()->switched == 'advertiser' && Auth::user()->user_type != 'admin')
                                               <!-- <a href="javascript:void(0)" class="btn btn-brand btn-upper continue" data-domain-id="{{$split_Query->domain_id}}" data-domain-seller-id="{{$value->user_id}}" data-domain-price="{{$split_Query->cost_price}}"  data-domain-extra-cost="{{$split_Query->extra_cost}}" data-domain-content="{{$split_Query->number_words}}" data-grand-total-with-extra-cost="{{buyerDomainPriceCalculate($split_Query->cost_price,$split_Query->extra_cost )}}" data-grand-total-without-extra-cost="{{buyerDomainWebsitePriceCalculate($split_Query->cost_price )}}">Continue
                                                 (${{buyerDomainWebsitePriceCalculate(@$split_Query->cost_price)}})</a> -->
                                                 <!-- <a href="{{url('/search/website').'/'.$split_Query->domain_url.'/'.$next_domain_id}}" class="btn btn-brand btn-upper" >Continue
                                             (${{buyerDomainWebsitePriceCalculate(@$split_Query->cost_price)}})</a> -->
                                             <a href="javascript:void(0);" class="btn btn-brand btn-upper redirect-continue" data-domain-id="{{$next_domain_id}}" data-domain-url="{{$split_Query->domain_url}}">Continue
                                             (${{buyerDomainWebsitePriceCalculate(@$split_Query->cost_price)}})</a>
                                            @else
                                                <a href="javascript:void(0)" class="btn btn-brand btn-upper advertiser" >Continue
                                                 (${{buyerDomainWebsitePriceCalculate(@$split_Query->cost_price)}})</a>
                                            @endif
                                        @else
                                            <a href="javascript:void(0)" class="btn btn-brand btn-upper disabled" >Continue
                                        (${{buyerDomainWebsitePriceCalculate(@$split_Query->cost_price)}})</a>
                                        @endif
                                    @else

                                        <!-- <a href="javascript:void(0)" class="btn btn-brand btn-upper continue" data-domain-id="{{$split_Query->domain_id}}" data-domain-seller-id="{{$value->user_id}}" data-domain-price="{{$split_Query->cost_price}}"  data-domain-extra-cost="{{$split_Query->extra_cost}}" data-domain-content="{{$split_Query->number_words}}" data-grand-total-with-extra-cost="{{buyerDomainPriceCalculate($split_Query->cost_price,$split_Query->extra_cost )}}" data-grand-total-without-extra-cost="{{buyerDomainWebsitePriceCalculate($split_Query->cost_price )}}">Continue
                                                 (${{buyerDomainWebsitePriceCalculate(@$split_Query->cost_price)}})</a> -->
                                        <!-- <a href="{{url('/search/website').'/'.$split_Query->domain_url.'/'.$next_domain_id}}" class="btn btn-brand btn-upper" >Continue
                                             (${{buyerDomainWebsitePriceCalculate(@$split_Query->cost_price)}})</a> -->
                                             <a href="javascript:void(0);" class="btn btn-brand btn-upper redirect-continue" data-domain-id="{{$next_domain_id}}" data-domain-url="{{$split_Query->domain_url}}">Continue
                                             (${{buyerDomainWebsitePriceCalculate(@$split_Query->cost_price)}})</a>

                                    @endif

                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @else
            No Publisher found for this website...
        @endif
@endforeach
