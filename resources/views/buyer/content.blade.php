@extends('layout.app')
@php
$totalTitle = ' Looking for reliable website details on GuestPostEngine?';
@endphp
@section('title', $totalTitle)
@section('content')
@section('mainhead')
@parent

@section('title-description')

<meta name="description" content="Get the appropriate website details on GuestPostEngine by exploring a wide range of quality websites with high domain authority, category rank, etc.">
    
@endsection


<!-- <link rel="shortcut icon" href="https://www.guestpostengine.com/images/favicon.ico" /> -->
@show
<!-- Authentication Links --> 
@if(session()->has('message'))
    <?php
        $form = session()->get('form');
        if($form == ''){
            $form = 'signin';
            $result = 1;
            $message = 'Password has been changed.';
            $alert = 'success';
        } else {
            $result = 0;
        }
        
    ?>
@else 
    <?php
        $form = 'signin';
        $result = 0;
    ?>
@endif
<style>
.checked {
  color: orange;
}
</style>
<div class="kt-body website-details-wrapper kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
            </div>
        </div>
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">
            <!--begin::Portlet-->
            <div class="" data-sticky-container>
                <div class="row position-relative">
                    <div class="col-12 col-lg-1"></div>
                    <div class="col-12 col-lg-10">
                        <div class="kt-portlet ">
                            <div class="kt-portlet__body">
                                <h2 class="pulish_title">Content Management</h2>
                                <form class="kt-form kt-login__signin" method="POST" action="{{ url('advertiser/content')  }}" id="setpolicyform">
                                @csrf  
                                    <span id="alert-show-add"></span>
                                    
                                    <div class="col-12 kt-mt-40">
                                        <div class="modal_buyer" >
                                            <div class="container kt-padding-20">
                                                <div class="row">
                                                    <input id="order_id" name="order_id" type="hidden" value="{{$order_id}}">
                                                    <div class="form-group col-12">
                                                        <div class="input-group">
                                                            <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ @$order_details->title }}" autocomplete="off" placeholder="Article Title" maxlength="70" >
                                                        </div>
                                                        <span class="title-error"></span>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-6">
                                                        <div class="input-group">
                                                            <input id="url" type="text" class="form-control @error('url') is-invalid @enderror" name="url" value="{{ @$order_details->url }}" autocomplete="off" placeholder="Target URL" maxlength="200">
                                                        </div>
                                                        <span class="url-error"></span>
                                                    </div>
                                                    <div class="form-group col-6">
                                                        <div class="input-group">
                                                            <input id="keyword" type="text" class="form-control @error('keyword') is-invalid @enderror" name="keyword" value="{{ @$order_details->keyword }}" autocomplete="off" placeholder="Keyword" maxlength="70">
                                                        </div>
                                                        <span class="keyword-error"></span>
                                                    </div>
                                                </div>  
                                                 <div class="row">
                                                    <div class="col-12 text-center kt-mt-20 d-flex">
                                                        <div class="form-group col-12">
                                                            <div class="input-group lnbrd">
                                                                <textarea placeholder="Article Body" name="cktext" id="cktext" @if($order_details->content_type == 'seller') {{'disabled'}} @endif>
                                                                    {{@$order_details->content}}
                                                                </textarea>
                                                            </div>
                                                            <span class="cktext-error">
                                                                
                                                            </span>
                                                            @if($order_details->content_type == 'seller')
                                                                <button class="btn btn-label-brand kt-mt-10 kt-mr-10" id="approve" data-id="{{$order_id}}">Approve</button>
                                                                <a class="btn btn-label-primary kt-mt-10 kt-mr-10" id="disapprove" data-id="{{$order_id}}">Disapprove</a>
                                                            @endif
                                                        </div>

                                                    </div>
                                                </div>                                        
                                            </div>
                                        </div>
                                        <div class="col-12 text-center kt-mt-20 d-flex justify-content-center">
                                            <button type="submit" class="btn btn-label-brand kt-mt-10 kt-mr-10" id="content_save" >Update</button>
                                            <a class="btn btn-label-primary kt-mt-10 kt-mr-10" href="{{url('advertiser/purchases')}}">Cancel</a>
                                        </div>
                                    </div>
                                </form>
                                <!-- end -->
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-1"></div>
                </div>
            </div>
            
            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--ENd:: Chat-->
@section('mainscripts')
@parent
@section('customScript')
<script src="https://cdn.ckeditor.com/4.9.2/standard/ckeditor.js"></script> 
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>      
<!-- Include CKEditor and jQuery adapter -->
<!-- <script src="//cdn.ckeditor.com/4.4.3/basic/ckeditor.js"></script> -->
<!-- <script src="//cdn.ckeditor.com/4.4.3/basic/adapters/jquery.js"></script> -->

<script>  
    CKEDITOR.replace('cktext');
</script>  

<!--begin::Page Scripts(used by this page) -->
<script src="{{ asset('') . config('app.public_url') . '/assets/js/pages/custom/login/login-general.js'  }}" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<!--end::Page Scripts -->
<script type="text/javascript">


$(document).on('click','#content_save',function(e) {    
    e.preventDefault();
    var btn = $(this);
    var form = $(this).closest('form');
    form.validate({
        // Validate only visible fields
        ignore: [],
        debug: false,
        rules: {
            url: {
                required: true,
                maxlength:200,
                url: '/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/'
            },
            keyword: {
                required: true,
                maxlength:70
            },
            title: {
                required: true,
                maxlength:70,
            },
            cktext:{
                required: function() {
                    CKEDITOR.instances.cktext.updateElement();
                },
                maxlength:"60000",
            }
            // cktext: {  
            //     //ckrequired: true //CKEDITORustom required field   
            //     ckrequired: true, //CKEDITORustom required field   
            
            // }
        },

        messages: {
            // cktext:{
            //     //required:"Please enter Text",
            //     minlength:"Please enter 10 characters"
            // }
        },
        onkeyup: true,
        onfocusout: true,
        onclick: true
    });


    if (!form.valid()) {
        return;
    }
    //form.submit();
    $('.loader').fadeIn('slow');
    $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{ url('advertiser/content') }}",
            type: 'POST',
            async: true,
            dataType: 'json',
            enctype: 'multipart/form-data',
            data: {
                url: $('#url').val(),
                keyword: $('#keyword').val(),
                title: $('#title').val(),
                cktext: $('#cktext').val(),
                order_id: $('#order_id').val()
            },
            cache: false,
            success: function(response) {                
                
                if(response.status == false){
                    if(response.errors){
                        if(response.errors.keyword){
                           $('.keyword-error').html('<span class="error" role="alert"><strong>'+response.errors.keyword[0]+'</strong></span>');
                        }
                        if(response.errors.url){
                            
                            $('.url-error').html('<span class="error" role="alert"><strong>'+response.errors.url[0]+'</strong></span>');
                        }
                        if(response.errors.title){                                
                            $('.title-error').html('<span class="error" role="alert"><strong>'+response.errors.title[0]+'</strong></span>');
                        }
                        if(response.errors.cktext){
                            swal.fire({
                                "title": "",
                                "text": response.errors.cktext[0],
                                "type": "danger",
                                "confirmButtonClass": "btn btn-secondary"
                            });
                            $('.cktext-error').html('<span class="error" role="alert"><strong>'+response.errors.cktext[0]+'</strong></span>');
                        }
                    }
                }
                var alert = 'alert-outline-' + response.alert;
                var html_message = response.html;
                var message = response.message;
                var alert = '<div class="alert ' + alert + ' fade show" role="alert"><div class="alert-icon"><i class="flaticon-warning"></i></div><div class="alert-text message">' + response.message + '</div><div class="alert-close"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="la la-close"></i></span></button></div></div>';
                $('#alert-show-add').html(alert);
                
                $('#partials-profile').html(html_message);
                KTUtil.scrollTop();
                $('.loader').fadeOut('slow');
                setTimeout(function(){ 
                    window.location.href = "{{url('advertiser/purchases')}}";
                }, 1000);

            },
            error: function() {}
        });
    
    e.stopImmediatePropagation();
    return false;
});

        //Extention method for check CKEditor Control   
        //jQuery.validator.addMethod("customfunctionanme",validationfunction,validationmessage);  
  
        jQuery.validator.addMethod("ckrequired", function (value, element) {  
            var number_words = "{{$number_words}}";
            var idname = $(element).attr('id');  
            var editor = CKEDITOR.instances[idname];  
            var ckValue = GetTextFromHtml(editor.getData()).replace(/<[^>]*>/gi, '').trim();
            if (ckValue.length === 0) {  
                //if empty or trimmed value then remove extra spacing to current control  
                $(element).val(ckValue);  
            } else {
                //If not empty then leave the value as it is                
                $(element).val(editor.getData());                
            }  
            return $(element).val().length > 0;  
        }, "This field is required");  
  
        function GetTextFromHtml(html) {  
            var dv = document.createElement("DIV");  
            dv.innerHTML = html;  
            return dv.textContent || dv.innerText || "";  
        }  


$(document).on('click','#disapprove',function(e){
    var order_id = $(this).attr('data-id');
    Swal.fire({
      title: 'Are you sure?',
      html: "You want to disapprove this content?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonClass: 'btn btn-secondary',
      cancelButtonClass: 'btn btn-secondary',
      confirmButtonText: 'Yes'

    }).then((result) => {
        Swal.fire({
          title: 'Comment',
          input: 'text',
          inputAttributes: {
            autocapitalize: 'off'
          },
          showCancelButton: true,
          confirmButtonText: 'Done',
          showLoaderOnConfirm: true,
          preConfirm: (login) => {
                if(login == ''){
                    Swal.showValidationMessage(
                      `Request failed: Not found`
                    )
                    return false;
                } 
          },
          allowOutsideClick: () => !Swal.isLoading()
        }).then((result) => {
            if (result.value) {
                var url = "{{url('stages-changes')}}";
                $('.loader').fadeIn('slow'); 
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url  : url,
                    type : 'POST',
                    data : {
                        'order_id'      : order_id,
                        'stages'        : 6,
                        'comment'       : result.value,
                    },
                    async: true,
                    dataType: 'json',
                    enctype: 'multipart/form-data',
                    cache: false, 
                    success:function(response){
                        $('.loader').fadeOut('slow'); 
                        if(response.status === true) {
                            swal.fire({
                              title: '',
                              html: "Content has been disapproved",
                              icon: 'success',
                              showCancelButton: false,
                              confirmButtonClass: "btn btn-secondary",
                              confirmButtonText: "Ok"
                            }).then((result) => {
                               window.location.href = "{{url('advertiser/purchases')}}";
                            });
                        } 
                    },
                    error:function(){
                        Swal.showValidationMessage(
                          `Request failed: Something were wrong`
                        )
                    }
                });
            }
        });
    });
    return false;
});
$(document).on('click','#approve',function(e){
    var order_id = $(this).attr('data-id');
    Swal.fire({
      title: 'Are you sure?',
      html: "You want to approve this content?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonClass: 'btn btn-secondary',
      cancelButtonClass: 'btn btn-secondary',
      confirmButtonText: 'Yes'

    }).then((result) => {
        
            if (result.value) {
                var url = "{{url('stages-changes')}}";
                $('.loader').fadeIn('slow'); 
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url  : url,
                    type : 'POST',
                    data : {
                        'order_id'      : order_id,
                        'stages'        : 5,
                        'comment'       : result.value,
                    },
                    async: true,
                    dataType: 'json',
                    enctype: 'multipart/form-data',
                    cache: false, 
                    success:function(response){
                        $('.loader').fadeOut('slow'); 
                        if(response.status === true) {
                            swal.fire({
                              title: '',
                              html: "Content has been approved",
                              icon: 'success',
                              showCancelButton: false,
                              confirmButtonClass: "btn btn-secondary",
                              confirmButtonText: "Ok"
                            }).then((result) => {
                               window.location.href = "{{url('advertiser/purchases')}}";
                            });
                        } 
                    },
                    error:function(){
                        Swal.showValidationMessage(
                          `Request failed: Something were wrong`
                        )
                    }
                });
            }
    });
    return false;
});

</script>

@endsection

@show
@endsection