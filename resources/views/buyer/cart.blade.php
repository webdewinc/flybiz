@extends('layout.app')
@section('title', 'Add to Cart - GuestPostEngine')
@section('content')
@section('mainhead')

@parent
    <!--begin::Page Vendors Styles(used by this page) -->
    <link href="{{ asset( 'public/assets/css/owl.carousel.min.css' ) }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset( 'public/assets/css/pages/pricing/pricing-2.css' ) }}" rel="stylesheet" type="text/css" />
        <!--end::Page Vendors Styles -->

     <style type="text/css">
        .panel-title {
            display: inline;
            font-weight: bold;
        }

        .display-table {
            display: table;
        }
        .display-tr {
            display: table-row;
        }
        .display-td {
            display: table-cell;
            vertical-align: middle;
            width: 61%;
        }
        .hide{
            display:none;
        }
        span.paypal-button-text{
            display:none;
        }
        #append{
            height:100%;
        }
        input#apply_coupon {
            width: 100%;
        }
    </style>
@show
@section('title-description')

<meta name="description" content="At GuestPostEngine marketplace, add to cart section manifest you about how many websites are you keen to buy at a particular time. Feel free to append your website for future use.">
    
@endsection
<!-- <h3 id="show_msg" class="text-success text-center"></h3> -->
<span id="append">
    <div class="kt-container  kt-grid__item kt-grid__item--fluid cart-container">
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        </div>
        <div class="row" id="append">
            <div class="col-md-8 col-12">
                <div class="row">
                    @php
                    $total_price = 0;
                    $count = 0;
                    $domain_arr= [];
                    @endphp
                    @foreach($cartList as $key => $split_Query)
                        @if($split_Query->content_type == 'buyer')

                             @if($split_Query->promo_code > 0)
                                @php
                                    $promo_number = DB::table('promo_codes')->where('promo_id',$split_Query->promo_code)->value('promo_number');
                                    $cost = buyerDomainWebsitePriceCalculate($split_Query->cost_price);
                                @endphp
                               
                                @php                                    
                                    $discountPrice = $cost * $promo_number / 100;
                                    $priceLeft     = round($cost - $discountPrice);
                                    $cost = $priceLeft;
                                @endphp                                
                            @else
                                @php
                                    $cost = buyerDomainWebsitePriceCalculate($split_Query->cost_price);
                                @endphp
                            @endif                           

                        @elseif($split_Query->content_type == 'seller')
                            @php
                                $extra_cost = 0;
                            @endphp
                            @if(!empty($split_Query->extra_cost))
                                @php
                                    $extra_cost = $split_Query->extra_cost;
                                @endphp
                            @endif

                            @if($split_Query->promo_code > 0)
                                @php
                                    $promo_number = DB::table('promo_codes')->where('promo_id',$split_Query->promo_code)->value('promo_number');
                                    $cost = buyerDomainPriceCalculate($split_Query->cost_price, $extra_cost);
                                @endphp
                                @php
                                    $discountPrice = $cost * $promo_number / 100;
                                    $priceLeft     = round($cost - $discountPrice);
                                    $cost = $priceLeft;
                                @endphp
                            @else
                                @php
                                    $cost = buyerDomainPriceCalculate($split_Query->cost_price, $extra_cost);
                                @endphp
                            @endif

                        @endif

                        @php
                            $total_price += $cost;
                        @endphp

                        @php
                        $domain_arr[$count]['domain_id'] = $split_Query->domain_id; 
                        $domain_arr[$count]['domain']    = $split_Query->domain_url;
                        $domain_arr[$count]['status']    = $split_Query->domain_status;
                         
                        @endphp
                        <!--begin:: Portlet-->
                        <div class="kt-portlet">
                            <div class="kt-portlet__body">
                                <div class="kt-widget kt-widget--user-profile-3">
                                    <div class="kt-widget__top">
                                       
                                        <div class="kt-widget__content">
                                            <div class="kt-widget__head">
                                                <div class="d-flex align-items-center flex-wrap">
                                                    <!-- <div class="kt-widget__media kt-hidden-">
                                                        <img src="https://d1gwm4cf8hecp4.cloudfront.net/images/favicons/favicon-16x16.png" alt="image">
                                                    </div> -->
                                                    <div class="kt-widget__media kt-hidden-">
                                                        <img width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon favicon-svg" src="<?php echo 'https://www.google.com/s2/favicons?domain='. $split_Query->domain_url; ?>" />
                                                    </div>
                                                   <!--  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon favicon-svg kt-mr-5">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"></rect>
                                                            <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="9"></circle>
                                                            <path d="M11.7357634,20.9961946 C6.88740052,20.8563914 3,16.8821712 3,12 C3,11.9168367 3.00112797,11.8339369 3.00336944,11.751315 C3.66233009,11.8143341 4.85636818,11.9573854 4.91262842,12.4204038 C4.9904938,13.0609191 4.91262842,13.8615942 5.45804656,14.101772 C6.00346469,14.3419498 6.15931561,13.1409372 6.6267482,13.4612567 C7.09418079,13.7815761 8.34086797,14.0899175 8.34086797,14.6562185 C8.34086797,15.222396 8.10715168,16.1034596 8.34086797,16.2636193 C8.57458427,16.423779 9.5089688,17.54465 9.50920913,17.7048097 C9.50956962,17.8649694 9.83857487,18.6793513 9.74040201,18.9906563 C9.65905192,19.2487394 9.24857641,20.0501554 8.85059781,20.4145589 C9.75315358,20.7620621 10.7235846,20.9657742 11.7357634,20.9960544 L11.7357634,20.9961946 Z M8.28272988,3.80112099 C9.4158415,3.28656421 10.6744554,3 12,3 C15.5114513,3 18.5532143,5.01097452 20.0364482,7.94408274 C20.069657,8.72412177 20.0638332,9.39135321 20.2361262,9.6327358 C21.1131932,10.8600506 18.0995147,11.7043158 18.5573343,13.5605384 C18.7589671,14.3794892 16.5527814,14.1196773 16.0139722,14.886394 C15.4748026,15.6527403 14.1574598,15.137809 13.8520064,14.9904917 C13.546553,14.8431744 12.3766497,15.3341497 12.4789081,14.4995164 C12.5805657,13.664636 13.2922889,13.6156126 14.0555619,13.2719546 C14.8184743,12.928667 15.9189236,11.7871741 15.3781918,11.6380045 C12.8323064,10.9362407 11.963771,8.47852395 11.963771,8.47852395 C11.8110443,8.44901109 11.8493762,6.74109366 11.1883616,6.69207022 C10.5267462,6.64279981 10.170464,6.88841096 9.20435656,6.69207022 C8.23764828,6.49572949 8.44144409,5.85743687 8.2887174,4.48255778 C8.25453994,4.17415686 8.25619136,3.95717082 8.28272988,3.80112099 Z M20.9991771,11.8770357 C20.9997251,11.9179585 21,11.9589471 21,12 C21,16.9406923 17.0188468,20.9515364 12.0895088,20.9995641 C16.970233,20.9503326 20.9337111,16.888438 20.9991771,11.8770357 Z" fill="#000000" opacity="0.3"></path>
                                                        </g>
                                                    </svg> -->
                                                    <a href="{{url('search/website/')}}{{'/'.$split_Query->domain_url}}" class="kt-widget__username">
                                                    {{$split_Query->domain_url}}
                                                    {{domain_status($split_Query->domain_url)}}
                                                    </a>&nbsp;
                                                    <span>
                                                        @if(!empty($split_Query->backlink_type))
                                                            @if($split_Query->backlink_type == 'no_follow')
                                                                @php
                                                                    $follow = 'No Follow';
                                                                    $class = 'warning';
                                                                @endphp                          
                                                            @elseif($split_Query->backlink_type == 'follow')
                                                                @php
                                                                    $follow = 'Do Follow';
                                                                    $class = 'success';
                                                                @endphp
                                                            @endif
                                                        @else
                                                            @php
                                                                $follow = 'No Follow';
                                                                $class = 'warning';
                                                            @endphp
                                                        @endif

                                                        <span class="kt-badge kt-badge--{{$class}} kt-badge--dot"></span>&nbsp;
                                                        <span class="kt-font-bold kt-font-{{$class}}">
                                                           {{$follow}} 
                                                        </span>
                                                    </span>
                                                     @php
                                                        $promo_number = DB::table('promo_codes')->where('promo_id',$split_Query->promo_code)->value('promo_number');
                                                        @endphp 
                                                        @if( $split_Query->promo_code > 0)
                                                        <div class="alert alert-primary kt-margin-0 kt-padding-5 kt-ml-15" role="alert"><div class="alert-text">Promo Code Applied with discount {{ $promo_number }}% </div></div>
                                                        @endif

                                                </div>
                                                
                                                <div class="kt-widget__info">

                                                    <div class="kt-widget__action d-flex align-items-center">
                                                        <div class="website-price" style="font-size: 22px;">  <div class="kt-notification__item-time">
                                                                ${{$cost}}
                                                            </div>
                                                        </div>
                                                         <a href="javascript:void(0);" class="delete btn btn-sm btn-clean btn-icon btn-icon-md" rel="{{$split_Query->id}}">
                                                        <i class="la la-trash-o" style="font-size: 1.6rem;" title="Delete"></i>
                                                        </a>    
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-widget__bottom">
                                        <div class="kt-widget__item">
                                            <div class="kt-widget__icon">
                                                <svg class="moz-svg" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 280.6 280.3" style="enable-background:new 0 0 280.6 280.3;" width="30px" height="30px" xml:space="preserve">
                                                    <path class="st0" d="M140.2,280.3C66.1,282.2-0.6,217.8,0,138.9C0.6,64.8,64.4-2.1,144.5,0c71.7,1.9,136.8,63.8,136.1,141.7
                                                        C280,218.2,214.6,282.3,140.2,280.3z M185.1,139.7c0.4,0.1,0.7,0.3,1.1,0.4c0,13.1,0.1,26.2-0.1,39.3c-0.1,4.7,2.1,6.7,6.6,6.6
                                                        c6.3-0.1,12.7-0.1,19,0c2.5,0,3.6-0.7,3.5-3.4c-0.1-28.3-0.1-56.6,0-85c0-2.8-1.2-3.3-3.6-3.3c-5.2,0.1-10.3,0.1-15.5,0
                                                        c-6.6-0.1-11.8,2.3-16.2,7.3c-12,13.6-24.3,27-36.3,40.6c-2.6,2.9-4,3.3-6.8,0.1c-12.1-13.8-24.5-27.3-36.6-41
                                                        c-3.6-4.1-7.9-6.7-13.3-6.9c-6.2-0.3-12.3,0-18.5-0.1c-2.4,0-3.2,0.8-3.2,3.2c0.1,28.3,0.1,56.6,0,85c0,2.6,0.8,3.5,3.4,3.5
                                                        c6.3-0.1,12.7,0,19-0.1c5.2-0.1,6.6-1.7,6.6-7.1c0-11.8,0-23.7,0.1-35.5c0-1.2-0.8-2.6,1-3.7c12.9,14.4,25.8,28.7,38.6,43.1
                                                        c4.2,4.7,8,4.8,12.4-0.1C159.4,168.4,172.3,154.1,185.1,139.7z"></path>
                                                    <path class="st1" d="M185.1,139.7c-12.9,14.4-25.8,28.7-38.7,43.1c-4.3,4.8-8.2,4.8-12.4,0.1c-12.9-14.4-25.8-28.7-38.6-43.1
                                                        c-1.8,1-1,2.5-1,3.7c-0.1,11.8,0,23.7-0.1,35.5c0,5.4-1.4,7-6.6,7.1c-6.3,0.1-12.7-0.1-19,0.1c-2.6,0.1-3.4-0.9-3.4-3.5
                                                        c0.1-28.3,0.1-56.6,0-85c0-2.4,0.8-3.2,3.2-3.2c6.2,0.1,12.3-0.2,18.5,0.1c5.4,0.2,9.7,2.8,13.3,6.9c12.2,13.7,24.6,27.2,36.6,41
                                                        c2.8,3.2,4.2,2.8,6.8-0.1c12-13.6,24.3-27,36.3-40.6c4.4-5,9.7-7.4,16.2-7.3c5.2,0.1,10.3,0.1,15.5,0c2.4-0.1,3.6,0.4,3.6,3.3
                                                        c-0.1,28.3-0.1,56.6,0,85c0,2.7-1.1,3.4-3.5,3.4c-6.3-0.1-12.7-0.1-19,0c-4.6,0.1-6.7-1.9-6.6-6.6c0.1-13.1,0.1-26.2,0.1-39.3
                                                        C185.9,140,185.5,139.8,185.1,139.7z"></path>
                                                </svg>
                                            </div>
                                            <div class="kt-widget__details">
                                                <div class="d-flex"><span class="kt-widget__value">{{$split_Query->dp_da}}</span>
                                                    <span class="d-flex align-items-end">/100</span>
                                                </div>
                                                <span class="kt-widget__title">Domain Authority</span>
                                            </div>
                                        </div>
                                        <div class="kt-widget__item">
                                            <div class="kt-widget__icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="34px" height="34px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <path d="M13,18.9450712 L13,20 L14,20 C15.1045695,20 16,20.8954305 16,22 L8,22 C8,20.8954305 8.8954305,20 10,20 L11,20 L11,18.9448245 C9.02872877,18.7261967 7.20827378,17.866394 5.79372555,16.5182701 L4.73856106,17.6741866 C4.36621808,18.0820826 3.73370941,18.110904 3.32581341,17.7385611 C2.9179174,17.3662181 2.88909597,16.7337094 3.26143894,16.3258134 L5.04940685,14.367122 C5.46150313,13.9156769 6.17860937,13.9363085 6.56406875,14.4106998 C7.88623094,16.037907 9.86320756,17 12,17 C15.8659932,17 19,13.8659932 19,10 C19,7.73468744 17.9175842,5.65198725 16.1214335,4.34123851 C15.6753081,4.01567657 15.5775721,3.39010038 15.903134,2.94397499 C16.228696,2.49784959 16.8542722,2.4001136 17.3003976,2.72567554 C19.6071362,4.40902808 21,7.08906798 21,10 C21,14.6325537 17.4999505,18.4476269 13,18.9450712 Z" fill="#000000" fill-rule="nonzero"></path>
                                                        <circle fill="#000000" opacity="0.3" cx="12" cy="10" r="6"></circle>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="kt-widget__details">
                                                <span class="kt-widget__value">
                                                    @php
                                                        $global_rank = 0;
                                                    @endphp
                                                    @if(@$split_Query->sd_global_rank)
                                                        @php
                                                            $global_rank = unserialize(@$split_Query->sd_global_rank);
                                                            $global_rank = @$global_rank['rank'];
                                                        @endphp
                                                    @endif
                                                    {{$global_rank}}
                                                </span>
                                                <span class="kt-widget__title">Global Rank</span>
                                            </div>
                                        </div>
                                        <div class="kt-widget__item">
                                            <div class="kt-widget__icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <path d="M4.00246329,12.2004927 L13,14 L13,4.06189375 C16.9463116,4.55399184 20,7.92038235 20,12 C20,16.418278 16.418278,20 12,20 C7.64874861,20 4.10886412,16.5261253 4.00246329,12.2004927 Z" fill="#000000" opacity="0.3"></path>
                                                        <path d="M3.0603968,10.0120794 C3.54712466,6.05992157 6.91622084,3 11,3 L11,11.6 L3.0603968,10.0120794 Z" fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="kt-widget__details">
                                                 @if($split_Query->sd_visits)
                                                    <span class="kt-widget__value">{{@$split_Query->sd_visits}}</span>
                                                    @else
                                                    <span class="kt-widget__value">0</span>
                                                    @endif
                                                <span class="kt-widget__title">Total Traffic</span>
                                            </div>
                                        </div>
                                        <div class="kt-widget__item">
                                            <div class="kt-widget__icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                        <path d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z" fill="#000000" fill-rule="nonzero"></path>
                                                        <path d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z" fill="#000000" opacity="0.3"></path>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="kt-widget__details">
                                                <span class="kt-widget__value">
                                                    {{totalOrderCount($split_Query->domain_url)}}
                                                </span>
                                                <span class="kt-widget__title">Orders in Queue</span>
                                            </div>
                                        </div>


                                        <div class="kt-widget__item">
                                            <div class="kt-widget__details">
                                                <span class="kt-widget__value">
                                                    Article Title
                                                </span>
                                                <span class="kt-widget__title">{{$split_Query->title}}</span>
                                            </div>
                                        </div>
                                        <div class="kt-widget__item">
                                            <div class="kt-widget__details">
                                                <span class="kt-widget__value">
                                                    Target URL
                                                </span>
                                                <span class="kt-widget__title">
                                                    <a href="{{$split_Query->url}}">Target URL</a>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end:: Portlet-->
                    @php
                    $count++;
                    @endphp
                    @endforeach
                </div>
            </div>
            @foreach($cartList as $get_cartList)
            @php
            $domainID[] = array('id'=>$get_cartList->domain_id);
            @endphp
            @endforeach
            <input type="hidden" name="domain_inCart" id="domain_inCart" value="" />
            <div class="col-md-4 col-12">
            <div class="fixed-sidebar">
                <section>
                    <div class="kt-portlet kt-portlet--tabs kt-portlet--height-fluid">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Order Details
                                </h3>
                            </div>
                        </div>
                        <div class="kt-portlet__body">
                            <div class="kt-notification kt-mb-20">
                                <a href="javascript:void(0);" class="kt-notification__item">
                                    <div class="kt-notification__item-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <rect x="0" y="0" width="24" height="24"/>
                                                <path d="M12,4.56204994 L7.76822128,9.6401844 C7.4146572,10.0644613 6.7840925,10.1217854 6.3598156,9.76822128 C5.9355387,9.4146572 5.87821464,8.7840925 6.23177872,8.3598156 L11.2317787,2.3598156 C11.6315738,1.88006147 12.3684262,1.88006147 12.7682213,2.3598156 L17.7682213,8.3598156 C18.1217854,8.7840925 18.0644613,9.4146572 17.6401844,9.76822128 C17.2159075,10.1217854 16.5853428,10.0644613 16.2317787,9.6401844 L12,4.56204994 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                                <path d="M3.28077641,9 L20.7192236,9 C21.2715083,9 21.7192236,9.44771525 21.7192236,10 C21.7192236,10.0817618 21.7091962,10.163215 21.6893661,10.2425356 L19.5680983,18.7276069 C19.234223,20.0631079 18.0342737,21 16.6576708,21 L7.34232922,21 C5.96572629,21 4.76577697,20.0631079 4.43190172,18.7276069 L2.31063391,10.2425356 C2.17668518,9.70674072 2.50244587,9.16380623 3.03824078,9.0298575 C3.11756139,9.01002735 3.1990146,9 3.28077641,9 Z M12,12 C11.4477153,12 11,12.4477153 11,13 L11,17 C11,17.5522847 11.4477153,18 12,18 C12.5522847,18 13,17.5522847 13,17 L13,13 C13,12.4477153 12.5522847,12 12,12 Z M6.96472382,12.1362967 C6.43125772,12.2792385 6.11467523,12.8275755 6.25761704,13.3610416 L7.29289322,17.2247449 C7.43583503,17.758211 7.98417199,18.0747935 8.51763809,17.9318517 C9.05110419,17.7889098 9.36768668,17.2405729 9.22474487,16.7071068 L8.18946869,12.8434035 C8.04652688,12.3099374 7.49818992,11.9933549 6.96472382,12.1362967 Z M17.0352762,12.1362967 C16.5018101,11.9933549 15.9534731,12.3099374 15.8105313,12.8434035 L14.7752551,16.7071068 C14.6323133,17.2405729 14.9488958,17.7889098 15.4823619,17.9318517 C16.015828,18.0747935 16.564165,17.758211 16.7071068,17.2247449 L17.742383,13.3610416 C17.8853248,12.8275755 17.5687423,12.2792385 17.0352762,12.1362967 Z" fill="#000000"/>
                                            </g>
                                        </svg>
                                    </div>
                                    <div class="kt-notification__item-details flex-row justify-content-between">
                                        <div class="kt-notification__item-title">Total Items</div>
                                        <div class="kt-notification__item-time">{{$count}}</div>
                                    </div>
                                </a>
                                <a href="#" class="kt-notification__item">
                                    <div class="kt-notification__item-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <rect x="0" y="0" width="24" height="24"/>
                                                <rect fill="#000000" opacity="0.3" x="11.5" y="2" width="2" height="4" rx="1"/>
                                                <rect fill="#000000" opacity="0.3" x="11.5" y="16" width="2" height="5" rx="1"/>
                                                <path d="M15.493,8.044 C15.2143319,7.68933156 14.8501689,7.40750104 14.4005,7.1985 C13.9508311,6.98949895 13.5170021,6.885 13.099,6.885 C12.8836656,6.885 12.6651678,6.90399981 12.4435,6.942 C12.2218322,6.98000019 12.0223342,7.05283279 11.845,7.1605 C11.6676658,7.2681672 11.5188339,7.40749914 11.3985,7.5785 C11.2781661,7.74950085 11.218,7.96799867 11.218,8.234 C11.218,8.46200114 11.2654995,8.65199924 11.3605,8.804 C11.4555005,8.95600076 11.5948324,9.08899943 11.7785,9.203 C11.9621676,9.31700057 12.1806654,9.42149952 12.434,9.5165 C12.6873346,9.61150047 12.9723317,9.70966616 13.289,9.811 C13.7450023,9.96300076 14.2199975,10.1308324 14.714,10.3145 C15.2080025,10.4981676 15.6576646,10.7419985 16.063,11.046 C16.4683354,11.3500015 16.8039987,11.7268311 17.07,12.1765 C17.3360013,12.6261689 17.469,13.1866633 17.469,13.858 C17.469,14.6306705 17.3265014,15.2988305 17.0415,15.8625 C16.7564986,16.4261695 16.3733357,16.8916648 15.892,17.259 C15.4106643,17.6263352 14.8596698,17.8986658 14.239,18.076 C13.6183302,18.2533342 12.97867,18.342 12.32,18.342 C11.3573285,18.342 10.4263378,18.1741683 9.527,17.8385 C8.62766217,17.5028317 7.88033631,17.0246698 7.285,16.404 L9.413,14.238 C9.74233498,14.6433354 10.176164,14.9821653 10.7145,15.2545 C11.252836,15.5268347 11.7879973,15.663 12.32,15.663 C12.5606679,15.663 12.7949989,15.6376669 13.023,15.587 C13.2510011,15.5363331 13.4504991,15.4540006 13.6215,15.34 C13.7925009,15.2259994 13.9286662,15.0740009 14.03,14.884 C14.1313338,14.693999 14.182,14.4660013 14.182,14.2 C14.182,13.9466654 14.1186673,13.7313342 13.992,13.554 C13.8653327,13.3766658 13.6848345,13.2151674 13.4505,13.0695 C13.2161655,12.9238326 12.9248351,12.7908339 12.5765,12.6705 C12.2281649,12.5501661 11.8323355,12.420334 11.389,12.281 C10.9583312,12.141666 10.5371687,11.9770009 10.1255,11.787 C9.71383127,11.596999 9.34650161,11.3531682 9.0235,11.0555 C8.70049838,10.7578318 8.44083431,10.3968355 8.2445,9.9725 C8.04816568,9.54816454 7.95,9.03200304 7.95,8.424 C7.95,7.67666293 8.10199848,7.03700266 8.406,6.505 C8.71000152,5.97299734 9.10899753,5.53600171 9.603,5.194 C10.0970025,4.85199829 10.6543302,4.60183412 11.275,4.4435 C11.8956698,4.28516587 12.5226635,4.206 13.156,4.206 C13.9160038,4.206 14.6918294,4.34533194 15.4835,4.624 C16.2751706,4.90266806 16.9686637,5.31433061 17.564,5.859 L15.493,8.044 Z" fill="#000000"/>
                                            </g>
                                        </svg>
                                    </div>
                                    <div class="kt-notification__item-details flex-row justify-content-between">
                                        <div class="kt-notification__item-title" id="discount_title">Total Amount</div>
                                            <input type="hidden" id="before_discount_price" value="{{$total_price}}" />
                                            <div id="totPrice" class="kt-notification__item-time">${{$total_price}}</div>
                                        
                                    </div>
                                </a>
                                <?php 
                                    $sum = \App\Wallet::where('user_id',Auth::user()->user_id)->sum('amount');
                                    $pay = \App\Payment::where('user_id',Auth::user()->user_id)->sum('wallet_amount');
                                    $t_sum = $sum - $pay;
                                    if($t_sum > 0){
                                        echo '<input type="checkbox" name="wallet" id="wallet" value="'.$t_sum.'"> Wallet ';
                                        echo '<span>$'.$t_sum.'</span>';
                                    } else {
                                        echo '<input type="hidden" name="wallet" id="wallet" value="0">';
                                    }
                                    
                                 ?>
                                <form method="post" id="coupon_form">
                                    <div class="row">                           
                                        <div class="col-lg-6">
                                            <input type="text" name="coupon_code" id="coupon_code" class="form-control" placeholder="Coupon Code" />
                                        </div>
                                        <div class="col-lg-6 float-left">
                                            <input type="submit" name="apply_coupon" id="apply_coupon" value="Apply Coupon" class="btn btn-brand" />
                                        </div>
                                    </div>
                                </form>

                            </div>

                            <div class="accordion accordion-solid " id="accordionExample6">
                                <div class="card">
                                    <!-- tab for paypal -->
                                    <div id="payment_gateway">
                                        
                                        <div id="paypal-button-container"></div>
                                        <!-- old stripe code -->
                                        <!-- end here code for paypal -->
                                        <!--  new button code -->
                                        
                                        <div class="card-header" id="headingOne6">
                                                    
                                                <form role="form" action="{{ route('stripe.post') }}" method="post" id="payment-form">
                                                @csrf
                                                    <input id="amount" value="{{$total_price}}" name="amount" type="hidden">
                                                    <input type="hidden" id="tot_price" value="{{ $total_price}}" />
                                                    <input type="hidden" name="stripe_promo_id" id="stripe_promo_id" value="0" />
                                                    <input type="hidden" id="stripe_wallet" name="wallet" value="0">

                                                    <noscript>You must <a href="http://www.enable-javascript.com" target="_blank">enable JavaScript</a> in your web browser in order to pay via Stripe.</noscript>
                                                       
                                                        <!-- <i class="fa fa-credit-card"></i> -->
                                                        <input 
                                                            type="submit"
                                                            class="btn btn-brand btn-upper btn-block" 
                                                            value="Pay Now with Card"
                                                            data-key={{env('STRIPE_KEY')}}
                                                            data-amount="{{$total_price*100}}"
                                                            data-currency="usd"
                                                            data-name="GuestPostEngine"
                                                            data-email="{{Auth::user()->email}}"
                                                            id="stripe"
                                                        />
                                                    
                                                        <script src="https://checkout.stripe.com/v2/checkout.js"></script>
                                                        <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script> -->
                                                        <script>
                                                        $(document).ready(function() {
                                                            $(document).on('click','#stripe' ,function(event) {
                                                                event.preventDefault();

                                                                var $button = $(this),
                                                                    $form = $button.parents('form');

                                                                var opts = $.extend({}, $button.data(), {
                                                                    token: function(result) {
                                                                    $form.append($('<input>').attr({ type: 'hidden', name: 'stripeToken', value: result.id })).submit();
                                                                    }
                                                                });

                                                                StripeCheckout.open(opts);
                                                            });
                                                        });
                                                        </script>
                                                </form>
                                            
                                        </div>
                                        
                                    </div>
                                    <div id="card_detail" style="padding-top:15px;">
                                        
                                                <?php 
                                                    if(isset($cardlist->card)){
                                                        $card = $cardlist->card;
                                                        $month = $card->exp_month;
                                                        $year = $card->exp_year;
                                                        $cardnumber = 'XXXXXXXXXXXX'.$card->last4;
                                                        $cvv = '***';        
                                                        $name = $customer->description;
                                                        ?>  

                                                            <div class="card-header" id="headingOne6">
                                                                <div class="btn btn-brand btn-upper btn-block collapsed" data-toggle="collapse" data-target="#collapseOne6" aria-expanded="false" aria-controls="collapseOne6" style="padding-bottom:15px;">
                                                                    <i class="fa fa-credit-card"></i> Pay Later with Saved Card
                                                                </div>
                                                            </div>
                                                            <!-- pay by card end here -->
                                                            <div id="collapseOne6" class="collapse" aria-labelledby="headingOne6" data-parent="#accordionExample6" style="">     
                                                                

                                                                    
                                                                <div class="card-body" id="save-card-db">
                                                                    <a href="javascript:void(0);" id="change-card">Pay with other card</a>
                                                                    <form role="form" action="{{ route('stripeCardSave.post') }}" method="post" >
                                                                     @csrf
                                                                        <input id="save_amount_total" value="{{$total_price}}" name="amount" type="hidden">
                                                                        <input type="hidden" id="email" name="email" value="{{Auth::user()->email}}">

                                                                        <input type="hidden" id="stripe_card_wallet" name="wallet" value="0">

                                                                        <div class="form-group">
                                                                            <div class="input-group">
                                                                                <input autocomplete="off" class="form-control fullname required"  type="text" placeholder="Full name" name="fullname" maxlength="20" value="{{@$name}}" disabled="">
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <div class="input-group">
                                                                                <div class="input-group-prepend"><span class="input-group-text"><i class="la la-credit-card"></i></span></div>
                                                                                <input autocomplete="off" class="form-control card-number stripe-sensitive required" size="20" type="text" id="cnum" placeholder="Card number" name="card-number" maxlength="16" value="{{@$cardnumber}}" disabled="">
                                                                            </div>
                                                                        </div>
                                                                        <div class="row form-group">
                                                                            <div class="col-4 kt-pl-0">
                                                                                <select class="form-control card-expiry-month" name="card-expiry-month" type="text" disabled="">
                                                                                </select>
                                                                                <script type="text/javascript">
                                                                                        var $mon = "{{@$month}}";
                                                                                        if($mon){
                                                                                            var select = $(".card-expiry-month"),
                                                                                            month = parseInt($mon);
                                                                                        } else {
                                                                                            var select = $(".card-expiry-month"),
                                                                                            month = new Date().getMonth() + 1;
                                                                                        }
                                                                                        
                                                                                        for (var i = 1; i <= 12; i++) {
                                                                                            select.append($("<option value='"+i+"' "+(month === i ? "selected" : "")+">"+i+"</option>"))
                                                                                        }
                                                                                    </script>
                                                                            </div>
                                                                            <div class="col-4">
                                                                                <select class="form-control card-expiry-year stripe-sensitive required" id="year" name="card-expiry-year" disabled=""></select>
                                                                                <script type="text/javascript">
                                                                                    var year = '{{@$year}}';
                                                                                    if(year){
                                                                                        var select = $(".card-expiry-year"),year = parseInt(year);    
                                                                                    } else {
                                                                                        var select = $(".card-expiry-year"),
                                                                                        year = new Date().getFullYear();

                                                                                    }

                                                                                    for (var i = 0; i < 12; i++) {
                                                                                        select.append($("<option value='"+(i + year)+"' "+(i === 0 ? "selected" : "")+">"+(i + year)+"</option>"))
                                                                                    }
                                                                                </script>
                                                                            </div>
                                                                            <div class="col-4">
                                                                            <input autocomplete="off" class="form-control card-cvc stripe-sensitive required" size="3" type="password" maxlength="3" name="card-cvc" placeholder="CVV"  value="{{@$cvv}}" disabled="">
                                                                        </div> 
                                                                        </div>
                                                                        <input type="hidden" name="promo_id_card_stripe" id="promo_id_card_stripe_save" value="0" />
                                                                        <div class="col-12">
                                                                            <h6 class="text-center kt-mb-15"><span class="fa fa-lock kt-mr-5"></span>Your information is secure.</h6>
                                                                            <h6 class="text-center kt-mb-15" id="note">Note: Your card will be charged for ${{$total_price}} when the order is accepted by Publisher</h6>
                                                                            <div class="">
                                                                                <button type="submit" class="btn btn-brand btn-upper btn-block pay-later" name="submit-button">Pay Later ${{$total_price}}</button>
                                                                            </div>
                                                                        </form>

                                                                        <div class="kt-mt-10 text-center d-flex align-items-center justify-content-center">Powered by <i class="fab fa-stripe" style="font-size: 2.4rem;margin-left: 0.4rem;"></i></div>
                                                                    </div>
                                                                    
                                                                </div>

                                                                <div class="card-body" id="stripe-render">
                                                                    <a href="javascript:void(0);" id="back-save-card">Go to Save Card</a>
                                                                    @include('buyer.stripe-render')
                                                                    <!-- old code stripe -->
                                                                    <script type="text/javascript">
                                                                        $(document).ready(function(){
                                                                            $('#stripe-render').hide();
                                                                        })
                                                                        $(document).on('click','#change-card',function(){
                                                                            $('#payment-form-card')[0].reset();
                                                                            $('#stripe-render').show();
                                                                            $('#save-card-db').hide();
                                                                        });
                                                                        $(document).on('click','#back-save-card',function(){

                                                                            $('#stripe-render').hide();
                                                                            $('#save-card-db').show();
                                                                        });
                                                                    </script>
                                                                </div>

                                                            </div>                                                                

                                                        <?php
                                                    } else {
                                                        ?>
                                                            <!--pay by card -->
                                                            <div class="card-header" id="headingOne6">
                                                                <div class="btn btn-brand btn-upper btn-block collapsed" data-toggle="collapse" data-target="#collapseOne6" aria-expanded="false" aria-controls="collapseOne6" style="padding-bottom:15px;">
                                                                    <i class="fa fa-credit-card"></i> Pay Later with Saved Card
                                                                </div>
                                                            </div>
                                                            <!-- pay by card end here -->
                                                            <div id="collapseOne6" class="collapse" aria-labelledby="headingOne6" data-parent="#accordionExample6" style="">     
                                                                <div class="card-body">
                                                                    @include('buyer.stripe-render')
                                                                </div>    
                                                            </div>
                                                            <!-- old code stripe -->

                                                        <?php
                                                    }
                                        ?>                                       

                                    </div>
                                    <!-- <div id="pay_discount" style="padding-top:15px;">
                                        <div class="btn btn-success btn-upper btn-block">
                                            <i class="fa fa-check"></i>Pay with Discount
                                        </div>
                                    </div> -->
                                    <span id="append-with-discount-owner">
                                        
                                    </span>
                                    
                                    <!-- <div class="modal_coupon" style="display:none;">
                                        <div class="container kt-padding-20">
                                            <div class="row">    
                                                <form method="post" id="coupon_form">
                                                    <p>Enter Discount Coupon Code</p>
                                                    <p><input type="text" name="coupon_code" id="coupon_code" class="form-control" placeholder="Coupon Code"  /></p>
                                                    <p><input type="submit" name="apply_coupon" id="apply_coupon" value="Apply Coupon" class="btn btn-success" /></p>
                                                </form>
                                            </div>                                        
                                        </div>
                                    </div> -->
                                    <!-- Include the PayPal JavaScript SDK -->
                                    <script src="https://www.paypal.com/sdk/js?client-id={{ env('PAYPAL_key') }}&currency=USD"></script>
                                    <script>
                                    // Render the PayPal button into #paypal-button-container
                                    paypal.Buttons({
                                        // Set up the transaction
                                        style: {
                                            layout : 'horizontal',
                                            color  : 'gold',
                                            shape  : 'rect',
                                            height :  38,
                                            tagline: 'false',
                                            label : 'pay'
                                        },
                                        createOrder: function(data, actions) {
                                            //$(".loader").fadeIn("slow");
                                            return actions.order.create({
                                                purchase_units: [{
                                                    amount: {
                                                        value: $('#tot_price').val(),
                                                    }
                                                }]
                                            });
                                        },

                                        // Finalize the transaction
                                        onApprove: function(data, actions) {
                                            $(".loader").fadeIn("slow");
                                            return actions.order.capture().then(function(details) {
                                                
                                                //convert obj to JSON
                                                var newDetails = JSON.stringify(details);   
                                                //convert JSON to array
                                                var detailsArr = JSON.parse(newDetails);
                                                var domain_arr = '<?php echo json_encode($domain_arr); ?>';
                                                $.ajax({
                                                     headers: {
                                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                                      },
                                                      url  : "{{ url('/advertiser/add_orders') }}",
                                                      type : 'POST',
                                                      data: {
                                                        'details': detailsArr,
                                                        'domain_arr': domain_arr,
                                                        'promo_id' : $('#promo_id').val(),
                                                        'wallet'   : $('#wallet').val(),
                                                      },
                                                      async: true,
                                                      dataType: 'json',
                                                      enctype: 'multipart/form-data',
                                                      cache: false, 
                                                      beforeSend: function() {
                                                            //something before send
                                                            $(".loader").fadeIn("slow");
                                                        },                     
                                                      success: function(response){
                                                         if(response.status == 'true'){
                                                            $('#append').html(response.html);
                                                            $('#cartlist_notify_full').html(response.cart);
                                                            $('.loader').fadeOut('slow');
                                                         }
                                                         else{
                                                            $('#append').html(response.html);
                                                            $('#cartlist_notify_full').html(response.cart);
                                                            $('.loader').fadeOut('slow');
                                                         }
                                                      },
                                                      error: function(errorThrown){
                                                        $('.loader').fadeOut('slow');
                                                        alert(errorThrown);
                                                      }
                                                });
                                                // Show a success message to the buyer
                                                //alert('Transaction completed by ' + details.payer.name.given_name + '!');
                                            });
                                        }
                                    }).render('#paypal-button-container');
                                    </script>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <!-- end:: Content -->           
            
        </div>
        


        <!--begin::Modal-->
        <div class="modal fade" id="kt_modal_delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header  text-center">
                        <h5 class="modal-title" id="exampleModalLabel">Delete</h5>
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon" data-dismiss="modal" aria-label="Close" style="cursor:pointer;">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#000000">
                                    <rect x="0" y="7" width="16" height="2" rx="1"></rect>
                                    <rect opacity="0.3" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000) " x="0" y="7" width="16" height="2" rx="1"></rect>
                                </g>
                            </g>
                        </svg>
                    </div>

                    <div class="modal-body">
                        <div class="form-group">
                            <span class="form-text text-muted">Are you sure you want to delete this item?</span>
                        </div>
                    </div>
                    <div class="modal-footer kt-login__actions">                          
                        <button type="submit" id="delete" class="btn btn-label-primary kt-mr-10 delete no">
                            {{ __('No') }}
                        </button>
                        <button type="submit" class="btn btn-primary delete yes">
                            {{ __('Yes') }}
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Modal-->
        <!-- Modal for add discount -->
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
            
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal"></button>
                      <h4 class="modal-title">Add Discount</h4>
                    </div>
                    <div class="modal-body">
                        <div class="container">
                            <div class="row">
                                <form method="POST">
                                    <p>Enter Discount Coupon Code</p>
                                    <p><input type="hidden" name="promo_id" id="promo_id" value="0" /></p>
                                    <p><input type="text" name="coupon_code" id="coupon_code" class="form-control" placeholder="Coupon Code"  /></p>
                                    <p><input type="submit" name="apply_coupon" id="apply_coupon" value="Apply Coupon" class="btn btn-success" /></p>
                                </form>
                            </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
              
                </div>
            </div>
        </div>
        <!-- end here modal -->
    </div>
</span>



@section('mainscripts')
@parent
@section('customScript')
<!--begin::Page Vendors Styles(used by this page) -->
<script href="{{ asset( 'public/assets/js/owl.carousel.js' ) }}" type="text/javascript"></script>
<script src="{{ asset( 'public/assets/js/pages/crud/forms/widgets/ion-range-slider.js' ) }}" type="text/javascript"></script>
<script>
    $(document).ready(function() {
        $('.owl-carousel').owlCarousel({
            loop: true,
            responsiveClass: true,
            responsive: {
              0: {
                items: 1,
                nav: false,
                dots: false,
                autoplay: true
              },
              600: {
                items: 2,
                nav: false,
                dots: false,
                autoplay: true
              },
              768: {
                items: 2,
                nav: false,
                dots: false
              },
              900: {
                items: 3,
                nav: false,
                dots: false
              },
              1200: {
                items: 4,
                nav: false,
                dots: false
              },
              1399: {
                items: 5,
                nav: false,
                dots: false
              }
            }
        });
    });
</script>
<!-- <script type="text/javascript" src="https://js.stripe.com/v2/"></script> -->
<script type="text/javascript">
    $(function() {
        $(document).on('click','.delete',function(){
            $('.delete.yes').attr('rel',$(this).attr('rel'));
            $('#kt_modal_delete').modal('show');
        });
        $(document).on('click','.delete.yes',function(e){
            $('#kt_modal_delete').modal('hide');
            e.preventDefault();
            $.ajax({
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                url  : "{{url('/cart/delete')}}",
                type: 'POST',
                data: {id : $(this).attr('rel')},
                async: true,
                dataType: 'json',
                enctype: 'multipart/form-data',
                cache: false,                      
                beforeSend: function() {
                    //something before send
                    $(".loader").fadeIn("slow");
                },
                success: function(data) {
                    //console.log('Data: '+data);
                    if(data.alert == 'success'){
                        window.location.href = data.url;
                        //$('#show_msg').html(data.message);
                    } else {
                        $(".loader").fadeOut("slow");
                    }                            
                },
                error: function(xhr,textStatus,thrownError) {
                        alert(xhr + "\n" + textStatus + "\n" + thrownError);
                }
            });
            e.stopImmediatePropagation();
            return false;
            
        });
        $(document).on('click','.delete.no',function(){
            $('#kt_modal_delete').modal('hide');
        });
    //     var $form     = $(".require-validation");
    //     $('form.require-validation').bind('submit', function(e) {
    //         var $form     = $(".require-validation"),
    //         inputSelector = ['input[type=email]', 'input[type=password]',
    //                         'input[type=text]', 'input[type=file]',
    //                         'textarea'].join(', '),
    //         $inputs       = $form.find('.required').find(inputSelector),
    //         $errorMessage = $form.find('div.error'),

    //         valid         = true;
    //         $errorMessage.addClass('hide');
    //         $('.has-error').removeClass('has-error');

    //         $inputs.each(function(i, el) {
    //             alert('sss');
    //             var $input = $(el);
    //             if ($input.val() === '') {
    //                 $input.parent().addClass('has-error');
    //                 $errorMessage.removeClass('hide');
    //                 e.preventDefault();
    //             }
    //         });
    //         if (!$form.data('cc-on-file')) {
    //             e.preventDefault();
    //             Stripe.setPublishableKey($form.data('stripe-publishable-key'));
    //             Stripe.createToken({
    //                 number: $('.card-number').val(),
    //                 cvc: $('.card-cvc').val(),
    //                 exp_month: $('.card-expiry-month').val(),
    //                 exp_year: $('.card-expiry-year').val()
    //             }, stripeResponseHandler);
    //             e.stopImmediatePropagation();
    //             return false;
    //         }
    //     });
    //     function stripeResponseHandler(status, response) {
    //         if (response.error) {
    //             $('.error')
    //                 .removeClass('hide')
    //                 .find('.alert')
    //                 .text(response.error.message);
    //         } else {
    //             $(".loader").fadeIn("slow");

    //             // token contains id, last4, and card type
    //             var token = response['id'];
    //             // insert the token into the form so it gets submitted to the server
    //             $form.find('input[type=text]').empty();
    //             $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
    //             $form.get(0).submit();
    //         }
    //     }
    });


// Stripe.setPublishableKey("{{env('STRIPE_KEY')}}");

// $(function() {
//   var $form = $('#payment-formr');
//   $form.submit(function(event) {
//     // Disable the submit button to prevent repeated clicks:
//     $form.find('.submit').prop('disabled', true);

//     // Request a token from Stripe:
//     Stripe.card.createToken($form, stripeResponseHandler);

//     // Prevent the form from being submitted:
//     return false;
//   });
// });

// function stripeResponseHandler(status, response) {
//   // Grab the form:
//   var $form = $('#payment-formr');

//   if (response.error) { // Problem!

//     // Show the errors on the form:
//     $form.find('.payment-errors').text(response.error.message);
//     $form.find('.submit').prop('disabled', false); // Re-enable submission

//   } else { // Token was created!

//     // Get the token ID:
//     var token = response.id;

//     // Insert the token ID into the form so it gets submitted to the server:
//     $form.append($('<input type="hidden" name="stripeToken">').val(token));

//     // Submit the form:
//     //$form.get(0).submit();
    
//     // For this demo, we're simply showing the token:
//     alert("Token: " + token);
//   }
// };

</script>
<script>
    $(document).on('click','#pay_discount',function(e){
       $('.modal_coupon').slideToggle();
        e.stopImmediatePropagation();
        return false; 
    });
</script>
<script>
    $(document).on('click','#apply_coupon',function(e){

        var coupon_code = $('#coupon_code').val();
        var beforediscountPrice = $('#before_discount_price').val();
        if(coupon_code == ''){
            Swal.fire(
              'Enter Coupon Code First',
            );
        }
       else{
            //check coupon code exists in db or not
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url  : "{{ url('/advertiser/apply_coupon') }}",
                type : 'POST',
                data : {
                    'coupon_code': coupon_code,
                    'price' : beforediscountPrice
                },
                async: true,
                dataType: 'json',
                enctype: 'multipart/form-data',
                cache: false,    
                success:function(response){
                    if(response.success == 'true') {
                        var afterdiscountPrice = response.afterdiscountAmount;
                        $('#promo_id').val(response.promo_id);

                        Swal.fire(
                          response.message,
                        )
                        $('#totPrice').html('$'+afterdiscountPrice);
                        $('#tot_price').val(afterdiscountPrice);
                        $('#amount').val(afterdiscountPrice);
                        $('#amount_total').val(afterdiscountPrice);
                        $('#save_amount_total').val(afterdiscountPrice);

                        $('#coupon_form').trigger("reset");
                        $('#discount_title').append('<p class="text-success">Coupon code has been applied.</p>');
                        
                        $('.modal_coupon').hide();
                        var stripeAmount = afterdiscountPrice * 100;
                        $('#stripe').attr('data-amount',stripeAmount);
                        $('#stripe_promo_id').val(response.promo_id);
                        $('#promo_id_card_stripe').val(response.promo_id);
                        $('#promo_id_card_stripe_save').val(response.promo_id);

                        

                        $('#note').html('Note: Your card will be charged for $' +afterdiscountPrice + ' when the order is accepted by Publisher.');
                        $('.pay-later').html('Pay Later ' + '$'+afterdiscountPrice);

                        if(response.for == 'owner'){
                            $('#payment_gateway').hide();
                            $('#pay_discount').hide();
                            $('#card_detail').hide();

                            $('#append-with-discount-owner').html('<div id="pay_withDiscount" style="padding-top:15px;"><div class="btn btn-success btn-upper btn-block"><i class="fa fa-check"></i>Pay with Discount</div></div>');
                            $('#pay_withDiscount').show();

                            $('#coupon_code').val(coupon_code);    
                        } else {
                            $('#coupon_code').val(coupon_code);
                        }
                        
                    }
                    if(response.success == 'false'){
                        Swal.fire(
                          response.message,
                        )
                        $('#totPrice').html('$'+$('#before_discount_price').val());
                        $('#tot_price').val($('#before_discount_price').val());
                        $('#amount').val($('#before_discount_price').val());
                        $('#payment_gateway').show();
                        $('#pay_discount').show();
                        $('#pay_withDiscount').hide();
                        $('#coupon_code').val(coupon_code);
                    }
                },
                error:function(){}
            });
       }
        e.stopImmediatePropagation();
        return false;
    });


    $(document).on('click','#wallet',function(){
        if($(this).prop("checked") == true) {

            var wallet_amount = $(this).val();
            var beforediscountPrice = $('#before_discount_price').val();
            $('#stripe_wallet').val(wallet_amount);
            $('#stripe_card_wallet').val(wallet_amount);
            if(parseFloat(wallet_amount) >= parseFloat(beforediscountPrice)){
                
                $('#payment_gateway').hide();
                $('#pay_discount').hide();
                $('#card_detail').hide();
                $('#append-with-discount-owner').html('<div id="pay_withWallet" style="padding-top:15px;"><div class="btn btn-success btn-upper btn-block"><i class="fa fa-check"></i>Pay with Wallet</div></div>');
                $('#pay_withDiscount').show();                
                
            } else {
                var afterdiscountPrice = parseFloat(beforediscountPrice) - parseFloat(wallet_amount);
                $('#totPrice').html('$'+afterdiscountPrice);
                $('#tot_price').val(afterdiscountPrice);
                $('#amount').val(afterdiscountPrice);
                $('#amount_total').val(afterdiscountPrice);
                $('#save_amount_total').val(afterdiscountPrice);        
                var stripeAmount = afterdiscountPrice * 100;
                $('#stripe').attr('data-amount',stripeAmount);
                $('.pay-later').html('Pay Later ' + '$'+afterdiscountPrice);
            }
        }
        else if($(this).prop("checked") == false){            

            $('#stripe_wallet').val(0);
            $('#stripe_card_wallet').val(0);
            var beforediscountPrice = $('#before_discount_price').val();
            var afterdiscountPrice = beforediscountPrice;
            $('#totPrice').html('$'+afterdiscountPrice);
            $('#tot_price').val(afterdiscountPrice);
            $('#amount').val(afterdiscountPrice);
            $('#amount_total').val(afterdiscountPrice);
            $('#save_amount_total').val(afterdiscountPrice);        
            var stripeAmount = afterdiscountPrice * 100;
            $('#stripe').attr('data-amount',stripeAmount);
            $('.pay-later').html('Pay Later ' + '$'+afterdiscountPrice);
            $('#payment_gateway').show();
            $('#pay_discount').show();
            $('#card_detail').show();
            $('#append-with-discount-owner').html('');
            $('#pay_withDiscount').hide();
        }
    });



</script> 
<script>
$(document).on('click','#pay_withDiscount',function(e){
    $('.loader').show();
    var domain_arr  = '<?php echo json_encode($domain_arr); ?>';
    var amount      = $('#amount').val();
    var coupon_code = $('#promo_id').val();
    $.ajax({
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          url  : "{{ url('/advertiser/add_manualOrder') }}",
          type : 'POST',
          data: {
            'domain_arr'    : domain_arr,
            'amount'        : amount,
            'coupon_code'   : $('#coupon_code').val(), 
            'promo_id'   : $('#promo_id').val()
          },
          async: true,
          dataType: 'json',
          enctype: 'multipart/form-data',
          cache: false,                      
          success: function(response){
            $('.loader').hide();
             if(response.status == 'true'){
                $('#append').html(response.html);
                $('#cartlist_notify_full').html(response.cart);
             }
             else{
                $('#append').html(response.html);
                $('#cartlist_notify_full').html(response.cart);
             }
          },
          error: function(errorThrown){
            alert(errorThrown);
          }
    });
e.stopImmediatePropagation();
return false;
});
$(document).on('click','#pay_withWallet',function(e){
    $('.loader').show();
    var domain_arr  = '<?php echo json_encode($domain_arr); ?>';
    var amount      = $('#amount').val();
    $.ajax({
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          url  : "{{ url('/advertiser/add_WalletOrder') }}",
          type : 'POST',
          data: {
            'domain_arr'    : domain_arr,
            'amount'        : amount
          },
          async: true,
          dataType: 'json',
          enctype: 'multipart/form-data',
          cache: false,                      
          success: function(response){
            $('.loader').hide();
             if(response.status == 'true'){
                $('#append').html(response.html);
                $('#cartlist_notify_full').html(response.cart);
             }
             else{
                $('#append').html(response.html);
                $('#cartlist_notify_full').html(response.cart);
             }
          },
          error: function(errorThrown){
            alert(errorThrown);
          }
    });
e.stopImmediatePropagation();
return false;
});
</script> 
<!--end::Page Scripts -->
@show
@endsection
@endsection
