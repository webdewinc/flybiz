@extends('layout.app')
@section('title', 'Purchase section of Advertiser - GuestPostEngine')
@section('title-description')

<meta name="description" content="At purchase section, an advertiser will be able to see all the details of publisher regarding on which date he/she purchased a website along with their URL.">
    
@endsection
@section('content')
@section('mainhead')
@parent
    <!--begin::Page Vendors Styles(used by this page) -->
    <link href="{{ asset('') . config('app.public_url') . '/assets/css/pages/wizard/wizard-2.css' }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('') . config('app.public_url') . '/css/jsRapStar.css' }}" />
        <!--end::Page Vendors Styles -->
@show
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container "> 
            </div>
        </div>

        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">
            <div class="alert alert-light alert-elevate" role="alert">
                <div class="kt-portlet__body w-100">

                    <!--begin: Search Form -->
                    <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                        <div class="row align-items-center">
                            <div class="col-xl-8 order-2 order-xl-1">
                                <div class="row align-items-center">
                                    <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                        <div class="kt-form__label">
                                            <label>Search:</label>
                                        </div>
                                        <div class="kt-input-icon kt-input-icon--left">
                                            <input type="text" class="form-control" placeholder="Search..." id="generalSearch">
                                            <span class="kt-input-icon__icon kt-input-icon__icon--left">
                                                <span><i class="la la-search"></i></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                        
                                            <div class="kt-form__label">
                                                <label>Payment Status:</label>
                                            </div>
                                            <div class="kt-form__control">
                                                <select class="form-control bootstrap-select" id="kt_form_status">
                                                    <option value="">All</option>
                                                    <option value="2">Unpaid</option>              
                                                    <option value="1">Paid</option>
                                                </select>
                                            </div>
                                       
                                    </div>

                                </div>
                            </div>
                           
                        </div>
                    </div>

                    <!--end: Search Form -->
                </div>
                
            </div>
            
            <div class="kt-portlet kt-portlet--mobile">
                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">

                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect x="0" y="0" width="24" height="24"></rect>
                                    <path d="M8,3 L8,3.5 C8,4.32842712 8.67157288,5 9.5,5 L14.5,5 C15.3284271,5 16,4.32842712 16,3.5 L16,3 L18,3 C19.1045695,3 20,3.8954305 20,5 L20,21 C20,22.1045695 19.1045695,23 18,23 L6,23 C4.8954305,23 4,22.1045695 4,21 L4,5 C4,3.8954305 4.8954305,3 6,3 L8,3 Z" fill="#000000" opacity="0.3"></path>
                                    <path d="M11,2 C11,1.44771525 11.4477153,1 12,1 C12.5522847,1 13,1.44771525 13,2 L14.5,2 C14.7761424,2 15,2.22385763 15,2.5 L15,3.5 C15,3.77614237 14.7761424,4 14.5,4 L9.5,4 C9.22385763,4 9,3.77614237 9,3.5 L9,2.5 C9,2.22385763 9.22385763,2 9.5,2 L11,2 Z" fill="#000000"></path>
                                    <rect fill="#000000" opacity="0.3" x="10" y="9" width="7" height="2" rx="1"></rect>
                                    <rect fill="#000000" opacity="0.3" x="7" y="9" width="2" height="2" rx="1"></rect>
                                    <rect fill="#000000" opacity="0.3" x="7" y="13" width="2" height="2" rx="1"></rect>
                                    <rect fill="#000000" opacity="0.3" x="10" y="13" width="7" height="2" rx="1"></rect>
                                    <rect fill="#000000" opacity="0.3" x="7" y="17" width="2" height="2" rx="1"></rect>
                                    <rect fill="#000000" opacity="0.3" x="10" y="17" width="7" height="2" rx="1"></rect>
                                </g>
                            </svg>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            Purchases
                        </h3>
                    </div>
                

                </div>
                
                <div class="kt-portlet__body kt-portlet__body--fit">

                    <!--begin: Datatable -->
                    <div class="kt-datatable" id="local_data"></div>

                    <!--end: Datatable -->
                </div>
            </div>
        </div>

        <!-- end:: Content -->
    </div>
</div>
    @section('mainscripts')
    @parent
    <!--begin::Page Vendors Styles(used by this page) -->
    <script>
    $(document).ready(function(){
        $(document).on('click', '.link_redirect', function(e){ 
            e.preventDefault(); 
            var url = $(this).attr('href'); 
            window.open(url, '_blank');
        });
    });
    </script>
    <script>
        'use strict';
        // Class definition
        
        var KTDatatableDataLocalDemo = function() {
            // Private functions
            
            var data = '<?php echo json_encode( $data ); ?>';
          
            // var split_data = JSON.parse(data);
            // var domain_id = split_data[0]['domain_id'];
                    

            // demo initializer
            var demo = function() {
                var dataJSONArray = JSON.parse(data);                
                var datatable = $('.kt-datatable').KTDatatable({
                    
                    // datasource definition
                    data: {
                        type: 'local',
                        source: dataJSONArray,
                        pageSize: 10,
                    },                    
                    // layout definition
                    layout: {
                        scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
                        // height: 450, // datatable's body's fixed height
                        footer: false, // display/hide footer
                    },
        
                    // column sorting
                    sortable: true,
        
                    pagination: true,
        
                    search: {
                        input: $('#generalSearch'),
                    },
        
                    // columns definition
                    columns: [
                        {
                            field: 'RecordID',
                            title: '#',
                            sortable: false,
                            width: 20,
                            type: 'number',
                            selector: {class: 'kt-checkbox--solid'},
                            textAlign: 'center',
                        },
                        {
                            field: 'Seller',
                            title: 'Publisher',              
                            width: 150,
                        },
                        {
                            field: 'SellerURL',
                            title: 'Publisher Url', 
                            width: 300,             
                        },
                        {
                            field: 'cost_price',
                            title: 'Cost price',
                        },
                        {
                            field: 'extra_cost',
                            title: 'Extra cost',              
                        },
                        {
                            field: 'net_amount',
                            title: 'Net amount',
                        },
                        {
                            field: 'Total',
                            title: 'Gross amount',
                        },
                        {
                            field: 'PaymentStatus',
                            title: 'Payment Status',
                            // callback function support for column rendering
                            template: function(row) {
                                var status = {
                                    1: {'title': 'Paid', 'class': ' kt-badge--success'},
                                    2: {'title': 'Unpaid', 'class': 'kt-badge--danger'},
                                };
                                return '<span class="kt-badge ' + status[row.PaymentStatus].class + ' kt-badge--inline kt-badge--pill">' + status[row.PaymentStatus].title + '</span>';
                            },
                        },
                        {
                            field: 'content_type',
                            title: 'Content Type',
                            template: function(row) {
                                var status = {
                                    'buyer': {'title': 'Advertiser', 'class': ' kt-badge--success'},
                                    'seller': {'title': 'Publisher', 'class': 'kt-badge--success'},
                                };
                                if(row.content_type == 'buyer'){
                                    return '<span class="kt-badge ' + status[row.content_type].class + ' kt-badge--inline kt-badge--pill">' + status[row.content_type].title + '</span>';
                                }
                                return '<span class="kt-badge ' + status[row.content_type].class + ' kt-badge--inline kt-badge--pill">' + status[row.content_type].title + '</span>';

                            },
                        },
                        {
                            field: 'OrderDate',
                            title: 'Purchase Date',
                            type: 'date',
                            format: 'YYYY-MM-DD',
                            width: 150,
                        },
                        {
                            field: 'is_accept',
                            title: 'Order Status',
                            sortable: false,
                            width: 150,
                            overflow: 'visible',
                            autoHide: false,
                            // callback function support for column rendering
                            template: function(row) {
                                var status = {
                                    0: {'title': 'Order Placed', 'class': ' kt-badge--warning'},
                                    1: {'title': 'Order Accepted by Publisher', 'class': ' kt-badge--success'},
                                    2: {'title': 'Order Rejected by Publisher', 'class': 'kt-badge--danger'},
                                    3: {'title': 'Order Cancelled by Admin', 'class': 'kt-badge--danger'},
                                };
                                if(row.is_accept == 0){
                                    return '<span class="kt-badge ' + status[row.is_accept].class + ' kt-badge--inline kt-badge--pill">' + status[row.is_accept].title + '</span>';
                                } else if(row.is_accept == 1){
                                    if(row.stages > 3){

                                        switch(row.stages) {
                                          case '4':
                                                return '<span class="kt-badge kt-badge--warning kt-badge--inline kt-badge--pill">Content approval pending</span>';
                                            break;
                                          case '5':

                                                return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">Content Approved.</span>';
                                                
                                            break;
                                          case '6':
                                                return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">Content rejected.</span>';
                                                
                                            break;
                                          case '7':
                                               // live link update
                                               return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">live link updated</span>';
                                                
                                            break;
                                          case '8':
                                               // live link update
                                               return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">live link approved</span>';
                                                
                                            break;
                                          case '9':
                                               // live link update
                                               return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">Wait till Re-submission live link</span>';
                                                
                                            break;
                                          case '10':
                                            // order delever
                                                return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">Order Delivered</span>';
                                            break;
                                          case '11':
                                            // order closed
                                                if(row.pay_to_seller_id < 1) {
                                                   // payment released
                                                    return '<span class="kt-badge kt-badge--warning kt-badge--inline kt-badge--pill">Payment will release after 15 days to publisher.</span>';
                                                } else {
                                                    // payment released
                                                    return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">Payment released. </span>';
                                                } 
                                            break;
                                        } 
                                        
                                    } else {
                                        return '<span class="kt-badge ' + status[row.is_accept].class + ' kt-badge--inline kt-badge--pill">' + status[row.is_accept].title + '</span>';
                                    }
                                    
                                } else {
                                    return '<span class="kt-badge ' + status[row.is_accept].class + ' kt-badge--inline kt-badge--pill">' + status[row.is_accept].title + '</span>';
                                }
                                
                            },
                        }, 
                        {
                            field: 'Actions',
                            title: 'Actions',
                            sortable: false,
                            width: 160,
                            overflow: 'visible',
                            autoHide: false,
                            template: function(row) {
                                if(row.is_accept == 1){
                                    switch(row.stages) {
                                      case '0':
                                            // not submitted yet
                                            if(row.content_type == 'buyer'){
                                                return '<a href="{{url("advertiser")}}'+'/content/'+row.encrypt_id+'"><span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">Content submit here</span></a>';
                                            }
                                        break;
                                       case '4':
                                            if(row.content_type == 'seller'){
                                                return '<a href="{{url("advertiser")}}'+'/content/'+row.encrypt_id+'"><span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">View content</span></a>';
                                            }
                                        break;
                                      case '6':
                                            if(row.content_type == 'buyer'){
                                                return '<a href="#"  id="re_enter_content" data-content ="'+row.comment+'" data-encrypt = '+row.encrypt_id+'><span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">Re Enter content</span></a>';
                                            }
                                        break;
                                      case '7':
                                        // live link update
                                            return '<a href="#" class="approve_link" rel="'+row.encrypt_id+'" data-link="'+row.live_link+'"><span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">Check live link</span></a>';
                                        break;
                                      case '10':
                                        // order compplete
                                            return '<a href="#" class="payment_release" rel="'+row.encrypt_id+'" data-id="'+row.domain_id+'" data-domain-url ="'+row.domain_url+'"><span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">Release payment</span></a>';
                                        break;
                                    } 
                                }

                                   
                            }, 

                        },
                        
                        {
                            field: 'payment_type',
                            title: 'Payment Type',
                            template: function(row) {
                                var status = {
                                    'stripe-save-card': {'title': 'Stripe Card', 'class': ' kt-badge--success'},
                                    'stripe': {'title': 'Stripe', 'class': 'kt-badge--success'},
                                    'manually': {'title': 'Manually', 'class': 'kt-badge--warning'},
                                    'paypal': {'title': 'Paypal', 'class': 'kt-badge--success'},
                                    'wallet': {'title': 'Wallet', 'class': 'kt-badge--success'},
                                };
                                return '<span class="kt-badge ' + status[row.payment_type].class + ' kt-badge--inline kt-badge--pill">' + status[row.payment_type].title + '</span>';
                            },
                        },
                        {
                            field: 'refund',
                            title: 'Refunded status',
                            // callback function support for column rendering
                            template: function(row) {
                                var status = {
                                    1: {'title': 'NA', 'class': ' kt-badge--warning'},
                                    2: {'title': 'Refunded', 'class': 'kt-badge--danger'},
                                    3: {'title': 'Never charged before', 'class': 'kt-badge--warning'},
                                };
                                return '<span class="kt-badge ' + status[row.refund].class + ' kt-badge--inline kt-badge--pill">' + status[row.refund].title + '</span>';
                            },
                        },
                        {
                            field: 'refund_amount',
                            title: 'Refunded amount',
                            // callback function support for column rendering
                            template: function(row) {
                                var status = {
                                    'NA': {'title': 'NA', 'class': ' kt-badge--warning'},
                                    
                                };
                                if(row.refund_amount == 'NA'){
                                    return '<span class="kt-badge ' + status[row.refund_amount].class + ' kt-badge--inline kt-badge--pill">' + status[row.refund_amount].title + '</span>';    
                                } else {
                                    return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">' + row.refund_amount + '</span>';
                                }
                                
                            },
                        },
                        {
                            field: 'promo_code',
                            title: 'Promo Code',
                            // callback function support for column rendering
                            template: function(row) {
                                var status = {
                                    '0': {'title': 'NA', 'class': ' kt-badge--warning'}
                                };
                                if(row.promo_code == 0){
                                    return '<span class="kt-badge ' + status[row.promo_code].class + ' kt-badge--inline kt-badge--pill">' + status[row.promo_code].title + '</span>';    
                                } else {
                                    return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">' + row.promo_code + '</span>';
                                }
                                
                            },
                        },
                        {
                            field: 'wallet',
                            title: 'Wallet',
                            template: function(row) {

                                if( row.wallet > 0) {
                                    return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">Wallet ('+'$'+row.wallet+')</span>';
                                }
                                
                            }
                        },
                        {
                            field: 'url',
                            title: 'URL',
                            width: 300,
                            template: function(row) {
                                var status = {
                                    0: {'title': 'NA', 'class': ' kt-badge--warning'}
                                };
                                if(row.url == '' || row.url == null) {
                                    return '<span class="kt-badge ' + status[0].class + ' kt-badge--inline kt-badge--pill">' + status[0].title + '</span>';
                                } else {                                    
                                    return '<a href="'+row.url+'" target="_blank"><span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">click here</span></a>';
                                }
                            },
                        },
                        {
                            field: 'keyword',
                            title: 'Keyword',             
                            template: function(row) {
                                var status = {
                                    0: {'title': 'NA', 'class': ' kt-badge--warning'}
                                };
                                if(row.keyword == '' || row.keyword == null) {
                                    return '<span class="kt-badge ' + status[0].class + ' kt-badge--inline kt-badge--pill">' + status[0].title + '</span>';
                                } else {
                                    return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">'+row.keyword+'</span>';
                                }
                            },
                        },
                        {
                            field: 'title',
                            title: 'Title',             
                            template: function(row) {
                                var status = {
                                    0: {'title': 'NA', 'class': ' kt-badge--warning'}
                                };
                                if(row.title == '' || row.title == null) {
                                    return '<span class="kt-badge ' + status[0].class + ' kt-badge--inline kt-badge--pill">' + status[0].title + '</span>';    
                                } else {
                                    return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">'+row.title+'</span>';
                                }
                            },
                        }, 

                        {
                            field: 'Invoice',
                            title: 'Invoice',
                            // sortable: false,
                            // width: 110,
                            // overflow: 'visible',
                            // autoHide: false,
                            template: function(row) {
                                return '<a href="'+row.invoiceUrl+'/'+row.encrypt_id+'" class="link_redirect"><span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">Invoice</span><a>';
                            }
                        },
                    ],
        
                });
        
                $('#kt_form_status').on('change', function() {
                    datatable.search($(this).val().toLowerCase(), 'PaymentStatus');
                });
        
                $('#kt_form_type').on('change', function() {
                    datatable.search($(this).val().toLowerCase(), 'Type');
                });
        
                $('#kt_form_status,#kt_form_type').selectpicker();
        
            };
        
            return {
                // Public functions
                init: function() {
                    // init dmeo
                    demo();
                },
            };
        }();
        
        jQuery(document).ready(function() {
            KTDatatableDataLocalDemo.init();
        });
        
</script>
<script>
    $(document).ready(function(){
        $('#re_enter_content').on('click',function(e){
            var comment    = $(this).attr('data-content');
            var encrypt_id = $(this).attr('data-encrypt');
            Swal.fire({
              title: "Please check Comment before Re-submission",
              html: "<p class='text-danger'><strong>"+comment+"</strong></p>",
              type: "warning",
              showCancelButton: true,
              confirmButtonClass: "btn-success",
              confirmButtonText: "Proceed",
              cancelButtonText: "Cancel",
              closeOnConfirm: false,
              closeOnCancel: false
            }).then((result) => {
                if (result.value) {

                    redirectURL = "{{url('advertiser')}}"+'/content/'+encrypt_id;
                    window.location = redirectURL;
                }
            });
            e.stopImmediatePropagation();
            return false; 
        });
    });
</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script>
    $(document).on('click','.approve_link',function(e){
            
    var cart_id = $(this).attr('rel');
    var link    = $(this).attr('data-link');
    Swal.fire({
          title: "Are you sure?",
          text: "Select the option to check link",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-success",
          confirmButtonText: "Approve",
          cancelButtonText: "Disapproved",
          closeOnConfirm: false,
          closeOnCancel: false
        }).then((result) => {
        
            if (result.value) {
                 
                  Swal.fire({
                      html: '<a href="'+link+'" target="_blank" class="text-primary">Click here to check link</a>',
                      inputAttributes: {
                        autocapitalize: 'off'
                      },
                      showCancelButton: true,
                      confirmButtonText: 'Approve',
                      preConfirm: (login) => {
                            if(login == ''){
                                Swal.showValidationMessage(
                                  `Request failed: Not found`
                                )
                                return false;
                            } 
                      },
                      allowOutsideClick: () => !Swal.isLoading()
                    }).then((result) => {
                        if (result.value) {
                            var url = "{{url('stages-changes_2')}}";
                            $('.loader').fadeIn('slow'); 
                            $.ajax({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                url  : url,
                                type : 'POST',
                                data : {
                                    'cart_id'       : cart_id,
                                    'stages'        : 8,
                                    'link'          : link,
                                },
                                async: true,
                                dataType: 'json',
                                enctype: 'multipart/form-data',
                                cache: false, 
                                success:function(response){
                                    $('.loader').fadeOut('slow'); 
                                    if(response.status === true) {
                                        swal.fire({
                                          title: '',
                                          html: "Link has been Approved",
                                          icon: 'success',
                                          showCancelButton: false,
                                          confirmButtonClass: "btn btn-secondary",
                                          confirmButtonText: "Ok"
                                        }).then((result) => {
                                           window.location.href = "{{url('advertiser/purchases')}}";
                                        });
                                    } 
                                },
                                error:function(){
                                    Swal.showValidationMessage(
                                      `Request failed: Something were wrong`
                                    )
                                }
                            });
                        }
                    }); 
            }
            else{
            //disapproved
                Swal.fire({
                  html: '<a href="'+link+'" target="_blank" class="text-primary">Click here to check link</a>',
                  title: 'Enter Comment for Disapprove',
                  input: 'text',
                  inputAttributes: {
                    autocapitalize: 'off'
                  },
                  showCancelButton: true,
                  confirmButtonText: 'Disapproved',
                  preConfirm: (login) => {
                        if(login == ''){
                            Swal.showValidationMessage(
                              `Request failed: Not found`
                            )
                            return false;
                        } 
                  },
                  allowOutsideClick: () => !Swal.isLoading()
                }).then((result) => {
                    if (result.value) {
                        var url = "{{url('stages-changes_3')}}";
                        $('.loader').fadeIn('slow'); 
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url  : url,
                            type : 'POST',
                            data : {
                                'cart_id'       : cart_id,
                                'stages'        : 9,
                                'link'          : link,
                                'comment'       : result.value,

                            },
                            async: true,
                            dataType: 'json',
                            enctype: 'multipart/form-data',
                            cache: false, 
                            success:function(response){
                                $('.loader').fadeOut('slow'); 
                                if(response.status === true) {
                                    swal.fire({
                                      title: '',
                                      html: "Link has been disapproved",
                                      icon: 'success',
                                      showCancelButton: false,
                                      confirmButtonClass: "btn btn-secondary",
                                      confirmButtonText: "Ok"
                                    }).then((result) => {
                                       window.location.href = "{{url('advertiser/purchases')}}";
                                    });
                                } 
                            },
                            error:function(){
                                Swal.showValidationMessage(
                                  `Request failed: Something were wrong`
                                )
                            }
                        });
                    }
                });
            }
        });
     return false;

    });
</script>
<script>
    $(document).on('click','.payment_release',function(e){            
        $('#myModal').modal('show');
        $('#domain_id').val($(this).attr('data-id'));
        e.stopImmediatePropagation();
        return false;
    });
</script>
<script src="{{ asset('') . config('app.public_url') . '/js/jsRapStar.js' }}"></script>            
<!--end::Page Scripts -->
<script>
    $(document).ready(function(){
        $('#demo5').jsRapStar({colorFront:'red',length:5,starHeight:32,step:false,
            
            onClick:function(score){
                $(this)[0].StarF.css({color:'red'});
                $('#user_reviews').val(score);
                alert(score);
            },
            onMousemove:function(score){
                $(this).attr('title','Rate '+score);
            },
            onMouseleave(score){
                $('#label').text(score);
            }

        });
        $('#demo6').jsRapStar({colorFront:'red',length:5,starHeight:32,step:false,
        onClick:function(score){
            $(this)[0].StarF.css({color:'red'});
            $('#website_reviews').val(score);
        },
        onMousemove:function(score){
            $(this).attr('title','Rate '+score);
        },
        onMouseleave(score){
            $('#label').text(score);
        }
        });
    });
</script>
<script>
    $(document).on('click','#submit_reviews',function(e){
        
        var user_reviews    = $('#user_reviews').val();
        var website_reviews = $('#website_reviews').val();
        var domain_id       = $('#domain_id').val();
        var domain_url      = $('.payment_release').attr('data-domain-url');

        if(user_reviews == '' ||  website_reviews == ''){

            swal.fire('Select user and website Rating');
        }
        else{
            
            $.ajax({
                headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url  : "{{ url('/advertiser/add_reviews_db') }}",
                type : 'POST',
                data: {
                    'user_reviews'    : user_reviews,
                    'website_reviews' : website_reviews,
                    'domain_id'       : domain_id,
                    'domain_url'      : domain_url,
                },
                async: true,
                dataType: 'json',
                enctype: 'multipart/form-data',
                cache: false,                      
                success: function(response){

                    if(response.success == "true"){
                        swal.fire(response.message);
                        $('#myModal').modal('hide');               

                        var cart_id = $('.payment_release').attr('rel');

                        Swal.fire({
                          title: 'Reviews Submitted successfully',
                          html: "You can proceed to Release Payment",
                          icon: 'warning',
                          showCancelButton: true,
                          confirmButtonClass: 'btn btn-secondary',
                          cancelButtonClass: 'btn btn-secondary',
                          confirmButtonText: 'Yes'

                            }).then((result) => {
                            
                            if (result.value) {

                                Swal.fire({
                                  title: 'Are you sure',
                                  html: "Do you want to release payment?",
                                  icon: 'warning',
                                  showCancelButton: true,
                                  confirmButtonClass: 'btn btn-secondary',
                                  cancelButtonClass: 'btn btn-secondary',
                                  confirmButtonText: 'Yes'

                                    }).then((result) => {
                                    
                                    if (result.value) {
                                           Swal.fire({
                                          html : 'Give your confirmation',
                                          inputAttributes: {
                                            autocapitalize: 'off'
                                          },
                                          showCancelButton: true,
                                          confirmButtonText: 'Done',
                                          showLoaderOnConfirm: true,
                                          preConfirm: (login) => {
                                                if(login == ''){
                                                    Swal.showValidationMessage(
                                                      `Request failed: Not found`
                                                    )
                                                    return false;
                                                } 
                                          },
                                        allowOutsideClick: () => !Swal.isLoading()
                                        }).then((result) => {
                                        if (result.value) {
                                            var url = "{{url('/release_payment')}}";
                                            $('.loader').fadeIn('slow'); 
                                            $.ajax({
                                                headers: {
                                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                                },
                                                url  : url,
                                                type : 'POST',
                                                data : {
                                                    'cart_id'       : cart_id,
                                                    'stages'        : 11,
                                                },
                                                async: true,
                                                dataType: 'json',
                                                enctype: 'multipart/form-data',
                                                cache: false, 
                                                success:function(response){
                                                    $('.loader').fadeOut('slow'); 
                                                    if(response.status === true) {
                                                        swal.fire({
                                                          title: '',
                                                          html: "Payment has been release Successfully",
                                                          icon: 'success',
                                                          showCancelButton: false,
                                                          confirmButtonClass: "btn btn-secondary",
                                                          confirmButtonText: "Ok"
                                                        }).then((result) => {
                                                           window.location.href = "{{url('advertiser/purchases')}}";
                                                        });
                                                    } 
                                                },
                                                error:function(){
                                                    Swal.showValidationMessage(
                                                      `Request failed: Something were wrong`
                                                    )
                                                }
                                            });
                                        }
                                    }); 
                                        }
                                });
                            }
                        });

                        return false;
                        
                    }
                    else{

                        swal.fire(response.message);   
                    }
                },
                error: function(){}
            });
        }
        e.stopImmediatePropagation();
        return false; 
    });
</script>
<!--end::Page Vendors Styles -->
<!-- popup modal -->
<style>
*:before, *:after {
-webkit-box-sizing: border-box;
-moz-box-sizing: border-box;
box-sizing: border-box;
}

.clearfix {
  clear:both;
}

.text-center {text-align:center;}

a {
  color: tomato;
  text-decoration: none;
}

a:hover {
  color: #2196f3;
}

pre {
display: block;
padding: 9.5px;
margin: 0 0 10px;
font-size: 13px;
line-height: 1.42857143;
color: #333;
word-break: break-all;
word-wrap: break-word;
background-color: #F5F5F5;
border: 1px solid #CCC;
border-radius: 4px;
}

.header {
  padding:20px 0;
  position:relative;
  margin-bottom:10px;
  
}

.header:after {
  content:"";
  display:block;
  height:1px;
  background:#eee;
  position:absolute; 
  left:30%; right:30%;
}

.header h2 {
  font-size:3em;
  font-weight:300;
  margin-bottom:0.2em;
}

.header p {
  font-size:14px;
}



#a-footer {
  margin: 20px 0;
}

.new-react-version {
  padding: 20px 20px;
  border: 1px solid #eee;
  border-radius: 20px;
  box-shadow: 0 2px 12px 0 rgba(0,0,0,0.1);
  
  text-align: center;
  font-size: 14px;
  line-height: 1.7;
}

.new-react-version .react-svg-logo {
  text-align: center;
  max-width: 60px;
  margin: 20px auto;
  margin-top: 0;
}

.success-box {
  margin:50px 0;
  padding:10px 10px;
  border:1px solid #eee;
  background:#f9f9f9;
}

.success-box img {
  margin-right:10px;
  display:inline-block;
  vertical-align:top;
}

.success-box > div {
  vertical-align:top;
  display:inline-block;
  color:#888;
}



/* Rating Star Widgets Style */
.rating-stars ul {
  list-style-type:none;
  padding:0;
  
  -moz-user-select:none;
  -webkit-user-select:none;
}
.rating-stars ul > li.star {
  display:inline-block;
  
}

/* Idle State of the stars */
.rating-stars ul > li.star > i.fa {
  font-size:2.5em; /* Change the size of the stars */
  color:#ccc; /* Color on idle state */
}

/* Hover state of the stars */
.rating-stars ul > li.star.hover > i.fa {
  color:#FFCC36;
}

/* Selected state of the stars */
.rating-stars ul > li.star.selected > i.fa {
  color:#FF912C;
}

</style>
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <!-- review code start -->
                <h3>Share Rating!!!</h3>
                <p><strong>User Reviews</strong></p>
                <input type="hidden" id="domain_id" name="domain_id" value="" />
                <section class='rating-widget'>
                        <!-- Rating Stars Box -->
                        <input type="hidden" name="user_reviews" id="user_reviews" value="" />
                        <div class='rating-stars text-center'>
                            <ul id='stars'>
                              <li class='star' title='Poor' data-value='1'>
                                <i class='fa fa-star fa-fw'></i>
                              </li>
                              <li class='star' title='Fair' data-value='2'>
                                <i class='fa fa-star fa-fw'></i>
                              </li>
                              <li class='star' title='Good' data-value='3'>
                                <i class='fa fa-star fa-fw'></i>
                              </li>
                              <li class='star' title='Excellent' data-value='4'>
                                <i class='fa fa-star fa-fw'></i>
                              </li>
                              <li class='star' title='WOW!!!' data-value='5'>
                                <i class='fa fa-star fa-fw'></i>
                              </li>
                            </ul>
                         </div>
                      
                        <div class='success-box'>
                         <div class='clearfix'></div>
                         
                        <div class='text-message'></div>
                        <div class='clearfix'></div>
                    </div> 
                </section>
                <!-- reviews code end here -->
                <p><strong>Website Reviews</strong></p>
                <section class='rating-widget'>
                        <!-- Rating Stars Box -->
                        <input type="hidden" name="website_reviews" id="website_reviews" value="" />
                        <div class='rating-stars text-center'>
                            <ul id='stars_1'>
                              <li class='star' title='Poor' data-value='1'>
                                <i class='fa fa-star fa-fw'></i>
                              </li>
                              <li class='star' title='Fair' data-value='2'>
                                <i class='fa fa-star fa-fw'></i>
                              </li>
                              <li class='star' title='Good' data-value='3'>
                                <i class='fa fa-star fa-fw'></i>
                              </li>
                              <li class='star' title='Excellent' data-value='4'>
                                <i class='fa fa-star fa-fw'></i>
                              </li>
                              <li class='star' title='WOW!!!' data-value='5'>
                                <i class='fa fa-star fa-fw'></i>
                              </li>
                            </ul>
                         </div>
                      
                        <div class='success-box'>
                         <div class='clearfix'></div>
                         
                        <div class='text-message_1'></div>
                        <div class='clearfix'></div>

                    </div> 
                    
                    <div class="text-center">    
                        <button type="submit" id="submit_reviews" class="btn btn-primary">Submit Reviews</button>
                    </div>
                </section>
                <!-- end here -->
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- popup modal-->
<script>
$(document).ready(function(){
  
  /* 1. Visualizing things on Hover - See next part for action on click */
  $('#stars li').on('mouseover', function(){
    var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on
   
    // Now highlight all the stars that's not after the current hovered star
    $(this).parent().children('li.star').each(function(e){
      if (e < onStar) {
        $(this).addClass('hover');
      }
      else {
        $(this).removeClass('hover');
      }
    });
    
  }).on('mouseout', function(){
    $(this).parent().children('li.star').each(function(e){
      $(this).removeClass('hover');
    });
  });
  
  
  /* 2. Action to perform on click */
  $('#stars li').on('click', function(){
    var onStar = parseInt($(this).data('value'), 10); // The star currently selected
    var stars = $(this).parent().children('li.star');
    
    for (i = 0; i < stars.length; i++) {
      $(stars[i]).removeClass('selected');
    }
    
    for (i = 0; i < onStar; i++) {
      $(stars[i]).addClass('selected');
    }
    
    // JUST RESPONSE (Not needed)
    var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
    $('#user_reviews').val(ratingValue);
    var msg = "";
    if (ratingValue > 1) {
        msg = "Thanks! You rated this " + ratingValue + " stars.";
    }
    else {
        msg = "We will improve ourselves. You rated this " + ratingValue + " stars.";
    }
    responseMessage(msg);
    
  });
   $('#stars_1 li').on('click', function(){
    var onStar = parseInt($(this).data('value'), 10); // The star currently selected
    var stars = $(this).parent().children('li.star');
    
    for (i = 0; i < stars.length; i++) {
      $(stars[i]).removeClass('selected');
    }
    
    for (i = 0; i < onStar; i++) {
      $(stars[i]).addClass('selected');
    }
    
    // JUST RESPONSE (Not needed)
    var ratingValue = parseInt($('#stars_1 li.selected').last().data('value'), 10);
    $('#website_reviews').val(ratingValue);
    var msg = "";
    if (ratingValue > 1) {
        msg = "Thanks! You rated this " + ratingValue + " stars.";
    }
    else {
        msg = "We will improve ourselves. You rated this " + ratingValue + " stars.";
    }
    responseMessage_1(msg);
    
  });
  
  
});


function responseMessage(msg) {
  $('.success-box').fadeIn(200);  
  $('.success-box div.text-message').html("<span>" + msg + "</span>");
 
}
function responseMessage_1(msg) {
  $('.success-box').fadeIn(200);  
  $('.success-box div.text-message_1').html("<span>" + msg + "</span>");
}
</script>
    @show
@endsection
