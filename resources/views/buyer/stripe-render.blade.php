<form role="form" action="{{ route('stripeSave.post') }}" method="post" class="require-validation" data-cc-on-file="false" data-stripe-publishable-key="{{ env('STRIPE_KEY') }}" id="payment-form-card">
 @csrf
    <input id="amount_total" value="{{$total_price}}" name="amount" type="hidden">
    <input type="hidden" id="email" name="email" value="{{Auth::user()->email}}">
    <div class="form-group">
        <div class="input-group">
            <input autocomplete="off" class="form-control fullname required"  type="text" placeholder="Full name" name="fullname" maxlength="20" >
        </div>
    </div>

    <div class="form-group">
        <div class="input-group">
            <div class="input-group-prepend"><span class="input-group-text"><i class="la la-credit-card"></i></span></div>
            <input autocomplete="off" class="form-control card-number stripe-sensitive required" size="20" type="text" id="cnum" placeholder="Card number" name="card-number" maxlength="16">
        </div>
    </div>
    <div class="row form-group">
        <div class="col-4 kt-pl-0">
            <select class="form-control card-expiry-month" name="card-expiry-month" type="text">
            </select>
            <script type="text/javascript">
                        var select = $(".card-expiry-month"),
                        month = new Date().getMonth() + 1;
                    
                    for (var i = 1; i <= 12; i++) {
                        select.append($("<option value='"+i+"' "+(month === i ? "selected" : "")+">"+i+"</option>"))
                    }
                </script>
        </div>
        <div class="col-4">
            <select class="form-control card-expiry-year stripe-sensitive required" id="year" name="card-expiry-year"></select>
            <script type="text/javascript">
                    var select = $(".card-expiry-year"),
                    year = new Date().getFullYear();

                for (var i = 0; i < 12; i++) {
                    select.append($("<option value='"+(i + year)+"' "+(i === 0 ? "selected" : "")+">"+(i + year)+"</option>"))
                }
            </script>
        </div>
        <div class="col-4">
            <input autocomplete="off" class="form-control card-cvc stripe-sensitive required" size="3" type="password" maxlength="3" name="card-cvc" placeholder="CVV" >
        </div> 
    </div>
    <input type="hidden" name="promo_id_card_stripe" id="promo_id_card_stripe" value="0" />
    <span class="payment-errors"></span>
    <div class="col-12">
        <h6 class="text-center kt-mb-15"><span class="fa fa-lock kt-mr-5"></span>Your information is secure.</h6>
        <h6 class="text-center kt-mb-15" id="note">Note: Your card will be charged for ${{$total_price}} when the order is accepted by Publisher</h6>
        <div class="">
            <button type="submit" class="btn btn-brand btn-upper btn-block pay-later" name="submit-button">Pay Later ${{$total_price}}</button>
        </div>
    </form>

    <div class="kt-mt-10 text-center d-flex align-items-center justify-content-center">Powered by <i class="fab fa-stripe" style="font-size: 2.4rem;margin-left: 0.4rem;"></i></div>
</div>

<script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.8.1/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://js.stripe.com/v1/"></script>
<script type="text/javascript">
  Stripe.setPublishableKey("{{env('STRIPE_KEY')}}");
    $(document).ready(function() {
        function addInputNames() {
            // Not ideal, but jQuery's validate plugin requires fields to have names
            // so we add them at the last possible minute, in case any javascript 
            // exceptions have caused other parts of the script to fail.
            $(".card-number").attr("name", "card-number")
            $(".card-cvc").attr("name", "card-cvc")
            $(".card-expiry-year").attr("name", "card-expiry-year")
            $("#promo_id_card_stripe").attr("name", "promo_id_card_stripe")                                                        
        }

        function removeInputNames() {
            $(".card-number").removeAttr("name")
            $(".card-cvc").removeAttr("name")
            $(".card-expiry-year").removeAttr("name")
        }

        function submit(form) {
            // remove the input field names for security
            // we do this *before* anything else which might throw an exception
            //removeInputNames(); // THIS IS IMPORTANT!

            // given a valid form, submit the payment details to stripe
            $(form['submit-button']).attr("disabled", "disabled")

            Stripe.createToken({
                number: $('.card-number').val(),
                cvc: $('.card-cvc').val(),
                exp_month: $('.card-expiry-month').val(), 
                exp_year: $('.card-expiry-year').val()
            }, function(status, response) {
                if (response.error) {
                    // re-enable the submit button
                    $(form['submit-button']).removeAttr("disabled")

                    // show the error
                    $(".payment-errors").html(response.error.message);

                    // we add these names back in so we can revalidate properly
                    addInputNames();
                } else {
                    // token contains id, last4, and card type
                    var token = response['id'];

                    // insert the stripe token
                    var input = $("<input name='stripeToken' value='" + token + "' style='display:none;' />");
                    form.appendChild(input[0])

                    // and submit
                    form.submit();
                }
            });
            return false;
        }
        
        // add custom rules for credit card validating
        jQuery.validator.addMethod("cardNumber", Stripe.validateCardNumber, "Please enter a valid card number");
        jQuery.validator.addMethod("cardCVC", Stripe.validateCVC, "Please enter a valid security code");
        jQuery.validator.addMethod("cardExpiry", function() {
            return Stripe.validateExpiry($(".card-expiry-month").val(), 
                                         $(".card-expiry-year").val())
        }, "Please enter a valid expiration");

        // We use the jQuery validate plugin to validate required params on submit
        $("#payment-form-card").validate({
            submitHandler: submit,
            rules: {
                "card-cvc" : {
                    cardCVC: true,
                    required: true
                },
                "card-number" : {
                    cardNumber: true,
                    required: true
                },
                "card-expiry-year" : "cardExpiry" // we don't validate month separately
            }
        });

        // adding the input field names is the last step, in case an earlier step errors                
        addInputNames();
    });
</script>

<script>if (window.Stripe) $("#payment-form-card").show()</script>
<noscript><p>JavaScript is required for the registration form.</p></noscript>