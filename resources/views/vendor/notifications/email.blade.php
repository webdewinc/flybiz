
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="x-apple-disable-message-reformatting">
    <title></title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700">
    <!--   <link href="http://fly.biz/public/assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link href="http://fly.biz/public/assets/css/style.bundle.css" rel="stylesheet" type="text/css" /> -->
    <style>
    /* What it does: Remove spaces around the email design added by some email clients. */
    /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
    body {
        background-color: #fff;
    }

    table,
    td {
        mso-table-lspace: 0pt !important;
        mso-table-rspace: 0pt !important;
    }

    /* What it does: Fixes webkit padding issue. */
    table {
        border-spacing: 0 !important;
        border-collapse: collapse !important;
        table-layout: fixed !important;
        margin: 0 auto !important;
    }

    ul.social {
        padding: 0;
    }

    ul.social li {
        display: inline-block;
        margin-right: 10px;
    }

    /*FOOTER*/
    .footer ul {
        margin: 0;
        padding: 0;
    }

    .footer ul li {
        list-style: none;
        margin-bottom: 10px;
    }

    .footer ul li a {
        color: rgba(0, 0, 0, 1);
    }
    </style>
</head>

<body>
    <center style=" background-color: #fff;">
        <div style="max-width: 600px; margin: 0 auto; border: 2px solid #009bff;" class="email-container">
             <table align="center" class="" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto;padding:10px 20px;    color: #595d6e;">
                <tr>
                    <td valign="top">
                        <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%" style="margin: 0 auto 2rem !important;background: #e5f5ff;">
                            <tr>
                                <td style="text-align: center;padding: 1rem 0 2rem;">                                    
                                    <a href="{{url('/')}}" target="_blank" style="width: 110px;"><img src="{{ asset('') . config('app.public_url') . '/images/fly-biz-logo.png' }}" alt=""></a>
                                </td>
                            </tr> 
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div>
                            <div>
                                
                                {{-- Greeting --}}
                                @if (! empty($greeting))
                                    <p style="margin-bottom:10px;padding: 10px 20px;font-size: 13px;font-weight: 300;font-family: Poppins, Helvetica, sans-serif;">
                                        {{ $greeting }}
                                    </p>
                                @else
                                    @if ($level === 'error')
                                        @lang('Whoops!')
                                    @else
                                        <p style="margin-bottom:10px;padding: 10px 20px;font-size: 13px;font-weight: 300;font-family: Poppins, Helvetica, sans-serif;">                           
                                            @lang('Hello!'),
                                        </p>
                                    @endif
                                @endif
                                
                                <p style="margin-bottom:10px;padding: 10px 20px;font-size: 13px;font-weight: 300;font-family: Poppins, Helvetica, sans-serif;">
                                    {{-- Intro Lines --}}
                                    @foreach ($introLines as $line)
                                        {{ $line }}
                                    @endforeach

                                </p>

                                <div class="text" style="padding: 0 2.5em; text-align: center;">
                                    <p style="padding: 10px 20px;">
                                        {{-- Action Button --}}
                                        @isset($actionText)
                                        <?php
                                            switch ($level) {
                                                case 'success':
                                                case 'error':
                                                    $color = $level;
                                                    break;
                                                default:
                                                    $color = 'primary';
                                            }
                                        ?>
                                        @component('mail::button', ['url' => $actionUrl, 'color' => $color])
                                        {{ $actionText }}
                                        @endcomponent
                                        @endisset
                                    </p>
                                </div>

                                <p style="margin-bottom:10px;padding: 10px 20px;font-size: 13px;font-weight: 300;font-family: Poppins, Helvetica, sans-serif;">
                                {{-- Outro Lines --}}
                                @foreach ($outroLines as $line)
                                    {{ $line }}
                                @endforeach
                                </p>

                                <p style="margin-bottom:10px;padding: 10px 20px;font-size: 13px;font-weight: 300;font-family: Poppins, Helvetica, sans-serif;"><br>
                                    {{-- Salutation --}}
                                    @if (! empty($salutation))
                                    {{ $salutation }}
                                    @else
                                    @lang('Regards'),<br>
                                    {{ config('app.name') }}
                                    @endif    
                                </p>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto;">
                <tr>
                    <td valign="middle" class="footer" style="background: #e5f5ff;">
                        <table>
                            <tr style="text-align: left;">
                                <td valign="middle" width="40%" style="padding-top: 20px; text-align: center;padding-bottom: 20px;">
                                    <div class="kt-social_icons text-center w-100 pb-3">
                                        <a href="https://www.facebook.com/guestpostengine/" target="_blank" style="margin: 0.5rem;"><img src="{{ asset('') . config('app.public_url') . '/images/facebook.png' }}" alt=""></a>
                                        <a href="https://www.facebook.com/guestpostengine/" target="_blank" style="margin: 0.5rem;"><img src="{{ asset('') . config('app.public_url') . '/images/user.png' }}" alt=""></a>
                                        <a href="https://twitter.com/GuestPostEngine" target="_blank" style="margin: 0.5rem;"><img src="{{ asset('') . config('app.public_url') . '/images/twitter.png' }}" alt=""></a>
                                        <a href="https://twitter.com/GuestPostEngine" target="_blank" style="margin: 0.5rem;"><img src="{{ asset('') . config('app.public_url') . '/images/whatsapp.png' }}" alt=""></a>
                                        <a href="https://twitter.com/GuestPostEngine" target="_blank" style="margin: 0.5rem;"><img src="{{ asset('') . config('app.public_url') . '/images/skype.png' }}" alt=""></a>
                                        <a href="https://www.facebook.com/guestpostengine/" target="_blank" style="margin: 0.5rem;"><img src="{{ asset('') . config('app.public_url') . '/images/email.png' }}" alt=""></a>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </center>
</body>

</html>
