@extends('layout.app')

@php
if(Auth::check()):
    
    $switched = @Auth::user()->switched;
    $switched_to = 'Publisher';
    
else:
    
        $switched = 'publisher';
        $switched_to = 'Publisher';
    
endif;


$metatitle = "Profile section for " . $switched_to . " - GuestPostEngine";
$metadescription = "Under profile section, an " . $switched . " will be able to view his/her details which can be further used to contact him/her.";
@endphp

@section('title',$metatitle)

@section('title-description')

<meta name="description" content="{{$metadescription}}">
    
@endsection

@section('mainhead')
    @parent
@endsection

@section('content')
<link href="{{ asset('') . config('app.public_url') . '/assets/css/pages/wizard/wizard-2.css' }}" rel="stylesheet" type="text/css" />
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
            </div>
        </div>
        <!-- end:: Subheader -->
        <!-- begin:: Content -->
         <style>      
            .checked{  color:orange; }
        </style> 
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">
            <div class="kt-portlet">
                <div class="kt-portlet__body kt-portlet__body--fit">
                    <div class="kt-grid  kt-wizard-v2 kt-wizard-v2--white" id="kt_wizard_v2" data-ktwizard-state="step-first">
                        
                        <div class="kt-grid__item kt-wizard-v2__aside">
                            <!--begin: Form Wizard Nav -->
                            <div class="kt-wizard-v2__nav">
                                <!--doc: Remove "kt-wizard-v2__nav-items--clickable" class and also set 'clickableSteps: false' in the JS init to disable manually clicking step titles -->
                                <div class="kt-widget kt-widget--user-profile-1 w-100">
                                    <div class="kt-widget__head">
                                        <div class="kt-widget__media">
                                            @php
                                            $image = $user_data->image;
                                            @endphp
                                            @if(!empty($image))
                                            @php
                                                $image = url('storage/app/') . '/' . $image;
                                            @endphp
                                            @else
                                            @php
                                                $image = asset('') . config('app.public_url') .'/assets/media/users/300_21.jpg';
                                            @endphp
                                            @endif
                                            <img src="{{$image}}" alt="image">
                                        </div>
                                        <div class="kt-widget__content">
                                            <div class="kt-widget__section">
                                                <a href="#" class="kt-widget__username">
                                                    {{ $user_data->user_fname }}{{ $user_data->user_lname }}
                                                </a>
                                            </div>   
                                        </div>
                                    </div>
                                    <div class="kt-widget__body">
                                        <div class="kt-widget__content">
                                            {{--
                                            <div class="kt-widget__info">
                                                <span class="kt-widget__label">Email:</span>
                                                <a href="#" class="kt-widget__data">{{ $user_data->email }}</a>
                                            </div>

                                            <div class="kt-widget__info">
                                                <span class="kt-widget__label">Phone:</span>
                                                <a href="#" class="kt-widget__data">{{ $user_data->user_mob_no }}</a>
                                            </div>
                                            --}}
                                            <div class="kt-widget__info">
                                                <span class="kt-widget__label">Reviews:</span>
                                                <span class="kt-widget__data">
                                                    <div class="d-flex">         
                                                    <?php
                                                    $user_id = $user_data->user_id;
                                                        $review_lists = DB::table('reviews')->where('user_id',$user_id)->get()->toArray();
                                                        $calc_rating = 0;
                                                        foreach( $review_lists as $split_review_lists ){
                                                            if( $split_review_lists->type == 'user'){
                                                                $total_rec = count($review_lists);
                                                                $calc_rating += $split_review_lists->rating;
                                                            }
                                                        }
                                                        $averageStar = round($calc_rating / 5);
                                                    ?>
                                                      @if(empty($averageStar))
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                       @elseif($averageStar == 1)
                                                       <div class="kt-demo-icon__preview  checked">
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        @elseif( $averageStar == 2)
                                                        <div class="kt-demo-icon__preview checked">
                                                            <i class="fas fa-star" data-val ="1"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview checked">
                                                            <i class="fas fa-star" data-val ="2"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star" data-val ="3"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star" data-val ="4"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star" data-val ="5"></i>
                                                        </div>
                                                        @elseif( $averageStar == 3)
                                                        <div class="kt-demo-icon__preview checked">
                                                            <i class="fas fa-star" data-val ="1"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview checked">
                                                            <i class="fas fa-star" data-val ="2"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview checked">
                                                            <i class="fas fa-star" data-val = "3"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star" data-val ="4"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star" data-val ="5"></i>
                                                        </div>
                                                        @elseif( $averageStar == 4)
                                                        <div class="kt-demo-icon__preview checked">
                                                            <i class="fas fa-star" data-val ="1"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview checked">
                                                            <i class="fas fa-star" data-val ="2"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview checked">
                                                            <i class="fas fa-star" data-val = "3"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview checked">
                                                            <i class="fas fa-star" data-val = "4"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview">
                                                            <i class="fas fa-star" data-val ="5"></i>
                                                        </div>
                                                        @elseif( $averageStar == 5)
                                                        <div class="kt-demo-icon__preview checked">
                                                            <i class="fas fa-star" data-val ="1"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview checked">
                                                            <i class="fas fa-star" data-val ="2"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview checked">
                                                            <i class="fas fa-star" data-val = "3"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview checked">
                                                            <i class="fas fa-star" data-val = "4"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview checked">
                                                            <i class="fas fa-star" data-val = "5"></i>
                                                        </div>
                                                        <div class="kt-demo-icon__preview checked">
                                                            <i class="fas fa-star" data-val ="1"></i>
                                                        </div>
                                                        @endif
                                                    </div>

                                                </span>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="fixed-sidebar">
                        <section>
                            <div class="kt-portlet kt-portlet--tabs kt-portlet--height-fluid">
                                <div class="kt-portlet__head">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title">
                                            <i class="la la-filter kt-padding-r-5"></i>Filter Websites
                                        </h3>
                                    </div>
                                </div>
                                <div class="kt-portlet__body">
                                    <!-- <form class="" id="kt_subheader_search_form"> -->
                                        <div class="kt-input-icon kt-input-icon--right kt-subheader__search">
                                            <input type="text" class="form-control search-ajax" placeholder="Search Domain" id="generalSearch" rel="website" name="website" value="{{@$_GET['generalSearch']}}">
                                            <span class="kt-input-icon__icon kt-input-icon__icon--right">
                                                <span>
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                            <rect x="0" y="0" width="24" height="24"></rect>
                                                            <path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                                            <path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" fill="#000000" fill-rule="nonzero"></path>
                                                        </g>
                                                    </svg>
                                                    <!--<i class="flaticon2-search-1"></i>-->
                                                </span>
                                            </span>
                                        </div>
                                    <!-- </form> -->
                                    <div class="kt-checkbox-list kt-mt-25">                             
                                        <label class="kt-checkbox">
                                        <?php if(@$_GET['verify']) :?>

                                        
                                                <input type="checkbox" name="verify" id="owner_verify" checked="checkbox">   
                                        Show Owners Websites

                                        <?php   else: ?>
                                                <input type="checkbox" name="verify" id="owner_verify" >  
                                        Show Owners Websites
                                        <?php endif; ?>

                                        <span></span>
                                        </label>
                                    </div>
                                    <div class="kt-mt-20 da-irs">
                                        <label class="col-form-label">Domain Authority</label>
                                        <div class="kt-ion-range-slider">
                                            
                                            <input type="hidden" id="kt_slider_DA" name="da" />
                                        </div>
                                    </div>
                                    <div class="kt-mt-20 traffic-irs">
                                        <label class="col-form-label">Traffic</label>
                                        <div class="kt-ion-range-slider">
                                            <input type="hidden" id="kt_slider_traffic" name="traffc" />
                                        </div>
                                    </div>
                                    <div class="kt-mt-20 order-irs">
                                        <label class="col-form-label">Orders in Queue</label>
                                        <div class="kt-ion-range-slider">
                                            <input type="hidden" id="kt_slider_orders" name="order" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <?php 
                           $_REQUEST['q']= 'blog';
                            $query = $_REQUEST['q'];
                            $findme = '"';
                            $quotes = 0;
                            if (strpos($query, $findme) === false) {
                                $quotes = 0;
                                $findme   = "'";
                                if (strpos($query, $findme) === false) {
                                    $quotes = 0;
                                } else {
                                    $quotes = 1;
                                }
                            } else {
                                $quotes = 1;
                            }
                            $terms = str_replace('"', "", $query);
                            $terms = str_replace("'", "", $terms);
                        ?>
                        <input type="hidden" id="get_value" name="get_value" value="{{$terms}}">
                    </div>
                                <!-- <div class="kt-wizard-v2__nav-items kt-wizard-v2__nav-items--clickable">
                                    <div class="kt-wizard-v2__nav-item" data-ktwizard-type="step" data-ktwizard-state="current">
                                        <div class="kt-wizard-v2__nav-body">
                                            <div class="kt-wizard-v2__nav-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                        <path d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z" fill="#000000" fill-rule="nonzero"></path>
                                                        <path d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z" fill="#000000" opacity="0.3"></path>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="kt-wizard-v2__nav-label">
                                                <div class="kt-wizard-v2__nav-label-title">
                                                    Profile
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                            </div>
                            <!--end: Form Wizard Nav -->
                        </div>
                        <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v2__wrapper">
                            <!--begin: Form Wizard Step 1-->
                            <div class="kt-wizard-v2__content" data-ktwizard-type="step-content" data-ktwizard-state="current">
                                <div class="kt-form__section kt-form__section--first">
                                    <!--begin: Form Wizard Form-->
                                    <?php /* ?>
                                    <form class="kt-form" method="POST" action="{{ url('/update_singleProfile') }}">
                                        @csrf
                                        <div class="kt-wizard-v2__form">
                                            <div class="form-group row">
                                                <div class="col-md-6 col-12">
                                                    <div class="kt-avatar kt-avatar--outline kt-avatar--circle-" id="kt_user_edit_avatar">
                                                        
                                                        <!-- <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="Change avatar">
                                                            <i class="fa fa-pen"></i>
                                                            <input type="file" name="profile_avatar" accept=".png, .jpg, .jpeg" />
                                                        </label>
                                                        <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="Cancel avatar">
                                                            <i class="fa fa-times"></i>
                                                        </span> -->



                                                         @php
                                                            $image = @$user_data->image;
                                                            $name  = '';
                                                            $lname = '';
                                                        @endphp
                                                        @php
                                                            $name  = strtoupper($rest = substr(@$user_data->user_fname, 0, 1));

                                                            $lname = strtoupper($rest = substr(@$user_data->user_lname, 0, 1));
                                                        @endphp
                                                         
                                                       
                                                        @php
                                                            $image = asset('') . config('app.public_url') .'/assets/media/users/default.jpg';
                                                        @endphp
                                                        @if(!empty($name))
                                                            <div class="kt-widget4__pic kt-widget__pic--primary kt-font-primary kt-font-boldest kt-font-light">
                                                            <span class="kt-badge kt-badge--username kt-badge--unified-primary kt-badge--lg kt-badge--rounded kt-badge--bold">
                                                            {{$name}}{{$lname}}</span>
                                                                
                                                            </div>
                                                        @else
                                                            <div class="kt-widget4__pic kt-widget4__pic--pic">
                                                                <img src="{{$image}}" alt="">           
                                                            </div>

                                                        @endif
                                                       

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-md-6 col-12">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-user"></i></span></div>
                                                        <input type="text" class="form-control" name="first_name" placeholder="First Name" value="{{ @$user_data->user_fname }}"  maxlength="20" disabled="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-md-6 col-12">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-user"></i></span></div>
                                                        <input type="text" class="form-control" name="last_name" placeholder="Last Name" value="{{ @$user_data->user_lname }}"  maxlength="20" disabled=""  />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-md-6 col-12">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-user"></i></span></div>
                                                        <input type="text" class="form-control" name="user_code" placeholder="User Code" value="{{ @$user_data->user_code }}" disabled="" />
                                                    </div>
                                                </div>
                                            </div>
                                            {{--
                                            <div class="form-group row">
                                                <div class="col-md-6 col-12">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-globe"></i></span></div>
                                                        <input type="text" class="form-control" name="user_email" disabled=""   value="{{ @$user_data->email}}" disabled="disabled" />
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group row">
                                                <div class="col-md-6 col-12">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-phone"></i></span></div>
                                                        <input type="text" class="form-control" name="contact_number" placeholder="Phone" aria-describedby="basic-addon1" value="{{ @$user_data->user_mob_no }}"  maxlength="10" disabled=""  />
                                                    </div>
                                                </div>
                                            </div>
                                            --}}
                                        </div>
                                    </form>
                                <?php */ ?>
                                <div class="col-md-12 col-12" id="append-search-data">
                                    <!-- render search -->
                                    
                                    @include('render.search')
                                </div>
                                </div>
                            </div>
                            <!--end: Form Wizard Step 1-->
                            <!--begin: Form Wizard Step 2-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end:: Content -->
    </div>
</div>
<script src="{{ asset('') . config('app.public_url') . '/assets/js/pages/custom/wizard/wizard-2.js' }}" type="text/javascript"></script>
<!--begin::Page Vendors Styles(used by this page) -->
        <script href="{{ asset('') . config('app.public_url') . '/assets/js/owl.carousel.js'  }}" type="text/javascript"></script>
    <!--     <script src="{{ asset('') . config('app.public_url') . '/assets/js/pages/crud/forms/widgets/ion-range-slider.js' }}" type="text/javascript"></script> -->

        <script>
            // Class definition

            var KTIONRangeSlider = function () {
                
                var ajaxResultSlider = function () {
                    ajaxResult();
                };
                
                function ajaxResult(data_order='dp_da', data_order_wise='DESC' ){
                    var quotes = '<?php echo $quotes;?>';
                    var url = "{{url('/profile')}}";
                    // var q = $('#get_value').val();
                    var q = $('#search_keyword').val();

                    if(q != ''){
                        url += "?q="+q;    
                    }
                    var offset = "{{@$_GET['offset']}}";
                    if(offset != ''){
                        url += "&offset="+offset;    
                    }

                    url += "&quotes="+quotes;    
                    
                    var page = "{{@$_GET['page']}}";
                    if(page != ''){
                        url += "&page="+page;
                    }
                    var generalSearch = $('#generalSearch').val();
                    generalSearch = generalSearch.replace(/ /g, '');
                    if(generalSearch != ''){
                        url += "&generalSearch="+generalSearch;
                    }
                    var da_start = $('.da-irs .irs-from').text();
                    da_start = da_start.replace(/ /g, '');
                    var da_end = $('.da-irs .irs-to').text();
                    da_end = da_end.replace(/ /g, '');
                    if(da_start != '' && da_end != ''){
                        url += "&da_start="+da_start;
                        url += "&da_end="+da_end;
                    }
                    var traffic_start = $('.traffic-irs .irs-from').text();
                    traffic_start = traffic_start.replace(/ /g, '');
                    var traffic_end = $('.traffic-irs .irs-to').text();
                    traffic_end = traffic_end.replace(/ /g, '');
                    if(traffic_start != '' && traffic_end != ''){
                        if(traffic_start > 0 || traffic_end < 1000000){
                            url += "&traffic_start="+traffic_start;
                            url += "&traffic_end="+traffic_end;
                        }
                    }

                    var orders_start = $('.order-irs .irs-from').text();
                    orders_start = orders_start.replace(/ /g, '');
                    var orders_end = $('.order-irs .irs-to').text();
                    orders_end = orders_end.replace(/ /g, '');
                    if(orders_start != '' && orders_end != ''){
                        if(orders_start > 0 || orders_end < 100){
                            url += "&orders_start="+orders_start;
                            url += "&orders_end="+orders_end;
                        }
                    }

                    var $boxes = $('input[name=verify]:checked');
                    var checked = 2;
                    if($boxes[0]){
                        if($boxes[0].type == 'checkbox'){
                            checked = 1;
                            url += "&verify="+checked;
                        } else {
                            checked = 2;
                        }
                    }
                                

                    if($( "#da_high" ).hasClass( "kt-nav__item--active" )){
                        url += "&order=dp_da&orderwise=DESC";
                    } else if($( "#traffic_high" ).hasClass( "kt-nav__item--active" )){
                        url += "&order=sd_visits&orderwise=DESC";
                    } else if($( "#price_high" ).hasClass( "kt-nav__item--active" )){
                        url += "&order=cost_price&orderwise=DESC";
                    } else if($( "#price_low" ).hasClass( "kt-nav__item--active" )){
                        url += "&order=cost_price&orderwise=ASC";
                    } else {
                        url += "&order="+data_order+"&orderwise="+data_order_wise;
                    }
                    
                    $(".loader").fadeIn("slow");
                    window.location.href = url;

                    // $.ajax({
                    //     headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    //     url  : url,
                    //     type: 'GET',
                    //     cache: false,                        
                    //     beforeSend: function() {
                    //         //something before send
                    //         $(".loader").fadeIn("slow");
                    //     },
                    //     success: function(data) {
                    //         $('#append-search-data').html(data);
                    //         $(".loader").fadeOut("slow");
                    //     },
                    //     error: function(xhr,textStatus,thrownError) {
                    //         alert(xhr + "\n" + textStatus + "\n" + thrownError);
                    //     }
                    // });                    
                }

                // Private functions
                var demos = function () {

                    var get = <?php echo json_encode($_GET); ?>;
                    
                    var da_start = 20;
                    var da_end = 100;
                    if(get.da_start){
                        da_start = get.da_start;
                        da_end = get.da_end;
                    }

                    // min & max values
                    $('#kt_slider_DA').ionRangeSlider({
                        type: "double",
                        grid: true,
                        min: 20,
                        max: 100,
                        from: da_start,
                        to: da_end,
                        onFinish: ajaxResultSlider
                    });                    

                    var traffic_start = 0;
                    var traffic_end = 1000000;
                    if(get.traffic_start){
                        traffic_start = get.traffic_start;
                        traffic_end = get.traffic_end;
                    }


                    $('#kt_slider_traffic').ionRangeSlider({
                        type: "double",
                        grid: true,
                        min: 0,
                        max: 1000000,
                        //step:5000,
                        from: traffic_start,
                        to: traffic_end,
                       // scale:[100,200,300,400,500,600,1000000],
                        //isRange:true,
                        onFinish: ajaxResultSlider
                    });                    


                    var orders_start = 0;
                    var orders_end = 100;
                    if(get.orders_start){
                        orders_start = get.orders_start;
                        orders_end = get.orders_end;
                    }

                    $('#kt_slider_orders').ionRangeSlider({
                        type: "double",
                        grid: true,
                        min: 0,
                        max: 100,
                        from: orders_start,
                        to: orders_end,
                        onFinish: ajaxResultSlider
                    });


                    $(document).on('keypress','#generalSearch',function(event){
                        var keycode = (event.keyCode ? event.keyCode : event.which);
                        if(keycode == '13'){
                            ajaxResult();
                        }
                        
                    });
                    $(document).on('click','#owner_verify',function(){
                        ajaxResult();
                    });

                    $(document).on('click','.kt-nav .kt-nav__item .kt-nav__link',function() {
                        var rel = $(this).attr('rel');
                        var data_rel = $(this).attr('data-rel');
                        var data_order = $(this).attr('data-order');
                        var data_order_wise = $(this).attr('data-order-wise');
                        $('.kt-nav .kt-nav__item').removeClass('kt-nav__item--active');
                        $('#'+rel).addClass('kt-nav__item--active');
                        $('#sort-append').text(data_rel);                        
                        ajaxResult(data_order,data_order_wise);
                    });                    
                }

                return {
                    // public functions
                    init: function() {
                        demos(); 
                    }
                };
            }();

            jQuery(document).ready(function() {
                KTIONRangeSlider.init();
            });

            $(document).ready(function() {
              $('.owl-carousel').owlCarousel({
                loop: true,
                responsiveClass: true,
                responsive: {
                  0: {
                    items: 1,
                    nav: false,
                    dots: false,
                    autoplay: true
                  },
                  600: {
                    items: 2,
                    nav: false,
                    dots: false,
                    autoplay: true
                  },
                  768: {
                    items: 2,
                    nav: false,
                    dots: false
                  },
                  900: {
                    items: 3,
                    nav: false,
                    dots: false
                  },
                  1200: {
                    items: 4,
                    nav: false,
                    dots: false
                  },
                  1399: {
                    items: 5,
                    nav: false,
                    dots: false
                  }
                }
              })
            })          
        </script>
        <script type="text/javascript">
            //Getting value from "ajax.php".
            function fill(Value) {
               //Assigning value to "search" div in "search.php" file.
               $('#search_keyword').val(Value);
               window.location.href = 'search?q='+Value;
               //Hiding "display" div in "search.php" file.
               $('#display').hide();
            }

            $(document).on('click','.ajax-display',function(){
                fill($(this).attr('rel'));
            });

            /** script for onsroll down and up **/
            // $(window).keydown(function(e){ 
            //     if(e.which ===   40 ){
            //        //alert('true');
            //     }else if(e.which === 38){
            //         //alert('false');
            //     }
            // });

             /*$(document).hover('.list-items >  li > a',function() {
                    alert('hover working!!');
                });*/
            
            // window.onload = function(){
            //     var hideMe = document.getElementById('display');
            //     document.onclick = function(e){
            //        if(e.target.id !== 'hideMe'){

            //         alert('s');

            //          $('#display').hide();
                      /*$('#display').removeClass('search-dropdown');
                      $('#display').html('');*/
            //        }
            //     };
            // };
            $(document).ready(function() {
               //On pressing a key on "Search box" in "search.php" file. This function will be called.
               $("#search_keyword").keyup(function() {
                    var rel = $(this).attr('rel');
                    $('#display').addClass('search-dropdown');
                   //Assigning search box value to javascript variable named as "name".
                   var name = $(this).val();

                   //Validating, if "name" is empty.
                   if (name == "") {
                       //Assigning empty value to "display" div in "search.php" file.
                       $('#display').html("");
                   }
                   //If name is not empty.
                   else {
                       //AJAX is called.
                       $.ajax({
                           //AJAX type is "Post".
                           headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                           },
                            type: "POST",
                           //Data will be sent to "ajax.php".
                           url: "{{ url('/search_result') }}",
                           //Data, that will be sent to "ajax.php".
                           data: {
                               //Assigning value of "name" into "search" variable.
                               search: name,
                           },
                           //If result found, this funtion will be called.
                           success: function(html) {
                               //Assigning result to "display" div in "search.php" file.
                               $('#display').html(html).show();
                           }
                       });
                   }
               });
            });

            $(document).on('change','.kt-font-brand', function(){
                var value = $(this).val();
                var search = "{{@$_GET['q']}}";

                if(search){
                    search = '?q=' + search;
                }

                var page = "{{@$_GET['page']}}";
                if(page){
                    page = '&page=' + page;
                }

                if(value){
                    value = '&offset=' + value;   
                }
                var APP_URL = "{{url('')}}" + '/search/' + search + page + value;
                window.location.href = APP_URL;
            });


        </script>
@section('mainscripts')
@parent
@show
@endsection