@extends('layout.app')

@php
$switched = Auth::user()->switched;
if($switched == 'publisher'){
    $switched_to = 'Publisher';
} else if($switched == 'advertiser'){
    $switched_to = 'Advertiser';
}
$metatitle = "Edit Profile section for " . $switched_to . " - GuestPostEngine";
$metadescription = "Under Edit profile section, an " . $switched . " will be able to fill in his/her details which can be further used to contact him/her.";
@endphp

@section('title',$metatitle)

@section('title-description')

<meta name="description" content="{{$metadescription}}">
    
@endsection

@section('mainhead')
@parent
@endsection
@section('content')
<link href="{{ asset('') . config('app.public_url') . '/assets/css/pages/wizard/wizard-2.css' }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="{{ asset('') . config('app.public_url') . '/assets/css/intlTelInput.css' }}">
<link rel="stylesheet" href="{{ asset('') . config('app.public_url') . '/assets/css/demo.css' }}">

<style>
.checked {
  color: orange;
}
</style>

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
            </div>
        </div>

       
        <!-- end:: Subheader -->
        <!-- begin:: Content -->
        <div class="text-success"></div>
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">
            
            <div class="kt-portlet">
                <div class="kt-portlet__body kt-portlet__body--fit">

                    @php
                        $current_index = 1;
                    @endphp
                    @if(session()->has('message'))
                        @php
                            $current_index = session()->get('current_index')
                        @endphp
                    @endif

                    <div class="kt-grid  kt-wizard-v2 kt-wizard-v2--white" id="kt_wizard_v2" data-ktwizard-state="step-first">

                        <div class="kt-grid__item kt-wizard-v2__aside">
                            <!--begin: Form Wizard Nav -->
                            <div class="kt-wizard-v2__nav">
                                <!--doc: Remove "kt-wizard-v2__nav-items--clickable" class and also set 'clickableSteps: false' in the JS init to disable manually clicking step titles -->
                                <div class="kt-wizard-v2__nav-items kt-wizard-v2__nav-items--clickable">
                                   
                                    <div class="kt-wizard-v2__nav-item" data-ktwizard-type="step" data-ktwizard-state="current">
                                        <div class="kt-wizard-v2__nav-body">
                                            <div class="kt-wizard-v2__nav-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                        <path d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z" fill="#000000" fill-rule="nonzero"></path>
                                                        <path d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z" fill="#000000" opacity="0.3"></path>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="kt-wizard-v2__nav-label">
                                                <div class="kt-wizard-v2__nav-label-title">
                                                    Profile
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-wizard-v2__nav-item" data-ktwizard-type="step" >
                                        <div class="kt-wizard-v2__nav-body">
                                            <div class="kt-wizard-v2__nav-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                        <path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                                        <path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero"></path>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="kt-wizard-v2__nav-label">
                                                <div class="kt-wizard-v2__nav-label-title">
                                                    Account
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-wizard-v2__nav-item" data-ktwizard-type="step" >
                                        <div class="kt-wizard-v2__nav-body">
                                            <div class="kt-wizard-v2__nav-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <path d="M4,4 L11.6314229,2.5691082 C11.8750185,2.52343403 12.1249815,2.52343403 12.3685771,2.5691082 L20,4 L20,13.2830094 C20,16.2173861 18.4883464,18.9447835 16,20.5 L12.5299989,22.6687507 C12.2057287,22.8714196 11.7942713,22.8714196 11.4700011,22.6687507 L8,20.5 C5.51165358,18.9447835 4,16.2173861 4,13.2830094 L4,4 Z" fill="#000000" opacity="0.3"></path>
                                                        <path d="M12,11 C10.8954305,11 10,10.1045695 10,9 C10,7.8954305 10.8954305,7 12,7 C13.1045695,7 14,7.8954305 14,9 C14,10.1045695 13.1045695,11 12,11 Z" fill="#000000" opacity="0.3"></path>
                                                        <path d="M7.00036205,16.4995035 C7.21569918,13.5165724 9.36772908,12 11.9907452,12 C14.6506758,12 16.8360465,13.4332455 16.9988413,16.5 C17.0053266,16.6221713 16.9988413,17 16.5815,17 C14.5228466,17 11.463736,17 7.4041679,17 C7.26484009,17 6.98863236,16.6619875 7.00036205,16.4995035 Z" fill="#000000" opacity="0.3"></path>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="kt-wizard-v2__nav-label">
                                                <div class="kt-wizard-v2__nav-label-title">
                                                    Change Password
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                        if($switched == 'advertiser'):
                                    ?>
                                        <div class="kt-wizard-v2__nav-item" data-ktwizard-type="step" >
                                            <div class="kt-wizard-v2__nav-body">
                                                <div class="kt-wizard-v2__nav-icon">

                                                   <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
														<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
															<rect x="0" y="0" width="24" height="24"></rect>
															<circle fill="#000000" opacity="0.3" cx="20.5" cy="12.5" r="1.5"></circle>
															<rect fill="#000000" opacity="0.3" transform="translate(12.000000, 6.500000) rotate(-15.000000) translate(-12.000000, -6.500000) " x="3" y="3" width="18" height="7" rx="1"></rect>
															<path d="M22,9.33681558 C21.5453723,9.12084552 21.0367986,9 20.5,9 C18.5670034,9 17,10.5670034 17,12.5 C17,14.4329966 18.5670034,16 20.5,16 C21.0367986,16 21.5453723,15.8791545 22,15.6631844 L22,18 C22,19.1045695 21.1045695,20 20,20 L4,20 C2.8954305,20 2,19.1045695 2,18 L2,6 C2,4.8954305 2.8954305,4 4,4 L20,4 C21.1045695,4 22,4.8954305 22,6 L22,9.33681558 Z" fill="#000000"></path>
														</g>
													</svg>
                                                </div>
                                                <div class="kt-wizard-v2__nav-label">
                                                    <div class="kt-wizard-v2__nav-label-title">
                                                        Wallet
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    
                                </div>
                            </div>
                            <!--end: Form Wizard Nav -->
                        </div>
                        <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v2__wrapper">                            
                            <!--begin: Form Wizard Step 1-->
                            <div class="kt-wizard-v2__content" data-ktwizard-type="step-content">
                                <div class="kt-form__section kt-form__section--first">
                                   

                                    <!--begin: Form Wizard Form-->
                                    <form class="kt-form" method="POST" action="{{ url('/update_profile') }}" enctype="multipart/form-data">
                                        @csrf
                                        <div class="kt-wizard-v2__form">
                                            @if(session()->has('message'))
                                                @if($current_index == 1)
                                                    <div class="alert alert-outline-{{ session()->get('alert') }} fade show" role="alert">
                                                        <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                                        <div class="alert-text">{{ session()->get('message') }}</div>
                                                        <div class="alert-close">
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                <span aria-hidden="true"><i class="la la-close"></i></span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endif
                                            <div class="form-group row " id="image-append">
                                                <div class="col-md-6 col-12">
                                                    <div class="kt-avatar kt-avatar--outline kt-avatar--circle-" id="kt_user_edit_avatar">
                                                        @php
                                                            $image = Auth::user()->image;
                                                        @endphp
                                                        @if(!empty($image))
                                                        @php
                                                            $image = url('storage/app/') . '/' . $image;
                                                        @endphp
                                                        @else
                                                        @php
                                                            $image = asset('') . config('app.public_url') .'/assets/media/users/300_21.jpg';
                                                        @endphp
                                                        @endif
                                                        <div class="kt-avatar__holder" style="background-image: url('{{$image}}');"></div>
                                                        <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="" data-original-title="Change avatar">
                                                            <i class="fa fa-pen"></i>
                                                            <!-- <input type="file" name="image" accept=".png, .jpg, .jpeg" /> -->
                                                            <input type="file" id="image" name="image" accept="image/*"/>
                                                        </label>
                                                        <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="" data-original-title="Cancel avatar">
                                                            <i class="fa fa-times"></i>
                                                        </span>
                                                        
                                                    </div>
                                                    <div class="error" style="color:red;" id="image-error"></div>
                                                </div>

                                                <!-- user reviews start -->
                                                <div class="user_reviews">
                                                    <div class="kt-mb-5 d-flex">
                                                        <div class=" kt-ml-10 kt-mt-5">
                                                            <!-- <h4>User Reviews</h4> -->
                                                            <?php
                                                            $average_rating = 0;
                                                            $count_rating   = 0;
                                                            foreach( $reviews as $split_reviews ){
                                                                $count_rating += $split_reviews['rating'];
                                                                if($count_reviews <= '1'){

                                                                    $average_rating = $count_rating; 
                                                                }
                                                                else{

                                                                    $average_rating = round($count_rating / 5);    
                                                                }
                                                            }
                                                             ?>
                                                            <div class="d-flex website_reviews">
                                                                <?php  if( $average_rating == '1'){ ?>
                                                                <div class="kt-demo-icon__preview checked">
                                                                    <i class="fas fa-star"></i>
                                                                </div>
                                                                <div class="kt-demo-icon__preview">
                                                                    <i class="fas fa-star"></i>
                                                                </div>
                                                                <div class="kt-demo-icon__preview">
                                                                    <i class="fas fa-star"></i>
                                                                </div>
                                                                <div class="kt-demo-icon__preview">
                                                                    <i class="fas fa-star"></i>
                                                                </div>
                                                                <div class="kt-demo-icon__preview">
                                                                    <i class="fas fa-star"></i>
                                                                </div>
                                                                <?php  } 
                                                                else if( $average_rating == '2'){
                                                                ?>
                                                                <div class="kt-demo-icon__preview checked">
                                                                    <i class="fas fa-star"></i>
                                                                </div>
                                                                <div class="kt-demo-icon__preview checked">
                                                                    <i class="fas fa-star"></i>
                                                                </div>
                                                                <div class="kt-demo-icon__preview">
                                                                    <i class="fas fa-star"></i>
                                                                </div>
                                                                <div class="kt-demo-icon__preview">
                                                                    <i class="fas fa-star"></i>
                                                                </div>
                                                                <div class="kt-demo-icon__preview">
                                                                    <i class="fas fa-star"></i>
                                                                </div>
                                                                <?php  } 
                                                                else if( $average_rating == '3'){
                                                                ?>
                                                                <div class="kt-demo-icon__preview checked">
                                                                    <i class="fas fa-star"></i>
                                                                </div>
                                                                <div class="kt-demo-icon__preview checked">
                                                                    <i class="fas fa-star"></i>
                                                                </div>
                                                                <div class="kt-demo-icon__preview checked">
                                                                    <i class="fas fa-star"></i>
                                                                </div>
                                                                <div class="kt-demo-icon__preview">
                                                                    <i class="fas fa-star"></i>
                                                                </div>
                                                                <div class="kt-demo-icon__preview">
                                                                    <i class="fas fa-star"></i>
                                                                </div>
                                                                <?php  } 
                                                                else if( $average_rating == '4'){
                                                                ?>
                                                                <div class="kt-demo-icon__preview checked">
                                                                    <i class="fas fa-star"></i>
                                                                </div>
                                                                <div class="kt-demo-icon__preview checked">
                                                                    <i class="fas fa-star"></i>
                                                                </div>
                                                                <div class="kt-demo-icon__preview checked">
                                                                    <i class="fas fa-star"></i>
                                                                </div>
                                                                <div class="kt-demo-icon__preview checked">
                                                                    <i class="fas fa-star"></i>
                                                                </div>
                                                                <div class="kt-demo-icon__preview">
                                                                    <i class="fas fa-star"></i>
                                                                </div>
                                                                <?php  } 
                                                                else if( $average_rating == '5'){
                                                                ?>
                                                                <div class="kt-demo-icon__preview checked">
                                                                    <i class="fas fa-star"></i>
                                                                </div>
                                                                <div class="kt-demo-icon__preview checked">
                                                                    <i class="fas fa-star"></i>
                                                                </div>
                                                                <div class="kt-demo-icon__preview checked">
                                                                    <i class="fas fa-star"></i>
                                                                </div>
                                                                <div class="kt-demo-icon__preview checked">
                                                                    <i class="fas fa-star"></i>
                                                                </div>
                                                                <div class="kt-demo-icon__preview checked">
                                                                    <i class="fas fa-star"></i>
                                                                </div>
                                                                <?php } ?>
                                                            </div>
                                                            <?php
                                                            if(empty($reviews)){
                                                            ?>
                                                            <div class="d-flex website_reviews">
                                                                <div class="kt-demo-icon__preview">
                                                                    <i class="fas fa-star"></i>
                                                                </div>
                                                                <div class="kt-demo-icon__preview">
                                                                    <i class="fas fa-star"></i>
                                                                </div>
                                                                <div class="kt-demo-icon__preview">
                                                                    <i class="fas fa-star"></i>
                                                                </div>
                                                                <div class="kt-demo-icon__preview">
                                                                    <i class="fas fa-star"></i>
                                                                </div>
                                                                <div class="kt-demo-icon__preview">
                                                                    <i class="fas fa-star"></i>
                                                                </div>
                                                            </div>
                                                            <span style="
                                                                    display: flex;
                                                                    font-size: 0.9rem;
                                                                    color: #74788d;
                                                                    font-weight: normal;
                                                                    " class="kt-widget4__text">
                                                                    No Reviews
                                                            </span>
                                                            <?php } ?>
                                                        </div>
                                                    </div>  
                                                </div>
                                                <!-- user reviews end -->
                                    
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-md-6 col-12">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-user"></i></span></div>
                                                        <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" value="{{ @$user_data->user_fname }}"  maxlength="20" required />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-md-6 col-12">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-user"></i></span></div>
                                                        <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name" value="{{ @$user_data->user_lname }}"  maxlength="20" required />
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <div class="form-group row">
                                                <div class="col-md-6 col-12">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-user"></i></span></div>
                                                        <input type="text" class="form-control" name="domain_name" placeholder="Company Name" value="{{ @$user_data->domain_website }}" disabled="disabled" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-md-6 col-12">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-globe"></i></span></div>
                                                        <input type="text" class="form-control" name="domain_url" placeholder="Company URL" value="{{ @$user_data->domain_website}}" disabled="disabled" />
                                                        <div class="input-group-append"><span class="input-group-text">.com</span></div>
                                                    </div>
                                                </div>
                                            </div> -->
                                            <div class="form-group row">
                                                <div class="col-md-6 col-12">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-phone"></i></span></div>
                                                        <input type="hidden" name="selected_flag"  id="selected_flag" value="{{ @$user_data->select_flag }}" />
                                                        <input type="tel" class="form-control" id="contact_number" name="contact_number" placeholder="Phone" aria-describedby="basic-addon1" value="{{ @$user_data->user_mob_no }}"  maxlength="10" required />
                                                    </div>
                                                </div>
                                            </div>
                                             <div class="form-group row">
                                                <div class="col-md-6 col-12">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-paypal"></i></span></div>
                                                        <input type="email" class="form-control" id="paypal_email" name="paypal_email" placeholder="Enter Paypal Email" aria-describedby="basic-addon1"  value="{{ @$user_data->paypal_email }}" required />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="alert alert-primary fade show" role="alert">
                                                    <div class="alert-icon"><i class="flaticon-questions-circular-button"></i></div>
                                                    <div class="alert-text"><span style="font-weight: 500;">It is necessary to Enter Paypal Email to get Amount. Otherwise no amount will be paid.</span></div>
                                                </div>                                           
                                           </div>
                                            <div class="form-group">
                                                <div class="kt-form__actions">
                                                    <button class="btn btn-label-brand btn-bold" id="kt_update_info" type="submit">Update</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!--end: Form Wizard Step 1-->
                            <!--begin: Form Wizard Step 2-->
                            <div class="kt-wizard-v2__content" data-ktwizard-type="step-content" >
                                <div class="kt-form__section kt-form__section--first">
                                    
                                    <form class="kt-form" id="kt_form" method="POST" action="#">
                                        @csrf
                                        <div class="kt-wizard-v2__form">
                                            @if(session()->has('message'))
                                                @if($current_index == 2)
                                                    <div class="alert alert-outline-{{ session()->get('alert') }} fade show" role="alert">
                                                        <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                                        <div class="alert-text">{{ session()->get('message') }}</div>
                                                        <div class="alert-close">
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                <span aria-hidden="true"><i class="la la-close"></i></span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endif
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="form-group row">
                                                        <div class="col-md-6 col-12">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend"><span class="input-group-text"><i class="la la-user"></i></span></div>
                                                               
                                                                    <input class="form-control" name="username" type="text" value="{{ @$user_data->user_code }}" disabled="disabled" placeholder="Username" />
                                                               
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-md-6 col-12">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend"><span class="input-group-text"><i class="la la-at"></i></span></div>
                                                                <input type="email" class="form-control" name="email_address" value="{{ @$user_data->email }}" placeholder="Email" aria-describedby="basic-addon1" disabled="disabled" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- <div class="form-group">
                                                        <div class="kt-form__actions">
                                                            <button class="btn btn-label-brand btn-bold" type="submit" disabled>Update</button>
                                                        </div>
                                                    </div> -->
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!--end: Form Wizard Step 2-->
                            <!--begin: Form Wizard Step 5-->
                            <div class="kt-wizard-v2__content" data-ktwizard-type="step-content" >
                                <div class="kt-form__section kt-form__section--first">
                                    
                                    <form class="kt-form" name="password" method="POST" action="{{ url('/change_password') }}">
                                        @csrf
                                        <div class="kt-wizard-v2__form">
                                            @if(session()->has('message'))
                                                @if($current_index == 3)
                                                    <div class="alert alert-outline-{{ session()->get('alert') }} fade show" role="alert">
                                                        <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                                        <div class="alert-text">{{ session()->get('message') }}</div>
                                                        <div class="alert-close">
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                <span aria-hidden="true"><i class="la la-close"></i></span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endif
                                            <div class="row">
                                                <div class="col-12">                                                 
                                                    <div class="form-group row">
                                                        <div class="col-md-6 col-12">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend"><span class="input-group-text"><i class="la la-lock"></i></span></div>
                                                                <input type="password" class="form-control" name="current_password" id="current_password" placeholder="Current password"  minlength="8" maxlength="12">
                                                            </div>
                                                           
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-md-6 col-12">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend"><span class="input-group-text"><i class="la la-lock"></i></span></div>
                                                                <input type="password" id="new_password" class="form-control" name="new_password" placeholder="New password" minlength="8" maxlength="12">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group  row">
                                                        <div class="col-md-6 col-12">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend"><span class="input-group-text"><i class="la la-key"></i></span></div>
                                                                <input type="password" class="form-control" id="verify_password" name="verify_password" placeholder="Verify password" minlength="8" maxlength="12">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="kt-form__actions">
                                                            <button class="btn btn-label-brand btn-bold" id="kt_change_pass" type="submit">Update</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end: Form Actions -->
                                    </form>
                                </div>
                            </div>
                            <!--end: Form Wizard Step 5-->

                            <?php
                            if($switched == 'advertiser'):
                            ?>
                            <!--begin: Form Wizard Step 2-->
                            <div class="kt-wizard-v2__content" data-ktwizard-type="step-content" >
                                <div class="kt-form__section kt-form__section--first">
                                    
                                    <form class="kt-form" id="kt_form" >
                                        <div class="kt-wizard-v2__form">
                                            
                                            <div class="row">
											<h3 class="kt-portlet__head-title" style="color: #509cff;">
												Total Available Wallet Balance
											</h3>
                                                <!--<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                                                    <img src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDQ4MCA0ODAiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDQ4MCA0ODA7IiB4bWw6c3BhY2U9InByZXNlcnZlIiB3aWR0aD0iNTEycHgiIGhlaWdodD0iNTEycHgiPgo8cGF0aCBzdHlsZT0iZmlsbDojN0ZDQUM5OyIgZD0iTTgsMTEydjMyMGMwLDIyLjA5MSwxNy45MDksNDAsNDAsNDBoNDI0VjE1Mkg0OEMyNS45MDksMTUyLDgsMTM0LjA5MSw4LDExMnoiLz4KPGc+Cgk8Zz4KCQk8cGF0aCBpZD0iU1ZHQ2xlYW5lcklkXzAiIHN0eWxlPSJmaWxsOiMyMzg4OTI7IiBkPSJNNDgsMTc2aDMydjE2SDQ4VjE3NnoiLz4KCTwvZz4KCTxnPgoJCTxwYXRoIGlkPSJTVkdDbGVhbmVySWRfMSIgc3R5bGU9ImZpbGw6IzIzODg5MjsiIGQ9Ik05NiwxNzZoMzJ2MTZIOTZWMTc2eiIvPgoJPC9nPgoJPGc+CgkJPHBhdGggaWQ9IlNWR0NsZWFuZXJJZF8yIiBzdHlsZT0iZmlsbDojMjM4ODkyOyIgZD0iTTE0NCwxNzZoMzJ2MTZoLTMyVjE3NnoiLz4KCTwvZz4KCTxnPgoJCTxwYXRoIGlkPSJTVkdDbGVhbmVySWRfMyIgc3R5bGU9ImZpbGw6IzIzODg5MjsiIGQ9Ik0xOTIsMTc2aDMydjE2aC0zMlYxNzZ6Ii8+Cgk8L2c+Cgk8Zz4KCQk8cGF0aCBpZD0iU1ZHQ2xlYW5lcklkXzQiIHN0eWxlPSJmaWxsOiMyMzg4OTI7IiBkPSJNMjQwLDE3NmgzMnYxNmgtMzJWMTc2eiIvPgoJPC9nPgoJPGc+CgkJPHBhdGggaWQ9IlNWR0NsZWFuZXJJZF81IiBzdHlsZT0iZmlsbDojMjM4ODkyOyIgZD0iTTI4OCwxNzZoMzJ2MTZoLTMyVjE3NnoiLz4KCTwvZz4KCTxnPgoJCTxwYXRoIGlkPSJTVkdDbGVhbmVySWRfNiIgc3R5bGU9ImZpbGw6IzIzODg5MjsiIGQ9Ik0zMzYsMTc2aDMydjE2aC0zMlYxNzZ6Ii8+Cgk8L2c+Cgk8Zz4KCQk8cGF0aCBpZD0iU1ZHQ2xlYW5lcklkXzciIHN0eWxlPSJmaWxsOiMyMzg4OTI7IiBkPSJNMzg0LDE3NmgzMnYxNmgtMzJWMTc2eiIvPgoJPC9nPgoJPGc+CgkJPHBhdGggaWQ9IlNWR0NsZWFuZXJJZF84IiBzdHlsZT0iZmlsbDojMjM4ODkyOyIgZD0iTTQ4LDQzMmgzMnYxNkg0OFY0MzJ6Ii8+Cgk8L2c+Cgk8Zz4KCQk8cGF0aCBpZD0iU1ZHQ2xlYW5lcklkXzkiIHN0eWxlPSJmaWxsOiMyMzg4OTI7IiBkPSJNOTYsNDMyaDMydjE2SDk2VjQzMnoiLz4KCTwvZz4KCTxnPgoJCTxwYXRoIGlkPSJTVkdDbGVhbmVySWRfMTAiIHN0eWxlPSJmaWxsOiMyMzg4OTI7IiBkPSJNMTQ0LDQzMmgzMnYxNmgtMzJWNDMyeiIvPgoJPC9nPgoJPGc+CgkJPHBhdGggaWQ9IlNWR0NsZWFuZXJJZF8xMSIgc3R5bGU9ImZpbGw6IzIzODg5MjsiIGQ9Ik0xOTIsNDMyaDMydjE2aC0zMlY0MzJ6Ii8+Cgk8L2c+Cgk8Zz4KCQk8cGF0aCBpZD0iU1ZHQ2xlYW5lcklkXzEyIiBzdHlsZT0iZmlsbDojMjM4ODkyOyIgZD0iTTI0MCw0MzJoMzJ2MTZoLTMyVjQzMnoiLz4KCTwvZz4KCTxnPgoJCTxwYXRoIGlkPSJTVkdDbGVhbmVySWRfMTMiIHN0eWxlPSJmaWxsOiMyMzg4OTI7IiBkPSJNMjg4LDQzMmgzMnYxNmgtMzJWNDMyeiIvPgoJPC9nPgoJPGc+CgkJPHBhdGggaWQ9IlNWR0NsZWFuZXJJZF8xNCIgc3R5bGU9ImZpbGw6IzIzODg5MjsiIGQ9Ik0zMzYsNDMyaDMydjE2aC0zMlY0MzJ6Ii8+Cgk8L2c+Cgk8Zz4KCQk8cGF0aCBpZD0iU1ZHQ2xlYW5lcklkXzE1IiBzdHlsZT0iZmlsbDojMjM4ODkyOyIgZD0iTTM4NCw0MzJoMzJ2MTZoLTMyVjQzMnoiLz4KCTwvZz4KCTxnPgoJCTxwYXRoIGlkPSJTVkdDbGVhbmVySWRfMTYiIHN0eWxlPSJmaWxsOiMyMzg4OTI7IiBkPSJNNDMyLDE3NmgxNnYzMmgtMTZWMTc2eiIvPgoJPC9nPgoJPGc+CgkJPHBhdGggaWQ9IlNWR0NsZWFuZXJJZF8xNyIgc3R5bGU9ImZpbGw6IzIzODg5MjsiIGQ9Ik00MzIsMjI0aDE2djMyaC0xNlYyMjR6Ii8+Cgk8L2c+Cgk8Zz4KCQk8cGF0aCBpZD0iU1ZHQ2xlYW5lcklkXzE4IiBzdHlsZT0iZmlsbDojMjM4ODkyOyIgZD0iTTQzMiwyNzJoMTZ2MzJoLTE2VjI3MnoiLz4KCTwvZz4KCTxnPgoJCTxwYXRoIGlkPSJTVkdDbGVhbmVySWRfMTkiIHN0eWxlPSJmaWxsOiMyMzg4OTI7IiBkPSJNNDMyLDMyMGgxNnYzMmgtMTZWMzIweiIvPgoJPC9nPgoJPGc+CgkJPHBhdGggaWQ9IlNWR0NsZWFuZXJJZF8yMCIgc3R5bGU9ImZpbGw6IzIzODg5MjsiIGQ9Ik00MzIsMzY4LjAwOGgxNnYzMmgtMTZWMzY4LjAwOHoiLz4KCTwvZz4KCTxnPgoJCTxwYXRoIGlkPSJTVkdDbGVhbmVySWRfMjEiIHN0eWxlPSJmaWxsOiMyMzg4OTI7IiBkPSJNNDMyLDQxNi4wMDhoMTZ2MzJoLTE2VjQxNi4wMDh6Ii8+Cgk8L2c+CjwvZz4KPGNpcmNsZSBzdHlsZT0iZmlsbDojRkNGMDVBOyIgY3g9IjI0MCIgY3k9IjMxMiIgcj0iOTYiLz4KPGc+Cgk8cGF0aCBzdHlsZT0iZmlsbDojMjM4ODkyOyIgZD0iTTQ4LDcyQzI1LjkwOSw3Miw4LDg5LjkwOSw4LDExMnMxNy45MDksNDAsNDAsNDBoNTZWNzJINDh6Ii8+Cgk8cGF0aCBzdHlsZT0iZmlsbDojMjM4ODkyOyIgZD0iTTQwOCw3MnY4MGg2NHYtNDBWNzJINDA4eiIvPgo8L2c+CjxwYXRoIHN0eWxlPSJmaWxsOiNDOEQ5NTI7IiBkPSJNMTM2LDQwVjhoMjcydjE0NGgtMzJWNDBIMTM2eiIvPgo8cGF0aCBzdHlsZT0iZmlsbDojRjQ5RTIxOyIgZD0iTTI4MCwyNzJ2LTE2aC0zMnYtMTZoLTE2djE2aC0zMnY2NGg2NHYzMmgtNjR2MTZoMzJ2MTZoMTZ2LTE2aDMydi02NGgtNjR2LTMySDI4MHoiLz4KPHBhdGggc3R5bGU9ImZpbGw6IzU1QjU2QTsiIGQ9Ik0xMDQsNDBoMjcydjExMkgxMDRWNDB6Ii8+CjxnPgoJPHBhdGggc3R5bGU9ImZpbGw6I0ZDRjA1QTsiIGQ9Ik0xNDQsMTI4aC0xNlY2NGg2NHYxNmgtNDhWMTI4eiIvPgoJPHBhdGggc3R5bGU9ImZpbGw6I0ZDRjA1QTsiIGQ9Ik0zNTIsMTI4aC0xNlY4MGgtNDhWNjRoNjRWMTI4eiIvPgo8L2c+CjxnPgoJPHBhdGggc3R5bGU9ImZpbGw6IzIzODg5MjsiIGQ9Ik00MCwzMjhjLTQuNDE4LDAtOC0zLjU4Mi04LThWMjE2YzAtNC40MTgsMy41ODItOCw4LThzOCwzLjU4Miw4LDh2MTA0ICAgQzQ4LDMyNC40MTgsNDQuNDE4LDMyOCw0MCwzMjh6Ii8+Cgk8cGF0aCBzdHlsZT0iZmlsbDojMjM4ODkyOyIgZD0iTTQwLDM4NGMtNC40MTgsMC04LTMuNTgyLTgtOHYtMjRjMC00LjQxOCwzLjU4Mi04LDgtOHM4LDMuNTgyLDgsOHYyNEM0OCwzODAuNDE4LDQ0LjQxOCwzODQsNDAsMzg0ICAgeiIvPgo8L2c+CjxwYXRoIGlkPSJTVkdDbGVhbmVySWRfMF8xXyIgZD0iTTQ4LDE3NmgzMnYxNkg0OFYxNzZ6Ii8+CjxwYXRoIGlkPSJTVkdDbGVhbmVySWRfMV8xXyIgZD0iTTk2LDE3NmgzMnYxNkg5NlYxNzZ6Ii8+CjxwYXRoIGlkPSJTVkdDbGVhbmVySWRfMl8xXyIgZD0iTTE0NCwxNzZoMzJ2MTZoLTMyVjE3NnoiLz4KPHBhdGggaWQ9IlNWR0NsZWFuZXJJZF8zXzFfIiBkPSJNMTkyLDE3NmgzMnYxNmgtMzJWMTc2eiIvPgo8cGF0aCBpZD0iU1ZHQ2xlYW5lcklkXzRfMV8iIGQ9Ik0yNDAsMTc2aDMydjE2aC0zMlYxNzZ6Ii8+CjxwYXRoIGlkPSJTVkdDbGVhbmVySWRfNV8xXyIgZD0iTTI4OCwxNzZoMzJ2MTZoLTMyVjE3NnoiLz4KPHBhdGggaWQ9IlNWR0NsZWFuZXJJZF82XzFfIiBkPSJNMzM2LDE3NmgzMnYxNmgtMzJWMTc2eiIvPgo8cGF0aCBpZD0iU1ZHQ2xlYW5lcklkXzdfMV8iIGQ9Ik0zODQsMTc2aDMydjE2aC0zMlYxNzZ6Ii8+CjxwYXRoIGlkPSJTVkdDbGVhbmVySWRfOF8xXyIgZD0iTTQ4LDQzMmgzMnYxNkg0OFY0MzJ6Ii8+CjxwYXRoIGlkPSJTVkdDbGVhbmVySWRfOV8xXyIgZD0iTTk2LDQzMmgzMnYxNkg5NlY0MzJ6Ii8+CjxwYXRoIGlkPSJTVkdDbGVhbmVySWRfMTBfMV8iIGQ9Ik0xNDQsNDMyaDMydjE2aC0zMlY0MzJ6Ii8+CjxwYXRoIGlkPSJTVkdDbGVhbmVySWRfMTFfMV8iIGQ9Ik0xOTIsNDMyaDMydjE2aC0zMlY0MzJ6Ii8+CjxwYXRoIGlkPSJTVkdDbGVhbmVySWRfMTJfMV8iIGQ9Ik0yNDAsNDMyaDMydjE2aC0zMlY0MzJ6Ii8+CjxwYXRoIGlkPSJTVkdDbGVhbmVySWRfMTNfMV8iIGQ9Ik0yODgsNDMyaDMydjE2aC0zMlY0MzJ6Ii8+CjxwYXRoIGlkPSJTVkdDbGVhbmVySWRfMTRfMV8iIGQ9Ik0zMzYsNDMyaDMydjE2aC0zMlY0MzJ6Ii8+CjxwYXRoIGlkPSJTVkdDbGVhbmVySWRfMTVfMV8iIGQ9Ik0zODQsNDMyaDMydjE2aC0zMlY0MzJ6Ii8+CjxwYXRoIGlkPSJTVkdDbGVhbmVySWRfMTZfMV8iIGQ9Ik00MzIsMTc2aDE2djMyaC0xNlYxNzZ6Ii8+CjxwYXRoIGlkPSJTVkdDbGVhbmVySWRfMTdfMV8iIGQ9Ik00MzIsMjI0aDE2djMyaC0xNlYyMjR6Ii8+CjxwYXRoIGlkPSJTVkdDbGVhbmVySWRfMThfMV8iIGQ9Ik00MzIsMjcyaDE2djMyaC0xNlYyNzJ6Ii8+CjxwYXRoIGlkPSJTVkdDbGVhbmVySWRfMTlfMV8iIGQ9Ik00MzIsMzIwaDE2djMyaC0xNlYzMjB6Ii8+CjxwYXRoIGlkPSJTVkdDbGVhbmVySWRfMjBfMV8iIGQ9Ik00MzIsMzY4LjAwOGgxNnYzMmgtMTZWMzY4LjAwOHoiLz4KPHBhdGggaWQ9IlNWR0NsZWFuZXJJZF8yMV8xXyIgZD0iTTQzMiw0MTYuMDA4aDE2djMyaC0xNlY0MTYuMDA4eiIvPgo8cGF0aCBkPSJNMjQwLDQxNmM1Ny40MzgsMCwxMDQtNDYuNTYyLDEwNC0xMDRzLTQ2LjU2Mi0xMDQtMTA0LTEwNHMtMTA0LDQ2LjU2Mi0xMDQsMTA0QzEzNi4wNjYsMzY5LjQxLDE4Mi41OSw0MTUuOTM0LDI0MCw0MTZ6ICAgTTI0MCwyMjRjNDguNjAxLDAsODgsMzkuMzk5LDg4LDg4cy0zOS4zOTksODgtODgsODhzLTg4LTM5LjM5OS04OC04OEMxNTIuMDU3LDI2My40MjMsMTkxLjQyMywyMjQuMDU3LDI0MCwyMjR6Ii8+CjxwYXRoIGQ9Ik00MTYsMEgxMjh2MzJIOTZ2MzJINDhDMjEuNTAxLDY0LjAyNiwwLjAyNiw4NS41MDEsMCwxMTJ2MzIwYzAuMDI2LDI2LjQ5OSwyMS41MDEsNDcuOTc0LDQ4LDQ4aDQzMlY2NGgtNjRWMHogTTE0NCwxNmgyNTYgIHYxMjhoLTE2VjMySDE0NFYxNnogTTExMiw0OGgyNTZ2OTZIMTEyVjQ4eiBNNDgsODBoNDh2NjRINDhjLTE3LjY3MywwLTMyLTE0LjMyNy0zMi0zMlMzMC4zMjcsODAsNDgsODB6IE00NjQsNDY0SDQ4ICBjLTE3LjY3MywwLTMyLTE0LjMyNy0zMi0zMlYxNDcuNzUyYzAuMTQ0LDAuMTIsMC4zMTIsMC4yMTYsMC40NTYsMC4zMzZjMS44NywxLjYzNiwzLjg2MiwzLjEyNSw1Ljk2LDQuNDU2bDEuMDE2LDAuNjE2ICBjMi4xNjcsMS4zMDksNC40MzUsMi40NCw2Ljc4NCwzLjM4NGwwLjk4NCwwLjM2YzIuNDY4LDAuOTQxLDUuMDEsMS42NzQsNy42LDIuMTkybDAuNzEyLDAuMTA0YzIuOCwwLjUxOSw1LjY0LDAuNzg2LDguNDg4LDAuOGg0MTYgIFY0NjR6IE00NjQsODB2NjRoLTQ4VjgwSDQ2NHoiLz4KPHBhdGggZD0iTTIzMiwzODRoMTZ2LTE2aDMydi02NGgtNjR2LTMyaDY0di0xNmgtMzJ2LTE2aC0xNnYxNmgtMzJ2NjRoNjR2MzJoLTY0djE2aDMyVjM4NHoiLz4KPHBhdGggZD0iTTEyOCwxMjhoMTZWODBoNDhWNjRoLTY0VjEyOHoiLz4KPHBhdGggZD0iTTI4OCw4MGg0OHY0OGgxNlY2NGgtNjRWODB6Ii8+CjxwYXRoIGQ9Ik00MCwzMjhjNC40MTgsMCw4LTMuNTgyLDgtOFYyMTZjMC00LjQxOC0zLjU4Mi04LTgtOHMtOCwzLjU4Mi04LDh2MTA0QzMyLDMyNC40MTgsMzUuNTgyLDMyOCw0MCwzMjh6Ii8+CjxwYXRoIGQ9Ik00OCwzNTJjMC00LjQxOC0zLjU4Mi04LTgtOHMtOCwzLjU4Mi04LDh2MjRjMCw0LjQxOCwzLjU4Miw4LDgsOHM4LTMuNTgyLDgtOFYzNTJ6Ii8+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo=" width="100"  />
                                                </div>-->
                                                
                                                    
                                                    
                                                        <?php

                                                            $sum = \App\Wallet::where('user_id',Auth::user()->user_id)->sum('amount');
                                                            $pay = \App\Payment::where('user_id',Auth::user()->user_id)->sum('wallet_amount');
                                                            $t_sum = $sum - $pay;
                                                            if($t_sum > 0){
                                                                echo '<span class="wallet-balance-section custom">';
                                                                echo 'Wallet amount is : $' . $t_sum;
                                                                echo '</span>';
                                                            } else {
                                                                echo '<span class="wallet-balance-section custom">';
                                                                echo '$ ' . 0;
                                                                echo '</span>';
                                                            }

                                                        ?>
                                                    
                                               
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!--end: Form Wizard Step 2-->
                            <?php endif; ?>

                            <!--end: Form Wizard Form-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end:: Content -->
    </div>
</div>


<!-- <script src="{{ asset('') . config('app.public_url') . '/assets/js/pages/custom/wizard/wizard-2.js' }}" type="text/javascript"></script> -->
 <script src="{{ asset('') . config('app.public_url') . '/assets/js/intlTelInput.js' }}"></script>
 <script> 
    $(document).ready(function(){
        $('.iti__country-list li').on('click',function(){
            var item_id = $(this).attr('id');
             $('#selected_flag').val(item_id);
        });
        var selected_flag = $('#selected_flag').val();
        var rep_flag_code = selected_flag.replace("-item-","__");
        $('.iti__selected-flag .iti__flag').addClass(rep_flag_code);
    });
 </script>
  <script>
    var input = document.querySelector("#contact_number");
    window.intlTelInput(input, {
      // allowDropdown: false,
      // autoHideDialCode: false,
      // autoPlaceholder: "off",
      // dropdownContainer: document.body,
      // excludeCountries: ["us"],
      // formatOnDisplay: false,
      // geoIpLookup: function(callback) {
      //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
      //     var countryCode = (resp && resp.country) ? resp.country : "";
      //     callback(countryCode);
      //   });
      // },
      // hiddenInput: "full_number",
      // initialCountry: "auto",
      // localizedCountries: { 'de': 'Deutschland' },
      // nationalMode: false,
      // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
      // placeholderNumberType: "MOBILE",
      // preferredCountries: ['cn', 'jp'],
      // separateDialCode: true,
      utilsScript: "{{ asset('') . config('app.public_url') . '/assets/js/utils.js' }}",
    });
  </script>
<!-- <script src="{{ asset('') . config('app.public_url') . '/assets/js/pages/custom/user/edit-user.js' }}" type="text/javascript"></script> -->
<script type="text/javascript">
    
    "use strict";

// Class definition
var KTWizard2 = function () {
    // Base elements
    var wizardEl;
    var formEl;
    var validator;
    var wizard;

    // Private functions
    var initWizard = function () {
        // Initialize form wizard
        wizard = new KTWizard('kt_wizard_v2', {
            startStep: "{{$current_index}}" , // initial active step number
            clickableSteps: true  // allow step clicking
        });
        
        // Change event
        wizard.on('change', function(wizard) {

            KTUtil.scrollTop();
        });
    }

    var initValidation = function() {
        validator = formEl.validate({
            // Validate only visible fields
            ignore: ":hidden",

            // Validation rules
            rules: {
                //= Step 1
                
            },

            // Display error
            invalidHandler: function(event, validator) {
                KTUtil.scrollTop();

                swal.fire({
                    "title": "",
                    "text": "There are some errors in your submission. Please correct them.",
                    "type": "error",
                    "confirmButtonClass": "btn btn-secondary"
                });
            },

            // Submit valid form
            submitHandler: function (form) {

            }
        });
    }

    var initSubmit = function() {
        var btn = formEl.find('[data-ktwizard-type="action-submit"]');

        btn.on('click', function(e) {
            e.preventDefault();

            //Get reference of FileUpload.

            if (validator.form()) {                  

                // See: src\js\framework\base\app.js
                KTApp.progress(btn);
                //KTApp.block(formEl);

                // See: http://malsup.com/jquery/form/#ajaxSubmit
                formEl.ajaxSubmit({
                    success: function(response) {
                        KTApp.unprogress(btn);
                        //KTApp.unblock(formEl);

                        // swal.fire({
                        //     "title": "",
                        //     "text": "The application has been successfully submitted!",
                        //     "type": "success",
                        //     "confirmButtonClass": "btn btn-secondary"
                        // });
                    }
                });
            }
        });
    }

    return {
        // public functions
        init: function() {
            wizardEl = KTUtil.get('kt_wizard_v2');
            formEl = $('#kt_form');

            initWizard();
            initValidation();
            initSubmit();
        }
    };
}();

jQuery(document).ready(function() {
    KTWizard2.init();
});

// function Upload() {
//     //Get reference of FileUpload.
//     var fileUpload = document.getElementById("image");
 
//     //Check whether the file is valid Image.
//     var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.gif)$");
//     if (regex.test(fileUpload.value.toLowerCase())) {
 
//         //Check whether HTML5 is supported.
//         if (typeof (fileUpload.files) != "undefined") {
            
//             //Initiate the FileReader object.
            
//             //Read the contents of Image File.
//             reader.readAsDataURL(fileUpload.files[0]);
//             reader.onload = function (e) {
//                 //Initiate the JavaScript Image object.
//                 var image = new Image();
 
//                 //Set the Base64 string return from FileReader as source.
//                 image.src = e.target.result;
                       
//                 //Validate the File Height and Width.
//                 image.onload = function () {
//                     var height = this.height;
//                     var width = this.width;
//                     if (height > 100 || width > 100) {
//                         $('#image-append').addClass('validate is-invalid');
//                         $('#image-error').html("Height and Width must not exceed 100px.");
//                         $('#image-error').addClass('invalid-feedback');
//                         return;
//                     } else {
//                         $('#image-append').removeClass('validate is-invalid');
//                         $('#image-error').html("Uploaded image has valid Height and Width.");
//                         $('#image-error').removeClass('invalid-feedback');
//                         return true;
//                     }

//                 };
                
//             }
//         } else {
//             $('#image-append').addClass('validate is-invalid');
//             $('#image-error').html("This browser does not support HTML5.");
//             $('#image-error').addClass('invalid-feedback');
//             return;
//         }
//     } else {
//         $('#image-append').addClass('validate is-invalid');
//         $('#image-error').html("Please select a valid Image file.");
//         $('#image-error').addClass('invalid-feedback');        
//         return;
//     }
    
// //     is-invalid


// // first_name-error  

// }

// $('#image').on('change',function(){
//     var this_val = Upload();
// });
//  $('#kt_change_pass').click(function(e) {
//             e.preventDefault();

//             var btn = $(this);
//             var form = $(this).closest('form');          

//             form.validate({
//                 rules: {
//                     current_password: {
//                         required: true
//                     },
//                     new_password: {
//                         required: true
//                     },
//                     verify_password: {
//                         equalTo: "#new_password"
//                     },                  
//                 },
//                 messages: {
//                     current_password: {
//                         required: 'Please enter a current password.',
//                     },
//                     new_password : {
//                         required: 'Please enter a new valid password.',
//                     },
//                     verify_password: {
//                         equalTo: 'Please enter a valid password.'
//                     },
//                 },
//                 onkeyup: false,
//                 onfocusout: false,
//                 onclick: false
//             });

//             if (!form.valid()) {
//                 return;
//             }

//             form.submit();
//         });

//         $('#kt_update_info').click(function(e) {
//             e.preventDefault();

//             var btn = $(this);
//             var form = $(this).closest('form');    
//             // var upload = Upload();
//             // alert(upload);
//             // return;

//             form.validate({
//                 rules: {                                       
//                     first_name: {
//                         required: true
//                     },
//                     last_name: {
//                         required: true
//                     },
//                     contact_number: {                       
//                         required: false,
//                         digits: true,
//                     }
//                 },
//                 messages: {
//                     first_name: {
//                         required: 'Please enter a valid first name.',
//                     },
                   
//                     last_name: {
//                         required: ' Please enter a valid last name.',
//                     },
//                 },
//                 onkeyup: false,
//                 onfocusout: false,
//                 onclick: false
//             });

//             if (!form.valid()) {
//                 return;
//             }


            

//             form.submit();
//         });

        

 


        
// $(document).ready(function() {
//     $("#contact_number").keydown(function(event) {
//         // Allow only backspace and delete
//         if ( event.keyCode == 46 || event.keyCode == 8 ) {
//             // let it happen, don't do anything
//         }
//         else {
//             // Ensure that it is a number and stop the keypress
//             if (event.keyCode < 48 || event.keyCode > 57 ) {
//                 event.preventDefault(); 
//             }   
//         }
//     });
// });

</script>

@section('mainscripts')
@parent
@show
@endsection