@extends('layout.app')
@section('content')
@section('mainhead')
@parent
<!--begin::Page Vendors Styles(used by this page) -->
<link href="{{ asset('') . config('app.public_url') . '/assets/css/pages/wizard/wizard-2.css'  }}" rel="stylesheet" type="text/css" />
<!--end::Page Vendors Styles -->
@show
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
        <!-- begin:: Content -->
        <div class="kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            <div class="banner-wrapper text-center">
                <h2 class="">Alerts</h2>
                <div class="kt-quick-search kt-quick-search--dropdown kt-quick-search--result-compact" id="kt_quick_search_dropdown">
                    <form method="get" class="kt-quick-search__form">
                        <div class="input-group">
                            <div class="input-group-prepend"><span class="input-group-text"><i class="flaticon2-search-1"></i></span></div>
                            <input type="text" class="form-control kt-quick-search__input" placeholder="Search" value="">
                            <div class="input-group-append"><span class="input-group-text"><i class="la la-close kt-quick-search__close"></i></span></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">
            <div class="col-md-8 col-12 kt-mt-20" style="margin:auto;">
                <!--begin:: Widgets/Best Sellers-->
                <div class="kt-portlet kt-portlet--height-fluid">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">My alerts</h3>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="kt-widget5">
                            <div class="kt-widget5__item">
                                <div class="kt-widget5__content">
                                    <div class="kt-widget5__section">
                                        <a href="#" class="kt-widget5__title">alert1</a>
                                    </div>
                                </div>
                                <div class="kt-widget5__content">
                                    <div class=" d-flex">
                                        <a title="Edit details" class="btn btn-sm btn-clean btn-icon btn-icon-md" href="edit-website.html"> <i class="la la-edit"></i> </a>
                                        <a title="Delete" class="btn btn-sm btn-clean btn-icon btn-icon-md"> <i class="la la-trash"></i> </a></div>
                                </div>
                            </div>
                            <div class="kt-widget5__item">
                                <div class="kt-widget5__content">
                                    <div class="kt-widget5__section">
                                        <a href="#" class="kt-widget5__title">alert2</a>
                                    </div>
                                </div>
                                <div class="kt-widget5__content">
                                    <div class=" d-flex">
                                        <a title="Edit details" class="btn btn-sm btn-clean btn-icon btn-icon-md" href="edit-website.html"> <i class="la la-edit"></i> </a>
                                        <a title="Delete" class="btn btn-sm btn-clean btn-icon btn-icon-md"> <i class="la la-trash"></i> </a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end:: Widgets/Best Sellers-->
            </div>
        </div>
        <!--end:: Widgets/Best Sellers-->
    </div>
</div>
<!-- end:: Content -->

@section('mainscripts')
@parent
@show
@endsection