@extends('layout.app')
@php
$totalTitle = 'List of ' . total_website_count() . ' websites that accept Guest Blogs by GuestPostEngine.';
@endphp
@section('title', $totalTitle)
@section('title-description')

<meta name="description" content="GuestPostEngine is a database of 2416 websites of all categories that accept guest posting. In this pool, you can search websites according to your category.">
    
@endsection
@section('content')
@section('mainhead')
@parent
    <!--begin::Page Vendors Styles(used by this page) -->
    <link href="{{ asset('') . config('app.public_url') . '/assets/css/owl.carousel.min.css' }}" rel="stylesheet" type="text/css" />
    
        <!--end::Page Vendors Styles -->
@show
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="banner-wrapper text-center">
        <h2 class="">ABOUT US</h2>
    </div>
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor bg-white" id="kt_content">
        <!-- begin:: Content Head -->
        <!-- end:: Content Head -->
        <!-- begin:: Content -->
        <!-- better way html start here -->
        <div class="kt_about-banner pt-7">
            <div class="kt-container">
                <div class="row">                   
                    <div class="col-md-6" style="margin:auto;">
                        <div class="kt-better_way-video d-flex justify-content-between align-items-end">
                            <div class="kt-better_way-video-box text-center">
                                <iframe class="" width="560" height="315" src="https://www.youtube.com/embed/p8OqDB2tLjQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- better way html end here -->
        <!-- trusted by html start here -->
        <div class="col-12 kt-padding-0">
            <div class="kt_trusted-by">
                <div class="kt-container">
                    <div class="kt_trusted-by-inner d-flex flex-wrap">
                        <div class="kt_trusted-by_left text-center pt-5 pb-5 col-xl-2 col-lg-2 col-md-2">
                            <h4>Trusted by:</h4>
                        </div>
                        <div class="kt_trusted-by_slider col-xl-10 col-lg-10 col-md-10">
                            <div id="kt_trustedby-carousel" class="owl-carousel h-100 owl-theme d-flex align-items-center">
                                <div class="item">
                                    <div class="kt_trusted-by_logos">
                                        <img src="{{ asset('') . config('app.public_url') . '/assets/media/client-logos/calendar-logo.png' }}" alt="calendar-logo">
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="kt_trusted-by_logos">
                                        <img src="{{ asset('') . config('app.public_url') . '/assets/media/client-logos/knowledgehut-logo.png' }}" alt="knowledgehut-logo">
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="kt_trusted-by_logos">
                                        <img src="{{ asset('') . config('app.public_url') . '/assets/media/client-logos/rentomojo-logo.png' }}" alt="rentomojo-logo">
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="kt_trusted-by_logos">
                                        <img src="{{ asset('') . config('app.public_url') . '/assets/media/client-logos/simplilearn-logo.png' }}" alt="simplilearn-logo">
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="kt_trusted-by_logos">
                                        <img src="{{ asset('') . config('app.public_url') . '/assets/media/client-logos/intellect-logo.png' }}" alt="intellect-logo">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- trusted by html end here -->
        <!-- executive html start here -->
        <div class="kt_executive pt-7 pb-7">
            <div class="kt-container">
                <div class="row pb-5 justify-content-center">
                    <h2>Executive</h2>
                </div>
                <div class="row">
                    <div class="col-xl-8 offset-xl-2 d-flex flex-wrap align-items-center">
                        <div class="col-12">
                            <div class="kt-portlet kt-iconbox kt-icon-size kt-iconbox--primary kt-iconbox--animate-slow">
                                <div class="kt-portlet__body">
                                    <div class="row">
                                        <div class="kt-iconbox__icon col-xl-4 text-center">
                                            <img src="{{ asset('') . config('app.public_url') . '/assets/media/misc/Danish-Wadhwa.jpg' }}" alt="Danish-Wadhwa">
                                        </div>
                                        <div class="kt-iconbox__desc col-xl-8 kt-mt-20">
                                            <h3 class="kt-iconbox__title">Danish Wadhwa</h3>
                                            <h5 class="text-primary pb-2">Co-Founder & CEO</h5>
                                            <div class="kt-iconbox__content">
                                                <p>Danish Wadhwa is an Entrepreneur and Growth Hacker with more than ten years of expertise in Data Driven Marketing. He is a high energy individual fueled by his passion for helping businesses grow Digitally.</p>                                             
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- executive html end here -->

        <div class="kt_values-virtues pt-2 pb-7 d-none">
            <div class="kt-container">
                <div class="row col-xl-10 offset-xl-1">
                    <div class="col-xl-4">
                        <div class="row">
                            <div class="col-12 pb-5">
                                <div class="kt_values-believe">
                                    <h2>Values WE BELIEVE</h2>
                                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="kt-portlet kt-iconbox kt-iconbox--brand kt-iconbox--animate-slow">
                                    <div class="kt-portlet__body">
                                        <div class="kt-iconbox__body">
                                            <div class="kt-iconbox__icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <path d="M8,3 L8,3.5 C8,4.32842712 8.67157288,5 9.5,5 L14.5,5 C15.3284271,5 16,4.32842712 16,3.5 L16,3 L18,3 C19.1045695,3 20,3.8954305 20,5 L20,21 C20,22.1045695 19.1045695,23 18,23 L6,23 C4.8954305,23 4,22.1045695 4,21 L4,5 C4,3.8954305 4.8954305,3 6,3 L8,3 Z" fill="#000000" opacity="0.3"></path>
                                                        <path d="M10.875,15.75 C10.6354167,15.75 10.3958333,15.6541667 10.2041667,15.4625 L8.2875,13.5458333 C7.90416667,13.1625 7.90416667,12.5875 8.2875,12.2041667 C8.67083333,11.8208333 9.29375,11.8208333 9.62916667,12.2041667 L10.875,13.45 L14.0375,10.2875 C14.4208333,9.90416667 14.9958333,9.90416667 15.3791667,10.2875 C15.7625,10.6708333 15.7625,11.2458333 15.3791667,11.6291667 L11.5458333,15.4625 C11.3541667,15.6541667 11.1145833,15.75 10.875,15.75 Z" fill="#000000"></path>
                                                        <path d="M11,2 C11,1.44771525 11.4477153,1 12,1 C12.5522847,1 13,1.44771525 13,2 L14.5,2 C14.7761424,2 15,2.22385763 15,2.5 L15,3.5 C15,3.77614237 14.7761424,4 14.5,4 L9.5,4 C9.22385763,4 9,3.77614237 9,3.5 L9,2.5 C9,2.22385763 9.22385763,2 9.5,2 L11,2 Z" fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="kt-iconbox__desc">
                                                <h3 class="kt-iconbox__title">
                                                    <a class="kt-link" href="#">Tutorials</a>
                                                </h3>
                                                <div class="kt-iconbox__content">
                                                    Books &amp; Articles
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="kt-portlet kt-iconbox kt-iconbox--brand kt-iconbox--animate-slow">
                                    <div class="kt-portlet__body">
                                        <div class="kt-iconbox__body">
                                            <div class="kt-iconbox__icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <path d="M8,3 L8,3.5 C8,4.32842712 8.67157288,5 9.5,5 L14.5,5 C15.3284271,5 16,4.32842712 16,3.5 L16,3 L18,3 C19.1045695,3 20,3.8954305 20,5 L20,21 C20,22.1045695 19.1045695,23 18,23 L6,23 C4.8954305,23 4,22.1045695 4,21 L4,5 C4,3.8954305 4.8954305,3 6,3 L8,3 Z" fill="#000000" opacity="0.3"></path>
                                                        <path d="M10.875,15.75 C10.6354167,15.75 10.3958333,15.6541667 10.2041667,15.4625 L8.2875,13.5458333 C7.90416667,13.1625 7.90416667,12.5875 8.2875,12.2041667 C8.67083333,11.8208333 9.29375,11.8208333 9.62916667,12.2041667 L10.875,13.45 L14.0375,10.2875 C14.4208333,9.90416667 14.9958333,9.90416667 15.3791667,10.2875 C15.7625,10.6708333 15.7625,11.2458333 15.3791667,11.6291667 L11.5458333,15.4625 C11.3541667,15.6541667 11.1145833,15.75 10.875,15.75 Z" fill="#000000"></path>
                                                        <path d="M11,2 C11,1.44771525 11.4477153,1 12,1 C12.5522847,1 13,1.44771525 13,2 L14.5,2 C14.7761424,2 15,2.22385763 15,2.5 L15,3.5 C15,3.77614237 14.7761424,4 14.5,4 L9.5,4 C9.22385763,4 9,3.77614237 9,3.5 L9,2.5 C9,2.22385763 9.22385763,2 9.5,2 L11,2 Z" fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="kt-iconbox__desc">
                                                <h3 class="kt-iconbox__title">
                                                    <a class="kt-link" href="#">Tutorials</a>
                                                </h3>
                                                <div class="kt-iconbox__content">
                                                    Books &amp; Articles
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="kt-portlet kt-iconbox kt-iconbox--brand kt-iconbox--animate-slow">
                                    <div class="kt-portlet__body">
                                        <div class="kt-iconbox__body">
                                            <div class="kt-iconbox__icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <path d="M8,3 L8,3.5 C8,4.32842712 8.67157288,5 9.5,5 L14.5,5 C15.3284271,5 16,4.32842712 16,3.5 L16,3 L18,3 C19.1045695,3 20,3.8954305 20,5 L20,21 C20,22.1045695 19.1045695,23 18,23 L6,23 C4.8954305,23 4,22.1045695 4,21 L4,5 C4,3.8954305 4.8954305,3 6,3 L8,3 Z" fill="#000000" opacity="0.3"></path>
                                                        <path d="M10.875,15.75 C10.6354167,15.75 10.3958333,15.6541667 10.2041667,15.4625 L8.2875,13.5458333 C7.90416667,13.1625 7.90416667,12.5875 8.2875,12.2041667 C8.67083333,11.8208333 9.29375,11.8208333 9.62916667,12.2041667 L10.875,13.45 L14.0375,10.2875 C14.4208333,9.90416667 14.9958333,9.90416667 15.3791667,10.2875 C15.7625,10.6708333 15.7625,11.2458333 15.3791667,11.6291667 L11.5458333,15.4625 C11.3541667,15.6541667 11.1145833,15.75 10.875,15.75 Z" fill="#000000"></path>
                                                        <path d="M11,2 C11,1.44771525 11.4477153,1 12,1 C12.5522847,1 13,1.44771525 13,2 L14.5,2 C14.7761424,2 15,2.22385763 15,2.5 L15,3.5 C15,3.77614237 14.7761424,4 14.5,4 L9.5,4 C9.22385763,4 9,3.77614237 9,3.5 L9,2.5 C9,2.22385763 9.22385763,2 9.5,2 L11,2 Z" fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="kt-iconbox__desc">
                                                <h3 class="kt-iconbox__title">
                                                    <a class="kt-link" href="#">Tutorials</a>
                                                </h3>
                                                <div class="kt-iconbox__content">
                                                    Books &amp; Articles
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-8">
                        <div class="row">
                            <div class="col-12 pb-5">
                                <div class="kt_values-believe">
                                    <h2>Virtues WE BELIEVE</h2>
                                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum <br/> deleniti atque corrupti quos</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-12">
                                <div class="kt-portlet kt-iconbox kt-iconbox--brand kt-iconbox--animate-slow">
                                    <div class="kt-portlet__body">
                                        <div class="kt-iconbox__body">
                                            <div class="kt-iconbox__icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <path d="M8,3 L8,3.5 C8,4.32842712 8.67157288,5 9.5,5 L14.5,5 C15.3284271,5 16,4.32842712 16,3.5 L16,3 L18,3 C19.1045695,3 20,3.8954305 20,5 L20,21 C20,22.1045695 19.1045695,23 18,23 L6,23 C4.8954305,23 4,22.1045695 4,21 L4,5 C4,3.8954305 4.8954305,3 6,3 L8,3 Z" fill="#000000" opacity="0.3"></path>
                                                        <path d="M10.875,15.75 C10.6354167,15.75 10.3958333,15.6541667 10.2041667,15.4625 L8.2875,13.5458333 C7.90416667,13.1625 7.90416667,12.5875 8.2875,12.2041667 C8.67083333,11.8208333 9.29375,11.8208333 9.62916667,12.2041667 L10.875,13.45 L14.0375,10.2875 C14.4208333,9.90416667 14.9958333,9.90416667 15.3791667,10.2875 C15.7625,10.6708333 15.7625,11.2458333 15.3791667,11.6291667 L11.5458333,15.4625 C11.3541667,15.6541667 11.1145833,15.75 10.875,15.75 Z" fill="#000000"></path>
                                                        <path d="M11,2 C11,1.44771525 11.4477153,1 12,1 C12.5522847,1 13,1.44771525 13,2 L14.5,2 C14.7761424,2 15,2.22385763 15,2.5 L15,3.5 C15,3.77614237 14.7761424,4 14.5,4 L9.5,4 C9.22385763,4 9,3.77614237 9,3.5 L9,2.5 C9,2.22385763 9.22385763,2 9.5,2 L11,2 Z" fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="kt-iconbox__desc">
                                                <h3 class="kt-iconbox__title">
                                                    <a class="kt-link" href="#">Tutorials</a>
                                                </h3>
                                                <div class="kt-iconbox__content">
                                                    Books &amp; Articles
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="kt-portlet kt-iconbox kt-iconbox--brand kt-iconbox--animate-slow">
                                    <div class="kt-portlet__body">
                                        <div class="kt-iconbox__body">
                                            <div class="kt-iconbox__icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <path d="M8,3 L8,3.5 C8,4.32842712 8.67157288,5 9.5,5 L14.5,5 C15.3284271,5 16,4.32842712 16,3.5 L16,3 L18,3 C19.1045695,3 20,3.8954305 20,5 L20,21 C20,22.1045695 19.1045695,23 18,23 L6,23 C4.8954305,23 4,22.1045695 4,21 L4,5 C4,3.8954305 4.8954305,3 6,3 L8,3 Z" fill="#000000" opacity="0.3"></path>
                                                        <path d="M10.875,15.75 C10.6354167,15.75 10.3958333,15.6541667 10.2041667,15.4625 L8.2875,13.5458333 C7.90416667,13.1625 7.90416667,12.5875 8.2875,12.2041667 C8.67083333,11.8208333 9.29375,11.8208333 9.62916667,12.2041667 L10.875,13.45 L14.0375,10.2875 C14.4208333,9.90416667 14.9958333,9.90416667 15.3791667,10.2875 C15.7625,10.6708333 15.7625,11.2458333 15.3791667,11.6291667 L11.5458333,15.4625 C11.3541667,15.6541667 11.1145833,15.75 10.875,15.75 Z" fill="#000000"></path>
                                                        <path d="M11,2 C11,1.44771525 11.4477153,1 12,1 C12.5522847,1 13,1.44771525 13,2 L14.5,2 C14.7761424,2 15,2.22385763 15,2.5 L15,3.5 C15,3.77614237 14.7761424,4 14.5,4 L9.5,4 C9.22385763,4 9,3.77614237 9,3.5 L9,2.5 C9,2.22385763 9.22385763,2 9.5,2 L11,2 Z" fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="kt-iconbox__desc">
                                                <h3 class="kt-iconbox__title">
                                                    <a class="kt-link" href="#">Tutorials</a>
                                                </h3>
                                                <div class="kt-iconbox__content">
                                                    Books &amp; Articles
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="kt-portlet kt-iconbox kt-iconbox--brand kt-iconbox--animate-slow">
                                    <div class="kt-portlet__body">
                                        <div class="kt-iconbox__body">
                                            <div class="kt-iconbox__icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <path d="M8,3 L8,3.5 C8,4.32842712 8.67157288,5 9.5,5 L14.5,5 C15.3284271,5 16,4.32842712 16,3.5 L16,3 L18,3 C19.1045695,3 20,3.8954305 20,5 L20,21 C20,22.1045695 19.1045695,23 18,23 L6,23 C4.8954305,23 4,22.1045695 4,21 L4,5 C4,3.8954305 4.8954305,3 6,3 L8,3 Z" fill="#000000" opacity="0.3"></path>
                                                        <path d="M10.875,15.75 C10.6354167,15.75 10.3958333,15.6541667 10.2041667,15.4625 L8.2875,13.5458333 C7.90416667,13.1625 7.90416667,12.5875 8.2875,12.2041667 C8.67083333,11.8208333 9.29375,11.8208333 9.62916667,12.2041667 L10.875,13.45 L14.0375,10.2875 C14.4208333,9.90416667 14.9958333,9.90416667 15.3791667,10.2875 C15.7625,10.6708333 15.7625,11.2458333 15.3791667,11.6291667 L11.5458333,15.4625 C11.3541667,15.6541667 11.1145833,15.75 10.875,15.75 Z" fill="#000000"></path>
                                                        <path d="M11,2 C11,1.44771525 11.4477153,1 12,1 C12.5522847,1 13,1.44771525 13,2 L14.5,2 C14.7761424,2 15,2.22385763 15,2.5 L15,3.5 C15,3.77614237 14.7761424,4 14.5,4 L9.5,4 C9.22385763,4 9,3.77614237 9,3.5 L9,2.5 C9,2.22385763 9.22385763,2 9.5,2 L11,2 Z" fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="kt-iconbox__desc">
                                                <h3 class="kt-iconbox__title">
                                                    <a class="kt-link" href="#">Tutorials</a>
                                                </h3>
                                                <div class="kt-iconbox__content">
                                                    Books &amp; Articles
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="kt-portlet kt-iconbox kt-iconbox--brand kt-iconbox--animate-slow">
                                    <div class="kt-portlet__body">
                                        <div class="kt-iconbox__body">
                                            <div class="kt-iconbox__icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <path d="M8,3 L8,3.5 C8,4.32842712 8.67157288,5 9.5,5 L14.5,5 C15.3284271,5 16,4.32842712 16,3.5 L16,3 L18,3 C19.1045695,3 20,3.8954305 20,5 L20,21 C20,22.1045695 19.1045695,23 18,23 L6,23 C4.8954305,23 4,22.1045695 4,21 L4,5 C4,3.8954305 4.8954305,3 6,3 L8,3 Z" fill="#000000" opacity="0.3"></path>
                                                        <path d="M10.875,15.75 C10.6354167,15.75 10.3958333,15.6541667 10.2041667,15.4625 L8.2875,13.5458333 C7.90416667,13.1625 7.90416667,12.5875 8.2875,12.2041667 C8.67083333,11.8208333 9.29375,11.8208333 9.62916667,12.2041667 L10.875,13.45 L14.0375,10.2875 C14.4208333,9.90416667 14.9958333,9.90416667 15.3791667,10.2875 C15.7625,10.6708333 15.7625,11.2458333 15.3791667,11.6291667 L11.5458333,15.4625 C11.3541667,15.6541667 11.1145833,15.75 10.875,15.75 Z" fill="#000000"></path>
                                                        <path d="M11,2 C11,1.44771525 11.4477153,1 12,1 C12.5522847,1 13,1.44771525 13,2 L14.5,2 C14.7761424,2 15,2.22385763 15,2.5 L15,3.5 C15,3.77614237 14.7761424,4 14.5,4 L9.5,4 C9.22385763,4 9,3.77614237 9,3.5 L9,2.5 C9,2.22385763 9.22385763,2 9.5,2 L11,2 Z" fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="kt-iconbox__desc">
                                                <h3 class="kt-iconbox__title">
                                                    <a class="kt-link" href="#">Tutorials</a>
                                                </h3>
                                                <div class="kt-iconbox__content">
                                                    Books &amp; Articles
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="kt-portlet kt-iconbox kt-iconbox--brand kt-iconbox--animate-slow">
                                    <div class="kt-portlet__body">
                                        <div class="kt-iconbox__body">
                                            <div class="kt-iconbox__icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <path d="M8,3 L8,3.5 C8,4.32842712 8.67157288,5 9.5,5 L14.5,5 C15.3284271,5 16,4.32842712 16,3.5 L16,3 L18,3 C19.1045695,3 20,3.8954305 20,5 L20,21 C20,22.1045695 19.1045695,23 18,23 L6,23 C4.8954305,23 4,22.1045695 4,21 L4,5 C4,3.8954305 4.8954305,3 6,3 L8,3 Z" fill="#000000" opacity="0.3"></path>
                                                        <path d="M10.875,15.75 C10.6354167,15.75 10.3958333,15.6541667 10.2041667,15.4625 L8.2875,13.5458333 C7.90416667,13.1625 7.90416667,12.5875 8.2875,12.2041667 C8.67083333,11.8208333 9.29375,11.8208333 9.62916667,12.2041667 L10.875,13.45 L14.0375,10.2875 C14.4208333,9.90416667 14.9958333,9.90416667 15.3791667,10.2875 C15.7625,10.6708333 15.7625,11.2458333 15.3791667,11.6291667 L11.5458333,15.4625 C11.3541667,15.6541667 11.1145833,15.75 10.875,15.75 Z" fill="#000000"></path>
                                                        <path d="M11,2 C11,1.44771525 11.4477153,1 12,1 C12.5522847,1 13,1.44771525 13,2 L14.5,2 C14.7761424,2 15,2.22385763 15,2.5 L15,3.5 C15,3.77614237 14.7761424,4 14.5,4 L9.5,4 C9.22385763,4 9,3.77614237 9,3.5 L9,2.5 C9,2.22385763 9.22385763,2 9.5,2 L11,2 Z" fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="kt-iconbox__desc">
                                                <h3 class="kt-iconbox__title">
                                                    <a class="kt-link" href="#">Tutorials</a>
                                                </h3>
                                                <div class="kt-iconbox__content">
                                                    Books &amp; Articles
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="kt-portlet kt-iconbox kt-iconbox--brand kt-iconbox--animate-slow">
                                    <div class="kt-portlet__body">
                                        <div class="kt-iconbox__body">
                                            <div class="kt-iconbox__icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <path d="M8,3 L8,3.5 C8,4.32842712 8.67157288,5 9.5,5 L14.5,5 C15.3284271,5 16,4.32842712 16,3.5 L16,3 L18,3 C19.1045695,3 20,3.8954305 20,5 L20,21 C20,22.1045695 19.1045695,23 18,23 L6,23 C4.8954305,23 4,22.1045695 4,21 L4,5 C4,3.8954305 4.8954305,3 6,3 L8,3 Z" fill="#000000" opacity="0.3"></path>
                                                        <path d="M10.875,15.75 C10.6354167,15.75 10.3958333,15.6541667 10.2041667,15.4625 L8.2875,13.5458333 C7.90416667,13.1625 7.90416667,12.5875 8.2875,12.2041667 C8.67083333,11.8208333 9.29375,11.8208333 9.62916667,12.2041667 L10.875,13.45 L14.0375,10.2875 C14.4208333,9.90416667 14.9958333,9.90416667 15.3791667,10.2875 C15.7625,10.6708333 15.7625,11.2458333 15.3791667,11.6291667 L11.5458333,15.4625 C11.3541667,15.6541667 11.1145833,15.75 10.875,15.75 Z" fill="#000000"></path>
                                                        <path d="M11,2 C11,1.44771525 11.4477153,1 12,1 C12.5522847,1 13,1.44771525 13,2 L14.5,2 C14.7761424,2 15,2.22385763 15,2.5 L15,3.5 C15,3.77614237 14.7761424,4 14.5,4 L9.5,4 C9.22385763,4 9,3.77614237 9,3.5 L9,2.5 C9,2.22385763 9.22385763,2 9.5,2 L11,2 Z" fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="kt-iconbox__desc">
                                                <h3 class="kt-iconbox__title">
                                                    <a class="kt-link" href="#">Tutorials</a>
                                                </h3>
                                                <div class="kt-iconbox__content">
                                                    Books &amp; Articles
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- executive html end here -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">
            <div class="row">
                <!-- body content start -->
                <!-- trusted by html start here -->
                <div class="col-12 d-none">
                    <div class="kt_trusted-by">
                        <div class="kt_trusted-by-inner d-flex flex-wrap">
                            <div class="kt_trusted-by_left text-center pt-5 pb-5 col-xl-2 col-lg-2 col-md-2">
                                <h4>Trusted by:</h4>
                            </div>
                            <div class="kt_trusted-by_slider col-xl-10 col-lg-10 col-md-10">
                                <div id="kt_trustedby-carousel" class="owl-carousel h-100 owl-theme d-flex align-items-center">
                                    <div class="item">
                                        <div class="kt_trusted-by_logos">
                                            <img src="{{ asset('') . config('app.public_url') . '/assets/media/client-logos/calendar-logo.png'}}" alt="calendar-logo">
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="kt_trusted-by_logos">
                                            <img src="{{ asset('') . config('app.public_url') . '/assets/media/client-logos/knowledgehut-logo.png'}}" alt="knowledgehut-logo">
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="kt_trusted-by_logos">
                                            <img src="{{ asset('') . config('app.public_url') . '/assets/media/client-logos/rentomojo-logo.png'}}" alt="rentomojo-logo">
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="kt_trusted-by_logos">
                                            <img src="{{ asset('') . config('app.public_url') . '/assets/media/client-logos/simplilearn-logo.png'}}" alt="simplilearn-logo">
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="kt_trusted-by_logos">
                                            <img src="{{ asset('') . config('app.public_url') . '/assets/media/client-logos/intellect-logo.png'}}" alt="intellect-logo">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- trusted by html end here -->
                <!-- publish post html start here -->
                <div class="col-12 kt-hidden">
                    <div class="kt_publish-post pt-7 pb-6">
                        <div class="kt_publish-post-head text-center">
                            <h2 class="pb-3">Publish your guest posts on quality, high traffic websites</h2>
                            <p>Accessily is the first and the leading marketplace for Guest Posts. You can buy guest posts.</p>
                        </div>
                    </div>
                    <div class="col-md-10 col-12 offset-md-1 pb-6">
                        <div class="row">
                            <!--begin:: Portlet-->
                            <div class="kt-portlet">
                                <div class="kt-portlet__body">
                                    <div class="kt-widget kt-widget--user-profile-3">
                                        <div class="kt-widget__top">
                                            <div class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-font-light kt-hidden">
                                                JM
                                            </div>
                                            <div class="kt-widget__content">
                                                <div class="kt-widget__head">
                                                    <div class="d-flex align-items-center">
                                                        <div class="kt-widget__media kt-hidden-">
                                                            <img src="https://www.webdew.com/hubfs/favicon-3.ico" alt="image">
                                                        </div>
                                                        <a href="#" class="kt-widget__username">
                                                        webdew.com  
                                                        <i class="flaticon2-correct kt-font-primary"></i>                      
                                                        </a>&nbsp;
                                                        <span><span class="kt-badge kt-badge--success kt-badge--dot"></span>&nbsp;<span class="kt-font-bold kt-font-success">Do Follow</span></span>
                                                    </div>
                                                    <div class="whishlist-ribbon kt-ribbon kt-ribbon--clip kt-ribbon--right kt-ribbon--danger">
                                                        <div class="kt-ribbon__target" style="top: 12px;">
                                                            <span class="kt-ribbon__inner"></span><i class="la la-heart-o" style="
                                                                font-size: 1.5rem;
                                                                "></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="kt-widget__subhead">
                                                    webdew: Best HubSpot Partners Agency, with 100+ Certifications on wall
                                                </div>
                                                <div class="kt-widget__info">
                                                    <div class="kt-widget__desc">
                                                        Hey! we are webdew, Inbound Marketing and HubSpot Partner Agency based out in India,
                                                        <br> where 50+ Agency members are Breathing HubSpot in and out.
                                                    </div>
                                                    <div class="kt-widget__action d-flex align-items-center">
                                                        <div class="website-price">
                                                            $ 1234
                                                        </div>
                                                        &nbsp; &nbsp;
                                                        <button type="button" class="btn btn-brand btn-upper">Order Now</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="kt-widget__bottom">
                                            <div class="kt-widget__item">
                                                <div class="kt-widget__icon">
                                                    <img src="{{ asset('') . config('app.public_url') . '/assets/media/icons/Moz.svg'}}" style="width:35px;">
                                                </div>
                                                <div class="kt-widget__details">
                                                    <span class="kt-widget__title">Domain Authority</span>
                                                    <div class="d-flex"><span class="kt-widget__value">29</span>
                                                        <span class="d-flex align-items-end">/100</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="kt-widget__item">
                                                <div class="kt-widget__icon">
                                                    <i class="flaticon-earth-globe"></i>
                                                </div>
                                                <div class="kt-widget__details">
                                                    <span class="kt-widget__title">Global Rank</span>
                                                    <span class="kt-widget__value">3,185,623</span>
                                                </div>
                                            </div>
                                            <div class="kt-widget__item">
                                                <div class="kt-widget__icon">
                                                    <i class="flaticon-line-graph"></i>
                                                </div>
                                                <div class="kt-widget__details">
                                                    <span class="kt-widget__title">Traffic</span>
                                                    <span class="kt-widget__value">2.08K</span>
                                                </div>
                                            </div>
                                            <!-- <div class="kt-widget__item">
                                                <div class="kt-widget__icon">
                                                <i class="flaticon-squares-3"></i>
                                                </div>
                                                <div class="kt-widget__details">
                                                <span class="kt-widget__title">Category</span>
                                                <span class="kt-widget__value">2.08K</span>
                                                </div>
                                                </div> -->
                                            <div class="kt-widget__item">
                                                <div class="kt-widget__icon">
                                                    <i class="flaticon-layers"></i>
                                                </div>
                                                <div class="kt-widget__details">
                                                    <span class="kt-widget__title">Orders in Queue</span>
                                                    <span class="kt-widget__value">2.08K</span>
                                                </div>
                                            </div>
                                            <div class="kt-widget__item">
                                                <div class="kt-widget__icon">
                                                    <i class="flaticon-chat-1"></i>
                                                </div>
                                                <div class="kt-widget__details">
                                                    <a href="#" class="kt-widget__value kt-font-brand">Chat <br> Now</a>
                                                </div>
                                            </div>
                                            <!-- <div class="kt-widget__item">
                                                <div class="kt-portlet__head-toolbar">
                                                <a href="#" class="btn btn-label-primary btn-bold dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                View More
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right" style="">
                                                <ul class="kt-nav">
                                                <li class="kt-nav__item">
                                                <a href="#" class="kt-nav__link">
                                                <i class="kt-nav__link-icon flaticon2-line-chart"></i>
                                                <span class="kt-nav__link-text">Reports</span>
                                                </a>
                                                </li>
                                                <li class="kt-nav__item">
                                                <a href="#" class="kt-nav__link">
                                                <i class="kt-nav__link-icon flaticon2-send"></i>
                                                <span class="kt-nav__link-text">Messages</span>
                                                </a>
                                                </li>
                                                <li class="kt-nav__item">
                                                <a href="#" class="kt-nav__link">
                                                <i class="kt-nav__link-icon flaticon2-pie-chart-1"></i>
                                                <span class="kt-nav__link-text">Charts</span>
                                                </a>
                                                </li>
                                                <li class="kt-nav__item">
                                                <a href="#" class="kt-nav__link">
                                                <i class="kt-nav__link-icon flaticon2-avatar"></i>
                                                <span class="kt-nav__link-text">Members</span>
                                                </a>
                                                </li>
                                                <li class="kt-nav__item">
                                                <a href="#" class="kt-nav__link">
                                                <i class="kt-nav__link-icon flaticon2-settings"></i>
                                                <span class="kt-nav__link-text">Settings</span>
                                                </a>
                                                </li>
                                                </ul> </div>
                                                </div>
                                                </div> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end:: Portlet-->
                            <div class="kt-portlet">
                                <div class="kt-portlet__body">
                                    <div class="kt-widget kt-widget--user-profile-3">
                                        <div class="kt-widget__top">
                                            <div class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-font-light kt-hidden">
                                                JM
                                            </div>
                                            <div class="kt-widget__content">
                                                <div class="kt-widget__head">
                                                    <div class="d-flex align-items-center">
                                                        <div class="kt-widget__media kt-hidden-">
                                                            <img src="https://www.webdew.com/hubfs/favicon-3.ico" alt="image">
                                                        </div>
                                                        <a href="#" class="kt-widget__username">
                                                        webdew.com  
                                                        <i class="flaticon2-correct kt-font-primary"></i>                      
                                                        </a>&nbsp;
                                                        <span><span class="kt-badge kt-badge--warning kt-badge--dot"></span>&nbsp;<span class="kt-font-bold kt-font-warning">No Follow</span></span>
                                                    </div>
                                                    <div class="whishlist-ribbon kt-ribbon kt-ribbon--clip kt-ribbon--right kt-ribbon--danger">
                                                        <div class="kt-ribbon__target" style="top: 12px;">
                                                            <span class="kt-ribbon__inner"></span><i class="la la-heart-o" style="
                                                                font-size: 1.5rem;
                                                                "></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="kt-widget__subhead">
                                                    webdew: Best HubSpot Partners Agency, with 100+ Certifications on wall
                                                </div>
                                                <div class="kt-widget__info">
                                                    <div class="kt-widget__desc">
                                                        Hey! we are webdew, Inbound Marketing and HubSpot Partner Agency based out in India,
                                                        <br> where 50+ Agency members are Breathing HubSpot in and out.
                                                    </div>
                                                    <div class="kt-widget__action d-flex align-items-center">
                                                        <div class="website-price">
                                                            $ 1234
                                                        </div>
                                                        &nbsp; &nbsp;
                                                        <button type="button" class="btn btn-brand btn-upper">Order Now</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="kt-widget__bottom">
                                            <div class="kt-widget__item">
                                                <div class="kt-widget__icon">
                                                    <img src="{{ asset('') . config('app.public_url') . '/assets/media/icons/Moz.svg'}}" style="width:35px;">
                                                </div>
                                                <div class="kt-widget__details">
                                                    <span class="kt-widget__title">Domain Authority</span>
                                                    <div class="d-flex"><span class="kt-widget__value">29</span>
                                                        <span class="d-flex align-items-end">/100</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="kt-widget__item">
                                                <div class="kt-widget__icon">
                                                    <i class="flaticon-earth-globe"></i>
                                                </div>
                                                <div class="kt-widget__details">
                                                    <span class="kt-widget__title">Global Rank</span>
                                                    <span class="kt-widget__value">3,185,623</span>
                                                </div>
                                            </div>
                                            <div class="kt-widget__item">
                                                <div class="kt-widget__icon">
                                                    <i class="flaticon-line-graph"></i>
                                                </div>
                                                <div class="kt-widget__details">
                                                    <span class="kt-widget__title">Traffic</span>
                                                    <span class="kt-widget__value">2.08K</span>
                                                </div>
                                            </div>
                                            <!-- <div class="kt-widget__item">
                                                <div class="kt-widget__icon">
                                                <i class="flaticon-squares-3"></i>
                                                </div>
                                                <div class="kt-widget__details">
                                                <span class="kt-widget__title">Category</span>
                                                <span class="kt-widget__value">2.08K</span>
                                                </div>
                                                </div> -->
                                            <div class="kt-widget__item">
                                                <div class="kt-widget__icon">
                                                    <i class="flaticon-layers"></i>
                                                </div>
                                                <div class="kt-widget__details">
                                                    <span class="kt-widget__title">Orders in Queue</span>
                                                    <span class="kt-widget__value">2.08K</span>
                                                </div>
                                            </div>
                                            <div class="kt-widget__item">
                                                <div class="kt-widget__icon">
                                                    <i class="flaticon-chat-1"></i>
                                                </div>
                                                <div class="kt-widget__details">
                                                    <a href="#" class="kt-widget__value kt-font-brand">Chat <br> Now</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--begin:: Portlet-->
                            <div class="kt-portlet">
                                <div class="kt-portlet__body">
                                    <div class="kt-widget kt-widget--user-profile-3">
                                        <div class="kt-widget__top">
                                            <div class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-font-light kt-hidden">
                                                JM
                                            </div>
                                            <div class="kt-widget__content">
                                                <div class="kt-widget__head">
                                                    <div class="d-flex align-items-center">
                                                        <div class="kt-widget__media kt-hidden-">
                                                            <img src="https://www.webdew.com/hubfs/favicon-3.ico" alt="image">
                                                        </div>
                                                        <a href="#" class="kt-widget__username">
                                                        webdew.com  
                                                        <i class="flaticon2-correct kt-font-primary"></i>                      
                                                        </a>&nbsp;
                                                        <span><span class="kt-badge kt-badge--success kt-badge--dot"></span>&nbsp;<span class="kt-font-bold kt-font-success">Do Follow</span></span>
                                                    </div>
                                                    <div class="whishlist-ribbon kt-ribbon kt-ribbon--clip kt-ribbon--right kt-ribbon--danger">
                                                        <div class="kt-ribbon__target" style="top: 12px;">
                                                            <span class="kt-ribbon__inner"></span><i class="la la-heart-o" style="
                                                                font-size: 1.5rem;
                                                                "></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="kt-widget__subhead">
                                                    webdew: Best HubSpot Partners Agency, with 100+ Certifications on wall
                                                </div>
                                                <div class="kt-widget__info">
                                                    <div class="kt-widget__desc">
                                                        Hey! we are webdew, Inbound Marketing and HubSpot Partner Agency based out in India,
                                                        <br> where 50+ Agency members are Breathing HubSpot in and out.
                                                    </div>
                                                    <div class="kt-widget__action d-flex align-items-center">
                                                        <div class="website-price">
                                                            $ 1234
                                                        </div>
                                                        &nbsp; &nbsp;
                                                        <button type="button" class="btn btn-brand btn-upper">Order Now</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="kt-widget__bottom">
                                            <div class="kt-widget__item">
                                                <div class="kt-widget__icon">
                                                    <img src="{{ asset('') . config('app.public_url') . '/assets/media/icons/Moz.svg'}}" style="width:35px;">
                                                </div>
                                                <div class="kt-widget__details">
                                                    <span class="kt-widget__title">Domain Authority</span>
                                                    <div class="d-flex"><span class="kt-widget__value">29</span>
                                                        <span class="d-flex align-items-end">/100</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="kt-widget__item">
                                                <div class="kt-widget__icon">
                                                    <i class="flaticon-earth-globe"></i>
                                                </div>
                                                <div class="kt-widget__details">
                                                    <span class="kt-widget__title">Global Rank</span>
                                                    <span class="kt-widget__value">3,185,623</span>
                                                </div>
                                            </div>
                                            <div class="kt-widget__item">
                                                <div class="kt-widget__icon">
                                                    <i class="flaticon-line-graph"></i>
                                                </div>
                                                <div class="kt-widget__details">
                                                    <span class="kt-widget__title">Traffic</span>
                                                    <span class="kt-widget__value">2.08K</span>
                                                </div>
                                            </div>
                                            <!-- <div class="kt-widget__item">
                                                <div class="kt-widget__icon">
                                                <i class="flaticon-squares-3"></i>
                                                </div>
                                                <div class="kt-widget__details">
                                                <span class="kt-widget__title">Category</span>
                                                <span class="kt-widget__value">2.08K</span>
                                                </div>
                                                </div> -->
                                            <div class="kt-widget__item">
                                                <div class="kt-widget__icon">
                                                    <i class="flaticon-layers"></i>
                                                </div>
                                                <div class="kt-widget__details">
                                                    <span class="kt-widget__title">Orders in Queue</span>
                                                    <span class="kt-widget__value">2.08K</span>
                                                </div>
                                            </div>
                                            <div class="kt-widget__item">
                                                <div class="kt-widget__icon">
                                                    <i class="flaticon-chat-1"></i>
                                                </div>
                                                <div class="kt-widget__details">
                                                    <a href="#" class="kt-widget__value kt-font-brand">Chat <br> Now</a>
                                                </div>
                                            </div>
                                            <!-- <div class="kt-widget__item">
                                                <div class="kt-portlet__head-toolbar">
                                                <a href="#" class="btn btn-label-primary btn-bold dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                View More
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right" style="">
                                                <ul class="kt-nav">
                                                <li class="kt-nav__item">
                                                <a href="#" class="kt-nav__link">
                                                <i class="kt-nav__link-icon flaticon2-line-chart"></i>
                                                <span class="kt-nav__link-text">Reports</span>
                                                </a>
                                                </li>
                                                <li class="kt-nav__item">
                                                <a href="#" class="kt-nav__link">
                                                <i class="kt-nav__link-icon flaticon2-send"></i>
                                                <span class="kt-nav__link-text">Messages</span>
                                                </a>
                                                </li>
                                                <li class="kt-nav__item">
                                                <a href="#" class="kt-nav__link">
                                                <i class="kt-nav__link-icon flaticon2-pie-chart-1"></i>
                                                <span class="kt-nav__link-text">Charts</span>
                                                </a>
                                                </li>
                                                <li class="kt-nav__item">
                                                <a href="#" class="kt-nav__link">
                                                <i class="kt-nav__link-icon flaticon2-avatar"></i>
                                                <span class="kt-nav__link-text">Members</span>
                                                </a>
                                                </li>
                                                <li class="kt-nav__item">
                                                <a href="#" class="kt-nav__link">
                                                <i class="kt-nav__link-icon flaticon2-settings"></i>
                                                <span class="kt-nav__link-text">Settings</span>
                                                </a>
                                                </li>
                                                </ul> </div>
                                                </div>
                                                </div> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end:: Portlet-->
                        </div>
                    </div>
                </div>
                <!-- publish post html end here -->
                <!-- body content end -->
            </div>
        </div>
    </div>
</div>

@section('mainscripts')
@parent
    <!--begin::Page Vendors Styles(used by this page) -->
        <script href="{{ asset('') . config('app.public_url') . '/assets/js/owl.carousel.js'  }}" type="text/javascript"></script>
  
         <script>

            $(document).ready(function() {
              $('.owl-carousel').owlCarousel({
                loop: true,
                responsiveClass: true,
                nav: true,               
                navText : ["<i class='la la-angle-left'></i>","<i class='la la-angle-right'></i>"],
                responsive: {
                  0: {
                    items: 1,
                    dots: false,
                    autoplay: true
                  },
                  600: {
                    items: 2,
                    dots: false,
                    autoplay: true
                  },
                
                  900: {
                    items: 3,
                    dots: false
                  },
                  1200: {
                    items: 4,
                    dots: false
                  },
                  1399: {
                    items: 5,
                    dots: false
                  }
                }
              })
            })          
        </script>
<!--end::Page Scripts -->
@show
@endsection
