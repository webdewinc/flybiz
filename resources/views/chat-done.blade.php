<!DOCTYPE html>
<html lang="en">
  <head>
    <link href='//fonts.googleapis.com/css?family=Oswald:400,300|Source+Sans+Pro:400,300' rel='stylesheet' type='text/css'>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>Twilio Chat</title>
    <!-- CSS -->
    <link rel="stylesheet"
          href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet"
          href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('public/css/twiliochat.css') }}">
  </head>
  <body>
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-4 col-md-offset-4">
          <div class="row"><div id="logo-column" class="col-md-2 col-md-offset-5">
            <img id="logo-image" src="{{ asset('img/twilio-logo.png') }}"/>
          </div></div>
          <div id="status-row" class="row disconnected">
            <div class="col-md-5 left-align">
              <span id="delete-channel-span"><b>Delete current channel</b></span>
            </div>
            <div class="col-md-7 right-align">
              <span id="status-span">Connected as <b><span id="username-span"></span></b></span>
              <span id="leave-span"><b>x Leave</b></span>
            </div>
          </div>
        </div>
      </div>
      <div id="container" class="row">
        <div id="channel-panel" class="col-md-offset-2 col-md-2">
          <div id="channel-list" class="row not-showing"></div>
          <div id="new-channel-input-row" class="row not-showing">
            <textarea placeholder="Type channel name" id="new-channel-input" rows="1" maxlength="20" class="channel-element"></textarea>
          </div>
          <div class="row">
            <img id="add-channel-image" src="{{ asset('img/add-channel-image.png') }}"/>
          </div>
        </div>
        <div id="chat-window" class="col-md-4 with-shadow">
          <div id="message-list" class="row disconnected"></div>
          <div id="typing-row" class="row disconnected">
            <p id="typing-placeholder"></p>
          </div>
          <div id="input-div" class="row">
            <div id="channel-list" class="row not-showing" style="display:none;"></div>
                <div class="row">
                    <div class="col-lg-12">                              
                        <div class="all-messages" id="message-list">
                            
                        <?php $numItems = count($getmsg); $i=0;?>    
                                        <!-- message list here -->
                            @foreach($getmsg as $msg)
                                <?php $date = date_create($msg['created_at']);
                                        $date=date_format($date,"d/m/Y h:iA");
                                        $date1=explode(' ', $date);
                                        $todaydate=date("d/m/Y");
                                        if($todaydate == $date1[0])
                                        {
                                            $actualdate='Today';
                                        }
                                        else
                                        {
                                            $actualdate=$date1[0];
                                        }
                                        
                                ?>
                                    @if($msg['sender_id'] == Auth::user()->user_id)
                                        <div class="kt-chat__message kt-chat__message--right">
                                            <div class="kt-chat__user">
                                                <span data-content="date" class="kt-chat__datetime">{{$actualdate}} - {{$date1[1]}}</span>

                                                <?php 
                                                    $name = \App\User::where('user_id',$msg['receiver_id'])->first();
                                                    $name = json_decode(json_encode($name),true);

                                                ?>

                                                <a href="javascript:void(0);" class="kt-chat__username"  data-content="username">You</span></a>
                                                <span class="kt-userpic kt-userpic--circle kt-userpic--sm">
                                                    <img src="{{url('public/assets/media/users/default.jpg')}}" alt="image">
                                                </span>
                                            </div>
                                            
                                            <div class="kt-chat__text kt-bg-light-success" data-content="body">
                                                {{$msg['reply']}}
                                            </div>
                                        </div>
                                    @else  
                                        
                                        <div class="kt-chat__message">
                                            <div class="kt-chat__user">
                                                <span class="kt-userpic kt-userpic--circle kt-userpic--sm">
                                                    <!-- <img src="{{asset('frontend/userImages/'.Auth::user()->image)}}" alt="image"> -->
                                                    <img src="{{url('public/assets/media/users/default.jpg')}}" alt="image">
                                                </span>
                                                <?php 
                                                    $name = \App\User::where('user_id',$msg['sender_id'])->first();
                                                    $name = json_decode(json_encode($name),true);

                                                ?>

                                                <a href="javascript:void(0);" data-content="username" class="kt-chat__username">{{$name['name']}}</span></a>
                                                <span data-content="date" class="kt-chat__datetime">{{$actualdate}} - {{$date1[1]}}</span>
                                            </div>               
                                            <div class="kt-chat__text kt-bg-light-success" data-content="body">
                                                {{$msg['reply']}}
                                            </div>                                    
                                        </div>

                                    @endif    
                                @endforeach      

                        </div>
                    </div>
                </div>  
            <textarea id="input-text" disabled="true" placeholder="   Your message"></textarea>
          </div>
          <div id="connect-panel" class="disconnected row with-shadow">
            <?php 
                // Available alpha caracters
                // $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' . Auth::user()->user_fname;

                // // generate a pin based on 2 * 7 digits + a random character
                // $pin = mt_rand(1000000, 9999999)
                //     . mt_rand(1000000, 9999999)
                //     . $characters[rand(0, strlen($characters) - 1)];

                // // shuffle the result
                // $string = str_shuffle($pin);
            ?>
            <div class="row">
              <div class="col-md-12">
                <label for="username-input">Username: </label>
                <input id="username-input" value="{{Auth::user()->user_fname}}" type="text" placeholder="username"/>
                <input id="username-input-get" value="{{Auth::user()->name}}" type="hidden" placeholder="username"/>
                <input id="chatChannelId" name="chatChannelId" value="{{$channel_id}}" type="hidden">
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <img id="connect-image" src="{{ asset('img/connect-image.png') }}"/>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- HTML Templates -->
    <script type="text/html" id="message-template">
      <div class="row no-margin">
        <div class="row no-margin message-info-row" style="">
          <div class="col-md-8 left-align"><p data-content="username" class="message-username"></p></div>
          <div class="col-md-4 right-align"><span data-content="date" class="message-date"></span></div>
        </div>
        <div class="row no-margin message-content-row">
          <div style="" class="col-md-12"><p data-content="body" class="message-body"></p></div>
        </div>
      </div>
    </script>
    <script type="text/html" id="channel-template">
      <div class="col-md-12">
        <p class="channel-element" data-content="channelName"></p>
      </div>
    </script>
    <script type="text/html" id="member-notification-template">
      <p class="member-status" data-content="status"></p>
    </script>


     <!-- HTML Templates -->
    <script type="text/html" id="message-template">
        
            <div class="kt-chat__user">
                <span class="kt-userpic kt-userpic--circle kt-userpic--sm">
                    <!-- <img src="{{asset('frontend/userImages/'.Auth::user()->image)}}" alt="image"> -->
                    <img src="{{url('public/assets/media/users/default.jpg')}}" alt="image">
                </span>
                <a href="javascript:void(0);" data-content="username" class="kt-chat__username"></span></a>
                <span data-content="date" class="kt-chat__datetime"></span>
            </div>                                                                                                
            <div class="kt-chat__text kt-bg-light-success" data-content="body">
                
            </div>

        <!-- <div class="kt-chat__message kt-chat__message--right">
            <div class="kt-chat__user">
                <span class="kt-chat__datetime">10 Seconds</span>
                <a href="#" class="kt-chat__username">You</span></a>
                <span class="kt-userpic kt-userpic--circle kt-userpic--sm">
                    <img src="../assets/media/users/default.jpg" alt="image">
                </span>
            </div>
            <div class="kt-chat__text kt-bg-light-brand">
                Offer Proposed: Full installation support. $21
            </div><br>
            <div class="kt-chat__text kt-bg-light-brand">
                Offer Accepted: Full installation support.
            </div><br>
            <div class="kt-chat__text kt-bg-light-brand">
                The Customer has accepted your Answer.
            </div>
        </div> -->
    </script>
    <!-- HTML Templates -->
    <script type="text/html" id="message-template-right">
        
        <div class="kt-chat__user">
            <span class="kt-userpic kt-userpic--circle kt-userpic--sm">
                <!-- <img src="{{asset('frontend/userImages/'.Auth::user()->image)}}" alt="image"> -->
                <img src="{{url('public/assets/media/users/default.jpg')}}" alt="image">
            </span>
            
            <span data-content="date" class="kt-chat__datetime"></span>
            <a href="javascript:void(0);" class="kt-chat__username"  data-content="username"></span></a>
            <span class="kt-userpic kt-userpic--circle kt-userpic--sm">
                <img src="{{url('public/assets/media/users/default.jpg')}}" alt="image">
            </span>
        </div>
        
        <div class="kt-chat__text kt-bg-light-success" data-content="body">
            
        </div>
    </script>
    <!-- JavaScript -->
    <script src="//code.jquery.com/jquery-2.2.0.min.js"></script>
    <script src="{{ asset('public/js/vendor/jquery-throttle.min.js') }}"></script>
    <script src="{{ asset('public/js/vendor/jquery.loadTemplate-1.4.4.min.js') }}"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.1/moment.min.js"></script>
    <!-- Twilio Common helpers and Twilio Chat JavaScript libs from CDN. -->
    <script src="//media.twiliocdn.com/sdk/js/common/v0.1/twilio-common.min.js"></script>
    <script src="//media.twiliocdn.com/sdk/js/chat/v3.0/twilio-chat.min.js"></script>
    <script type="text/javascript">
// var twiliochat = (function() {
// var tc = {};

// var GENERAL_CHANNEL_UNIQUE_NAME = "{{$room_name}}";

//   var GENERAL_CHANNEL_NAME = "{{$room_name_view}}";

//   var MESSAGES_HISTORY_LIMIT = 50;

//   var $channelList;
//   var $inputText;
//   var $usernameInput;
//   var $statusRow;
//   var $connectPanel;
//   var $newChannelInputRow;
//   var $newChannelInput;
//   var $typingRow;
//   var $typingPlaceholder;

//   $(document).ready(function() {
//     tc.$messageList = $('#message-list');
//     $channelList = $('#channel-list');
//     $inputText = $('#input-text');
//     $usernameInput = $('#username-input');
//     $statusRow = $('#status-row');
//     $connectPanel = $('#connect-panel');
//     $newChannelInputRow = $('#new-channel-input-row');
//     $newChannelInput = $('#new-channel-input');
//     $typingRow = $('#typing-row');
//     $typingPlaceholder = $('#typing-placeholder');
//     $usernameInput.focus();
//     $usernameInput.on('keypress', handleUsernameInputKeypress);
//     $inputText.on('keypress', handleInputTextKeypress);
//     $newChannelInput.on('keypress', tc.handleNewChannelInputKeypress);
//     $('#connect-image').on('click', connectClientWithUsername);
//     $('#add-channel-image').on('click', showAddChannelInput);
//     $('#leave-span').on('click', disconnectClient);
//     $('#delete-channel-span').on('click', deleteCurrentChannel);
//   });

//   function handleUsernameInputKeypress(event) {
//     if (event.keyCode === 13){
//       connectClientWithUsername();
//     }     
//   }
//   connectClientWithUsername();

//   function handleInputTextKeypress(event) {
//     if (event.keyCode === 13) {
//       tc.currentChannel.sendMessage($(this).val());
//       event.preventDefault();
//       $(this).val('');
//     }
//     else {
//       notifyTyping();
//     }
//   }

//   var notifyTyping = $.throttle(function() {
//     tc.currentChannel.typing();
//   }, 1000);

//   tc.handleNewChannelInputKeypress = function(event) {
//     if (event.keyCode === 13) {
//       tc.messagingClient.createChannel({
//         friendlyName: $newChannelInput.val()
//       }).then(hideAddChannelInput);
//       $(this).val('');
//       event.preventDefault();
//     }
//   };

//   function connectClientWithUsername() {
//     //var usernameText = $usernameInput.val();
//     var usernameText = $('#username-input').val();
//     $('#username-input').val('');
//     if (usernameText == '') {
//       alert('Username cannot be empty');
//       return;
//     }
//     tc.username = usernameText;
//     fetchAccessToken(tc.username, connectMessagingClient);
//     //disconnectClient();
//   }

//   function fetchAccessToken(username, handler) {
    
//       $.post("{{url('/token')}}", {identity: username, device: 'browser'}, null, 'json')
//       .done(function(response) {
//           handler(response.token);

//       })
//       .fail(function(error) {
//           conshandlerole.log('Failed to fetch the Access Token with error: ' + error);
//       });
//   }

//   function connectMessagingClient(token) {
//     // Initialize the Chat messaging client
//     tc.accessManager = new Twilio.AccessManager(token);
//     Twilio.Chat.Client.create(token).then(function(client) {
//       tc.messagingClient = client;
//       updateConnectedUI();
//       tc.loadChannelList(tc.joinGeneralChannel);
//       tc.messagingClient.on('channelAdded', $.throttle(tc.loadChannelList));
//       tc.messagingClient.on('channelRemoved', $.throttle(tc.loadChannelList));
//       tc.messagingClient.on('tokenExpired', refreshToken);
//     });
//   }

//   function refreshToken() {
//     fetchAccessToken(tc.username, setNewToken);
//   }

//   function setNewToken(tokenResponse) {
//     tc.accessManager.updateToken(tokenResponse.token);
//   }

//   function updateConnectedUI() {
//     $('#username-span').text(tc.username);
//     $statusRow.addClass('connected').removeClass('disconnected');
//     tc.$messageList.addClass('connected').removeClass('disconnected');
//     $connectPanel.addClass('connected').removeClass('disconnected');
//     $inputText.addClass('with-shadow');
//     $typingRow.addClass('connected').removeClass('disconnected');
//   }

//   tc.loadChannelList = function(handler) {
//     if (tc.messagingClient === undefined) {
//       console.log('Client is not initialized');
//       return;
//     }

//     tc.messagingClient.getPublicChannelDescriptors().then(function(channels) {
//       tc.channelArray = tc.sortChannelsByName(channels.items);
//       $channelList.text('');
//       tc.channelArray.forEach(addChannel);
//       if (typeof handler === 'function') {
//         handler();
//       }
//     });
//   };

//   tc.joinGeneralChannel = function() {
//     console.log('Attempting to join "general" chat channel...');
//     if (!tc.generalChannel) {

//       // If it doesn't exist, let's create it
//       tc.messagingClient.createChannel({
//         uniqueName: GENERAL_CHANNEL_UNIQUE_NAME,
//         friendlyName: GENERAL_CHANNEL_NAME
//       }).then(function(channel) {
//         console.log('Created general channel');
//         tc.generalChannel = channel;
//         tc.loadChannelList(tc.joinGeneralChannel);
//       });
//     }
//     else {
//       console.log('Found general channel:');
//       setupChannel(tc.generalChannel);
//     }
//   };

//   function initChannel(channel) {
//     console.log('Initialized channel ' + channel.friendlyName);

//     //disconnectClient();
//     return tc.messagingClient.getChannelBySid(channel.sid);
//   }

//   function joinChannel(_channel) {
//     return _channel.join()
//       .then(function(joinedChannel) {
//         console.log('Joined channel ' + joinedChannel.friendlyName);
//         updateChannelUI(_channel);
//         tc.currentChannel = _channel;
//         tc.loadMessages();
//         return joinedChannel;
//       });
//   }

//   function initChannelEvents() {
//     console.log(tc.currentChannel.friendlyName + ' ready.');
//     tc.currentChannel.on('messageAdded', tc.addMessageToList);
//     tc.currentChannel.on('typingStarted', showTypingStarted);
//     tc.currentChannel.on('typingEnded', hideTypingStarted);
//     tc.currentChannel.on('memberJoined', notifyMemberJoined);
//     tc.currentChannel.on('memberLeft', notifyMemberLeft);
//     $inputText.prop('disabled', false).focus();
//   }

//   function setupChannel(channel) {
//     return leaveCurrentChannel()
//       .then(function() {
//         return initChannel(channel);
//       })
//       .then(function(_channel) {
//         return joinChannel(_channel);
//       })
//       .then(initChannelEvents);
//   }

//   tc.loadMessages = function() {
//     tc.currentChannel.getMessages(MESSAGES_HISTORY_LIMIT).then(function (messages) {
//       messages.items.forEach(tc.addMessageToList);
//     });
//   };

//   function leaveCurrentChannel() {
//     if (tc.currentChannel) {
//       return tc.currentChannel.leave().then(function(leftChannel) {
//         console.log('left ' + leftChannel.friendlyName);
//         leftChannel.removeListener('messageAdded', tc.addMessageToList);
//         leftChannel.removeListener('typingStarted', showTypingStarted);
//         leftChannel.removeListener('typingEnded', hideTypingStarted);
//         leftChannel.removeListener('memberJoined', notifyMemberJoined);
//         leftChannel.removeListener('memberLeft', notifyMemberLeft);
//       });
//     } else {
//       return Promise.resolve();
//     }
//   }

//   tc.addMessageToList = function(message) {
//     var rowDiv = $('<div>').addClass('row no-margin');
//     rowDiv.loadTemplate($('#message-template'), {
//       username: message.author,
//       date: dateFormatter.getTodayDate(message.timestamp),
//       body: message.body
//     });
//     if (message.author === tc.username) {
//       rowDiv.addClass('own-message');
//     }

//     tc.$messageList.append(rowDiv);
//     scrollToMessageListBottom();
//   };

//   function notifyMemberJoined(member) {
//     notify(member.identity + ' joined the channel')
//   }

//   function notifyMemberLeft(member) {
//     notify(member.identity + ' left the channel');
//   }

//   function notify(message) {
//     var row = $('<div>').addClass('col-md-12');
//     row.loadTemplate('#member-notification-template', {
//       status: message
//     });
//     tc.$messageList.append(row);
//     scrollToMessageListBottom();
//   }

//   function showTypingStarted(member) {
//     $typingPlaceholder.text(member.identity + ' is typing...');
//   }

//   function hideTypingStarted(member) {
//     $typingPlaceholder.text('');
//   }

//   function scrollToMessageListBottom() {
//     tc.$messageList.scrollTop(tc.$messageList[0].scrollHeight);
//   }

//   function updateChannelUI(selectedChannel) {
//     console.log('update');
//     var channelElements = $('.channel-element').toArray();
//     var channelElement = channelElements.filter(function(element) {
//       return $(element).data().sid === selectedChannel.sid;
//     });
//     channelElement = $(channelElement);
//     if (tc.currentChannelContainer === undefined && selectedChannel.uniqueName === GENERAL_CHANNEL_UNIQUE_NAME) {
//       tc.currentChannelContainer = channelElement;
//     }
//     tc.currentChannelContainer.removeClass('selected-channel').addClass('unselected-channel');
//     channelElement.removeClass('unselected-channel').addClass('selected-channel');
//     tc.currentChannelContainer = channelElement;
//   }

//   function showAddChannelInput() {
//     if (tc.messagingClient) {
//       $newChannelInputRow.addClass('showing').removeClass('not-showing');
//       $channelList.addClass('showing').removeClass('not-showing');
//       $newChannelInput.focus();
//     }
//   }

//   function hideAddChannelInput() {
//     $newChannelInputRow.addClass('not-showing').removeClass('showing');
//     $channelList.addClass('not-showing').removeClass('showing');
//     $newChannelInput.val('');
//   }

//   function addChannel(channel) {
//     if (channel.uniqueName === GENERAL_CHANNEL_UNIQUE_NAME) {
//       tc.generalChannel = channel;
//     }
//     var rowDiv = $('<div>').addClass('row channel-row');
//     rowDiv.loadTemplate('#channel-template', {
//       channelName: channel.friendlyName
//     });

//     var channelP = rowDiv.children().children().first();

//     rowDiv.on('click', selectChannel);
//     channelP.data('sid', channel.sid);
//     if (tc.currentChannel && channel.sid === tc.currentChannel.sid) {
//       tc.currentChannelContainer = channelP;
//       channelP.addClass('selected-channel');
//     }
//     else {
//       channelP.addClass('unselected-channel')
//     }

//     $channelList.append(rowDiv);
//   }

//   function deleteCurrentChannel() {
//     if (!tc.currentChannel) {
//       return;
//     }
//     if (tc.currentChannel.sid === tc.generalChannel.sid) {
//       alert('You cannot delete the general channel');
//       return;
//     }
//     tc.currentChannel.delete().then(function(channel) {
//       console.log('channel: '+ channel.friendlyName + ' deleted');
//       setupChannel(tc.generalChannel);
//     });
//   }

//   function selectChannel(event) {
//     var target = $(event.target);
//     var channelSid = target.data().sid;
//     var selectedChannel = tc.channelArray.filter(function(channel) {
//       return channel.sid === channelSid;
//     })[0];
//     if (selectedChannel === tc.currentChannel) {
//       return;
//     }
//     setupChannel(selectedChannel);
//   };

//   function disconnectClient() {
//     leaveCurrentChannel();
//     $channelList.text('');
//     tc.$messageList.text('');
//     channels = undefined;
//     $statusRow.addClass('disconnected').removeClass('connected');
//     tc.$messageList.addClass('disconnected').removeClass('connected');
//     $connectPanel.addClass('disconnected').removeClass('connected');
//     $inputText.removeClass('with-shadow');
//     $typingRow.addClass('disconnected').removeClass('connected');
//   }

//   tc.sortChannelsByName = function(channels) {
//     return channels.sort(function(a, b) {
//       if (a.friendlyName === GENERAL_CHANNEL_NAME) {
//         return -1;
//       }
//       if (b.friendlyName === GENERAL_CHANNEL_NAME) {
//         return 1;
//       }
//       return a.friendlyName.localeCompare(b.friendlyName);
//     });
//   };

//   return tc;
// })();

                                var twiliochat = (function() {
                                var tc = {};

                                var GENERAL_CHANNEL_UNIQUE_NAME = "{{$room_name}}";

                                var GENERAL_CHANNEL_NAME = "{{$room_name_view}}";
                                var MESSAGES_HISTORY_LIMIT = 50;

                                var $channelList;
                                var $inputText;
                                var $usernameInput;
                                var $statusRow;
                                var $connectPanel;
                                var $newChannelInputRow;
                                var $newChannelInput;
                                var $typingRow;
                                var $typingPlaceholder;

                                $(document).ready(function() {
                                    tc.$messageList = $('#message-list');
                                    $channelList = $('#channel-list');
                                    $inputText = $('#input-text');
                                    $usernameInput = $('#username-input');
                                    $statusRow = $('#status-row');
                                    $connectPanel = $('#connect-panel');
                                    $newChannelInputRow = $('#new-channel-input-row');
                                    $newChannelInput = $('#new-channel-input');
                                    $typingRow = $('#typing-row');
                                    $typingPlaceholder = $('#typing-placeholder');
                                    $usernameInput.focus();
                                    $usernameInput.on('keypress', handleUsernameInputKeypress);
                                    
                                    $inputText.on('keypress', handleInputTextKeypress);
                                    $newChannelInput.on('keypress', tc.handleNewChannelInputKeypress);
                                    $('#connect-image').on('click', connectClientWithUsername);
                                    $('#add-channel-image').on('click', showAddChannelInput);
                                    $('#leave-span').on('click', disconnectClient);
                                    $('#delete-channel-span').on('click', deleteCurrentChannel);
                                });

                                $( window ).on( "load", function() {
                                   
                                    connectClientWithUsername();
                                    //addChannel();

                                });

                                function getUsername(author) {
                                    var str = author;
                                    var res = str.split("_");
                                    return res[0];
                                }

                                function handleUsernameInputKeypress(event) {
                                    if (event.keyCode === 13){
                                    connectClientWithUsername();
                                    }
                                }

                                function handleInputTextKeypress(event) {
                                    var sender = '{{Auth::user()->user_id}}';    
                                    var receiver = '{{$userdata["user_id"]}}';
                                    var channelid = tc.currentChannel.sid;
                                    var msg = $('#input-text').val();
                                    if (event.keyCode === 13) {
                                    tc.currentChannel.sendMessage($(this).val());
                                    event.preventDefault();
                                    $(this).val('');
                                    var chatChannelId = $('#chatChannelId').val();
                                    var roomname='{{$room_name}}';
                                    var date= dateFormatter.getTodayDate();
                                    $.post("{{url('/storemsg')}}", {chatChannelId:chatChannelId,sender:sender,receiver:receiver,channelid:channelid,msg:msg,roomname:roomname,date:date}, null, 'json')
                                        .done(function(response) {            

                                        })
                                        .fail(function(error) {
                                            alert('error saving data');
                                            console.log('Failed to fetch the Access Token with error: ' + error);
                                        });
                                    }
                                    else {
                                    notifyTyping();
                                    }
                                }

                                var notifyTyping = $.throttle(function() {
                                    tc.currentChannel.typing();
                                }, 1000);

                                tc.handleNewChannelInputKeypress = function(event) {
                                    if (event.keyCode === 13) {
                                    tc.messagingClient.createChannel({
                                        friendlyName: $newChannelInput.val()
                                    }).then(hideAddChannelInput);
                                    $(this).val('');
                                    event.preventDefault();
                                    }
                                };

                                function connectClientWithUsername() {
                                    var usernameText = $usernameInput.val();
                                    $usernameInput.val('');
                                    if (usernameText == '') {
                                        alert('Username cannot be empty');
                                        return;
                                    }
                                    tc.username = usernameText;
                                    fetchAccessToken(tc.username, connectMessagingClient);
                                }

                                function fetchAccessToken(username, handler) {
                                    
                                    $.post("{{url('/token')}}", {identity: username, device: 'browser'}, null, 'json')
                                    .done(function(response) {
                                        handler(response.token);
                                    })
                                    .fail(function(error) {
                                        console.log('Failed to fetch the Access Token with error: ' + error);
                                    });
                                }

                                function connectMessagingClient(token) {
                                    // Initialize the Chat messaging client
                                    
                                    tc.accessManager = new Twilio.AccessManager(token);
                                    Twilio.Chat.Client.create(token).then(function(client) {
                                    tc.messagingClient = client;
                                    updateConnectedUI();
                                    
                                    tc.loadChannelList(tc.joinGeneralChannel);
                                    tc.messagingClient.on('channelAdded', $.throttle(tc.loadChannelList));
                                    tc.messagingClient.on('channelRemoved', $.throttle(tc.loadChannelList));
                                    tc.messagingClient.on('tokenExpired', refreshToken);
                                    });
                                }

                                function refreshToken() {
                                    fetchAccessToken(tc.username, setNewToken);
                                }

                                function setNewToken(tokenResponse) {
                                    tc.accessManager.updateToken(tokenResponse.token);
                                }

                                function updateConnectedUI() {
                                    $('#username-span').text(tc.username);
                                    $statusRow.addClass('connected').removeClass('disconnected');
                                    tc.$messageList.addClass('connected').removeClass('disconnected');
                                    $connectPanel.addClass('connected').removeClass('disconnected');
                                    $inputText.addClass('with-shadow');
                                    $typingRow.addClass('connected').removeClass('disconnected');
                                }

                                tc.loadChannelList = function(handler) {
                                    if (tc.messagingClient === undefined) {
                                    console.log('Client is not initialized');
                                    return;
                                    }

                                    tc.messagingClient.getPublicChannelDescriptors().then(function(channels) {
                                    tc.channelArray = tc.sortChannelsByName(channels.items);
                                    $channelList.text('');
                                    tc.channelArray.forEach(addChannel);
                                    if (typeof handler === 'function') {
                                        handler();
                                    }
                                    });
                                };

                                tc.joinGeneralChannel = function() {
                                    console.log('Attempting to join "general" chat channel...');
                                    if (!tc.generalChannel) {
                                        //If it doesn't exist, let's create it
                                        tc.messagingClient.createChannel({
                                            uniqueName: GENERAL_CHANNEL_UNIQUE_NAME,
                                            friendlyName: GENERAL_CHANNEL_NAME
                                        }).then(function(channel) {

                                            console.log('Created general channel');
                                            tc.generalChannel = channel;
                                            tc.loadChannelList(tc.joinGeneralChannel);
                                        });

                                        
                                    }
                                    else {
                                    console.log('Found general channel:');
                                    setupChannel(tc.generalChannel);
                                    }
                                };

                                function initChannel(channel) {
                                    console.log('Initialized channel ' + channel.friendlyName);
                                    return tc.messagingClient.getChannelBySid(channel.sid);
                                }

                                function joinChannel(_channel) {
                                    
                                        return _channel.join()
                                        .then(function(joinedChannel) {
                                            console.log('Joined channel ' + joinedChannel.friendlyName);
                                            updateChannelUI(_channel);
                                            tc.currentChannel = _channel;
                                            //tc.loadMessages();
                                            return joinedChannel;
                                        });
                                    
                                }

                                function initChannelEvents() {
                                    console.log(tc.currentChannel.friendlyName + ' ready.');
                                    tc.currentChannel.on('messageAdded', tc.addMessageToList);
                                    tc.currentChannel.on('typingStarted', showTypingStarted);
                                    tc.currentChannel.on('typingEnded', hideTypingStarted);
                                    tc.currentChannel.on('memberJoined', notifyMemberJoined);
                                    tc.currentChannel.on('memberLeft', notifyMemberLeft);
                                    $inputText.prop('disabled', false).focus();
                                }

                                function setupChannel(channel) {
                                    return leaveCurrentChannel()
                                    .then(function() {
                                        
                                        return initChannel(channel);
                                    })
                                    .then(function(_channel) {

                                        return joinChannel(_channel);
                                    })
                                    .then(initChannelEvents);
                                }

                                tc.loadMessages = function() {
                                    tc.currentChannel.getMessages(MESSAGES_HISTORY_LIMIT).then(function (messages) {
                                    messages.items.forEach(tc.addMessageToList);
                                    });
                                };

                                function leaveCurrentChannel() {

                                    if (tc.currentChannel) {
                                        
                                    return tc.currentChannel.leave().then(function(leftChannel) {
                                        console.log('left ' + leftChannel.friendlyName);
                                        leftChannel.removeListener('messageAdded', tc.addMessageToList);
                                        leftChannel.removeListener('typingStarted', showTypingStarted);
                                        leftChannel.removeListener('typingEnded', hideTypingStarted);
                                        leftChannel.removeListener('memberJoined', notifyMemberJoined);
                                        leftChannel.removeListener('memberLeft', notifyMemberLeft);
                                    });
                                    } else {
                                        
                                        return Promise.resolve();
                                    }
                                }

                                tc.addMessageToList = function(message) {
                                    
                                    var username = getUsername(message.author);
                                    
                                    if (message.author === tc.username) {

                                        var rowDiv = $('<div>').addClass('kt-chat__message kt-chat__message--right');
                                        rowDiv.loadTemplate($('#message-template-right'), {
                                        username: 'You',
                                        date: dateFormatter.getTodayDate(message.timestamp),
                                        body: message.body
                                        });
                                        rowDiv.addClass('own-message');

                                        
                                    } else {

                                        var rowDiv = $('<div>').addClass('kt-chat__message');
                                        rowDiv.loadTemplate($('#message-template'), {
                                        username: username,
                                        date: dateFormatter.getTodayDate(message.timestamp),
                                        body: message.body
                                        });
                                        rowDiv.addClass('own-message');
                                        
                                    }

                                    tc.$messageList.append(rowDiv);
                                    scrollToMessageListBottom();
                                };

                                function notifyMemberJoined(member) {
                                    notify(member.identity + ' joined the channel')
                                }

                                function notifyMemberLeft(member) {
                                    notify(member.identity + ' left the channel');
                                }

                                function notify(message) {
                                    var row = $('<div>').addClass('col-md-12');
                                    row.loadTemplate('#member-notification-template', {
                                    status: message
                                    });
                                    tc.$messageList.append(row);
                                    scrollToMessageListBottom();
                                }

                                function showTypingStarted(member) {
                                    $typingPlaceholder.text(member.identity + ' is typing...');
                                }

                                function hideTypingStarted(member) {
                                    $typingPlaceholder.text('');
                                }

                                function scrollToMessageListBottom() {
                                    tc.$messageList.scrollTop(tc.$messageList[0].scrollHeight);
                                }

                                function updateChannelUI(selectedChannel) {
                                    var channelElements = $('.channel-element').toArray();
                                    var channelElement = channelElements.filter(function(element) {
                                    return $(element).data().sid === selectedChannel.sid;
                                    });
                                    channelElement = $(channelElement);
                                    if (tc.currentChannelContainer === undefined && selectedChannel.uniqueName === GENERAL_CHANNEL_UNIQUE_NAME) {
                                    tc.currentChannelContainer = channelElement;
                                    }
                                    tc.currentChannelContainer.removeClass('selected-channel').addClass('unselected-channel');
                                    channelElement.removeClass('unselected-channel').addClass('selected-channel');
                                    tc.currentChannelContainer = channelElement;
                                }

                                function showAddChannelInput() {
                                    if (tc.messagingClient) {
                                    $newChannelInputRow.addClass('showing').removeClass('not-showing');
                                    $channelList.addClass('showing').removeClass('not-showing');
                                    $newChannelInput.focus();
                                    }
                                }

                                function hideAddChannelInput() {
                                    $newChannelInputRow.addClass('not-showing').removeClass('showing');
                                    $channelList.addClass('not-showing').removeClass('showing');
                                    $newChannelInput.val('');
                                }

                                function addChannel(channel) {
                                   
                                    if (channel.uniqueName === GENERAL_CHANNEL_UNIQUE_NAME) {
                                        tc.generalChannel = channel;
                                    }
                                   
                                    var rowDiv = $('<div>').addClass('row channel-row');
                                    rowDiv.loadTemplate('#channel-template', {
                                    channelName: channel.friendlyName
                                    });

                                    var channelP = rowDiv.children().children().first();
                                    
                                    rowDiv.on('click', selectChannel);
                                    channelP.data('sid', channel.sid);
                                    if (tc.currentChannel && channel.sid === tc.currentChannel.sid) {
                                    tc.currentChannelContainer = channelP;
                                    channelP.addClass('selected-channel');

                                    }
                                    else {
                                    channelP.addClass('unselected-channel')
                                    }

                                    $channelList.append(rowDiv);
                                }

                                function deleteCurrentChannel() {
                                    if (!tc.currentChannel) {
                                    return;
                                    }
                                    if (tc.currentChannel.sid === tc.generalChannel.sid) {
                                    alert('You cannot delete the general channel');
                                    return;
                                    }
                                    tc.currentChannel.delete().then(function(channel) {
                                    console.log('channel: '+ channel.friendlyName + ' deleted');
                                    setupChannel(tc.generalChannel);
                                    });
                                }

                                function selectChannel(event) {
                                    var target = $(event.target);
                                    var channelSid = target.data().sid;
                                    var selectedChannel = tc.channelArray.filter(function(channel) {
                                    return channel.sid === channelSid;
                                    })[0];
                                    if (selectedChannel === tc.currentChannel) {
                                    return;
                                    }
                                    setupChannel(selectedChannel);
                                };

                                function disconnectClient() {
                                    leaveCurrentChannel();
                                    $channelList.text('');
                                    tc.$messageList.text('');
                                    channels = undefined;
                                    $statusRow.addClass('disconnected').removeClass('connected');
                                    tc.$messageList.addClass('disconnected').removeClass('connected');
                                    $connectPanel.addClass('disconnected').removeClass('connected');
                                    $inputText.removeClass('with-shadow');
                                    $typingRow.addClass('disconnected').removeClass('connected');
                                }

                                tc.sortChannelsByName = function(channels) {
                                    return channels.sort(function(a, b) {
                                    if (a.friendlyName === GENERAL_CHANNEL_NAME) {
                                        return -1;
                                    }
                                    if (b.friendlyName === GENERAL_CHANNEL_NAME) {
                                        return 1;
                                    }
                                    return a.friendlyName.localeCompare(b.friendlyName);
                                    });
                                };

                                return tc;
                                })();
    </script>
    <script src="{{ asset('public/js/dateformatter.js') }}"></script>
  </body>
</html>
