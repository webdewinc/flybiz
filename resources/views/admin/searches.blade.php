@extends('layout.app')
@section('title', 'Search Page - GuestPostEngine')
@section('title-description')

<meta name="description" content="In a search section, an admin can see how many times a particular keyword or domain name have searched on GuestPostEngine marketplace.">
    
@endsection
@section('content')
@section('mainhead')
@parent
	@section('customCSS')
	<!--begin::Page Vendors Styles(used by this page) -->
	<link href="{{ asset('') . config('app.public_url') . '/assets/css/pages/wizard/wizard-2.css' }}" rel="stylesheet" type="text/css" />
	<!--end::Page Vendors Styles -->
	@endsection
@show

					
					<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
						<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

							<!-- begin:: Subheader -->
							<div class="kt-subheader   kt-grid__item" id="kt_subheader">
								<div class="kt-container ">
									
								</div>
							</div>

							<!-- end:: Subheader -->

							<!-- begin:: Content -->
							<div class="kt-container  kt-grid__item kt-grid__item--fluid">
								<div class="alert alert-light alert-elevate" role="alert">
									<div class="kt-portlet__body w-100">

										<!--begin: Search Form -->
										<div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
											<div class="row align-items-center">
												<div class="col-xl-12 order-2 order-xl-1">
													<div class="row align-items-center">
														<div class="col-md-3 kt-margin-b-20-tablet-and-mobile">
															<div class="kt-form__label">
																<label>Search:</label>
															</div>
															<div class="kt-input-icon kt-input-icon--left">
																<input type="text" class="form-control" placeholder="Search..." id="generalSearch">
																<span class="kt-input-icon__icon kt-input-icon__icon--left">
																	<span><i class="la la-search"></i></span>
																</span>
															</div>
														</div>
														<!-- <div class="col-md-3 kt-margin-b-20-tablet-and-mobile">
															
															
																<div class="kt-form__label">
																	<label>Status:</label>
																</div>
																<div class="kt-form__control">
																	<select class="form-control " id="kt_form_status">
																		<option value="">All</option>
																		<option value="1">Pending</option>
																		<option value="4">Approved</option>
																	</select>
																</div>
															
															
														</div> -->
														<div class="col-md-3 kt-margin-b-20-tablet-and-mobile">
															
																<div class="kt-form__label">
																	<label>Start Date:</label>
																</div>
																<div class="kt-form__control">
																	<input type="date" name="start_date" id="start_date" class="form-control" placeholder="Select Date" />
																</div>		
															
														</div>
														<div class="col-md-3 kt-margin-b-20-tablet-and-mobile">
															
																<div class="kt-form__label">
																	<label>End Date:</label>
																</div>
																<div class="kt-form__control">
																	<input type="date" name="end_date" id="end_date" class="form-control" placeholder="End Date" />
																</div>		
															
														</div>
														<div class="col-md-3 kt-margin-b-20-tablet-and-mobile">
															

																<div class="kt-portlet__head-toolbar">
																	<div class="kt-portlet__head-wrapper">
																		
																			<button type="button" class="btn btn-label-brand btn-bold"  id="filter">
																				<i class="flaticon2-search"></i> Filter
																			</button>
																	</div>
																</div>
															
														</div>
														
													</div>
												</div>
												<!-- <div class="col-xl-4 order-1 order-xl-2 kt-align-right">
													<a href="#" class="btn btn-default kt-hidden">
														<i class="la la-cart-plus"></i> New Order
													</a>
													<div class="kt-separator kt-separator--border-dashed kt-separator--space-lg d-xl-none"></div>
												</div> -->
											</div>
										</div>
										

										<!--end: Search Form -->
									</div>
									
								</div>
								
								<div class="kt-portlet kt-portlet--mobile">
									<div class="kt-portlet__head kt-portlet__head--lg">
										<div class="kt-portlet__head-label">
											<span class="kt-portlet__head-icon">
												<i class="kt-font-brand la la-laptop"></i>
											</span>
											<h3 class="kt-portlet__head-title">
												Searches
											</h3>
										</div>
										<div class="kt-portlet__head-toolbar">
											<div class="kt-portlet__head-wrapper">
												
												<!-- <div class="dropdown dropdown-inline">
													<button type="button" class="btn btn-label-brand btn-bold" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
														<i class="flaticon2-plus"></i> Add Websites
													</button>
												</div> -->
											</div>
										</div>
									</div>
									
									<div class="kt-portlet__body kt-portlet__body--fit">

										<!--begin: Datatable -->
										<div class="kt-datatable" id="local_data"></div>

										<!--end: Datatable -->
									</div>
								</div>
							</div>

							<!-- end:: Content -->
						</div>
					</div>


				<!-- </div>
			</div>
		</div> -->

		<!-- begin::Scrolltop -->
		<div id="kt_scrolltop" class="kt-scrolltop">
			<i class="fa fa-arrow-up"></i>
		</div>

		<!-- end::Scrolltop -->
		@section('mainscripts')
    	@parent
    	@section('customScript')
	<script>
	'use strict';
	// Class definition
	var KTDatatableDataLocalDemo = function() {
		// Private functions
		var data = '<?php echo json_encode( $searchResult ); ?>';
        var split_data = JSON.parse(data);
        var query_id   = split_data[0]['query_id'];
		// demo initializer
		var demo = function() {
			// var dataJSONArray = JSON.parse('[{"RecordID":1,"Websiteurl":"www.website.com","Price":"$1","Orders":"1","Status":1,"Type":1,"Actions":null},\n' +
			// 	'{"RecordID":2,"Websiteurl":"www.website.com","Price":"$2","Orders":"2","Status":2,"Type":2,"Actions":null},\n' +
			// 	'{"RecordID":3,"Websiteurl":"www.website.com","Price":"$3","Orders":"3","Status":1,"Type":1,"Actions":null},\n' +
			// 	'{"RecordID":4,"Websiteurl":"www.website.com","Price":"$4","Orders":"4","Status":2,"Type":2,"Actions":null},\n' +
			// 	'{"RecordID":1,"Websiteurl":"www.website.com","Price":"$1","Orders":"1","Status":1,"Type":1,"Actions":null},\n' +
			// 	'{"RecordID":2,"Websiteurl":"www.website.com","Price":"$2","Orders":"2","Status":2,"Type":2,"Actions":null},\n' +
			// 	'{"RecordID":3,"Websiteurl":"www.website.com","Price":"$3","Orders":"3","Status":1,"Type":1,"Actions":null},\n' +
			// 	'{"RecordID":2,"Websiteurl":"www.website.com","Price":"$2","Orders":"2","Status":2,"Type":2,"Actions":null},\n' +
			// 	'{"RecordID":3,"Websiteurl":"www.website.com","Price":"$3","Orders":"3","Status":1,"Type":1,"Actions":null},\n' +
			// 	'{"RecordID":2,"Websiteurl":"www.website.com","Price":"$2","Orders":"2","Status":2,"Type":2,"Actions":null},\n' +
			// 	'{"RecordID":3,"Websiteurl":"www.website.com","Price":"$3","Orders":"3","Status":1,"Type":1,"Actions":null},\n' +
			// 	'{"RecordID":4,"Websiteurl":"www.website.com","Price":"$4","Orders":"4","Status":2,"Type":2,"Actions":null}]');

			var dataJSONArray = JSON.parse(data);
			var datatable = $('.kt-datatable').KTDatatable({
				// datasource definition
				data: {
					type: 'local',
					source: dataJSONArray,
					pageSize: 10,
				},

				// datasource definition
				  // data: {
				  //   type: 'remote',
				  //   method:'GET',
				  //   source: {
				  //     read: {
				  //       url: "{{url('/')}}"+'/searches-ajax',
				  //       map: function(raw) {

				  //         // sample data mapping
				  //         var dataSet = raw;
				  //         if (typeof raw.data !== 'undefined') {
				  //           dataSet = raw.data;
				  //         }
				  //         return dataSet;
				  //       },
				  //     },
				  //   },
				  //   pageSize: 10,
				  //   serverPaging: true,
				  //   serverFiltering: true,
				  //   serverSorting: true
				  // },
	
				// layout definition
				layout: {
					scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
					// height: 450, // datatable's body's fixed height
					footer: false, // display/hide footer
				},
	
				// column sorting
				sortable: true,
	
				pagination: true,
	
				search: {
					input: $('#generalSearch'),
				},
				// columns definition
				columns: [
					{
						field: 'query_id',
						title: '#',
						sortable: false,
						width: 20,
						type: 'number',
						selector: {class: 'kt-checkbox--solid'},
						textAlign: 'center',
					},
					 {
						field: 'query_string',
						title: 'Query String',
					}, {
						field: 'query_index',
						title: 'Number Of Search',				
					}],
			});
	
			// $('#kt_form_status').on('change', function() {
			// 	datatable.search($(this).val().toLowerCase(), 'Status');
			// });

			$(document).on('click','#filter', function(e) {

				var start_date = $('#start_date').val();
				var end_date = $('#end_date').val();
				var searchVal  = $('#generalSearch').val();
				
				if(start_date != '' || end_date != ''){
					$.ajax({
	                     headers: {
	                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	                      },
	                      url  : "{{ url('/admin/searches') }}",
	                      type : 'POST',
	                      data: {
	                      		'start_date': start_date,
	                            'end_date'  : end_date,
	                            'searchVal' : searchVal,
	                        },
	                      async: true,
	                      dataType: 'json',
	                      enctype: 'multipart/form-data',
	                      cache: false,                      
	                      success: function(response){
	                      	alert('Response '+response);
	                      },
	                     error:function(){}
	                });
					e.stopImmediatePropagation();
	            	return false; 	
				}else{
					alert('s');
				}
				

				//datatable.search($(this).val().toLowerCase(), 'Status');
			});

			
			$('#kt_form_type').on('change', function() {
				datatable.search($(this).val().toLowerCase(), 'end_date');
			});
			$('#kt_form_status,#kt_form_type').selectpicker();
	
		};
	
		return {
			// Public functions
			init: function() {
				// init dmeo
				demo();
			},
		};
	}();
	
	jQuery(document).ready(function() {
		KTDatatableDataLocalDemo.init();
	});


		// $(document).on('change','#start_date',function(e){
		// 	var start_date = $(this).val();
		// 	var end_date   = $('#end_date').val();
		// 	var searchVal  = $('#generalSearch').val();
		// 	$.ajax({
  //                    headers: {
  //                           'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  //                     },
  //                     url  : "{{ url('/admin/searches') }}",
  //                     type : 'POST',
  //                     data: {
  //                           'start_date': start_date,
  //                           'end_date'  : end_date,
  //                           'searchVal' : searchVal,
  //                       },
  //                     async: true,
  //                     dataType: 'json',
  //                     enctype: 'multipart/form-data',
  //                     cache: false,                      
  //                     success: function(response){
  //                     	alert('Response '+response);
  //                     },
  //                    error:function(){}
  //               });
		// 	e.stopImmediatePropagation();
  //           return false; 
		// });
		// $(document).on('change','#end_date',function(e){
			
		// });
	</script>
	@endsection
	@show
@endsection