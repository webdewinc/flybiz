@extends('layout.app')
@section('title', 'See all received order as a Admin - GuestPostEngine')
@section('title-description')

<meta name="description" content="As a Admin, you can visit my orders page on GuestPostEngine marketplace to see how many orders of websites have you received.">
    
@endsection
@section('content')
@section('mainhead')
@parent
    @section('customCSS')
    <!--begin::Page Vendors Styles(used by this page) -->
    <link href="{{ asset('') . config('app.public_url') . '/assets/css/pages/wizard/wizard-2.css'  }}" rel="stylesheet" type="text/css" />
        <!--end::Page Vendors Styles -->
    @endsection
@show
        <div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
            <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
                <!-- begin:: Subheader -->
                <div class="kt-subheader   kt-grid__item" id="kt_subheader">
                    <div class="kt-container ">
                        
                    </div>
                </div>
                <!-- end:: Subheader -->

                <!-- begin:: Content -->
                <div class="kt-container  kt-grid__item kt-grid__item--fluid">
                    
                    <div class="kt-portlet kt-portlet--mobile">
                        <div class="kt-portlet__head kt-portlet__head--lg">
                            <div class="kt-portlet__head-label">
                                <span class="kt-portlet__head-icon">
                                    <i class="kt-font-brand la la-laptop"></i>
                                </span>
                                <h3 class="kt-portlet__head-title">
                                    Wishlist Items
                                </h3>
                            </div>
                            <div class="kt-portlet__head-toolbar">
                                <div class="kt-portlet__head-wrapper">
                                    
                                    <!-- <div class="dropdown dropdown-inline">
                                        <button type="button" class="btn btn-label-brand btn-bold" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="flaticon2-plus"></i> Add Websites
                                        </button>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                        
                        <div class="kt-portlet__body kt-portlet__body--fit">
                          
                            <!--begin: Datatable -->
                            <div class="kt-datatable" id="local_data"></div>
                            <!--end: Datatable -->
                        </div>
                    </div>
                   @if(!empty($wishlistMessage))
                        <h2 class="text-center">{{ $wishlistMessage }}</h2>
                   @endif  
                </div>
            <!-- end:: Content -->
            </div>
        </div>

        <!-- begin::Scrolltop -->
        <div id="kt_scrolltop" class="kt-scrolltop">
            <i class="fa fa-arrow-up"></i>
        </div>
       <!-- end::Scrolltop -->
        @section('mainscripts')
        @parent
        @section('customScript')

        @if(!empty($wishlistArr))
        <script>
        'use strict';
        // Class definition
        var KTDatatableDataLocalDemo = function() {
            // Private functions
            var data        = '<?php echo json_encode( $wishlistArr ); ?>';
            var split_data  = JSON.parse(data);
            var id          = split_data[0]['wishlist_id'];
            // demo initializer
            var demo = function() {

                var dataJSONArray = JSON.parse(data);
                var datatable = $('.kt-datatable').KTDatatable({
                    // datasource definition
                    data: {
                        type: 'local',
                        source: dataJSONArray,
                        pageSize: 10,
                    },
        
                    // layout definition
                    layout: {
                        scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
                        // height: 450, // datatable's body's fixed height
                        footer: false, // display/hide footer
                    },
        
                    // column sorting
                    sortable: true,
        
                    pagination: true,
        
                    search: {
                        input: $('#generalSearch'),
                    },
                    // columns definition
                    columns: [
                        {
                            field: 'id',
                            title: '#',
                            sortable: false,
                            width: 20,
                            type: 'number',
                            selector: {class: 'kt-checkbox--solid'},
                            textAlign: 'center',
                        },
                        {
                            field: 'domain_url',
                            title: 'Website',              
                        },
                        {
                            field: 'advertiser',
                            title: 'Advertiser',
                        },
                        {
                            field: 'publisher',
                            title: 'Publisher'
                        },
                        {

                            field: 'advertiser_email',
                            title: 'Advertiser Email'
                        }, 
                        {
                            field: 'cost_price',
                            title: 'Cost Price',
                        },
                        {
                            field: 'extra_cost',
                            title: 'Extra Cost',
                        },
                        {
                            field: 'total_amount',
                            title: 'FLYBIZ Amount',
                        }
                        ],
                });
        
                // $('#kt_form_status').on('change', function() {
                //  datatable.search($(this).val().toLowerCase(), 'Status');
                // }) 
                
                $('#kt_form_type').on('change', function() {
                    datatable.search($(this).val().toLowerCase(), 'end_date');
                });
                $('#kt_form_status,#kt_form_type').selectpicker();
            };
            return {
                // Public functions
                init: function() {
                    // init dmeo
                    demo();
                },
            };
        }();
        
        jQuery(document).ready(function() {
            KTDatatableDataLocalDemo.init();
        });
        </script>
        @endif

        @endsection
    @show
@endsection