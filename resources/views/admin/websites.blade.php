@extends('layout.app')
@section('title', 'See website added as a Admin - GuestPostEngine')
@section('title-description')

<meta name="description" content="Under my websites section at GuestPostEngine, you can see the catalogue of all the websites added by you at anytime with their relevant details.">
    
@endsection
@section('content')
@section('mainhead')
@parent
	<!--begin::Page Vendors Styles(used by this page) -->
	 <link href="{{ asset('') . config('app.public_url') . '/assets/css/pages/wizard/wizard-2.css' }}" rel="stylesheet" type="text/css" />
    <!--end::Page Vendors Styles -->
	@show
	<!-- end:: Aside -->
	<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
		<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

			<!-- begin:: Subheader -->
			<div class="kt-subheader   kt-grid__item" id="kt_subheader">
				<div class="kt-container ">
				</div>
			</div>

			<!-- begin:: Content -->
			<div class="kt-container  kt-grid__item kt-grid__item--fluid">
				<div class="alert alert-light alert-elevate" role="alert">
					<div class="kt-portlet__body w-100">

						<!--begin: Search Form -->
						<div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
							<div class="row align-items-center">
								<div class="col-xl-8 order-2 order-xl-1">
									<div class="row align-items-center">
										<div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
											<div class="kt-form__label">
												<label>Search:</label>
											</div>
											<div class="kt-input-icon kt-input-icon--left">
												<input type="text" class="form-control" placeholder="Search..." id="generalSearch">
												<span class="kt-input-icon__icon kt-input-icon__icon--left">
													<span><i class="la la-search"></i></span>
												</span>
											</div>
										</div>
										<div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
											
												<div class="kt-form__label">
													<label>OwnerShip:</label>
												</div>
												<div class="kt-form__control">
													<select class="form-control bootstrap-select" id="kt_form_status">
														<option value="">All</option>
														<option value="-1">Pending</option>
														<option value="0">Unverified</option>
														<option value="1">Verified</option>
													</select>
												</div>
											
										</div>
										<div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
											
												<div class="kt-form__label">
													<label>Status:</label>
												</div>
												<div class="kt-form__control">
													<select class="form-control bootstrap-select" id="kt_form_status_1">
														<option value="">All</option>
														<option value="3">Not Accept</option>
														<option value="1">Accepted</option>
														<option value="2">Rejected</option>
													</select>
												</div>
											
										</div>
										
									</div>
								</div>
								<div class="col-xl-4 order-1 order-xl-2 kt-align-right">
									<a href="#" class="btn btn-default kt-hidden">
										<i class="la la-cart-plus"></i> New Order
									</a>
									<div class="kt-separator kt-separator--border-dashed kt-separator--space-lg d-xl-none"></div>
								</div>
							</div>
						</div>

						<!--end: Search Form -->
					</div>
					
				</div>
				<div class="kt-portlet kt-portlet--mobile">
					<div class="kt-portlet__head kt-portlet__head--lg">
						<div class="kt-portlet__head-label">
							<span class="kt-portlet__head-icon">
								<i class="kt-font-brand la la-laptop"></i>
							</span>
							<h3 class="kt-portlet__head-title">
								Websites
							</h3>
						</div>
					</div>
					<input type="hidden"  id="APP_URL" value="{{ url('/admin/edit_website') }}" />
                    <input type="hidden"  id="APP_URL2" value="{{ url('/admin/delete_website') }}" />
					<div class="kt-portlet__body kt-portlet__body--fit">
						<!--begin: Datatable -->
						<div class="kt-datatable" id="local_data"></div>
						<!--end: Datatable -->
					</div>


				</div>
			</div>

			<!-- end:: Content -->


		</div>
	</div>
		<!-- end:: Page -->

	

	<!--begin::Modal-->
            <div class="modal fade" id="kt_modal_da" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header text-center kt-pb-0">
                            
                            <h5 class="modal-title" id="exampleModalLabel">Update DA</h5>
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon" data-dismiss="modal" aria-label="Close" style="cursor:pointer;">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#000000">
                                        <rect x="0" y="7" width="16" height="2" rx="1"></rect>
                                        <rect opacity="0.3" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000) " x="0" y="7" width="16" height="2" rx="1"></rect>
                                    </g>
                                </g>
                            </svg>
                            </button>
                        </div>
                        <div class="modal-body">
                            
                            <div class="kt-portlet__body">
                                <div class="tab-content kt-login kt-login--v6 kt-login--signin" id="kt_login">
                                    <div class="tab-pane active" id="kt_tabs_1_1" role="tabpanel">
                                        <span id="alert-show"></span>
                                        <!-- <form class="kt-form" method="POST" action="{{ route('login-ajax') }}"> -->
                                        <form id="kt_da_pa" name="kt_da_pa" method="POST" action="{{url('admin/daPaSave')}}">
                                            @csrf
                                            <input type="hidden" name="domain_id" id="domain_id" value="0">	                        
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="data_da" id="data_da" value="" required autocomplete="off" autofocus placeholder="DA">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="input-group">                          
                                                    <input type="text" class="form-control" name="data_pa" id="data_pa" value="" required autocomplete="off" placeholder="PA">
                                                </div>
                                            </div>
                                            <div class="modal-footer kt-padding-0 kt-padding-t-15">
                                                <button type="submit" class="btn btn-primary" id="kt_da_submit">
                                                    {{ __('Submit') }}
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Modal--> 


	<!-- begin::Scrolltop -->
	<div id="kt_scrolltop" class="kt-scrolltop">
		<i class="fa fa-arrow-up"></i>
	</div>

	<!-- end::Scrolltop -->

	@section('mainscripts')
    @parent
    @section('customScript')
	<script>
		$(document).on('click','.da_class',function(){
			var domain_id = $(this).attr('rel');
			var data_da = $(this).attr('data-da');
			var data_pa = $(this).attr('data-pa');
			$('#domain_id').val(domain_id);
			if(data_da == 0) {
				$('#data_da').val('');
				$('#data_pa').val('');
			} else {
				$('#data_da').val(data_da);
				$('#data_pa').val(data_pa);
			}
			$('#kt_modal_da').modal('show');
		});


		$('#kt_da_submit').click(function(e) {
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');           

            form.validate({
                rules: {
                    data_da: {
                        required: true,
                        min:20,
                        max:100,
                        digits:true
                    },
                    data_pa: {
                        required: true,
                        min:20,
                        max:100,
                        digits:true
                    }
                },
                onkeyup: true,
                onfocusout: true,
                onclick: true
            });

            if (!form.valid()) {
                return;
            }

            btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', true);

            //form.submit();

            $.ajax({
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                url  : "{{url('/admin/daPaSave')}}",
                type: 'POST',
                data: {data_da : $('#data_da').val(),data_pa : $('#data_pa').val(),domain_id : $('#domain_id').val()},
                async: true,
                dataType: 'json',
                enctype: 'multipart/form-data',
                cache: false,
                success: function(data) {
                    if(data.alert == 'success') {

                    	var alert = 'alert-outline-' + data.alert;
	                    var message = data.message;
	                    var alert = '<div class="alert ' + alert + ' fade show" role="alert"><div class="alert-icon"><i class="flaticon-warning"></i></div><div class="alert-text message">' + message + '</div><div class="alert-close"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="la la-close"></i></span></button></div></div>';
	                    $('#alert-show').html(alert);

	                    setTimeout(function(){ 
	                    	window.location.href = data.url;
	                    }, 100);

                        
                    } else {
                        btn.addClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
                    }
                },
                error: function(xhr,textStatus,thrownError) {
                    alert(xhr + "\n" + textStatus + "\n" + thrownError);
                }
            });
            
        });
	'use strict';
	// Class definition
	var KTDatatableDataLocalDemo = function() {

		var data = '<?php echo json_encode($domain_lists); ?>';
		var split_data = JSON.parse(data);
        var domain_id = split_data[0]['domain_id'];
		// Private functions
		// demo initializer
		var demo = function() {
			// var dataJSONArray = JSON.parse('[{"RecordID":1,"Websiteurl":"www.website.com","Price":"$1","Orders":"1","Status":1,"Type":1,"Actions":null},\n' +
			// 	'{"RecordID":2,"Websiteurl":"www.website.com","Price":"$2","Orders":"2","Status":2,"Type":2,"Actions":null},\n' +
			// 	'{"RecordID":3,"Websiteurl":"www.website.com","Price":"$3","Orders":"3","Status":1,"Type":1,"Actions":null},\n' +
			// 	'{"RecordID":4,"Websiteurl":"www.website.com","Price":"$4","Orders":"4","Status":2,"Type":2,"Actions":null},\n' +
			// 	'{"RecordID":1,"Websiteurl":"www.website.com","Price":"$1","Orders":"1","Status":1,"Type":1,"Actions":null},\n' +
			// 	'{"RecordID":2,"Websiteurl":"www.website.com","Price":"$2","Orders":"2","Status":2,"Type":2,"Actions":null},\n' +
			// 	'{"RecordID":3,"Websiteurl":"www.website.com","Price":"$3","Orders":"3","Status":1,"Type":1,"Actions":null},\n' +
			// 	'{"RecordID":2,"Websiteurl":"www.website.com","Price":"$2","Orders":"2","Status":2,"Type":2,"Actions":null},\n' +
			// 	'{"RecordID":3,"Websiteurl":"www.website.com","Price":"$3","Orders":"3","Status":1,"Type":1,"Actions":null},\n' +
			// 	'{"RecordID":2,"Websiteurl":"www.website.com","Price":"$2","Orders":"2","Status":2,"Type":2,"Actions":null},\n' +
			// 	'{"RecordID":3,"Websiteurl":"www.website.com","Price":"$3","Orders":"3","Status":1,"Type":1,"Actions":null},\n' +
			// 	'{"RecordID":4,"Websiteurl":"www.website.com","Price":"$4","Orders":"4","Status":2,"Type":2,"Actions":null}]');
			//console.log('Data '+data);
			var dataJSONArray  = JSON.parse(data);
			var datatable = $('.kt-datatable').KTDatatable({
				// datasource definition
				data: {
					type: 'local',
					source: dataJSONArray,
					pageSize: 10,
				},
	
				// layout definition
				layout: {
					scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
					// height: 450, // datatable's body's fixed height
					footer: false, // display/hide footer
				},
	
				// column sorting
				sortable: true,
	
				pagination: true,
	
				search: {
					input: $('#generalSearch'),
				},
	
				// columns definition
				columns: [
	                        {
	                            field: 'domain_id',
	                            title: '#',
	                            sortable: false,
	                            width: 20,
	                            type: 'number',
	                            selector: {class: 'kt-checkbox--solid'},
	                            textAlign: 'center',
	                        },
	                         {
	                         	width: 400,
	                            field: 'domain_url',
	                            title: 'Website',
	                        }, 
	                        {
	                            field: 'cost_price',
	                            title: 'Cost Price',             
	                        },
	                        {
	                            field: 'extra_cost',
	                            title: 'Extra Cost',             
	                        }, 
	                        {
	                            field: 'selling_price',
	                            title: 'Webdew Price',             
	                        },
	                        {
	                            field: 'domain_approval_status',
	                            title: 'Approval status',
	                            //callback function support for column rendering
	                            template: function(row) {
	                            	
	                                var status = {
	                                    1: {'title': 'Accepted', 'class': 'kt-badge--brand'},
	                                    2: {'title': 'Rejected', 'class': 'kt-badge--danger'},
	                                    3: {'title': 'Not Accept', 'class': 'kt-badge--warning'}
	                                
	                                };	                               
	                                return '<span class="kt-badge ' + status[row.domain_approval_status].class + ' kt-badge--inline kt-badge--pill">' + status[row.domain_approval_status].title + '</span>';
	                                
	                            },
	                        },
	                        {
	                            field: 'orders',
	                            title: 'Orders',                
	                        },
	                        {
	                        	field: 'dp_da',
	                            title: 'Domain Authority',	

	                        },
	                        {
	                        	field: 'dp_pa',
	                            title: 'Page Authority',	
	                        },
	                        {
	                        	field: 'number_words',
	                            title: 'Number of Words',	
	                        },
	                        {
	                        	field: 'user_code',
	                        	title: 'Publisher',
	                        },
	                        {
	                            field: 'created_at',
	                            title: 'Created Date',                
	                        },
	                        {
	                            field: 'domain_status',
	                            title: 'Domain Status',
	                            //callback function support for column rendering
	                            template: function(row) {
	                                var status = {
	                                	'-1': {'title': 'Pending', 'class': 'kt-badge--warning'},
	                                    0: {'title': 'Unverified', 'class': 'kt-badge--warning'},                          
	                                    1: {'title': 'Verified', 'class': ' kt-badge--brand'},
	                                
	                                };
	                                return '<span class="kt-badge ' + status[row.domain_status].class + ' kt-badge--inline kt-badge--pill">' + status[row.domain_status].title + '</span>';
	                            },
	                        },
	                        {
	                            field: 'backlink_type',
	                            title: 'Backlink',
	                            autoHide: false,
	                            // callback function support for column rendering
	                            template: function(row) {

	                                var status = {
	                                    'follow': {'title': 'Do Follow', 'state': 'success'},
	                                    'no_follow': {'title': 'No Follow', 'state': 'warning'},                          
	                                };
	                                return '<span class="kt-badge kt-badge--' + status[row.backlink_type].state + ' kt-badge--dot"></span>&nbsp;<span class="kt-font-bold kt-font-' + status[row.backlink_type].state +
	                                    '">' +
	                                    status[row.backlink_type].title + '</span>';
	                            },
	                        } ,
	                         {
	                            field: 'Actions',
	                            title: 'Actions',
	                            sortable: false,
	                            width: 230,
	                            overflow: 'visible',
	                            autoHide: false,
	                            //callback function support for column rendering
	                            template: function(row) {	                            	
	                            	var status = {
	                                    1: {'title': 'Accept', 'class': 'kt-badge--success'},
	                                    2: {'title': 'Reject', 'class': 'kt-badge--danger'},
	                                    3: {'title': 'Not Accept', 'class': 'kt-badge--success'}, 
	                                };

	                            	if(row.dp_da == 0){

		                                return '<a href="#" data-da="'+row.dp_da+'"  data-pa="'+row.dp_pa+'" class="da_class" rel="'+row.domain_id+'"><span class="kt-badge '+ status[1].class+' kt-badge--inline kt-badge--pill">DA Manage</span><a>';

	                            	} else {
	                            		
		                                var urlAccept = "{{url('admin/website/accept/')}}";
		                                var urlReject = "{{url('admin/website/reject/')}}";
		                                if(row.domain_approval_status == 1){
		                                	return '<a href="'+urlReject+'/'+row.url+'" rel="'+row.domain_approval_status+'"><span class="kt-badge '+ status[2].class+' kt-badge--inline kt-badge--pill">Reject</span></a> | <a href="#" data-da="'+row.dp_da+'"  data-pa="'+row.dp_pa+'" class="da_class" rel="'+row.domain_id+'"><span class="kt-badge '+ status[1].class+' kt-badge--inline kt-badge--pill">DA Manage</span></a>';
		                                } else if(row.domain_approval_status == 2) {
		                                	return '<a href="'+urlAccept+'/'+row.url+'" rel="'+row.domain_approval_status+'"><span class="kt-badge '+ status[1].class+' kt-badge--inline kt-badge--pill">Accept</span></a> | <a href="#" data-da="'+row.dp_da+'"  data-pa="'+row.dp_pa+'" class="da_class" rel="'+row.domain_id+'"><span class="kt-badge '+ status[1].class+' kt-badge--inline kt-badge--pill">DA Manage</span></a>';	
		                                } else {
		                                	return '<a href="'+urlAccept+'/'+row.url+'" rel="'+row.domain_approval_status+'"><span class="kt-badge '+ status[1].class+' kt-badge--inline kt-badge--pill">Accept</span></a> | <a href="'+urlReject+'/'+row.url+'" rel="'+row.domain_approval_status+'"><span class="kt-badge '+ status[2].class+' kt-badge--inline kt-badge--pill">Reject</span></a> | <a href="#" data-da="'+row.dp_da+'" data-pa="'+row.dp_pa+'" class="da_class" rel="'+row.domain_id+'"><span class="kt-badge '+ status[1].class+' kt-badge--inline kt-badge--pill">DA Manage</span></a>';
		                                }
	                            	}
	                                
	                                
	                            },
	                        },
	    //                     {
	    //                         field: 'Actions',
	    //                         title: 'Actions',
	    //                         sortable: false,
	    //                         width: 110,
	    //                         overflow: 'visible',
	    //                         autoHide: false,
	    //                         template: function(row) {
	    //                            var APP_URL  = $('#APP_URL').val();
	    //                            var APP_URL2 = $('#APP_URL2').val(); 
	    //                         //     return '\<a href="'+APP_URL+'/'+row.domain_id+'" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit details">\
	    //                         //         <i class="la la-edit"></i>\
	    //                         //     </a>\
	    //                         //     <a href="'+APP_URL2+'/'+row.domain_id+'" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Delete">\
	    //                         //         <i class="la la-trash"></i>\
	    //                         //     </a>\
	    //                         // ';
	    //                         // return '\<a href="'+APP_URL+'/'+row.domain_id+'" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit details">\
	    //                         //         <i class="la la-edit"></i>\
	    //                         //     </a>\
	    //                         // ';
	    //                     }
					// }
					],
			});
	
			$('#kt_form_status').on('change', function() {
				datatable.search($(this).val().toLowerCase(), 'domain_status');
			});

			$('#kt_form_status_1').on('change', function() {
				datatable.search($(this).val().toLowerCase(), 'domain_approval_status');
			});
	
			$('#kt_form_type').on('change', function() {
				datatable.search($(this).val().toLowerCase(), 'Type');
			});
	
			$('#kt_form_status,#kt_form_type,#kt_form_status_1').selectpicker();
	
		};
	
		return {
			// Public functions
			init: function() {
				// init dmeo
				demo();
			},
		};
	}();
	
	jQuery(document).ready(function() {
		KTDatatableDataLocalDemo.init();
	});
	
	</script>	
	<!--end::Page Scripts -->
	@endsection
	@show
@endsection