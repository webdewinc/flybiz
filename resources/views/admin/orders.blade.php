@extends('layout.app')
@section('title', 'See all received order as a Admin - GuestPostEngine')
@section('title-description')

<meta name="description" content="As a Admin, you can visit my orders page on GuestPostEngine marketplace to see how many orders of websites have you received.">
    
@endsection
@section('content')
@section('mainhead')
@parent
    @section('customCSS')
    <!--begin::Page Vendors Styles(used by this page) -->
    <link href="{{ asset('') . config('app.public_url') . '/assets/css/pages/wizard/wizard-2.css'  }}" rel="stylesheet" type="text/css" />
        <!--end::Page Vendors Styles -->
    @endsection
@show
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                
            </div>
        </div>

        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">
            <div class="alert alert-light alert-elevate" role="alert">
                <div class="kt-portlet__body w-100">

                <!--begin: Search Form -->
                <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                    <div class="row align-items-center">
                            <div class="col-xl-8 order-2 order-xl-1">
                                <div class="row align-items-center">
                                    <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                            <div class="kt-form__label">
                                                <label>Search:</label>
                                            </div>
                                        <div class="kt-input-icon kt-input-icon--left">
                                            <input type="text" class="form-control" placeholder="Search..." id="generalSearch">
                                            <span class="kt-input-icon__icon kt-input-icon__icon--left">
                                                <span><i class="la la-search"></i></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                       
                                            <div class="kt-form__label">
                                                <label>Payment Status:</label>
                                            </div>
                                            <div class="kt-form__control">
                                                <select class="form-control" id="kt_form_status">
                                                    <option value="">All</option>
                                                    <option value="2">Unpaid</option>
                                                    <option value="1">Paid</option>
                                                </select>
                                            </div>
                                       
                                    </div>

                                    <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                        
                                            <div class="kt-form__label">
                                                <label>Order Status:</label>
                                            </div>
                                            <div class="kt-form__control">
                                                <select class="form-control" id="kt_form_order_status">
                                                    <option value="">All</option>
                                                    <option value="0">Pending</option>
                                                    <option value="1">Accepted</option>
                                                    <option value="2">Rejected</option>
                                                </select>
                                            </div>
                                        </div>
                                  

                                </div>
                            </div>
                           
                        </div>
                    </div>
                    <!--end: Search Form -->
                </div>              
            </div>
                                
            <div class="kt-portlet kt-portlet--mobile">
                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">

                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect x="0" y="0" width="24" height="24"></rect>
                                    <path d="M8,3 L8,3.5 C8,4.32842712 8.67157288,5 9.5,5 L14.5,5 C15.3284271,5 16,4.32842712 16,3.5 L16,3 L18,3 C19.1045695,3 20,3.8954305 20,5 L20,21 C20,22.1045695 19.1045695,23 18,23 L6,23 C4.8954305,23 4,22.1045695 4,21 L4,5 C4,3.8954305 4.8954305,3 6,3 L8,3 Z" fill="#000000" opacity="0.3"></path>
                                    <path d="M11,2 C11,1.44771525 11.4477153,1 12,1 C12.5522847,1 13,1.44771525 13,2 L14.5,2 C14.7761424,2 15,2.22385763 15,2.5 L15,3.5 C15,3.77614237 14.7761424,4 14.5,4 L9.5,4 C9.22385763,4 9,3.77614237 9,3.5 L9,2.5 C9,2.22385763 9.22385763,2 9.5,2 L11,2 Z" fill="#000000"></path>
                                    <rect fill="#000000" opacity="0.3" x="10" y="9" width="7" height="2" rx="1"></rect>
                                    <rect fill="#000000" opacity="0.3" x="7" y="9" width="2" height="2" rx="1"></rect>
                                    <rect fill="#000000" opacity="0.3" x="7" y="13" width="2" height="2" rx="1"></rect>
                                    <rect fill="#000000" opacity="0.3" x="10" y="13" width="7" height="2" rx="1"></rect>
                                    <rect fill="#000000" opacity="0.3" x="7" y="17" width="2" height="2" rx="1"></rect>
                                    <rect fill="#000000" opacity="0.3" x="10" y="17" width="7" height="2" rx="1"></rect>
                                </g>
                            </svg>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            Orders
                        </h3>
                    </div>
                </div>              
                <div class="kt-portlet__body kt-portlet__body--fit">

                    <!--begin: Datatable -->
                    <div class="kt-datatable" id="local_data"></div>

                    <!--end: Datatable -->
                </div>
            </div>
        </div>
        <!-- end:: Content -->
    </div>
</div>
<!--popup for empty paypal email -->
<div class="modal" id="myModal" role="dialog">
    <div class="modal-dialog">
    <input type="hidden" id="payer_email" name="payer_email" value="" />
    <input type="hidden" id="payer_name" name="payer_name" value="" />
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title text-success">Paypal Email is Missing!!</h4>
        </div>
        <div class="modal-body">
            <p class="text-success"><strong>Do you Want to Send Email to the Publisher ?</strong></p>
            <button class="btn btn-primary text-center send_email" data-status="1">Yes</button>
            <button class="btn btn-primary text-center send_email" data-status="2">No</button>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>    
@section('mainscripts')
@parent
    @section('customScript')
<?php if(!empty( $message )){ ?>
    <p style="text-align: center;">{{$message}}</p>
<?php } else{ ?>
<!--begin::Page Vendors Styles(used by this page) -->  
<script>
    $(document).ready(function(){
        $(document).on('click', '.link_redirect', function(e){ 
            e.preventDefault(); 
            var url = $(this).attr('href'); 
            window.open(url, '_blank');
        });
    });
</script>
<script>
   'use strict';
    // Class definition
    var KTDatatableDataLocalDemo = function() {
        // Private functions
        var data = '<?php echo json_encode( $orderLists ); ?>';
        var split_data = JSON.parse(data);
        var order_id   = split_data[0]['id'];
        var domain_id  = split_data[0]['domain_id']; 
        // var newdata =  JSON.parse(data);
        // demo initializer
        var demo = function() {

            var dataJSONArray = JSON.parse(data);
            var datatable = $('.kt-datatable').KTDatatable({
                // datasource definition
                data: {
                    type: 'local',
                    source: dataJSONArray,
                    pageSize: 10,
                },

                // layout definition
                layout: {
                    scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
                    // height: 450, // datatable's body's fixed height
                    footer: false, // display/hide footer
                },

                // column sorting
                sortable: true,

                pagination: true,

                search: {
                    input: $('#generalSearch'),
                },

                // columns definition
                columns: [
                    {
                        field: '',
                        title: '#',
                        sortable: false,
                        width: 20,
                        type: 'number',
                        selector: {class: 'kt-checkbox--solid'},
                        textAlign: 'center',
                    }, 
                    {
                        field: 'domain_url',
                        title: 'URL',
                        width: 300,
                    },
                    {
                        field: 'cost_price',
                        title: 'Cost price',
                        template: function(row) {
                            if(row.cost_price > 0){
                                return '$'+row.cost_price;    
                            }
                            return row.cost_price;
                        }
                    },
                    {
                        field: 'gross',
                        title: 'Gross Amount',
                        template: function(row) {
                            if(row.gross > 0){
                                return '$'+row.gross;    
                            }
                            return '0';
                        }
                    },
                    {
                        field: 'profit',
                        title: 'Profit',
                        template: function(row) {
                            if(row.profit > 0){
                                return '$'+row.profit;    
                            } 
                            return '0';
                            
                        }
                    },
                    {
                        field: 'status',
                        title: 'Payment Status',
                        
                        // callback function support for column rendering
                        template: function(row) {
                            var statuss = {
                                2: {'title': 'Unpaid', 'class': ' kt-badge--warning'},
                                1: {'title': 'Paid', 'class': ' kt-badge--success'},                                
                            };
                            return '<span class="kt-badge ' + statuss[row.status].class + ' kt-badge--inline kt-badge--pill">' + statuss[row.status].title + '</span>';
                        },
                    },
                    {
                        field: 'a_user_fname',
                        title: 'Advertiser',  
                        width: 150,            
                    },
                    {
                        field: 'p_user_fname',
                        title: 'Publisher',  
                        width: 150,
                    },
                    {
                        field: 'extra_cost',
                        title: 'Extra Cost',
                        template: function(row) {
                            if(row.extra_cost > 0){
                                return '$'+row.extra_cost;    
                            }
                            return row.extra_cost;
                        }
                    },
                    {
                        field: 'created_at',
                        title: 'Order Date',
                        type: 'date',
                        format: 'YYYY-MM-DD',
                        width: 150,
                    },
                    {
                        field: 'p_email',
                        title: 'Publisher Email',
                        width: 150,
                    },
                    {
                        field: 'a_email',
                        title: 'Advertiser Email',
                        width: 150,
                    },
                    {
                        field: 'p_paypal_email',
                        title: 'Publisher Paypal Email',
                        width: 150,
                    },
                    {
                        field: 'keyword',
                        title: 'Keyword',
                        
                    },
                    {
                        field: 'title',
                        title: 'Article Title',
                        
                    },
                    {
                        field: 'url',
                        title: 'Target URL',
                        
                    },
                    {
                        field: 'is_accept',
                        title: 'Order status',
                        sortable: false,
                        width: 160,
                        overflow: 'visible',
                        autoHide: false,
                        // callback function support for column rendering
                        template: function(row) {
                            var status = {
                                0: {'title': 'Pending', 'class': 'kt-badge--warning'},
                                1: {'title': 'Accepted', 'class': ' kt-badge--success'},
                                2: {'title': 'Rejected', 'class': ' kt-badge--danger'},
                                3: {'title': 'Cancelled', 'class': ' kt-badge--danger'},
                            };
                            //return '<span class="kt-badge ' + status[row.is_accept].class + ' kt-badge--inline kt-badge--pill">' + status[row.is_accept].title + '</span>';
                            if(row.is_accept == 0){
                                return '<span class="kt-badge ' + status[row.is_accept].class + ' kt-badge--inline kt-badge--pill">' + status[row.is_accept].title + '</span>';
                            } else if(row.is_accept == 1){

                                if(row.stages > 3){

                                    switch(row.stages) {
                                      case '4':
                                            return '<span class="kt-badge kt-badge--warning kt-badge--inline kt-badge--pill">Content approval pending</span>';
                                        break;
                                      case '5':
                                            return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">Content approved</span>';
                                        break;
                                      case '6':
                                            return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">Content rejected</span>';
                                        break;
                                      case '7':
                                           // live link update
                                           return '<span class="kt-badge kt-badge--warning kt-badge--inline kt-badge--pill">live link pending for approval</span>';
                                            
                                        break;
                                      case '8':
                                           // live link update
                                           return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">live link approved</span>';
                                        break;
                                      case '9':
                                           // live link update
                                           return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">live link Rejected</span>';
                                        break;
                                      case '10':
                                        // order delever
                                            return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">Order Delivered</span>';
                                        break;
                                      case '11':
                                            if(row.pay_to_seller_id < 1) {
                                               // order delever
                                                return '<span class="kt-badge kt-badge--warning kt-badge--inline kt-badge--pill">Payment release after 15 days </span>';

                                            } else {
                                                // order delever
                                                return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">Payment released </span>';
                                            } 
                                        break;

                                    }
                                    
                                } else {
                                    return '<span class="kt-badge ' + status[row.is_accept].class + ' kt-badge--inline kt-badge--pill">' + status[row.is_accept].title + '</span>';
                                }
                                
                            } else {
                                return '<span class="kt-badge ' + status[row.is_accept].class + ' kt-badge--inline kt-badge--pill">' + status[row.is_accept].title + '</span>';
                            }

                        },
                    },
                    
                    {
                        field: 'content_type',
                        title: 'Content Type',
                        template: function(row) {
                            var status = {
                                'buyer': {'title': 'Advertiser', 'class': ' kt-badge--success'},
                                'seller': {'title': 'Publisher', 'class': 'kt-badge--success'},
                            };
                            return '<span class="kt-badge ' + status[row.content_type].class + ' kt-badge--inline kt-badge--pill">' + status[row.content_type].title + '</span>';
                        },
                    },
                    {
                        field: 'promo_code',
                        title: 'Promo Code',
                        // callback function support for column rendering
                        template: function(row) {
                            var status = {
                                0: {'title': 'NA', 'class': ' kt-badge--warning'}
                            };
                            if(row.promo_code == 0){
                                return '<span class="kt-badge ' + status[row.promo_code].class + ' kt-badge--inline kt-badge--pill">' + status[row.promo_code].title + '</span>';    
                            } else {
                                return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">' + row.promo_code + '</span>';
                            }
                            
                        },
                    },
                    {
                        field: 'Pay to Publisher',
                        title: 'Pay to Publisher',
                        sortable: false,
                        width: 150,
                        overflow: 'visible',
                        autoHide: false,
                        template: function(row) {
                            
                            //console.log(row.payer_email);
                           // if(row.paypal_email != '' && row.paypal_email != 0 ){
                                
                                //if(row.promo_code == 0) {

                                    if( row.is_accept == 1 ) {
                                        if(row.stages == 11){
                                            if(row.pay_to_seller_id > 0) {
                                                return '<a><span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">PAID('+'$'+row.total_amount+')</span><a>';
                                            }
                                            else{
                                                return '<a href="#" data-id="'+row.id+'" data-amount="'+row.total_amount+'" data-email ="'+row.paypal_email+'" data-payer-email="'+row.payer_email+'" class="pay_to_seller"><span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">Pay to Publisher ($'+row.total_amount+')</span><a>';
                                            }
                                        }

                                    }
                                    //  else if( row.is_accept == 2) {
                                    //     return '<a><span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">Order Rejected</span><a>';
                                    // } else if( row.is_accept == 3) {
                                    //     return '<a><span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">Cancelled</span><a>';
                                    // } else {
                                    //     return '<a><span class="kt-badge kt-badge--warning kt-badge--inline kt-badge--pill">Order Not Accepted yet</span><a>';
                                    // }
                                    
                                // } else {

                                //     if( row.is_accept == 1) {
                                //         return '<a><span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">PAID('+'$'+row.total_amount+')</span><a>';
                                //     } else if( row.is_accept == 2) {
                                //         return '<a><span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">Order Rejected</span><a>';
                                //     } else {
                                //         return '<a><span class="kt-badge kt-badge--warning kt-badge--inline kt-badge--pill">Order Not Accepted yet</span><a>';
                                //     }
       
                                // }
                            // } else {
                            //     return '\<div><button class="btn btn-warning"> Paypal Email required </button></div>\
                            //             ';
                            // }
                            
                        }
                    },
                    {
                        field: 'Pay to Advertiser',
                        title: 'Advertiser Wallet',
                        template: function(row) {

                            if( row.wallet > 0) {
                                return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">Wallet ('+'$'+row.wallet+')</span>';
                            }
                            
                        }
                    },
                    {
                        field: 'Invoice',
                        title: 'Invoice',
                        sortable: false,
                        width: 110,
                        overflow: 'visible',
                        template: function(row) {

                             return '<a class="link_redirect" href="'+row.invoiceUrl+'/'+row.encrypt_id+'"><span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">Invoice</span><a>';
                        }
                    },
                    {
                        field: 'Actions',
                        title: 'Actions',
                        sortable: false,
                        width: 150,
                        overflow: 'visible',
                        autoHide: false,
                        template: function(row) {
                            var id = row.id;
                            var html = '';
                            if(row.is_accept == 0){

                                html += '<a href="#" data-orderid="'+id+'" class="status_active"  data-status="1" ><span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">Accept</span></a> | <a href="#" class="status_active" data-orderid="'+id+'"  data-status="0"><span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">Reject</span></a> | ';

                            } 
                            // else if(row.is_accept == 1) {
                            //     html += '<a href="javascript:void(0);" ><span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">Accepted</span></a>';
                            // } else if(row.is_accept == 2) {
                            //     return '<a href="javascript:void(0);"><span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">Rejected</span></a>';
                            // } else if(row.is_accept == 3) {
                            //     return '<a href="javascript:void(0);"><span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">Cancelled</span></a>';
                            // }
                            if(row.pay_to_seller_id < 1){
                                html +='<a href="#" data-orderid="'+id+'" class="status_cancel"  data-status="3" ><span class="kt-badge kt-badge--warning kt-badge--inline kt-badge--pill">Cancel</span></a>';    
                            }
                            return html;
                            
                            
                        }, 

                    }
                    ],
                });

 // {
 //                        field: 'Actions',
 //                        title: 'Actions',
 //                        sortable: false,
 //                        width: 110,
 //                        overflow: 'visible',
 //                        autoHide: false,
 //                        template: function(row) {
 //                            return '\<div class="dropdown"><a href="javascript:void(0);" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="false"><i class="la la-cog"></i></a>    <div class="dropdown-menu dropdown-menu-right" style="display: none;">        <a href="javascript:void(0);" data-domainid="'+domain_id+'" class="dropdown-item status_active"  data-status="1" >Accept</a> <a  href="javascript:void(0);" class="dropdown-item status_active" data-domainid="'+domain_id+'"  data-status="0" domainID="4">Reject</a>    </div></div>\
 //                        ';
 //                        }, 

 //                    },

            $('#kt_form_status').on('change', function() {
                datatable.search($(this).val().toLowerCase(), 'status');
            });
            $('#kt_form_order_status').on('change', function() {
                datatable.search($(this).val().toLowerCase(), 'is_accept');
            });

            $('#kt_form_type').on('change', function() {
                datatable.search($(this).val().toLowerCase(), 'Type');
            });

            $('#kt_form_status,#kt_form_type,#kt_form_order_status').selectpicker();

        };

        return {
            // Public functions
            init: function() {
                // init dmeo
                demo();
            },
        };
    }();

jQuery(document).ready(function() {
    KTDatatableDataLocalDemo.init();
});

</script>
<script>
    $(document).on('click','.status_active',function(e){
        var dataStatus    =   $(this).attr('data-status');
        var order_id     =   $(this).attr('data-orderid');
        $('.loader').fadeIn('slow');

        if( dataStatus == '1'){
            
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url  : "{{ url('/admin/order/accept') }}",
                type : 'POST',
                data : {
                    'order_id' : order_id,
                },
                async: true,
                dataType: 'json',
                enctype: 'multipart/form-data',
                cache: false, 
                success:function(response){
                     if(response.success == true){
                        $('.loader').fadeOut('slow');
                        swal.fire({
                            "title": "",
                            "text": response.message,
                            "type": "success",
                            "confirmButtonClass": "btn btn-secondary"
                        });
                     } else {
                        $('.loader').fadeOut('slow');
                        swal.fire({
                            "title": "",
                            "text": "something were wrong",
                            "type": "warning",
                            "confirmButtonClass": "btn btn-secondary"
                        });
                     }
                     window.location.href = "{{url('admin/orders')}}";
                },
                error:function(){
                    $('.loader').fadeOut('slow');
                    swal.fire({
                            "title": "",
                            "text": "something were wrong",
                            "type": "danger",
                            "confirmButtonClass": "btn btn-secondary"
                    });
                }      
            });
        } else {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url  : "{{ url('/admin/order/reject') }}",
                type : 'POST',
                data : {
                    'order_id' : order_id,
                },
                async: true,
                dataType: 'json',
                enctype: 'multipart/form-data',
                cache: false,
                success:function(response){
                     if(response.success == false){
                        $('.loader').fadeOut('slow');
                         swal.fire({
                            "title": "",
                            "text": "Order has been Rejected.",
                            "type": "success",
                            "confirmButtonClass": "btn btn-secondary"
                        });
                     } else {
                        $('.loader').fadeOut('slow');
                        swal.fire({
                            "title": "",
                            "text": "something were wrong",
                            "type": "warning",
                            "confirmButtonClass": "btn btn-secondary"
                        });
                     }
                     window.location.href = "{{url('admin/orders')}}";
                },
                error:function(){
                    $('.loader').fadeOut('slow');
                    swal.fire({
                            "title": "",
                            "text": "something were wrong",
                            "type": "danger",
                            "confirmButtonClass": "btn btn-secondary"
                    });
                }
            });


        }

        e.stopImmediatePropagation();
        return false;
    });
        $(document).on('click','.status_cancel',function(e){
        var dataStatus    =   $(this).attr('data-status');
        var order_id     =   $(this).attr('data-orderid');
        $('.loader').fadeIn('slow');

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url  : "{{ url('/admin/order/cancel') }}",
                type : 'POST',
                data : {
                    'order_id' : order_id,
                },
                async: true,
                dataType: 'json',
                enctype: 'multipart/form-data',
                cache: false,
                success:function(response){
                     if(response.success == false){
                        $('.loader').fadeOut('slow');
                         swal.fire({
                            "title": "",
                            "text": "Order has been cancelled and amount has stored into Advertiser wallet.",
                            "type": "success",
                            "confirmButtonClass": "btn btn-secondary"
                        });
                     } else {
                        $('.loader').fadeOut('slow');
                        swal.fire({
                            "title": "",
                            "text": "something were wrong",
                            "type": "warning",
                            "confirmButtonClass": "btn btn-secondary"
                        });
                     }
                     window.location.href = "{{url('admin/orders')}}";
                },
                error:function(){
                    $('.loader').fadeOut('slow');
                    swal.fire({
                            "title": "",
                            "text": "something were wrong",
                            "type": "danger",
                            "confirmButtonClass": "btn btn-secondary"
                    });
                }
            });

        e.stopImmediatePropagation();
        return false;
    });
</script>
<script>

    // $(document).on('click','.pay_to_seller',function(e){
    //     Swal.fire({
    //       title: 'Are you sure?',
    //       html: "Amount will be credited to publisher",
    //       icon: 'warning',
    //       showCancelButton: true,
    //       confirmButtonClass: 'btn btn-secondary',
    //       cancelButtonClass: 'btn btn-secondary',
    //       confirmButtonText: 'Yes'

    //     }).then((result) => {
    //       if (result.value) {
    //             // your deletion code
    //         $('.loader').fadeIn('slow');
    //         var dataAmount     =   $(this).attr('data-amount');
    //         var order_id       =   $(this).attr('data-id');
    //         var paypal_email   =   $(this).attr('data-email');
    //         var payer_email    =   $(this).attr('data-payer-email');
    //         var payer_name     =   $(this).attr('data-payer-name'); 
    //         //apply ajax
    //         $.ajax({
    //             headers: {
    //                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //             },
    //             url  : "{{ url('/admin/payToSeller') }}",
    //             type : 'POST',
    //             data : {
    //                 'dataAmount'    : dataAmount,
    //                 'order_id'      : order_id,
    //                 'paypal_email'  : paypal_email,
    //                 'payer_email'   : payer_email,
    //                 'payer_name'    : payer_name,
    //             },
    //             async: true,
    //             dataType: 'json',
    //             enctype: 'multipart/form-data',
    //             cache: false, 
    //             success:function(response){
    //                 $('.loader').fadeOut('slow'); 
    //                 if(response.success == 'true' ){
    //                     swal.fire({
    //                         "title" : "",
    //                         "text"  : "Payment has been done!!",
    //                         "type"  : "success",
    //                         "confirmButtonClass": "btn btn-secondary"
    //                     });
    //                     location.reload();
    //                 }
    //                 if(response.success == 'false'){
    //                     swal.fire({
    //                         "title" : "",
    //                         "text"  : "Payment has already Done",
    //                         "type"  : "warning",
    //                         "confirmButtonClass": "btn btn-secondary"
    //                     });
    //                 }
    //                 if(response.success == 'mail_empty'){
    //                     $('#payer_name').val(response.payer_name);
    //                     $('#payer_email').val(response.payer_email);
    //                     $('#myModal').modal('show');
    //                 }
    //             },
    //             error:function(){} 
    //         });

    //         e.stopImmediatePropagation();
    //         return false; 
    //       }
    //     });
    //     return false;
    // });


    // $(document).on('click','.pay_to_seller',function(e){
    //     Swal.fire({
    //       title: 'Are you sure?',
    //       html: "Amount will be credited to publisher",
    //       icon: 'warning',
    //       showCancelButton: true,
    //       confirmButtonClass: 'btn btn-secondary',
    //       cancelButtonClass: 'btn btn-secondary',
    //       confirmButtonText: 'Yes'

    //     }).then((result) => {
    //       if (result.value) {
    //             // your deletion code
    //         $('.loader').fadeIn('slow');
    //         var dataAmount     =   $(this).attr('data-amount');
    //         var order_id       =   $(this).attr('data-id');
    //         var paypal_email   =   $(this).attr('data-email');
    //         var payer_email    =   $(this).attr('data-payer-email');
    //         var payer_name     =   $(this).attr('data-payer-name'); 
    //         //apply ajax
    //         $.ajax({
    //             headers: {
    //                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //             },
    //             url  : "{{ url('/admin/payToSellerPayouts') }}",
    //             type : 'POST',
    //             data : {
    //                 'dataAmount'    : dataAmount,
    //                 'order_id'      : order_id,
    //                 'paypal_email'  : paypal_email,
    //                 'payer_email'   : payer_email,
    //                 'payer_name'    : payer_name,
    //             },
    //             async: true,
    //             dataType: 'json',
    //             enctype: 'multipart/form-data',
    //             cache: false, 
    //             success:function(response){
    //                 $('.loader').fadeOut('slow'); 
    //                 if(response.success == 'true' ){
    //                     swal.fire({
    //                         "title" : "",
    //                         "text"  : "Payment has been done!!",
    //                         "type"  : "success",
    //                         "confirmButtonClass": "btn btn-secondary"
    //                     });
    //                     location.reload();
    //                 }
    //                 if(response.success == 'false'){
    //                     swal.fire({
    //                         "title" : "",
    //                         "text"  : "Payment has not been Done",
    //                         "type"  : "warning",
    //                         "confirmButtonClass": "btn btn-secondary"
    //                     });
    //                 }
    //                 if(response.success == 'mail_empty'){
    //                     $('#payer_name').val(response.payer_name);
    //                     $('#payer_email').val(response.payer_email);
    //                     $('#myModal').modal('show');
    //                 }
    //             },
    //             error:function(){} 
    //         });

    //         e.stopImmediatePropagation();
    //         return false; 
    //       }
    //     });
    //     return false;
    // });


    $(document).on('click','.pay_to_seller',function(e){
        Swal.fire({
          title: 'Are you sure?',
          html: "Make sure you have sent money using direct paypal account to " + $(this).attr('data-payer-email') + ".",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonClass: 'btn btn-secondary',
          cancelButtonClass: 'btn btn-secondary',
          confirmButtonText: 'Yes'

        }).then((result) => {
          if (result.value) {
                // your deletion code
            $('.loader').fadeIn('slow');
            var dataAmount     =   $(this).attr('data-amount');
            var order_id       =   $(this).attr('data-id');
            var paypal_email   =   $(this).attr('data-email');
            var payer_email    =   $(this).attr('data-payer-email');
            var payer_name     =   $(this).attr('data-payer-name'); 
            //apply ajax
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url  : "{{ url('/admin/payToSellerManual') }}",
                type : 'POST',
                data : {
                    'dataAmount'    : dataAmount,
                    'order_id'      : order_id,
                    'paypal_email'  : paypal_email,
                    'payer_email'   : payer_email,
                    'payer_name'    : payer_name,
                },
                async: true,
                dataType: 'json',
                enctype: 'multipart/form-data',
                cache: false, 
                success:function(response){
                    $('.loader').fadeOut('slow'); 
                    if(response.success == 'true' ){
                        swal.fire({
                            "title" : "",
                            "text"  : "Payment has been done!!",
                            "type"  : "success",
                            "confirmButtonClass": "btn btn-secondary"
                        });
                        //location.reload();
                    }
                    if(response.success == 'false'){
                        swal.fire({
                            "title" : "",
                            "text"  : "Payment has already Done",
                            "type"  : "warning",
                            "confirmButtonClass": "btn btn-secondary"
                        });
                    }
                    if(response.success == 'mail_empty'){
                        $('#payer_name').val(response.payer_name);
                        $('#payer_email').val(response.payer_email);
                        $('#myModal').modal('show');
                    }
                },
                error:function(){} 
            });

            e.stopImmediatePropagation();
            return false; 
          }
        });
        return false;
    });

</script>
<script>
    $(document).on('click','.send_email',function(e){
        var status = $(this).attr('data-status');
        $('.loader').fadeIn('slow'); 
        if( status == '1'){
            var payer_email = $('#payer_email').val();
            var payer_name  = $('#payer_name').val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url  : "{{ url('/admin/add_paypalEmail') }}",
                type : 'POST',
                data : {
                    'payer_email' : payer_email,
                    'payer_name'  : payer_name,
                },
                async: true,
                dataType: 'json',
                enctype: 'multipart/form-data',
                cache: false,
                success:function(response){
                    $('.loader').fadeOut('slow'); 
                     if(response.success == 'true'){
                        $('#myModal').modal('hide');
                         swal.fire({
                            "title": "",
                            "text": "Email has been Sent to publisher",
                            "type": "success",
                            "confirmButtonClass": "btn btn-secondary"
                        });
                     }
                },
                error:function(){}       
            });
        }   
        else{
            $('#myModal').modal('hide');
        }
        e.stopImmediatePropagation();
        return false;
    });
</script>
 <?php } ?>           
<!--end::Page Vendors Styles -->
@endsection
@show
@endsection
