<div class="kt-portlet kt-portlet--last">
    <div class="kt-portlet__head">
        <div class="kt-chat__head ">
            <div class="kt-chat__left">
                <div class="kt-chat__label">
                    <a href="javascript:void(0);" class="kt-chat__title">{{$otheruserdata['user_fname']}} {{$otheruserdata['user_lname']}}</a>
                    <!-- <span class="kt-chat__status">
                        <span class="kt-badge kt-badge--dot kt-badge--success"></span> Active
                    </span> -->
                </div>
            </div>
            <input id="username-input" value="{{$userdata['user_fname']}}" type="hidden" placeholder="username"/>
            <input id="chatChannelId" name="chatChannelId" value="{{$channel_id}}" type="hidden">
            <input id="roomname" value="{{$room_name}}" type="hidden"/>
            <input id="roomnameview" value="{{$room_name_view}}" type="hidden">
            <input id="user_id" value="{{$otheruserdata['user_id']}}" type="hidden"/>
            <input id="auth" value="{{$userdata['user_id']}}" type="hidden"/>
            <div class="kt-chat__right">
                <div class="dropdown dropdown-inline">
                    
                </div>
                <button type="button" class="btn btn-clean btn-sm btn-icon" data-dismiss="modal">
                    <i class="flaticon2-cross"></i>
                </button>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body">
        <div class="kt-scroll kt-scroll--pull" data-height="410" data-mobile-height="225" style="overflow-y: scroll;height: 240px;">
            <span id="channel-list" style="display:none;"></span>
            <div class="kt-chat__messages kt-chat__messages--solid"  id="message-list">
                <!-- messages -->
            <?php $numItems = count($getmsg); $i=0;?>    
                <!-- message list here -->
            @foreach($getmsg as $msg)
                <?php   
                    $date = date_create($msg['created_at']);
                    $date=date_format($date,"d/m/Y h:iA");
                    $date1=explode(' ', $date);
                    $todaydate=date("d/m/Y");
                    if($todaydate == $date1[0]) {
                        $actualdate='Today';
                    } else {
                        $actualdate=$date1[0];
                    }
                ?>
                <?php
                    $id = 0;
                    $date = '';
                    $ids_explode = explode("_",$msg['chatroom']['chatroom']);
                    $otherUser = '';
                    if($ids_explode[0] == Auth::user()->user_id):
                        $id = $ids_explode[0];
                        $otherUser = $ids_explode[1];
                        $image = \App\User::where('user_id',$otherUser)->value('image');
                        $name = \App\User::where('user_id',$otherUser)->value('user_fname');
                    else :
                        $id = $ids_explode[1];
                        $otherUser = $ids_explode[0];
                        $image = \App\User::where('user_id',$otherUser)->value('image');
                        $name = \App\User::where('user_id',$otherUser)->value('user_fname');
                    endif;
                    ?>
                    @if($msg['sender_id'] == Auth::user()->user_id)
                        
                        <div class="kt-chat__message kt-chat__message--right kt-chat__message--brand">
                            <div class="kt-chat__user">
                                <span class="kt-chat__datetime">{{$actualdate}} - {{$date1[1]}}</span>
                                <?php 
                                    $name = \App\User::where('user_id',$msg['sender_id'])->value('user_fname')
                                ?>
                                <a href="javascript:void(0);" class="kt-chat__username">{{@$name}}</span></a>
                                <span class="kt-media kt-media--circle kt-media--sm">
                                    <img src="{{ asset('') . config('app.public_url') . '/assets/media/users/default.jpg'}}" alt="image">
                                </span>
                            </div>
                            <div class="kt-chat__text">
                                {{$msg['reply']}}
                            </div>
                        </div>
                    @else  
                        

                        <div class="kt-chat__message kt-chat__message--success">
                            <div class="kt-chat__user">
                                <span class="kt-media kt-media--circle kt-media--sm">
                                    <img src="{{ asset('') . config('app.public_url') . '/assets/media/users/default.jpg'}}" alt="image">
                                </span>
                                <?php 
                                    $name = \App\User::where('user_id',$msg['sender_id'])->value('user_fname');
                                ?>

                                <a href="javascript:void(0);" class="kt-chat__username">{{@$name}}</span></a>
                                <span class="kt-chat__datetime">{{$actualdate}} - {{$date1[1]}}</span>
                            </div>
                            <div class="kt-chat__text">
                                {{$msg['reply']}}
                            </div>
                        </div>
                    @endif    
                @endforeach      
             
            </div>
        </div>
    </div>
    <div class="kt-portlet__foot">
        <div class="kt-chat__input">
            <div class="kt-chat__editor">                            
                <textarea id="input-text" disabled="true" style="height: 50px" placeholder="Type here..."></textarea>
                <label class="error"></label>
            </div>
            <div class="kt-chat__toolbar">
                <!-- <div class="kt_chat__tools">
                    <a href="#"><i class="flaticon2-link"></i></a>
                    <a href="#"><i class="flaticon2-photograph"></i></a>
                    <a href="#"><i class="flaticon2-photo-camera"></i></a>
                </div> -->
                <div class="kt_chat__actions">
                    <button type="button" id="input-button" class="btn btn-brand btn-md btn-upper btn-bold kt-chat__reply">reply</button>
                </div>                            
            </div>
        </div>
    </div>
</div>