@if(!empty($chatChannel))
 
    @foreach($chatChannel as $key => $val)
        <?php
        $id = 0;
        $date = '';
        $domain_id = $val->domain_id;
        $ids_explode = explode("_",$val->chatroom);
        
        $id = $ids_explode[0];
        $otherUser = $ids_explode[1];

       $count = \App\Chat::where('chatroom_id',$val->id)->where('is_viewed',0)->where('sender_id','=',$otherUser)->count();
        $domain_url = \App\MstDomainUrl::where('domain_id',$val->domain_id)->value('domain_url');
        $all_chat = \App\Chat::where('chatroom_id',$val->id)->orderBy('id','DESC')->first();        

        $all_chat = json_decode(json_encode($all_chat), true);
        
        ?>
        @if($all_chat)
        @php
            $date = date_create(@$all_chat['created_at']);
            $date = date_format($date,"d/m/Y h:iA");
            $date1 = explode(' ', $date);
            $todaydate=date("d/m/Y");
            if($todaydate == $date1[0]) :
                $actualdate='Today';
            else :
                $actualdate=$date1[0];
            endif;
        @endphp
        @endif
            <a href="javascript:void(0);" data-chatroom="{{$val->chatroom}}" data-auth-user="{{$id}}"  data-other-user="{{$otherUser}}" data-domain-id="{{$val->domain_id}}"  class="kt-widget__username chatroom">
                <div class="kt-widget__item align-items-center position-relative">
                    <?php
                    
                        if(!empty($image)) :
                            $image = url('storage/app/') . '/' . $image;
                        else :
                            $name = strtoupper($rest = substr(@$name, 0, 1));
							$lname = strtoupper($rest = substr(@$lname, 0, 1));
                        endif;
                        if(!empty($image)) :
                            ?>
                            <span class="kt-media kt-media--circle">
                                <img src="{{$image}}" alt="image">
                            </span>
                            <?php
                        else :
                            
                            $image = asset('') . config('app.public_url') .'/assets/media/users/default.jpg';
                            
                            if(!empty($name)) :
                                ?>
                                <span class="kt-badge kt-badge--username kt-badge--unified-primary kt-badge--lg kt-badge--rounded kt-badge--bold">
                                {{$name}}{{$lname}}</span>
                                <?php
                            else :
                                ?>
                                <span class="kt-media kt-media--circle">
                                        <img src="{{$image}}" alt="image">
                                </span>
                                <?php
                            endif;
                        endif;

                    ?>
                    @if($count > 0)
                         <span class="kt-badge kt-badge--success kt-font-bold">{{$count}}</span>
                    @endif
                    <div class="kt-widget__info">
                        <div class="kt-widget__section">

                            <?php
                                 $image = \App\User::where('user_id',$otherUser)->value('image');
                $name = \App\User::where('user_id',$otherUser)->value('user_fname');
                $lname = \App\User::where('user_id',$otherUser)->value('user_lname');
                

                $imageid = \App\User::where('user_id',$id)->value('image');
                $nameid = \App\User::where('user_id',$id)->value('user_fname');
                $lnameid = \App\User::where('user_id',$id)->value('user_lname');

                
                            ?>
                            {{@$name}} {{@$lname}} to {{@$nameid}} {{@$lnameid}} for {{@$domain_url}} 
                        </div>
                        <span class="kt-widget__desc">
                            <!-- last message -->
                            @if($all_chat)
                                {{$all_chat['reply']}}
                            @endif
                        </span>
                    </div>
                    <div class="kt-widget__action">
                        <span class="kt-widget__date">{{$date}}</span>
                    </div>
                </div>
                </a>
            
    @endforeach
@endif