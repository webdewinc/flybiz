<div class="kt-portlet__head">
    <div class="kt-chat__head ">
        <div class="kt-chat__left d-flex">
            <div class="kt-chat__pic kt-mr-10">
                <span class="kt-media kt-media--sm" data-toggle="kt-tooltip" data-placement="right" title="" data-original-title="Tooltip title">
                    <?php 
                        $name = @$otheruserdata['user_fname'];
                        $lname = @$otheruserdata['user_lname'];
                        $image = @$otheruserdata['image'];

                        $domain_url = \App\MstDomainUrl::where('domain_id',$domain_id)->value('domain_url');
                        if(!empty($image)) :
                            $image = url('storage/app/') . '/' . $image;
                        else :
                            $name = strtoupper($rest = substr(@$name, 0, 1));
                            $lname = strtoupper($rest = substr(@$lname, 0, 1));
                        endif;
                        if(!empty($image)) :
                            ?>
                            <span class="kt-media kt-media--circle">
                                    <img src="{{$image}}" alt="image">
                            </span>
                            <?php
                        else :
                            
                            $image = asset('') . config('app.public_url') .'/assets/media/users/default.jpg';
                            
                            if(!empty($name)) :
                                ?>
                                <span class="kt-badge kt-badge--username kt-badge--unified-primary kt-badge--lg kt-badge--rounded kt-badge--bold">
                                {{$name}}{{$lname}}</span>
                                <?php
                            else :
                               ?>
                                <div class="kt-avatar__holder" style="background-image: url('<?php echo $image ?>');"></div>
                                <?php
                            endif;
                        endif;

                    ?>
                </span>
            </div>
            <div class="kt-chat__label d-flex align-items-center">
                <a href="{{url('profile').'/'.$otheruserdata['user_code']}}" class="kt-chat__title">{{$otheruserdata['user_fname']}} {{$otheruserdata['user_lname']}} for {{$domain_url}}</a>
                <!-- <span class="kt-chat__status">
                    <span class="kt-badge kt-badge--dot kt-badge--success"></span> Active
                </span> -->
            </div>
        </div>
        <div class="kt-chat__center">
            
        </div>
        <div class="kt-chat__right">
          
        </div>
    </div>
</div>
<input id="username-input" value="{{$userdata['user_fname']}}" type="hidden" placeholder="username"/>
<input id="chatChannelId" name="chatChannelId" value="{{$channel_id}}" type="hidden">
<input id="roomname" value="{{$room_name}}" type="hidden"/>
<input id="roomnameview" value="{{$room_name_view}}" type="hidden">
<input id="user_id" value="{{$otheruserdata['user_id']}}" type="hidden"/>
<input id="auth" value="{{$userdata['user_id']}}" type="hidden"/>

<div class="kt-portlet__body">
    <div class="kt-scroll kt-scroll--pull" id="input-div" data-mobile-height="300">                
        <span id="channel-list" style="display:none;"></span>
        <div class="kt-chat__messages" id="message-list">
            <!-- messages -->
            <?php $numItems = count($getmsg); $i=0;?>    
                <!-- message list here -->
            @foreach($getmsg as $msg)
                <?php   
                    $date = date_create($msg['created_at']);
                    $date=date_format($date,"d/m/Y h:iA");
                    $date1=explode(' ', $date);
                    $todaydate=date("d/m/Y");
                    if($todaydate == $date1[0]) {
                        $actualdate='Today';
                    } else {
                        $actualdate=$date1[0];
                    }
                ?>
                <?php
                    $id = 0;
                    $date = '';
                    $ids_explode = explode("_",$msg['chatroom']['chatroom']);
                    $otherUser = '';
                    if($ids_explode[0] == Auth::user()->user_id):
                        $id = $ids_explode[0];
                        $otherUser = $ids_explode[1];
                        $image = \App\User::where('user_id',$otherUser)->value('image');
                        $name = \App\User::where('user_id',$otherUser)->value('user_fname');
                        $lname = \App\User::where('user_id',$otherUser)->value('user_lname');
                        $code = \App\User::where('user_id',$otherUser)->value('user_code');
                    else :
                        $id = $ids_explode[1];
                        $otherUser = $ids_explode[0];
                        $image = \App\User::where('user_id',$otherUser)->value('image');
                        $name = \App\User::where('user_id',$otherUser)->value('user_fname');
                        $lname = \App\User::where('user_id',$otherUser)->value('user_lname');

                        $code = \App\User::where('user_id',$otherUser)->value('user_code');

                    endif;
                    ?>
                    @if($msg['sender_id'] == Auth::user()->user_id)
                        <div class="kt-chat__message kt-chat__message--right">
                            <div class="kt-chat__user">
                                <span class="kt-chat__datetime">{{$actualdate}} - {{$date1[1]}}</span>
                                <?php 
                                    $name = \App\User::where('user_id',$msg['sender_id'])->value('user_fname');
                                    $lname = \App\User::where('user_id',$msg['sender_id'])->value('user_lname');
                                    $code = \App\User::where('user_id',$msg['sender_id'])->value('user_code');
                                ?>
                                <a href="{{url('profile').'/'.$code}}" class="kt-chat__username">{{@$name}}</span></a>
                                <span class="kt-media kt-media--sm">
                                    <!-- <img src="{{ asset('') . config('app.public_url') . '/assets/media/users/default.jpg'}}" alt="image"> -->
                                    <?php
                                    if(!empty($image)) :
                                        $image = url('storage/app/') . '/' . $image;
                                    else :
                                        $name = strtoupper($rest = substr(@$name, 0, 1));
                                        $lname = strtoupper($rest = substr(@$lname, 0, 1));
                                    endif;
                                    if(!empty($image)) :
                                        ?>
                                        <span class="kt-media kt-media--circle">
                                                <img src="{{$image}}" alt="image">
                                        </span>
                                        <?php
                                    else :
                                        
                                        $image = asset('') . config('app.public_url') .'/assets/media/users/default.jpg';
                                        
                                        if(!empty($name)) :
                                            ?>
                                            <span class="kt-badge kt-badge--username kt-badge--unified-primary kt-badge--lg kt-badge--rounded kt-badge--bold">
                                            {{$name}}{{$lname}}</span>
                                            <?php
                                        else :
                                           ?>
                                            <div class="kt-avatar__holder" style="background-image: url('<?php echo $image ?>');"></div>
                                            <?php
                                        endif;
                                    endif;
                                    ?>
                                </span>
                            </div>
                            <div class="kt-chat__text kt-bg-light-brand">
                                {{$msg['reply']}}
                            </div>
                        </div>
                    @else  
                        
                        <div class="kt-chat__message">
                            <div class="kt-chat__user">    
                                <span class="kt-media kt-media--sm">
                                    <!-- <img src="{{ asset('') . config('app.public_url') . '/assets/media/users/default.jpg'}}" alt="image"> -->

                                    <?php
                                    if(!empty($image)) :
                                        $image = url('storage/app/') . '/' . $image;
                                    else :
                                        $name = strtoupper($rest = substr(@$name, 0, 1));
                                        $lname = strtoupper($rest = substr(@$lname, 0, 1));
                                    endif;
                                    if(!empty($image)) :
                                        ?>
                                        <span class="kt-media kt-media--circle">
                                                <img src="{{$image}}" alt="image">
                                        </span>
                                        <?php
                                    else :
                                        
                                        $image = asset('') . config('app.public_url') .'/assets/media/users/default.jpg';
                                        
                                        if(!empty($name)) :
                                            ?>
                                            <span class="kt-badge kt-badge--username kt-badge--unified-primary kt-badge--lg kt-badge--rounded kt-badge--bold">
                                            {{$name}}{{$lname}}</span>
                                            <?php
                                        else :
                                           ?>
                                            <div class="kt-avatar__holder" style="background-image: url('<?php echo $image ?>');"></div>
                                            <?php
                                        endif;
                                    endif;
                                    ?>
                                </span>
                                <?php 
                                    $name = \App\User::where('user_id',$msg['sender_id'])->value('user_fname');
                                ?>

                                <a href="{{url('profile').'/'.$code}}" class="kt-chat__username">{{@$name}}</span></a>
                                <span class="kt-chat__datetime">{{$actualdate}} - {{$date1[1]}}</span>
                            </div>               
                            <div class="kt-chat__text kt-bg-light-success">
                                {{$msg['reply']}}
                            </div>
                        </div>
                    @endif    
                @endforeach
        </div>
    </div>
</div>
<div class="kt-portlet__foot">
    <div class="kt-chat__input">
        
            <div class="kt-chat__editor d-flex">                            
                <textarea id="input-text" disabled="true" style="height: 50px" placeholder="Type here..."></textarea>
                <div class="kt_chat__actions kt-ml-10 kt-mt-5">
                    <button type="button" id="input-button" class="btn btn-outline-primary btn-primary btn-circle btn-icon kt-chat__reply"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <rect x="0" y="0" width="24" height="24"></rect>
        <path d="M3,13.5 L19,12 L3,10.5 L3,3.7732928 C3,3.70255344 3.01501031,3.63261921 3.04403925,3.56811047 C3.15735832,3.3162903 3.45336217,3.20401298 3.70518234,3.31733205 L21.9867539,11.5440392 C22.098181,11.5941815 22.1873901,11.6833905 22.2375323,11.7948177 C22.3508514,12.0466378 22.2385741,12.3426417 21.9867539,12.4559608 L3.70518234,20.6826679 C3.64067359,20.7116969 3.57073936,20.7267072 3.5,20.7267072 C3.22385763,20.7267072 3,20.5028496 3,20.2267072 L3,13.5 Z" fill="#fff"></path>
    </g>
</svg></button>
                </div>                
            </div>
            <label class="error error-msg"></label>
    </div>
</div>


<!-- HTML Templates -->
    <script type="text/html" id="message-template">        

       <div class="kt-chat__message">
            <div class="kt-chat__user">
                <span class="kt-media kt-media--circle kt-media--sm">
                    <!-- <img src="{{ asset('') . config('app.public_url') . '/assets/media/users/default.jpg'}}" alt="image"> -->
                    <?php 
                        $name = $otheruserdata['user_fname'];
                        $lname = $otheruserdata['user_lname'];
                        $image = @$otheruserdata['image'];
                        $code = $otheruserdata['user_code'];
                        if(!empty($image)) :
                            $image = url('storage/app/') . '/' . $image;
                        else :
                            $name = strtoupper($rest = substr(@$name, 0, 1));
                            $lname = strtoupper($rest = substr(@$lname, 0, 1));
                        endif;
                        if(!empty($image)) :
                            ?>
                            <span class="kt-media kt-media--circle">
                                    <img src="{{$image}}" alt="image">
                            </span>
                            <?php
                        else :
                            
                            $image = asset('') . config('app.public_url') .'/assets/media/users/default.jpg';
                            
                            if(!empty($name)) :
                                ?>
                                <span class="kt-badge kt-badge--username kt-badge--unified-primary kt-badge--lg kt-badge--rounded kt-badge--bold">
                                {{$name}}{{$lname}}</span>
                                <?php
                            else :
                               ?>
                                
                                <span class="kt-media kt-media--circle">
                                        <img src="{{$image}}" alt="image">
                                </span>
                                <?php
                            endif;
                        endif;

                    ?>

                </span>
                <a href="{{url('profile').'/'.$code}}" data-content="username" class="kt-chat__username"></span></a>
                <span data-content="date" class="kt-chat__datetime"></span>
            </div>
            <div class="kt-chat__text kt-bg-light-success" data-content="body">
                
            </div>
        </div>
    </script>
    <!-- HTML Templates -->


    <script type="text/html" id="message-template-right">

        <div class="kt-chat__message kt-chat__message--right">
            <div class="kt-chat__user">
               <?php $code = Auth::user()->user_code; ?>
                <span data-content="date" class="kt-chat__datetime"></span>
                <a href="{{url('profile').'/'.$code}}" class="kt-chat__username"  data-content="username"></span></a>
                
                <span class="kt-media kt-media--circle kt-media--sm">
                    <!-- <img src="{{ asset('') . config('app.public_url') . '/assets/media/users/default.jpg'}}" alt="image"> -->

                     <?php 
                        $name = Auth::user()->user_fname;
                        $lname = Auth::user()->user_lname;
                        $image = @Auth::user()->image;
                        
                        if(!empty($image)) :
                            $image = url('storage/app/') . '/' . $image;
                        else :
                            $name = strtoupper($rest = substr(@$name, 0, 1));
                            $lname = strtoupper($rest = substr(@$lname, 0, 1));
                        endif;
                        if(!empty($image)) :
                            ?>
                            <span class="kt-media kt-media--circle">
                                    <img src="{{$image}}" alt="image">
                            </span>
                            <?php
                        else :
                            
                            $image = asset('') . config('app.public_url') .'/assets/media/users/default.jpg';
                            
                            if(!empty($name)) :
                                ?>
                                <span class="kt-badge kt-badge--username kt-badge--unified-primary kt-badge--lg kt-badge--rounded kt-badge--bold">
                                {{$name}}{{$lname}}</span>
                                <?php
                            else :
                               ?>
                                <span class="kt-media kt-media--circle">
                                    <img src="{{$image}}" alt="image">
                                </span>
                                <?php
                            endif;
                        endif;

                    ?>

                    

                </span>
            </div>
            <div class="kt-chat__text kt-bg-light-brand" data-content="body">
                
            </div>
        </div>

    </script>


