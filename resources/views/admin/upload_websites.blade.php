@extends('layout.app')
@section('title', 'Dashboard for Admin - GuestPostEngine')
@section('title-description')

<meta name="description" content="On the admin dashboard at GuestPostEngine, you, as an admin, can find all the information related to total orders, total live posts, and total Commission.">
    
@endsection
@section('content')
@section('mainhead')
@parent
    <!--begin::Page Vendors Styles(used by this page) -->
    <link href="{{ asset('') . config('app.public_url') . '/assets/css/pages/wizard/wizard-2.css' }}" rel="stylesheet" type="text/css" />
        <!--end::Page Vendors Styles -->
    <style type="text/css">
        label.error{
            color: red;
        }
    </style>
@show
<!-- end:: Header Mobile -->
 <div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor admin-dashboard-content" id="kt_content">

        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                
            </div>
        </div>

        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">
            <div class="row">
                <div class="col-12">
                    <!--begin:: Widgets/Activity-->
                    <div class="kt-portlet kt-portlet--fit kt-portlet--head-lg kt-portlet--head-overlay kt-portlet--skin-solid kt-portlet--height-fluid no-box-shadow">
                        <div class="kt-portlet__head kt-portlet__head--noborder kt-portlet__space-x">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Websites Upload
                                </h3>
                            </div>
                        </div>
                        <div class="form_upload">
							<p class="text-primary">Upload csv file for website listings</p>
                                <form class="kt-form" id="kt_form" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group row">
                                    <div class="col-6">
                                        <select name="user_id" id="user_id" class="form-control" required="">
                                            <?php 
                                                $user_list = \App\User::select('user_id','email','user_code')->get();
                                             ?>
                                            <option value="">Choose..</option>
                                            <?php 
                                                foreach ($user_list as $key => $value) :
                                                    echo '<option value="'.$value->user_id.'">@'.$value->user_code . '-' . $value->email.'</option>';
                                                endforeach;
                                            ?>
                                        </select>        
                                    </div>
                                </div>
                                <!-- <div class="row form-group">
                                    <div class="col-6">
                                        <input type="radio" name="file_type" checked="" class="file_type" value="link"> Link
                                        <input type="radio" name="file_type" class="file_type" value="file"> File
                                    </div>   
                                </div> -->
                                <div class="row form-group">
                                    <div class="col-6">
                                        <!-- <input type="file" name="upload_csv" id="upload_csv"  class="btn btn-primary" required style="display: none;" /> -->
                                        <input type="text" class="form-control" name="upload_link" id="upload_link" required  placeholder="Enter google sheet link after publish with csv." >
                                    </div>   
                                </div>
                                <div class="row form-group">
                                    <div class="form-control col-6">
                                        <button class="btn btn-label-brand btn-bold"  data-ktwizard-type="action-submit" type="submit" id="action_submit">
                                        Submit
                                            </button>
                                    </div>   
                                </div>
                                
							</form>
						</div>
                    </div>
                    <!--end:: Widgets/Activity-->
                </div>
            </div>
        </div>

        <!-- end:: Content -->
    </div>
</div>
    @section('mainscripts')
    @parent

    @section('customScript')
<script>
    
$(document).ready(function(){
    $('.file_type').on('click',function(e){
        if($(this).val() == 'file'){
            //$('#upload_csv').attr('type','file');
            $('#upload_csv').show();
            //$('#upload_link').attr('type','hidden');
            $('#upload_link').hide();
        } else {
            //$('#upload_csv').attr('type','hidden');
            $('#upload_csv').hide();
            //$('#upload_link').attr('type','text');
            $('#upload_link').show();
        }
    });
    $('#action_submit').on('click',function(e) {

            var formData = new FormData();
            e.preventDefault();

            var btn = $(this);
            var form = $(this).closest('form');

            form.validate({
                ignore: ":hidden",
                rules: {
                    user_id: {
                        required: true
                    },
                    upload_link: {
                        required: true
                    },
                    upload_csv: {
                        required: true
                    },
                },
                onkeyup: true,
                onfocusout: true,
                onclick: true,
            });

            if (!form.valid()) {
                return;
            }

            $('.loader').fadeIn('slow');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url  : "{{ url('/admin/upload_file') }}",
                method : "POST",
                data : $('#kt_form').serialize(),
                // contentType:false,
                // cache: false,
                // processData:false,
                success : function(response) {
                    $('.loader').fadeOut('slow');
                    if(response.status == false){
                        Swal.fire({
                          html: response.message,
                          icon: 'warning',
                          showCancelButton: true,
                          confirmButtonClass: 'btn btn-secondary',
                          cancelButtonClass: 'btn btn-secondary',
                          confirmButtonText: 'Ok'
                        }).then((result) => {
                            window.location.href = "{{url('admin/bulk_upload_websites')}}";
                        });
                    } else {
                        Swal.fire({
                          html: response.message,
                          icon: 'success',
                          showCancelButton: true,
                          confirmButtonClass: 'btn btn-secondary',
                          cancelButtonClass: 'btn btn-secondary',
                          confirmButtonText: 'Ok'
                        }).then((result) => {
                            window.location.href = "{{url('admin/bulk_upload_websites')}}";
                        });
                    }
                },
                error:function(error){
                    $('.loader').fadeOut('slow');
                    Swal.fire({
                      html: "Something went wrong. link is not valid.",
                      icon: 'danger',
                      showCancelButton: true,
                      confirmButtonClass: 'btn btn-secondary',
                      cancelButtonClass: 'btn btn-secondary',
                      confirmButtonText: 'Ok'
                    }).then((result) => {
                    });
                }
            });
        });
});

</script>
@endsection
    @show
@endsection
