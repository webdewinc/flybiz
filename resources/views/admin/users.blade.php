@extends('layout.app')
@section('title', 'See all registered users as a admin - GuestPostEngine')
@section('title-description')

<meta name="description" content="In a user section, admin can see who registered to GuestPostEngine marketplace with all their details which includes firstname, lastname, user type, email ID, and etc.">
    
@endsection
@section('content')
@section('mainhead')
@parent
<!--begin::Page Vendors Styles(used by this page) -->
<link href="{{ asset('') . config('app.public_url') . '/assets/css/pages/wizard/wizard-2.css'  }}" rel="stylesheet" type="text/css" />
<!--end::Page Vendors Styles -->
	@show
	<!-- end:: Aside -->
	<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
		<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
			<!-- begin:: Subheader -->
			<div class="kt-subheader   kt-grid__item" id="kt_subheader">
				<div class="kt-container ">	
				</div>
			</div>
			<!-- end:: Subheader -->
			<!-- begin:: Content -->
			<div class="kt-container  kt-grid__item kt-grid__item--fluid">
				<div class="alert alert-light alert-elevate" role="alert">
					<div class="kt-portlet__body w-100">
						<!--begin: Search Form -->
						<div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
							<div class="row align-items-center">
								<div class="col-xl-8 order-2 order-xl-1">
									<div class="row align-items-center">
										<div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
										<div class="kt-form__label">
												<label>Search:</label>
											</div>
											<div class="kt-input-icon kt-input-icon--left">
												<input type="text" class="form-control" placeholder="Search..." id="generalSearch">
												<span class="kt-input-icon__icon kt-input-icon__icon--left">
													<span><i class="la la-search"></i></span>
												</span>
											</div>
										</div>
										<div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
											
												<div class="kt-form__label">
													<label>Status:</label>
												</div>
												<div class="kt-form__control">
													<select class="form-control" id="kt_form_status">
														<option value="">All</option>
														<option value="2">Unverified</option>
														<option value="1">Verified</option>
													</select>
												</div>
											
										</div>
									</div>
								</div>
								<div class="col-xl-4 order-1 order-xl-2 kt-align-right">
									<a href="#" class="btn btn-default kt-hidden">
										<i class="la la-cart-plus"></i> New Order
									</a>
									<div class="kt-separator kt-separator--border-dashed kt-separator--space-lg d-xl-none"></div>
								</div>
							</div>
						</div>
						<!--end: Search Form -->
					</div>	
				</div>
				<div class="kt-portlet kt-portlet--mobile">
					<div class="kt-portlet__head kt-portlet__head--lg">
						<div class="kt-portlet__head-label">
							<span class="kt-portlet__head-icon">
								<i class="kt-font-brand la la-laptop"></i>
							</span>
							<h3 class="kt-portlet__head-title">
								Users
							</h3>
						</div>
						<div class="kt-portlet__head-toolbar">
							<div class="kt-portlet__head-wrapper">
								
								
							</div>
						</div>
					</div>
					
					<div class="kt-portlet__body kt-portlet__body--fit">
						<!--begin: Datatable -->
						<div class="kt-datatable" id="local_data"></div>
						<!--end: Datatable -->
					</div>
				</div>
			</div>
			<!-- end:: Content -->
		</div>
	</div>

		<!-- end:: Page -->
 

	<!-- begin::Scrolltop -->
	<div id="kt_scrolltop" class="kt-scrolltop">
		<i class="fa fa-arrow-up"></i>
	</div>

	<!-- end::Scrolltop -->
	<!--begin::Modal-->
    <div class="modal fade" id="kt_modal_delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    	<div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header  text-center">
                    <h5 class="modal-title" id="exampleModalLabel">Update Status</h5>
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon" data-dismiss="modal" aria-label="Close" style="cursor:pointer;">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#000000">
                                <rect x="0" y="7" width="16" height="2" rx="1"></rect>
                                <rect opacity="0.3" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000) " x="0" y="7" width="16" height="2" rx="1"></rect>
                            </g>
                        </g>
                    </svg>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <span class="form-text text-muted">Are you sure you want to Change the Status?</span>
                    </div>
                </div>
                <div class="modal-footer kt-login__actions">                          
                    <button type="submit" id="delete" class="btn btn-label-primary kt-mr-10 delete no">
                        {{ __('No') }}
                    </button>
                    <button type="submit" class="btn btn-primary delete yes">
                        {{ __('Yes') }}
                    </button>
                </div>
            </div>
    	</div>
	</div>
	<!--end::Modal-->
	@section('mainscripts')
    @parent
    @section('customScript')
	<script>
	
	'use strict';
	// Class definition
	
	var KTDatatableDataLocalDemo = function() {
		// Private functions
		var data = '<?php echo json_encode($user_lists); ?>';
		var split_data = JSON.parse(data);

        // var domain_id = split_data[0]['domain_id'];
		// demo initializer
		var demo = function() {
			// var dataJSONArray = JSON.parse('[{"RecordID":1,"Websiteurl":"www.website.com","Price":"$1","Orders":"1","Status":1,"Type":1,"Actions":null},\n' +
			// 	'{"RecordID":2,"Websiteurl":"www.website.com","Price":"$2","Orders":"2","Status":2,"Type":2,"Actions":null},\n' +
			// 	'{"RecordID":3,"Websiteurl":"www.website.com","Price":"$3","Orders":"3","Status":1,"Type":1,"Actions":null},\n' +
			// 	'{"RecordID":4,"Websiteurl":"www.website.com","Price":"$4","Orders":"4","Status":2,"Type":2,"Actions":null},\n' +
			// 	'{"RecordID":1,"Websiteurl":"www.website.com","Price":"$1","Orders":"1","Status":1,"Type":1,"Actions":null},\n' +
			// 	'{"RecordID":2,"Websiteurl":"www.website.com","Price":"$2","Orders":"2","Status":2,"Type":2,"Actions":null},\n' +
			// 	'{"RecordID":3,"Websiteurl":"www.website.com","Price":"$3","Orders":"3","Status":1,"Type":1,"Actions":null},\n' +
			// 	'{"RecordID":2,"Websiteurl":"www.website.com","Price":"$2","Orders":"2","Status":2,"Type":2,"Actions":null},\n' +
			// 	'{"RecordID":3,"Websiteurl":"www.website.com","Price":"$3","Orders":"3","Status":1,"Type":1,"Actions":null},\n' +
			// 	'{"RecordID":2,"Websiteurl":"www.website.com","Price":"$2","Orders":"2","Status":2,"Type":2,"Actions":null},\n' +
			// 	'{"RecordID":3,"Websiteurl":"www.website.com","Price":"$3","Orders":"3","Status":1,"Type":1,"Actions":null},\n' +
			// 	'{"RecordID":4,"Websiteurl":"www.website.com","Price":"$4","Orders":"4","Status":2,"Type":2,"Actions":null}]');
			 
			var dataJSONArray = split_data;
			// var updateData    = dataJSONArray.toString();
			// console.log('Below Data '+updateData);
			var datatable 	   = $('.kt-datatable').KTDatatable({
				// datasource definition
				data: {
					type: 'local',
					source: dataJSONArray,
					pageSize: 10,
				},
	
				// layout definition
				layout: {
					scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
					// height: 450, // datatable's body's fixed height
					footer: false, // display/hide footer
				},
	
				// column sorting
				sortable: true,
	
				pagination: true,
	
				search: {
					input: $('#generalSearch'),
				},
	
				// columns definition
				columns: [
					{
						field: 'user_id',
						title: '#',
						sortable: true,
						width: 20,
						type: 'number',
						selector: {class: 'kt-checkbox--solid'},
						textAlign: 'center',
					},
					 {
						field: 'user_code',
						title: 'User Code',
						width: 300,
					}, 
					{
						field: 'user_fname',
						title: 'Firstname',				
					}, {
						field: 'user_lname',
						title: 'Lastname',				
					},
					{
						field: 'user_type',
						title :'Type',
					},
					{
						field: 'email',
						title: 'User Email',
						width: 300,
					},
					{
						field: 'user_created_time',
						title: 'Sign up Date',
					},
					{
						field: 'user_status',
						title: 'Status',
						// callback function support for column rendering
						template: function(row) {
							var status = {
								2: {'title': 'Unverified', 'class': 'kt-badge--warning'},
								1: {'title': 'Verified', 'class': ' kt-badge--brand'}
							};
							return '<span class="kt-badge ' + status[row.user_status].class + ' kt-badge--inline kt-badge--pill">' + status[row.user_status].title + '</span>';
						},
					}, 
					{
						field: 'Actions',
						title: 'Actions',
						sortable: false,
						width: 110,
						overflow: 'visible',
						autoHide: false,
						template: function(row) {
							return '\<div class="dropdown"><a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="false"><i class="la la-cog"></i></a> <div class="dropdown-menu dropdown-menu-right" style="display: none;"> <a class="dropdown-item status_active" href="javascript:void(0);" data-status="1" rel="'+row.user_id+'">Active</a> <a class="dropdown-item status_active" href="javascript:void(0);" data-status="0" rel="'+row.user_id+'">Inactive</a>    </div></div>\
						';
						}, 

					}],
			});
	
			$('#kt_form_status').on('change', function() {
				datatable.search($(this).val().toLowerCase(), 'user_status');
			});
	
			$('#kt_form_type').on('change', function() {
				datatable.search($(this).val().toLowerCase(), 'Type');
			});
	
			$('#kt_form_status,#kt_form_type').selectpicker();
	
		};
	
		return {
			// Public functions
			init: function() {
				// init dmeo
				demo();
			},
		};
	}();
	
	jQuery(document).ready(function() {
		KTDatatableDataLocalDemo.init();
	});
	</script>
	<script>
    $(function() {
        $(document).on('click','.status_active',function(){
            $('.delete.yes').attr('rel',$(this).attr('rel'));
            $('.delete.yes').attr('data-status',$(this).attr('data-status'));
            $('#kt_modal_delete').modal('show');
        });
        $(document).on('click','.delete.yes',function(e){
            e.preventDefault();
            $('#kt_modal_delete').modal('hide');
            

            $.ajax({
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                url  : "{{url('/admin/updateUserStatus')}}",
                type: 'POST',
                data: {
                	id 		: $(this).attr('rel'),
                	status  : $(this).attr('data-status'),
                },                        
                async: true,
                dataType: 'json',
                enctype: 'multipart/form-data',
                cache: false,                      
                beforeSend: function() {
                    //something before send
                    $(".loader").fadeIn("slow");
                },
                success: function(data) {
                    //console.log('Data: '+data);
                    if(data.alert == 'success'){
                        $(".loader").fadeOut("slow");
                        swal.fire({
                            "title": "",
                            "text": data.message,
                            "type": "success",
                            "confirmButtonClass": "btn btn-secondary"
                        });
                    } else {
                        $(".loader").fadeOut("slow");
                        swal.fire({
                            "title": "",
                            "text": data.message,
                            "type": "success",
                            "confirmButtonClass": "btn btn-secondary"
                        });
                    }                            
                    window.location.href = "{{ url('/admin/users') }}";
                },
                error: function(xhr,textStatus,thrownError) {
                    //alert(xhr + "\n" + textStatus + "\n" + thrownError);
                    $(".loader").fadeOut("slow");
                    $('#kt_modal_success .form-text.text-muted').html(xhr + "\n" + textStatus + "\n" + thrownError);
                    $('#kt_modal_success').modal('show');
                }
            });
            e.stopImmediatePropagation();
            return false;
        });
        $(document).on('click','.delete.no',function(){
            $('#kt_modal_delete').modal('hide');
            e.stopImmediatePropagation();
            return false;
        });
    });
    </script>
		<!--end::Page Scripts -->
	@endsection
	@show
@endsection