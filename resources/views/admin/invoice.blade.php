@extends('layout.app')
@section('title', 'Invoice Page - GuestPostEngine')

@section('title-description')

<meta name="description" content="In the Invoice page, an advertiser and a publisher can see all the details of his/her order regarding purchasing a website which includes website name, price of the website, date of order placed, order ID and so on.">
    
@endsection
@section('content')
@section('mainhead')
@parent
    <!--begin::Page Vendors Styles(used by this page) -->
    <link href="{{ asset('') . config('app.public_url') . '/assets/css/pages/wizard/wizard-2.css'  }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('') . config('app.public_url') . '/assets/css/pages/invoices/invoice-2.css'  }}" rel="stylesheet" type="text/css" />
        <!--end::Page Vendors Styles -->
@show
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
            </div>
        </div>
        <!-- end:: Subheader -->
        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">
            <div class="kt-portlet">
                <div class="kt-portlet__body kt-portlet__body--fit">
                    <div class="kt-invoice-2">
                        <!-- code for order-invoice start -->
                         <div class="kt-invoice__head">
                            <div class="kt-invoice__container">
                                <div class="kt-invoice__brand">
                                    <h1 class="kt-invoice__title">INVOICE(Order)</h1>
                                </div>
                                <div class="kt-invoice__items">
                                    <div class="kt-invoice__item">
                                        <span class="kt-invoice__subtitle">INVOICE DATE</span>
                                        <span class="kt-invoice__text">{{ date('Y-m-d', strtotime($invoiceData['created_at'])) }}</span>
                                    </div>
                                    <div class="kt-invoice__item">
                                        <span class="kt-invoice__subtitle">INVOICE NO.</span>
                                        <span class="kt-invoice__text">{{ '#'.$invoiceData['order_id']}}</span>
                                    </div>
                                    @if($orderData['payment_type'] == 'paypal')
                                    <div class="kt-invoice__item">
                                        <span class="kt-invoice__subtitle">INVOICE TO.</span>
                                        <span class="kt-invoice__text">{{ $orderData['paypal_address'] }}</span>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="kt-invoice__body">
                            <div class="kt-invoice__container">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Domain</th>
                                                <th>Advertiser</th>
                                                <th style="text-align:center;">Merchant ID</th>
                                                @if($orderData['payment_type'] == 'paypal')
                                                <th>Paypal Email</th>
                                                @endif
                                                <th>Status</th>
                                                <th>Amount Paid</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                            $total = 0;
                                            @endphp
                                            @foreach( $invoicearr as $split_invoicearr )
                                            @php
                                                $total  = $total + $split_invoicearr['amount'];
                                            @endphp
                                            <tr>
                                                <td>{{ @$split_invoicearr['domain_url'] }}</td>
                                                <td>{{ @$split_invoicearr['advertiser_fname'] }}</td>
                                                @if($split_invoicearr['payment_type'] == 'paypal')
                                                <td>{{ @$split_invoicearr['paypal_merchant_id'] }}</td>
                                                <td>{{ @$split_invoicearr['paypal_email'] }}</td>
                                                @elseif($split_invoicearr['payment_type'] == 'stripe')
                                                <td></td>
                                                <td>{{ @$split_invoicearr['stripe_merchant_id'] }}</td>
                                                @elseif($split_invoicearr['payment_type'] == 'manually')
                                                <td>Manually</td>
                                                @endif
                                                @if($split_invoicearr['paypal_status'] == 1 || $split_invoicearr['payment_type'] == 'paypal')
                                                <td>Success</td>
                                                @endif
                                                @if($split_invoicearr['payment_type'] == 'stripe' || $split_invoicearr['stripe_status'] == 'succeeded')
                                                <td>Success</td>
                                                @endif
                                                <td class="kt-font-success kt-font-lg">${{ @$split_invoicearr['amount'] }}.00</td>
                                            </tr>
                                            @endforeach
                                            <tr colspan="5">
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td>Total</td>
                                                <td>${{ $total }}.00</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- end here code for order-invoice -->
                        @if(!empty($invoiceData['merchant_id']))
                        <!-- code for pay to seller start here  -->
                        <div class="kt-invoice__head">
                            <div class="kt-invoice__container">
                                <div class="kt-invoice__brand">
                                    <h1 class="kt-invoice__title">INVOICE(Pay to Advertiser)</h1>
                                </div>
                                <div class="kt-invoice__items">
                                    <div class="kt-invoice__item">
                                        <span class="kt-invoice__subtitle">INVOICE DATE</span>
                                        <span class="kt-invoice__text">{{ substr($invoiceData['created_at'],0,10) }}</span>
                                    </div>
                                    <div class="kt-invoice__item">
                                        <span class="kt-invoice__subtitle">INVOICE NO.</span>
                                        <span class="kt-invoice__text">{{ '#'.$invoiceData['order_id']}}</span>
                                    </div>
                                    <div class="kt-invoice__item">
                                        <span class="kt-invoice__subtitle">INVOICE TO.</span>
                                        <span class="kt-invoice__text">{{ $invoiceData['publisher_fname'].' '.$invoiceData['publisher_lname'] }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="kt-invoice__body">
                            <div class="kt-invoice__container">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Domain</th>
                                                <th>Publisher</th>
                                                <th style="text-align:center;">Account Email</th>
                                                <th>Status</th>
                                                <th>Amount Paid</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{{ $invoiceData['domain_url'] }}</td>
                                                <td>{{ $invoiceData['publisher_fname']}}</td>
                                                <td>{{ $invoiceData['publisher_paypal_email']}}</td>
                                                <td>{{ $invoiceData['invoice_status']}}</td>
                                                <td class="kt-font-success kt-font-lg">${{ $invoiceData['amount']}}.00</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="kt-invoice__footer">
                            <div class="kt-invoice__container">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Co-RelationID</th>
                                                <th>Account ID</th>
                                                <th>Seller Account</th>
                                                <th>Transaction Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{{ $invoiceData['correlationId'] }}</td>
                                                <td>{{ $invoiceData['accountId'] }}</td>
                                                <td>{{ $invoiceData['seller_accountId'] }}</td>
                                                <td class="kt-font-success kt-font-xl kt-font-boldest">{{ $invoiceData['senderTransactionStatus'] }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        @elseif($invoiceData['order_status'] == '2')
                        <div class="kt-invoice__head">
                            <div class="kt-invoice__container">
                                <div class="kt-invoice__brand">
                                    <h1 class="kt-invoice__title">INVOICE(Pay to Advertiser) - <span class="text-danger">Order Rejected</span></h1>
                                </div>
                            </div>
                        </div>
                         <div class="kt-invoice__body">
                            <div class="kt-invoice__container">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Domain</th>
                                                <th>Publisher</th>
                                                <th style="text-align:center;">Account Email</th>
                                                <th>Status</th>
                                                <th>Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach( $invoicearr as $split_invoicearr )
                                            @php
                                            $refundTotal  = $total - $split_invoicearr['amount'];
                                            @endphp
                                                @if( $split_invoicearr['order_status'] == '2' )
                                                <tr>
                                                    <td>{{ @$split_invoicearr['domain_url'] }}</td>
                                                    <td>{{ $invoiceData['publisher_fname']}}</td>
                                                    <td>{{ $invoiceData['publisher_paypal_email']}}</td>
                                                    <td class="text-danger">Refunded</td>                                                
                                                    <td class="kt-font-success kt-font-lg">-${{ @$split_invoicearr['amount'] }}.00</td>
                                                </tr>
                                                @endif
                                            @endforeach
                                            <tr> 
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td>Total</td>
                                                <td>${{ $refundTotal }}.00</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        @else 
                        <div class="kt-invoice__head">
                            <div class="kt-invoice__container">
                                <div class="kt-invoice__brand">
                                    <h1 class="kt-invoice__title">INVOICE(Pay to Advertiser) - <span class="text-danger">NOT PAID YET</span></h1>
                                </div>
                            </div>
                        </div>
                        @endif
                        <!-- end here code for pay to seller -->
                        <div class="kt-invoice__actions">
                            <div class="kt-invoice__container">
                                <button type="button" class="btn btn-label-brand btn-bold" onclick="window.print();">Download Invoice</button>
                                <button type="button" class="btn btn-brand btn-bold" onclick="window.print();">Print Invoice</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end:: Content -->
</div>       
@section('mainscripts')
@parent
    <!--begin::Page Scripts(used by this page) -->
@show
@endsection