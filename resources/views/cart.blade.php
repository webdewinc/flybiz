@extends('layout.app')
@section('title', 'Add to Cart - GuestPostEngine')
@section('content')
@section('mainhead')
@section('title-description')

<meta name="description" content="At GuestPostEngine marketplace, add to cart section manifest you about how many websites are you keen to buy at a particular time. Feel free to append your website for future use.">
    
@endsection
@parent
    <!--begin::Page Vendors Styles(used by this page) -->
    <link href="{{ asset( 'public/assets/css/owl.carousel.min.css' ) }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset( 'public/assets/css/pages/pricing/pricing-2.css' ) }}" rel="stylesheet" type="text/css" />
        <!--end::Page Vendors Styles -->
@show
<div class="kt-container  kt-grid__item kt-grid__item--fluid cart-container">
                            <div class="kt-subheader   kt-grid__item" id="kt_subheader">
                            </div>
                            <div class="row">
                                <div class="col-md-8 col-12">
                                    <div class="row">
                                        <!--begin:: Portlet-->
                                        <div class="kt-portlet">
                                            <div class="kt-portlet__body">
                                                <div class="kt-widget kt-widget--user-profile-3">
                                                    <div class="kt-widget__top">
                                                        <div class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-font-light kt-hidden">
                                                            JM
                                                        </div>
                                                        <div class="kt-widget__content">
                                                            <div class="kt-widget__head">
                                                                <div class="d-flex align-items-center flex-wrap">
                                                                    <div class="kt-widget__media kt-hidden-">
                                                                        <img src="https://d1gwm4cf8hecp4.cloudfront.net/images/favicons/favicon-16x16.png" alt="image">
                                                                    </div>
                                                                    <a href="#" class="kt-widget__username">
                                                                        asana.com
                                                                        <i class="flaticon2-correct kt-font-primary"></i>
                                                                    </a>&nbsp;
                                                                    <span><span class="kt-badge kt-badge--success kt-badge--dot"></span>&nbsp;<span class="kt-font-bold kt-font-success">Do Follow</span></span>
</div>
<div class="kt-widget__info">                                                              
<div class="kt-widget__action d-flex align-items-center">
<div class="website-price" style="font-size: 22px;">$123</div>&nbsp; &nbsp;

<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon" style="
width: 23px;
">
<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
<g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#000000">
<rect x="0" y="7" width="16" height="2" rx="1"></rect>
<rect opacity="0.3" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000) " x="0" y="7" width="16" height="2" rx="1"></rect>
</g>
</g>
</svg>
</div>
</div>
                                                             
                                                            </div>
                                                         
                                                           
                                                        </div>
                                                    </div>
                                                    <div class="kt-widget__bottom">
                                                        <div class="kt-widget__item">
                                                            <div class="kt-widget__icon">
<svg class="moz-svg" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 280.6 280.3" style="enable-background:new 0 0 280.6 280.3;" width="30px" height="30px" xml:space="preserve">
<path class="st0" d="M140.2,280.3C66.1,282.2-0.6,217.8,0,138.9C0.6,64.8,64.4-2.1,144.5,0c71.7,1.9,136.8,63.8,136.1,141.7
C280,218.2,214.6,282.3,140.2,280.3z M185.1,139.7c0.4,0.1,0.7,0.3,1.1,0.4c0,13.1,0.1,26.2-0.1,39.3c-0.1,4.7,2.1,6.7,6.6,6.6
c6.3-0.1,12.7-0.1,19,0c2.5,0,3.6-0.7,3.5-3.4c-0.1-28.3-0.1-56.6,0-85c0-2.8-1.2-3.3-3.6-3.3c-5.2,0.1-10.3,0.1-15.5,0
c-6.6-0.1-11.8,2.3-16.2,7.3c-12,13.6-24.3,27-36.3,40.6c-2.6,2.9-4,3.3-6.8,0.1c-12.1-13.8-24.5-27.3-36.6-41
c-3.6-4.1-7.9-6.7-13.3-6.9c-6.2-0.3-12.3,0-18.5-0.1c-2.4,0-3.2,0.8-3.2,3.2c0.1,28.3,0.1,56.6,0,85c0,2.6,0.8,3.5,3.4,3.5
c6.3-0.1,12.7,0,19-0.1c5.2-0.1,6.6-1.7,6.6-7.1c0-11.8,0-23.7,0.1-35.5c0-1.2-0.8-2.6,1-3.7c12.9,14.4,25.8,28.7,38.6,43.1
c4.2,4.7,8,4.8,12.4-0.1C159.4,168.4,172.3,154.1,185.1,139.7z"></path>
<path class="st1" d="M185.1,139.7c-12.9,14.4-25.8,28.7-38.7,43.1c-4.3,4.8-8.2,4.8-12.4,0.1c-12.9-14.4-25.8-28.7-38.6-43.1
c-1.8,1-1,2.5-1,3.7c-0.1,11.8,0,23.7-0.1,35.5c0,5.4-1.4,7-6.6,7.1c-6.3,0.1-12.7-0.1-19,0.1c-2.6,0.1-3.4-0.9-3.4-3.5
c0.1-28.3,0.1-56.6,0-85c0-2.4,0.8-3.2,3.2-3.2c6.2,0.1,12.3-0.2,18.5,0.1c5.4,0.2,9.7,2.8,13.3,6.9c12.2,13.7,24.6,27.2,36.6,41
c2.8,3.2,4.2,2.8,6.8-0.1c12-13.6,24.3-27,36.3-40.6c4.4-5,9.7-7.4,16.2-7.3c5.2,0.1,10.3,0.1,15.5,0c2.4-0.1,3.6,0.4,3.6,3.3
c-0.1,28.3-0.1,56.6,0,85c0,2.7-1.1,3.4-3.5,3.4c-6.3-0.1-12.7-0.1-19,0c-4.6,0.1-6.7-1.9-6.6-6.6c0.1-13.1,0.1-26.2,0.1-39.3
C185.9,140,185.5,139.8,185.1,139.7z"></path>
</svg>
                                                            </div>
                                                            <div class="kt-widget__details">
                                                             
                                                                <div class="d-flex"><span class="kt-widget__value">29</span>
<span class="d-flex align-items-end">/100</span></div>
<span class="kt-widget__title">Domain Authority</span>
                                                            </div>
                                                        </div>
                                                        <div class="kt-widget__item">
                                                            <div class="kt-widget__icon">
<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="34px" height="34px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
<rect x="0" y="0" width="24" height="24"></rect>
<path d="M13,18.9450712 L13,20 L14,20 C15.1045695,20 16,20.8954305 16,22 L8,22 C8,20.8954305 8.8954305,20 10,20 L11,20 L11,18.9448245 C9.02872877,18.7261967 7.20827378,17.866394 5.79372555,16.5182701 L4.73856106,17.6741866 C4.36621808,18.0820826 3.73370941,18.110904 3.32581341,17.7385611 C2.9179174,17.3662181 2.88909597,16.7337094 3.26143894,16.3258134 L5.04940685,14.367122 C5.46150313,13.9156769 6.17860937,13.9363085 6.56406875,14.4106998 C7.88623094,16.037907 9.86320756,17 12,17 C15.8659932,17 19,13.8659932 19,10 C19,7.73468744 17.9175842,5.65198725 16.1214335,4.34123851 C15.6753081,4.01567657 15.5775721,3.39010038 15.903134,2.94397499 C16.228696,2.49784959 16.8542722,2.4001136 17.3003976,2.72567554 C19.6071362,4.40902808 21,7.08906798 21,10 C21,14.6325537 17.4999505,18.4476269 13,18.9450712 Z" fill="#000000" fill-rule="nonzero"></path>
<circle fill="#000000" opacity="0.3" cx="12" cy="10" r="6"></circle>
</g>
</svg>
                                                            </div>
                                                            <div class="kt-widget__details">
                                                               
<span class="kt-widget__value">3,185,623</span>
<span class="kt-widget__title">Global Rank</span>
                                                            </div>
                                                        </div>
                                                        <div class="kt-widget__item">
                                                            <div class="kt-widget__icon">
<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
<rect x="0" y="0" width="24" height="24"></rect>
<path d="M4.00246329,12.2004927 L13,14 L13,4.06189375 C16.9463116,4.55399184 20,7.92038235 20,12 C20,16.418278 16.418278,20 12,20 C7.64874861,20 4.10886412,16.5261253 4.00246329,12.2004927 Z" fill="#000000" opacity="0.3"></path>
<path d="M3.0603968,10.0120794 C3.54712466,6.05992157 6.91622084,3 11,3 L11,11.6 L3.0603968,10.0120794 Z" fill="#000000"></path>
</g>
</svg>
                                                            </div>
                                                            <div class="kt-widget__details">
                                                               
<span class="kt-widget__value">2.08K</span>
<span class="kt-widget__title">Total Traffic</span>
                                                            </div>
                                                        </div>
                                                    <div class="kt-widget__item">
                                                            <div class="kt-widget__icon">
                                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
<polygon points="0 0 24 0 24 24 0 24"></polygon>
<path d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z" fill="#000000" fill-rule="nonzero"></path>
<path d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z" fill="#000000" opacity="0.3"></path>
</g>
</svg>
                                                            </div>
                                                            <div class="kt-widget__details">
                                                               
<span class="kt-widget__value">2.08K</span>
<span class="kt-widget__title">Orders in Queue</span>
                                                            </div>
                                                        </div>                                                      
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
<!--end:: Portlet-->
<!--begin:: Portlet-->
<div class="kt-portlet">
                                            <div class="kt-portlet__body">
                                                <div class="kt-widget kt-widget--user-profile-3">
                                                    <div class="kt-widget__top">
                                                        <div class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-font-light kt-hidden">
                                                            JM
                                                        </div>
                                                        <div class="kt-widget__content">
                                                            <div class="kt-widget__head">
                                                                <div class="d-flex align-items-center flex-wrap">
                                                                    <div class="kt-widget__media kt-hidden-">
                                                                        <img src="https://d1gwm4cf8hecp4.cloudfront.net/images/favicons/favicon-16x16.png" alt="image">
                                                                    </div>
                                                                    <a href="#" class="kt-widget__username">
                                                                        asana.com
                                                                        <i class="flaticon2-correct kt-font-primary"></i>
                                                                    </a>&nbsp;
                                                                    <span><span class="kt-badge kt-badge--success kt-badge--dot"></span>&nbsp;<span class="kt-font-bold kt-font-success">Do Follow</span></span>
</div>
<div class="kt-widget__info">                                                              
<div class="kt-widget__action d-flex align-items-center">
<div class="website-price" style="font-size: 22px;">$123</div>&nbsp; &nbsp;

<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon" style="
width: 23px;
">
<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
<g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#000000">
<rect x="0" y="7" width="16" height="2" rx="1"></rect>
<rect opacity="0.3" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000) " x="0" y="7" width="16" height="2" rx="1"></rect>
</g>
</g>
</svg>
</div>
</div>
                                                             
                                                            </div>
                                                         
                                                           
                                                        </div>
                                                    </div>
                                                    <div class="kt-widget__bottom">
                                                        <div class="kt-widget__item">
                                                            <div class="kt-widget__icon">
<svg class="moz-svg" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 280.6 280.3" style="enable-background:new 0 0 280.6 280.3;" width="30px" height="30px" xml:space="preserve">
<path class="st0" d="M140.2,280.3C66.1,282.2-0.6,217.8,0,138.9C0.6,64.8,64.4-2.1,144.5,0c71.7,1.9,136.8,63.8,136.1,141.7
C280,218.2,214.6,282.3,140.2,280.3z M185.1,139.7c0.4,0.1,0.7,0.3,1.1,0.4c0,13.1,0.1,26.2-0.1,39.3c-0.1,4.7,2.1,6.7,6.6,6.6
c6.3-0.1,12.7-0.1,19,0c2.5,0,3.6-0.7,3.5-3.4c-0.1-28.3-0.1-56.6,0-85c0-2.8-1.2-3.3-3.6-3.3c-5.2,0.1-10.3,0.1-15.5,0
c-6.6-0.1-11.8,2.3-16.2,7.3c-12,13.6-24.3,27-36.3,40.6c-2.6,2.9-4,3.3-6.8,0.1c-12.1-13.8-24.5-27.3-36.6-41
c-3.6-4.1-7.9-6.7-13.3-6.9c-6.2-0.3-12.3,0-18.5-0.1c-2.4,0-3.2,0.8-3.2,3.2c0.1,28.3,0.1,56.6,0,85c0,2.6,0.8,3.5,3.4,3.5
c6.3-0.1,12.7,0,19-0.1c5.2-0.1,6.6-1.7,6.6-7.1c0-11.8,0-23.7,0.1-35.5c0-1.2-0.8-2.6,1-3.7c12.9,14.4,25.8,28.7,38.6,43.1
c4.2,4.7,8,4.8,12.4-0.1C159.4,168.4,172.3,154.1,185.1,139.7z"></path>
<path class="st1" d="M185.1,139.7c-12.9,14.4-25.8,28.7-38.7,43.1c-4.3,4.8-8.2,4.8-12.4,0.1c-12.9-14.4-25.8-28.7-38.6-43.1
c-1.8,1-1,2.5-1,3.7c-0.1,11.8,0,23.7-0.1,35.5c0,5.4-1.4,7-6.6,7.1c-6.3,0.1-12.7-0.1-19,0.1c-2.6,0.1-3.4-0.9-3.4-3.5
c0.1-28.3,0.1-56.6,0-85c0-2.4,0.8-3.2,3.2-3.2c6.2,0.1,12.3-0.2,18.5,0.1c5.4,0.2,9.7,2.8,13.3,6.9c12.2,13.7,24.6,27.2,36.6,41
c2.8,3.2,4.2,2.8,6.8-0.1c12-13.6,24.3-27,36.3-40.6c4.4-5,9.7-7.4,16.2-7.3c5.2,0.1,10.3,0.1,15.5,0c2.4-0.1,3.6,0.4,3.6,3.3
c-0.1,28.3-0.1,56.6,0,85c0,2.7-1.1,3.4-3.5,3.4c-6.3-0.1-12.7-0.1-19,0c-4.6,0.1-6.7-1.9-6.6-6.6c0.1-13.1,0.1-26.2,0.1-39.3
C185.9,140,185.5,139.8,185.1,139.7z"></path>
</svg>
                                                            </div>
                                                            <div class="kt-widget__details">
                                                             
                                                                <div class="d-flex"><span class="kt-widget__value">29</span>
<span class="d-flex align-items-end">/100</span></div>
<span class="kt-widget__title">Domain Authority</span>
                                                            </div>
                                                        </div>
                                                        <div class="kt-widget__item">
                                                            <div class="kt-widget__icon">
<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="34px" height="34px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
<rect x="0" y="0" width="24" height="24"></rect>
<path d="M13,18.9450712 L13,20 L14,20 C15.1045695,20 16,20.8954305 16,22 L8,22 C8,20.8954305 8.8954305,20 10,20 L11,20 L11,18.9448245 C9.02872877,18.7261967 7.20827378,17.866394 5.79372555,16.5182701 L4.73856106,17.6741866 C4.36621808,18.0820826 3.73370941,18.110904 3.32581341,17.7385611 C2.9179174,17.3662181 2.88909597,16.7337094 3.26143894,16.3258134 L5.04940685,14.367122 C5.46150313,13.9156769 6.17860937,13.9363085 6.56406875,14.4106998 C7.88623094,16.037907 9.86320756,17 12,17 C15.8659932,17 19,13.8659932 19,10 C19,7.73468744 17.9175842,5.65198725 16.1214335,4.34123851 C15.6753081,4.01567657 15.5775721,3.39010038 15.903134,2.94397499 C16.228696,2.49784959 16.8542722,2.4001136 17.3003976,2.72567554 C19.6071362,4.40902808 21,7.08906798 21,10 C21,14.6325537 17.4999505,18.4476269 13,18.9450712 Z" fill="#000000" fill-rule="nonzero"></path>
<circle fill="#000000" opacity="0.3" cx="12" cy="10" r="6"></circle>
</g>
</svg>
                                                            </div>
                                                            <div class="kt-widget__details">
                                                               
<span class="kt-widget__value">3,185,623</span>
<span class="kt-widget__title">Global Rank</span>
                                                            </div>
                                                        </div>
                                                        <div class="kt-widget__item">
                                                            <div class="kt-widget__icon">
<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
<rect x="0" y="0" width="24" height="24"></rect>
<path d="M4.00246329,12.2004927 L13,14 L13,4.06189375 C16.9463116,4.55399184 20,7.92038235 20,12 C20,16.418278 16.418278,20 12,20 C7.64874861,20 4.10886412,16.5261253 4.00246329,12.2004927 Z" fill="#000000" opacity="0.3"></path>
<path d="M3.0603968,10.0120794 C3.54712466,6.05992157 6.91622084,3 11,3 L11,11.6 L3.0603968,10.0120794 Z" fill="#000000"></path>
</g>
</svg>
                                                            </div>
                                                            <div class="kt-widget__details">
                                                               
<span class="kt-widget__value">2.08K</span>
<span class="kt-widget__title">Total Traffic</span>
                                                            </div>
                                                        </div>
                                                    <div class="kt-widget__item">
                                                            <div class="kt-widget__icon">
                                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
<polygon points="0 0 24 0 24 24 0 24"></polygon>
<path d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z" fill="#000000" fill-rule="nonzero"></path>
<path d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z" fill="#000000" opacity="0.3"></path>
</g>
</svg>
                                                            </div>
                                                            <div class="kt-widget__details">
                                                               
<span class="kt-widget__value">2.08K</span>
<span class="kt-widget__title">Orders in Queue</span>
                                                            </div>
                                                        </div>                                                      
                                                       </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end:: Portlet-->
                                     
                                       
                                    </div>
                                 
                                </div>
                                <div class="col-md-4 col-12">
                                    <div class="fixed-sidebar">
                                        <section>
                                            <div class="kt-portlet kt-portlet--tabs kt-portlet--height-fluid">
                                                <div class="kt-portlet__head">
                                                    <div class="kt-portlet__head-label">
                                                        <h3 class="kt-portlet__head-title">
                                                            Order Details
                                                        </h3>
                                                    </div>
                                                </div>
                                                <div class="kt-portlet__body">
<div class="kt-notification kt-mb-20">
<a href="#" class="kt-notification__item">
<div class="kt-notification__item-icon">
<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
<rect x="0" y="0" width="24" height="24"/>
<path d="M12,4.56204994 L7.76822128,9.6401844 C7.4146572,10.0644613 6.7840925,10.1217854 6.3598156,9.76822128 C5.9355387,9.4146572 5.87821464,8.7840925 6.23177872,8.3598156 L11.2317787,2.3598156 C11.6315738,1.88006147 12.3684262,1.88006147 12.7682213,2.3598156 L17.7682213,8.3598156 C18.1217854,8.7840925 18.0644613,9.4146572 17.6401844,9.76822128 C17.2159075,10.1217854 16.5853428,10.0644613 16.2317787,9.6401844 L12,4.56204994 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
<path d="M3.28077641,9 L20.7192236,9 C21.2715083,9 21.7192236,9.44771525 21.7192236,10 C21.7192236,10.0817618 21.7091962,10.163215 21.6893661,10.2425356 L19.5680983,18.7276069 C19.234223,20.0631079 18.0342737,21 16.6576708,21 L7.34232922,21 C5.96572629,21 4.76577697,20.0631079 4.43190172,18.7276069 L2.31063391,10.2425356 C2.17668518,9.70674072 2.50244587,9.16380623 3.03824078,9.0298575 C3.11756139,9.01002735 3.1990146,9 3.28077641,9 Z M12,12 C11.4477153,12 11,12.4477153 11,13 L11,17 C11,17.5522847 11.4477153,18 12,18 C12.5522847,18 13,17.5522847 13,17 L13,13 C13,12.4477153 12.5522847,12 12,12 Z M6.96472382,12.1362967 C6.43125772,12.2792385 6.11467523,12.8275755 6.25761704,13.3610416 L7.29289322,17.2247449 C7.43583503,17.758211 7.98417199,18.0747935 8.51763809,17.9318517 C9.05110419,17.7889098 9.36768668,17.2405729 9.22474487,16.7071068 L8.18946869,12.8434035 C8.04652688,12.3099374 7.49818992,11.9933549 6.96472382,12.1362967 Z M17.0352762,12.1362967 C16.5018101,11.9933549 15.9534731,12.3099374 15.8105313,12.8434035 L14.7752551,16.7071068 C14.6323133,17.2405729 14.9488958,17.7889098 15.4823619,17.9318517 C16.015828,18.0747935 16.564165,17.758211 16.7071068,17.2247449 L17.742383,13.3610416 C17.8853248,12.8275755 17.5687423,12.2792385 17.0352762,12.1362967 Z" fill="#000000"/>
</g>
</svg></div>
<div class="kt-notification__item-details flex-row justify-content-between">
<div class="kt-notification__item-title">Total Order</div>
<div class="kt-notification__item-time">2</div>
</div>
</a>
<a href="#" class="kt-notification__item">
<div class="kt-notification__item-icon">
<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
<rect x="0" y="0" width="24" height="24"/>
<rect fill="#000000" opacity="0.3" x="11.5" y="2" width="2" height="4" rx="1"/>
<rect fill="#000000" opacity="0.3" x="11.5" y="16" width="2" height="5" rx="1"/>
<path d="M15.493,8.044 C15.2143319,7.68933156 14.8501689,7.40750104 14.4005,7.1985 C13.9508311,6.98949895 13.5170021,6.885 13.099,6.885 C12.8836656,6.885 12.6651678,6.90399981 12.4435,6.942 C12.2218322,6.98000019 12.0223342,7.05283279 11.845,7.1605 C11.6676658,7.2681672 11.5188339,7.40749914 11.3985,7.5785 C11.2781661,7.74950085 11.218,7.96799867 11.218,8.234 C11.218,8.46200114 11.2654995,8.65199924 11.3605,8.804 C11.4555005,8.95600076 11.5948324,9.08899943 11.7785,9.203 C11.9621676,9.31700057 12.1806654,9.42149952 12.434,9.5165 C12.6873346,9.61150047 12.9723317,9.70966616 13.289,9.811 C13.7450023,9.96300076 14.2199975,10.1308324 14.714,10.3145 C15.2080025,10.4981676 15.6576646,10.7419985 16.063,11.046 C16.4683354,11.3500015 16.8039987,11.7268311 17.07,12.1765 C17.3360013,12.6261689 17.469,13.1866633 17.469,13.858 C17.469,14.6306705 17.3265014,15.2988305 17.0415,15.8625 C16.7564986,16.4261695 16.3733357,16.8916648 15.892,17.259 C15.4106643,17.6263352 14.8596698,17.8986658 14.239,18.076 C13.6183302,18.2533342 12.97867,18.342 12.32,18.342 C11.3573285,18.342 10.4263378,18.1741683 9.527,17.8385 C8.62766217,17.5028317 7.88033631,17.0246698 7.285,16.404 L9.413,14.238 C9.74233498,14.6433354 10.176164,14.9821653 10.7145,15.2545 C11.252836,15.5268347 11.7879973,15.663 12.32,15.663 C12.5606679,15.663 12.7949989,15.6376669 13.023,15.587 C13.2510011,15.5363331 13.4504991,15.4540006 13.6215,15.34 C13.7925009,15.2259994 13.9286662,15.0740009 14.03,14.884 C14.1313338,14.693999 14.182,14.4660013 14.182,14.2 C14.182,13.9466654 14.1186673,13.7313342 13.992,13.554 C13.8653327,13.3766658 13.6848345,13.2151674 13.4505,13.0695 C13.2161655,12.9238326 12.9248351,12.7908339 12.5765,12.6705 C12.2281649,12.5501661 11.8323355,12.420334 11.389,12.281 C10.9583312,12.141666 10.5371687,11.9770009 10.1255,11.787 C9.71383127,11.596999 9.34650161,11.3531682 9.0235,11.0555 C8.70049838,10.7578318 8.44083431,10.3968355 8.2445,9.9725 C8.04816568,9.54816454 7.95,9.03200304 7.95,8.424 C7.95,7.67666293 8.10199848,7.03700266 8.406,6.505 C8.71000152,5.97299734 9.10899753,5.53600171 9.603,5.194 C10.0970025,4.85199829 10.6543302,4.60183412 11.275,4.4435 C11.8956698,4.28516587 12.5226635,4.206 13.156,4.206 C13.9160038,4.206 14.6918294,4.34533194 15.4835,4.624 C16.2751706,4.90266806 16.9686637,5.31433061 17.564,5.859 L15.493,8.044 Z" fill="#000000"/>
</g>
</svg> </div>
<div class="kt-notification__item-details flex-row justify-content-between">
<div class="kt-notification__item-title">Total Amount</div>
<div class="kt-notification__item-time">$123</div>
</div>
</a>




</div>
<div class="accordion accordion-solid " id="accordionExample6">
<div class="card">
<div class="card-header" id="headingOne6">
<div class="btn btn-brand btn-upper btn-block collapsed" data-toggle="collapse" data-target="#collapseOne6" aria-expanded="false" aria-controls="collapseOne6">
                                 <i class="fa fa-credit-card"></i> Pay by Card</div>
</div>
<div id="collapseOne6" class="collapse" aria-labelledby="headingOne6" data-parent="#accordionExample6" style="">
<div class="card-body"><div class="form-group">

<input type="text" class="form-control" placeholder="1234">

</div>
<div class="row form-group ">
<div class="col-6 kt-pl-0">

<input type="text" class="form-control" placeholder="MM/YY">

</div>
<div class="col-6">

<input type="text" class="form-control" placeholder="CVV">

</div></div>

<div class="form-group">

<input type="text" class="form-control" placeholder="Name on Card">

</div>
<div class="">
<a href="#" class="btn btn-brand btn-upper btn-block" data-toggle="modal" data-target="#kt_modal_6">Pay $ 12.00</a>
</div>
<div class="kt-mt-10 text-center d-flex align-items-center justify-content-center">Powered by <i class="fab fa-stripe" style="font-size: 2.4rem;margin-left: 0.4rem;"></i></div></div>
</div>
</div>
<div class="btn btn-warning btn-upper btn-block">
   
<i class="fab fa-paypal"></i>Pay by paypal</div>

</div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                                <!-- end:: Content -->
                            </div>
                        </div>


@section('mainscripts')
@parent
    <!--begin::Page Vendors Styles(used by this page) -->
        <script href="{{ asset( 'public/assets/js/owl.carousel.js' ) }}" type="text/javascript"></script>


        <script src="{{ asset( 'public/assets/js/pages/crud/forms/widgets/ion-range-slider.js' ) }}" type="text/javascript"></script>
        <script>
            $(document).ready(function() {
              $('.owl-carousel').owlCarousel({
                loop: true,
                responsiveClass: true,
                responsive: {
                  0: {
                    items: 1,
                    nav: false,
                    dots: false,
                    autoplay: true
                  },
                  600: {
                    items: 2,
                    nav: false,
                    dots: false,
                    autoplay: true
                  },
                  768: {
                    items: 2,
                    nav: false,
                    dots: false
                  },
                  900: {
                    items: 3,
                    nav: false,
                    dots: false
                  },
                  1200: {
                    items: 4,
                    nav: false,
                    dots: false
                  },
                  1399: {
                    items: 5,
                    nav: false,
                    dots: false
                  }
                }
              })
            })          
        </script>
<!--end::Page Scripts -->
@show
@endsection
