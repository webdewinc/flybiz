@extends('layout.app')

@section('content')
@section('mainhead')
@parent
    
    <link href="{{ asset( 'public/assets/css/owl.carousel.min.css' ) }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset( 'public/assets/css/pages/pricing/pricing-2.css' ) }}" rel="stylesheet" type="text/css" />
        <!--end::Page Vendors Styles -->
@show


<div class="kt-container  kt-grid__item kt-grid__item--fluid">

<div class="row">

<!-- body content start -->

<!-- tabs html start here -->
<div class="kt-portlet__body pt-7 pb-6">
               <ul class="nav nav-pills justify-content-center pb-3" role="tablist">
                   <li class="nav-item">
                       <a class="nav-link active" data-toggle="tab" href="#kt_tabs_3_1"><i class="flaticon-lock"></i>Privacy Policy</a>
                   </li>
                   <li class="nav-item">
                       <a class="nav-link" data-toggle="tab" href="#kt_tabs_3_3"><i class="flaticon-interface-10"></i> Terms of Service</a>
                   </li>
               </ul>                    

               <div class="tab-content">
                   <div class="tab-pane active" id="kt_tabs_3_1" role="tabpanel">
                       <p>Our Guest Post Engine is accessible from www.guestpostengine.com, and one of our main priorities is visitors privacy. The following privacy policy document holds information related to the information collected and recorded by guestpostengine. Also, you can understand how we are using it.</p>
                       <p>If document unable to satisfy all your question or want to know more about Privacy Policy, then feel free to contact through email at <a href="mailto: hey@guestpostengine.com">hey@guestpostengine.com</a>.</p>
                       <div class="pt-4"></div>
                       <h5 class="pb-2">Log Files</h5>
                       <p>Guest Post Engine follows a standard procedure of using log files. These files log visitors when they visit websites. All hosting companies do this and as a part of hosting service analytics.</p>
                       <p>In log files, there are following the information which gets collected such as the date and time stamp, IP (Internet Protocol) addresses, ISP (Internet Service Provider), browser type, possibly the number of clicks, and referring/exit pages.</p>
                       <p>These are not linked to any information that is personally identifiable. The purpose of the information is for analyzing trends, administering the site, tracking users' movement on the website, and gathering demographic information.</p>
                       <div class="pt-4"></div>
                       <h5 class="pb-2">Cookies and Web Beacons</h5>
                       <p>Like any other website, Guest Post Engine uses “cookies”. These cookies are used to store information including visitors' preferences, and the pages on the website that the visitor accessed or visited. The information is used to optimize the users' experience by customizing our web page content based on visitors' browser type and/or other information</p>
                       <div class="pt-4"></div>
                       <h5 class="pb-2">Privacy Policies</h5>
                       <p>Third-party ad servers or ad networks uses technologies like cookies, JavaScript, or Web Beacons that are used in their respective advertisements, which are sent directly to users' browser. They automatically receive your IP address when this occurs. These technologies are used to measure the effectiveness of their advertising campaigns and/or to personalize the advertising content that you see on websites that you visit.</p>
                       <p><strong>Note:</strong> Guest Post Engine has no access or control over the cookies that are used by third-party advertisers.</p>
                       <div class="pt-4"></div>
                       <h5 class="pb-2">Third-Party Privacy Policies</h5>
                       <p>Guest Post Engine's Privacy Policy does not apply to other advertisers or websites. Thus, we are advising you to consult the respective Privacy Policies of these third-party ad servers for more detailed information. It may include their practices and instructions about how to opt-out of certain options. You may find a complete list of these Privacy Policies and their links here: Privacy Policy Links.</p>
                       <p>You can choose to disable cookies through your individual browser options. To know more detailed information about cookie management with specific web browsers, it can be found at the browsers' respective websites. What Are Cookies?</p>
                       <div class="pt-4"></div>
                       <h5 class="pb-2">Online Privacy Policy Only</h5>
                       <p>This Privacy Policy applies only to our online activities and is valid for visitors to our website with regards to the information that they shared and/or collect. This policy is not applicable to any information collected offline or via channels other than this website.</p>
                       <div class="pt-4"></div>
                       <h5 class="pb-2">Consent</h5>
                       <p>By using our website, you hereby consent to our Privacy Policy and agree to its Terms and Conditions.</p>
                   </div>
                   <div class="tab-pane" id="kt_tabs_3_3" role="tabpanel">
                       <p>Welcome to GuestPostEngine!</p>
                       <p>The following terms and conditions outline rules and regulations related to the use of Guestpostengine.com.</p>
                       <p>If you are accessing the guestpostengine.com that means you accept its terms and conditions. Do not use Guestpostengine if you do not agree to all the terms and conditions stated on this page.</p>
                       <div class="pt-2"></div>
                       <h5>Terminology</h5>
                       <p>The following terminology applies to these Terms and Conditions, Privacy Statement and Disclaimer Notice and all Agreements:</p>
                       <p class="kt-font-bolder"><i>"Client", "You" and "Your" refers to you and the person log on to this website is compliant to the website's terms and conditions. "The Company", "Ourselves", "We", "Our" and "Us", refers to our Company. "Party", "Parties", or "Us", refers to both the Client and ourselves.</i></p>
                       <p>Any use of the above terminology or other words in the singular, plural, capitalization and/or he/she or they, are taken as interchangeable and therefore as referring to same.</p>
                       <div class="pt-2"></div>
                       <h5>Cookies</h5>
                       <p>We employ the use of cookies, as by accessing Guestpostengine, you agreed to use cookies in agreement with the Guestpostengine's Privacy Policy.</p>
                       <p>Most interactive websites use cookies to retrieve the user’s details on every visit. Cookies are used by our website to enable the functionality of certain areas, that makes it easier for a user to visit our website.</p>
                       <div class="pt-2"></div>
                       <h5>Content Liability</h5>
                       <p>We shall not be held responsible for any content that appears on your Website. You agree to protect and defend us against all claims that are rising on your Website.</p>
                       <p>No link(s) should appear on any Website that may be interpreted as libellous- obscene or criminal, or which infringes, otherwise violates, or advocates the infringement or other violation of, any third party rights.</p>
                       <div class="pt-2"></div>
                       <h5>License</h5>
                       <p>Unless otherwise stated, Guestpostengine and/or its licensors own the intellectual property rights for all material on Guestpostengine. All intellectual property rights are reserved. You may access this from Guestpostengine for your own personal use subjected to restrictions set in these terms and conditions.</p>
                       <div class="pt-2"></div>
                       <h6>You must not:</h6>
                       <ul>
                        <li>Redistribute content from Guestpostengine.</li>
                        <li>Reproduce, duplicate or copy material from Guestpostengine.</li>
                        <li>Sell, rent or sub-license material from Guestpostengine.</li>
                        <li>Republish material from Guestpostengine.</li>
                       </ul>
                       <div class="pt-2"></div>
                       <h6>Approved organizations may hyperlink to our Website as follows:</h6>
                       <ul>
                        <li>By use of our website name; or</li>
                        <li>By use of the uniform resource locator being linked to; or</li>
                        <li>No use of Guestpostengine logo or other artwork will be allowed for linking absent a trademark license agreement</li>
                       </ul>
                       <div class="pt-2"></div>
                       <h5>Disclaimer</h5>
                       <p>To the maximum extent permitted by applicable law, we exclude all representations, warranties and conditions relating to our website. Nothing in this disclaimer will:</p>
                       <p>The limitations and prohibitions of liability set in this Section and elsewhere in this disclaimer: (a) are subject to the preceding paragraph; and (b) govern all liabilities arising under the disclaimer, including liabilities arising in contract, in tort and for breach of statutory duty.</p>
                       <p>As long as the website and the information and services on the website are provided free of charge, we will not be liable for any loss or damage of any nature.</p>
                   </div>
               </div>      
           </div>
           <!-- tabs html end here -->





<!-- body content end -->

</div>
</div>

@section('mainscripts')
@parent
    <!--begin::Page Vendors Styles(used by this page) -->
        <script href="{{ asset( 'public/assets/js/owl.carousel.js' ) }}" type="text/javascript"></script>


        <script src="{{ asset( 'public/assets/js/pages/crud/forms/widgets/ion-range-slider.js' ) }}" type="text/javascript"></script>
        <script>
            $(document).ready(function() {
              $('.owl-carousel').owlCarousel({
                loop: true,
                responsiveClass: true,
                responsive: {
                  0: {
                    items: 1,
                    nav: false,
                    dots: false,
                    autoplay: true
                  },
                  600: {
                    items: 2,
                    nav: false,
                    dots: false,
                    autoplay: true
                  },
                  768: {
                    items: 2,
                    nav: false,
                    dots: false
                  },
                  900: {
                    items: 3,
                    nav: false,
                    dots: false
                  },
                  1200: {
                    items: 4,
                    nav: false,
                    dots: false
                  },
                  1399: {
                    items: 5,
                    nav: false,
                    dots: false
                  }
                }
              })
            })          
        </script>
<!--end::Page Scripts -->
@show
@endsection
