@extends('layout.app')
@section('title', 'Add website as a Publisher- GuestPostEngine')
@section('title-description')

<meta name="description" content="Under add website section at GuestPostEngine, you can add any number of websites with an affordable price as a publisher.">
    
@endsection
@section('content')
@section('mainhead')
@parent
    <!--begin::Page Vendors Styles(used by this page) -->
    <link href="{{ asset('') . config('app.public_url') . '/assets/css/pages/wizard/wizard-2.css'  }}" rel="stylesheet" type="text/css" />

        <!--end::Page Vendors Styles -->
@show
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
            </div>
        </div>
        <!-- end:: Subheader -->
        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">
            <div class="kt-portlet">
                <div class="kt-portlet__body kt-portlet__body--fit">
                    <div class="kt-grid  kt-wizard-v2 kt-wizard-v2--white" id="kt_wizard_v2" data-ktwizard-state="step-first">
                        <div class="kt-grid__item kt-wizard-v2__aside">
                            <!--begin: Form Wizard Nav -->
                            <div class="kt-wizard-v2__nav">
                                <!--doc: Remove "kt-wizard-v2__nav-items--clickable" class and also set 'clickableSteps: false' in the JS init to disable manually clicking step titles -->
                                <div class="kt-wizard-v2__nav-items kt-wizard-v2__nav-items--clickable">
                                    <div class="kt-wizard-v2__nav-item" data-ktwizard-type="step" data-ktwizard-state="current">
                                        <div class="kt-wizard-v2__nav-body">
                                            <div class="kt-wizard-v2__nav-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"/>
                                                        <polygon fill="#000000" opacity="0.3" points="6 7 6 15 18 15 18 7"/>
                                                        <path d="M11,19 L11,16 C11,15.4477153 11.4477153,15 12,15 C12.5522847,15 13,15.4477153 13,16 L13,19 L14.5,19 C14.7761424,19 15,19.2238576 15,19.5 C15,19.7761424 14.7761424,20 14.5,20 L9.5,20 C9.22385763,20 9,19.7761424 9,19.5 C9,19.2238576 9.22385763,19 9.5,19 L11,19 Z" fill="#000000" opacity="0.3"/>
                                                        <path d="M6,7 L6,15 L18,15 L18,7 L6,7 Z M6,5 L18,5 C19.1045695,5 20,5.8954305 20,7 L20,15 C20,16.1045695 19.1045695,17 18,17 L6,17 C4.8954305,17 4,16.1045695 4,15 L4,7 C4,5.8954305 4.8954305,5 6,5 Z" fill="#000000" fill-rule="nonzero"/>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="kt-wizard-v2__nav-label">
                                                <div class="kt-wizard-v2__nav-label-title">
                                                    Website
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-wizard-v2__nav-item" data-ktwizard-type="step">
                                        <div class="kt-wizard-v2__nav-body">
                                            <div class="kt-wizard-v2__nav-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <path d="M4,4 L11.6314229,2.5691082 C11.8750185,2.52343403 12.1249815,2.52343403 12.3685771,2.5691082 L20,4 L20,13.2830094 C20,16.2173861 18.4883464,18.9447835 16,20.5 L12.5299989,22.6687507 C12.2057287,22.8714196 11.7942713,22.8714196 11.4700011,22.6687507 L8,20.5 C5.51165358,18.9447835 4,16.2173861 4,13.2830094 L4,4 Z" fill="#000000" opacity="0.3"></path>
                                                        <path d="M11.1750002,14.75 C10.9354169,14.75 10.6958335,14.6541667 10.5041669,14.4625 L8.58750019,12.5458333 C8.20416686,12.1625 8.20416686,11.5875 8.58750019,11.2041667 C8.97083352,10.8208333 9.59375019,10.8208333 9.92916686,11.2041667 L11.1750002,12.45 L14.3375002,9.2875 C14.7208335,8.90416667 15.2958335,8.90416667 15.6791669,9.2875 C16.0625002,9.67083333 16.0625002,10.2458333 15.6791669,10.6291667 L11.8458335,14.4625 C11.6541669,14.6541667 11.4145835,14.75 11.1750002,14.75 Z" fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="kt-wizard-v2__nav-label">
                                                <div class="kt-wizard-v2__nav-label-title">
                                                    Verify
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-wizard-v2__nav-item" data-ktwizard-type="step">
                                        <div class="kt-wizard-v2__nav-body">
                                            <div class="kt-wizard-v2__nav-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <path d="M8,3 L8,3.5 C8,4.32842712 8.67157288,5 9.5,5 L14.5,5 C15.3284271,5 16,4.32842712 16,3.5 L16,3 L18,3 C19.1045695,3 20,3.8954305 20,5 L20,21 C20,22.1045695 19.1045695,23 18,23 L6,23 C4.8954305,23 4,22.1045695 4,21 L4,5 C4,3.8954305 4.8954305,3 6,3 L8,3 Z" fill="#000000" opacity="0.3"></path>
                                                        <path d="M11,2 C11,1.44771525 11.4477153,1 12,1 C12.5522847,1 13,1.44771525 13,2 L14.5,2 C14.7761424,2 15,2.22385763 15,2.5 L15,3.5 C15,3.77614237 14.7761424,4 14.5,4 L9.5,4 C9.22385763,4 9,3.77614237 9,3.5 L9,2.5 C9,2.22385763 9.22385763,2 9.5,2 L11,2 Z" fill="#000000"></path>
                                                        <rect fill="#000000" opacity="0.3" x="7" y="10" width="5" height="2" rx="1"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="7" y="14" width="9" height="2" rx="1"></rect>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="kt-wizard-v2__nav-label">
                                                <div class="kt-wizard-v2__nav-label-title">
                                                    Content
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-wizard-v2__nav-item" href="#" data-ktwizard-type="step">
                                        <div class="kt-wizard-v2__nav-body">
                                            <div class="kt-wizard-v2__nav-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <path d="M8,3 L8,3.5 C8,4.32842712 8.67157288,5 9.5,5 L14.5,5 C15.3284271,5 16,4.32842712 16,3.5 L16,3 L18,3 C19.1045695,3 20,3.8954305 20,5 L20,21 C20,22.1045695 19.1045695,23 18,23 L6,23 C4.8954305,23 4,22.1045695 4,21 L4,5 C4,3.8954305 4.8954305,3 6,3 L8,3 Z" fill="#000000" opacity="0.3"></path>
                                                        <path d="M11,2 C11,1.44771525 11.4477153,1 12,1 C12.5522847,1 13,1.44771525 13,2 L14.5,2 C14.7761424,2 15,2.22385763 15,2.5 L15,3.5 C15,3.77614237 14.7761424,4 14.5,4 L9.5,4 C9.22385763,4 9,3.77614237 9,3.5 L9,2.5 C9,2.22385763 9.22385763,2 9.5,2 L11,2 Z" fill="#000000"></path>
                                                        <rect fill="#000000" opacity="0.3" x="10" y="9" width="7" height="2" rx="1"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="7" y="9" width="2" height="2" rx="1"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="7" y="13" width="2" height="2" rx="1"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="10" y="13" width="7" height="2" rx="1"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="7" y="17" width="2" height="2" rx="1"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="10" y="17" width="7" height="2" rx="1"></rect>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="kt-wizard-v2__nav-label">
                                                <div class="kt-wizard-v2__nav-label-title">
                                                    Publish
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-wizard-v2__nav-item" data-ktwizard-type="step">
                                        <div class="kt-wizard-v2__nav-body">
                                            <div class="kt-wizard-v2__nav-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="2" y="5" width="20" height="14" rx="2"></rect>
                                                        <rect fill="#000000" x="2" y="8" width="20" height="3"></rect>
                                                        <rect fill="#000000" opacity="0.3" x="16" y="14" width="4" height="2" rx="1"></rect>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="kt-wizard-v2__nav-label">
                                                <div class="kt-wizard-v2__nav-label-title">
                                                    Pricing
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="kt-wizard-v2__nav-item" data-ktwizard-type="step">
                                        <div class="kt-wizard-v2__nav-body">
                                            <div class="kt-wizard-v2__nav-icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"></rect>
                                                        <path d="M8,3 L8,3.5 C8,4.32842712 8.67157288,5 9.5,5 L14.5,5 C15.3284271,5 16,4.32842712 16,3.5 L16,3 L18,3 C19.1045695,3 20,3.8954305 20,5 L20,21 C20,22.1045695 19.1045695,23 18,23 L6,23 C4.8954305,23 4,22.1045695 4,21 L4,5 C4,3.8954305 4.8954305,3 6,3 L8,3 Z" fill="#000000" opacity="0.3"></path>
                                                        <path d="M10.875,15.75 C10.6354167,15.75 10.3958333,15.6541667 10.2041667,15.4625 L8.2875,13.5458333 C7.90416667,13.1625 7.90416667,12.5875 8.2875,12.2041667 C8.67083333,11.8208333 9.29375,11.8208333 9.62916667,12.2041667 L10.875,13.45 L14.0375,10.2875 C14.4208333,9.90416667 14.9958333,9.90416667 15.3791667,10.2875 C15.7625,10.6708333 15.7625,11.2458333 15.3791667,11.6291667 L11.5458333,15.4625 C11.3541667,15.6541667 11.1145833,15.75 10.875,15.75 Z" fill="#000000"></path>
                                                        <path d="M11,2 C11,1.44771525 11.4477153,1 12,1 C12.5522847,1 13,1.44771525 13,2 L14.5,2 C14.7761424,2 15,2.22385763 15,2.5 L15,3.5 C15,3.77614237 14.7761424,4 14.5,4 L9.5,4 C9.22385763,4 9,3.77614237 9,3.5 L9,2.5 C9,2.22385763 9.22385763,2 9.5,2 L11,2 Z" fill="#000000"></path>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div class="kt-wizard-v2__nav-label">
                                                <div class="kt-wizard-v2__nav-label-title">
                                                    Submit
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end: Form Wizard Nav -->
                        </div>
                        <div class="kt-grid__item kt-grid__item--fluid kt-wizard-v2__wrapper">
                            <!--begin: Form Wizard Form-->

                            <form class="kt-form" id="kt_form" method="POST">
                                @csrf
                                <!--begin: Form Wizard Step 1-->
                                <input type="hidden" name="auth_id" id="auth_id" value="{{ Auth::user()->user_id }}" />
                                <!-- <input type="hidden" name="dp_da" id="dp_da" value="" />
                                <input type="hidden" name="dp_pa" id="dp_pa" value="" /> -->
                                <div class="kt-wizard-v2__content" data-ktwizard-type="step-content" data-ktwizard-state="current">
                                    <div class="kt-heading kt-heading--md">Add Website</div>
                                    <div class="kt-form__section kt-form__section--first">
                                        <div class="kt-wizard-v2__form">
                                            <div class="form-group row" id="web_error_div">
                                                <div class="col-md-6 col-12">
                                                    <label class="required">Website URL</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-globe"></i></span></div>
                                                        <input type="text" class="form-control" name="web_url" id="web_url" placeholder="Enter Website URL with http or https"  />            
                                                    </div>
                                                    <span id="web_error_span" style="display: none; color: red;" class="error custom-error">Please enter a valid website URL.</span>
                                                    <span class="form-text text-muted">Please use http://www or https://www or another prefix at the beginning.</span>
                                                    <i id="da_check"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end: Form Wizard Step 1-->
                                <!--begin: Form Wizard Step 2-->
                                <div class="kt-wizard-v2__content" data-ktwizard-type="step-content">
                                    <div class="kt-heading kt-heading--md">Verify Website</div>
                                    <div class="kt-form__section kt-form__section--first">
                                        <div class="kt-wizard-v2__form">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <label>Do you own this Website ?</label>
                                                        <div class="kt-radio-inline">
                                                            <label class="kt-radio kt-radio--success">
                                                                <input type="radio" class="verify_website" name="radio2" value="yes" checked>Yes<span></span>
                                                            </label>
                                                            <label class="kt-radio kt-radio--danger">
                                                                <input type="radio" class="verify_website" name="radio2" value="no">No<span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row" id="own_website">
                                                        <div class="col-md-6 col-12">
                                                            <label class="required">Verify Domain Ownership</label>

                                                            <div class="input-group">
                                                                <input type="text" class="form-control" id="verify_domain" name="verify_domain" placeholder="Enter your domain email" />
                                                                <div class="input-group-append"><span class="input-group-text" id="email_domain">@domain.com</span></div>
                                                                <span class="form-text text-muted">Enter your domain email for verification Ex: name@domain.com</span>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end: Form Wizard Step 2-->
                                <!--begin: Form Wizard Step 5-->
                                <div class="kt-wizard-v2__content" data-ktwizard-type="step-content">
                                    <div class="kt-heading kt-heading--md">Content Details</div>
                                    <div class="kt-form__section kt-form__section--first">
                                        <div class="kt-wizard-v2__form">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="row form-group">
                                                        <div class="col-12">
                                                            <label>Who can Provide Content ?</label>
                                                            <div class="kt-checkbox-inline">
                                                                <label class="kt-checkbox kt-checkbox--brand">
                                                                    <input type="checkbox" name="provide_content[]" class="provide_content" value="seller"
                                                                    checked="checked">Publisher<span></span>
                                                                </label>
                                                                <label class="kt-checkbox kt-checkbox--brand">
                                                                    <input type="checkbox" name="provide_content[]" class="provide_content" value="buyer">Advertiser<span></span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row" id="content_row">
                                                        <div class="col-md-6 col-12 form-group">
                                                            <label class="required">Number of words for Content ?</label>
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" id="number_words" name="number_words" placeholder="" aria-describedby="basic-addon2" maxlength="5" />
                                                                <div class="input-group-append"><span class="input-group-text" id="basic-addon2">Words</span></div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-12 form-group">
                                                            <label class="required">Content Cost</label>
                                                            <div class="input-group">
                                                                <div class="input-group-prepend"><span class="input-group-text">$</span></div>
                                                                <input type="text" name="extra_cost" id="extra_cost" class="form-control" maxlength="3" placeholder="" aria-describedby="basic-addon1"  />
                                                            </div>
                                                        </div>
                                                        <!-- add promo code for content -->
                                                        <!-- <div class="col-md-6 col-12 form-group">
                                                            <label>Want to Give Discount for Content Cost</label>
                                                            <div class="input-group">
                                                                <select class="form-control promo_discount">
                                                                    <option value="0">Select Promo</option>
                                                                    @php
                                                                    $promoCodes = DB::table('promo_codes')->get()->toArray();
                                                                    @endphp
                                                                    @foreach( $promoCodes as $splitCodes )
                                                                    <option value="{{ $splitCodes->promo_id}}">Discount {{ $splitCodes->promo_number }}% </option>
                                                                    @endforeach 
                                                                </select>
                                                            </div>
                                                        </div> -->
                                                        <!-- end promo code for content -->
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="kt-portlet kt-iconbox kt-iconbox--brand kt-iconbox--animate-slow">
                                                        <div class="kt-portlet__body">
                                                            <div class="kt-iconbox__body">
                                                                <div class="kt-iconbox__icon">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                            <rect x="0" y="0" width="24" height="24"></rect>
                                                                            <path d="M8,3 L8,3.5 C8,4.32842712 8.67157288,5 9.5,5 L14.5,5 C15.3284271,5 16,4.32842712 16,3.5 L16,3 L18,3 C19.1045695,3 20,3.8954305 20,5 L20,21 C20,22.1045695 19.1045695,23 18,23 L6,23 C4.8954305,23 4,22.1045695 4,21 L4,5 C4,3.8954305 4.8954305,3 6,3 L8,3 Z" fill="#000000" opacity="0.3"></path>
                                                                            <path d="M10.875,15.75 C10.6354167,15.75 10.3958333,15.6541667 10.2041667,15.4625 L8.2875,13.5458333 C7.90416667,13.1625 7.90416667,12.5875 8.2875,12.2041667 C8.67083333,11.8208333 9.29375,11.8208333 9.62916667,12.2041667 L10.875,13.45 L14.0375,10.2875 C14.4208333,9.90416667 14.9958333,9.90416667 15.3791667,10.2875 C15.7625,10.6708333 15.7625,11.2458333 15.3791667,11.6291667 L11.5458333,15.4625 C11.3541667,15.6541667 11.1145833,15.75 10.875,15.75 Z" fill="#000000"></path>
                                                                            <path d="M11,2 C11,1.44771525 11.4477153,1 12,1 C12.5522847,1 13,1.44771525 13,2 L14.5,2 C14.7761424,2 15,2.22385763 15,2.5 L15,3.5 C15,3.77614237 14.7761424,4 14.5,4 L9.5,4 C9.22385763,4 9,3.77614237 9,3.5 L9,2.5 C9,2.22385763 9.22385763,2 9.5,2 L11,2 Z" fill="#000000"></path>
                                                                        </g>
                                                                    </svg> </div>
                                                                <div class="kt-iconbox__desc w-100">
                                                                    <a href="">
                                                                    </a>
                                                                    <h3 class="kt-iconbox__title"><a href="">
                                                                        </a>Hire Guest Post Engine
                                                                    </h3>
                                                                    <div class="kt-iconbox__content">Content Writer</div>
                                                                </div>
                                                                <div style="width: 100%;float: right; -ms-flex-direction: column;">
                                                                    <button type="button" class="btn btn-label-brand btn-bold kt-mt-10" style="float: right;">Hire Writer Now</button></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end: Form Wizard Step 5-->
                                <!--begin: Form Wizard Step 3-->
                                <div class="kt-wizard-v2__content" data-ktwizard-type="step-content">
                                    <div class="kt-heading kt-heading--md">Publishing Details</div>
                                    <div class="kt-form__section kt-form__section--first">
                                        <div class="kt-wizard-v2__form">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <label>Backlink Type</label>
                                                        <div class="kt-radio-inline">
                                                            <label class="kt-radio kt-radio--success">
                                                                <input type="radio" name="backlink_type"  class="backlink_type" value="follow" checked="checked" />Do Follow<span></span>
                                                            </label>
                                                            <label class="kt-radio kt-radio--warning">
                                                                <input type="radio" name="backlink_type" class="backlink_type" value="no_follow" />No Follow<span></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6 col-12 form-group">
                                                            <label class="required">Maximum Number of Backlinks</label>
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" id="max_backlinks" name="max_backlinks" placeholder="Maximum Number of Backlinks" aria-describedby="basic-addon2" />
                                                                <div class="input-group-append"><span class="input-group-text" id="basic-addon2">Backlinks</span></div>
                                                            </div>                                                        
                                                        </div>
                                                        <div class="col-md-6 col-12 form-group">
                                                            <label class="required">Number of words for Content ?</label>
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" id="words_content" name="words_content" placeholder="Number of words for Content" aria-describedby="basic-addon2" />
                                                                <div class="input-group-append"><span class="input-group-text" id="basic-addon2">Words</span></div>
                                                            </div>
                                                        </div>
                                                        <!-- <div class="col-xl-6">
                                                            <div class="form-group">
                                                                <label>Maximum Number Of Words</label>
                                                                <input type="text" class="form-control" name="city" placeholder="" value="">
                                                                
                                                            </div>
                                                        </div> -->
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6 col-12 form-group">
                                                            <label>Guest Post Guideline URL</label>
                                                            <div class="input-group">
                                                                <div class="input-group-prepend"><span class="input-group-text"><i class="la la-globe"></i></span></div>
                                                                <input type="text" class="form-control" id="guideline_url" name="guideline_url" id="guideline_url" placeholder="Enter Guideline URL" />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-12">
                                                            <div class="form-group">
                                                                <label for="exampleSelect1">Turnaround Time</label>
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-clock-o"></i></span></div>
                                                                    <select class="form-control" id="turnaround_time" name="turnaround_time"> 
                                                                        <option value="1">1 Week</option>
                                                                        <option value="2">2 Weeks</option>
                                                                        <option value="3">3 Weeks</option>
                                                                        <option value="4">4 Weeks</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group ">
                                                        <label>Guest Post Examples</label>
                                                        <div class="row">
                                                            <div class="col-md-6 col-12 form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend"><span class="input-group-text">#1</span></div>
                                                                    <input type="text" class="form-control" id="example_1" name="example_1" placeholder="Enter Domain" aria-describedby="basic-addon1" />
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 col-12">
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend"><span class="input-group-text">#2</span></div>
                                                                    <input type="text" class="form-control" id="example_2" name="example_2" placeholder="Enter Domain" aria-describedby="basic-addon1" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end: Form Wizard Step 3-->
                                <!--begin: Form Wizard Step 4-->
                                <div class="kt-wizard-v2__content" data-ktwizard-type="step-content">
                                    <div class="kt-heading kt-heading--md">Pricing Details</div>
                                    <div class="kt-form__section kt-form__section--first">
                                        <div class="kt-wizard-v2__form">
                                            <div class="row">
                                                <div class="col-md-6 col-12">
                                                    <div class="form-group">
                                                        <label class="required">Your Website Price</label>
                                                        <div class="input-group">
                                                            <div class="input-group-prepend"><span class="input-group-text">$</span></div>
                                                            <input type="text" class="form-control" id="your_price" name="your_price" placeholder="Enter Price" aria-describedby="basic-addon1" />
                                                        </div>
                                                        <span id="price_msg" class="form-text text-muted"></span>
                                                    </div>
                                                </div>
                                                <div class="form-group col-md-6 col-12 hidden">
                                                    <label>Selling Price</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend"><span class="input-group-text">$</span></div>
                                                        <input type="hidden" class="form-control" id="selling_price" name="selling_price" placeholder="Enter Price" aria-describedby="basic-addon1" value=""  disabled />
                                                    </div>
                                                  
                                                </div>
                                            </div>
                                            <!-- add promo code for website -->
                                            <!-- <div class="col-md-6 col-12 form-group">
                                                <label>Want to Give Discount for Website Cost</label>
                                                <div class="input-group">
                                                    <select class="form-control promo_web_discount">
                                                        <option value="0">Select Promo</option>
                                                        @php
                                                        $promoCodes = DB::table('promo_codes')->get()->toArray();
                                                        @endphp
                                                        @foreach( $promoCodes as $splitCodes )
                                                        <option value="{{ $splitCodes->promo_id}}">Discount {{ $splitCodes->promo_number }}% </option>
                                                        @endforeach 
                                                    </select>
                                                </div>
                                            </div> -->
                                            <!-- end promo code for website -->
                                            <div class="alert alert-primary fade show" role="alert">
                                                <div class="alert-icon"><i class="flaticon-questions-circular-button"></i></div>
                                                <div class="alert-text"><span style="font-weight: 500;">Pricing Formula</span><br>
                                                    Selling Price = Your Price + $10 + 10% (Rounded to next 10's)</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end: Form Wizard Step 4-->
                                <!--begin: Form Wizard Step 6-->
                                <div class="kt-wizard-v2__content" data-ktwizard-type="step-content">
                                    <div class="kt-heading kt-heading--md">Review your Details and Submit</div>
                                    <div class="kt-form__section kt-form__section--first">
                                        <div class="kt-wizard-v2__review">
                                            <div class="kt-wizard-v2__review-item">
                                                <div class="kt-wizard-v2__review-title">
                                                    Add Website
                                                </div>
                                                <div class="kt-wizard-v2__review-content"><span id="push_domain">website.com</span>
                                                    <!-- <br>
                                                    Domain Authority: <span id="push_da">40</span> -->
                                                </div>
                                            </div>
                                            <div class="kt-wizard-v2__review-item">
                                                <div class="kt-wizard-v2__review-title">
                                                    Verify Website
                                                </div>
                                                <div class="kt-wizard-v2__review-content">Do you own this Website: <span id="push_web">Yes</span> </div>
                                            </div>
                                             <div class="kt-wizard-v2__review-item">
                                                <div class="kt-wizard-v2__review-title">
                                                    Content Details
                                                </div>
                                                <div class="kt-wizard-v2__review-content">
                                                    Who can Provide Content: <span id="push_provide">Publisher</span>
                                                    <br>
                                                    Number of words for Content: <span id="push_words">1000</span>
                                                    <br>
                                                    Content Cost: $<span id="push_cost">120</span>
                                                </div>
                                            </div>
                                            <div class="kt-wizard-v2__review-item">
                                                <div class="kt-wizard-v2__review-title">
                                                    Publishing Details
                                                </div>
                                                <div class="kt-wizard-v2__review-content">Backlink Type: <span id="push_type">Do Follow</span> <br>
                                                    Maximum Number of Backlinks: <span id="push_backlinks">1</span><br> Number of words for Content: <span id="push_content">1000</span>
                                                    <br>
                                                    Guest Post Guideline URL: <span id="push_guideline">www.website.com</span>
                                                    <br>
                                                    Turnaround Time: <span id="push_turnaround">2</span> Weeks
                                                    <br>
                                                    Guest Post Examples: <br>
                                                    <span id="push_example_1">Example 1.com</span><br>
                                                    <span id="push_example_2">Example 2.com</span><br>
                                                    <br>
                                                </div>
                                            </div>                                          
                                            <div class="kt-wizard-v2__review-item">
                                                <div class="kt-wizard-v2__review-title">
                                                    Pricing Details
                                                </div>
                                                <div class="kt-wizard-v2__review-content">
                                                    Your website Price: $<span id="push_your_price">100</span>
                                                    <br>
                                                    <!-- Selling Price: $<span id="push_selling_price">120</span> -->
                                                </div>
                                            </div>
                                            <div class="kt-wizard-v2__review-item">
                                                <label class="kt-checkbox kt-checkbox--brand">
                                                    <input type="checkbox" name="agree" required="">I Agree to Terms & Conditions for Publishing Websites on Guest Post Engine <span></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end: Form Wizard Step 6-->
                                <!--begin: Form Actions -->
                                <div class="kt-form__actions">
                                    <button class="btn btn-label-brand btn-bold" data-ktwizard-type="action-next">
                                        Next Step
                                    </button>
                                    <button class="btn btn-label-brand btn-bold"  data-ktwizard-type="action-submit" type="submit" id="action_submit">
                                        Submit
                                    </button>
                                    <button class="btn btn-secondary btn-hover-brand" data-ktwizard-type="action-prev">
                                        Previous
                                    </button>
                                </div>
                                <!--end: Form Actions -->
                            </form>
                            <!--end: Form Wizard Form-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end:: Content -->
    </div>
</div>
    @section('mainscripts')
    @parent
    @section('customScript')
        <!--begin::Page Scripts(used by this page) -->
        <!-- <script src="{{ asset('') . config('app.public_url') . '/assets/js/pages/custom/wizard/wizard-2.js'}}" type="text/javascript"></script> -->
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
         <script type="text/javascript">
            "use strict";

            // Class definition
            var KTWizard2 = function () {
                // Base elements
                var wizardEl;
                var formEl;
                var validator;
                var wizard;

                // Private functions
                var initWizard = function () {
                    // Initialize form wizard
                    wizard = new KTWizard('kt_wizard_v2', {
                        startStep: 1, // initial active step number
                        clickableSteps: true  // allow step clicking
                    });

                    // Validation before going to next page
                    wizard.on('beforeNext', function(wizardObj) {
                        // console.log(wizardObj);
                        if (validator.form() !== true) {
                            wizardObj.stop();  // don't go to the next step
                        } 
                    });

                    // wizard.on('beforePrev', function(wizardObj) {
                    //     if (validator.form() !== false || validator.form() !== true) {
                    //         wizardObj.stop();  // don't go to the next step
                    //     }
                    // });

                    // Change event
                    wizard.on('change', function(wizard) {
                        KTUtil.scrollTop();
                    });
                }

                var initValidation = function() {
                    validator = formEl.validate({
                        // Validate only visible fields
                        ignore: ":hidden",

                        // Validation rules
                        rules: {
                            //= Step 1
                            web_url: {
                                required: true,
                                maxlength:200,
                                url: '/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
                                remote: {
                                        url: "{{url('/verifyExistUrl')}}",
                                        type: "GET",                                       
                                        dataFilter: function (data) {
                                            var response = JSON.parse(data);
                                            if(response.success == false){
                                                swal.fire({
                                                    "title": "",
                                                    "text": "Domain URL Already exist.",
                                                    "type": "error",
                                                    "confirmButtonClass": "btn btn-secondary"
                                                });
                                                $('#web_error').attr('aria-describedby','web_error_span');
                                                $('#web_error_span').html('Domain URL Already exist.');
                                                $('#web_error_span').show();
                                                return false;  
                                            } else {
                                                $('#web_error').attr('aria-describedby','web_error_span');
                                                $('#web_error_span').html('');
                                                $('#web_error_span').hide();
                                                return true;      
                                            }
                                            
                                        },
                                        error: function(){
                                            // swal.fire({
                                            //     "title": "",
                                            //     "text": "Server error.",
                                            //     "type": "error",
                                            //     "confirmButtonClass": "btn btn-secondary"
                                            // });
                                            // $('#da_check').html('');
                                            // $('#web_error_span').html('Server error.');
                                            // $('#web_error_span').show();
                                            // return false;
                                        }
                                    },
                            },
                            verify_domain : {
                                required : true,
                                maxlength:50,
                            },
                            number_words :{
                                required: true,
                                maxlength: 8,
                                digits:true
                            },
                            extra_cost : {
                                required : true,
                                maxlength: 8,
                                digits:true,
                                lessThanEqual : '#number_words',
                            },
                            max_backlinks :{
                                required: true,
                                maxlength: 8,
                                digits:true
                            },
                            words_content :{
                                maxlength:8,
                                digits:true
                            },
                            guideline_url: {
                                maxlength:25,
                            },
                            guideline_url : {
                                maxlength:200,
                                notEqualTo: "#web_url",
                                url: '/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/'
                            },
                            example_1 : {
                                maxlength:200,
                                notEqualTo: "#web_url",
                                url: '/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/'
                            },
                            example_2 : {
                                maxlength:200,
                                notEqualTo: "#web_url",
                                url: '/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/'
                            },
                            your_price : {
                                required: true,
                                digits:true,
                                maxlength:8,
                            },
                           
                        },
                        messages:{
                            "provide_content[]" : {
                                required: 'Atleast select one checkbox.',
                            },
                            web_url: {
                                required: 'Please enter a valid URL.',
                                url: "Please enter a valid URL." , 
                                remote: "" // already exist
                            },
                            verify_domain : {
                                required : 'Please enter a valid email address.',
                            },
                            number_words :{
                                required: '',
                            },
                            extra_cost : {
                                required : '',
                            },
                            max_backlinks : {
                                //required : 'Please enter a valid number.',
                                required : '',
                            },
                            words_content :{
                                required : '',
                            },
                            your_price : {
                                required : 'Please enter a valid price.',
                            },
                        },
                        // errorPlacement: function (error, element) {
                        //     //error.insertBefore(element);
                        //     error.insertAfter(element.closest('div'));
                        // },
                        // Display error
                        invalidHandler: function(event, validator) {
                            KTUtil.scrollTop();

                            swal.fire({
                                "title": "",
                                "text": "There are some errors in your submission. Please correct them.",
                                "type": "error",
                                "confirmButtonClass": "btn btn-secondary"
                            });
                        },
                        
                    });
                }

                var initSubmit = function() {
                    var btn = formEl.find('[data-ktwizard-type="action-submit"]');

                    btn.on('click', function(e) {
                        e.preventDefault();

                        if (validator.form()) {
                            // e.preventDefault();
                            $('.loader').fadeIn('slow');
                            var web_url    = $('#web_url').val();
                            var return_val = isUrlValid(web_url);
                            
                            var upd_web_url     = web_url.replace(/^(?:https?:\/\/)?(?:www\.)?/i, "").split('/')[0];
                            var domain_website  = upd_web_url.substring(0, upd_web_url.lastIndexOf("."));
                            var verify_website  = $('.verify_website:checked').val();
                            var auth_id         = $('#auth_id').val();
                            if(verify_website == 'yes'){
                                var domain_email   = $('#verify_domain').val() + ('@'+upd_web_url);
                            } else{
                                var domain_email   = '';
                            }
                           
                           var provide_content = [];
                           $('.provide_content:checked').each(function(){
                              provide_content.push($(this).val());
                           });
                            var provide_content     = provide_content.toString();
                            var number_words        = $('#number_words').val();
                            var extra_cost          = $('#extra_cost').val();
                            var promo_discount      = $('.promo_discount option:checked').val();
                            var promo_webdiscount   = $('.promo_web_discount option:checked').val();
                            var backlink_type       = $('.backlink_type:checked').val();
                            var max_backlinks       = $('#max_backlinks').val();
                            var words_content       = $('#words_content').val();
                            var guideline_url       = $('#guideline_url').val();
                            var turnaround_time     = $('#turnaround_time').val();
                            var example_1           = $('#example_1').val();
                            var example_2           = $('#example_2').val();
                            var your_price          = $('#your_price').val();
                            // var dp_da               = $('#dp_da').val();
                            // var dp_pa               = $('#dp_pa').val();
                            
                            var upd_price           = parseInt(your_price, 10) + parseInt(10);
                            var upd_sell_price      = parseInt( upd_price, 10) + parseInt(upd_price * 10 / 100);
                            var round_price         = upd_sell_price % 10;

                            if(round_price !=  0){
                                var upd_round_price = 10 - round_price;
                            }
                            else{
                                var upd_round_price  = 0;
                            }
                            var selling_price   = parseInt(upd_sell_price) + parseInt(upd_round_price, 10);
                            $.ajax({
                                 headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                  },
                                  url  : "{{ url('/publisher/add_website') }}",
                                  type : 'POST',
                                  data: {
                                        'domain_suffix'         : web_url,
                                        'domain_url'            : upd_web_url,
                                        'domain_website'        : domain_website,
                                        'domain_status'         : 0,
                                        'domain_fk_user_id'     : auth_id,
                                        'domain_email'          : domain_email,
                                        'guest_content'         : provide_content,
                                        'number_words'          : number_words,
                                        'extra_cost'            : extra_cost,
                                        'promo_code_content'    : promo_discount,
                                        'promo_code_website'    : promo_webdiscount,
                                        'backlink_type'         : backlink_type,
                                        "backlinks"             : max_backlinks,
                                        'max_words'             : words_content,
                                        'cost_price'            : your_price,
                                        'domain_guideline_url'  : guideline_url,
                                        'turnaround_time'       : turnaround_time,
                                        'exampleurlOne'         : example_1,
                                        'exampleurlTwo'         : example_2,
                                        // 'dp_da'                 : dp_da,
                                        // 'dp_pa'                 : dp_pa,
                                        //'form' : form
                                     },
                                  async: true,
                                  dataType: 'json',
                                  enctype: 'multipart/form-data',
                                  cache: false,                      
                                  success: function(response){
                                    $('.loader').fadeOut('slow');
                                        if(response.status == false){
                                            swal.fire({
                                                "title": "",
                                                "text": response.message,
                                                "type": "warning",
                                                "confirmButtonClass": "btn btn-secondary"
                                            });
                                       }
                                       else if( response.status == true){

                                            Swal.fire({
                                              title: "", 
                                              html: response.message,  
                                              type: "success"
                                            }, function() {
                                                window.location.href = response.url;
                                            });

                                            window.location.href = response.url;
                                            
                                       }

                                  },
                                  error: function(){}
                                });
                            e.stopImmediatePropagation();
                            return false; 

                        }
                    });
                }

                return {
                    // public functions
                    init: function() {
                        wizardEl = KTUtil.get('kt_wizard_v2');
                        formEl = $('#kt_form');

                        initWizard();
                        initValidation();
                        initSubmit();
                    }
                };
            }();
            
            jQuery(document).ready(function() {
                KTWizard2.init();
                /** validation for guest example not to match with domain url **/
                jQuery.validator.addMethod("notEqual", function(value, element, param) {
                  return this.optional(element) || value != param;
                }, "Please specify a different value");
                /** end here code **/
                $.validator.addMethod('lessThanEqual', function(value, element, param) {
                    return this.optional(element) || parseInt(value) <= parseInt($(param).val());
                }, "The content cost must be less than number words");
            });


        </script>     
        <script>
            $(document).ready(function(){

                $('.provide_content').on('click',function(){ 
                     if($(this).prop("checked") == true ){
                        if($(this).val() == "seller"){
                            $('#content_row').removeAttr('style');
                        }
                     }
                     else{

                        if($(this).val() == "seller" ){
                            $('#content_row').css('display','none');
                        }
                    } 
                });
                $('.verify_website').on('change',function(){
                    var checked_val = $(this).val();
                    if( checked_val == 'yes'){
                        $('#own_website').show();
                    } 
                    else{
                        $('#own_website').hide();
                    }
                });
            });
        </script>   
        <script>
            $(document).on('keyup','#web_url',function(e) {
                e.preventDefault();
                var webUrl = $(this).val();
                var upd_web_url = webUrl.replace(/^(?:https?:\/\/)?(?:www\.)?/i, "").split('/')[0];
                $('#push_domain').html(upd_web_url);
                $('#email_domain').html('@'+upd_web_url);
            });
            var getVal = $('.verify_website:checked').val();
            $('#push_web').html(getVal);
            $(document).on('change','.verify_website',function(){
                 var select_val = $(this).val();
                  $('#push_web').html(select_val);
            });
            var backlink_type = $('.backlink_type:checked').val();
            $('#push_type').html(backlink_type);
            $(document).on('change','.backlink_type',function(){
                var backlink_type = $(this).val();
                $('#push_type').html(backlink_type);
            });
            $(document).on('keyup','#max_backlinks',function() {
                var max_backlinks = $(this).val();
                 $('#push_backlinks').html(max_backlinks);
            });
            $(document).on('keyup','#number_words',function() {
                var number_words = $(this).val();
                 $('#push_content').html(number_words);
            });
            $(document).on('keyup','#guideline_url',function() {
                var guideline_url = $(this).val();
                 $('#push_guideline').html(guideline_url);
            });
            $(document).on('change','#turnaround_time',function() {
                var turnaround_time = $(this).val();
                 $('#push_turnaround').html(turnaround_time);
            });
            $(document).on('keyup','#example_1',function() {
                var example_1 = $(this).val();
                 $('#push_example_1').html(example_1);
            });
             $(document).on('keyup','#example_2',function() {
                var example_2 = $(this).val();
                 $('#push_example_2').html(example_2);
            });
            $('#push_provide').html($('.provide_content:checked').val());
            $(document).on('change','.provide_content',function(){
                $('#push_provide').html($('.provide_content:checked').val());
            });
            $(document).on('keyup','#number_words',function() {
                var number_words = $(this).val();
                 $('#push_words').html(number_words);
            });
             $(document).on('keyup','#extra_cost',function() {
                var extra_cost = $(this).val();
                 $('#push_cost').html(extra_cost);
            });
             $(document).on('keyup','#your_price',function() {
                var your_price = $(this).val();
                 $('#push_your_price').html(your_price);
            });

        </script>
        <script>
            $(document).on('keyup','#your_price,#extra_cost',function(e) {
                e.preventDefault();
                var extra_cost = Number($('#extra_cost').val());
                var cur = Number($('#your_price').val());
                var total = parseInt(extra_cost, 10) + parseInt(cur, 10);
                if(cur != '' || cur > 0){
                    var fixed_amount = 10;
                    var fixed_percentage = 10;

                    if(extra_cost != '' || extra_cost > 0){
                    cur = Number(cur) + Number(extra_cost);
                    }
                    var newTotal = parseInt(total, 10) + parseInt(10);
                    var webdew_commision_amount = ( ( Number(newTotal) * Number(fixed_percentage) / 100 ) + Number(newTotal)) ;

                    var roundTotal = Math.round(webdew_commision_amount);
                    var remainderVal = (roundTotal % 10);
                    if( remainderVal != 0 ){
                        var lastNo   = parseInt(10) - parseInt(remainderVal, 10);
                        var updTotal = parseInt(lastNo) + parseInt(roundTotal);

                        $('#selling_price').val(updTotal);
                        $('#price_msg').html('You will get  $' +total+ ' and we are going to Publish this website at $'+updTotal);
                        $('#push_selling_price').html(updTotal);
                    }
                    else{

                        $('#selling_price').val(roundTotal);
                        $('#price_msg').html('You will get $' +total+ ' and we are going to Publish this website at $'+updTotal);
                        $('#push_selling_price').html(roundTotal);
                    }
                }   
            });
        </script>
        <script type="text/javascript">
            function ShowHideDiv() {
                var chkYes = document.getElementById("chkYes");
                var dvPassport = document.getElementById("dvPassport");
                dvPassport.style.display = chkYes.checked ? "block" : "none";
            }

            //remove class onclick function
              $("button").click(function(){
              $("#remove").removeClass("disabled");
              });

            function isUrlValid(url) {
                return /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(url);
            }
        </script>
        <script type="text/javascript">
        $(function () {
            $("#verify_domain").keypress(function (e) {
                var keyCode = e.keyCode || e.which;
     
                $("#lblError").html("");
     
                //Regex for Valid Characters i.e. Alphabets and Numbers.
                var regex = /^[aA-zZ0-9-.]+$/;
     
                //Validate TextBox value against the Regex.
                var isValid = regex.test(String.fromCharCode(keyCode));
                if (!isValid) {
                    $("#lblError").html("Only Alphabets and Numbers allowed.");
                }
     
                return isValid;
            });
        });
    </script>
    @endsection
    @show
@endsection
