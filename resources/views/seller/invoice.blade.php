@extends('layout.app')
@section('title', 'Invoice Page - GuestPostEngine')

@section('title-description')

<meta name="description" content="In the Invoice page, an advertiser and a publisher can see all the details of his/her order regarding purchasing a website which includes website name, price of the website, date of order placed, order ID and so on.">
    
@endsection
@section('content')
@section('mainhead')
@parent
    <!--begin::Page Vendors Styles(used by this page) -->
    <link href="{{ asset('') . config('app.public_url') . '/assets/css/pages/wizard/wizard-2.css'  }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('') . config('app.public_url') . '/assets/css/pages/invoices/invoice-2.css'  }}" rel="stylesheet" type="text/css" />
        <!--end::Page Vendors Styles -->
@show
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
            </div>
        </div>
        <!-- end:: Subheader -->
        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">
            <div class="kt-portlet">
                <div class="kt-portlet__body kt-portlet__body--fit">
                    <div class="kt-invoice-2">
                        <div class="kt-invoice__head">
                            <div class="kt-invoice__container">
                                <div class="kt-invoice__brand">
                                    <h1 class="kt-invoice__title">INVOICE(For Publisher)</h1>
                                    <div class="kt-invoice__logo">
                                        <span class="kt-invoice__desc">
                                            @if($invoice_arr['paypal_address'] && $invoice_arr['payment_type'] == 'paypal')
                                            <span>{{ $invoice_arr['paypal_address'] }}</span>
                                            @elseif($invoice_arr['payment_type'] == 'manually')
                                            <span class="text-success">Coupon Code Applied(100%)</span>
                                            @endif
                                        </span>
                                    </div>
                                </div>
                                <div class="kt-invoice__items">
                                    <div class="kt-invoice__item">
                                        <span class="kt-invoice__subtitle">DATE</span>
                                        <span class="kt-invoice__text">{{ date('Y-m-d',strtotime($invoice_arr['created_at'])) }}</span>
                                    </div>
                                    <div class="kt-invoice__item">
                                        <span class="kt-invoice__subtitle">INVOICE NO.</span>
                                        <span class="kt-invoice__text">{{ '#'.$invoice_arr['order_id'] }}</span>
                                    </div>
                                    <div class="kt-invoice__item">
                                        <span class="kt-invoice__subtitle">INVOICE TO.</span>
                                        <span class="kt-invoice__text">{{ $invoice_arr['publisher_fname'] }}<br>{{ $invoice_arr['publisher_lname'] }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="kt-invoice__body">
                            <div class="kt-invoice__container">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Domain</th>
                                                <th style="text-align:left;">Advertiser</th>
                                                <th>Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                            $totAmount = 0;
                                            @endphp
                                            @foreach($data as $key => $invoice_arr1)
                                            @php
                                                $totAmount  = $totAmount + $invoice_arr1['domain_amount'];
                                            @endphp    
                                                <tr>
                                                    <td>{{ $invoice_arr1['domain_url'] }}</td>
                                                    <td style="text-align:left;">{{ $invoice_arr1['publisher_fname'].' '.$invoice_arr1['publisher_lname'] }}</td>
                                                    <td class="kt-font-success kt-font-lg">{{ '$'.$invoice_arr1['domain_amount'].'.00' }}</td>
                                                </tr>
                                            @endforeach
                                            <tr>
                                                <td></td>
                                                <td>Total</td>
                                                <td>${{ $totAmount }}.00</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="kt-invoice__footer">
                            <div class="kt-invoice__container">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>          
                                            <tr>
                                                <th>Merchant ID</th>
                                                <th>Status</th>
                                                @if($invoice_arr['promo_code'] > 0)
                                                    <th>Discount coupon</th>
                                                @endif
                                                <th>Total Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                @if($invoice_arr['payment_type'] == 'paypal')
                                                    <td>{{ $invoice_arr['paypal_merchant_id'] }}</td>
                                                    @if($invoice_arr['paypal_status'] == 1 || $invoice_arr['paypal_status'] == 'COMPLETED')
                                                        <td class="text-success">Success</td>
                                                    @endif
                                                @elseif($invoice_arr['payment_type'] == 'stripe')
                                                    <td>{{ $invoice_arr['stripe_merchant_id'] }}</td>
                                                    <td>{{ $invoice_arr['stripe_status'] }}</td>
                                                @else
                                                    <td>{{'Manualy'}}</td>
                                                    <td class="text-success">Success</td>
                                                @endif
                                                @if($invoice_arr['promo_code'] > 0)
                                                    <?php
                                                        $prmo = \DB::table('promo_codes')->where('promo_id',$invoice_arr['promo_code'])->first();
                                                    ?>
                                                    <td class="text-success"> {{@$prmo->promo_code}}</td>
                                                @endif
                                                <td class="kt-font-success kt-font-xl kt-font-boldest">{{ '$'.$totAmount.'.00' }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        @foreach( $comb_order as $split_comb_order )
                        @if( $split_comb_order['order_status'] == '2' )
                        <div class="kt-invoice__footer">
                            <div class="kt-invoice__container">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Domain</th>
                                                <th style="text-align:left;">Advertiser</th>
                                                <th>Status</th>
                                                <th>Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                            $leftAmount = 0;
                                            @endphp
                                            @foreach($data as $key => $invoice_arr1)
                                                @php
                                                    $leftAmount  = $totAmount - $invoice_arr1['domain_amount'];
                                                @endphp
                                                @if( $invoice_arr1['order_status'] == '2')    
                                                <tr>
                                                    <td>{{ $invoice_arr1['domain_url'] }}</td>
                                                    <td style="text-align:left;">{{ $invoice_arr1['publisher_fname'].' '.$invoice_arr1['publisher_lname'] }}</td>
                                                    <td class="text-danger"><strong>Rejected</strong></td>
                                                    <td class="kt-font-success kt-font-lg">-{{ '$'.$invoice_arr1['domain_amount'].'.00' }}</td>
                                                </tr>
                                                @endif
                                            @endforeach
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td>Total</td>
                                                <td>${{ $leftAmount }}.00</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        @endif  
                        @endforeach
                        <div class="kt-invoice__actions">
                            <div class="kt-invoice__container">
                                <button type="button" class="btn btn-label-brand btn-bold" onclick="window.print();">Download Invoice</button>
                                <button type="button" class="btn btn-brand btn-bold" onclick="window.print();">Print Invoice</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end:: Content -->
</div>       
@section('mainscripts')
@parent
    <!--begin::Page Scripts(used by this page) -->
@show
@endsection