@extends('layout.app')
@section('title', 'See all received order as a Publisher - GuestPostEngine')
@section('title-description')

<meta name="description" content="As a publisher, you can visit my orders page on GuestPostEngine marketplace to see how many orders of websites have you received.">
    
@endsection
@section('content')
@section('mainhead')
@parent
    @section('customCSS')
    <!--begin::Page Vendors Styles(used by this page) -->
    <link href="{{ asset('') . config('app.public_url') . '/assets/css/pages/wizard/wizard-2.css'  }}" rel="stylesheet" type="text/css" />
        <!--end::Page Vendors Styles -->
    @endsection
@show
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
                
            </div>
        </div>

        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">
            <div class="alert alert-light alert-elevate" role="alert">
                <div class="kt-portlet__body w-100">

                <!--begin: Search Form -->
                <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                    <div class="row align-items-center">
                            <div class="col-xl-8 order-2 order-xl-1">
                                <div class="row align-items-center">
                                    <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                    <div class="kt-form__label">
                                            <label>Search:</label>
                                        </div>
                                        <div class="kt-input-icon kt-input-icon--left">
                                            <input type="text" class="form-control" placeholder="Search..." id="generalSearch">
                                            <span class="kt-input-icon__icon kt-input-icon__icon--left">
                                                <span><i class="la la-search"></i></span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-4 kt-margin-b-20-tablet-and-mobile">
                                        
                                            <div class="kt-form__label">
                                                <label>Payment Status:</label>
                                            </div>
                                            <div class="kt-form__control">
                                                <select class="form-control" id="kt_form_status">
                                                    <option value="">All</option>

                                                    <option value="1">Paid</option>              
                                                    <option value="2">Unpaid</option>
                                                </select>
                                            </div>
                                       
                                    </div>

                                </div>
                            </div>
                           
                        </div>
                    </div>
                    <!--end: Search Form -->
                </div>              
            </div>
                                
            <div class="kt-portlet kt-portlet--mobile">
                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">

                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect x="0" y="0" width="24" height="24"></rect>
                                    <path d="M8,3 L8,3.5 C8,4.32842712 8.67157288,5 9.5,5 L14.5,5 C15.3284271,5 16,4.32842712 16,3.5 L16,3 L18,3 C19.1045695,3 20,3.8954305 20,5 L20,21 C20,22.1045695 19.1045695,23 18,23 L6,23 C4.8954305,23 4,22.1045695 4,21 L4,5 C4,3.8954305 4.8954305,3 6,3 L8,3 Z" fill="#000000" opacity="0.3"></path>
                                    <path d="M11,2 C11,1.44771525 11.4477153,1 12,1 C12.5522847,1 13,1.44771525 13,2 L14.5,2 C14.7761424,2 15,2.22385763 15,2.5 L15,3.5 C15,3.77614237 14.7761424,4 14.5,4 L9.5,4 C9.22385763,4 9,3.77614237 9,3.5 L9,2.5 C9,2.22385763 9.22385763,2 9.5,2 L11,2 Z" fill="#000000"></path>
                                    <rect fill="#000000" opacity="0.3" x="10" y="9" width="7" height="2" rx="1"></rect>
                                    <rect fill="#000000" opacity="0.3" x="7" y="9" width="2" height="2" rx="1"></rect>
                                    <rect fill="#000000" opacity="0.3" x="7" y="13" width="2" height="2" rx="1"></rect>
                                    <rect fill="#000000" opacity="0.3" x="10" y="13" width="7" height="2" rx="1"></rect>
                                    <rect fill="#000000" opacity="0.3" x="7" y="17" width="2" height="2" rx="1"></rect>
                                    <rect fill="#000000" opacity="0.3" x="10" y="17" width="7" height="2" rx="1"></rect>
                                </g>
                            </svg>
                        </span>
                        <h3 class="kt-portlet__head-title">
                            Orders
                        </h3>
                    </div>
                </div>              
                <div class="kt-portlet__body kt-portlet__body--fit">

                    <!--begin: Datatable -->
                    <div class="kt-datatable" id="local_data"></div>

                    <!--end: Datatable -->
                </div>
            </div>
        </div>
        <!-- end:: Content -->
    </div>
</div>
@section('mainscripts')
@parent

@section('customScript')
<?php if(!empty( $message )){ ?>
    <p style="text-align: center;">{{$message}}</p>
<?php } else{ ?>
<!--begin::Page Vendors Styles(used by this page) -->     
<script>
    $(document).ready(function(){
        $(document).on('click', '.link_redirect', function(e){ 
            e.preventDefault(); 
            var url = $(this).attr('href'); 
            window.open(url, '_blank');
        });
    });
</script>
<script>
   'use strict';
    // Class definition
    var KTDatatableDataLocalDemo = function() {
        // Private functions
        var data = '<?php echo json_encode( $orderLists ); ?>';
        //console.log(data);
        // var split_data = JSON.parse(data);
        // var order_id   = split_data[0]['id']; 
        // var json        = $.parseJSON(data);
        // $(json).each(function(i,val){
        //     $.each(val,function(k,v){
        //  //   console.log(k+" : "+ v);       
        // });
        // });
       // console.log('Data '+json);
        
        // var newdata =  JSON.parse(data);
        // demo initializer
        var demo = function() {

            var dataJSONArray = JSON.parse(data);
            var datatable = $('.kt-datatable').KTDatatable({
                // datasource definition
                data: {
                    type: 'local',
                    source: dataJSONArray,
                    pageSize: 10,
                },

                // layout definition
                layout: {
                    scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
                    // height: 450, // datatable's body's fixed height
                    footer: false, // display/hide footer
                },

                // column sorting
                sortable: true,

                pagination: true,

                search: {
                    input: $('#generalSearch'),
                },

                // columns definition
                columns: [
                    {
                        field: 'id',
                        title: '#',
                        sortable: false,
                        width: 20,
                        type: 'number',
                        selector: {class: 'kt-checkbox--solid'},
                        textAlign: 'center',
                    },
                    {
                        field: 'user_code',
                        title: 'Advertiser',
                        width: 150,
                    },
                    {
                        field: 'domain_url',
                        title: 'Advertiser URL',
                        width: 300,           
                    },
                    {
                        field: 'cost_price',
                        title: 'Price',              
                        template: function(row) {

                            // if(row.cost_price > 0){
                            //     return '$'+row.cost_price+row.id;
                            // }
                            return '$'+row.cost_price;
                        },
                    },
                    {
                        field: 'extra_cost',
                        title: 'Extra cost',
                        // callback function support for column rendering
                        template: function(row) {

                            if(row.extra_cost > 0){
                                return '$'+row.extra_cost;
                            }
                            return row.extra_cost;
                        },
                    },
                    {
                        field: 'PaymentStatus',
                        title: 'Payment Status',
                        // callback function support for column rendering
                        template: function(row) {
                            var status = {
                                1: {'title': 'Paid', 'class': 'kt-badge--success'},
                                2: {'title': 'Unpaid', 'class': ' kt-badge--danger'},
                            };
                            return '<span class="kt-badge ' + status[row.PaymentStatus].class + ' kt-badge--inline kt-badge--pill">' + status[row.PaymentStatus].title + '</span>';
                            
                        },
                    },
                    {
                        field: 'is_accept',
                        title: 'Order Status',
                        sortable: false,
                        width: 150,
                        overflow: 'visible',
                        autoHide: false,
                        // callback function support for column rendering
                        template: function(row) {
                            var status = {
                                0: {'title': 'Order Received', 'class': ' kt-badge--warning'},
                                1: {'title': 'Content not submitted yet', 'class': ' kt-badge--warning'},
                                2: {'title': 'Rejected', 'class': 'kt-badge--danger'},
                                3: {'title': 'Cancelled', 'class': 'kt-badge--danger'},
                            };
                            if(row.is_accept == 0){
                                return '<span class="kt-badge ' + status[row.is_accept].class + ' kt-badge--inline kt-badge--pill">' + status[row.is_accept].title + '</span>';
                            } else if(row.is_accept == 1){

                                if(row.stages > 3){

                                    switch(row.stages) {
                                      case '4':
                                            return '<span class="kt-badge kt-badge--warning kt-badge--inline kt-badge--pill">Content approval pending</span>';
                                        break;
                                      case '5':
                                            return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">Content approved</span>';
                                        break;
                                      case '6':
                                            return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">Content rejected</span>';
                                        break;
                                      case '7':
                                           // live link update
                                           return '<span class="kt-badge kt-badge--warning kt-badge--inline kt-badge--pill">live link pending for approval</span>';
                                            
                                        break;
                                      case '8':
                                           // live link update
                                           return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">live link approved</span>';
                                        break;
                                      case '9':
                                           // live link update
                                           return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">live link Rejected</span>';
                                        break;
                                      case '10':
                                        // order delever
                                            return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">Order Delivered</span>';
                                        break;
                                      case '11':
                                            if(row.pay_to_seller_id < 1) {
                                               // order delever
                                                return '<span class="kt-badge kt-badge--warning kt-badge--inline kt-badge--pill">Payment release after 15 days </span>';

                                            } else {
                                                // order delever
                                                return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">Payment released </span>';
                                            } 
                                        break;

                                    }
                                    
                                } else {
                                    return '<span class="kt-badge ' + status[row.is_accept].class + ' kt-badge--inline kt-badge--pill">' + status[row.is_accept].title + '</span>';
                                }
                                
                            } else {
                                return '<span class="kt-badge ' + status[row.is_accept].class + ' kt-badge--inline kt-badge--pill">' + status[row.is_accept].title + '</span>';
                            }
                        },
                    },
                    {
                        field: 'content_type',
                        title: 'Content type',
                        template: function(row) {
                            var status = {
                                'buyer': {'title': 'Advertiser', 'class': ' kt-badge--success'},
                                'seller': {'title': 'Publisher', 'class': 'kt-badge--success'},
                            };
                            if(row.content_type == 'seller'){
                                return '<span class="kt-badge ' + status[row.content_type].class + ' kt-badge--inline kt-badge--pill">' + status[row.content_type].title + '</span>';
                            }

                            return '<span class="kt-badge ' + status[row.content_type].class + ' kt-badge--inline kt-badge--pill">' + status[row.content_type].title + '</span>';
                            
                        },              
                    },
                    {
                        field: 'OrderDate',
                        title: 'Order Date',
                        type: 'date',
                        format: 'YYYY-MM-DD',
                        width: 150,
                    },
                    {
                        field: 'amount',
                        title: 'Total', 
                        template: function(row) {

                            if(row.amount > 0){
                                return '$'+row.amount;
                            }
                            return row.amount;
                        },            
                    },                   
                    
                    
                    {
                        field: 'payment_type',
                        title: 'Payment Type',
                        template: function(row) {
                            var status = {
                                'stripe-save-card': {'title': 'Stripe Card', 'class': ' kt-badge--success'},
                                'stripe': {'title': 'Stripe', 'class': 'kt-badge--success'},
                                'manually': {'title': 'Manually', 'class': 'kt-badge--warning'},
                                'paypal': {'title': 'Paypal', 'class': 'kt-badge--success'},
                                'wallet': {'title': 'Wallet', 'class': 'kt-badge--success'},
                            };
                            return '<span class="kt-badge ' + status[row.payment_type].class + ' kt-badge--inline kt-badge--pill">' + status[row.payment_type].title + '</span>';
                        },
                    },
                    {
                        field: 'refund',
                        title: 'Refunded status',
                        // callback function support for column rendering
                        template: function(row) {
                            var status = {
                                1: {'title': 'NA', 'class': ' kt-badge--warning'},
                                2: {'title': 'Refunded', 'class': 'kt-badge--danger'},
                                3: {'title': 'Never charged before', 'class': 'kt-badge--warning'},
                            };
                            return '<span class="kt-badge ' + status[row.refund].class + ' kt-badge--inline kt-badge--pill">' + status[row.refund].title + '</span>';
                        },
                    },
                    {
                        field: 'refund_amount',
                        title: 'Refunded amount',
                        // callback function support for column rendering
                        template: function(row) {
                            var status = {
                                'NA': {'title': 'NA', 'class': ' kt-badge--warning'}
                            };
                            if(row.refund_amount == 'NA'){
                                return '<span class="kt-badge ' + status[row.refund_amount].class + ' kt-badge--inline kt-badge--pill">' + status[row.refund_amount].title + '</span>';    
                            } else {
                                return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">' + row.refund_amount + '</span>';
                            }
                            
                        },
                    },
                    {
                        field: 'promo_code',
                        title: 'Promo Code',
                        // callback function support for column rendering
                        template: function(row) {
                            var status = {
                                '0': {'title': 'NA', 'class': ' kt-badge--warning'}
                            };
                            if(row.promo_code == 0){
                                return '<span class="kt-badge ' + status[row.promo_code].class + ' kt-badge--inline kt-badge--pill">' + status[row.promo_code].title + '</span>';    
                            } else {
                                return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">' + row.promo_code + '</span>';
                            }
                            
                        },
                    },

                    {
                        field: 'url',
                        title: 'URL',
                        width: 300,
                        template: function(row) {
                            var status = {
                                0: {'title': 'NA', 'class': ' kt-badge--warning'}
                            };
                            if(row.url == '' || row.url == null) {
                                return '<span class="kt-badge ' + status[0].class + ' kt-badge--inline kt-badge--pill">' + status[0].title + '</span>';
                            } else {                                    
                                return '<a href="'+row.url+'" target="_blank"><span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">click here</span></a>';
                            }
                        },
                    },
                    {
                        field: 'keyword',
                        title: 'Keyword',             
                        template: function(row) {
                            var status = {
                                0: {'title': 'NA', 'class': ' kt-badge--warning'}
                            };
                            if(row.keyword == '' || row.keyword == null) {
                                return '<span class="kt-badge ' + status[0].class + ' kt-badge--inline kt-badge--pill">' + status[0].title + '</span>';
                            } else {
                                return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">'+row.keyword+'</span>';
                            }
                        },
                    },
                    {
                        field: 'title',
                        title: 'Title',             
                        template: function(row) {
                            var status = {
                                0: {'title': 'NA', 'class': ' kt-badge--warning'}
                            };
                            if(row.title == '' || row.title == null) {
                                return '<span class="kt-badge ' + status[0].class + ' kt-badge--inline kt-badge--pill">' + status[0].title + '</span>';    
                            } else {
                                return '<span class="kt-badge kt-badge--success kt-badge--inline kt-badge--pill">'+row.title+'</span>';
                            }
                        },
                    }, 
                    {
                        field: 'Invoice',
                        title: 'Invoice',
                        // sortable: false,
                        // width: 110,
                        // overflow: 'visible',
                        // autoHide: false,
                        template: function(row) {
                            return '<a href="'+row.invoiceUrl+'/'+row.encrypt_id+'" class="link_redirect"><span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">Invoice</span><a>';
                        }
                    },
                    {
                        field: 'Actions',
                        title: 'Actions',
                        sortable: false,
                        width: 160,
                        overflow: 'visible',
                        autoHide: false,
                        template: function(row) {
                            var id = row.id;
                            if(row.is_accept == 0){

                                if(row.is_billed == 5 || row.is_billed == 1){
                                    return '<a href="#" data-orderid="'+id+'" class="status_active"  data-status="1" ><span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">Accept</span></a> | <a href="#" class="status_active" data-orderid="'+id+'"  data-status="0"><span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">Reject</span></a>';
                                } else {
                                    return '<span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">Unpaid</span>';
                                }
                            } else if(row.is_accept == 1) {
                                
                                switch(row.stages) {
                                  case '0':
                                        // not submitted yet
                                        if(row.content_type == 'seller'){
                                            return '<a href="{{url("publisher")}}'+'/content/'+row.encrypt_id+'"><span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">Content submit here</span></a>';
                                        }
                                        // 0 to 4 after submision
                                    break;
                                  case '4':
                                        if(row.content_type == 'buyer'){
                                            return '<a href="{{url("publisher")}}'+'/content/'+row.encrypt_id+'"><span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">View content</span></a>';
                                        }
                                        //approve 4 to 5
                                        // not apporve 4 to 6
                                    break;
                                  case '5':
                                       return '<a href="#" class="live_link_update" data-case-from="5" data-case-to="7" rel="'+row.encrypt_id+'"><span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">Live link update</span></a>';
                                       // 5 to 7 after updated live link
                                  break;
                                  case '6':
                                        if(row.content_type == 'seller') {
                                            return '<a href="{{url("publisher")}}'+'/content/'+row.encrypt_id+'"><span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">Content Re-submission here</span></a>';
                                        }
                                    break;
                                  case '8':
                                    // live link update
                                        return '<a href="#" class="order_deliver" data-case-from="8" data-case-to="10" rel="'+row.encrypt_id+'"><span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">Click here for Deliver this order</span></a>';
                                    break;
                                  case '9':
                                    // live link update
                                        return '<a href="#" class="re_submission_link" data-case-from="9" data-case-to="7"  data-comment = "'+row.comment+'" rel="'+row.encrypt_id+'"><span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">Re-submission live link</span></a>';
                                        // after submission 9 to 7
                                    break;
                                    // case '11':
                                    //   return '<a href="{{url("publisher")}}'+'/reviews/'+row.encrypt_id+'"><span class="kt-badge kt-badge--brand kt-badge--inline kt-badge--pill">Add Reviews</span></a>';
                                    // break;
                                } 
                                
                            } else if(row.is_accept == 2) {
                                return '<a href="javascript:void(0);"><span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill">Rejected</span></a>';
                            }                            
                        }, 

                    }],
            });

            $('#kt_form_status').on('change', function() {
                datatable.search($(this).val().toLowerCase(), 'PaymentStatus');
            });

            $('#kt_form_type').on('change', function() {
                datatable.search($(this).val().toLowerCase(), 'Type');
            });

            $('#kt_form_status,#kt_form_type').selectpicker();
        };

        

        return {
            // Public functions
            init: function() {
                // init dmeo
                demo();
            },
        };
    }();

jQuery(document).ready(function() {
    KTDatatableDataLocalDemo.init();
});
</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script>
    $(document).on('click','.status_active',function(e){
        var dataStatus = $(this).attr('data-status');
        var order_id  = $(this).attr('data-orderid');
        $('.loader').fadeIn('slow');
        if( dataStatus == '1'){
            
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url  : "{{ url('/publisher/order/accept') }}",
                type : 'POST',
                data : {
                    'order_id' : order_id,
                },
                async: true,
                dataType: 'json',
                enctype: 'multipart/form-data',
                cache: false, 
                success:function(response){
                     if(response.success == true){
                         swal.fire({
                            "title": "",
                            "text": response.message,
                            "type": "success",
                            "confirmButtonClass": "btn btn-secondary"
                        });
                        $('.loader').fadeOut('slow');
                     } else {
                        swal.fire({
                            "title": "",
                            "text": "something were wrong.",
                            "type": "warning",
                            "confirmButtonClass": "btn btn-secondary"
                        });
                        $('.loader').fadeOut('slow');
                     }
                    window.location.href = "{{url('publisher/orders')}}";
                },
                error:function(){
                    $('.loader').fadeOut('slow');
                    swal.fire({
                            "title": "",
                            "text": "something were wrong",
                            "type": "danger",
                            "confirmButtonClass": "btn btn-secondary"
                        });
                }
            });
        } else {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url  : "{{ url('/publisher/order/reject') }}",
                type : 'POST',
                data : {
                    'order_id' : order_id,
                },
                async: true,
                dataType: 'json',
                enctype: 'multipart/form-data',
                cache: false,
                success:function(response){
                     if(response.success == false){
                         swal.fire({
                            "title": "",
                            "text": "Order has been Rejected",
                            "type": "success",
                            "confirmButtonClass": "btn btn-secondary"
                        });
                     } else {
                        swal.fire({
                            "title": "",
                            "text": "something wrong.",
                            "type": "warning",
                            "confirmButtonClass": "btn btn-secondary"
                        });
                        $('.loader').fadeOut('slow');
                     }
                    window.location.href = "{{url('publisher/orders')}}";
                },
                error:function(){
                    $('.loader').fadeOut('slow');
                    swal.fire({
                            "title": "",
                            "text": "something were wrong",
                            "type": "danger",
                            "confirmButtonClass": "btn btn-secondary"
                        });
                }       
            });
        }

    e.stopImmediatePropagation();
    return false;
    });
</script> 
<script>
    $(document).on('click','.live_link_update',function(e){
            
    var cart_id = $(this).attr('rel');
    Swal.fire({
      title: 'Are you sure?',
      html: "You want to add the link?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonClass: 'btn btn-secondary',
      cancelButtonClass: 'btn btn-secondary',
      confirmButtonText: 'Yes'

    }).then((result) => {
        
            if (result.value) {
               Swal.fire({
          title: 'Add Link',
          input: 'url',
          inputAttributes: {
            autocapitalize: 'off'
          },
          showCancelButton: true,
          confirmButtonText: 'Done',
          showLoaderOnConfirm: true,
          preConfirm: (login) => {
                if(login == ''){
                    Swal.showValidationMessage(
                      `Request failed: Not found`
                    )
                    return false;
                } 
          },
          allowOutsideClick: () => !Swal.isLoading()
        }).then((result) => {
            if (result.value) {
                var url = "{{url('stages-changes_1')}}";
                $('.loader').fadeIn('slow'); 
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url  : url,
                    type : 'POST',
                    data : {
                        'cart_id'       : cart_id,
                        'stages'        : 7,
                        'link'          : result.value,
                    },
                    async: true,
                    dataType: 'json',
                    enctype: 'multipart/form-data',
                    cache: false, 
                    success:function(response){
                        $('.loader').fadeOut('slow'); 
                        if(response.status === true) {
                            swal.fire({
                              title: '',
                              html: "Link has been submitted",
                              icon: 'success',
                              showCancelButton: false,
                              confirmButtonClass: "btn btn-secondary",
                              confirmButtonText: "Ok"
                            }).then((result) => {
                               window.location.href = "{{url('publisher/orders')}}";
                            });
                        } 
                    },
                    error:function(){
                        Swal.showValidationMessage(
                          `Request failed: Something were wrong`
                        )
                    }
                });
            }
        }); 
            }
    });
    return false;

    });
</script>
<script>
    $(document).on('click','.re_submission_link',function(e){
            
    var cart_id = $(this).attr('rel');
    if($(this).attr('data-comment') != ''){
        var comment = $(this).attr('data-comment');
    }
    Swal.fire({
      title: "Please check Comment :"+comment,
      html: 'Do You want to Resubmission of link?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonClass: 'btn btn-secondary',
      cancelButtonClass: 'btn btn-secondary',
      confirmButtonText: 'Yes'

    }).then((result) => {
        
            if (result.value) {
               Swal.fire({
          title: 'Add Link',
          input: 'url',
          inputAttributes: {
            autocapitalize: 'off'
          },
          showCancelButton: true,
          confirmButtonText: 'Done',
          showLoaderOnConfirm: true,
          preConfirm: (login) => {
                if(login == ''){
                    Swal.showValidationMessage(
                      `Request failed: Not found`
                    )
                    return false;
                } 
          },
          allowOutsideClick: () => !Swal.isLoading()
        }).then((result) => {
            if (result.value) {
                var url = "{{url('stages-changes_1')}}";
                $('.loader').fadeIn('slow'); 
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url  : url,
                    type : 'POST',
                    data : {
                        'cart_id'       : cart_id,
                        'stages'        : 7,
                        'link'          : result.value,
                    },
                    async: true,
                    dataType: 'json',
                    enctype: 'multipart/form-data',
                    cache: false, 
                    success:function(response){
                        $('.loader').fadeOut('slow'); 
                        if(response.status === true) {
                            swal.fire({
                              title: '',
                              html: "Link has been submitted",
                              icon: 'success',
                              showCancelButton: false,
                              confirmButtonClass: "btn btn-secondary",
                              confirmButtonText: "Ok"
                            }).then((result) => {
                               window.location.href = "{{url('publisher/orders')}}";
                            });
                        } 
                    },
                    error:function(){
                        Swal.showValidationMessage(
                          `Request failed: Something were wrong`
                        )
                    }
                });
            }
        }); 
            }
    });
    return false;

    });
</script>
<script>
$(document).on('click','.order_deliver',function(e){
            
    var cart_id = $(this).attr('rel');
    Swal.fire({
      title: 'Are you sure?',
      html: "You want to deliver the order",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonClass: 'btn btn-secondary',
      cancelButtonClass: 'btn btn-secondary',
      confirmButtonText: 'Yes'

    }).then((result) => {
        
            if (result.value) {
               Swal.fire({
          html : 'Give your confirmation',
          inputAttributes: {
            autocapitalize: 'off'
          },
          showCancelButton: true,
          confirmButtonText: 'Done',
          showLoaderOnConfirm: true,
          preConfirm: (login) => {
                if(login == ''){
                    Swal.showValidationMessage(
                      `Request failed: Not found`
                    )
                    return false;
                } 
          },
          allowOutsideClick: () => !Swal.isLoading()
        }).then((result) => {
            if (result.value) {
                var url = "{{url('/order_deliver')}}";
                $('.loader').fadeIn('slow'); 
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url  : url,
                    type : 'POST',
                    data : {
                        'cart_id'       : cart_id,
                        'stages'        : 10,
                    },
                    async: true,
                    dataType: 'json',
                    enctype: 'multipart/form-data',
                    cache: false, 
                    success:function(response){
                        $('.loader').fadeOut('slow'); 
                        if(response.status === true) {
                            swal.fire({
                              title: '',
                              html: "Order has been delivered successfully!!",
                              icon: 'success',
                              showCancelButton: false,
                              confirmButtonClass: "btn btn-secondary",
                              confirmButtonText: "Ok"
                            }).then((result) => {
                               window.location.href = "{{url('publisher/orders')}}";
                            });
                        } 
                    },
                    error:function(){
                        Swal.showValidationMessage(
                          `Request failed: Something were wrong`
                        )
                    }
                });
            }
        }); 
            }
    });
    return false;

    });
</script>
<!-- script to add reviews -->
<script>
    // $(document).on('click','.add_reviews',function(e){
        
    //     Swal.fire({
    //       title: 'Are you sure?',
    //       html: "You want to deliver the order",
    //       icon: 'warning',
    //       showCancelButton: true,
    //       confirmButtonClass: 'btn btn-secondary',
    //       cancelButtonClass: 'btn btn-secondary',
    //       confirmButtonText: 'Yes'

    //     }).then((result) => {
            
    //         if (result.value) {
    //             alert('true');
    //         }
    //         else{
    //             alert('false');
    //         }
    //     });
    //     return false;

    // });
</script>
<!-- end here script for reviews -->
 <?php } ?>           
<!--end::Page Vendors Styles -->
@endsection
@show
@endsection
