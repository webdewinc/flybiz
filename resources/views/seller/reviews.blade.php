@extends('layout.app')
@php
$totalTitle = ' Looking for reliable website details on GuestPostEngine?';
@endphp
@section('title', $totalTitle)
@section('content')
@section('mainhead')
@parent

@section('title-description')

<meta name="description" content="Get the appropriate website details on GuestPostEngine by exploring a wide range of quality websites with high domain authority, category rank, etc.">
    
@endsection
<link rel="stylesheet" href="{{ asset('') . config('app.public_url') . '/css/jsRapStar.css' }}" />
<!-- <link rel="shortcut icon" href="https://www.guestpostengine.com/images/favicon.ico" /> -->
@show
<!-- Authentication Links --> 
@if(session()->has('message'))
    <?php
        $form = session()->get('form');
        if($form == ''){
            $form = 'signin';
            $result = 1;
            $message = 'Password has been changed.';
            $alert = 'success';
        } else {
            $result = 0;
        }
        
    ?>
@else 
    <?php
        $form = 'signin';
        $result = 0;
    ?>
@endif
<style>
.checked {
  color: orange;
}
</style>
<div class="kt-body website-details-wrapper kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
            </div>
        </div>
        <div class="kt-container  kt-grid__item kt-grid__item--fluid">
            <!--begin::Portlet-->
            <div class="" data-sticky-container>
                <div class="row position-relative">
                    <div class="col-12 col-lg-1"></div>
                    <div class="col-12 col-lg-10">
                        <div class="kt-portlet ">
                            <div class="kt-portlet__body">
                                <form method="POST">
                                    <h2 class="pulish_title">Submit Reviews</h2>
                                    <input type="hidden"  name="cart_id" id="cart_id" value="{{ $cart_id }}" />
                                        <!--user reviews -->
                                        <div id="divMain">
                                             <input type="hidden" name="user_reviews" id="user_reviews" value="" />
                                             <h3>User Reviews</h3>:<div id="demo5" value="0"></div>
                                        </div>
                                        <!-- End user reviews -->
                                        <!-- website reviews -->
                                        <div id="divMain">
                                             <input type="hidden" name="website_reviews" id="website_reviews" value="" />                                        
                                             <h3>Website Reviews</h3>:<div id="demo6" value="0"></div>
                                        </div>
                                        <!-- End website reviews -->
                                    <div class="text-center">     
                                        <input type="submit" class="btn btn-primary" id="submit_reviews"  name="submit_reviews" value="Submit Reviews" />
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-1"></div>
                </div>
            </div>
            
            <!--end::Portlet-->
        </div>
    </div>
</div>
<!--ENd:: Chat-->
@section('mainscripts')
@parent
@section('customScript')
<script src="{{ asset('') . config('app.public_url') . '/js/jsRapStar.js' }}"></script>
<script src="https://cdn.ckeditor.com/4.9.2/standard/ckeditor.js"></script> 
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>      
<!-- Include CKEditor and jQuery adapter -->  
<!--begin::Page Scripts(used by this page) -->
<script src="{{ asset('') . config('app.public_url') . '/assets/js/pages/custom/login/login-general.js'  }}" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<!--end::Page Scripts -->
<script>
    $(document).ready(function(){
    $('#demo5').jsRapStar({colorFront:'red',length:10,starHeight:64,step:false,
    onClick:function(score){
        $(this)[0].StarF.css({color:'red'});
        $('#user_reviews').val(score);
        alert(score);
    },
    onMousemove:function(score){
        $(this).attr('title','Rate '+score);
    },
    onMouseleave(score){
        $('#label').text(score);
    }
    });
    $('#demo6').jsRapStar({colorFront:'red',length:10,starHeight:64,step:false,
    onClick:function(score){
        $(this)[0].StarF.css({color:'red'});
        $('#website_reviews').val(score);
        alert(score);
    },
    onMousemove:function(score){
        $(this).attr('title','Rate '+score);
    },
    onMouseleave(score){
        $('#label').text(score);
    }
    });
    });
</script>
<script>
    $(document).on('click','#submit_reviews',function(){
        var user_reviews    = $('#user_reviews').val();
        var website_reviews = $('#website_reviews').val();
        var cart_id         = $('#cart_id').val();

        if(user_reviews == '' &&  website_reviews == ''){

            swal.fire('Please select the star Rating');
        }
        else{
            
            $.ajax({
                headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url  : "{{ url('/publisher/add_reviews_db') }}",
                type : 'POST',
                data: {
                    'user_reviews'    : user_reviews,
                    'website_reviews' : website_reviews,
                    'cart_id'         : cart_id,
                },
                async: true,
                dataType: 'json',
                enctype: 'multipart/form-data',
                cache: false,                      
                success: function(response){
                     alert(response);
                },
                error: function(){}
            });
            return false; 
        }
        return false;
    });
</script>
@endsection
@show
@endsection