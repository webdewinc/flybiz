@extends('layout.app')
@section('title', 'Contact Us - GuestPostEngine')
@section('title-description')

<meta name="description" content="If you have any queries or doubts, please call us on +1 617 936 8707 or even you can email us at go@fly.biz. GuestPostEngine team is always available to resolve all your queries.">
    
@endsection
@section('content')
@section('mainhead')
@parent
    <!--begin::Page Vendors Styles(used by this page) -->
    <link href="{{ asset('') . config('app.public_url') . '/assets/css/owl.carousel.min.css' }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('') . config('app.public_url') . '/assets/css/pages/pricing/pricing-2.css' }}" rel="stylesheet" type="text/css" />
        <!--end::Page Vendors Styles -->
@show
<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor bg-white" id="kt_content">
        <div class="banner-wrapper text-center contact-banner-wrapper kt-mb-30">
            <h2 class="">Get in touch and let us know how we can help.</h2>
            <div class="d-flex kt-mt-30">
               
                  <div class="kt-mr-40">
                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                      <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                          <rect x="0" y="0" width="24" height="24"></rect>
                          <path d="M12.2674799,18.2323597 L12.0084872,5.45852451 C12.0004303,5.06114792 12.1504154,4.6768183 12.4255037,4.38993949 L15.0030167,1.70195304 L17.5910752,4.40093695 C17.8599071,4.6812911 18.0095067,5.05499603 18.0083938,5.44341307 L17.9718262,18.2062508 C17.9694575,19.0329966 17.2985816,19.701953 16.4718324,19.701953 L13.7671717,19.701953 C12.9505952,19.701953 12.2840328,19.0487684 12.2674799,18.2323597 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.701953, 10.701953) rotate(-135.000000) translate(-14.701953, -10.701953) "></path>
                          <path d="M12.9,2 C13.4522847,2 13.9,2.44771525 13.9,3 C13.9,3.55228475 13.4522847,4 12.9,4 L6,4 C4.8954305,4 4,4.8954305 4,6 L4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 L2,6 C2,3.790861 3.790861,2 6,2 L12.9,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                      </g>
                  </svg>
                  <a href="https://blog.fly.biz" class="kt-mt-5">Blog</a>
                  </div>
                  <div class="kt-mr-40">
                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                      <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                          <polygon points="0 0 24 0 24 24 0 24"></polygon>
                          <path d="M18,14 C16.3431458,14 15,12.6568542 15,11 C15,9.34314575 16.3431458,8 18,8 C19.6568542,8 21,9.34314575 21,11 C21,12.6568542 19.6568542,14 18,14 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                          <path d="M17.6011961,15.0006174 C21.0077043,15.0378534 23.7891749,16.7601418 23.9984937,20.4 C24.0069246,20.5466056 23.9984937,21 23.4559499,21 L19.6,21 C19.6,18.7490654 18.8562935,16.6718327 17.6011961,15.0006174 Z M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero"></path>
                      </g>
                  </svg>
                  <a href="{{url('about')}}" class="kt-mt-5">About us</a>
                </div>
                <div class="kt-mr-40">
                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <rect x="0" y="0" width="24" height="24"></rect>
        <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10"></circle>
        <path d="M12,16 C12.5522847,16 13,16.4477153 13,17 C13,17.5522847 12.5522847,18 12,18 C11.4477153,18 11,17.5522847 11,17 C11,16.4477153 11.4477153,16 12,16 Z M10.591,14.868 L10.591,13.209 L11.851,13.209 C13.447,13.209 14.602,11.991 14.602,10.395 C14.602,8.799 13.447,7.581 11.851,7.581 C10.234,7.581 9.121,8.799 9.121,10.395 L7.336,10.395 C7.336,7.875 9.31,5.922 11.851,5.922 C14.392,5.922 16.387,7.875 16.387,10.395 C16.387,12.915 14.392,14.868 11.851,14.868 L10.591,14.868 Z" fill="#000000"></path>
    </g>
</svg>
                  <a href="https://help.fly.biz/" class="kt-mt-5">Help</a>
                </div>
            </div>
        </div>
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container ">
            </div>
        </div>
        <div class="kt-container  kt-grid__item kt-grid__item--fluid bg-white contact-conatiner">
            <div class="">
                <div class="row h-100 pb-4">
                    <div class="col-md-6 col-12">
                        <div class="kt-mb-30">
                            <div class="kt-mb-20">
                                <h5>Call us</h5>
                                <a href="tel:+1 617 936 8707">+1 617 936 8707</a>
                            </div>
                            <div class="kt-mb-20">
                                <h5>Email Us</h5>
                                <a href="mailto:go@fly.biz">go@fly.biz</a>
                            </div>
                            <div class="kt-mb-20">
                                <h5>Visit Us</h5>
                                <p><span>1700 Westlake Ave N Ste. 200</span><br><span>Seattle, WA 98109</span>
                                    <br><span>United States</span></p>
                            </div>
                        </div>
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2688.5688630582163!2d-122.34179158412765!3d47.63451139404223!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x54901517f3cd368d%3A0x26f1bee1b90ffe72!2s1700%20Westlake%20Ave%20N%20%23200%2C%20Seattle%2C%20WA%2098109%2C%20USA!5e0!3m2!1sen!2sin!4v1582720200674!5m2!1sen!2sin" width="450" height="250" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                    </div>
                    <div class="col-md-6 col-12">
                        <div class="kt_blog-subscribe-main kt-widget5__item kt-portlet kt-iconbox--primary kt-iconbox--animate-slow flex-row justify-content-center align-items-center h-100">
                            <div class="kt_blog-subscribe-inner w-100 blog-subscribe-form">
                                <form class="kt-form" id="kt_form" novalidate="novalidate">
                                    <!--begin: Form Wizard Step 1-->
                                    <div class="kt-wizard-v3__content text-center" data-ktwizard-type="step-content" data-ktwizard-state="current">
                                        <div class="kt-mb-30">
                                            <h3>Contact GuestPostEngine</h3>
                                            <p>LET'S CHAT! Fill this out so we can learn more about you and your needs.</p>
                                        </div>
                                        <!--[if lte IE 8]>
                                        <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
                                        <![endif]-->
                                        <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
                                        <script>
                                        hbspt.forms.create({
                                            portalId: "6715790",
                                            formId: "02340ca4-9d82-4bc5-84c1-aa2b96f501fa"
                                        });
                                        </script>
                                    </div>
                                    <!--end: Form Wizard Step 1-->
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@section('mainscripts')
@parent

@show
@endsection
