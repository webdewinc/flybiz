@extends('layout.website')
@php
$totalTitle = 'List of ' . total_website_count() . ' websites that accept Guest Blogs by GuestPostEngine.';
@endphp
@section('title', $totalTitle)
@section('title-description')

<meta name="description" content="GuestPostEngine is one of the remarkable marketplace where you get tons of website opportunities. Feel free to reach us!">
    
@endsection
@section('content')
@section('mainhead')
@parent    
@show

@section('customCSS')
<!--begin::Page Vendors Styles(used by this page) -->
    <link href="{{ asset('') . config('app.public_url') . '/assets/css/owl.carousel.min.css'  }}" rel="stylesheet" type="text/css" />

    <link href="{{ asset('') . config('app.public_url') . '/assets/css/pages/pricing/pricing-2.css' }}" rel="stylesheet" type="text/css" />

        <!--end::Page Vendors Styles -->
@endsection

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
        <div class="banner-wrapper text-center">
            @include('layout.include.partials.banner')
        </div>

        <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor bg-white" id="kt_content">

        <!-- begin:: Content -->
        <div class="kt-container  kt-grid__item kt-grid__item--fluid home-container">
           
            <div class="row">
                <!-- body content start -->

                <!-- better way html start here -->
                <div class="col-12" id="how-it-works">
                    <div class="kt-better_way text-center pt-7 position-relative">
                        <div class="kt-better_way-head pb-3">
                            <h2 class="pb-2">The Smart Route to Guest Posting</h2>
                            <p>GuestPostEngine  is a marketplace where you can easily buy guest posts from high-quality<br> websites and sell guest post opportunities on yours.</p>
                        </div>
                        <div class="kt-better_way-action-btn text-center pb-6">
                        <a href="{{url('signup')}}" class="btn btn-label-brand kt-mr-10 btn-bold">Signup Now</a>
                            <a href="#book-a-demo" class="btn btn-brand btn-bold">Book a Demo</a>
                        </div>
                        <div class="kt-better_way-video d-flex justify-content-center align-items-end">
                            <div class="kt-better_way-video-left position-absolute pl-5">
                                <img src="{{ asset('') . config('app.public_url') . '/assets/media/icons/tea-cup-svg.svg' }}" height="209" alt="tea-cup-svg">
                            </div>
                            <div class="kt-better_way-video-box">
                                <iframe class="" width="560" height="315" src="https://www.youtube.com/embed/p8OqDB2tLjQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                            <div class="kt-better_way-video-right position-absolute pr-5">
                                <img src="{{ asset('') . config('app.public_url') . '/assets/media/icons/books-svg.svg' }}" height="89" alt="books-svg">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- better way html end here -->

                <!-- trusted by html start here -->
                <div class="col-12 kt-padding-0">
                    <div class="kt_trusted-by">
                        <div class="kt_trusted-by-inner d-flex flex-wrap">
                            <div class="kt_trusted-by_left text-center pt-5 pb-5 col-xl-2 col-lg-2 col-md-2">
                                <h4>Trusted by:</h4>
                            </div>
                            <div class="kt_trusted-by_slider col-xl-10 col-lg-10 col-md-10">
                                <div id="kt_trustedby-carousel" class="owl-carousel h-100 owl-theme d-flex align-items-center">
                                    <div class="item">
                                        <div class="kt_trusted-by_logos">
                                            <img src="{{ asset('') . config('app.public_url') . '/assets/media/client-logos/calendar-logo.png' }} " alt="calendar-logo">
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="kt_trusted-by_logos">
                                            <img src="{{ asset('') . config('app.public_url') . '/assets/media/client-logos/knowledgehut-logo.png' }}" alt="knowledgehut-logo">
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="kt_trusted-by_logos">
                                            <img src="{{ asset('') . config('app.public_url') . '/assets/media/client-logos/rentomojo-logo.png' }}" alt="rentomojo-logo">
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="kt_trusted-by_logos">
                                            <img src="{{ asset('') . config('app.public_url') . '/assets/media/client-logos/simplilearn-logo.png' }}" alt="simplilearn-logo">
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="kt_trusted-by_logos">
                                            <img src="{{ asset('') . config('app.public_url') . '/assets/media/client-logos/intellect-logo.png' }}" alt="intellect-logo">
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="kt_trusted-by_logos">
                                            <img src="{{ asset('') . config('app.public_url') . '/assets/media/client-logos/knowledgehut-logo.png' }}" alt="knowledgehut-logo">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- trusted by html end here -->

                <!-- publish post html start here -->
                <div class="col-12 kt-hidden">
                    <div class="kt_publish-post pt-7 pb-6">
                        <div class="kt_publish-post-head text-center">
                            <h2 class="pb-3">Publish your guest posts on quality, high traffic websites</h2>
                            <p>Accessily is the first and the leading marketplace for Guest Posts. You can buy guest posts.</p>
                        </div>
                    </div>
                    <div class="col-md-10 col-12 offset-md-1 pb-6">
                        <div class="row">
                            <!--begin:: Portlet-->
                            <div class="kt-portlet">
                                <div class="kt-portlet__body">
                                    <div class="kt-widget kt-widget--user-profile-3">
                                        <div class="kt-widget__top">
                                           
                                            <div class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-font-light kt-hidden">
                                                JM
                                            </div>
                                            <div class="kt-widget__content">
                                                <div class="kt-widget__head">
                                                    <div class="d-flex align-items-center">
                                                        <div class="kt-widget__media kt-hidden-">
                                                            <img src="https://www.webdew.com/hubfs/favicon-3.ico" alt="image">
                                                        </div>
                                                        <a href="#" class="kt-widget__username">
                                                            webdew.com  
                                                            <i class="flaticon2-correct kt-font-primary"></i>                      
                                                        </a>&nbsp;
                                                        <span><span class="kt-badge kt-badge--success kt-badge--dot"></span>&nbsp;<span class="kt-font-bold kt-font-success">Do Follow</span></span>
                                                    </div>
                                                    <div class="whishlist-ribbon kt-ribbon kt-ribbon--clip kt-ribbon--right kt-ribbon--danger">
                                                        <div class="kt-ribbon__target" style="top: 12px;">
                                                            <span class="kt-ribbon__inner"></span><i class="la la-heart-o" style="font-size: 1.5rem;"></i></div>                                                     
                                                    </div>                                                  
                                                </div>
                           
                                                <div class="kt-widget__subhead">
                                                   
                                                    webdew: Best HubSpot Partners Agency, with 100+ Certifications on wall
                                                </div>
                           
                                                <div class="kt-widget__info">
                                                    <div class="kt-widget__desc">
                                                        Hey! we are webdew, Inbound Marketing and HubSpot Partner Agency based out in India,
                                                        <br> where 50+ Agency members are Breathing HubSpot in and out.
                                                    </div>
                                                    <div class="kt-widget__action d-flex align-items-center">
                                                        <div class="website-price">
                                                            $ 1234
                                                        </div>&nbsp; &nbsp;
                                                       
                                                        <button type="button" class="btn btn-brand btn-upper">Order Now</button>
                                                    </div>
                                                   
                                                </div>
                                            </div>
                                        </div>
                                        <div class="kt-widget__bottom">
                                            <div class="kt-widget__item">
                                                <div class="kt-widget__icon">
                                                   
                                                    <img src="{{ asset('') . config('app.public_url') . '/assets/media/icons/Moz.svg' }}" style="width:35px;">
                                                </div>
                                                <div class="kt-widget__details">
                                                    <span class="kt-widget__title">Domain Authority</span>
                                                    <div class="d-flex"><span class="kt-widget__value">29</span>
                                                        <span class="d-flex align-items-end">/100</span></div>
                                                </div>
                                            </div>
                           
                                            <div class="kt-widget__item">
                                                <div class="kt-widget__icon">
                                                    <i class="flaticon-earth-globe"></i>
                                                </div>
                                                <div class="kt-widget__details">
                                                    <span class="kt-widget__title">Global Rank</span>
                                                    <span class="kt-widget__value">3,185,623</span>
                                                </div>
                                            </div>
                           
                                            <div class="kt-widget__item">
                                                <div class="kt-widget__icon">
                                                    <i class="flaticon-line-graph"></i>
                                                </div>
                                                <div class="kt-widget__details">
                                                    <span class="kt-widget__title">Traffic</span>
                                                    <span class="kt-widget__value">2.08K</span>
                                                </div>
                                            </div>
                           
                                            <!-- <div class="kt-widget__item">
                                                <div class="kt-widget__icon">
                                                    <i class="flaticon-squares-3"></i>
                                                </div>
                                                <div class="kt-widget__details">
                                                    <span class="kt-widget__title">Category</span>
                                                    <span class="kt-widget__value">2.08K</span>
                                                </div>
                                            </div> -->
                           
                                            <div class="kt-widget__item">
                                                <div class="kt-widget__icon">
                                                    <i class="flaticon-layers"></i>
                                                </div>
                                                <div class="kt-widget__details">
                                                    <span class="kt-widget__title">Orders in Queue</span>
                                                    <span class="kt-widget__value">2.08K</span>
                                                </div>
                                            </div>
                                            <div class="kt-widget__item">
                                                <div class="kt-widget__icon">
                                                    <i class="flaticon-chat-1"></i>
                                                </div>
                                                <div class="kt-widget__details">
                                                   
                                                    <a href="#" class="kt-widget__value kt-font-brand">Chat <br> Now</a>
                                                </div>
                                            </div>
                           
                                            <!-- <div class="kt-widget__item">
                                               
                                                <div class="kt-portlet__head-toolbar">
                                                    <a href="#" class="btn btn-label-primary btn-bold dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                        View More
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right" style="">
                                                        <ul class="kt-nav">
                                            <li class="kt-nav__item">
                                                <a href="#" class="kt-nav__link">
                                                    <i class="kt-nav__link-icon flaticon2-line-chart"></i>
                                                    <span class="kt-nav__link-text">Reports</span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a href="#" class="kt-nav__link">
                                                    <i class="kt-nav__link-icon flaticon2-send"></i>
                                                    <span class="kt-nav__link-text">Messages</span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a href="#" class="kt-nav__link">
                                                    <i class="kt-nav__link-icon flaticon2-pie-chart-1"></i>
                                                    <span class="kt-nav__link-text">Charts</span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a href="#" class="kt-nav__link">
                                                    <i class="kt-nav__link-icon flaticon2-avatar"></i>
                                                    <span class="kt-nav__link-text">Members</span>
                                                </a>
                                            </li>
                                            <li class="kt-nav__item">
                                                <a href="#" class="kt-nav__link">
                                                    <i class="kt-nav__link-icon flaticon2-settings"></i>
                                                    <span class="kt-nav__link-text">Settings</span>
                                                </a>
                                            </li>
                                        </ul>           </div>
                                                </div>
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end:: Portlet-->
                            <div class="kt-portlet">
                                <div class="kt-portlet__body">
                                    <div class="kt-widget kt-widget--user-profile-3">
                                        <div class="kt-widget__top">
                                           
                                            <div class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-font-light kt-hidden">
                                                JM
                                            </div>
                                            <div class="kt-widget__content">
                                                <div class="kt-widget__head">
                                                    <div class="d-flex align-items-center">
                                                        <div class="kt-widget__media kt-hidden-">
                                                            <img src="https://www.webdew.com/hubfs/favicon-3.ico" alt="image">
                                                        </div>
                                                        <a href="#" class="kt-widget__username">
                                                            webdew.com  
                                                            <i class="flaticon2-correct kt-font-primary"></i>                      
                                                        </a>&nbsp;
                                                        <span><span class="kt-badge kt-badge--warning kt-badge--dot"></span>&nbsp;<span class="kt-font-bold kt-font-warning">No Follow</span></span>
                                                    </div>
                                                    <div class="whishlist-ribbon kt-ribbon kt-ribbon--clip kt-ribbon--right kt-ribbon--danger">
                                                        <div class="kt-ribbon__target" style="top: 12px;">
                                                            <span class="kt-ribbon__inner"></span><i class="la la-heart-o" style="
                                            font-size: 1.5rem;
                                        "></i></div>
                                                       
                                                    </div>
                                                   
                                                </div>
                           
                                                <div class="kt-widget__subhead">
                                                   
                                                    webdew: Best HubSpot Partners Agency, with 100+ Certifications on wall
                                                </div>
                           
                                                <div class="kt-widget__info">
                                                    <div class="kt-widget__desc">
                                                        Hey! we are webdew, Inbound Marketing and HubSpot Partner Agency based out in India,
                                                        <br> where 50+ Agency members are Breathing HubSpot in and out.
                                                    </div>
                                                    <div class="kt-widget__action d-flex align-items-center">
                                                        <div class="website-price">
                                                            $ 1234
                                                        </div>&nbsp; &nbsp;
                                                       
                                                        <button type="button" class="btn btn-brand btn-upper">Order Now</button>
                                                    </div>
                                                   
                                                </div>
                                            </div>
                                        </div>
                                        <div class="kt-widget__bottom">
                                            <div class="kt-widget__item">
                                                <div class="kt-widget__icon">
                                                   
                                                    <img src="{{ asset('') . config('app.public_url') . '/media/icons/Moz.svg' }}" style="width:35px;">
                                                </div>
                                                <div class="kt-widget__details">
                                                    <span class="kt-widget__title">Domain Authority</span>
                                                    <div class="d-flex"><span class="kt-widget__value">29</span>
                                                        <span class="d-flex align-items-end">/100</span></div>
                                                </div>
                                            </div>
                           
                                            <div class="kt-widget__item">
                                                <div class="kt-widget__icon">
                                                    <i class="flaticon-earth-globe"></i>
                                                </div>
                                                <div class="kt-widget__details">
                                                    <span class="kt-widget__title">Global Rank</span>
                                                    <span class="kt-widget__value">3,185,623</span>
                                                </div>
                                            </div>
                           
                                            <div class="kt-widget__item">
                                                <div class="kt-widget__icon">
                                                    <i class="flaticon-line-graph"></i>
                                                </div>
                                                <div class="kt-widget__details">
                                                    <span class="kt-widget__title">Traffic</span>
                                                    <span class="kt-widget__value">2.08K</span>
                                                </div>
                                            </div>
                           
                                            <!-- <div class="kt-widget__item">
                                                <div class="kt-widget__icon">
                                                    <i class="flaticon-squares-3"></i>
                                                </div>
                                                <div class="kt-widget__details">
                                                    <span class="kt-widget__title">Category</span>
                                                    <span class="kt-widget__value">2.08K</span>
                                                </div>
                                            </div> -->
                           
                                            <div class="kt-widget__item">
                                                <div class="kt-widget__icon">
                                                    <i class="flaticon-layers"></i>
                                                </div>
                                                <div class="kt-widget__details">
                                                    <span class="kt-widget__title">Orders in Queue</span>
                                                    <span class="kt-widget__value">2.08K</span>
                                                </div>
                                            </div>
                                            <div class="kt-widget__item">
                                                <div class="kt-widget__icon">
                                                    <i class="flaticon-chat-1"></i>
                                                </div>
                                                <div class="kt-widget__details">
                                                   
                                                    <a href="#" class="kt-widget__value kt-font-brand">Chat <br> Now</a>
                                                </div>
                                            </div>
                           
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                                <!--begin:: Portlet-->
                                <div class="kt-portlet">
                                    <div class="kt-portlet__body">
                                        <div class="kt-widget kt-widget--user-profile-3">
                                            <div class="kt-widget__top">
                                               
                                                <div class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-font-light kt-hidden">
                                                    JM
                                                </div>
                                                <div class="kt-widget__content">
                                                    <div class="kt-widget__head">
                                                        <div class="d-flex align-items-center">
                                                            <div class="kt-widget__media kt-hidden-">
                                                                <img src="https://www.webdew.com/hubfs/favicon-3.ico" alt="image">
                                                            </div>
                                                            <a href="#" class="kt-widget__username">
                                                                webdew.com  
                                                                <i class="flaticon2-correct kt-font-primary"></i>                      
                                                            </a>&nbsp;
                                                            <span><span class="kt-badge kt-badge--success kt-badge--dot"></span>&nbsp;<span class="kt-font-bold kt-font-success">Do Follow</span></span>
                                                        </div>
                                                        <div class="whishlist-ribbon kt-ribbon kt-ribbon--clip kt-ribbon--right kt-ribbon--danger">
                                                            <div class="kt-ribbon__target" style="top: 12px;">
                                                                <span class="kt-ribbon__inner"></span><i class="la la-heart-o" style="
                                                font-size: 1.5rem;
                                            "></i></div>
                                                           
                                                        </div>
                                                       
                                                    </div>
                               
                                                    <div class="kt-widget__subhead">
                                                       
                                                        webdew: Best HubSpot Partners Agency, with 100+ Certifications on wall
                                                    </div>
                               
                                                    <div class="kt-widget__info">
                                                        <div class="kt-widget__desc">
                                                            Hey! we are webdew, Inbound Marketing and HubSpot Partner Agency based out in India,
                                                            <br> where 50+ Agency members are Breathing HubSpot in and out.
                                                        </div>
                                                        <div class="kt-widget__action d-flex align-items-center">
                                                            <div class="website-price">
                                                                $ 1234
                                                            </div>&nbsp; &nbsp;
                                                           
                                                            <button type="button" class="btn btn-brand btn-upper">Order Now</button>
                                                        </div>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="kt-widget__bottom">
                                                <div class="kt-widget__item">
                                                    <div class="kt-widget__icon">
                                                       
                                                        <img src="../dist/assets/media/icons/Moz.svg" style="width:35px;">
                                                    </div>
                                                    <div class="kt-widget__details">
                                                        <span class="kt-widget__title">Domain Authority</span>
                                                        <div class="d-flex"><span class="kt-widget__value">29</span>
                                                            <span class="d-flex align-items-end">/100</span></div>
                                                    </div>
                                                </div>
                               
                                                <div class="kt-widget__item">
                                                    <div class="kt-widget__icon">
                                                        <i class="flaticon-earth-globe"></i>
                                                    </div>
                                                    <div class="kt-widget__details">
                                                        <span class="kt-widget__title">Global Rank</span>
                                                        <span class="kt-widget__value">3,185,623</span>
                                                    </div>
                                                </div>
                               
                                                <div class="kt-widget__item">
                                                    <div class="kt-widget__icon">
                                                        <i class="flaticon-line-graph"></i>
                                                    </div>
                                                    <div class="kt-widget__details">
                                                        <span class="kt-widget__title">Traffic</span>
                                                        <span class="kt-widget__value">2.08K</span>
                                                    </div>
                                                </div>
                               
                                                <!-- <div class="kt-widget__item">
                                                    <div class="kt-widget__icon">
                                                        <i class="flaticon-squares-3"></i>
                                                    </div>
                                                    <div class="kt-widget__details">
                                                        <span class="kt-widget__title">Category</span>
                                                        <span class="kt-widget__value">2.08K</span>
                                                    </div>
                                                </div> -->
                               
                                                <div class="kt-widget__item">
                                                    <div class="kt-widget__icon">
                                                        <i class="flaticon-layers"></i>
                                                    </div>
                                                    <div class="kt-widget__details">
                                                        <span class="kt-widget__title">Orders in Queue</span>
                                                        <span class="kt-widget__value">2.08K</span>
                                                    </div>
                                                </div>
                                                <div class="kt-widget__item">
                                                    <div class="kt-widget__icon">
                                                        <i class="flaticon-chat-1"></i>
                                                    </div>
                                                    <div class="kt-widget__details">
                                                       
                                                        <a href="#" class="kt-widget__value kt-font-brand">Chat <br> Now</a>
                                                    </div>
                                                </div>
                               
                                                <!-- <div class="kt-widget__item">
                                                   
                                                    <div class="kt-portlet__head-toolbar">
                                                        <a href="#" class="btn btn-label-primary btn-bold dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                            View More
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right" style="">
                                                            <ul class="kt-nav">
                                                <li class="kt-nav__item">
                                                    <a href="#" class="kt-nav__link">
                                                        <i class="kt-nav__link-icon flaticon2-line-chart"></i>
                                                        <span class="kt-nav__link-text">Reports</span>
                                                    </a>
                                                </li>
                                                <li class="kt-nav__item">
                                                    <a href="#" class="kt-nav__link">
                                                        <i class="kt-nav__link-icon flaticon2-send"></i>
                                                        <span class="kt-nav__link-text">Messages</span>
                                                    </a>
                                                </li>
                                                <li class="kt-nav__item">
                                                    <a href="#" class="kt-nav__link">
                                                        <i class="kt-nav__link-icon flaticon2-pie-chart-1"></i>
                                                        <span class="kt-nav__link-text">Charts</span>
                                                    </a>
                                                </li>
                                                <li class="kt-nav__item">
                                                    <a href="#" class="kt-nav__link">
                                                        <i class="kt-nav__link-icon flaticon2-avatar"></i>
                                                        <span class="kt-nav__link-text">Members</span>
                                                    </a>
                                                </li>
                                                <li class="kt-nav__item">
                                                    <a href="#" class="kt-nav__link">
                                                        <i class="kt-nav__link-icon flaticon2-settings"></i>
                                                        <span class="kt-nav__link-text">Settings</span>
                                                    </a>
                                                </li>
                                            </ul>           </div>
                                                    </div>
                                                </div> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end:: Portlet-->                            
                                    </div>
                                </div>
                            </div>
                            <!-- publish post html end here -->


                            <!-- why guest post html start here -->
                            <div class="col-12">
                                <div class="row">
                                    <div class="kt_why-gpengine pb-7 pt-7">
                                        <div class="col-12">
                                            <div class="text-center col-12">
                                                <h2 class="pb-6">Why GuestPostEngine</h2>
                                            </div>
                                        </div>
                                        <div class="kt_why-gpengine-service col-12">
                                            <div class="row ml-0 mr-0">
                                                <div class="col-xl-4 col-lg-6 col-md-6">
                                                    <div class="kt-portlet kt-iconbox kt-icon-size kt-iconbox--primary kt-iconbox--animate-slow">
                                                        <div class="kt-portlet__body">
                                                            <div class="kt-iconbox__body">
                                                                <div class="kt-iconbox__icon">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                            <rect x="0" y="0" width="24" height="24"/>
                                                                            <path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                                                            <path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" fill="#000000" fill-rule="nonzero"/>
                                                                            <path d="M10.5,10.5 L10.5,9.5 C10.5,9.22385763 10.7238576,9 11,9 C11.2761424,9 11.5,9.22385763 11.5,9.5 L11.5,10.5 L12.5,10.5 C12.7761424,10.5 13,10.7238576 13,11 C13,11.2761424 12.7761424,11.5 12.5,11.5 L11.5,11.5 L11.5,12.5 C11.5,12.7761424 11.2761424,13 11,13 C10.7238576,13 10.5,12.7761424 10.5,12.5 L10.5,11.5 L9.5,11.5 C9.22385763,11.5 9,11.2761424 9,11 C9,10.7238576 9.22385763,10.5 9.5,10.5 L10.5,10.5 Z" fill="#000000" opacity="0.3"/>
                                                                        </g>
                                                                    </svg>
                                                                </div>
                                                                <div class="kt-iconbox__desc">
                                                                    <h3 class="kt-iconbox__title">
                                                                        <a>Search Engine</a>
                                                                    </h3>
                                                                    <div class="kt-iconbox__content">
                                                                    Your search for high-quality websites ends here as GuestPostEngine  brings 1000’s of hand-picked and quality sites for your SEO campaign.
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xl-4 col-lg-6 col-md-6">
                                                    <div class="kt-portlet kt-iconbox kt-icon-size kt-iconbox--primary kt-iconbox--animate-slow">
                                                        <div class="kt-portlet__body">
                                                            <div class="kt-iconbox__body">
                                                                <div class="kt-iconbox__icon">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                            <polygon points="0 0 24 0 24 24 0 24"/>
                                                                            <path d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z" fill="#000000" fill-rule="nonzero"/>
                                                                            <path d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z" fill="#000000" opacity="0.3"/>
                                                                        </g>
                                                                    </svg>
                                                                </div>
                                                                <div class="kt-iconbox__desc">
                                                                    <h3 class="kt-iconbox__title">
                                                                        <a>Largest Database</a>
                                                                    </h3>
                                                                    <div class="kt-iconbox__content">
                                                                    GuestPostEngine is one of the largest databases with around more than 10,000 websites, and the list gets updated regularly.
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>  
                                                <div class="col-xl-4 col-lg-6 col-md-6">
                                                    <div class="kt-portlet kt-iconbox kt-icon-size kt-iconbox--primary kt-iconbox--animate-slow">
                                                        <div class="kt-portlet__body">
                                                            <div class="kt-iconbox__body">
                                                                <div class="kt-iconbox__icon">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                            <rect x="0" y="0" width="24" height="24"/>
                                                                            <rect fill="#000000" opacity="0.3" x="17" y="4" width="3" height="13" rx="1.5"/>
                                                                            <rect fill="#000000" opacity="0.3" x="12" y="9" width="3" height="8" rx="1.5"/>
                                                                            <path d="M5,19 L20,19 C20.5522847,19 21,19.4477153 21,20 C21,20.5522847 20.5522847,21 20,21 L4,21 C3.44771525,21 3,20.5522847 3,20 L3,4 C3,3.44771525 3.44771525,3 4,3 C4.55228475,3 5,3.44771525 5,4 L5,19 Z" fill="#000000" fill-rule="nonzero"/>
                                                                            <rect fill="#000000" opacity="0.3" x="7" y="11" width="3" height="6" rx="1.5"/>
                                                                        </g>
                                                                    </svg>
                                                                </div>
                                                                <div class="kt-iconbox__desc">
                                                                    <h3 class="kt-iconbox__title">
                                                                        <a>Updated Key Metrics</a>
                                                                    </h3>
                                                                    <div class="kt-iconbox__content">
                                                                    The websites listed on GuestPostEngine  gets updated regularly with their metrics. We usually update the website’s key metrics once a month.
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>  
                                                <div class="col-xl-4 col-lg-6 col-md-6">
                                                    <div class="kt-portlet kt-iconbox kt-icon-size kt-iconbox--primary kt-iconbox--animate-slow">
                                                        <div class="kt-portlet__body">
                                                            <div class="kt-iconbox__body">
                                                                <div class="kt-iconbox__icon">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                            <rect x="0" y="0" width="24" height="24"/>
                                                                            <path d="M8,3 L8,3.5 C8,4.32842712 8.67157288,5 9.5,5 L14.5,5 C15.3284271,5 16,4.32842712 16,3.5 L16,3 L18,3 C19.1045695,3 20,3.8954305 20,5 L20,21 C20,22.1045695 19.1045695,23 18,23 L6,23 C4.8954305,23 4,22.1045695 4,21 L4,5 C4,3.8954305 4.8954305,3 6,3 L8,3 Z" fill="#000000" opacity="0.3"/>
                                                                            <path d="M11,2 C11,1.44771525 11.4477153,1 12,1 C12.5522847,1 13,1.44771525 13,2 L14.5,2 C14.7761424,2 15,2.22385763 15,2.5 L15,3.5 C15,3.77614237 14.7761424,4 14.5,4 L9.5,4 C9.22385763,4 9,3.77614237 9,3.5 L9,2.5 C9,2.22385763 9.22385763,2 9.5,2 L11,2 Z" fill="#000000"/>
                                                                            <rect fill="#000000" opacity="0.3" x="10" y="9" width="7" height="2" rx="1"/>
                                                                            <rect fill="#000000" opacity="0.3" x="7" y="9" width="2" height="2" rx="1"/>
                                                                            <rect fill="#000000" opacity="0.3" x="7" y="13" width="2" height="2" rx="1"/>
                                                                            <rect fill="#000000" opacity="0.3" x="10" y="13" width="7" height="2" rx="1"/>
                                                                            <rect fill="#000000" opacity="0.3" x="7" y="17" width="2" height="2" rx="1"/>
                                                                            <rect fill="#000000" opacity="0.3" x="10" y="17" width="7" height="2" rx="1"/>
                                                                        </g>
                                                                    </svg>
                                                                </div>
                                                                <div class="kt-iconbox__desc">
                                                                    <h3 class="kt-iconbox__title">
                                                                        <a>Quality Prospecting</a>
                                                                    </h3>
                                                                    <div class="kt-iconbox__content">
                                                                    We value quality. So, each listed website is hand-picked by our experts, making it easy for you to get quality backlinks.
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>  
                                                <div class="col-xl-4 col-lg-6 col-md-6">
                                                    <div class="kt-portlet kt-iconbox kt-icon-size kt-iconbox--primary kt-iconbox--animate-slow">
                                                        <div class="kt-portlet__body">
                                                            <div class="kt-iconbox__body">
                                                                <div class="kt-iconbox__icon">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                            <rect x="0" y="0" width="24" height="24"/>
                                                                            <path d="M7.14319965,19.3575259 C7.67122143,19.7615175 8.25104409,20.1012165 8.87097532,20.3649307 L7.89205065,22.0604779 C7.61590828,22.5387706 7.00431787,22.7026457 6.52602525,22.4265033 C6.04773263,22.150361 5.88385747,21.5387706 6.15999985,21.0604779 L7.14319965,19.3575259 Z M15.1367085,20.3616573 C15.756345,20.0972995 16.3358198,19.7569961 16.8634386,19.3524415 L17.8320512,21.0301278 C18.1081936,21.5084204 17.9443184,22.1200108 17.4660258,22.3961532 C16.9877332,22.6722956 16.3761428,22.5084204 16.1000004,22.0301278 L15.1367085,20.3616573 Z" fill="#000000"/>
                                                                            <path d="M12,21 C7.581722,21 4,17.418278 4,13 C4,8.581722 7.581722,5 12,5 C16.418278,5 20,8.581722 20,13 C20,17.418278 16.418278,21 12,21 Z M19.068812,3.25407593 L20.8181344,5.00339833 C21.4039208,5.58918477 21.4039208,6.53893224 20.8181344,7.12471868 C20.2323479,7.71050512 19.2826005,7.71050512 18.696814,7.12471868 L16.9474916,5.37539627 C16.3617052,4.78960984 16.3617052,3.83986237 16.9474916,3.25407593 C17.5332781,2.66828949 18.4830255,2.66828949 19.068812,3.25407593 Z M5.29862906,2.88207799 C5.8844155,2.29629155 6.83416297,2.29629155 7.41994941,2.88207799 C8.00573585,3.46786443 8.00573585,4.4176119 7.41994941,5.00339833 L5.29862906,7.12471868 C4.71284263,7.71050512 3.76309516,7.71050512 3.17730872,7.12471868 C2.59152228,6.53893224 2.59152228,5.58918477 3.17730872,5.00339833 L5.29862906,2.88207799 Z" fill="#000000" opacity="0.3"/>
                                                                            <path d="M11.9630156,7.5 L12.0475062,7.5 C12.3043819,7.5 12.5194647,7.69464724 12.5450248,7.95024814 L13,12.5 L16.2480695,14.3560397 C16.403857,14.4450611 16.5,14.6107328 16.5,14.7901613 L16.5,15 C16.5,15.2109164 16.3290185,15.3818979 16.1181021,15.3818979 C16.0841582,15.3818979 16.0503659,15.3773725 16.0176181,15.3684413 L11.3986612,14.1087258 C11.1672824,14.0456225 11.0132986,13.8271186 11.0316926,13.5879956 L11.4644883,7.96165175 C11.4845267,7.70115317 11.7017474,7.5 11.9630156,7.5 Z" fill="#000000"/>
                                                                        </g>
                                                                    </svg>
                                                                </div>
                                                                <div class="kt-iconbox__desc">
                                                                    <h3 class="kt-iconbox__title">
                                                                        <a>Save Time & Money</a>
                                                                    </h3>
                                                                    <div class="kt-iconbox__content">
                                                                    You get quality websites in a matter of seconds. Thus, you save ample time & money, otherwise wasted in doing intense research.
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>  
                                                <div class="col-xl-4 col-lg-6 col-md-6">
                                                    <div class="kt-portlet kt-iconbox kt-icon-size kt-iconbox--primary kt-iconbox--animate-slow">
                                                        <div class="kt-portlet__body">
                                                            <div class="kt-iconbox__body">
                                                                <div class="kt-iconbox__icon">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                            <rect x="0" y="0" width="24" height="24"/>
                                                                            <path d="M12.4644661,14.5355339 L9.46446609,14.5355339 C8.91218134,14.5355339 8.46446609,14.9832492 8.46446609,15.5355339 C8.46446609,16.0878187 8.91218134,16.5355339 9.46446609,16.5355339 L12.4644661,16.5355339 L12.4644661,17.5355339 C12.4644661,18.6401034 11.5690356,19.5355339 10.4644661,19.5355339 L6.46446609,19.5355339 C5.35989659,19.5355339 4.46446609,18.6401034 4.46446609,17.5355339 L4.46446609,13.5355339 C4.46446609,12.4309644 5.35989659,11.5355339 6.46446609,11.5355339 L10.4644661,11.5355339 C11.5690356,11.5355339 12.4644661,12.4309644 12.4644661,13.5355339 L12.4644661,14.5355339 Z" fill="#000000" opacity="0.3" transform="translate(8.464466, 15.535534) rotate(-45.000000) translate(-8.464466, -15.535534) "/>
                                                                            <path d="M11.5355339,9.46446609 L14.5355339,9.46446609 C15.0878187,9.46446609 15.5355339,9.01675084 15.5355339,8.46446609 C15.5355339,7.91218134 15.0878187,7.46446609 14.5355339,7.46446609 L11.5355339,7.46446609 L11.5355339,6.46446609 C11.5355339,5.35989659 12.4309644,4.46446609 13.5355339,4.46446609 L17.5355339,4.46446609 C18.6401034,4.46446609 19.5355339,5.35989659 19.5355339,6.46446609 L19.5355339,10.4644661 C19.5355339,11.5690356 18.6401034,12.4644661 17.5355339,12.4644661 L13.5355339,12.4644661 C12.4309644,12.4644661 11.5355339,11.5690356 11.5355339,10.4644661 L11.5355339,9.46446609 Z" fill="#000000" transform="translate(15.535534, 8.464466) rotate(-45.000000) translate(-15.535534, -8.464466) "/>
                                                                        </g>
                                                                    </svg>
                                                                </div>
                                                                <div class="kt-iconbox__desc">
                                                                    <h3 class="kt-iconbox__title">
                                                                        <a>Powerful Backlinks</a>
                                                                    </h3>
                                                                    <div class="kt-iconbox__content">
                                                                    You can quickly generate powerful, and quality backlinks as the listed websites are meant to serve your purpose of doing so.
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 pt-5">
                                                    <div class="kt-better_way-action-btn text-center">
                                                    <a href="{{url('signup')}}" class="btn btn-label-brand kt-mr-10 btn-bold">Signup Now</a>
                                                        <a href="#book-a-demo" class="btn btn-brand btn-bold">Book a Demo</a>
                                                    </div>
                                                </div>                              
                                            </div>                                  
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- why guest post html end here -->

                           <!-- faq html start here -->
                            <div class="col-12">
                                <div class="kt-faq">
                                    <div class="col-12 text-center">
                                        <h2 class="h pb-5">Frequently Asked Questions</h2>
                                    </div>
                                    <div class="d-flex align-items-center justify-content-center kt-mb-20">
                                        <div class="kt-portlet__head-toolbar">
                                            <ul class="nav nav-pills nav-pills-label nav-pills-bold" role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active" data-toggle="tab" href="#kt_widget5_tab1_content" role="tab" aria-selected="true">
                                                        General
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#kt_widget5_tab2_content" role="tab" aria-selected="false">
                                                        Advertiser
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#kt_widget5_tab3_content" role="tab">
                                                        Publisher
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="tab-content col-xl-8 offset-xl-2 col-lg-8 offset-lg-2 pb-7">
                                        <div class="tab-pane active" id="kt_widget5_tab1_content" aria-expanded="true">
                                            <div class="accordion accordion-solid accordion-toggle-svg" id="accordionExample8">
                                                <div class="card">
                                                    <div class="card-header" id="headingOne8">
                                                        <div class="card-title" data-toggle="collapse" data-target="#collapseOne8" aria-expanded="true" aria-controls="collapseOne8">
                                                            Who we are, and what we do?
                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                    <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                                    <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"></path>
                                                                    <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "></path>
                                                                </g>
                                                            </svg>
                                                        </div>
                                                    </div>
                                                    <div id="collapseOne8" class="collapse show" aria-labelledby="headingOne8" data-parent="#accordionExample8" style="">
                                                        <div class="card-body">
                                                            <p>With more than 10,000 listings, GuestPostEngine is a search platform that has simplified the entire process of guest post-management.</p>
                                                            <p>With GuestPostEngine , it does not matter if you want to buy a guest post for yourself or a client, or if you wish to sell the guest post opportunities. This dedicated platform will make the entire process of grabbing or selling opportunities and publishing easy for you.</p>
                                                            <p>Each listing is checked by our experts to ensure and deliver high-quality service to you.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card">
                                                    <div class="card-header" id="headingTwo8">
                                                        <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseTwo8" aria-expanded="false" aria-controls="collapseTwo8">
                                                            Why you need GuestPostEngine ?
                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                    <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                                    <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"></path>
                                                                    <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "></path>
                                                                </g>
                                                            </svg>
                                                        </div>
                                                    </div>
                                                    <div id="collapseTwo8" class="collapse" aria-labelledby="headingTwo8" data-parent="#accordionExample8">
                                                        <div class="card-body">
                                                            <p>A GuestPostEngine is a vast search engine that helps you find out relevant and quality websites in a fraction of seconds.</p>
                                                            <p>By using GuestPostEngine, you will end up saving a lot of time and money, which you would have otherwise invested in doing prospecting, outreaching, email handling, etc.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card">
                                                    <div class="card-header" id="headingFour8">
                                                        <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseFour8" aria-expanded="false" aria-controls="collapseFour8">
                                                            How does GuestPostEngine work?
                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                    <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                                    <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"></path>
                                                                    <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "></path>
                                                                </g>
                                                            </svg>
                                                        </div>
                                                    </div>
                                                    <div id="collapseFour8" class="collapse" aria-labelledby="headingFour8" data-parent="#accordionExample8">
                                                        <div class="card-body">
                                                            <p><span style="font-weight:500;">In the advertiser’s case:</span> Simply log in, search a relevant site, and place an order. </p>
                                                            <p><span style="font-weight:500;">In the publisher’s case:</span> Simply log in, add multiple websites, and start earning through your guest post network.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card">
                                                    <div class="card-header" id="headingFive8">
                                                        <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseFive8" aria-expanded="false" aria-controls="collapseFive8">
                                                            Is GuestPostEngine free to use?
                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                    <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                                    <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"></path>
                                                                    <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "></path>
                                                                </g>
                                                            </svg>
                                                        </div>
                                                    </div>
                                                    <div id="collapseFive8" class="collapse" aria-labelledby="headingFive8" data-parent="#accordionExample8">
                                                        <div class="card-body">
                                                            <p>Yes!! GuestPostEngine is free to use, and you only need to pay if you wish to get your post published. </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card">
                                                    <div class="card-header" id="headingnine8">
                                                        <div class="card-title collapsed" data-toggle="collapse" data-target="#collapsenine8" aria-expanded="false" aria-controls="collapsenine8">
                                                            How is GuestPostEngine helpful for SEO campaigns?
                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                    <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                                    <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"></path>
                                                                    <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "></path>
                                                                </g>
                                                            </svg>
                                                        </div>
                                                    </div>
                                                    <div id="collapsenine8" class="collapse" aria-labelledby="headingnine8" data-parent="#accordionExample8">
                                                        <div class="card-body">
                                                            <p>There are many white hat SEO techniques to build high authority links for your website. Out of all the methods, guest post link building stands on top.</p>
                                                            <p>With GuestPostEngine, you can take leverage of link building techniques by getting your post published on high-quality websites.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="kt_widget5_tab2_content">
                                            <div class="accordion accordion-solid accordion-toggle-svg" id="faqaccordion2">
                                                <div class="card">
                                                    <div class="card-header" id="headingThree8">
                                                        <div class="card-title" data-toggle="collapse" data-target="#collapseThree8" aria-expanded="false" aria-controls="collapseThree8">
                                                            Who is advertiser according to the GuestPostEngine?
                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                    <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                                    <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"></path>
                                                                    <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "></path>
                                                                </g>
                                                            </svg>
                                                        </div>
                                                    </div>
                                                    <div id="collapseThree8" class="collapse show" aria-labelledby="headingThree8" data-parent="#faqaccordion2">
                                                        <div class="card-body">
                                                            <p>A advertiser is the one who purchases a guest post on our platform.</p>
                                                        </div>
                                                    </div>
                                                </div> 
											    <div class="card">
                                                    <div class="card-header" id="advertiserfaq1">
                                                        <div class="card-title collapsed" data-toggle="collapse" data-target="#collapsefaq1" aria-expanded="false" aria-controls="collapsefaq1">
                                                            How can an advertiser purchase a guest post website?
                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                    <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                                    <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"></path>
                                                                    <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "></path>
                                                                </g>
                                                            </svg>
                                                        </div>
                                                    </div>
                                                    <div id="collapsefaq1" class="collapse" aria-labelledby="advertiserfaq1" data-parent="#faqaccordion2">
                                                        <div class="card-body">
                                                            <p>Search the website with his domain name or a keyword in a search bar. Click on the starting at button then you will see the complete details of a website which includes DA, traffic, price, and etc. For more information, reach out at <a href="https://help.fly.biz/how-to-purchase-a-website-as-an-advertiser" target="_blank">http://bit.ly/2U3Q4J7</a></p>
                                                        </div>
                                                    </div>
                                                </div>
												<div class="card">
                                                    <div class="card-header" id="advertiserfaq2">
                                                        <div class="card-title collapsed" data-toggle="collapse" data-target="#collapsefaq2" aria-expanded="false" aria-controls="collapsefaq2">
                                                            How to add a website to a wishlist?
                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                    <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                                    <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"></path>
                                                                    <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "></path>
                                                                </g>
                                                            </svg>
                                                        </div>
                                                    </div>
                                                    <div id="collapsefaq2" class="collapse" aria-labelledby="advertiserfaq2" data-parent="#faqaccordion2">
                                                        <div class="card-body">
                                                            <p>If an advertiser wishes to purchase a website at some point but not sure about that website, he/she can add that website in the <b>wishlist section</b>.</p>
                                                        </div>
                                                    </div>
                                                </div>
												<div class="card">
                                                    <div class="card-header" id="advertiserfaq3">
                                                        <div class="card-title collapsed" data-toggle="collapse" data-target="#collapsefaq3" aria-expanded="false" aria-controls="collapsefaq3">
                                                            How to add a website in add to cart?
                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                    <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                                    <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"></path>
                                                                    <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "></path>
                                                                </g>
                                                            </svg>
                                                        </div>
                                                    </div>
                                                    <div id="collapsefaq3" class="collapse" aria-labelledby="advertiserfaq3" data-parent="#faqaccordion2">
                                                        <div class="card-body">
                                                            <p>If an advertiser wants to purchase multiple websites at same time, he/she can add websites in the add to cart section.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="kt_widget5_tab3_content">
                                            <div class="accordion accordion-solid accordion-toggle-svg" id="faqaccordion3">
                                                <div class="card">
                                                    <div class="card-header" id="headingThree8">
                                                        <div class="card-title" data-toggle="collapse" data-target="#collapseThree8" aria-expanded="false" aria-controls="collapseThree8">
                                                            Who is publisher according to the GuestPostEngine?
                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                    <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                                    <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"></path>
                                                                    <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "></path>
                                                                </g>
                                                            </svg>
                                                        </div>
                                                    </div>
                                                    <div id="collapseThree8" class="collapse show" aria-labelledby="headingThree8" data-parent="#faqaccordion2">
                                                        <div class="card-body">
                                                            <p>A publisher is anyone who adds multiple websites and offers a pool of opportunities for people to buy guest posts.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card">
                                                    <div class="card-header" id="headingSix8">
                                                        <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseSix8" aria-expanded="false" aria-controls="collapseSix8">
                                                            Can anyone list their website on GuestPostEngine?
                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                    <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                                    <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"></path>
                                                                    <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "></path>
                                                                </g>
                                                            </svg>
                                                        </div>
                                                    </div>
                                                    <div id="collapseSix8" class="collapse" aria-labelledby="headingSix8" data-parent="#faqaccordion3">
                                                        <div class="card-body">
                                                            <p>Yes!! Anyone (webmaster and author) can list their website on GuestPostEngine provided that they fulfill the GuestPostEngine requirements.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card">
                                                    <div class="card-header" id="headingSeven8">
                                                        <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseSeven8" aria-expanded="false" aria-controls="collapseSeven8">
                                                            Can I list more than one website?
                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                    <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                                    <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"></path>
                                                                    <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "></path>
                                                                </g>
                                                            </svg>
                                                        </div>
                                                    </div>
                                                    <div id="collapseSeven8" class="collapse" aria-labelledby="headingSeven8" data-parent="#faqaccordion3">
                                                        <div class="card-body">
                                                            <p>Yes, you can list more than one website. In case, the site is listed by the website owner (webmaster), we do not allow anyone else to list the same website. </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card">
                                                    <div class="card-header" id="headingeight8">
                                                        <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseeight8" aria-expanded="false" aria-controls="collapseeight8">
                                                            How do I ensure the safety of my money?
                                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                    <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                                                    <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"></path>
                                                                    <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "></path>
                                                                </g>
                                                            </svg>
                                                        </div>
                                                    </div>
                                                    <div id="collapseeight8" class="collapse" aria-labelledby="headingeight8" data-parent="#faqaccordion3">
                                                        <div class="card-body">
                                                            <p>When you pay us to get your post published, we release the payment to the publisher after 15 days of getting your post live. </p>
                                                            <p>In case if the post doesn’t go live in 1 month, we ensure a complete refund of your money.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- faq html start here -->

                            <!-- Start of Meetings Embed Script -->
                            <div class="col-12" id="book-a-demo">
                                <div class="col-12 text-center">
                                    <h2 class="h pb-4">Book a Demo</h2>
                                </div>
                            <!-- Start of Meetings Embed Script -->
                                <!-- <div class="meetings-iframe-container" data-src="https://meetings.hubspot.com/prabhleen-k2/15-minute-demo?embed=true"></div>
                                <script type="text/javascript" src="https://static.hsappstatic.net/MeetingsEmbed/ex/MeetingsEmbedCode.js"></script> -->
                                <div class="meetings-iframe-container" data-src="https://www.webdew.com/meetings/danish/meet?embed=true"></div>
                                <script type="text/javascript" src="https://static.hsappstatic.net/MeetingsEmbed/ex/MeetingsEmbedCode.js"></script>
                            <!-- End of Meetings Embed Script -->
                            <!-- blog post html start here -->
                            <div class="col-12">
                                <div class="kt_blog-post pb-5">
                                    <div class="row">
                                        <div class="col-12 pb-6 text-center">
                                            <h2>Recent Blogs</h2>
                                        </div>
                                    </div>
                                    <div class="row justify-content-center">
                                        <div class="col-xl-8 col-lg-8 col-md-12">
                                            <div class="row">
                                                <div class="col-xl-6 col-lg-6 col-md-6">
                                                    <div class="kt-widget5">        
                                                        <div class="kt-widget5__item kt-portlet kt-iconbox--primary kt-iconbox--animate-slow pt-4 pb-4 pl-2 pr-2 bg-base kt-padding-0">
                                                            <div class="kt-widget5__content pl-0 flex-wrap">
                                                                <div class="col-12 kt-padding-0">
                                                                    <div class="kt-widget5__pic pr-0">
                                                                    <a href="https://blog.fly.biz/7-reasons-why-you-should-accept-guest-post-on-your-blog">
                                                                        <img class="kt-widget7__img" src="{{ asset('') . config('app.public_url') . '/assets/media/products/blog1.jpg' }}" alt="Recent-blog">
    </a>
                                                                    </div>
                                                                </div>
                                                                <div class="col-12 kt-padding-20">
                                                                    <div class="kt-widget5__section text-left">
                                                                        <h4>    
                                                                        <a href="https://blog.fly.biz/7-reasons-why-you-should-accept-guest-post-on-your-blog">                                                                      
                                                                            7 reasons why you should accept Guest Post on your Blog  
    </a>                                                                    
                                                                       </h4>
                                                                        <p class="kt-widget5__desc pb-3">
                                                                        So you have a well-built website, adopted fair promotional strategies..
                                                                        </p>
                                                                        <div class="kt-widget5__info">
                                                                            <a href="https://blog.fly.biz/7-reasons-why-you-should-accept-guest-post-on-your-blog" class="font-weight-normal">Read More</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>  
                                                        </div>                      
                                                    </div>                                      
                                                </div>
                                                <div class="col-xl-6 col-lg-6 col-md-6">
                                                    <div class="kt-widget5">        
                                                        <div class="kt-widget5__item kt-portlet kt-iconbox--primary kt-iconbox--animate-slow pt-4 pb-4 pl-2 pr-2 bg-base kt-padding-0">
                                                            <div class="kt-widget5__content pl-0 flex-wrap">
                                                                <div class="col-12 kt-padding-0">
                                                                    <div class="kt-widget5__pic pr-0">
                                                                    <a href="https://blog.fly.biz/why-guest-blogging-is-one-of-the-best-link-building-methods-out-of-all">
                                                                        <img class="kt-widget7__img" src="{{ asset('') . config('app.public_url') . '/assets/media/products/blog2.jpg' }}" alt="Recent-blog">
    </a>
                                                                    </div>
                                                                </div>
                                                                <div class="col-12 kt-padding-20">
                                                                    <div class="kt-widget5__section text-left">
                                                                        <h4>
                                                                        <a href="https://blog.fly.biz/why-guest-blogging-is-one-of-the-best-link-building-methods-out-of-all">
                                                                            Why Guest Blogging Is One Of The Best Link Building Methods Out Of All
    </a>
                                                                           
                                                                        </h4>
                                                                        <p class="kt-widget5__desc pb-3">
                                                                        The world has gone digital, and businesses of all sizes are going..
                                                                        </p>
                                                                        <div class="kt-widget5__info">
                                                                            <a href="https://blog.fly.biz/why-guest-blogging-is-one-of-the-best-link-building-methods-out-of-all" class="font-weight-normal">Read More</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>  
                                                        </div>                      
                                                    </div>                                  
                                                </div>
                                                <div class="col-12">
                                                    <div class="kt-portlet kt-iconbox kt-iconbox--primary kt-iconbox--animate-slow kt-gbplaybook">
                                                        <div class="kt-portlet__body">
                                                            <div class="kt-iconbox__body">
                                                                <div class="kt-iconbox__icon">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                            <rect x="0" y="0" width="24" height="24"/>
                                                                            <path d="M18,2 L20,2 C21.6568542,2 23,3.34314575 23,5 L23,19 C23,20.6568542 21.6568542,22 20,22 L18,22 L18,2 Z" fill="#000000" opacity="0.3"/>
                                                                            <path d="M5,2 L17,2 C18.6568542,2 20,3.34314575 20,5 L20,19 C20,20.6568542 18.6568542,22 17,22 L5,22 C4.44771525,22 4,21.5522847 4,21 L4,3 C4,2.44771525 4.44771525,2 5,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="#000000"/>
                                                                        </g>
                                                                    </svg>
                                                                </div>
                                                                <div class="kt-iconbox__desc pr-4">
                                                                    <h3 class="kt-iconbox__title">
                                                                       Guest Blogging Playbook
                                                                    </h3>
                                                                    <div class="kt-iconbox__content">
                                                                    Guest blogging is one of the well-known marketing techniques in which a blogger writes the content for a website that belongs to some other company or individual. In return, a blogger gain a backlink.
                                                                    </div>
                                                                </div>
                                                                <div class="kt-iconbox__btn d-flex align-items-center">
                                                                    <a href="{{url('/blog/').'/playbook'}}" class="btn btn-label-brand btn-bold text-nowrap">Go to Playbook</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>  
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-4 col-lg-4 col-md-12">
                                            <div class="row h-100 pb-4">
                                                <div class="col-12">
                                                    <div class="kt_blog-subscribe-main kt-widget5__item kt-portlet kt-iconbox--primary kt-iconbox--animate-slow flex-row justify-content-center align-items-center h-100">
                                                        <div class="kt_blog-subscribe-inner w-100 blog-subscribe-form">
                                                            <form class="kt-form" id="kt_form" novalidate="novalidate">
                                                                <!--begin: Form Wizard Step 1-->
                                                                <div class="kt-wizard-v3__content text-center" data-ktwizard-type="step-content" data-ktwizard-state="current">
                                                                    <div class="kt-mb-30">
                                                                        <h3>Subscribe to Our Blog</h3>
                                                                        <p>Never Miss Any Update From Us !</p>
                                                                    </div>
                                                                   

                                                                     <!--[if lte IE 8]>
                                                    <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
                                                    <![endif]-->
                                                    <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
                                                    <script>
                                                    hbspt.forms.create({
                                                        portalId: "6715790",
                                                        formId: "dd692bd3-6a8f-4cb6-a65e-50b0af1d5e45"
                                                    });
                                                    </script>
                                                                </div>
                                                                <!--end: Form Wizard Step 1-->
                                                            </form>
                                                        </div>
                                                    </div>

                                                   
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                  
                                </div>
                            </div>
                            <!-- blog post html end here -->
                <!-- body content end -->
            </div>
        </div>
    </div>                  
</div>

@section('mainscripts')
@parent


@section('customScript')
<!--begin::Page Vendors Styles(used by this page) -->
    <script href="{{ asset('') . config('app.public_url') . '/assets/js/owl.carousel.js'  }}" type="text/javascript"></script>
    <script src="{{ asset('') . config('app.public_url') . '/assets/js/pages/crud/forms/widgets/ion-range-slider.js' }}" type="text/javascript"></script>

        <script>
            $(document).ready(function() {
              $('.owl-carousel').owlCarousel({
                loop: true,
                responsiveClass: true,
                nav: true,               
                navText : ["<i class='la la-angle-left'></i>","<i class='la la-angle-right'></i>"],
                responsive: {
                  0: {
                    items: 1,
                    dots: false,
                    autoplay: true
                  },
                  600: {
                    items: 2,
                    dots: false,
                    autoplay: true
                  },
                
                  900: {
                    items: 3,
                    dots: false
                  },
                  1200: {
                    items: 4,
                    dots: false
                  },
                  1399: {
                    items: 5,
                    dots: false
                  }
                }
              })
            })          
        </script>
        <script type="text/javascript">
            //Getting value from "ajax.php".
            function fill(Value) {
               //Assigning value to "search" div in "search.php" file.

               $('#search_keyword').val(Value);
               window.location.href = 'search?q='+Value;
               //Hiding "display" div in "search.php" file.
               $('#display').hide();
            }


            $(document).on('click','.ajax-display',function(){
                fill($(this).attr('rel'));
            });

            /** script for onsroll down and up **/
            // $(window).keydown(function(e){ 
            //     if(e.which ===   40 ){
            //        alert('true');
            //     }else if(e.which === 38){
            //         alert('false');
            //     }
            // });

             /*$(document).hover('.list-items >  li > a',function() {
                    alert('hover working!!');
                });*/
            
            window.onload = function(){
                var hideMe = document.getElementById('display');
                document.onclick = function(e){
                   if(e.target.id !== 'hideMe'){
                     $('#display').hide();
                      /*$('#display').removeClass('search-dropdown');
                      $('#display').html('');*/
                   }
                };
            };
            $(document).ready(function() {
               //On pressing a key on "Search box" in "search.php" file. This function will be called.
               $("#search_keyword").keyup(function() {
                    $('#display').addClass('search-dropdown');
                   //Assigning search box value to javascript variable named as "name".
                   var name = $('#search_keyword').val();
                   //Validating, if "name" is empty.
                   if (name == "") {
                       //Assigning empty value to "display" div in "search.php" file.
                       $("#display").html("");
                   }
                   //If name is not empty.
                   else {
                       //AJAX is called.
                       $.ajax({
                           //AJAX type is "Post".
                           headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                           },
                            type: "POST",
                           //Data will be sent to "ajax.php".
                           url: "{{ url('/search_result') }}",
                           //Data, that will be sent to "ajax.php".
                           data: {
                               //Assigning value of "name" into "search" variable.
                               search: name,
                           },
                           //If result found, this funtion will be called.
                           success: function(html) {
                               //Assigning result to "display" div in "search.php" file.
                               $("#display").html(html).show();
                           }
                       });
                   }
               });
            });
        </script>        
<!--end::Page Scripts -->
@endsection
    
@show
@endsection
