@if($record_found > 0)

@php
$totalTitle = 'List of ' . $selectQuery->total() . ' blog websites that accept Guest Blogs by FLYBIZ.';
@endphp
@section('title', $totalTitle)
<div class="">
    <!--begin:: Portlet-->
    @foreach( $selectQuery as $split_Query )    
    <?php
        $domain_id = $split_Query->domain_id;
    ?>       
    <div class="kt-portlet">
        <div class="kt-portlet__body">
            <div class="kt-widget kt-widget--user-profile-3">
                <div class="kt-widget__top">                   
                    <div class="kt-widget__content">
                        <div class="kt-widget__head">
                            <div class="d-flex align-items-center flex-wrap">
                               <!--  <div class="kt-widget__media kt-hidden-">
                                    <img src="https://www.webdew.com/hubfs/favicon-3.ico" alt="image">
                                </div> -->
                                <!-- Display this icon when favicon unavailable -->
                                <div class="d-flex align-items-center">
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon favicon-svg kt-mr-5">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24"></rect>
                                        <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="9"></circle>
                                        <path d="M11.7357634,20.9961946 C6.88740052,20.8563914 3,16.8821712 3,12 C3,11.9168367 3.00112797,11.8339369 3.00336944,11.751315 C3.66233009,11.8143341 4.85636818,11.9573854 4.91262842,12.4204038 C4.9904938,13.0609191 4.91262842,13.8615942 5.45804656,14.101772 C6.00346469,14.3419498 6.15931561,13.1409372 6.6267482,13.4612567 C7.09418079,13.7815761 8.34086797,14.0899175 8.34086797,14.6562185 C8.34086797,15.222396 8.10715168,16.1034596 8.34086797,16.2636193 C8.57458427,16.423779 9.5089688,17.54465 9.50920913,17.7048097 C9.50956962,17.8649694 9.83857487,18.6793513 9.74040201,18.9906563 C9.65905192,19.2487394 9.24857641,20.0501554 8.85059781,20.4145589 C9.75315358,20.7620621 10.7235846,20.9657742 11.7357634,20.9960544 L11.7357634,20.9961946 Z M8.28272988,3.80112099 C9.4158415,3.28656421 10.6744554,3 12,3 C15.5114513,3 18.5532143,5.01097452 20.0364482,7.94408274 C20.069657,8.72412177 20.0638332,9.39135321 20.2361262,9.6327358 C21.1131932,10.8600506 18.0995147,11.7043158 18.5573343,13.5605384 C18.7589671,14.3794892 16.5527814,14.1196773 16.0139722,14.886394 C15.4748026,15.6527403 14.1574598,15.137809 13.8520064,14.9904917 C13.546553,14.8431744 12.3766497,15.3341497 12.4789081,14.4995164 C12.5805657,13.664636 13.2922889,13.6156126 14.0555619,13.2719546 C14.8184743,12.928667 15.9189236,11.7871741 15.3781918,11.6380045 C12.8323064,10.9362407 11.963771,8.47852395 11.963771,8.47852395 C11.8110443,8.44901109 11.8493762,6.74109366 11.1883616,6.69207022 C10.5267462,6.64279981 10.170464,6.88841096 9.20435656,6.69207022 C8.23764828,6.49572949 8.44144409,5.85743687 8.2887174,4.48255778 C8.25453994,4.17415686 8.25619136,3.95717082 8.28272988,3.80112099 Z M20.9991771,11.8770357 C20.9997251,11.9179585 21,11.9589471 21,12 C21,16.9406923 17.0188468,20.9515364 12.0895088,20.9995641 C16.970233,20.9503326 20.9337111,16.888438 20.9991771,11.8770357 Z" fill="#000000" opacity="0.3"></path>
                                    </g>
                                </svg>
                                <a href="{{url('search/website/')}}{{'/'.$split_Query->domain_url}}" class="kt-widget__username">
                                {{$split_Query->domain_url}}

                                <?php
                                    $d_id = \DB::table('mst_domain_url')->select('domain_id')->where('domain_url',$split_Query->domain_url)->where('domain_status',1)->first();
                                    $d_id = json_decode(json_encode($d_id), true);
                                ?>
                                @if( $split_Query->domain_status == 1 || $d_id)
                                    <?php
                                        $domain_id = $d_id['domain_id'];
                                    ?>
                                    <i class="flaticon2-correct kt-font-primary"></i>
                                @endif              
                                </a>&nbsp;
                                </div>
                                <span>
                                    @if(!empty($split_Query->backlink_type))

                                        @if($split_Query->backlink_type == 'no_follow')
                                            @php
                                                $follow = 'No Follow';
                                                $class = 'warning';
                                            @endphp                          
                                        @elseif($split_Query->backlink_type == 'follow')
                                            @php
                                                $follow = 'Do Follow';
                                                $class = 'success';
                                            @endphp
                                        @endif
                                    @else
                                        @php
                                            $follow = 'No Follow';
                                            $class = 'warning';
                                        @endphp
                                    @endif

                                    

                                    <span class="kt-badge kt-badge--{{$class}} kt-badge--dot"></span>&nbsp;
                                    <span class="kt-font-bold kt-font-{{$class}}">
                                       {{$follow}} 
                                    </span>
                                </span>
                            </div>
                            <div class="whishlist-ribbon kt-ribbon kt-ribbon--clip kt-ribbon--right kt-ribbon--danger">

                                    <?php
                                    if(Auth::check()){
                                        $get = [];
                                        $class = '';

                                        $wishlist   = DB::table('wishlists')
                                                           ->where('user_id',Auth::user()->user_id)->pluck('wishlist_id')->toArray();
                                        if(in_array($domain_id, $wishlist)){
                                            $class = 'active-wishlist';
                                        }
                                    } else {
                                        $get = [];
                                        $class = '';
                                        if( \Cache::has( 'wishlist' ) ) {
                                            $get = \Cache::get( 'wishlist' );
                                            $get = @unserialize($get);
                                        }
                                        
                                        if(in_array($domain_id, $get)){
                                            $class = 'active-wishlist';
                                        }
                                    }
                                    ?>
                                <div class="kt-ribbon__target {{$class}}" style="top: 12px;">
                                    <input type="hidden" id="domain_id" value="{{ $domain_id }}" />

                                    <span class="kt-ribbon__inner" title="Add to Wishlist"></span><i class="la la-heart-o" style="
                                        font-size: 1.5rem;
                                        "></i>
                                </div>
                            </div>
                            

                        </div>
                        <div class="kt-widget__subhead">
                            @if(trim($split_Query->sd_title))
                                {{ str_replace('â€', "", $split_Query->sd_title)  }}
                            @elseif(trim($split_Query->sd_title_n))
                                {{ $split_Query->sd_title_n }}
                            @else
                                {{ $split_Query->title }}
                            @endif
                            
                        </div>
                        <div class="kt-widget__info">
                            <div class="kt-widget__desc">
                                @if(trim($split_Query->sd_descr))
                                    {{ $split_Query->sd_descr }}
                                @elseif(trim($split_Query->sd_descr_n))
                                    {{ $split_Query->sd_descr_n }}
                                @else 
                                    {{ $split_Query->description }}
                                @endif
                            </div>
                            <div class="kt-widget__action d-flex align-items-center">
                                <a class="btn btn-brand btn-upper btn-hover-content" href="{{url('search/website/')}}{{'/'.$split_Query->domain_url}}"><span></span><strong>
                                    
                                    $ {{buyerDomainWebsitePriceCalculate(@$split_Query->cost_price)}}
                                    </strong></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-widget__bottom">
                    <div class="kt-widget__item">
                        <div class="kt-widget__icon">
                            <svg class="moz-svg" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 280.6 280.3" style="enable-background:new 0 0 280.6 280.3;" width="40px" height="40px" xml:space="preserve">
                                <path class="st0" d="M140.2,280.3C66.1,282.2-0.6,217.8,0,138.9C0.6,64.8,64.4-2.1,144.5,0c71.7,1.9,136.8,63.8,136.1,141.7
                                    C280,218.2,214.6,282.3,140.2,280.3z M185.1,139.7c0.4,0.1,0.7,0.3,1.1,0.4c0,13.1,0.1,26.2-0.1,39.3c-0.1,4.7,2.1,6.7,6.6,6.6
                                    c6.3-0.1,12.7-0.1,19,0c2.5,0,3.6-0.7,3.5-3.4c-0.1-28.3-0.1-56.6,0-85c0-2.8-1.2-3.3-3.6-3.3c-5.2,0.1-10.3,0.1-15.5,0
                                    c-6.6-0.1-11.8,2.3-16.2,7.3c-12,13.6-24.3,27-36.3,40.6c-2.6,2.9-4,3.3-6.8,0.1c-12.1-13.8-24.5-27.3-36.6-41
                                    c-3.6-4.1-7.9-6.7-13.3-6.9c-6.2-0.3-12.3,0-18.5-0.1c-2.4,0-3.2,0.8-3.2,3.2c0.1,28.3,0.1,56.6,0,85c0,2.6,0.8,3.5,3.4,3.5
                                    c6.3-0.1,12.7,0,19-0.1c5.2-0.1,6.6-1.7,6.6-7.1c0-11.8,0-23.7,0.1-35.5c0-1.2-0.8-2.6,1-3.7c12.9,14.4,25.8,28.7,38.6,43.1
                                    c4.2,4.7,8,4.8,12.4-0.1C159.4,168.4,172.3,154.1,185.1,139.7z"></path>
                                <path class="st1" d="M185.1,139.7c-12.9,14.4-25.8,28.7-38.7,43.1c-4.3,4.8-8.2,4.8-12.4,0.1c-12.9-14.4-25.8-28.7-38.6-43.1
                                    c-1.8,1-1,2.5-1,3.7c-0.1,11.8,0,23.7-0.1,35.5c0,5.4-1.4,7-6.6,7.1c-6.3,0.1-12.7-0.1-19,0.1c-2.6,0.1-3.4-0.9-3.4-3.5
                                    c0.1-28.3,0.1-56.6,0-85c0-2.4,0.8-3.2,3.2-3.2c6.2,0.1,12.3-0.2,18.5,0.1c5.4,0.2,9.7,2.8,13.3,6.9c12.2,13.7,24.6,27.2,36.6,41
                                    c2.8,3.2,4.2,2.8,6.8-0.1c12-13.6,24.3-27,36.3-40.6c4.4-5,9.7-7.4,16.2-7.3c5.2,0.1,10.3,0.1,15.5,0c2.4-0.1,3.6,0.4,3.6,3.3
                                    c-0.1,28.3-0.1,56.6,0,85c0,2.7-1.1,3.4-3.5,3.4c-6.3-0.1-12.7-0.1-19,0c-4.6,0.1-6.7-1.9-6.6-6.6c0.1-13.1,0.1-26.2,0.1-39.3
                                    C185.9,140,185.5,139.8,185.1,139.7z"></path>
                            </svg>
                        </div>
                        <div class="kt-widget__details">
                            <span class="kt-widget__title">Domain Authority</span>
                            <div class="d-flex"><span class="kt-widget__value">{{$split_Query->dp_da}}</span>
                                <span class="d-flex align-items-end">/100</span>
                            </div>
                        </div>
                    </div>
                    <div class="kt-widget__item">
                        <div class="kt-widget__icon">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="34px" height="34px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect x="0" y="0" width="24" height="24"></rect>
                                    <path d="M13,18.9450712 L13,20 L14,20 C15.1045695,20 16,20.8954305 16,22 L8,22 C8,20.8954305 8.8954305,20 10,20 L11,20 L11,18.9448245 C9.02872877,18.7261967 7.20827378,17.866394 5.79372555,16.5182701 L4.73856106,17.6741866 C4.36621808,18.0820826 3.73370941,18.110904 3.32581341,17.7385611 C2.9179174,17.3662181 2.88909597,16.7337094 3.26143894,16.3258134 L5.04940685,14.367122 C5.46150313,13.9156769 6.17860937,13.9363085 6.56406875,14.4106998 C7.88623094,16.037907 9.86320756,17 12,17 C15.8659932,17 19,13.8659932 19,10 C19,7.73468744 17.9175842,5.65198725 16.1214335,4.34123851 C15.6753081,4.01567657 15.5775721,3.39010038 15.903134,2.94397499 C16.228696,2.49784959 16.8542722,2.4001136 17.3003976,2.72567554 C19.6071362,4.40902808 21,7.08906798 21,10 C21,14.6325537 17.4999505,18.4476269 13,18.9450712 Z" fill="#000000" fill-rule="nonzero"></path>
                                    <circle fill="#000000" opacity="0.3" cx="12" cy="10" r="6"></circle>
                                </g>
                            </svg>
                        </div>
                        <div class="kt-widget__details">
                            <span class="kt-widget__title">Global Rank</span>
                            <span class="kt-widget__value">
                                @php
                                    $global_rank = 0;
                                @endphp
                                @if(@$split_Query->sd_global_rank)
                                    @php
                                        $global_rank = unserialize(@$split_Query->sd_global_rank);
                                        $global_rank = @$global_rank['rank'];
                                    @endphp
                                @endif
                                {{$global_rank}}
                            </span>
                        </div>
                    </div>
                    <div class="kt-widget__item">
                        <div class="kt-widget__icon">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect x="0" y="0" width="24" height="24"></rect>
                                    <path d="M4.00246329,12.2004927 L13,14 L13,4.06189375 C16.9463116,4.55399184 20,7.92038235 20,12 C20,16.418278 16.418278,20 12,20 C7.64874861,20 4.10886412,16.5261253 4.00246329,12.2004927 Z" fill="#000000" opacity="0.3"></path>
                                    <path d="M3.0603968,10.0120794 C3.54712466,6.05992157 6.91622084,3 11,3 L11,11.6 L3.0603968,10.0120794 Z" fill="#000000"></path>
                                </g>
                            </svg>
                        </div>
                        <div class="kt-widget__details">
                            <span class="kt-widget__title">Traffic</span>
                            @if($split_Query->sd_visits)
                            <span class="kt-widget__value">{{@$split_Query->sd_visits}}</span>
                            @else
                            <span class="kt-widget__value">0</span>
                            @endif
                        </div>
                    </div>
                    <!-- <div class="kt-widget__item">
                        <div class="kt-widget__icon">
                            <i class="flaticon-squares-3"></i>
                        </div>
                        <div class="kt-widget__details">
                            <span class="kt-widget__title">Category</span>
                            <span class="kt-widget__value">2.08K</span>
                        </div>
                        </div> -->
                    <div class="kt-widget__item">
                        <div class="kt-widget__icon">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <polygon points="0 0 24 0 24 24 0 24"></polygon>
                                    <path d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z" fill="#000000" fill-rule="nonzero"></path>
                                    <path d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z" fill="#000000" opacity="0.3"></path>
                                </g>
                            </svg>
                        </div>
                        <div class="kt-widget__details">
                            <span class="kt-widget__title">Orders in Queue</span>
                            <span class="kt-widget__value">{{$split_Query->orders}}</span>
                        </div>
                    </div>                  
                </div>
            </div>
        </div>
    </div>
    <!--end:: Portlet-->
    {{--
    </form>--}}
    @endforeach
   
</div>
<!--Begin::Pagination-->
<div class="row">
    <div class="col-xl-12">
        <!--begin:: Components/Pagination/Default-->
        <div class="kt-portlet">
            <div class="kt-portlet__body">
                <!--begin: Pagination-->
                <div class="kt-pagination kt-pagination--brand">
                    {!! $selectQuery->onEachSide(1)->links() !!}
                    <div class="kt-pagination__toolbar">
                        <?php
                            $no_of_page = 10;
                            if(isset($_GET['offset'])){
                                $no_of_page = $_GET['offset'];
                            }
                        ?>
                        <select class="form-control kt-font-brand custom-select" style="width: 75px" >
                            <option value="10" @if($no_of_page == 10){{'selected'}}@endif>10</option>
                            <option value="20" @if($no_of_page == 20){{'selected'}}@endif>20</option>
                            <option value="30" @if($no_of_page == 30){{'selected'}}@endif>30</option>
                            <option value="50" @if($no_of_page == 50){{'selected'}}@endif>50</option>
                            <option value="100" @if($no_of_page == 100){{'selected'}}@endif>100</option>
                        </select>
                        <span class="kt-datatable__pager-detail">

                        Displaying {{$no_of_page}} of {{$selectQuery->total()}} records
                        </span>
                    </div>
                </div>
                <!--end: Pagination-->
            </div>
        </div>
        <!--end:: Components/Pagination/Default-->    
    </div>
</div>
<!--End::Pagination-->


@else
<div class="row">
    <div class="col-xl-12">
        <div class="kt-portlet">
            <div class="kt-portlet__body">
                
                No record found..
                
            </div>
        </div> 
    </div>
</div>
@endif