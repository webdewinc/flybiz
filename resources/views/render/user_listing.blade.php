<?php
    $domain_id_list = \App\MstDomainUrl::where('domain_url',$domain_url)->where('domain_approval_status','=',1)->where('cost_price','>',0)->where('trash','<',1)->groupBy('domain_fk_user_id')->pluck('domain_id');
    $userdata = \App\AssignedWebsite::whereIn('mst_domain_url_id',$domain_id_list)->get();
?>
@foreach($userdata as $split_user_data)

    <?php
        $image  = @$split_user_data->data_username->image;
        $name   = @$split_user_data->data_username->user_fname;
        $lname  = @$split_user_data->data_username->user_lname;
        
         if(!empty($image)) :
            $image = url('storage/app/') . '/' . $image;
            $name = strtoupper($rest = substr(@$name, 0, 1));
            $lname = strtoupper($rest = substr(@$lname, 0, 1));
            ?>
            <?php  if(file_exists($image)): ?>
                <span class="kt-media kt-media--circle">
                    <img src="{{$image}}" data-id="2" alt="image" />
                </span>
            <?php else : 
                ?>
                <span class="kt-badge kt-badge--username kt-badge--unified-primary kt-badge--lg kt-badge--rounded kt-badge--bold test kt-margin-b-5">
                {{$name}}{{$lname}}</span>
            <?php  endif; ?>
            <?php
        else :
            
            $image = asset('') . config('app.public_url') .'/assets/media/users/default.jpg';
            $name = strtoupper($rest = substr(@$name, 0, 1));
            $lname = strtoupper($rest = substr(@$lname, 0, 1));

            if(!empty($name)) :
                ?>
                <span class="kt-badge kt-badge--username kt-badge--unified-primary kt-badge--lg kt-badge--rounded kt-badge--bold">
                {{$name}}{{$lname}}</span>
                <?php
            else :
               ?>
               <span class="kt-media kt-media--circle">
                    <img src="{{$image}}" data-id="1" alt="image" />
                </span>
                
                <?php
            endif;
        endif;
    ?>
@endforeach