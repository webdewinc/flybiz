@extends('layout.website')
@php
$totalTitle = 'List of ' . total_website_count() . ' websites that accept Guest Blogs by GuestPostEngine.';
@endphp
@section('title', $totalTitle)
@section('title-description')

<meta name="description" content="GuestPostEngine is one of the remarkable marketplace where you get tons of website opportunities. Feel free to reach us!">
    
@endsection
@section('content')
@section('mainhead')
@parent    
@show

@section('customCSS')
@endsection

<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch tool-wrapper" id="kt_body">

    <div class="banner-wrapper text-center tool-banner">
        <h1 class="">Moz Metrix</h1>
        <p class="kt-padding-t-10" style="font-size: 15px;">Enter an URL address and get a Free Website Analysis!</p>
            @csrf
          <div class="kt-quick-search kt-quick-search--dropdown kt-quick-search--result-compact" >
              <div>
                <form name="searchForm" id="searchForm" method="POST" >
                  <div class="input-group kt-quick-search__form" id="searches">
                      <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-globe"></i></span></div>
                      <input type="text" class="form-control kt-quick-search__input" id="kt-quick-search__input" placeholder="https://Example.com OR https://www.Example.com" name="kt_quick_search__input" autocomplete="off" value="" />
                      <input class="hidden" id="action_submit" type="submit"></a>
                  </div>
                </form>
              </div>
          </div>
          <button type="submit" class="btn btn-label-brand btn-bold d-flex" id="action_submit_a"><i class="flaticon2-search-1"></i>Moz Domain authority</button>
    </div>
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
        <div class="kt-container  kt-grid__item kt-grid__item--fluid" id="audit-now">
            <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            </div>
            <div class="kt-portlet" id="append_data">
                <!-- render function -->
            </div>
        </div>
    </div>
</div>

@section('mainscripts')
@parent
  @section('customScript')
    <script type="text/javascript">      
  
  $(document).on('click','#action_submit, #action_submit_a',function(e) {
          e.preventDefault();

          var btn = $(this);
          var form = $('#searchForm');
          form.validate({
              rules: {
                  kt_quick_search__input : {
                              required:true,
                              maxlength:200,
                              //url: '/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/'
                              url:"/^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/gm"
                          }
              },
              messages:{
                  kt_quick_search__input : {
                              required:'This field is required',
                              maxlength:'Url length should not above 200.',
                              url:'Please enter valid URL.',
                          }
              },
              errorPlacement: function(error, element) {
                  if (element.attr("name") == "kt_quick_search__input") {
                    error.insertAfter("#searches");
                  } else {
                    error.insertAfter(element);
                  }
              },
              onkeyup: true,
              onfocusout: true,
              onclick: true
          });

          if (!form.valid()) {
              return;
          }

          $('.loader').fadeIn('slow');

          var this_val = $('#kt-quick-search__input').val(); 
          

           $.ajax({
                 headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  url  : "{{ url('/search/dataForRapidMoz') }}",
                  type : 'POST',
                  data: {
                        'url' : this_val,
                     },                      
                  success: function(response){
                      $('#append_data').html(response);
                      $('.loader').fadeOut('slow'); 

                  },
                  error: function(errorThrown){}
                });
 
          

          
          $('html, body').animate({
            scrollTop: $("#audit-now").offset().top
          }, 2000);

          

        });
    </script>
  @endsection
@show
@endsection
