<?php
echo '<pre>';
function curlAccess($method, $array, $content_type = 'array' ) {
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $array['url']);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    switch ($method){
      case "POST":
        curl_setopt($ch, CURLOPT_POST, 1);        
         if ($array['data'])
            curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
         break;
      case "PUT":
         curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
         if ($array['data'])
            curl_setopt($ch, CURLOPT_POSTFIELDS, $array['data']);
         break;
      default:
         curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
   }

    $headers = array();
    $headers[] = 'Content-Type: application/json';
    
   // $headers[] = 'Reseller-ID: 1000219';
    if(!empty($array['token'])){
        $headers[] = 'Authorization: Bearer ' . $array['token'];
    } 

    
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $result = curl_exec($ch);
    
    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    }
    curl_close($ch);

    if($content_type == 'array'){
        $result = json_decode($result); 
    }  
    return $result;
}

$curl = curl_init();
$auth_data = array(
	'client_id' 		=> 'g13hV?gjBdyp!8G¡rz-6',
	'client_secret' 	=> 'asdfghjkDF214839dkdf',
);
curl_setopt($curl, CURLOPT_POST, 1);
curl_setopt($curl, CURLOPT_POSTFIELDS, $auth_data);
curl_setopt($curl, CURLOPT_URL, 'https://na-stg-api-cloud-im.azure-api.net/autodesk/sand/na/2.0/getToken');
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

$result = curl_exec($curl);
if(!$result){
	die("Connection Failure");
}
curl_close($curl);
$data = json_decode($result, true);
$token = $data['data'];

$args = [];
$args['url'] = "https://na-stg-api-cloud-im.azure-api.net/autodesk/sand/na/2.0/customerpo/CD1231230000";
$args['data'] = '{
                    "account_id": "1000219",
                    "end_customer": {
                        "first_name": "Randeep",
                        "last_name": "Singh",
                        "email": "randeep.s@webdew.com",
                        "company_name": "Webdew",
                        "address_line1": "8920 Timber Wolf Trail",
                        "address_line2": "",
                        "city": "Nampa",
                        "state": "Louisiana",
                        "postal_code": "46674",
                        "country_code": "US",
                        "phone_country_code": "1",
                        "phone_area_code": "602",
                        "phone_number": "7303096",
                        "phone_extension": "31"
                    },
                    "contract_manager": {
                        "first_name": "Randeep",
                        "last_name": "Singh",
                        "email": "randeep.s@webdew.com",
                        "country_code": "US",
                        "language": "EN"
                    },
                    "ship_to": {
                        "name": "Shipping Inc",
                        "address_line1": "8920 Timber Wolf Trail",
                        "address_line2": "Lot 32",
                        "city": "Los Angeles",
                        "state": "CA",
                        "postal_code": "90210",
                        "country_code": "US"
                    },
                    "customer_po_number": "CD1231230000",
                    "bill_to": "DEFAULT",
                    "line_items": [
                        {
                            "part_number": "057K1-WW5051-T455",
                            "quantity": "2",
                            "margin": "14.50"
                        }
                    ]
                }';

                $args['data'] = '{
"bill_to": "DEFAULT",
"line_items": [
{
"serial_number": "712-10450796",
"margin": "12.50"
}
]
}
';
$args['token'] = $token;

$result = curlAccess( 'GET', $args );
print_r($result );
die;

?>