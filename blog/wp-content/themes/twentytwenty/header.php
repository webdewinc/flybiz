<?php
/**
 * Header file for the Twenty Twenty WordPress default theme.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

?><!DOCTYPE html>

<html class="no-js" <?php language_attributes(); ?>>

	<head>

		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" >

		<link rel="profile" href="https://gmpg.org/xfn/11">

		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

		<?php wp_head(); ?>

	</head>

	<body <?php body_class(); ?>>

		<?php
		wp_body_open();
		?>
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

			<div id="kt_header" class="kt-header  kt-header--fixed" data-ktheader-minimize="on">
			<div class="kt-container  kt-container--fluid">
						<!-- begin: Header Menu -->
				<button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
				
				<!-- begin:: Brand -->
				<div class="kt-header__brand   kt-grid__item" id="kt_header_brand">		
					<div class="kt-header__brand   kt-grid__item" id="kt_header_brand">		
					<a class="kt-header__brand-logo" href="http://localhost/projects/laravel/gpe_laravel/">
						<img alt="Logo" src="http://localhost/projects/laravel/gpe_laravel/public/assets/media/logos/fly.biz_logo.svg" title="FLY.BIZ">
					</a>
					</div>		
			</div>
			<!-- end: Header Menu -->	
			<!-- begin:: Header Topbar -->
			<div class="kt-header__topbar kt-grid__item">

				<!-- end:: Brand -->
				<div class="kt-header-menu-wrapper kt-grid__item kt-grid__item--fluid justify-content-center flex-grow-1" id="kt_header_menu_wrapper">
					
					<div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile ">
						<div class="kt-quick-search kt-quick-search--dropdown kt-quick-search--result-compact" id="">
						
			            <form method="get" class="kt-quick-search__form kt-margin-0" onsubmit="return validateForm()" name="searchForm" action="http://localhost/projects/laravel/gpe_laravel/search">
						    <div class="input-group">
						        <div class="input-group-prepend"><span class="input-group-text"><i class="flaticon2-search-1"></i></span></div>
						        <input type="text" class="form-control search_keyword kt-quick-search__input" name="q" id="search_keyword_search" placeholder="Search from the List of 517 Blogs that accept Guest Posts" autocomplete="off" value="" rel="search" required="">
						        <div class="input-group-append"><span class="input-group-text"><i class="la la-close kt-quick-search__close"></i></span></div>
						    </div>
						</form>
						<div class="kt-quick-search__wrapper kt-scroll ps" data-scroll="true" data-height="325" data-mobile-height="200" style="height: 325px; overflow: hidden;">
							<div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
						</div
						><div class="ps__rail-y" style="top: 0px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div>
					</div>
				</div>
			</div>	
		</div>
	</div>

	 

	<!--begin: Cart -->
	 


					 
					<!--end: Cart -->

					<div class="kt-header__topbar-item">
						<div class="kt-header__topbar-wrapper align-items-center">
	<a href="http://localhost/projects/laravel/gpe_laravel/signup" class="btn btn-label-brand btn-bold">Sign Up</a>
</div>	

<div class="kt-header__topbar-wrapper align-items-center kt-ml-10">
	<a href="http://localhost/projects/laravel/gpe_laravel/signin" class="btn btn-label-brand btn-bold">Sign In</a>
</div>					</div>
				</div>
			
			
		</div>

		<!-- end:: Header Topbar -->
	</div>
</div>

		<?php
		// Output the menu modal.
		get_template_part( 'template-parts/modal-menu' );
