<?php
/**
 * The template for displaying the footer
 *
 * Contains the opening of the #site-footer div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

?>
			<div class="kt-footer kt-grid__item" id="kt_footer">
	<div class="kt_footer-top">
		<div class="kt-container ">
			<div class="kt-footer__wrapper pt-4 pb-4 flex-wrap">
				<div class="kt-social_icons text-center w-100 pb-3">
					<a href="https://www.facebook.com/flydotbiz/" target="_blank" class="kt-link mr-4"><i class="socicon-facebook text-white"></i></a>
					<a href="https://www.facebook.com/groups/2026441214124203/" target="_blank" class="kt-link mr-4"><i class="fa fa-user text-white"></i></a>
					<a href="https://twitter.com/flydotbiz" target="_blank" class="kt-link mr-4"><i class="socicon-twitter text-white"></i></a>
					<a href="mailto:go@fly.biz" target="_blank" class="kt-link"><i class="socicon-mail text-white"></i></a>
				</div>
				<div class="kt-footer__menu justify-content-center w-100 pb-3 pl-0">
					<a href="https://blog.fly.biz/playbook" target="_blank" class="kt-link">Playbook</a>
					<a href="https://blog.fly.biz" target="_blank" class="kt-link">Blog</a>
					<!-- <a href="#" target="_blank" class="kt-link">Affiliate</a> -->
					<a href="http://help.fly.biz/" target="_blank" class="kt-link">Help</a>
					<a href="http://localhost/projects/laravel/gpe_laravel/about" class="kt-link">About</a>
					<a href="http://localhost/projects/laravel/gpe_laravel/contact" class="kt-link">Contact</a>
				</div>				
				<div class="kt-footer__copyright w-100 justify-content-center pr-0">
					Made with <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--danger pr-1 pl-1">
						<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
						<polygon points="0 0 24 0 24 24 0 24"></polygon>
						<path d="M16.5,4.5 C14.8905,4.5 13.00825,6.32463215 12,7.5 C10.99175,6.32463215 9.1095,4.5 7.5,4.5 C4.651,4.5 3,6.72217984 3,9.55040872 C3,12.6834696 6,16 12,19.5 C18,16 21,12.75 21,9.75 C21,6.92177112 19.349,4.5 16.5,4.5 Z" fill="#000000" fill-rule="nonzero"></path>
						</g>
					</svg> by <a href="https://www.webdew.com/" class="kt-link" style="margin-left:5px;"> Webdew, Inc</a>
				</div>
			</div>
		</div>
	</div>
	<div class="kt_footer-bottom">
		<div class="kt-container ">
			<div class="kt-footer__wrapper row">
				<div class="col-md-6">
					<div class="kt-footer__copyright text-primary kt-font-bold">
						Copyright  2019 - 2020 FLY.BIZ. All Rights Reserved
					</div>
				</div>
				<div class="col-md-6">
					<div class="kt-footer__menu justify-content-center justify-content-xl-end justify-content-lg-end justify-content-md-end pl-0">
						<a href="https://help.fly.biz/privacy-policy" target="_blank" class="kt-link pl-0 ml-0 text-primary">Privacy Policy</a>
						<a href="https://help.fly.biz/terms-of-service" target="_blank" class="kt-link text-primary">Terms of Service</a>
					</div>
				</div>																	
			</div>
		</div>
	</div>
</div>

</div>

		<?php wp_footer(); ?>

	</body>
</html>
