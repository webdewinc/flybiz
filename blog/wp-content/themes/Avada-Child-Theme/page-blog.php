<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since Twenty Seventeen 1.0
 * @version 1.0
 */

// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
?>
<?php get_header(); ?>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php

				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                                  $args = array(
                                        'post_type' => 'post',
                                        'post_status' => 'publish',
                                        'posts_per_page' => '5',
                                        'paged' => $paged,
                                        'category__not_in'=> array(24)
                                    );
                                  $loop = new WP_Query( $args );
                                   query_posts($args);
                                 while ( $loop->have_posts() ) : $loop->the_post(); ?>
                        <div class="blog_wrap d-flex" id="my-posts">
                            <div class="blog_img">
                                <?php if ( has_post_thumbnail() ) { ?>
                                <img alt="<?php the_title(); ?>" title="<?php the_title(); ?>" src="<?php the_post_thumbnail_url( array(342, 218) );?>">
                                <?php }  ?>
                            </div>
                            <div class="blog_contant">
                                <h4><a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>" target="_blank"><?php the_title(); ?></a></h4>
                                <span class="masc-date">by <?php echo get_the_author(); ?> | <?php echo get_the_date('M d, Y'); ?></span>   
                                <p><?php echo wp_trim_words(get_the_content(), 25); ?><a class="read-more" href="<?php the_permalink() ?>" target="_blank">read more</a></p>
                               
                            </div>
                        </div>
                        <?php endwhile; ?>
                        <div class="clearfix"></div>
                        <?php wp_reset_postdata(); ?> 
                    </div>
		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->




<?php get_footer(); ?>
