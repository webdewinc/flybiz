<?php
/**
 * Header-3-content template.
 *
 * @author     ThemeFusion
 * @copyright  (c) Copyright by ThemeFusion
 * @link       https://theme-fusion.com
 * @package    Avada
 * @subpackage Core
 * @since      5.1.0
 */

if ( 'v4' !== Avada()->settings->get( 'header_layout' ) && 'top' === fusion_get_option( 'header_position' ) ) {
	return;
}

$header_content_3 = fusion_get_option( 'header_v4_content' );
?>



 <form method="get" class="kt-quick-search__form">
            <div class="input-group">
              <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-search" aria-hidden="true"></i></span></div>
              <input type="text" class="form-control kt-quick-search__input" placeholder="Search from the List of 11656 Blogs that accept Guest Posts">
              <div class="input-group-append"><span class="input-group-text"><i class="la la-close kt-quick-search__close"></i></span></div>
            </div>
          </form>






<div class="fusion-header-content-3-wrapper">
	<?php if ( 'tagline' === $header_content_3 ) : ?>
		<h3 class="fusion-header-tagline">
			<?php echo do_shortcode( Avada()->settings->get( 'header_tagline' ) ); ?>
		</h3>
	<?php elseif ( 'tagline_and_search' === $header_content_3 ) : ?>
		<h3 class="fusion-header-tagline">
			<?php echo do_shortcode( Avada()->settings->get( 'header_tagline' ) ); ?>
		</h3>
		<div class="fusion-secondary-menu-search">
			<?php get_search_form( true ); ?>
		</div>
	<?php elseif ( 'search' === $header_content_3 ) : ?>
		<div class="fusion-secondary-menu-search">
			<?php get_search_form( true ); ?>
		</div>
	<?php elseif ( 'banner' === $header_content_3 ) : ?>
		<div class="fusion-header-banner">
			<?php echo do_shortcode( Avada()->settings->get( 'header_banner_code' ) ); ?>
		</div>
	<?php endif; ?>
</div>
