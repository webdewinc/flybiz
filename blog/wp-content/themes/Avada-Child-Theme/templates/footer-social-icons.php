<?php
/**
 * Footer social icons template.
 *
 * @author     ThemeFusion
 * @copyright  (c) Copyright by ThemeFusion
 * @link       https://theme-fusion.com
 * @package    Avada
 * @subpackage Core
 * @since      5.3.0
 */

?>





<div class="kt-footer__menu justify-content-center justify-content-xl-end justify-content-lg-end justify-content-md-end pl-0">
											
<a class="privacy-policy" style="color:#009bff;" href="http://help.fly.biz/en/articles/3749167-privacy-policy" target="_blank" class="kt-link pl-0 ml-0 text-primary">Privacy Policy</a>&nbsp;&nbsp;
	<a class="privacy-policy" style="color:#009bff;" href="http://help.fly.biz/en/articles/3749151-terms-of-services" target="_blank" class="kt-link text-primary">Terms of Service</a>
</div>


<div class="fusion-social-links-footer">




	<?php
	$social_icons = fusion_get_social_icons_class();

	if ( $social_icons ) {
		$footer_social_icon_options = [
			'position'          => 'footer',
			'icon_boxed'        => Avada()->settings->get( 'footer_social_links_boxed' ),
			'tooltip_placement' => fusion_get_option( 'footer_social_links_tooltip_placement' ),
			'linktarget'        => Avada()->settings->get( 'social_icons_new' ),
		];

		//echo $social_icons->render_social_icons( $footer_social_icon_options ); // phpcs:ignore WordPress.Security.EscapeOutput
	}
	?>
</div>
