
jQuery(document).ready( function($) {
    $(".kt-quick-search__form").hide(); 
    $(".fix-link-header").hide(); 
    var topOfOthDiv = $(".post-content").offset().top;
    var second_header = $("#section1").offset().top;
    var second_header_flick = $(".min-space").offset().top;
    $(window).scroll(function() {
        if($(window).scrollTop() >= topOfOthDiv) { 
            $(".kt-quick-search__form").show();
        }else{
            $(".kt-quick-search__form").hide();
        }
        if($(window).scrollTop() >= second_header_flick) { 
            $(".fusion-header-wrapper").hide(); 
        }
        if($(window).scrollTop() >= second_header) { 
            $(".fix-link-header").show(); 
        }else{
            $(".fix-link-header").hide(); 
            $(".fusion-header-wrapper").show();             
        }

    });


    jQuery('a[href^="#"]').click(function(){

    var the_id = jQuery(this).attr("href");
    if(the_id == '#section1'){

        jQuery('html, body').animate({
            scrollTop:jQuery(the_id).offset().top
        }, 'slow');
    }else{
        jQuery('html, body').animate({
            scrollTop:jQuery(the_id).offset().top-70
        }, 'slow');

    }

    });
});

