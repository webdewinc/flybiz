<?php
error_reporting(0);
$login = "https://msgwow.com/signup-thank-you/";
?>
<style type="text/css">
form label {
    display: inline-block;
    width: 100px;
}

form div {
    margin-bottom: 10px;
}

.error {
    color: red;
    margin-left: 5px;
    font-size: 13px;
}

label.error {
    display: inline;
}

.clear span.error {
    font-size: 14px;
    position: absolute;
    top: 0;
    left: 0;
}
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<form action="" name="signup" id="first_form" method="POST">
    <div class="formRow col--company-name">
      <div class="row-inner">
        <p>Company Name*</p>
        <input type="text" placeholder="Company Name" class="inp inp-medium" id="user_company_name" name="user_company_name">
        </div>
    </div>
    <div class="formRow col--full-name">
      <div class="row-inner">
        <p>Full Name*</p>
        <input type="text" placeholder="Full Name" class="inp inp-medium" id="user_full_name" value="<?php echo $user_full_name?>" name="user_full_name">
        <input type="hidden" placeholder="" class="inp inp-medium" id="resellerId" value="your reseller_id" name="reseller_id">
        <input type="hidden" placeholder="" class="inp inp-medium" id="source" value="Unknown" name="source">
        <input type="hidden" name="redirect_url" id="redirect_url" value="https://fly.biz/shop/sms/login" />
    </div>
  </div>
    <input type="hidden" placeholder="Choose a unique username" class="inp inp-medium" id="user_name" name="user_name">
    <div class="formRow signup_text_boxes_mobile">
      <div class="row-inner">
        <p>Your Mobile Number*</p>
        <div class="clear mobile--display-flex">
            <input type="text" value="91" class="inp inp-medium" name="user_country_code" id="user_country_code" disabled value="<?php echo $user_country_code?>">
            <input type="text" class="inp inp-medium" id="user_mobile_number" name="user_mobile_number" value="<?php echo $user_mobile_number?>" placeholder="98XXXXXXXX">
        </div>
    </div>
  </div>

    <div class="formRow col--email-name">
      <div class="row-inner">
        <p>Email ID*</p>
        <input type="text" placeholder="User Name (Email)" class="inp inp-medium" id="user_email" value="<?php echo $user_email?>" name="user_email">
    </div>
  </div>

    <div class="formRow col--industries">
      <div class="row-inner">
        <p>Your Industry</p>
        <select name="user_industry" id="user_industry" class="selinpt" value="<?php echo $user_industry?>">
            <option value="">Select Industry</option>
            <option value="Agriculture ">Agriculture </option>
            <option value="Automobile & Transport">Automobile & Transport</option>
            <option value="Ecommerce">Ecommerce</option>
            <option value="Education">Education</option>
            <option value="Financial Institution">Financial Institution</option>
            <option value="Gym">Gym</option>
            <option value="Hospitality">Hospitality</option>
            <option value="IT Company">IT Company</option>
            <option value="Lifestyle Clubs">Lifestyle Clubs</option>
            <option value="Logistics">Logistics</option>
            <option value="Marriage Bureau">Marriage Bureau</option>
            <option value="Media & Advertisement">Media & Advertisement</option>
            <option value="Personal Use">Personal Use</option>
            <option value="Political ">Political </option>
            <option value="Public Sector">Public Sector</option>
            <option value="Real estate">Real estate</option>
            <option value="Reseller">Reseller</option>
            <option value="Retail & FMCG">Retail & FMCG</option>
            <option value="Stock and Commodity">Stock and Commodity</option>
            <option value="Telecom">Telecom</option>
            <option value="Tips And Alert">Tips And Alert</option>
            <option value="Travel">Travel</option>
            <option value="Wholesalers Distributors or Manufacturers">Wholesalers Distributors or Manufacturers</option>
        </select>
    </div>
  </div>

    <div class="clear signup--button">
        <button type="button" id="strtMsg" class="btn btn-medium btn-solid btn-block">Start Messaging</button>
    </div>
</form>
<script type="text/javascript">
$(document).ready(function() {
    $('#strtMsg').click(function(e) {
        var err = false;
        var user_company_name = $('#user_company_name').val();
        var user_full_name = $('#user_full_name').val();
        var user_country_code = $('#user_country_code').val();
        var user_mobile_number = $('#user_mobile_number').val();
        var user_email = $('#user_email').val();
        var user_industry = $('#user_industry option:selected').val();
        
        $(".error").remove();
        if (user_company_name.length < 1) {
            err = true;
            $('#user_company_name').after('<span class="error">Please Enter Company Name</span>');
        }
        if (user_full_name.length < 1 || (/^[\\p{L} .'-]+$/).test(user_full_name)) {
            err = true;
            $('#user_full_name').after('<span class="error">Please Enter Full Name</span>');
        }
        if (user_country_code.length < 1) {
            err = true;
            $('#user_country_code').after('<span class="error">Please Add Country Code 91</span>');
        }
        if (user_mobile_number.length != 10 || (/[^0-9]/).test(user_mobile_number)) {
            err = true;
            $('.mobile--display-flex').after('<span class="error">Please Enter Correct Mobile Number</span>');
        }
        if (!user_industry) {
            err = true;
            $('#user_industry').after('<span class="error">Please Select Altleast One Value</span>');
        }

        var regEx = /^\S+@\S+$/;
        var validEmail = regEx.test(user_email);

        if (!validEmail) {
            err = true;
            $('#user_email').after('<span class="error">Enter a valid email</span>');
        }

        if (!err) {

           var data = {
                'action': 'register_ajax_request',
                'user_company_name': user_company_name, 
                'user_full_name': user_full_name,
                'user_country_code': user_country_code,
                'user_mobile_number': user_mobile_number,
                'user_email': user_email,
                'user_industry': user_industry
           };
            $('#strtMsg').attr('disabled','disabled');
            $.ajax({
                //url: "<?php //echo admin_url( 'admin-ajax.php' ); ?>", // or example_ajax_obj.ajaxurl if using on frontend
                method: 'POST',
                url: 'msgwow',
                data: data,
                success:function(result) {

                    if(result.status == false){
                        $('#'+result.alert).after('<span class="error">'+result.message+'</span>');
                        $('#strtMsg').removeAttr('disabled');
                    } else {
                        //$('#'+result.alert).after('<label class="success">'+result.message+'</span>');
                        window.location.href = "https://msgwow.com/signup-thank-you/";
                    }
                   
                   
                },
                error: function(errorThrown){
             
                    console.log(errorThrown);
                }
            });
            return false;  //This doesn't prevent the form from submitting.
        }

    })
});
</script>