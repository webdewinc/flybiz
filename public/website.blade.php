<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

    <title>@yield('title')</title>
     
    <!-- begin::Head -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!--begin::Page Vendors Styles(used by this page) -->
    @yield('title-description')
    
    @section('mainhead')
        @include('layout.include.mainhead')
    @show

    @yield('customCSS')
    <!-- end::Head -->
    <!-- <script src="{{URL::asset('public/manifest.json')}}"></script> -->

     @if(Auth::check())

    <script>

      window.intercomSettings = {
        app_id: "qn6bzp46",
        name: "{{ Auth::user()->user_fname }}", // Full name
        email: "{{ Auth::user()->email }}", // Email address
        created_at: "{{ Auth::user()->created_at }}" // Signup date as a Unix timestamp
      };
    </script>

    

    @else
        
        <script>

          window.intercomSettings = {
            app_id: "qn6bzp46"
          };
        </script>

    @endif


    <script>(function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',w.intercomSettings);}else{var d=document;var i=function(){i.c(arguments);};i.q=[];i.c=function(args){i.q.push(args);};w.Intercom=i;var l=function(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/qn6bzp46';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);};if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>

</head>
    <?php 
        if(Auth::check()){
            $class = Auth::user()->switched . '-wrapper ';
        } else {
            $class = '';
        }
    ?>
    <body class="{{$class}}kt-page--loading-enabled kt-page--loading kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--left kt-aside--fixed kt-page--loading">   


    <!-- begin::Page loader -->

        <!-- loader -->
        @include('layout.include.partials.loader')

        <!-- end::Page Loader -->

        <!-- begin:: Page -->

        <!-- begin:: Header Mobile -->
        <div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
            <div class="kt-header-mobile__logo">
                @include('layout.include.partials.brand')
            </div>
            <div class="kt-header-mobile__toolbar">
                <button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more-1"></i></button>
            </div>
        </div>

        <!-- end:: Header Mobile -->
        <div class="kt-grid kt-grid--hor kt-grid--root">
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
                <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

                    @include('layout.include.websiteheader')
                    @if(Auth::check())
                        @include('layout.include.aside')                    
                    @endif

                         @yield('content')

            <!-- end::Page Loader -->
                    @include('layout.include.footer')
            <!-- end:: Footer -->
                </div>
            </div>
        </div>

    <!-- end:: Page -->
 
    

        <!-- begin::Scrolltop -->
        <div id="kt_scrolltop" class="kt-scrolltop">
            <i class="fa fa-arrow-up"></i>
        </div>

        <!-- end::Scrolltop -->

        @section('mainscripts')
            @include('layout.include.mainscripts')
        @show

        <script>
            $(document).ready(function(){
                $( ".txtOnly" ).keypress(function(e) {
                    var key = e.keyCode;
                    if (key >= 48 && key <= 57) {
                        e.preventDefault();
                    }
                });
                $( ".numberOnly" ).keypress(function(e) {
                    var key = e.keyCode;
                    if (key >= 48 && key <= 57) {
                        
                    } else {
                        e.preventDefault();
                    }
                });
            });
        </script>
        @yield('customScript')

    </body>
    <!-- end::Body -->
</html>