/* 
 ==========================================================================
 DOCUMENT INFORMATION
 ========================================================================== 
 *
 * Document: BA Main Scripts
 * Version:  1.0.0 
 * Client:   Hubspot Marketplace
 * Author:   Struto Ltd - www.struto.co.uk
 *

 ==========================================================================  */

$(document).ready(initPage);

function initPage(){
    initDOM();
}

/*  DOM Functions */
function initDOM(){
    //Mobile Menu
    $('.custom-menu-primary').addClass('js-enabled');
    $('.custom-menu-primary .hs-menu-wrapper').before('<div class="mobile-trigger"><i></i></div>');
    $('.custom-menu-primary .flyouts .hs-item-has-children > a').after(' <div class="child-trigger"><i></i></div>');
    $(".custom-menu-primary ul li.hs-menu-depth-1.hs-item-has-children").append("<span class='fa fa-angle-down'></span>");
    $(".custom-menu-primary ul li.hs-menu-depth-2.hs-item-has-children").append("<span class='fa fa-angle-right'></span>");
   $('.mobile-trigger').click(function() {
        $(this).next('.custom-menu-primary .hs-menu-wrapper').slideToggle(250);
        $('body').toggleClass('mobile-open');
        $('.child-trigger').removeClass('child-open');
        $('.hs-menu-children-wrapper').slideUp(250);
      /*
        var wHeight = $(window).height() - 100;
        $('.hs-menu-wrapper ul:first-child').css({
            'height' : wHeight    
        });*/
        return false;
    });
    $('.child-trigger').click(function() {
        $(this).parent().siblings('.hs-item-has-children').find('.child-trigger').removeClass('child-open');
        $(this).parent().siblings('.hs-item-has-children').find('.hs-menu-children-wrapper').slideUp(250);
        $(this).next('.hs-menu-children-wrapper').slideToggle(250);
        $(this).next('.hs-menu-children-wrapper').children('.hs-item-has-children').find('.hs-menu-children-wrapper').slideUp(250);
        $(this).next('.hs-menu-children-wrapper').children('.hs-item-has-children').find('.child-trigger').removeClass('child-open');
        $(this).toggleClass('child-open');
        return false;
    });
    
    //Google Search Animation
    $('.search-wrap').click(function() {
        $('.search-section, .custom-menu-primary, .search-wrap, .search-close, .google-search-wrapper','#mainMenu').addClass('search-open');
       // This "return false" is conflicting with the HubSpot search auto complete click event that why has been disabled
        //return false;
    });
    $('.search-close').click(function() {
        $('.search-section, .custom-menu-primary, .search-wrap, .search-close, .google-search-wrapper','#mainMenu').removeClass('search-open');
        // This "return false" is conflicting with the HubSpot search auto complete click event that why has been disabled
        //return false;
    });
    
    
    if ($(window).scrollTop() > 100 ){
  	    $('.hs-page').addClass('scrolling');
  	}
      	
    //Menu Background transition
    $(window).scroll(function() {
      	if ($(window).scrollTop() > 100 ){
      	    $('.hs-page').addClass('scrolling');
      	} else {
      	    $('.hs-page').removeClass('scrolling');
      	}; 
    });
  
//       /* testimonial slider */
//     $(function() {
//     /* Filter System - Resources */
//       $('#filterWrap').jplist({       
//         itemsBox: '.list' 
//         ,itemPath: '.list-item' 
//         ,panelPath: '.jplist-panel' 
//       });
//     });
//     // Team modal
//     teamModals();  
  
    //Scroll-to-top 
    $(window).scroll(function() {
        if ($(window).scrollTop() > 1000) {
            $(".c-scroll-top").addClass("scroll-show");
        } else {
            $(".c-scroll-top").removeClass("scroll-show");
        }
    });
    $(".c-scroll-top").click(function(event) {
        event.preventDefault();
        $("html, body").animate({scrollTop: 0}, 350);
        return false;
    });
}
 //Auto height on mobile menu dropdown 
$(window).on('load resize', function () {
    var wHeight = $(window).height() - 100;
    $('.mobile-open .hs-menu-wrapper ul:first-child').css({
        'height' : wHeight    
    });
});


/* Smooth anchor scrolling */

  
    //by Chris Coyier
    // Select all links with hashes
    $('a[href*="#"]')
      // Remove links that don't actually link to anything
      .not('[href="#"]')
      .not('[href="#0"]')
      .click(function(event) {
        // On-page links
        if (
          location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
          && 
          location.hostname == this.hostname
        ) {
          // Figure out element to scroll to
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
          // Does a scroll target exist?
          if (target.length) {
            // Only prevent default if animation is actually gonna happen
            event.preventDefault();
            $('html, body').animate({
              scrollTop: target.offset().top - 200
            }, 500, function() {
              // Callback after animation
              // Must change focus!
              var $target = $(target);
              $target.focus();
              if ($target.is(":focus")) { // Checking if the target was focused
                return false;
              } else {
                $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
                $target.focus(); // Set focus again
              };
            });
          }
        }
      });






var hsSearch = function(_instance) {
  var TYPEAHEAD_LIMIT     = 3;
  var searchForm          = _instance,
      searchField         = _instance.querySelector('.hs-search-field__input'),
      searchTerm          = "",
      searchResults       = _instance.querySelector('.hs-search-field__suggestions'),
      searchOptions       = function() {
        var formParams = [];
        var form = document.querySelector('form');
        for ( var i = 0; i < form.querySelectorAll('input[type=hidden]').length; i++ ) {
           var e = form.querySelectorAll('input[type=hidden]')[i];
            if (e.name !== 'limit') {
              formParams.push(encodeURIComponent(e.name) + "=" + encodeURIComponent(e.value));
            }
        }
        var queryString = formParams.join("&");
        return queryString;
      };
  
  var debounce = function(func, wait, immediate) {
    var timeout;
    return function() {
      var context = this,
          args = arguments;
      var later = function() { 
        timeout = null;
        if ( !immediate ) {
          func.apply(context, args);
        }
      };
      var callNow = immediate && !timeout;
      clearTimeout(timeout);
      timeout = setTimeout(later, wait || 200);
      if ( callNow ) { 
        func.apply(context, args); 
      }
    };
  },
  emptySearchResults = function() {
    var items = [];
    items.push( "<li id='results-for'>No Results</li>" );
    searchResults.innerHTML = items.join("");
    searchForm.classList.add('hs-search-field--open');
    
    if(searchField.value == ""){
      searchResults.innerHTML = '';
      searchField.focus();
      searchForm.classList.remove('hs-search-field--open');
    }
  },
  fillSearchResults = function(results){
    var items = [];
    
    items.push( "<li id='results-for'>Results for \"" + searchTerm + "\"</li>" );
    results.forEach(function(val, index) {
      var title             = val.title,
          resultUrl         = val.url,
          resultDescription = val.description;
      items.push( "<li id='result" + index + "'><a href='" + resultUrl + "'>" + title + "</a></li>" );
    });

    emptySearchResults();
    searchResults.innerHTML = items.join("");
    searchForm.classList.add('hs-search-field--open');
  },
  getSearchResults = function(){
    var searchUrl = "/_hcms/search?&term="+encodeURIComponent(searchTerm)+"&limit="+encodeURIComponent(TYPEAHEAD_LIMIT)+"&autocomplete=true&analytics=true&" + searchOptions();
    $.getJSON(searchUrl, function(data){
      
      if (data.results.length > 0) {
        fillSearchResults(data.results);
        trapFocus();      
        
      }
      else {
        emptySearchResults();
      }
    });
  },
  trapFocus = function(){
    var tabbable = [];
    tabbable.push(searchField);
    var tabbables = searchResults.getElementsByTagName('A');
    for (var i = 0; i < tabbables.length; i++) {
      tabbable.push(tabbables[i]);
    }
    var firstTabbable = tabbable[0],
        lastTabbable  = tabbable[tabbable.length-1];
    var tabResult = function(e){
      if (e.target == lastTabbable && !e.shiftKey) {
        e.preventDefault();
        firstTabbable.focus();      
      }
      else if (e.target == firstTabbable && e.shiftKey) {
        e.preventDefault();
        lastTabbable.focus();
      } 
    },
    nextResult = function(e) {
      e.preventDefault();
      if (e.target == lastTabbable) {
        firstTabbable.focus();
      }
      else {
        tabbable.forEach(function(el){
          if (el == e.target) {
            tabbable[tabbable.indexOf(el) + 1].focus();
          }
        });
      }
    },
    lastResult = function(e) {
      e.preventDefault();
      if (e.target == firstTabbable) {
        lastTabbable.focus();
      }
      else {
        tabbable.forEach(function(el){
          if (el == e.target) {
            tabbable[tabbable.indexOf(el) - 1].focus();
          }
        });
      }
    };
    searchForm.addEventListener('keydown', function(e){
      switch (e.which) {
        case 9:
          emptySearchResults(); 
          tabResult(e);
          break;
        case 27:
          emptySearchResults();
          break;
        case 38:
          lastResult(e);
          break;
        case 40:
          nextResult(e);
          break;
      }
    });      
  },
  db = debounce(function() {
    searchTerm = searchField.value;
    if(searchTerm.length > 2) {
      getSearchResults();
    }    
    else if (searchTerm.length == 0)  {
      
      emptySearchResults(); 
    }
  }, 250),
  init = (function(){
    searchField.addEventListener('input', function(e) {
      if ((e.which != 9) && (e.which != 40) && (e.which != 38) && (e.which != 27) && (searchTerm != searchField.value)) {
        db();
      }
    });
  })();
}

if (document.attachEvent ? document.readyState === "complete" : document.readyState !== "loading"){
  var searchResults = document.querySelectorAll('.hs-search-field');
  Array.prototype.forEach.call(searchResults, function(el){
    var hsSearchModule = hsSearch(el);
  });
} else {
  document.addEventListener('DOMContentLoaded', function() {
    var searchResults = document.querySelectorAll('.hs-search-field');
    Array.prototype.forEach.call(searchResults, function(el){
      var hsSearchModule = hsSearch(el);
    });
  });
}