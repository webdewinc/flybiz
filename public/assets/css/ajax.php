    <?php
    //Including Database configuration file.
    include_once "include/db_config.php";
    $db = new DB_con();
    $db = $db->ret_obj();
    //Getting value of "search" variable from "script.js".
    if (isset($_POST['search'])) {
    //Search box value assigning to $Name variable.
       $Name = $_POST['search'];
    //Search query.
       $Query = "SELECT DISTINCT query_string as 'name' FROM log_searchquery WHERE query_string LIKE '$Name%' LIMIT 500";

       // log_searchquery -> query_string
       // mst_domain_url -> domain_website
       // mst_final_domains -> Domain
       // mst_similar_site - > smsite_url

    //Query execution
       $ExecQuery =  $db->query($Query);
    //Creating unordered list to display result.
       echo '
    <ul>
       ';
       //Fetching result from database.
       while ($Result = mysqli_fetch_assoc($ExecQuery)) {
       	$result  = preg_replace('/[^a-zA-Z0-9_ -]/s','',$Result['name']);
           ?>
       <!-- Creating unordered list items.
            Calling javascript function named as "fill" found in "script.js" file.
            By passing fetched result as parameter. -->
       <li onclick='fill("<?php echo $result; ?>")'>
       <a>
       <!-- Assigning searched result in "Search box" in "search.php" file. -->
           <?php echo $result; ?>
       </li></a>
       <!-- Below php code is just for closing parenthesis. Don't be confused. -->
       <?php
    }}
    ?>
    </ul>